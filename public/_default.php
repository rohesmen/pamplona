<?php

error_reporting(E_ALL);

try {

    /**
     * Define some useful constants
     */
    define('BASE_DIR', dirname(__DIR__));
    define('APP_DIR', BASE_DIR . '/app');
	define('IMG_DIR', BASE_DIR . '/public/img');
    date_default_timezone_set ( "America/Monterrey" );

	/**
	 * Read the configuration
	 */
	$config = include APP_DIR . '/config/config.php';

    if (is_readable(APP_DIR . '/config/config.dev.php')) {
        $override = include APP_DIR . '/config/config.dev.php';
        $config->merge($override);
    }


	
	/**
	 * Read auto-loader
	 */
	include APP_DIR . '/config/loader.php';

	/**
	 * Read services
	 */
	include APP_DIR . '/config/services.php';

	/**
	 * Handle the request
	 */
	$application = new \Phalcon\Mvc\Application($di);

	$content = $application->handle()->getContent();

	if($application->request->getHeader("Enc")){
		$isCryptoStrong = false;
		$iv = openssl_random_pseudo_bytes(16, $isCryptoStrong);

		echo(base64_encode($iv).'.'.base64_encode(openssl_encrypt($content, $config->application->cryptMethod, $config->application->cryptKey, OPENSSL_RAW_DATA, $iv)));
	}else{
		echo($content);
	}


//	$application->logger->commit();
} catch (Exception $e) {
    echo $e->getMessage(), '<br>';
    echo nl2br(htmlentities($e->getTraceAsString()));
	$application->logger->info($e->getMessage());
	$application->logger->info(nl2br(htmlentities($e->getTraceAsString())));
//    $application->logger->commit();
?>
	<!DOCTYPE html>
	<html>

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>PMAPLONA | 404 Error</title>

		<link href="/css/bootstrap/bootstrap.min.css" rel="stylesheet">
		<link href="font-awesome/css/font-awesome.css" rel="stylesheet">

		<link href="css/animate.css" rel="stylesheet">
		<link href="css/inspinia.css" rel="stylesheet">

	</head>

	<body class="gray-bg">


	<div class="middle-box text-center animated fadeInDown">
		<h1>404</h1>
		<h3 class="font-bold">Página no encontrada</h3>

		<div class="error-desc">
			Lo sentimos, no se ha encontrado la página solicitada!
			<form class="form-inline m-t" role="form">
				<a href="/" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
					Página principal </a>
			</form>
		</div>
	</div>
	</body>

	</html>

	<?php
}
