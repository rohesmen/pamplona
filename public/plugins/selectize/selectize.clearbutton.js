Selectize.define('clear_button', function(options) {
    /**
     * Escapes a string for use within HTML.
     *
     * @param {string} str
     * @returns {string}
     */
    var escape_html = function(str) {
        return (str + '')
            .replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/"/g, '&quot;');
    };

    options = $.extend({
        label     : '&times;',
        title     : 'Remove',
        className : 'clearAll',
        append    : true
    }, options);

    var self = this;

    this.setup = (function() {
        console.log("Called setup")
        var original = self.setup;
        return function() {
            // override the item rendering method to add the button to each
            original.apply(this, arguments);

            var sel_input = this.$wrapper.find('.selectize-input');

            if ( sel_input.length > 0 ) {
                var disabled = sel_input.hasClass('disabled');

                if ( disabled ) {
                    options.className = options.className + " disabled";
                }
            }

            var html = '<span class="' + options.className + '" tabindex="-1" title="' + escape_html(options.title) + '">' + options.label + '</span>';

            this.$wrapper.append(html);

            // add event listener
            this.$wrapper.on('click', '.' + options.className, function(e) {
                e.preventDefault();
                if (self.isLocked) return;
                self.clear();
            });

        };
    })();

});