/**
 * OpenLayers 3 Layer Switcher Control.
 * See [the examples](./examples) for usage.
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object} opt_options Control options, extends olx.control.ControlOptions adding:
 *                              **`tipLabel`** `String` - the button tooltip.
 */
ol.control.Summary = function(opt_options) {

    var open = false;

    var options = opt_options || {};

    var countActives = 0;

    var tipLabel = options.tipLabel ?
        options.tipLabel : 'Resumen';

    this.dirty = true;
    this.tipLabel = tipLabel;

    this.mapListeners = [];

    this.hiddenClassName = 'ol-unselectable ol-control summary-geoadmin';
    if (ol.control.Summary.isTouchDevice_()) {
        this.hiddenClassName += ' touch';
    }
    this.shownClassName = this.hiddenClassName + ' shown';

    var element = document.createElement('div');
    element.className = this.hiddenClassName;

    var button = document.createElement('button');
    button.setAttribute('title', tipLabel);
    //button.className = "btn btn-default";
    button.innerHTML = tipLabel;
    element.appendChild(button);

    this.panel = document.createElement('div');
    this.panel.className = 'panel panel-default';
    element.appendChild(this.panel);

    var this_ = this;

    button.onclick = function(e) {
        e = e || window.event;
        this_.showPanel();
        e.preventDefault();
    };

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};

ol.inherits(ol.control.Summary, ol.control.Control);

/**
 * Show the layer panel.
 */
ol.control.Summary.prototype.showPanel = function() {
    if (this.element.className != this.shownClassName) {
        this.element.className = this.shownClassName;
        this.open = true;
    }
};

/**
 * Hide the layer panel.
 */
ol.control.Summary.prototype.hidePanel = function() {
    if (this.element.className != this.hiddenClassName) {
        this.element.className = this.hiddenClassName;
        this.open = false;
    }
};
ol.control.Summary.prototype._removeLayers = function() {
    var content = this.content;
    if(content) {
        for (var i = 0; i < content.length; i++) {
            map.removeLayer(content[i].layer);
        }
    }
};
ol.control.Summary.prototype.clear = function() {
    summaryControl._removeLayers();
    this.criteria = null;
    this.content = null;
    this.typeSearch = null;
    this.query = null;

    this.dirty = true;
    this.renderPanel();
    this.hidePanel();
};
ol.control.Summary.prototype.setContent = function(type, query, criteria, content) {
    this._removeLayers();

    this.criteria = criteria;
    this.content = content;
    this.typeSearch = type;
    this.query = query;
    var row;
    var coordinates = [];

    for(var i = 0; i < content.length; i++){
        row = content[i];

        if(row.sw && row.ne) {
            coordinates.push(row.sw);
            coordinates.push(row.ne);
        }

        row.params = {
            request: "GetLegendGraphic",
            version: "1.0.0",
            format: "image/png",
            width: 20,
            height: 20,
            strict: false,
            layer: row.capa,
            style: row.estilo
        };
        row.layer = getImageLayer(row, true);
        row.layer.getSource().updateParams({CQL_FILTER: row.cqlfilter})
        map.addLayer(row.layer);
    }
    if(coordinates.length > 0){
        this.lastExtent = ol.extent.boundingExtent(coordinates);
        map.getView().fit(this.lastExtent, map.getSize());
    }

    this.dirty = true;
    this.renderPanel();

    this.showPanel();

    checkWithDragAndResizeComponent(".summary-geoadmin .panel");
};

ol.control.Summary.prototype.fitToLayers = function(all) {
    if(this.content){
        var row = null;
        var coordinates = [];
        for(var i =0; i < this.content.length; i++){
            row = this.content[i];
            if(row.layer.getVisible() && row.sw && row.ne) {
                coordinates.push(row.sw);
                coordinates.push(row.ne);
            }
        }
        if(coordinates.length > 0){
            this.lastExtent = ol.extent.boundingExtent(coordinates);
            map.getView().fit(this.lastExtent, map.getSize());
        }
    }
};

ol.control.Summary.prototype.getLayer = function(id) {
    if(this.content){
        for(var i =0; i < this.content.length; i++){
            if(this.content[i].id == id){
                return this.content[i];
            }
        }
    }
};

ol.control.Summary.prototype._createDetails = function(title, data, totales){
    var table = document.createElement('TABLE');
    var tableBody = document.createElement('TBODY');
    var tableHeader = document.createElement('THEAD');

    table.appendChild(tableHeader);
    table.appendChild(tableBody);
    var tr,td, tdubicados, tdtotal, row = null;
    var this_ = this;

    var totalUbicados = 0;
    var totalTotal = 0;
    var prefix = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5)+'-';

    tr = document.createElement('TR');
    tr.className="lbl-title";
    tableHeader.appendChild(tr);

    td = document.createElement('TD');
    td.colSpan = totales? "4":"6";
    td.innerHTML = "&nbsp;"+title;
    tr.appendChild(td);

    if(totales) {
        tdubicados = document.createElement('TD');
        tr.appendChild(tdubicados);

        tdtotal = document.createElement('TD');
        tr.appendChild(tdtotal);
    }

    for (var i=0; i<data.length; i++){
        row = data[i];
		if(row){
			table.id = prefix+'table';
			tr = document.createElement('TR');
			tableBody.appendChild(tr);

			td = document.createElement('TD');
			td.className="sum-switch";
			td.innerHTML = '<div class="panel-control">'+
				'<input id="'+prefix+i+'" class="toggle-switch" type="checkbox" checked="" onchange="summaryLayerChange(\''+table.id+'\',\''+prefix+i+'\','+row.id+')">'+
				'<label for="'+prefix+i+'"></label>'+
				'</div>';
			tr.appendChild(td);

			td = document.createElement('TD');
			td.className="sum-legend";
			td.innerHTML = '<img src="'+'/analysis/legend?'+jQuery.param(row.params)+'"/>';
			tr.appendChild(td);

			td = document.createElement('TD');
			td.className="sum-label";
			td.innerHTML = '<label for="'+prefix+i+'">'+row.etiqueta+'</label>';
			tr.appendChild(td);

			td = document.createElement('TD');
			td.className="sum-table";
			var iTD = document.createElement('i');
			iTD.className = 'fa fa-table';
			iTD.setAttribute("data-idcat", row.id);
			iTD.onclick = function(e){
				e.stopPropagation();
				var idcat = this.getAttribute("data-idcat");
				createTableSummary(idcat, this_.typeSearch, this_.query);
			};
			td.appendChild(iTD);
			tr.appendChild(td);

			td = document.createElement('TD');
			td.className="sum-badge-ubicados";
			td.innerHTML = '<span class="badge badge-info">'+row.ubicados+'</span>';
			tr.appendChild(td);

			td = document.createElement('TD');
			td.className="sum-badge-total";
			td.innerHTML = '<span class="badge badge-primary">'+row.total+'</span>';
			tr.appendChild(td);

			totalUbicados += row.ubicados;
			totalTotal += row.total;
		}
		
        
    }

    if(totales) {
        tdubicados.innerHTML='<span class="badge badge-info ubicados">'+totalUbicados+'</span>';
        tdtotal.innerHTML='<span class="badge badge-primary total">'+totalTotal+'</span>';
    }

    return [table, totalUbicados, totalTotal];
};

ol.control.Summary.prototype.renderPanel = function() {
    if(!this.dirty){
        return;
    }
    this.dirty = false;
    var this_ = this;
    this.countActives = 0;
    while(this.panel.firstChild) {
        this.panel.removeChild(this.panel.firstChild);
    }

    var panelheading = document.createElement('div');
    panelheading.className = "panel-heading";

    this.panel.appendChild(panelheading);
    var panelControl = document.createElement('div');
    panelControl.className = "panel-control";
    var btnClose = document.createElement('a');

    btnClose.href = "#";
    btnClose.className = "close icon-lg";
    var span = document.createElement('span');

    span.className = "fa fa-times";
    btnClose.onclick = function(e){
        e = e || window.event;
        this_.hidePanel();
        e.preventDefault();
    };
    btnClose.appendChild(span);
    panelControl.appendChild(btnClose);
    panelheading.appendChild(panelControl);

    var panelTitle = document.createElement('h3');
    panelTitle.className = "panel-title";
    panelTitle.innerHTML = this.tipLabel;
    panelheading.appendChild(panelTitle);

    var panelBody = document.createElement('div');
    panelBody.className = "panel-body";
    ol.control.Summary.enableTouchScroll_(panelBody);

    var panelFooter = document.createElement('div');
    panelFooter.className = "panel-footer";

    var criteria = this.criteria? '<span style="float: left;">Busqueda: <span id="criterio">'+this.criteria+'</span></span>' : '';
    panelFooter.innerHTML = criteria+'<span class="label label-info">Ubicados</span> <span class="label label-primary">Total</span>';

    this.panelBody = panelBody;
    this.resize();
    this.panel.appendChild(panelBody);
    this.panel.appendChild(panelFooter);

    var ul = document.createElement('ul');
    ul.className = "list-group";
    panelBody.appendChild(ul);


    var li,label = null;

    if(this.content){
        var row= this.content[0];
        var details = this._createDetails("Clientes",[row]);

        li = document.createElement("li");
        li.className = 'list-group-item';
        li.appendChild(details[0]);
        ul.appendChild(li);

        if(this.content.length > 1){
            details = this._createDetails("Apoyos",this.content.slice(1), true);
            li = document.createElement('li');
            li.className = 'list-group-item';
            // label = document.createElement("label");
            // label.className = "lbl-title";
            // label.innerHTML = '<span id="atotal" class="badge badge-primary">'+details[2]+'</span>'+
            //     '<span id="aubicados" class="badge badge-info">'+details[1]+'</span>'+
            //     '<span class="slabel">&nbsp;Apoyos</span>';
            //
            // li.appendChild(label);
            li.appendChild(details[0]);
            ul.appendChild(li);
        }

    }else{
        li = document.createElement('li');
        li.className = 'list-group-item';
        li.innerHTML = 'Sin información';
        ul.appendChild(li);
    }
};
/**
 * Re-draw the layer panel to represent the current state of the layers.
 */
ol.control.Summary.prototype.renderPanelOld = function() {
    if(!this.dirty){
        return;
    }
    this.dirty = false;
    var this_ = this;
    this.countActives = 0;
    while(this.panel.firstChild) {
        this.panel.removeChild(this.panel.firstChild);
    }

    var panelheading = document.createElement('div');
    panelheading.className = "panel-heading";

    this.panel.appendChild(panelheading);
    var panelControl = document.createElement('div');
    panelControl.className = "panel-control";
    var btnClose = document.createElement('a');
    //btnClose.type = "button";
    btnClose.href = "#";
    btnClose.className = "close icon-lg";
    var span = document.createElement('span');
    //span.innerHTML = "X";
    span.className = "fa fa-times";
    btnClose.onclick = function(e){
        e = e || window.event;
        this_.hidePanel();
        e.preventDefault();
    };
    btnClose.appendChild(span);
    //panelheading.appendChild(btnClose);
    panelControl.appendChild(btnClose);
    panelheading.appendChild(panelControl);

    var panelTitle = document.createElement('h3');
    panelTitle.className = "panel-title";
    panelTitle.innerHTML = this.tipLabel;
    panelheading.appendChild(panelTitle);

    //this.panel.style.maxHeight = "400px";


    var panelBody = document.createElement('div');
    panelBody.className = "panel-body";



    var panelFooter = document.createElement('div');
    panelFooter.className = "panel-footer";

    panelFooter.innerHTML = '<span class="label label-info">Ubicados</span> <span class="label label-primary">Total</span>';

    this.panel.appendChild(panelBody);
    this.panel.appendChild(panelFooter);
    // this.renderLayers_(this.getMap(), ul);

    var ul = document.createElement('ul');
    ul.className = "list-group";
    panelBody.appendChild(ul);

    var ulVotantes = document.createElement('ul');
    ulVotantes.className = "list-group";

    var ulApoyos = document.createElement('ul');
    ulApoyos.className = "list-group";

    var li,label = null;

    if(this.content){
        console.log(this.content);
        var row= this.content[0];

        label = document.createElement("label");

        li = document.createElement("li");
        li.className = 'list-group-item';
        li.innerHTML ='<span class="badge badge-primary">'+row.total+'</span>'+
            '<span class="badge badge-info">'+row.ubicados+'</span>'+
            '<span class="slabel">&nbsp;'+row.etiqueta+'<i class="fa fa-table"></i></span>';
        // '<span class="sbtn"></i></span>';
        ulVotantes.appendChild(li);

        li = document.createElement('li');
        li.className = 'list-group-item';

        label.innerHTML ='<span class="badge badge-primary">'+row.total+'</span>'+
            '<span class="badge badge-info">'+row.ubicados+'</span>'+
            '<span class="slabel">&nbsp;Votantes</span>';
        li.appendChild(label);
        li.appendChild(ulVotantes);
        ul.appendChild(li);

        for(var i=1; i < this.content.length; i++){
            row = this.content[i];

            li = document.createElement("li");
            li.className = 'list-group-item';
            li.innerHTML ='<span class="badge badge-primary">'+row.total+'</span>'+
                '<span class="badge badge-info">'+row.ubicados+'</span>'+
                '<div class="panel-control">'+
                '<input id="l'+i+'" class="toggle-switch" type="checkbox" checked="">'+
                '<label for="l'+i+'"></label>'+
                '</div>'+
                '<span class="slabel">&nbsp;'+row.etiqueta+'<i class="fa fa-table"></i></span>';
                // '<span class="sbtn"></i></span>';
            ulApoyos.appendChild(li);
        }
        if(ulApoyos.childElementCount > 0){
            li = document.createElement('li');
            li.className = 'list-group-item';
            label = document.createElement("label");
            label.innerHTML = '<span id="atotal" class="badge badge-primary">'+row.total+'</span>'+
                '<span id="aubicados" class="badge badge-info">'+row.ubicados+'</span>'+
                '<span class="slabel">&nbsp;Apoyos</span>';
            // li.appendChild();
            li.appendChild(label);
            li.appendChild(ulApoyos);
            ul.appendChild(li);
        }

    }else{

        li = document.createElement('li');
        li.className = 'list-group-item';
        li.innerHTML = 'Sin información';
        ul.appendChild(li);
    }

};

/**
 * Set the map instance the control is associated with.
 * @param {ol.Map} map The map instance.
 */
ol.control.Summary.prototype.setMap = function(map) {
    // Clean up listeners associated with the previous map
    for (var i = 0, key; i < this.mapListeners.length; i++) {
        this.getMap().unByKey(this.mapListeners[i]);
    }
    this.mapListeners.length = 0;
    // Wire up listeners etc. and store reference to new map
    ol.control.Control.prototype.setMap.call(this, map);
    if (map) {
        var this_ = this;
        //this.mapListeners.push(map.on('pointerdown', function() {
        //    this_.hidePanel();
        //}));
        this.renderPanel();
    }
};

ol.control.Summary.prototype.getSimbologia = function(contenedor, layer, estilo) {
    var source = layer.get("source");
    var params = source.getParams();

    var data = {
        request: "GetLegendGraphic",
        version: "1.0.0",
        format: "image/png",
        width: 20,
        height: 20,
        strict: false,
        layer: params.LAYERS,
        style: estilo
    }
    $.ajax({
        url: "/ine/legend",
        data: jQuery.param(data),
        //async: false,
        success: function (resp) {
            var imgSimple = "<img src='data:image/png;base64," + resp + "'/>";
            contenedor.innerHTML = imgSimple;
        }
    });
}
/**
 * Render all layers that are children of a group.
 * @private
 * @param {ol.layer.Base} lyr Layer to be rendered (should have a title property).
 * @param {Number} idx Position in parent group list.
 */
ol.control.Summary.prototype.renderLayer_ = function(lyr, idx) {

    var this_ = this;

    var li = document.createElement('li');

    var lyrTitle = lyr.get('title');
    var lyrId = ol.control.Summary.uuid();
    var lyrLayer = lyr.get

    var estilo = lyr.get("properties").style;
    var tematico = lyr.get("properties").tematico;
    var divl = document.createElement('div');
    divl.className = "img-name-legend";
    //var img = document.createElement('img');

    //divl.appendChild(img);

    var divr = document.createElement('div');
    divr.className = "name-legend";
    divr.innerHTML = lyrTitle;
    divr.title = lyrTitle;

    var divTexto = document.createElement('div');
    li.appendChild(divTexto);
    divTexto.appendChild(divl);
    divTexto.appendChild(divr);

    if(tematico){
        divTexto.style.cursor = "pointer";
        $(divTexto).off("click");
        $(divTexto).on("click", function(){
            var $contentLegend = $(this).next();
            $contentLegend.slideToggle();
            $(this).children().children().toggleClass("fa-caret-down");
            $(this).children().children().toggleClass("fa-caret-right");
        });
        var i = document.createElement('li');
        i.className = "fa fa-caret-down fa-2x";
        divl.appendChild(i);

        var divTematico = document.createElement('div');
        li.appendChild(divTematico);
        divTematico.style.paddingLeft = "25px";
        this.getSimbologia(divTematico, lyr, estilo);
    }
    else{
        this.getSimbologia(divl, lyr, estilo);
    }
    return li;
};

/**
 * Render all layers that are children of a group.
 * @private
 * @param {ol.layer.Group} lyr Group layer whos children will be rendered.
 * @param {Element} elm DOM element that children will be appended to.
 */
ol.control.Summary.prototype.renderLayers_ = function(lyr, elm) {
    var lyrs = lyr.getLayers().getArray().slice().reverse();
    for (var i = 0, l; i < lyrs.length; i++) {
        l = lyrs[i];
        if(l.getLayers){
            this.renderLayers_(l, elm);
        }
        else{
            if(l.get('title') && l.getVisible() && l.get("type") != "base") {
                this.countActives ++;
                elm.appendChild(this.renderLayer_(l, i));
            }
        }
    }
};

/**
 * Generate a UUID
 * @returns {String} UUID
 *
 * Adapted from http://stackoverflow.com/a/2117523/526860
 */
ol.control.Summary.uuid = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}

/**
 * @private
 * @desc Apply workaround to enable scrolling of overflowing content within an
 * element. Adapted from https://gist.github.com/chrismbarr/4107472
 */
ol.control.Summary.enableTouchScroll_ = function(elm) {
    var supportsPassive = false;
    try {
        document.addEventListener("test", null, { get passive() { supportsPassive = true }});
    } catch(e) {}
    if(ol.control.LayerSwitcher.isTouchDevice_()){
        var scrollStartPos = 0;
        elm.addEventListener("touchstart", function(event) {
            // event.preventDefault();
            scrollStartPos = this.scrollTop + event.touches[0].pageY;
        }, supportsPassive ? { passive: true } : false);
        elm.addEventListener("touchmove", function(event) {
            // event.preventDefault();
            this.scrollTop = scrollStartPos - event.touches[0].pageY;
        }, supportsPassive ? { passive: true } : false);
    }
};

/**
 * @private
 * @desc Determine if the current browser supports touch events. Adapted from
 * https://gist.github.com/chrismbarr/4107472
 */
ol.control.Summary.isTouchDevice_ = function() {
    try {
        document.createEvent("TouchEvent");
        return true;
    } catch(e) {
        return false;
    }
};

ol.control.Summary.prototype.resize = function(){
    this.panelBody.style.maxHeight = "300px";
    if(window.innerWidth <= 992 && window.innerWidth < 768){
        var navBarHeight = document.getElementById("navbar-container").clientHeight;
        var busquedaHeight = document.getElementById("resultados").nextElementSibling.clientHeight;

        this.panelBody.style.maxHeight = (window.innerHeight - navBarHeight - busquedaHeight - 130) + "px";
    }
};