/**
 * OpenLayers 3 Layer Switcher Control.
 * See [the examples](./examples) for usage.
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object} opt_options Control options, extends olx.control.ControlOptions adding:
 *                              **`tipLabel`** `String` - the button tooltip.
 */
ol.control.BaseLayerSwitcher = function(opt_options) {

    var options = opt_options || {};

    var tipLabel = options.tipLabel ?
      options.tipLabel : 'Legend';

    this.mapListeners = [];

    this.hiddenClassName = 'ol-unselectable ol-control base-layer-switcher';
    if (ol.control.LayerSwitcher.isTouchDevice_()) {
        this.hiddenClassName += ' touch';
    }

    var element = document.createElement('div');
    element.className = this.hiddenClassName;

    var this_ = this;

    //this.render_(element);

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};

ol.inherits(ol.control.BaseLayerSwitcher, ol.control.Control);


/**
 * Re-draw the layer panel to represent the current state of the layers.
 */
ol.control.BaseLayerSwitcher.prototype.render_ = function(elm) {

    this.ensureTopVisibleBaseLayerShown_();

    var elm = this.element;
    var lyrs = this.getMap().getLayers().getArray().slice().reverse();
    for (var i = 0, l; i < lyrs.length; i++) {
        l = lyrs[i];
        if(l.get("type") == undefined || !l.get("type") === "base")
            continue;
        if (l.get('title')) {
            elm.appendChild(this.renderLayer_(l, i));
        }
    }
};

/**
 * Render all layers that are children of a group.
 * @private
 * @param {ol.layer.Base} lyr Layer to be rendered (should have a title property).
 * @param {Number} idx Position in parent group list.
 */
ol.control.BaseLayerSwitcher.prototype.renderLayer_ = function(lyr, idx) {

    var this_ = this;

    var btn = document.createElement('a');
    btn.role = "button";
    btn.href = "#";
    btn.style.marginRight = "5px";
    btn.className = "btn btn-default";
    if(lyr.getVisible()){
        btn.className += " active";
    }

    var lyrTitle = lyr.get('title');
    var lyrId = ol.control.BaseLayerSwitcher.uuid();
    btn.innerHTML = lyrTitle;
    btn.id = lyrId;
    var this_ = this;

    btn.onclick = function(e){
        e.preventDefault();
        this_.setVisible_(lyr, e.target);
        var cointainer = btn.parentNode;
        var c = cointainer.childNodes;
        var i;
        for (i = 0; i < c.length; i++) {
            this_.removeClass(c[i], "active");
        };
        var maxZoom = 22;
        var minZoom = 10;
        var paramsView = {
            maxZoom: maxZoom,
            minZoom: minZoom
        };
        btn.className = btn.className.trim() + " active";
        if(lyr.get("title") == "Satélite" || lyr.get("title") == "Híbrido"){
            var actualZoom = this_.getMap().getView().getZoom();
            if(actualZoom > 17){
                var the_map = this_.getMap();
                maxZoom = 17;
                paramsView.maxZoom = maxZoom;
                the_map.getView().setZoom(maxZoom);
                //var view = new ol.View({});
                //the_map.setView(view);

            }
        }
    };

    return btn;

};

/**
 * Set the map instance the control is associated with.
 * @param {ol.Map} map The map instance.
 */
ol.control.BaseLayerSwitcher.prototype.setMap = function(map) {
    // Clean up listeners associated with the previous map
    for (var i = 0, key; i < this.mapListeners.length; i++) {
        this.getMap().unByKey(this.mapListeners[i]);
    }
    this.mapListeners.length = 0;
    // Wire up listeners etc. and store reference to new map
    ol.control.Control.prototype.setMap.call(this, map);
    if (map) {
        var this_ = this;
        this.render_();
    }
};

ol.control.BaseLayerSwitcher.prototype.hassClass = function(el, clazz) {
    return el.className.match(/\clazz\b/);
};

ol.control.BaseLayerSwitcher.prototype.removeClass = function(el, clazz) {
    el.className = el.className.replace(' ' + clazz,'').trim();
};

/**
 * Ensure only the top-most base layer is visible if more than one is visible.
 * @private
 */
ol.control.BaseLayerSwitcher.prototype.ensureTopVisibleBaseLayerShown_ = function() {
    var lastVisibleBaseLyr;

    var lyrs = this.getMap().getLayers().getArray().slice().reverse();
    for (var i = 0, l; i < lyrs.length; i++) {
        l = lyrs[i];
        if (l.get('type') === 'base' && l.getVisible()) {
            lastVisibleBaseLyr = l;
        }
    }
    if (lastVisibleBaseLyr) this.setVisible_(lastVisibleBaseLyr, true);
};

/**
 * Toggle the visible state of a layer.
 * Takes care of hiding other layers in the same exclusive group if the layer
 * is toggle to visible.
 * @private
 * @param {ol.layer.Base} The layer whos visibility will be toggled.
 */
ol.control.BaseLayerSwitcher.prototype.setVisible_ = function(lyr, visible) {
    var map = this.getMap();
    lyr.setVisible(visible);

    var lyrs = this.getMap().getLayers().getArray().slice().reverse();
    for (var i = 0, l; i < lyrs.length; i++) {
        l = lyrs[i];
        if (l != lyr && l.get('type') === 'base') {
            l.setVisible(false);
        }
    }
};

/**
 * Generate a UUID
 * @returns {String} UUID
 *
 * Adapted from http://stackoverflow.com/a/2117523/526860
 */
ol.control.BaseLayerSwitcher.uuid = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}

/**
 * @private
 * @desc Determine if the current browser supports touch events. Adapted from
 * https://gist.github.com/chrismbarr/4107472
 */
ol.control.BaseLayerSwitcher.isTouchDevice_ = function() {
    try {
        document.createEvent("TouchEvent");
        return true;
    } catch(e) {
        return false;
    }
};
