/**
 * OpenLayers 3 Layer Switcher Control.
 * See [the examples](./examples) for usage.
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object} opt_options Control options, extends olx.control.ControlOptions adding:
 *                              **`tipLabel`** `String` - the button tooltip.
 */
ol.control.Export = function(opt_options) {
    var options = opt_options || {};

    var tipLabel = options.tipLabel ?
        options.tipLabel : 'Información';
    this.activate = false;

    this.mapListeners = [];

    this.hiddenClassName = 'ol-unselectable ol-control layer-export';
    this.shownClassName = this.hiddenClassName + ' activate';

    var element = document.createElement('div');
    element.setAttribute('data-status', 'deactivate');
    element.className = this.hiddenClassName;

    var button = document.createElement('button');
    button.innerHTML = '<i class="fa fa-map-o"></i>';
    button.setAttribute('title', tipLabel);
    element.appendChild(button);

    var anchor = document.createElement('a');
    anchor.setAttribute("download","mapa.png");
    element.appendChild(anchor);

    var this_ = this;

    button.onclick = function(e) {
        e = e || window.event;
        var button = e.target;
        var status = this_.element.getAttribute("data-status");

        map.once('postcompose', function(event) {
            var canvas = event.context.canvas;
            anchor.href = canvas.toDataURL('image/png');
            anchor.click();
        });
        map.renderSync();

        
        e.preventDefault();
    };

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};

ol.inherits(ol.control.Export, ol.control.Control);