function addInfoControl(map, typeClick, displayButtonControl, order) {
    var InfoControl = function(opt_options) {
        var self = this;
        var options = opt_options || {};
        this.active = options.active || false;
        this.order = options.order;
        this.typeClick = (options.typeClick==null||options.typeClick==""?"singleclick":options.typeClick);
        this.displayButtonControl = (options.displayButtonControl===undefined?true:options.displayButtonControl);
        this.mapInfoMarker = null;

        this.hiddenClassName = 'ol-unselectable ol-control info-point there-can-be-only-one';
        this.shownClassName = this.hiddenClassName + ' activate';

        this.getLayersSelected = function (lyr) {
            var layers = [];
            var lyrs = lyr.getLayers().getArray().slice().reverse();
            for (var i = 0, l; i < lyrs.length; i++) {
                l = lyrs[i];
                if(l.getLayers){
                    var layersChildren = self.getLayersSelected(l);
                    layers = layers.concat(layersChildren);
                } else {
                    if (l.get("type") === "base" || l.get("ignorame") == true || l.get("title") == 'Capas de información')
                        continue;
                    if (l.getVisible() == true) {
                        layers.push(l);
                    }
                }
            }
            return layers;
        }
        /**
         * Print layers data.
         * @param {type} layer
         * @returns {undefined}
         */
        this.datasLayers = function (map) {
            if(!map.getLayers){
                return;
            }
            var layers = map.getLayers();
            //var index = indexOf(layers, layer);
            for(var index = 0; index < layers.getLength();index++) {
                var layer = layers.item(index);
                // console.log("*********** LAYER ************");
                console.log("index: "+index+" type: "+layer.get('type')+" title: "+layer.get('title'));
                // console.log("Childrens:");
                self.datasLayers(layer);
                //console.log("visible: "+layer.getVisible());
            }
        }
        this.putStartPlaceLayer = function (map,layer) {
            var layers = map.getLayers();
            var index = self.indexOf(layers, layer);
            layers.removeAt(index);
            layers.setAt(0,layer);
        }
        this.putEndPlaceLayer = function (map,layer) {
            var layers = map.getLayers();
            var index = self.indexOf(layers, layer);
            layers.removeAt(index);
            layers.insertAt((99/*layers.getLength()*/),layer);
        }
        /**
         * Raise a layer one place.
         * @param {type} layer
         * @returns {undefined}
         */
        this.raiseLayer = function (map,layer) {
            var layers = map.getLayers();
            var index = this.indexOf(layers, layer);
            if (index < layers.getLength() - 1) {
                var next = layers.item(index + 1);
                layers.removeAt(index);
                layers.setAt(index + 1, layer);
                layers.setAt(index, next);

                // Moves li element up
                /*var elem = $('ul.layerstack li[data-layerid="' + layer.get('name') + '"]');
                 elem.prev().before(elem);*/
            }
        }
        /**
         * Lowers a layer once place.
         * @param {type} layer
         * @returns {undefined}
         */
        this.lowerLayer = function (map,layer) {
            var layers = map.getLayers();
            var index = self.indexOf(layers, layer);
            if (index > 0) {
                var prev = layers.item(index - 1);
                layers.removeAt(index);
                layers.setAt(index - 1, layer);
                layers.setAt(index, prev);

                // Moves li element down
                /*var elem = $('ul.layerstack li[data-layerid="' + layer.get('name') + '"]');
                 elem.next().after(elem);*/
            }
        }
        /**
         * Returns the index of the layer within the collection.
         * @param {type} layers
         * @param {type} layer
         * @returns {Number}
         */
        this.indexOf = function (layers, layer) {
            var length = layers.getLength();
            for (var i = 0; i < length; i++) {
                if (layer === layers.item(i)) {
                    return i;
                }
            }
            return -1;
        }

        Array.prototype.contains = function(obj) {
            var i = this.length;
            while (i--) {
                if (this[i] == obj) {
                    return true;
                }
            }
            return false;
        }
        this.convertJsonInfoToHtml = function  (jsonText,entity) {
            var jsonObj = jsonText;//$.parseJSON(jsonText);
            var html = '<table class="table info-modal-table" border="0">';
            var propertiesExclude = ["geometry","geometry_name", "id", "type", "idgeo",
                "urlpreview","contenido","etiqueta_contenido","nombre_original","urlcontenido"];
            $.each(jsonObj, function(key, value) {
                if(key == 'properties') {
                    var htmlValue = (typeof value == "object") ? (self.convertJsonInfoToHtml(value,entity)) : (value);
                    html += htmlValue;
                } else if(!propertiesExclude.contains(key) || (entity == 'predios' && key == 'urlcontenido')) {
                    html += '<tr>';
                    html += '<td><b>' + key.toUpperCase() + '</b></td>';
                    if(key == 'urlcontenido' && value != null && value != '' && !jQuery.isEmptyObject(value)) {
                        // html += '<td><a href="#" class="link-info-catastral" attr-uri="'+encodeURIComponent(value)+'">Información</a></td>';
                        //html += '<td><a target="_blank" href="' + value + '">Información</a></td>';
                    } else {
                        var htmlValue = (typeof value == "object") ? (self.convertJsonInfoToHtml(value,entity)) : (value);
                        html += '<td>' + htmlValue + '</td>';
                    }
                    html += '</tr>';
                }
            });
            html += '</table>';
            return html;
        };
        this.convertJsonInfoToHtmlPanel = function  (jsonText,entity) {
            var jsonObj = jsonText;//$.parseJSON(jsonText);
            var html = '';
            var propertiesExclude = ["geometry","geometry_name", "id", "type", "idgeo",
                "urlpreview","contenido","etiqueta_contenido","nombre_original","urlcontenido"];
            $.each(jsonObj, function(key, value) {
                if(value == null) value = "";
                if(key == 'properties') {
                    html += '<div class="well info-modal-table">';
                    var htmlValue = (typeof value == "object") ? (self.convertJsonInfoToHtmlPanel(value,entity)) : (value);
                    html += htmlValue;
                    html += '</div>';
                } else if(!propertiesExclude.contains(key) || (entity == 'predios' && key == 'urlcontenido')) {
                    //html += '<tr>';
                    html += '<b>' + key.toUpperCase() + '</b>:&nbsp;';
                    if(key == 'urlcontenido' && value != null && value != '' && !jQuery.isEmptyObject(value)) {
                        // html += '<a href="#" class="link-info-catastral" attr-uri="'+encodeURIComponent(value)+'">Información</a>';
                    } else {
                        var htmlValue = (typeof value == "object") ? (self.convertJsonInfoToHtmlPanel(value,entity)) : (value);
                        html += '' + htmlValue + '';
                    }
                    html += '<br>';
                }
            });
            html += '';
            return html;
        };
        this.putInfoContent = function(contentBodyHTML, contentFooterHTML, emptyInfo) {
            $(".detail-feature-info .panel-body").html(contentBodyHTML);
            $(".detail-feature-info .panel-footer").html(contentFooterHTML);

            var w = window.innerWidth
                || document.documentElement.clientWidth
                || document.body.clientWidth;
            var h = window.innerHeight
                || document.documentElement.clientHeight
                || document.body.clientHeight;
            var heightBody = $('.detail-feature-info .panel-body').height();
            var maxHeightBody = (h - 400);
            var maxHeightContent = (h - 335);
            var maxWidthContent = (w - 150);
            $('#detail-feature-info').resizable({
                minHeight: 300,
                minWidth: 300,
                maxHeight: maxHeightContent + 'px',
                maxWidth: maxWidthContent + 'px',
                resize: function (event, ui) {
                    var newHeight = (ui.size.height - 95);
                    if (newHeight <= maxHeightContent) {
                        // console.log("ui.size.height  "+ui.size.height);
                        // console.log("body  "+newHeight);
                        //$('#detail-feature-info .geo-info-body').height(newHeight);
                        $('#detail-feature-info .panel-body').css("max-height",newHeight);
                        $('#detail-feature-info').height(ui.size.height);
                    }
                    var newWidth = (ui.size.width - 50);
                    if(newWidth <= maxWidthContent) {
                        $('#detail-feature-info .panel-body').height(newWidth);
                        $('#detail-feature-info').width(ui.size.width);
                    }
                }
            });
            $('#detail-feature-info .panel-body').css({
                'max-height': maxHeightBody + 'px'
            });
            // $(".link-info-catastral").off("click");
            // $(".link-info-catastral").on("click", function () {
            //     var urlInfo = urlGEO + "/predio?t=" + tokenGeomerida +
            //         "&u=" + $(this).attr("attr-uri");
            //     $("#info-catastral-frame-modal").attr("src", urlInfo);
            //     $("#info-catastral-modal").modal("show");
            // });
        };
        this.clearMarkerAndModals = function() {
            $("#detail-feature-info" ).remove();
            // $("#info-catastral-modal").remove();
            if (self.mapInfoMarker != null) {
                self.getMap().removeLayer(self.mapInfoMarker);
                self.mapInfoMarker = null;
            }
        };
        this.getInfoLayer = function (e){
            console.log("CLICK INFO");
            e.stopPropagation();
            self.clearMarkerAndModals();
            // INIT FLOATING INFO
            $('<div id="detail-feature-info" class="detail-feature-info">' +
                '<div class="panel panel-default ui-draggable">' +
                    '<div class="panel-heading">' +
                        '<div class="panel-control"><a href="#" class="close icon-lg"><span class="fa fa-times"></span></a></div>' +
                        '<img src="'+urlBase+'/img/marker-info-cian-32.png" style="margin-top: 6px;" alt="" class="pull-left modal-icon" height="32" width="32">'+
                        '<h3 class="panel-title ui-draggable-handle" id="myModalLabel">Información</h3>' +
                    '</div>' +
                // '<hr class="geo-info-line">' +
                    '<div class="panel-body" style="overflow-y: auto;">' +
                        '<div style="padding: 15px; text-align: center;">Cargando...</div>' +
                    '</div>' +
                // '<hr class="geo-info-line">' +
                    '<div class="panel-footer"></div>' +
                '</div>' +
                // '</div>'+
                // '<div id="info-catastral-modal" class="modal fade bs-info-modal-sm" style="z-index: 9999999999;" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="false">'+
                // '<div class="modal-dialog" role="document" >'+
                // '<div class="modal-content">'+
                // '<div class="modal-header">'+
                // '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                // '<img src="'+urlBase+'/img/Logo-Ayuntamiento.jpg" alt="" class="pull-left modal-icon" height="42">'+
                // '<h5 class="modal-title" id="myModalLabel" style="margin-top: 10px; margin-left: 40px; color: #0096C2;">Información</h5>'+
                // '</div>'+
                // '<div id="info-catastral-modal-body" class="modal-body" style="overflow-y: auto; margin: 0px; padding: 5px;">'+
                // '<iframe src="" id="info-catastral-frame-modal"'+
                // 'style="width:100%; min-height: 500px;">&lt;iframe&gt;</iframe>'+
                // '</div>'+
                // '</div>'+
                // '</div>'+
                '</div>').appendTo(document.body);
            $("#detail-feature-info").draggable({
                //handle: ".modal-header",
                containment: "#analysis-map"
            });
            $("#detail-feature-info .close").on("click", function() {
                if(self.mapInfoMarker != null){
                    self.getMap().removeLayer(self.mapInfoMarker);
                    self.mapInfoMarker = null;
                }
                $("#detail-feature-info" ).remove();
                // $( "#info-catastral-modal" ).remove();
            });
            var infoEmptyHTML = '<div style="padding: 15px; text-align: center;">Sin información</div>';
            var infoEmpty = false;
            // END INIT FLOATING INFO
            // PUT MARKER
            var iconStyle = new ol.style.Style({
                image: new ol.style.Icon(({
                    //anchor: [0.5, 0.85], // 42px
                    anchor: [0.5, 0.98], // 32px
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'fraction',
                    src: '' + urlBase + '/img/marker-info-cian-32.png',
                    ignorame: true
                })),
            });
            var circleStyle = new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 5,
                    snapToPixel: false,
                    //fill: new ol.style.Fill({color: 'black', opacity: 0}),
                    stroke: new ol.style.Stroke({
                        color: 'black', width: 2
                    })
                })
            });
            var coordinates = e.coordinate;
            var point_feature = new ol.Feature({
                geometry: new ol.geom.Point(coordinates),
                ignorame: true
            });
            if(self.mapInfoMarker){
                self.getMap().removeLayer(self.mapInfoMarker);
            }

            self.mapInfoMarker = new ol.layer.Vector({
                title: 'marker-info',
                source: new ol.source.Vector({
                    features: [point_feature],
                    ignorame: true
                }),
                style: iconStyle,
                ignorame: true
            });
            self.getMap().addLayer(self.mapInfoMarker);
            //self.datasLayers(self.getMap());
            //console.log("MOVE LAYER");
            self.putEndPlaceLayer(self.getMap(), self.mapInfoMarker);
            //self.datasLayers(self.getMap());
            // END PUT MARKER


            var layers = self.getLayersSelected(self.getMap());//[];

            //layers.push(self.layerColonias);
            //layers.push(self.layerLocalidades);

            // if(layers.length == 0) {
            //     infoEmpty = true;
            // } else {
                var layersParams = '';
                var stylesParams = '';
                var source = this.wmsSourceColonias;
                for (var i = 0; i < layers.length; i++) {
                    if ($.isFunction(layers[i].get("source").getParams)) {
                        layersParams += layers[i].get("source").getParams().LAYERS;
                        var st = layers[i].get("source").getParams().STYLES;
						stylesParams += st == null ? "" : st;
                        if (i < (layers.length - 1)) {
                            layersParams += ",";
                            stylesParams += ",";
                        }
                        source = layers[i].get("source");
                    }
                }
                if (source == null) {
                    infoEmpty = true;
                } else {
                    var viewResolution = /** @type {number} */ (self.getMap().getView().getResolution());
                    var url = source.getGetFeatureInfoUrl(
                        coordinates, viewResolution, 'EPSG:3857',
                        {'INFO_FORMAT': 'application/json'});

                    if (url) {
                        var surl = url.split("?");
                        var queryLayersParams = '&TOKEN=' + TOKEN + '&QUERY_LAYERS=' + layersParams;
                        layersParams = '&LAYERS=' + layersParams;
                        stylesParams = '&STYLES=' + stylesParams + '&TILED='
                        var initUrl = surl[1].split("&QUERY_LAYERS=");
                        var lastUrl = surl[1].split("&TILED=");
                        var urlEnd = initUrl[0] + queryLayersParams + layersParams + stylesParams + lastUrl[1];
                        url = /*"/map/info?"*/ urlGeoserver + "?" + urlEnd/*surl[1]*/ + "&FEATURE_COUNT=100";
						$.ajax({
                            url: url,
                            beforeSend: function () {
                                /*$("#info-container").fadeOut("slow");
                                 $("#info-container-info").fadeIn("slow");
                                 $("#info-container-info").html('<div style="text-align: center;">' +
                                 '<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
                                 '</div>');*/
                            },
                            success: function (resp) {
                                console.log("INFO");
                                console.log(resp);
                                if (resp && resp.features && resp.features.length > 0) {
                                    var infoBodyHTML = "<br>";
                                    var infoFooterHTML = "";
                                    for (var i = 0; i < resp.features.length; i++) {
                                        var feature = resp.features[i];
                                        var entity = feature.id.split(".")[0];
                                        var featureHtml = self.convertJsonInfoToHtmlPanel(feature, entity);
                                        infoBodyHTML += featureHtml;
                                        if (i < (resp.features.length - 1)) {
                                            infoBodyHTML += '';//'<hr>';
                                        }
                                    }
                                    infoFooterHTML = resp.features.length + " elemento" + (resp.features.length > 1 ? "s" : "");

                                    self.putInfoContent(infoBodyHTML,infoFooterHTML,false);

                                } else {
                                    //infoEmpty = true;
                                    //$('#detail-feature-info .geo-info-body').height(50);
                                    self.putInfoContent(infoEmptyHTML,"",true);
                                }

                                checkWithDragAndResizeComponent("#detail-feature-info", "#detail-feature-info");
                                // MODAL INFO
                                /*$('<div id="info-container-info"></div>'+
                                 '<div id="info-modal" class="modal fade bs-info-modal-sm" style="z-index: 9999999999;" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="false">'+
                                 //'<div id="info-modal" class="modal fade bs-info-modal-sm" style="z-index: 9999999999;" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="false">'+
                                 '<div class="modal-dialog modal-sm" role="document">'+
                                 '<div class="modal-content">'+
                                 '<div class="modal-header">'+
                                 '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                                 '<img src="'+urlBase+'/img/marker-info-2.png" alt="" class="pull-left modal-icon" height="42" width="42">'+
                                 '<h5 class="modal-title" id="myModalLabel">Información</h5>'+
                                 '</div>'+
                                 '<div id="info-modal-body" class="modal-body" style="overflow-y: auto;">'+
                                 '...'+
                                 '</div>'+
                                 '<div id="info-modal-footer" class="modal-footer">'+
                                 '</div>'+
                                 '</div>'+
                                 '</div>'+
                                 '</div>'+
                                 '<div id="info-catastral-modal" class="modal fade bs-info-modal-sm" style="z-index: 9999999999;" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="false">'+
                                 '<div class="modal-dialog" role="document" >'+
                                 '<div class="modal-content">'+
                                 '<div class="modal-header">'+
                                 '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                                 '<img src="'+urlBase+'/img/LogoAyuntaGris.png" alt="" class="pull-left modal-icon" height="42" width="32">'+
                                 '<h5 class="modal-title" id="myModalLabel">Información</h5>'+
                                 '</div>'+
                                 '<div id="info-catastral-modal-body" class="modal-body" style="overflow-y: auto; max-height:510px;">'+
                                 '<iframe src="" id="info-catastral-frame-modal"'+
                                 'style="width:100%; min-height: 500px;">&lt;iframe&gt;</iframe>'+
                                 '</div>'+
                                 '<div id="info-catastral-modal-footer" class="modal-footer">'+
                                 '</div>'+
                                 '</div>'+
                                 '</div>'+
                                 '</div>').appendTo(document.body);

                                 $("#info-container-info").html("");
                                 $("#info-modal-body").html("");
                                 $("#info-modal-footer").html("");
                                 if(resp && resp.features && resp.features.length > 0){
                                 var infoBodyHTML = "";
                                 for(var i= 0; i < resp.features.length; i++) {
                                 var feature = resp.features[i];
                                 var entity = feature.id.split(".")[0];
                                 var featureHtml = self.convertJsonInfoToHtml(feature,entity);
                                 infoBodyHTML += featureHtml;
                                 if(i < (resp.features.length-1)) {
                                 infoBodyHTML += '<hr>';
                                 }
                                 }
                                 $("#info-modal-body").html(infoBodyHTML);
                                 $("#info-modal-footer").html(resp.features.length + " elemento"+(resp.features.length>1?"s":""));
                                 $("#info-modal").modal("show");

                                 var w = window.innerWidth
                                 || document.documentElement.clientWidth
                                 || document.body.clientWidth;

                                 var h = window.innerHeight
                                 || document.documentElement.clientHeight
                                 || document.body.clientHeight;
                                 var heightBody = $('#info-modal .modal-body').height();
                                 var maxHeightBody = (h-200);//(heightBody<(h-200)?heightBody:(h-200));
                                 var mexHeightContent = (h-150);//(heightBody<(h-200)?(heightBody+135):(h-150));
                                 $('#info-modal .modal-content').resizable({
                                 //alsoResize: "#info-modal-body",
                                 minHeight: 300,
                                 minWidth: 300,
                                 maxHeight: mexHeightContent+'px',
                                 maxWidth: (w-150)+'px',
                                 resize: function( event, ui ) {
                                 var newHeight = (ui.size.height-135);
                                 if(newHeight <= maxHeightBody) {
                                 $('#info-modal .modal-body').height(newHeight);
                                 }
                                 }
                                 });
                                 $("#info-modal").draggable({
                                 handle: ".modal-header"
                                 });
                                 $('#info-modal').on('shown.bs.modal', function() {
                                 $(".link-info-catastral").off("click");
                                 $(".link-info-catastral").on("click", function() {
                                 //var urlInfo = "https://geoportal.merida.gob.mx/predio?t="+tokenGeomerida+
                                 var urlInfo = urlGEO+"/predio?t="+tokenGeomerida+
                                 "&u="+$(this).attr("attr-uri");
                                 $("#info-catastral-frame-modal").attr("src",urlInfo);
                                 $("#info-catastral-modal").modal("show");
                                 });
                                 $(this).find('.modal-body').css({
                                 'max-height': maxHeightBody+'px'
                                 });
                                 });
                                 $('#info-modal').on('hidden.bs.modal', function (e) {
                                 if(self.mapInfoMarker != null){
                                 self.getMap().removeLayer(self.mapInfoMarker);
                                 self.mapInfoMarker = null;
                                 }
                                 $( "#info-container-info" ).remove();
                                 $( "#info-modal" ).remove();
                                 $( "#info-catastral-modal" ).remove();
                                 })
                                 }
                                 else{
                                 $("#info-modal-body").html("Sin información");
                                 $("#info-modal").modal("show");
                                 }*/
                            },
                            error: function (xhr, status, error) {
                                var textError = "";
                                var tipoError = "";
                                if (xhr.status == 401) {
                                    textError = "No cuenta con el permiso.";
                                    tipoError = "error";
                                }
                                else if (xhr.status == 404) {
                                    textError = "No se encontro información para el punto seleccionado.";
                                    tipoError = "warning";
                                }
                                else {
                                    textError = "Ocurrió un error al obtener la información";
                                    tipoError = "error";
                                }
                                console.log(textError);
                            }
                        });
                    }
                }
            // };
            if(infoEmpty) {
                self.putInfoContent(infoEmptyHTML,"",true);
                checkWithDragAndResizeComponent();
            }
        };

        this.wmsSourceColonias = new ol.source.ImageWMS({
            url: urlGeoserver,
            params: {
                'LAYERS': "geotecnologia:colonias_view",
                'STYLES': '',
                'TILED': false
            },
            serverType: 'geoserver',
            ignorame: true
        });

        this.layerColonias = new ol.layer.Image({
            title: 'Colonias',
            type: 'base',
            visible: false,
            ignorame: true,
            source: this.wmsSourceColonias
        });

        this.wmsSourceLocalidades = new ol.source.ImageWMS({
            url: urlGeoserver,
            params: {
                'LAYERS': "geotecnologia:localidades_view",
                'STYLES': '',
                'TILED': false
            },
            serverType: 'geoserver',
            ignorame: true
        });

        this.layerLocalidades = new ol.layer.Image({
            title: 'Localidades',
            type: 'base',
            visible: false,
            ignorame: true,
            source: this.wmsSourceLocalidades
        });

        //this.getMap().on(this.typeClick, this.getInfoLayer);

        var handleControl = function(e) {
            e.preventDefault();
            if (!self.active) {
                self.activate();
            } else {
                self.inactivate();
            }
        };
        this.activate = function() {
            if (!self.active) {
                self.element.setAttribute('data-status', 'activate');
                $(".there-can-be-only-one").not(self.element).trigger("geo:there-can-be-only-one");
                if (self.element.className != self.shownClassName) {
                    self.element.className = self.shownClassName;
                }

                //self.getMap().un(self.typeClick, self.getInfoLayer);
                self.getMap().on(self.typeClick, self.getInfoLayer);
                self.active = true;
                // self.anchor.style.backgroundColor = "#0A4686";
            }
        };
        this.inactivate = function () {
            self.element.setAttribute('data-status', 'deactivate');
            if (self.element.className != self.hiddenClassName) {
                self.element.className = self.hiddenClassName;
            }

            //if (this.active) {
                self.getMap().un(self.typeClick, self.getInfoLayer);
                self.active = false;
                self.clearMarkerAndModals();
                // self.anchor.style.backgroundColor = "rgba(0,60,136,.5)";
            //}
        };

        this.anchor = document.createElement('button');
        this.anchor.type = 'button';
        this.anchor.title = 'Información';
        this.anchor.innerHTML = '<i class="fa fa-info"></i>';
        if(this.displayButtonControl) {
            this.anchor.addEventListener('click', handleControl, false);
            this.anchor.addEventListener('touchstart', handleControl, false);
        } else {
            this.anchor.style.display = 'none';
        }

        var element = document.createElement('div');
        element.className = this.hiddenClassName;
        // element.style.right = '8px';
        // element.style.top = (2.5 + ((self.order - 1) * 40)) + 'px';
        element.appendChild(this.anchor);

    onlyOne = function(){
        var status = this.getAttribute("data-status");
        if(status == "activate"){
            self.inactivate();
        }
    };

    $(this.anchor.parentNode).on('geo:there-can-be-only-one', onlyOne);

        ol.control.Control.call(this, {
            element: element,
            target: options.target
        });
    };
    ol.inherits(InfoControl, ol.control.Control);

    var infoControl = new InfoControl({
        order: order,
        typeClick: typeClick,
        displayButtonControl: displayButtonControl
    });
    map.addControl(infoControl);

    /*console.log("Interacciones");
    console.log(map.getInteractions());
    console.log("Controles");
    console.log(map.getControls());*/

    return infoControl;
}

