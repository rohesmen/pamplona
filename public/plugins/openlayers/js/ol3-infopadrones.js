/**
 * OpenLayers 3 Layer Switcher Control.
 * See [the examples](./examples) for usage.
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object} opt_options Control options, extends olx.control.ControlOptions adding:
 *                              **`tipLabel`** `String` - the button tooltip.
 */
ol.control.InfoPadrones = function(opt_options) {
    var options = opt_options || {};

    var tipLabel = options.tipLabel ?
        options.tipLabel : 'Información';

    this.mapListeners = [];

    this.hiddenClassName = 'ol-unselectable ol-control layer-info-padrones';
    this.shownClassName = this.hiddenClassName + ' activate';

    var element = document.createElement('div');
    element.setAttribute('data-status', 'deactivate');
    element.className = this.hiddenClassName;

    var button = document.createElement('button');
    button.setAttribute('title', tipLabel);
    element.appendChild(button);

    var this_ = this;

    button.onclick = function(e) {
        e = e || window.event;
        var button = e.target;
        var status = this_.element.getAttribute("data-status");
        if(status == "deactivate"){
            this_._activteInfo();
        }
        else{
            this_._deactivateInfo();
        }
        e.preventDefault();
    };

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};

ol.inherits(ol.control.InfoPadrones, ol.control.Control);

ol.control.InfoPadrones.prototype._mouseEvent = function(e){
    //var map = this._map;
    console.log(e);
}
/**
 * Activar herramienta de info.
 */
ol.control.InfoPadrones.prototype._activteInfo = function() {
    this.element.setAttribute('data-status', 'activate');
    if (this.element.className != this.shownClassName) {
        this.element.className = this.shownClassName;
    }
    this._mapClickEvent = this.getMap().on("click", getInfoLayerPadrones);
    //this._map = this.getMap();
    //var elMap = this.getMap().getTargetElement();
    //$(elMap).on("mousemove", this._mouseEvent);
};

/**
 * desactivar herramienta de info.
 */
ol.control.InfoPadrones.prototype._deactivateInfo = function() {
    this.element.setAttribute('data-status', 'deactivate');
    if (this.element.className != this.hiddenClassName) {
        this.element.className = this.hiddenClassName;
    }

    closeInfoLayerPadrones();
    this.getMap().unByKey(this._mapClickEvent);
    //var elMap = this.getMap().getTargetElement();
    //$(elMap).off("mousemove", this._mouseEvent);
};