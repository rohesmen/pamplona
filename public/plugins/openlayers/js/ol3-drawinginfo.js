function addDrawingInfoControl(map, order) {
    var DrawingInfoControl = function(opt_options) {
        var options = opt_options || {};
        this.initializedControl = false;
        this.active = options.active || false;
        this.map = options.map;
        this.changingPosition = false;
        this.viewselect = null;
        var this_ = this;

        this.mapInfoMarker = null;
        this.zona = false;

        this.featuresSelection = new ol.Collection();
        this.sourceSelection = new ol.source.Vector({features: this_.featuresSelection});
        this.vectorDrawSelection = new ol.layer.Vector({
            source: this_.source,
            ignorame: true,
        });

        this.features = new ol.Collection();
        this.source = new ol.source.Vector({features: this_.features});
        this.vectorDraw = new ol.layer.Vector({
            source: this_.source,
            ignorame: true,
            style: new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                }),
                stroke: new ol.style.Stroke({
                    color: '#ffcc33',
                    width: 2
                }),
                image: new ol.style.Circle({
                    radius: 7,
                    fill: new ol.style.Fill({
                        color: '#ffcc33'
                    })
                })
            })
        });
        // this.map.addLayer(vector);
        this.vectorDraw.set('selectable-drawinfo', true);
        this.map.addLayer(this.vectorDraw);

        this.containerDatos = $("<div id='map-dash-clientes' style='display: none; position: absolute; top: 0px; left: 0px; height: 380px; width: 450px;'></div>");
        this.containerDatos.html('<div class="panel panel-default" style="margin-bottom: 0; height: 100%"> ' +
            '<div class="panel-heading" style="height: 35px;">' +
            '<div class="panel-control">' +
            '<a href="#" class="close icon-lg" id="resultados-streetview-close" style="margin-top: 6px;"><span class="fa fa-times"></span></a>' +
            '</div> ' +
            '<h3 class="panel-title" style="height: 35px;">Información</h3>' +
            '</div> ' +
            '<div class="panel-body" style="height: calc(100% - 35px)" id="map-container-dash"></div>' +
            '</div> ');
        $("#" + map.getTarget()).parent().append(this.containerDatos);
        this.containerDatos.draggable({
            handle: ".panel-heading ",
            containment: "#analysis-map"
        });

        this.containerDatos.resizable({
            containment: "#analysis-map",
            minHeight: 380,
            minWidth: 450
        });

        this.containerDatos.find(".close").on("click", function () {
            this_.containerDatos.hide();
            this_.viewselect.find(".show-analisys-area").show();
        });

        var Modify = {
            init: function() {
                console.log("Modify INIT");
                this.select = new ol.interaction.Select({
                    // style: [styleStreetPointSelected],
                    layers: function(layer) {
                        return layer.get('selectable-street') == true;
                    },
                });
                this_.map.addInteraction(this.select);

                this.modify = new ol.interaction.Modify({
                    features: this.select.getFeatures()
                });
                this_.map.addInteraction(this.modify);

                this.setEvents();
            },
            remove: function() {
                this_.map.removeInteraction(this.modify);
                this_.map.removeInteraction(this.select);
            },
            setEvents: function() {
                var selectedFeatures = this.select.getFeatures();

                this.select.unByKey('change:active');
                this.select.on('change:active', function() {
                    console.log('change:active');
                    /*selectedFeatures.forEach(selectedFeatures.remove, selectedFeatures);
                    //this_.closeView();
                    if(!this_.changingPosition) {
                        //this_.closeView();
                        setTimeout(function() {
                            this_.inactivate();
                        },100);
                    }*/
                });
                selectedFeatures.unByKey('remove');
                selectedFeatures.on('remove', function(event) {
                    console.log('remove');
                    if(!this_.changingPosition) {
                        //this_.closeView();
                        setTimeout(function() {
                            this_.inactivate();
                        },100);
                    } else {
                        this_.changingPosition = false;
                    }
                    event.stopPropagation();
                });
                this.modify.unByKey('modifyend');
                this.modify.on('modifyend', function (evt) {
                    evt.features.forEach(function (feature) {
                        this_.openView(feature);
                    });
                    evt.stopPropagation();
                })
            },
            setActive: function(active) {
                this.modify.setActive(active);
            }
        };
        var DrawPolygon = {
            init: function() {
                console.log("Draw INIT");
                this_.map.addInteraction(this.Point);
                this.Point.setActive(false);
                this.setEvents();
            },
            remove: function() {
                this_.map.removeInteraction(this.Point);
            },
            Point: new ol.interaction.Draw({
                source: this_.source,
                type: /** @type {ol.geom.GeometryType} */ ('Polygon')
            }),
            setEvents: function() {
                this.Point.unByKey('drawend');
                this.Point.on('drawend', function(e){
                    console.log('drawend');
                    this_.btnLimpiarAnalizar.parent().hide();
                    this_.btnAnalizar.parent().show();
                    this_.btnAnalizar.prop("disabled", false);
                    // this_.openView(e.feature);
                    // DrawPolygon.setActive(false);
                    // Modify.setActive(true);
                    e.stopPropagation();
                });
                this.Point.on('drawstart', function(){
                    this_.features.clear();
                    console.log('drawstart');
                    this_.btnAnalizar.prop("disabled", true);
                    // this_.openView(e.feature);
                    // DrawPBuffer.setActive(false);
                    // Modify.setActive(true);
                });
            },
            getActive: function() {
                return this.Point.getActive();
            },
            setActive: function(active) {
                this.Point.setActive(active);
            }
        };

        var DrawPBuffer = {
            init: function() {
                console.log("Draw INIT");
                this_.map.addInteraction(this.Point);
                this.Point.setActive(false);
                this.setEvents();
            },
            remove: function() {
                this_.map.removeInteraction(this.Point);
            },
            Point: new ol.interaction.Draw({
                source: this_.source,
                type: /** @type {ol.geom.GeometryType} */ ('Circle')
            }),
            setEvents: function() {
                this.Point.unByKey('drawend');
                this.Point.on('drawend', function(e){
                    console.log(e.features);
                    console.log('drawend');
                    this_.btnLimpiarAnalizar.parent().hide();
                    this_.btnAnalizar.parent().show();
                    this_.btnAnalizar.prop("disabled", false);
                    // this_.openView(e.feature);
                    // DrawPBuffer.setActive(false);
                    // Modify.setActive(true);
                    // this_.source.addFeature(e.feature);


                    var format = new ol.format.WKT(), data;

                    //this_.sourcerDraw.getSource().addFeatures(e.feature);
                    var auxPolygon = ol.geom.Polygon.fromCircle(e.feature.getGeometry());
                    try {
                        // convert the data of the vector_layer into the chosen format
                        // auxPolygon.transform('EPSG:3857', 'EPSG:32616');
                        data = format.writeGeometry(auxPolygon);

                        var CircleFeature = new ol.Feature(auxPolygon);
                        this_.source.addFeature(CircleFeature);

                        // busquedaGlobal('GEOMETRIA', data);
                        // console.log(data);
                    } catch (e) {
                        console.log("Error: " + e.message);
                        return;
                    }

                    e.stopPropagation();
                });

                this.Point.on('drawstart', function(){
                    this_.features.clear();
                    console.log('drawstart');
                    this_.btnAnalizar.prop("disabled", true);
                    // this_.openView(e.feature);
                    // DrawPBuffer.setActive(false);
                    // Modify.setActive(true);
                });
            },
            getActive: function() {
                return this.Point.getActive();
            },
            setActive: function(active) {
                this.Point.setActive(active);
            }
        };

        this.getUUID = function () {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
                return v.toString(16);
            });
        }

        this.openView = function(feature) {
            if(!this_.viewselect){
                this_.close = $('<a href="#" class="close icon-lg"><span class="fa fa-times"></span></a>');
                this_.viewselect = $('<div id="' + this_.getUUID() + '" class="modal-drawinginfo">' +
                    '<div class="panel panel-default ui-draggable">' +
                    '<div class="panel-heading">' +
                    '<div class="panel-control"></div>' +
                    '<img src="'+urlBase+'/img/marker-info-cian-32.png" style="margin-top: 6px;" alt="" class="pull-left modal-icon" height="32" width="32">'+
                    '<h3 class="panel-title ui-draggable-handle" id="myModalLabel">Análisis</h3>' +
                    '</div>' +
                    // '<hr class="geo-info-line">' +
                    '<div class="panel-body">' +



                        '<div class="row" style="margin-top: 10px;">' +
                            '<div class="col-sm-4">' +
                                '<button class="btn btn-primary btn-block btn-info-buffer" title="Buffer">' +
                                '<img src="img/circlei-on.png">' +
                                '</button>' +
                            '</div> ' +
                            '<div class="col-sm-4">' +
                                '<button class="btn btn-primary btn-block btn-info-polygon" title="Dibujar polígono">' +
                                '<img src="img/perimetro-on.png">' +
                                '</button>' +
                            '</div> ' +
                            '<div class="col-sm-4">' +
                                '<button class="btn btn-primary btn-block btn-info-zona" style="height: 38px" title="Zona marginada">Zona</button>' +
                            '</div> ' +
                        '</div> ' +

                        '<div class="row" style="margin-top: 10px;">' +
                            '<div class="col-sm-12">' +
                                '<select class="form-control select-capa-analisys">' +
                                    '<option value="clientes">Clientes</option>' +
                                    '<option value="comercios">Comercios</option>' +
                                    '<option value="predios">Predios Municipio</option>' +
                                '</select> ' +
                            '</div> ' +
                        '</div> ' +

                    '<div class="row zonas-marginadas" style="margin-top: 10px; display: none;">' +
                    '<div class="col-sm-12">' +
                    '<select class="form-control select-capa-zona-analisys">' +
                    '<option value="">Seleccionar</option>' +
                    '<option value="1">Ruta 1</option>' +
                    '<option value="2">Ruta 2</option>' +
                    '<option value="3">Ruta 3</option>' +
                    '<option value="4">Ruta 4</option>' +
                    '<option value="5">Ruta 5</option>' +
                    '<option value="6">Ruta 6</option>' +
                    '<option value="7">Ruta 7</option>' +
                    '<option value="8">Ruta 8</option>' +
                    '<option value="9">Ruta 9</option>' +
                    '<option value="10">Ruta 10</option>' +
                    '<option value="11">Ruta 11</option>' +
                    '<option value="12">Ruta 12</option>' +
                    '<option value="13">Ruta 13</option>' +
                    '<option value="14">Ruta 14</option>' +
                    '<option value="15">Ruta 15</option>' +
                    '</select> ' +
                    '</div> ' +
                    '</div> ' +


                        '<div class="row" style="margin-top: 10px;">' +
                            '<div class="col-sm-12 analisys-area">' +
                            '<button class="btn btn-primary btn-block btn-analisys-area" disabled>Analizar</button>' +
                            '</div> ' +
                            '<div class="col-sm-12 clear-analisys-area" style="display: none;">' +
                            '<button class="btn btn-danger btn-block btn-clear-analisys-area">Limpiar</button>' +
                            '</div> ' +
                            '<div class="col-sm-12 show-analisys-area" style="display: none; margin-top: 5px;">' +
                            '<button class="btn btn-primary btn-block btn-show-analisys-area">Mostrar análisis</button>' +
                            '</div> ' +
                        '</div> ' +


                    '</div>' +
                    '</div>' +
                    '</div>').appendTo(document.body);
                this_.viewselect.find(".panel-control").append(this_.close);
                this_.close.on("click", this_.deactivate);
                this_.btnAnalizar = this_.viewselect.find(".btn-analisys-area");
                this_.btnAnalizar.on("click", this_._analizar);
                this_.btnLimpiarAnalizar = this_.viewselect.find(".btn-clear-analisys-area");
                this_.btnLimpiarAnalizar.on("click", this_.limpiar_analisis);
                this_.btnCoultarAnalisis = this_.viewselect.find(".btn-show-analisys-area");
                this_.btnCoultarAnalisis.on("click", this_.showanalisiarea);
                this_.viewselect.find(".btn-info-buffer").on("click", this_.activeBuffer);
                this_.viewselect.find(".btn-info-polygon").on("click", this_.activePlygon);
                this_.viewselect.find(".btn-info-zona").on("click", this_.activeZona);
                this_.viewselect.find(".select-zona-marginada").on("change", this_.seletcZona);
                this_.viewselect.find(".select-capa-analisys").on("change", this_.selectcapaanalisys);
                this_.viewselect.find(".select-capa-zona-analisys").on("change", this_.selectcapazonaanalisys);

                // this_.viewselect.find("a.close")[0].addEventListener('click', this_.closeView(), false);
            }
            this_.viewselect.show();
        };
        this.closeView = function() {
            // this_.deactivate();
            this_.viewselect.hide();
        };

        var handleControl = function(e) {
            e.preventDefault();
            if (!this_.active) {
                this_.activate();
            } else {
                this_.deactivate();
            }
        };
        this.activate = function() {
            //     Modify.init();
            //     Draw.init();
            //     this_.initializedControl = true;
            // Draw.setActive(true);
            // Modify.setActive(false);

            this_.active = true;
            this_.anchor.parentNode.className = "ol-control drawinfo-geometry ol-unselectable there-can-be-only-one activate";
            $(".there-can-be-only-one").not(this_.anchor.parentNode).trigger("geo:there-can-be-only-one");
            this_.openView();
            // this_.addClass("show");
            // this_.anchor.style.backgroundColor = "#0A4686";
        };
        this.deactivate = function() {
            this_.viewselect.find(".show-analisys-area").hide();
            this_.closeView();

            this_.source.clear();
            // Draw.setActive(false);
            // Modify.setActive(false);
            // Modify.remove();
            // Draw.remove();

            this_.active = false;
            // this_.anchor.style.backgroundColor = "rgba(0,60,136,.5)";
            this_.anchor.parentNode.className = "ol-control drawinfo-geometry ol-unselectable there-can-be-only-one";
            DrawPolygon.setActive(false);
            DrawPBuffer.setActive(false);
            this_.containerDatos.hide();
            this_.containerDatos.find(".panel-body").html("");
            this_.btnLimpiarAnalizar.parent().hide();
            this_.btnAnalizar.parent().show();
            this_.btnAnalizar.prop("disabled", true);
            this_.clearGeom();
        };

        this.showanalisiarea = function () {
            this_.containerDatos.show();
            this_.viewselect.find(".show-analisys-area").hide();
        }

        this.anchor = document.createElement('button');
        this.anchor.type = 'button';
        this.anchor.title = 'Selección espacial';
        this.anchor.innerHTML = '<i class="fa fa-bar-chart"></i>';
        this.anchor.addEventListener('click', handleControl, false);
        this.anchor.addEventListener('touchstart', handleControl, false);



        var element = document.createElement('div');
        element.className = 'ol-control drawinfo-geometry ol-unselectable';
        // element.style.right = '0.5em';
        // element.style.top = (2.5+((order-1)*2))+'em';
        element.appendChild(this.anchor);

        ol.control.Control.call(this, {
            element: element,
            target: options.target
        });

        onlyOne = function(){
            var status = this.getAttribute("data-status");
            if(this_.active){
                this_.deactivate();
            }
        };

        $(this.anchor.parentNode).on('geo:there-can-be-only-one', onlyOne);

        this._analizar = function (e) {
            var capa = this_.viewselect.find(".select-capa-analisys").val();
            var geometry = this_.getJsonFeatures();

            this_.clearMarker();
            var data = {
                geometry: geometry,
                zona: this_.zona,
                idzona: this_.zona ? this_.viewselect.find(".select-capa-zona-analisys").val() : null
            }
            $.ajax({
                url: "/analysis/getdash/" + capa,
                type: "POST",
                data: JSON.stringify(data),
                beforeSend: function() {
                    this_.btnAnalizar.html("Cargando...");
                },
                success: function (resp) {
                    this_.containerDatos.find(".panel-body").html(resp);
                    this_.containerDatos.show();
                    dtsana.columns.adjust();
                    dtsana.columns.adjust();
                    this_.zona = false;
                },
                complete: function () {
                    this_.btnLimpiarAnalizar.parent().show();
                    this_.btnAnalizar.parent().hide();
                    this_.btnAnalizar.html("Anlizar");
                }
            });
        }

        this.limpiar_analisis = function (e) {
            var capa = this_.viewselect.find(".select-capa-analisys").val();
            this_.btnLimpiarAnalizar.parent().hide();
            this_.btnAnalizar.parent().show();
            this_.clearGeom();
            this_.zona = false;
        }

        this.selectcapaanalisys = function (e) {
            this_.btnLimpiarAnalizar.parent().hide();
            this_.btnAnalizar.parent().show();
            if(this_.wmslayer && this_.wmslayer.getVisible())
                this_.btnAnalizar.prop("disabled", false);
        }

        this.selectcapazonaanalisys = function (e) {
            this_.btnAnalizar.parent().show();
            this_.btnAnalizar.prop("disabled", true);

            this_.features.clear();
            this_.featuresSelection.clear();

            if(this.value == "") {
                this_.btnAnalizar.prop("disabled", true);
                return;
            }
            for (var i in ZONAS){
                var zona = ZONAS[i];
                if(this.value == zona.id){
                    for(var j = 0; j < zona.rutas.length; j++) {
                        // console.log(zonas_marginadas[i].the_geom);
                        var geomtryZona = JSON.parse(zona.rutas[j].the_geom);

                        var geojsonObject = {
                            'type': 'FeatureCollection',
                            'features': [
                                {"type": "Feature", "geometry": geomtryZona}
                            ]
                        };

                        var features = new ol.format.GeoJSON().readFeatures(geojsonObject, {
                            featureProjection: 'EPSG:3857'
                        });

                        this_.source.addFeatures(features);
                    }
                    var extent = this_.source.getExtent();
                    this_.map.getView().fit(extent, this_.map.getSize());
                    break;
                }
            }
            this_.btnAnalizar.prop("disabled", false);
        }

        this.activeBuffer = function (e) {
            this_.viewselect.find(".zonas-marginadas").hide();
            DrawPBuffer.init();
            Modify.init();
            Modify.setActive(true);
            DrawPBuffer.setActive(true);
            DrawPolygon.setActive(false);
            this_.zona = false;
        }

        this.activePlygon = function (e) {
            this_.viewselect.find(".zonas-marginadas").hide();
            DrawPolygon.init();
            Modify.init();
            Modify.setActive(true);
            DrawPolygon.setActive(true);
            DrawPBuffer.setActive(false);
            this_.zona = false;
        }

        this.activeZona = function (e) {
            this_.viewselect.find(".zonas-marginadas").show();
            this_.viewselect.find(".select-zona-marginada").val("");
            DrawPBuffer.setActive(false);
            DrawPolygon.setActive(false);
            this_.features.clear();
            this_.btnAnalizar.parent().show();
            this_.btnLimpiarAnalizar.parent().hide();
            this_.btnAnalizar.prop("disabled", true);
            this_.zona = true;



            // for(var i in zonas_marginadas) {
            //     var geomtryZona = JSON.parse(zonas_marginadas[i].the_geom);
            //
            //     var geojsonObject = {
            //         'type': 'FeatureCollection',
            //         'features': [
            //             {"type": "Feature", "geometry": geomtryZona}
            //         ]
            //     };
            //
            //     var features = new ol.format.GeoJSON().readFeatures(geojsonObject, {
            //         featureProjection: 'EPSG:3857'
            //     });
            //
            //     this_.source.addFeatures(features);
            // }
            //
            // var extent = this_.source.getExtent();
            // this_.map.getView().fit(extent, this_.map.getSize());

            // this_.wmslayer.getSource().getParams().CQL_FILTER;
            // var params = this_.wmslayer.getSource().getParams();
            // params.visible = false;
            // this_.wmslayer.getSource().updateParams(params);
            // this_.map.wmslayer.setVisible(false);
        }

        this.seletcZona = function () {
            this_.btnAnalizar.parent().show();

            if(this.value == "") {
                this_.btnAnalizar.prop("disabled", true);
                return;
            };
            for (var i in zonas_marginadas){
                if(this.value == zonas_marginadas[i].sigla){
                    // console.log(zonas_marginadas[i].the_geom);
                    var geomtryZona = JSON.parse(zonas_marginadas[i].the_geom);

                    var geojsonObject = {
                        'type': 'FeatureCollection',
                        'features': [
                            {"type":"Feature","geometry": geomtryZona}
                        ]
                    };

                    var features = new ol.format.GeoJSON().readFeatures(geojsonObject, {
                        featureProjection: 'EPSG:3857'
                    });

                    this_.source.addFeatures(features);
                    var extent = this_.source.getExtent();
                    this_.map.getView().fit(extent, this_.map.getSize());
                    break;
                }
            }
            this_.btnAnalizar.prop("disabled", false);
        }

        this.getJsonFeatures = function () {
            var allFeatures = this.vectorDraw.getSource().getFeatures();
            var format = new ol.format.GeoJSON();
            var g = format.writeFeatures(allFeatures);
            var obj = JSON.parse(g);
            var x = (obj.features[0].geometry);
            return JSON.stringify(x);
        };

        this.getWKT = function () {
            var format = new ol.format.WKT(), data;
            var features = new ol.Collection();
            var source = new ol.source.Vector({features: this.vectorDraw.getSource().getFeatures()});
            var vectorDraw = new ol.layer.Vector({
                source: source,
            });
            // source.setGeometry(this.vectorDraw.getSource().getFeatures()[0].getGeometry());
            var geometry = vectorDraw.getSource().getFeatures()[0].getGeometry().clone();
            geometry.transform('EPSG:3857', 'EPSG:32616');
            data = format.writeGeometry(geometry);
            return data;
        };

        this.clearGeom = function () {
            this_.features.clear();
            this.featuresSelection.clear();
            this_.wmslayer.getSource().getParams().CQL_FILTER;
            var params = this_.wmslayer.getSource().getParams();
            params.visible = false;
            this_.wmslayer.getSource().updateParams(params);
            this_.wmslayer.setVisible(false);
            this_.clearMarker();
        }

        this.updatewmsLayer = function (nombre_capa, layer, estilo) {
            if(this_.wmslayer == null) {
                var wmsSource = new ol.source.ImageWMS({
                    url: urlGeoserver,
                    params: {
                        'LAYERS': layer,
                        'STYLES': estilo,
                        'TILED': false,
                        CQL_FILTER: "WITHIN (the_geom, " + this_.getWKT() + ")",
                        'TOKEN': TOKEN
                    },
                    serverType: 'geoserver'
                });

                var wmsLayerOptions = {
                    source: wmsSource,
                    name: nombre_capa,
                    visible: true,
                    ignorame: true,

                };

                this_.wmslayer = new ol.layer.Image(wmsLayerOptions);
                this_.map.addLayer(this_.wmslayer);
            }
            else{
                this_.wmslayer.getSource().updateParams(
                    {
                        CQL_FILTER: "WITHIN (the_geom, " + this_.getWKT() + ")",
                        'LAYERS': layer,
                        'STYLES': estilo,
                    });
            }

            // this_.wmslayer.getSource().setVisible(true);
        }

        this.setMarker = function (latitud, longitud) {
            var iconStyle = new ol.style.Style({
                image: new ol.style.Icon(({
                    //anchor: [0.5, 0.85], // 42px
                    anchor: [0.5, 0.98], // 32px
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'fraction',
                    src: '' + urlBase + '/img/marker-info-cian-32.png',
                    ignorame: true
                })),
            });

            var coordinates = [parseFloat(longitud), parseFloat(latitud)];
            var point = new ol.geom.Point(coordinates);
            point.transform('EPSG:4326', 'EPSG:3857');
            var point_feature = new ol.Feature({
                geometry: point,
                ignorame: true
            });
            if(this_.mapInfoMarker){
                this_.map.removeLayer(this_.mapInfoMarker);
            }

            this_.mapInfoMarker = new ol.layer.Vector({
                title: 'marker-info',
                source: new ol.source.Vector({
                    features: [point_feature],
                    ignorame: true
                }),
                style: iconStyle,
                ignorame: true
            });
            this_.map.addLayer(this_.mapInfoMarker);
            this_.map.getView().setCenter(point.getCoordinates());
        }

        this.clearMarker = function () {
            if (this_.mapInfoMarker != null) {
                this_.map.removeLayer(this_.mapInfoMarker);
                this_.mapInfoMarker = null;
            }
        }
    };
    ol.inherits(DrawingInfoControl, ol.control.Control);

    var streetViewControl = new DrawingInfoControl({
        map: map,
    });
    map.addControl(streetViewControl);

    return streetViewControl;
}

