function addStreetViewControl(map, order) {
    var StreetViewControl = function(opt_options) {
        var options = opt_options || {};
        this.initializedControl = false;
        this.active = options.active || false;
        this.map = options.map;
        this.changingPosition = false;
        var this_ = this;

        var styleStreetPoint = new ol.style.Style({
            image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
                anchor: [0.5, 0.85],
                anchorXUnits: 'fraction',
                anchorYUnits: 'fraction',
                src: urlBase+'/img/person.png',
            }))
        });
        var styleStreetPointSelected = new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(255, 255, 255, 0.2)'
            }),
            stroke: new ol.style.Stroke({
                color: '#ff0000',
                width: 1
            }),
            image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
                anchor: [0.5, 0.85],
                anchorXUnits: 'fraction',
                anchorYUnits: 'fraction',
                src: urlBase+'/img/person-cian.png',
            })),
            /*text: new ol.style.Text({
                text: 'Clic y arrastre',
                scale: 1.3,
                fill: new ol.style.Fill({
                    color: '#000000'
                }),
                stroke: new ol.style.Stroke({
                    color: '#FFFFFF',
                    width: 3.5
                })
            })*/
        });

        var features = new ol.Collection();
        this.source = new ol.source.Vector({features: features});
        var vector = new ol.layer.Vector({
            source: this_.source,
            style: styleStreetPoint,
            ignorame: true
        });
        this.map.addLayer(vector);
        vector.set('selectable-street', true);

        var Modify = {
            init: function() {
                console.log("Modify INIT");
                this.select = new ol.interaction.Select({
                    style: [styleStreetPointSelected],
                    layers: function(layer) {
                        return layer.get('selectable-street') == true;
                    },
                });
                this_.map.addInteraction(this.select);

                this.modify = new ol.interaction.Modify({
                    features: this.select.getFeatures()
                });
                this_.map.addInteraction(this.modify);

                this.setEvents();
            },
            remove: function() {
                this_.map.removeInteraction(this.modify);
                this_.map.removeInteraction(this.select);
            },
            setEvents: function() {
                var selectedFeatures = this.select.getFeatures();

                this.select.unByKey('change:active');
                this.select.on('change:active', function() {
                    console.log('change:active');
                    /*selectedFeatures.forEach(selectedFeatures.remove, selectedFeatures);
                    //this_.closeView();
                    if(!this_.changingPosition) {
                        //this_.closeView();
                        setTimeout(function() {
                            this_.inactivate();
                        },100);
                    }*/
                });
                selectedFeatures.unByKey('remove');
                selectedFeatures.on('remove', function(event) {
                    console.log('remove');
                    if(!this_.changingPosition) {
                        //this_.closeView();
                        setTimeout(function() {
                            this_.inactivate();
                        },100);
                    } else {
                        this_.changingPosition = false;
                    }
                    event.stopPropagation();
                });
                this.modify.unByKey('modifyend');
                this.modify.on('modifyend', function (evt) {
                    evt.features.forEach(function (feature) {
                        this_.openView(feature);
                    });
                    evt.stopPropagation();
                })
            },
            setActive: function(active) {
                this.select.setActive(active);
                this.modify.setActive(active);
            }
        };
        //Modify.init();
        var Draw = {
            init: function() {
                console.log("Draw INIT");
                this_.map.addInteraction(this.Point);
                this.Point.setActive(false);
                this.setEvents();
            },
            remove: function() {
                this_.map.removeInteraction(this.Point);
            },
            Point: new ol.interaction.Draw({
                source: vector.getSource(),
                type: /** @type {ol.geom.GeometryType} */ ('Point')
            }),
            setEvents: function() {
                this.Point.unByKey('drawend');
                this.Point.on('drawend', function(e){
                    console.log('drawend');
                    this_.openView(e.feature);
                    Draw.setActive(false);
                    Modify.setActive(true);
                    e.stopPropagation();
                });
            },
            getActive: function() {
                return this.Point.getActive();
            },
            setActive: function(active) {
                this.Point.setActive(active);
            }
        };
        //Draw.init();
        //Draw.setActive(true);
        //Modify.setActive(false);
        /**
         * Let user change the geometry type.
         * @param {Event} e Change event.
         */
        /*optionsForm.onchange = function(e) {
            var type = e.target.getAttribute('name');
            var value = e.target.value;
            if (type == 'draw-type') {
                Draw.getActive() && Draw.setActive(true);
            } else if (type == 'interaction') {
                if (value == 'modify') {
                    Draw.setActive(false);
                    Modify.setActive(true);
                } else if (value == 'draw') {
                    Draw.setActive(true);
                    Modify.setActive(false);
                }
            }
        };*/

        // The snap interaction must be added after the Modify and Draw interactions
        // in order for its map browser event handlers to be fired first. Its handlers
        // are responsible of doing the snapping.
        /*var snap = new ol.interaction.Snap({
            source: vector.getSource()
        });
        this_.map.addInteraction(snap);*/


        this.openView = function(feature) {
            $("#map-streetview-container").css({height: 352});
            $("#map-streetview-container").html("");

            var coords = feature.getGeometry().getCoordinates();
            coords = ol.proj.transform(coords, 'EPSG:3857', 'EPSG:4326');
            var lat = coords[1];
            var lng = coords[0];

            /*var transform = ol.proj.getTransform('EPSG:4326', 'EPSG:3857');
            var newFeature = new ol.Feature({
                geometry: new ol.geom.Point(transform([lng, lat]))
            });
            this_.source.getFeatures().pop();
            //Modify.select.getFeatures().pop();
            this_.source.addFeature(newFeature);*/

            var $div = $("div");
            // <div class="panel-heading"><div class="panel-control"><a href="#" class="close icon-lg"><span class="fa fa-times"></span></a></div><h3 class="panel-title">Resumen</h3></div>
            $("#map-streetview-container").html('<div class="panel panel-default" style="margin-bottom: 0;"> ' +
                '<div class="panel-heading">' +
                    '<div class="panel-control">' +
                        '<a href="#" class="close icon-lg" id="resultados-streetview-close"><span class="fa fa-times"></span></a>' +
                    '</div> ' +
                    '<h3 class="panel-title">Street View</h3>' +
                '</div> ' +
                '<div class="panel-body" style="width: 400px; height: 300px;" id="map-streetview"></div>' +
            '</div> ');
            var resultadosPanorama = new google.maps.StreetViewPanorama(document.getElementById('map-streetview'), {
                position: {lat: lat, lng: lng},
                linksControl: false,
                panControl: false,
                enableCloseButton: false
            });
            resultadosPanorama.addListener('position_changed', function() {
                this_.changingPosition = true;

                var newCoordinates= resultadosPanorama.getPosition().toJSON();
                var transform = ol.proj.getTransform('EPSG:4326', 'EPSG:3857');
                var newFeature = new ol.Feature({
                    geometry: new ol.geom.Point(transform([newCoordinates.lng, newCoordinates.lat]))
                });

                //console.log("--- CAMBIO DE POSICION --- START");
                /*console.log("Features");
                console.log(features);*/
                /*console.log("vector.getSource().getFeatures()");
                console.log(vector.getSource().getFeatures());*/
                /*console.log("this_.source.getFeatures()");
                console.log(this_.source.getFeatures());
                console.log("Modify.select.getFeatures()");
                console.log(Modify.select.getFeatures());*/

                //console.log("CLEAR");
                //this_.source.getFeatures().pop();
                //this_.source.clear();


                features.clear();
                Modify.select.getFeatures().clear();

                /*console.log(this_.source.getFeatures());
                console.log("Modify.select.getFeatures()");
                console.log(Modify.select.getFeatures());*/

                setTimeout(function() {
                    console.log("ADD");
                    Modify.select.getFeatures().push(newFeature);
                },100);
                /*console.log("ADD");*/
                //this_.source.addFeature(newFeature);
                /*Modify.select.getFeatures().push(newFeature);*/
                //features.push(newFeature);
                /*console.log("this_.source.getFeatures()");
                console.log(this_.source.getFeatures());
                console.log("Modify.select.getFeatures()");
                console.log(Modify.select.getFeatures());*/

                /*features.pop();
                features.push(newFeature);*/
                //selectedFeatures.forEach(selectedFeatures.remove, selectedFeatures);

                //vector.getSource().forEachFeature(vector.getSource().getFeatures().remove,vector.getSource().getFeatures());
                //vector.getSource().addFeature(newFeature);

                //this_.source.getFeatures().pop();

                //this_.source.addFeature(newFeature);

                //this_.changingPosition = false;
                console.log("--- CAMBIO DE POSICION --- END");
            });
            setTimeout(function(){
                google.maps.event.trigger(resultadosPanorama, 'resize');
            },500);

            $("#resultados-streetview-close").off("click");
            $("#resultados-streetview-close").on("click", function(){
                $("#map-streetview-container").html("");
                $("#map-streetview-container").removeClass("show");
            });

            $("#map-streetview-container").draggable({
                handle: ".panel-heading ",
                containment: "#analysis-map"
            });

            checkWithDragAndResizeComponent("#map-streetview-container");
        };
        this.closeView = function() {
            $("#map-streetview-container").html("");
            //this_.source.getFeatures().pop();
            /*Draw.setActive(true);
            Modify.setActive(false);*/
        };

        /*var Modify = {
            init: function() {

                this.select = new ol.interaction.Select({
                    /!*condition: ol.events.condition.pointerMove,*!/
                    style: [styleStreetPointSelected],
                    features: this_.features,
                    layers: function(layer) {
                        return layer.get('selectable-street') == true;
                    },
                });
                this_.getMap().addInteraction(this.select);

                this.modify = new ol.interaction.Modify({
                    features: this.select.getFeatures()
                });
                this_.getMap().addInteraction(this.modify);
                this.setEvents();
            },
            remove: function() {
                this_.getMap().removeInteraction(this.modify);
                this_.getMap().removeInteraction(this.select);
            },
            setEvents: function() {
                var selectedFeatures = this.select.getFeatures();
                this.select.on('change:active', function() {
                    selectedFeatures.forEach(selectedFeatures.remove, selectedFeatures);
                });
                selectedFeatures.on('remove', function(event) {
                    this_.closeView();
                });
                this.modify.on('modifyend', function (evt) {
                    evt.features.forEach(function (feature) {
                        this_.openView(feature);
                    });
                })
            },
            setActive: function(active) {
                this.select.setActive(active);
                this.modify.setActive(active);
            }
        };
        var Draw = {
            init: function() {
                this_.getMap().addInteraction(this.Point);
                this.Point.setActive(false);
                this.setEvents();
            },
            remove: function() {
                this_.getMap().removeInteraction(this.Point);
            },
            Point: new ol.interaction.Draw({
                source: this_.source,
                type: /!** @type {ol.geom.GeometryType} *!/ ('Point')
            }),
            setEvents: function() {
                this.Point.on('drawend', function(e){
                    this_.openView(e.feature);
                });
            },
            getActive: function() {
                return this.Point.getActive();
            },
            setActive: function(active) {
                this.Point.setActive(active);
            }
        };*/

        var handleControl = function(e) {
            e.preventDefault();
            if (!this_.active) {
                this_.activate();
            } else {
                this_.inactivate();
            }
        };
        this.activate = function() {
            //if (!this_.initializedControl) {
                Modify.init();
                Draw.init();
                this_.initializedControl = true;
            //}
            Draw.setActive(true);
            Modify.setActive(false);

            this_.active = true;
            this_.anchor.parentNode.className = "ol-control draw-point ol-unselectable there-can-be-only-one activate";
            $(".there-can-be-only-one").not(this_.anchor.parentNode).trigger("geo:there-can-be-only-one");
            $("#map-streetview-container").addClass("show");
            // this_.anchor.style.backgroundColor = "#0A4686";
        };
        this.inactivate = function() {
            this_.closeView();

            this_.source.clear();
            features.clear();
            Draw.setActive(false);
            Modify.setActive(false);
            Modify.remove();
            Draw.remove();

            this_.active = false;
            // this_.anchor.style.backgroundColor = "rgba(0,60,136,.5)";
            this_.anchor.parentNode.className = "ol-control draw-point ol-unselectable there-can-be-only-one";
        };

        this.anchor = document.createElement('button');
        this.anchor.type = 'button';
        this.anchor.title = 'Street view';
        this.anchor.innerHTML = '<i class="fa fa-street-view"></i>';
        this.anchor.addEventListener('click', handleControl, false);
        this.anchor.addEventListener('touchstart', handleControl, false);



        var element = document.createElement('div');
        element.className = 'ol-control draw-point ol-unselectable';
        // element.style.right = '0.5em';
        // element.style.top = (2.5+((order-1)*2))+'em';
        element.appendChild(this.anchor);

        ol.control.Control.call(this, {
            element: element,
            target: options.target
        });

        onlyOne = function(){
            var status = this.getAttribute("data-status");
            if(this_.active){
                this_.inactivate();
            }
        };

        $(this.anchor.parentNode).on('geo:there-can-be-only-one', onlyOne);
    };
    ol.inherits(StreetViewControl, ol.control.Control);

    /*var features = new ol.Collection();
    var vectorSource = new ol.source.Vector({features: features});
    var styleStreetPoint = new ol.style.Style({
        image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ /*({
            /*anchor: [0.5, 0.85],
            anchorXUnits: 'fraction',
            anchorYUnits: 'fraction',
            /*color: colorGeometry,*/
            /*src: urlBase+'/img/person.png',//'/img/marker-icon.png',
        }))
    });
    var vectorLayer = new ol.layer.Vector({
                source: vectorSource,
                style: styleStreetPoint
            });
    map.addLayer(vectorLayer);
    vectorLayer.set('selectable-street', true);*/

    var streetViewControl = new StreetViewControl({
        map: map,
        /*source: vectorSource,
        features: features*/
    });
    map.addControl(streetViewControl);

    /*console.log("Interacciones");
    console.log(map.getInteractions());
    console.log("Controles");
    console.log(map.getControls());*/

    return streetViewControl;
}

