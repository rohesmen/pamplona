/**
 * OpenLayers 3 Layer Switcher Control.
 * See [the examples](./examples) for usage.
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object} opt_options Control options, extends olx.control.ControlOptions adding:
 *                              **`tipLabel`** `String` - the button tooltip.
 */
ol.control.InfoBuffer = function(opt_options) {
    var options = opt_options || {};

    var tipLabel = options.tipLabel ?
        options.tipLabel : 'Información';
    this.activate = false;

    this.mapListeners = [];

    this.hiddenClassName = 'ol-unselectable ol-control layer-info-buffer there-can-be-only-one';
    this.shownClassName = this.hiddenClassName + ' activate';

    var element = document.createElement('div');
    element.setAttribute('data-status', 'deactivate');
    element.className = this.hiddenClassName;

    var button = document.createElement('button');
    button.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
    button.setAttribute('title', tipLabel);
    element.appendChild(button);

    var this_ = this;

    button.onclick = function(e) {
        e = e || window.event;
        var button = e.target;
        var status = this_.element.getAttribute("data-status");
        if(status == "deactivate"){
            this_._activateInfo();
        }
        else{
            $('#search-category').val('PERSONA').trigger('change').trigger('select2:select');
            this_._deactivateInfo();
        }
        e.preventDefault();
    };

    onlyOne = function(){
        var status = this.getAttribute("data-status");
        if(status == "activate"){
            $('#search-category').val('PERSONA').trigger('change').trigger('select2:select');
            this_._deactivateInfo();
        }
    };

    $(button.parentNode).on('geo:there-can-be-only-one', onlyOne);

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};

ol.inherits(ol.control.InfoBuffer, ol.control.Control);

// ol.control.InfoBuffer.prototype._mouseEvent = function(e){
//     //var map = this._map;
//     console.log(e);
// };

ol.control.InfoBuffer.prototype._activateSearchAreaOverlay = function(){
    if(document.getElementsByClassName('overlay-search-area').length === 0){
        var parent = document.createElement('div');
        parent.className = "overlay overlay-search-area";
        parent.onclick = this._deactivateSearchAreaOverlay;

        var img = document.createElement('img');
        img.className = "center";
        img.src = urlBase+"/img/search-area.gif";

        parent.appendChild(img);

        document.body.appendChild(parent);
        var self = this;
        setTimeout(function(){ self._deactivateSearchAreaOverlay(); }, 5000);
    }
};

ol.control.InfoBuffer.prototype._deactivateSearchAreaOverlay = function(){
    if(document.getElementsByClassName('overlay-search-area').length > 0){
        $('.overlay').hide();
    }
};

/**
 * Activar herramienta de info.
 */
ol.control.InfoBuffer.prototype._activateInfo = function() {
    this.element.setAttribute('data-status', 'activate');
    $(".there-can-be-only-one").not(this.element).trigger("geo:there-can-be-only-one");
    this.activate = true;
    if (this.element.className != this.shownClassName) {
        this.element.className = this.shownClassName;
    }
    this._activateSearchAreaOverlay();
    var map = this.getMap();
    var this_ = this;
    if(!this.drawTool){
        var features = new ol.Collection();
        this.sourceDraw = new ol.source.Vector({features: features});
        this.vectorDraw = new ol.layer.Vector({
            source: this_.sourceDraw,
            style: new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                }),
                stroke: new ol.style.Stroke({
                    color: '#ffcc33',
                    width: 2
                }),
                image: new ol.style.Circle({
                    radius: 7,
                    fill: new ol.style.Fill({
                        color: '#ffcc33'
                    })
                })
            })
        });

        this.drawTool = new ol.interaction.Draw({
            source: this_.vectorDraw.getSource(),
            type: /** @type {ol.geom.GeometryType} */ "Circle"
        });
        var drawTool = this.drawTool;
        this.drawTool.on('drawend', function(e) {
            var format = new ol.format.WKT(), data;

            //this_.sourcerDraw.getSource().addFeatures(e.feature);
            var auxPolygon = ol.geom.Polygon.fromCircle(e.feature.getGeometry());
            try {
                // convert the data of the vector_layer into the chosen format
                auxPolygon.transform('EPSG:3857', 'EPSG:32616');
                data = format.writeGeometry(auxPolygon);

                busquedaGlobal('GEOMETRIA', data);
                // console.log(data);
            } catch (e) {
                console.log("Error: " + e.message);
                return;
            }
        });
        this.drawTool.on('drawstart', function(event) {
            if(this_.vectorDraw){
                // var format = new ol.format.WKT();
                // console.log(format.writeFeatures(this_.vectorDraw.getSource().getFeatures()));
                this_.vectorDraw.getSource().clear();
            }
        });
    }
    map.addLayer(this.vectorDraw);
    map.addInteraction(this.drawTool);
};

/**
 * desactivar herramienta de info.
 */
ol.control.InfoBuffer.prototype._deactivateInfo = function() {
    if(this.vectorDraw){
        this.element.setAttribute('data-status', 'deactivate');
        if (this.element.className != this.hiddenClassName) {
            this.element.className = this.hiddenClassName;
        }

        this.vectorDraw.getSource().clear();
        this.getMap().removeInteraction(this.drawTool);
    }

    //var elMap = this.getMap().getTargetElement();
    //$(elMap).off("mousemove", this._mouseEvent);
};