ol.control.LayerSwitcher.Geoadmin = function(settings){

    var filtroText = "";
    var layerSwitcher = new ol.control.LayerSwitcher(settings);

    layerSwitcher.renderLayers_ = function(lyr, elm) {
        var lyrs = lyr.getLayers().getArray().slice().reverse();
        for (var i = 0, l; i < lyrs.length; i++) {
            l = lyrs[i];
            if(l.get("type") === "base" || l.get('ignorame'))
                continue;

            if (l.get('title')) {
                elm.appendChild(this.renderLayer_(l, i));
            }
        }
    };

    layerSwitcher.quitaracentos = function(cadena) {
        return cadena.replace(/[áàãâä]/gi,"a")
            .replace(/[éè¨ê]/gi,"e")
            .replace(/[íìïî]/gi,"i")
            .replace(/[óòöôõ]/gi,"o")
            .replace(/[úùüû]/gi, "u")
            .replace(/[ç]/gi, "c")
    };

    layerSwitcher.renderLayer_ = function(lyr, idx){

        if(lyr.get("ignorame")){
            return;
        }
        var this_ = this;

        var li = document.createElement('li');

        var lyrTitle = lyr.get('title');
        var lyrId = ol.control.LayerSwitcher.uuid();

        var label = document.createElement('label');

        if(lyr.get("google")){
            li.className = 'layer';
            var input = document.createElement('input');
            if (lyr.get('type') === 'base') {
                input.type = 'radio';
                input.name = 'base';
            } else {
                input.type = 'checkbox';
            }
            input.id = lyrId;
            input.checked = lyr.get('visible');
            input.onchange = function(e) {
                this_.setVisible_(lyr, e.target.checked);
                if(this_.legendControl){
                    this_.legendControl.showPanel();
                }
            };
            li.appendChild(input);

            label.htmlFor = lyrId;
            label.innerHTML = lyrTitle;
            li.appendChild(label);
        }
        else if (lyr.getLayers && !lyr.get('combine')) {
            label.innerHTML = lyrTitle;
            label.className = "name-gropu-layer";
            li.appendChild(label);
            var ul = document.createElement('ul');
            li.appendChild(ul);

            this.renderLayers_(lyr, ul);
        }
        else {
            if(this.filtroText != ""){
                var valfiltro = this.quitaracentos(this.filtroText).toLowerCase();
                var auxNameLayer = this.quitaracentos(lyrTitle).toLowerCase();
                if(auxNameLayer.indexOf(valfiltro) !== -1) {
                    li.style.display = "table";
                } else {
                    li.style.display = "none";
                }
            }
            var layerSource = lyr.get('source');
            var layerSelected = lyr.get('visible');
            var lyrProperties = lyr.get('properties');
            var lyrLabeled = (lyrProperties != undefined)?lyrProperties.labeled:false;
            var lyrStyle = (lyrProperties != undefined)?lyrProperties.style:'';
            var lyrStyleLabel = (lyrProperties != undefined)?lyrProperties.styleLabel:'';

            li.className = 'layer';
            li.setAttribute("data-name", lyrTitle.toLowerCase());
            li.setAttribute("data-style", lyrTitle.toLowerCase());
            li.setAttribute("data-layer", layerSource.getParams().LAYERS);
            if(layerSelected){
                li.className += " blue-text";
            }
            var input = document.createElement('input');
            if (lyr.get('type') === 'base') {
                input.type = 'radio';
                input.name = 'base';
            } else {
                input.type = 'checkbox';
            }
            input.id = lyrId;
            input.checked = layerSelected;
            var eventInput = function (e) {
                var value = $(this).val();
                this_.setVisible_(lyr, e.target.checked);
                if(this_.legendControl){
                    this_.legendControl.renderPanel();
                    this_.legendControl.showPanel();
                }
                if(e.target.checked) {
                    $(this).parent().addClass("blue-text");
                    $(lyr.containerSlider).show();
                }
                else{
                    $(this).parent().removeClass("blue-text");
                    $(lyr.containerSlider).hide();
                }
                if(e.target.checked) {
                    layerSource.updateParams({"STYLES": (lyrProperties.labeled?lyrStyleLabel:lyrStyle)});
                }
            }

            input.onchange = eventInput;
            li.appendChild(input);
            
            label.htmlFor = lyrId;
            label.innerHTML = lyrTitle;
            label.className = 'label-name-layer';
            li.appendChild(label);

            if(lyrProperties.label){
                var buttonLabel = document.createElement('span');
            
				buttonLabel.className = 'label label-default btn-label-layer'+(lyrLabeled?' active':'');
                buttonLabel.innerHTML = 'E';
                buttonLabel.style.cursor = 'pointer';

                buttonLabel.onclick = function(e) {
                    var value = $(this).val();
                    $(this).toggleClass( "active" );
                    lyrProperties.labeled = !lyrProperties.labeled;
                    if(lyr.get('visible')) {
                        layerSource.updateParams({"STYLES": (lyrProperties.labeled?lyrStyleLabel:lyrStyle)});
                    }
                };

				var buttonLabel2 = document.createElement('span');
                buttonLabel2.className = 'label label-default fa-fa-search btn-label-layer';
                buttonLabel2.innerHTML = '';
                buttonLabel2.style.cursor = 'pointer';
				
				var iLabel = document.createElement('i');
				iLabel.className = "fa fa-search";

				buttonLabel2.appendChild(iLabel);
				
                buttonLabel2.onclick = function(e) {
                    map.getView().fit(data, map.getSize());
                };
				
				
                li.appendChild(buttonLabel);
				//li.appendChild(buttonLabel2);
            }

            var buttonZoom = document.createElement('span');

            buttonZoom.className = 'fa fa-search label label-default';
            buttonZoom.innerHTML = '&nbsp;';
            buttonZoom.style= 'margin-left: 5px;';
            buttonZoom.style.cursor = 'pointer';
            var map = this.getMap();
            buttonZoom.onclick = function(e) {
                var value = $(this).val();

                var $inj = angular.injector(['geoadmin']);
                var serv = $inj.get('mapModel');
                var namelayer = layerSource.getParams().LAYERS;
                //console.log(namelayer);
                serv.getExtent(namelayer).then(function(data){
                    map.getView().fit(data, map.getSize());
                }).catch(function(err) {
                    // Tratar el error
                });

            };
            //li.appendChild(buttonZoom);

            if(lyrProperties.table) {
                var buttonTable = document.createElement('span');

                buttonTable.className = 'fa fa-table label label-default';
                buttonTable.innerHTML = '&nbsp;';
                buttonTable.style = 'margin-left: 5px;';
                buttonTable.style.cursor = 'pointer';

                buttonTable.onclick = function (e) {
                    var namelayer = layerSource.getParams().LAYERS;

                    $.get(APP + "/map/datatable/" + lyrProperties.id, function (data) {
                        $(".geotable").remove();
                        $(".geotable-script").remove();

                        $("#page-wrapper").append(data);
                    });
                };
                li.appendChild(buttonTable);
            }

            var sliderVisible = "none";
            if(lyr.getVisible()){
                sliderVisible = "block";
            }
            var slider = document.createElement('div');
            var containerSlider = document.createElement('div');
            // containerSlider.style = 'padding: 10px 12px 15px 12px; display: ' + sliderVisible + ';';
            containerSlider.style.padding = "10px 12px 15px 12px";
            containerSlider.style.display = sliderVisible;
            containerSlider.appendChild(slider);
            // lyr.lastOpacity = 1;
            var range_all_sliders = {
                'min': [ 0 ],
                '10%': [ 0.1 ],
                '20%': [ 0.2 ],
                '30%': [ 0.3 ],
                '40%': [ 0.4 ],
                '50%': [ 0.5 ],
                '60%': [ 0.6 ],
                '70%': [ 0.7 ],
                '80%': [ 0.8 ],
                '90%': [ 0.9 ],
                'max': [ 1 ]
            };
            noUiSlider.create(slider, {
                start: [(typeof lyr.lastOpacity !== "undefined" && lyr.lastOpacity != null) ? lyr.lastOpacity : 1],
                range: range_all_sliders,
                // tooltips: [ true ],
                step: 0.1,
                // pips: { // Show a scale with the slider
                //     mode: 'steps',
                //     // stepped: true,
                //     format: wNumb({ decimals: 1 }),
                // },
                range: {
                    'min': 0,
                    'max': 1
                },
                format: wNumb({
                    decimals: 1,
                }),
                // connect: [true, false]
                connect : 'lower',
            });
            lyr.containerSlider = containerSlider;
            slider.noUiSlider.on('slide', function ( values, handle, unencoded, isTap, positions ) {
                console.log(values);
                lyr.setOpacity(parseFloat(values[0]));
                lyr.lastOpacity = parseFloat(values[0]);
            });
            li.appendChild(containerSlider);
        }

        return li;
    };

    layerSwitcher.searcher = function(filter) {
        var lyrs = this.getMap().getLayers().getArray().slice().reverse();
        for (var i = 0, l; i < lyrs.length; i++) {
            l = lyrs[i];
            if(l.get("type") === "base")
                continue;
            var id = l.get('id');
            if (l.get('title').toLowerCase().indexOf(filter.toLowerCase()) !== -1) {
                $("#layer-"+id).css("display",'block');
            } else {
                $("#layer-"+id).css("display",'none');
            }
        }
    }

    /**
     * Re-draw the layer panel to represent the current state of the layers.
     */
    layerSwitcher.renderPanel = function() {

        this.ensureTopVisibleBaseLayerShown_();

        while(this.panel.firstChild) {
            this.panel.removeChild(this.panel.firstChild);
        }

        //var ul = document.createElement('ul');
        //this.panel.appendChild(ul);
        //this.renderLayers_(this.getMap(), ul);

        var this_ = this;
        var panelheading = document.createElement('div');
        panelheading.className = "panel-heading";

        var panelControl = document.createElement('div');
        panelControl.className = "panel-control";
        var btnClose = document.createElement('a');
        //btnClose.type = "button";
        btnClose.href = "#";
        btnClose.className = "close icon-lg";
        var span = document.createElement('span');
        //span.innerHTML = "X";
        span.className = "fa fa-times";
        btnClose.onclick = function(e){
            e = e || window.event;
            this_.hidePanel();
            e.preventDefault();
        };
        btnClose.appendChild(span);
        panelControl.appendChild(btnClose);
        panelheading.appendChild(panelControl);


        var panelTitle = document.createElement('h3');
        panelTitle.className = "panel-title";
        panelTitle.innerHTML = "Manejador de capas";
        panelheading.appendChild(panelTitle);
        //var text = document.createTextNode("Manejador de capas");
        //panelheading.appendChild(text);
        this.panel.appendChild(panelheading);
        //<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

        //this.panel.style.maxHeight = "400px";


        var panelBody = document.createElement('div');
        panelBody.className = "panel-body";

        var searchContainer = document.createElement('div');
        searchContainer.className = "form-group has-feedback has-clear";
        var searchInput = document.createElement('input');
        searchInput.type = 'text';
        searchInput.value = this_.filtroText;
        searchInput.className = 'form-control input-sm';
        searchInput.placeholder = 'Buscar';
        $(searchInput).on('input propertychange', function() {
            var $this = $(this);
            var visible = Boolean($this.val());
            $this.siblings('.form-control-clear').toggleClass('hidden', !visible);
        }).trigger('propertychange');

        searchContainer.appendChild(searchInput);

        var span = document.createElement('span');
        span.className = "form-control-clear fa fa-remove form-control-feedback hidden";
        span.style="line-height: inherit";
        span.onclick = function(){
            $(this).siblings('input[type="text"]').val('')
                .trigger('propertychange').trigger('keyup').focus();
        };
        searchContainer.appendChild(span);

        searchInput.onkeyup = function() {
            var value = this.value;
            this_.filtroText = value;
            value = this_.quitaracentos(value);
            var this__ = this_;
            $(".layer").each(function() {
                var nameLayer = this.getAttribute("data-name");
                nameLayer = this__.quitaracentos(nameLayer);
                if(nameLayer.indexOf(value.toLowerCase()) !== -1) {
                    this.style.display = "table";
                } else {
                    this.style.display = "none";
                }
            });
        };
        panelBody.appendChild(searchContainer);

        var button = document.createElement('a');
        button.id = 'layers-off';
        button.className="btn btn-sm btn-block btn-default";
        button.innerHTML="Desactivar todas las capas";
        button.onclick = function(){
            $('.layer-switcher input[type=checkbox]:checked').click();
        };
        // panelBody.appendChild(button);

        var ul = document.createElement('ul');
        ul.style.overflowY = "auto";
        panelBody.appendChild(ul);

        var panelFooter = document.createElement('div');
        panelFooter.className = "panel-footer";

        panelFooter.appendChild(button);
        this.ulCapas = ul;
        this.resize();

        this.panel.appendChild(panelBody);
        this.panel.appendChild(panelFooter);
        // ol.control.GetLegend.enableTouchScroll_(ul);
        ol.control.LayerSwitcher.enableTouchScroll_(ul);
        this.renderLayers_(this.getMap(), ul);


    };

    layerSwitcher.resize = function(){
        this.ulCapas.style.maxHeight = "250px";
        if(window.innerWidth <= 992 && window.innerWidth < 768){
            var navBarHeight = document.getElementById("navbar-container").clientHeight;
            var busquedaHeight = document.getElementById("resultados").nextElementSibling.clientHeight;

            this.ulCapas.style.maxHeight = (window.innerHeight - navBarHeight - busquedaHeight -220) + "px";
        }
    };

    /*var headerSwitcher = $('<div>');
     headerSwitcher.html("Header capas");
     layerSwitcher.setHeader(headerSwitcher);*/

    return layerSwitcher;
};