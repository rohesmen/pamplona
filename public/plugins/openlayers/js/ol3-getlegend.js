/**
 * OpenLayers 3 Layer Switcher Control.
 * See [the examples](./examples) for usage.
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object} opt_options Control options, extends olx.control.ControlOptions adding:
 *                              **`tipLabel`** `String` - the button tooltip.
 */
ol.control.GetLegend = function(opt_options) {

    var open = false;

    var options = opt_options || {};

    var countActives = 0;

    var tipLabel = options.tipLabel ?
        options.tipLabel : 'Legend';
    this.recurso = options.recurso ? options.recurso : 'ine';

    this.mapListeners = [];

    this.hiddenClassName = 'ol-unselectable ol-control getlegend-geoadmin';
    if (ol.control.GetLegend.isTouchDevice_()) {
        this.hiddenClassName += ' touch';
    }
    this.shownClassName = this.hiddenClassName + ' shown';

    var element = document.createElement('div');
    element.className = this.hiddenClassName;

    var button = document.createElement('button');
    button.setAttribute('title', tipLabel);
    //button.className = "btn btn-default";
    button.innerHTML = "Simbología";
    element.appendChild(button);

    this.panel = document.createElement('div');
    this.panel.className = 'panel panel-default';
    element.appendChild(this.panel);

    var this_ = this;

    button.onclick = function(e) {
        e = e || window.event;
        this_.showPanel();
        e.preventDefault();
    };

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};

ol.inherits(ol.control.GetLegend, ol.control.Control);

/**
 * Show the layer panel.
 */
ol.control.GetLegend.prototype.showPanel = function() {
    if (this.element.className != this.shownClassName) {
        this.element.className = this.shownClassName;
        this.open = true;
        this.renderPanel();
        checkWithDragAndResizeComponent(".getlegend-geoadmin .panel");
    }
};

/**
 * Hide the layer panel.
 */
ol.control.GetLegend.prototype.hidePanel = function() {
    if (this.element.className != this.hiddenClassName) {
        this.element.className = this.hiddenClassName;
        this.open = false;
    }
};

/**
 * Re-draw the layer panel to represent the current state of the layers.
 */
ol.control.GetLegend.prototype.renderPanel = function() {
    if(!this.open){
        return;
    }
    var this_ = this;
    this.countActives = 0;
    while(this.panel.firstChild) {
        this.panel.removeChild(this.panel.firstChild);
    }

    var panelheading = document.createElement('div');
    panelheading.className = "panel-heading";
    //var text = document.createTextNode("Simbología");
    //panelheading.appendChild(text);


    this.panel.appendChild(panelheading);
    //<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    var panelControl = document.createElement('div');
    panelControl.className = "panel-control";
    var btnClose = document.createElement('a');
    //btnClose.type = "button";
    btnClose.href = "#";
    btnClose.className = "close icon-lg";
    var span = document.createElement('span');
    //span.innerHTML = "X";
    span.className = "fa fa-times";
    btnClose.onclick = function(e){
        e = e || window.event;
        this_.hidePanel();
        e.preventDefault();
    };
    btnClose.appendChild(span);
    //panelheading.appendChild(btnClose);
    panelControl.appendChild(btnClose);
    panelheading.appendChild(panelControl);

    var panelTitle = document.createElement('h3');
    panelTitle.className = "panel-title";
    panelTitle.innerHTML = "Simbologia";
    panelheading.appendChild(panelTitle);

    //this.panel.style.maxHeight = "400px";


    var panelBody = document.createElement('div');
    panelBody.className = "panel-body";
    var ul = document.createElement('ul');
    panelBody.appendChild(ul);
    this.panel.appendChild(panelBody);
    this.renderLayers_(this.getMap(), ul);

    if(this.countActives < 1){
        var li = document.createElement('li');
        li.innerHTML = "No hay capas activas";
        ul.appendChild(li);
    }
    ol.control.GetLegend.enableTouchScroll_(ul);
};

/**
 * Set the map instance the control is associated with.
 * @param {ol.Map} map The map instance.
 */
ol.control.GetLegend.prototype.setMap = function(map) {
    // Clean up listeners associated with the previous map
    for (var i = 0, key; i < this.mapListeners.length; i++) {
        this.getMap().unByKey(this.mapListeners[i]);
    }
    this.mapListeners.length = 0;
    // Wire up listeners etc. and store reference to new map
    ol.control.Control.prototype.setMap.call(this, map);
    if (map) {
        var this_ = this;
        //this.mapListeners.push(map.on('pointerdown', function() {
        //    this_.hidePanel();
        //}));
        this.renderPanel();
    }
};

ol.control.GetLegend.prototype.getSimbologia = function(contenedor, layer, estilo) {
    //var urlImg = "http://plataformadoscero.com/geoserver/simbologia/estilo/" + estilo;
    //$.ajax({
    //    url: urlImg,
    //    type: 'GET',
    //    dataType: 'json',
    //    converters: { "text json": function (json_string) {
    //        if ( typeof json_string != 'string' || !$.trim(json_string).length ) {
    //            return {};
    //        } else {
    //            return jQuery.parseJSON( json_string );
    //        }
    //    }},
    //    contentType: "application/json; charset=utf-8",
    //    success: function(imgSimbologia){
    //        var imgSimple = "<img src='data:image/png;base64," + imgSimbologia + "'/>";
    //        contenedor.innerHTML = imgSimple;
    //    },
    //    error: function(){
    //
    //    },
    //    crossDomain: true
    //});

    var source = layer.get("source");
    var params = source.getParams();

    var data = {
        request: "GetLegendGraphic",
        version: "1.0.0",
        format: "image/png",
        width: 20,
        height: 20,
        strict: false,
        layer: params.LAYERS,
        style: estilo,
        token: TOKEN
    };
    contenedor.innerHTML = "<img src='"+"/" + this.recurso + "?"+jQuery.param(data)+"'/>";
    // $.ajax({
    //     url: "/" + this.recurso + "/legend",
    //     data: jQuery.param(data),
    //     //async: false,
    //     success: function (resp) {
    //         var imgSimple = "<img src='data:image/png;base64," + resp + "'/>";
    //         contenedor.innerHTML = imgSimple;
    //     }
    // });
};
/**
 * Render all layers that are children of a group.
 * @private
 * @param {ol.layer.Base} lyr Layer to be rendered (should have a title property).
 * @param {Number} idx Position in parent group list.
 */
ol.control.GetLegend.prototype.renderLayer_ = function(lyr, idx) {

    var this_ = this;

    var li = document.createElement('li');

    var lyrTitle = lyr.get('title');
    var lyrId = ol.control.GetLegend.uuid();
    var lyrLayer = lyr.get

    var estilo = lyr.get("properties").style;
    var tematico = lyr.get("properties").tematico;
    var divl = document.createElement('div');
    divl.className = "img-name-legend";
    //var img = document.createElement('img');

    //divl.appendChild(img);

    var divr = document.createElement('div');
    divr.className = "name-legend";
    divr.innerHTML = lyrTitle;
    divr.title = lyrTitle;

    var divTexto = document.createElement('div');
    li.appendChild(divTexto);
    divTexto.appendChild(divl);
    divTexto.appendChild(divr);

    if(tematico){
        divTexto.style.cursor = "pointer";
        $(divTexto).off("click");
        $(divTexto).on("click", function(){
            var $contentLegend = $(this).next();
            $contentLegend.slideToggle();
            $(this).children().children().toggleClass("fa-caret-down");
            $(this).children().children().toggleClass("fa-caret-right");
        });
        var i = document.createElement('li');
        i.className = "fa fa-caret-down fa-2x";
        divl.appendChild(i);

        var divTematico = document.createElement('div');
        li.appendChild(divTematico);
        divTematico.style.paddingLeft = "25px";
        this.getSimbologia(divTematico, lyr, estilo);
    }
    else{
        this.getSimbologia(divl, lyr, estilo);
    }
    return li;
};

/**
 * Render all layers that are children of a group.
 * @private
 * @param {ol.layer.Group} lyr Group layer whos children will be rendered.
 * @param {Element} elm DOM element that children will be appended to.
 */
ol.control.GetLegend.prototype.renderLayers_ = function(lyr, elm) {
    var lyrs = lyr.getLayers().getArray().slice().reverse();
    for (var i = 0, l; i < lyrs.length; i++) {
        l = lyrs[i];
        if(l.get("ignorame")) continue;
        if(l.getLayers){
            this.renderLayers_(l, elm);
        }
        else{
            if(l.get('title') && l.getVisible() && l.get("type") != "base") {
                this.countActives ++;
                elm.appendChild(this.renderLayer_(l, i));
            }
        }
    }
};

/**
 * Generate a UUID
 * @returns {String} UUID
 *
 * Adapted from http://stackoverflow.com/a/2117523/526860
 */
ol.control.GetLegend.uuid = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}

/**
 * @private
 * @desc Apply workaround to enable scrolling of overflowing content within an
 * element. Adapted from https://gist.github.com/chrismbarr/4107472
 */
ol.control.GetLegend.enableTouchScroll_ = function(elm) {
    var supportsPassive = false;
    try {
        document.addEventListener("test", null, { get passive() { supportsPassive = true }});
    } catch(e) {}
    if(ol.control.LayerSwitcher.isTouchDevice_()){
        var scrollStartPos = 0;
        elm.addEventListener("touchstart", function(event) {
            // event.preventDefault();
            scrollStartPos = this.scrollTop + event.touches[0].pageY;
        }, supportsPassive ? { passive: true } : false);
        elm.addEventListener("touchmove", function(event) {
            // event.preventDefault();
            this.scrollTop = scrollStartPos - event.touches[0].pageY;
        }, supportsPassive ? { passive: true } : false);
    }
};

/**
 * @private
 * @desc Determine if the current browser supports touch events. Adapted from
 * https://gist.github.com/chrismbarr/4107472
 */
ol.control.GetLegend.isTouchDevice_ = function() {
    try {
        document.createEvent("TouchEvent");
        return true;
    } catch(e) {
        return false;
    }
};
