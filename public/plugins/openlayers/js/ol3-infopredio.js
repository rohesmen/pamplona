/**
 * OpenLayers 3 Layer Switcher Control.
 * See [the examples](./examples) for usage.
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object} opt_options Control options, extends olx.control.ControlOptions adding:
 *                              **`tipLabel`** `String` - the button tooltip.
 */
ol.control.InfoPredio = function(opt_options) {
    var options = opt_options || {};

    var tipLabel = options.tipLabel ?
        options.tipLabel : 'Información';

    this.mapListeners = [];

    this.hiddenClassName = 'ol-unselectable ol-control layer-info-predio there-can-be-only-one';
    this.shownClassName = this.hiddenClassName + ' activate';

    var element = document.createElement('div');
    element.setAttribute('data-status', 'deactivate');
    element.className = this.hiddenClassName;

    var button = document.createElement('button');
    button.innerHTML = '<i class="fa fa-home"></i>';
    button.setAttribute('title', tipLabel);
    element.appendChild(button);

    var this_ = this;

    button.onclick = function(e) {
        e = e || window.event;
        var button = e.target;
        var status = this_.element.getAttribute("data-status");
        if(status == "deactivate"){
            this_._activteInfo();
        }
        else{
            this_._deactivateInfo();
        }
        e.preventDefault();
    };

    onlyOne = function(){
        var status = this.getAttribute("data-status");
        if(status == "activate"){
            this_._deactivateInfo();
        }
    };

    $(button.parentNode).on('geo:there-can-be-only-one', onlyOne);

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};

ol.inherits(ol.control.InfoPredio, ol.control.Control);

ol.control.InfoPredio.prototype.show = function(coordinate, center) {
    if(coordinate != null){
        var this_ = this;
        if(status == "deactivate"){
            this_._activteInfo();
        }
        initMarkerPerson(coordinate, center == true? true : false);
    }
};

ol.control.InfoPredio.prototype._onclick = function(e) {

    console.log(e.coordinate);
    infoPredio.show(e.coordinate)
};


/**
 * Activar herramienta de info.
 */
ol.control.InfoPredio.prototype._activteInfo = function() {
    this.element.setAttribute('data-status', 'activate');
    $(".there-can-be-only-one").not(this.element).trigger("geo:there-can-be-only-one");
    if (this.element.className != this.shownClassName) {
        this.element.className = this.shownClassName;
    }
    this._mapClickEvent = this.getMap().on("click", this._onclick);
    //this._map = this.getMap();
    //var elMap = this.getMap().getTargetElement();
    //$(elMap).on("mousemove", this._mouseEvent);
};

/**
 * desactivar herramienta de info.
 */
ol.control.InfoPredio.prototype._deactivateInfo = function() {
    this.element.setAttribute('data-status', 'deactivate');
    if (this.element.className != this.hiddenClassName) {
        this.element.className = this.hiddenClassName;
    }

    $("#predio-info" ).remove();
    clearMarkerPerson();
    this.getMap().unByKey(this._mapClickEvent);
    //var elMap = this.getMap().getTargetElement();
    //$(elMap).off("mousemove", this._mouseEvent);
};