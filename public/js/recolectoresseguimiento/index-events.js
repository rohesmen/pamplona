
$(document).ready(function () {

    $("#btneditimg").on("click", function(){
        $("#fileUploadFoto").click();
    });

    $("#fileUploadFoto").on("change", function (e){
        console.log(e);
        let file = e.target.files;
        var fileReader = new FileReader();
        fileReader.onload = function () {
            var imgbase64 = fileReader.result; // data <-- in this var you have the file data in Base64 format
            setImages(imgbase64);
        };
        if(file.length > 0){
            fileReader.readAsDataURL(file[0]);
        }
    });

    $(".filtro-recolectoresseguimiento-clear-do").off("click");
    $(".filtro-recolectoresseguimiento-clear-do").on("click", function(){
        $(this).next().val("");
    });
    
    $('.Filt').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            $('#recolectoresseguimiento-search-do').click();
        }
    });    

    $("#recolectoresseguimiento-search-do").on("click", function () {
        recolectoresseguimientoDT.column(4).search($("#fnombre").val().trim());
        recolectoresseguimientoDT.column(5).search($("#fapepat").val().trim());
        recolectoresseguimientoDT.column(6).search($("#fapemat").val().trim());
        recolectoresseguimientoDT.column(9).search($("#fvigente").val());
        recolectoresseguimientoDT.draw();
    });

    $("#addrecolectoresseguimientoDo").on("click", function () {
        $("#recolectoresseguimiento-info-modal").modal("show");
    });

    $("#btnGuardar").on("click", function () {
        var error = "";
        var id = $("#id").val();
        var nombre = $("#txtNombre").val().trim();
        var apepat = $("#txtApepat").val().trim();
        var apemat = $("#txtApemat").val().trim();
        if(nombre == ""){
            error += "- El nombre no puede estar vacio<br>";
        }
        if(apepat == ""){
            error += "- El apellido paterno no puede estar vacio<br>";
        }
        if(error != ""){
            showGeneralMessage(error, "warning", true);
            return
        }
        var data = {
            id: id ? id : null,
            nombre: nombre,
            apepat: apepat,
            apemat: apemat
        }
        let formData = new FormData();
        let file = null;
        if($('input#fileUploadFoto')[0].files.length > 0){
            file = $('input#fileUploadFoto')[0].files[0];
        }
        formData.append('file', file);
        formData.append('data', JSON.stringify(data));
        $.ajax({
            url: "/recolectoresseguimiento/save",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $("#btnGuardar").prop("disabled", true);
            },
            success: function () {
                showGeneralMessage("La información se guardo correctamente", "success", true);
                recolectoresseguimientoDT.draw(false);
                $("#recolectoresseguimiento-info-modal").modal("hide");
            },
            complete: function () {
                $("#btnGuardar").prop("disabled", false);
            }
        })
    });

    $("#recolectoresseguimiento-info-modal").on("hidden.bs.modal", function () {
        $("#recolectoresseguimiento-info-modal .modal-title").html("Nuevo Recolector");
        $("#id").val("");
        setImages("/img/usuario.png");
        $(".txtDis").val("");
        $("#fileUploadFoto").val("");
        $(".txtDis").prop("disabled", false);
        $("#fileUploadFoto").prop("disabled", false);
        $("#btnGuardar").prop("disabled", false);
    });

    $("#recolectoresseguimiento-datatable").on('click', '.recolectoresseguimiento-edit', function () {
        $("#recolectoresseguimiento-info-modal .modal-title").html("Editar Recolector");
        var $tr = $(this).closest("tr");
        var data = recolectoresseguimientoDT.row($tr).data();
        $("#id").val(data[3]);
        $("#txtNombre").val(data[4]);
        $("#txtApepat").val(data[5]);
        $("#txtApemat").val(data[6]);
        if(data[10] != "" && data[10] != null && data[11] != "" && data[11] != null){
            setImages(data[11]+data[10]);
        }
        $("#recolectoresseguimiento-info-modal").modal("show");
    });

    $("#recolectoresseguimiento-datatable").on( 'click', '.recolectoresseguimiento-info', function () {
        $("#recolectoresseguimiento-info-modal .modal-title").html("Consultar Recolector");
        var $tr = $(this).closest("tr");
        var data = recolectoresseguimientoDT.row($tr).data();
        $("#id").val(data[3]);
        $("#txtNombre").val(data[4]);
        $("#txtApepat").val(data[5]);
        $("#txtApemat").val(data[6]);
        if(data[10] != "" && data[10] != null && data[11] != "" && data[11] != null){
            setImages(data[11]+data[10]);
        }
        $(".txtDis").prop("disabled", true);
        $("#fileUploadFoto").prop("disabled", true);
        $("#btnGuardar").prop("disabled", true);
        $("#recolectoresseguimiento-info-modal").modal("show");
    });

    $("#recolectoresseguimiento-datatable").on( 'click', '.recolectoresseguimiento-delete', function () {
        activate($(this), false);
    });

    $("#recolectoresseguimiento-datatable").on( 'click', '.recolectoresseguimiento-active', function () {
        activate($(this), true);
    });

    recolectoresseguimientoDT = $("#recolectoresseguimiento-datatable").DataTable({
        "dom": '<"clear"><"top container-float"<"filter-located">f><"datatable-scroll"rt><"bottom"lip><"clear">',
        "autoWidth": false,
        "paging": true,
        "select": true,
        "info": true,
        //searching: true,
        "processing": true,
        "scrollCollapse": false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
        "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
        "pageLength": 10,
        "order": [[ 3, 'asc' ]],
        responsive: {
            details: {
                type: 'column',
                target: 1
            }
        },
        "processing": true,
        "serverSide": true,
        //"deferLoading": 0,
        ajax: {
            url: "/recolectoresseguimiento/buscar",
        },
        //responsive: true,
        "columnDefs": [
            {
                targets: [0],
                orderable: false,
                searchable: false,
                className: 'dt-center no-wrap',
                visible: coacciones != 'no'
            },
            { targets: [2,3,7,8], className: 'dt-center no-wrap'},
            { targets: [1], orderable: false, className: 'control', searchable: false},
            { targets: [-1,-2,-3], visible: false },
            { targets: '_all', visible: true }
        ],
        "lengthChange": true
    });

});

function activate($this, activo){
    $(".popover.confirmation").confirmation("destroy");
    var id = $this.data("id");
    $this.confirmation({
        rootSelector: "body",
        container: "body",
        singleton: true,
        popout: true,
        btnOkLabel: "Si",
        onConfirm: function() {
            $.ajax({
                url: "/recolectoresseguimiento/delete/" + id + "/" + activo,
                success: function () {
                    showGeneralMessage("La información se guardo correctamente", "success", true);
                    recolectoresseguimientoDT.draw(false);
                }
            });
        },
        onCancel: function() {
            $this.confirmation('destroy');
        },
    });
    $this.confirmation("show");
}

function setImages(image){
    $("#imagenrecolector").attr('src', image);
    $("#imagenrecolectorhref").attr('href', image);
}
