$(document).ready(function(){
    $("#chgmens-monto").inputmask({ "mask": "9", "repeat": 4, "greedy": false});
    $("#clientes-chgmens").on("click", function () {
        var idcliente = $(this).data("idcliente");
        $("#chgmens-idcliente").val(idcliente);
        $("#chgmens-monto").val("");
        $("#chgmens-motivo").val("");
        $("#cliente-chgmens-modal").modal("show");
    });
    
    $("#chgmens-btnGuardar").on("click", function () {
        var idcliente = $("#chgmens-idcliente").val();
        var monto = $("#chgmens-monto").val();
        var motivo = $("#chgmens-motivo").val().trim();
        var errores = '';
        if(monto == "") {
            errores += '- Monto es obligatorio<br>';
        }
        if(monto != "" && parseInt(monto) === 0){
            errores += '- Monto debe ser mayor 0<br>';
        }
        if(motivo == ""){
            errores += '- Motivo es obligatorio'
        }
        if(errores){
            showGeneralMessage(errores, 'warning', true)
            return;
        }
        var data = {
            monto: monto,
            motivo: motivo
        }
        $.ajax({
            url: '/clientes/clichgmens/' + idcliente,
            type: "POST",
            data: JSON.stringify(data),
            beforeSend: function(){
                $("#chgmens-btnGuardar").prop("disabled", true);
                $("#chgmens-btnGuardar").html('<i class="fa fa-spinner fa-spin"></i> Espere...');
                $("#chgmens-btnGuardar").prev().prop("disabled", true);
            },
            success: function () {
                buscar();
                showGeneralMessage('La información se guardo correctamente', 'success', true);
                $("#cliente-chgmens-modal").modal("hide");
            },
            error: function () {
                showGeneralMessage("Ocurrió un error al guardar la información", "danger", true);
            },
            complete: function () {
                $("#chgmens-btnGuardar").prop("disabled", false);
                $("#chgmens-btnGuardar").html('Guardar');
                $("#chgmens-btnGuardar").prev().prop("disabled", false);
            }
        })
    });
});