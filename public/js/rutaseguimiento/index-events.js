
//var deleterutarecoleccion = [];
FILEXML = null;
CENTER_MAP_LAT = 21.02394395, CENTER_MAP_LON = -89.64514160,initZoom = 13;
map = null;
$(document).ready(function () {

    initAllMap();
    $("#fileUploadKML").on("change", function (){
        clearDataMap();
        //console.log(this);
        var fileReader = new FileReader();
        fileReader.onload = function () {
            var xmlStr = fileReader.result;  // data <-- in this var you have the file data in Base64 format
            $("#textKML").val(xmlStr);
            const parser = new DOMParser();
            const xmlDoc  = parser.parseFromString(xmlStr, "text/xml");
            dataGeojson = toGeoJSON.kml(xmlDoc);
            $("#textGeojson").val(JSON.stringify(dataGeojson));
            addGeoJsonToMap(dataGeojson);
            //console.log(toGeoJSON.kml(xmlDoc));
        };

        if($('input#fileUploadKML').val()){
            FILEXML = $('input#fileUploadKML')[0].files[0];
            fileReader.readAsText($('input#fileUploadKML')[0].files[0], "UTF-8");
            undefined
        }
        else{

        }

    });

    /*selectDia = $("#cmbDia").selectize({
        render: {
            option: function (data, escape) {
                //console.log(data);
                return "<div data-orden='" + escape(data.orden) + "'>" + escape(data.text) + "</div>"
            }
        }
    });
    selectizeDia = selectDia[0].selectize;
    selectizeDia.setValue("", false);

    selectTipo = $("#cmbTipo").selectize();
    selectizeTipo = selectTipo[0].selectize;
    selectizeTipo.setValue("", false);

    selectTurno = $("#cmbTurno").selectize();
    selectizeTurno = selectTurno[0].selectize;
    selectizeTurno.setValue("", false);*/

    $(".filtro-rutaseguimiento-clear-do").off("click");
    $(".filtro-rutaseguimiento-clear-do").on("click", function(){
        $(this).next().val("");
    });
    
    $('.Filt').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            $('#rutaseguimiento-search-do').click();
        }
    });    

    $("#rutaseguimiento-search-do").on("click", function () {
        rutaseguimientoDT.column(4).search($("#frutaseguimiento").val().trim());
        rutaseguimientoDT.column(7).search($("#fvigente").val());
        rutaseguimientoDT.draw();
    });

    $("#addRutaSeguimientoDo").on("click", function () {
        $("#rutaseguimiento-info-modal").modal("show");
    });

    $("#btnGuardar").on("click", function () {
        var id = $("#id").val();
        var nombre = $("#txtNombre").val().trim();
        var descripcion = $("#txtDescripcion").val().trim();
        var error = "";
        if(nombre == ""){
            error += "- El nombre no puede estar vacio<br>";
        }
        /*if(dataTableRutaRecoleccion.rows().count() == 0){
            error += "- Ingrese al menos una ruta de recoleccion";
        }*/
        if(error != ""){
            showGeneralMessage(error, "warning", true);
            return
        }
        /*$.each(dataTableRutaRecoleccion.data(), function (index, value) {
            deleterutarecoleccion.push(value);
        });*/
        //console.log(deleterutarecoleccion);
        var data = {
            id: id ? id : null,
            nombre: nombre,
            descripcion: descripcion,
            //rutarecoleccion: deleterutarecoleccion,
            geoJson: $("#textGeojson").val() ? $("#textGeojson").val() : null,
            kml: $("#textKML").val() ? $("#textKML").val() : null,
            sizexml: FILEXML ? FILEXML.size : null,
            namexml: FILEXML ? FILEXML.name : null,
        }
        $.ajax({
            url: "/rutaseguimiento/save",
            type: "POST",
            data: JSON.stringify(data),
            beforeSend: function(){
                $("#btnGuardar").prop("disabled", true);
            },
            success: function () {
                showGeneralMessage("La información se guardo correctamente", "success", true);
                rutaseguimientoDT.draw(false);
                $("#rutaseguimiento-info-modal").modal("hide");
            },
            complete: function () {
                $("#btnGuardar").prop("disabled", false);
            }
        })
    });

    /*$("#create_ruta_recoleccion").on("click", function () {
        var dia = $("#cmbDia").val().trim();
        var tipo = $("#cmbTipo").val().trim();
        var turno = $("#cmbTurno").val().trim();
        var error = "";
        if(dia == "" || dia == null){
            error += "- El día no puede estar vacio<br>";
        }
        if(tipo == "" || tipo == null){
            error += "- El tipo no puede estar vacio<br>";
        }
        if(turno == "" || turno == null){
            error += "- El turno no puede estar vacio<br>";
        }
        if(error != ""){
            showGeneralMessage(error, "warning", true);
            return
        }
        let add = true;
        $.each(dataTableRutaRecoleccion.rows().data(), function (index, value) {
            if(value[5] == dia && value[6] == tipo && value[7] == turno){
                add = false
                return false;
            }
        });
        if(!add){
            showGeneralMessage("Ya existe la ruta de recolección.", "warning", true);
            return
        }
        let btn = '<button class="btn btn-sm btn-danger delete_ruta_recoleccion" data-id="-1" title="¿Desea eliminar?"><i class="fa fa-remove"></i></button>';
        let getDia = selectizeDia.options[dia];
        let orden = 0;
        if(getDia){
            orden = parseInt(getDia.orden);
        }
        let array = ["", btn, $("#cmbDia option:selected").text(), $("#cmbTipo option:selected").text(), $("#cmbTurno option:selected").text(), dia, tipo, turno, true, -1, orden];
        //console.log(array);
        dataTableRutaRecoleccion.row.add(array).draw();
    });*/

    $("#rutaseguimiento-info-modal").on("hidden.bs.modal", function () {
        $("#rutaseguimiento-info-modal .modal-title").html("Nueva Ruta");
        $("#id").val("");
        $(".txtDis").val("");
        $("#fileUploadKML").val("");
        /*selectizeDia.setValue("", false);
        selectizeTipo.setValue("", false);
        selectizeTurno.setValue("", false);*/
        $(".txtDis").prop("disabled", false);
        $("#fileUploadKML").prop("disabled", false);
        //$("#create_ruta_recoleccion").prop("disabled", false);
        //$(".delete_ruta_recoleccion").prop("disabled", false);
        $("#btnGuardar").prop("disabled", false);
        /*selectizeDia.enable();
        selectizeTipo.enable();
        selectizeTurno.enable();
        deleterutarecoleccion = [];
        dataTableRutaRecoleccion.clear().draw();*/

        $("#textKML").val("");
        $("#textGeojson").val("");
        $('#fileUploadKML').val("");
        FILEXML = null;
        clearDataMap();
    });

    $("#rutaseguimiento-info-modal").on("shown.bs.modal", function () {
        if(!map){
            initMap();
        }
    });

    $("#rutaseguimiento-datatable").on('click', '.rutaseguimiento-edit', function () {
        $("#rutaseguimiento-info-modal .modal-title").html("Editar Ruta");
        var $tr = $(this).closest("tr");
        var data = rutaseguimientoDT.row($tr).data();
        $("#id").val(data[3]);
        $("#txtNombre").val(data[4]);
        $("#txtDescripcion").val(data[9]);
        loadcollectionroute(data[3], false);
        $("#rutaseguimiento-info-modal").modal("show");
        $("#textGeojson").val(JSON.parse(data[8]));
    });

    $("#rutaseguimiento-datatable").on( 'click', '.rutaseguimiento-info', function () {
        $("#rutaseguimiento-info-modal .modal-title").html("Consultar Ruta");
        var $tr = $(this).closest("tr");
        var data = rutaseguimientoDT.row($tr).data();
        $("#id").val(data[3]);
        $("#txtNombre").val(data[4]);
        $("#txtDescripcion").val(data[9]);
        loadcollectionroute(data[3], true);
        $(".txtDis").prop("disabled", true);
        $("#fileUploadKML").prop("disabled", true);
        $("#btnGuardar").prop("disabled", true);
        $("#textGeojson").val(JSON.parse(data[8]));
        /*$("#create_ruta_recoleccion").prop("disabled", true);
        selectizeDia.disable();
        selectizeTipo.disable();
        selectizeTurno.disable();*/
        $("#rutaseguimiento-info-modal").modal("show");
        $("#rutaseguimiento-info-modal").modal("show");
    });

    $("#rutaseguimiento-datatable").on( 'click', '.rutaseguimiento-delete', function () {
        activate($(this), false);
    });

    $("#rutaseguimiento-datatable").on( 'click', '.rutaseguimiento-active', function () {
        activate($(this), true);
    });

    rutaseguimientoDT = $("#rutaseguimiento-datatable").DataTable({
        "dom": '<"clear"><"top container-float"<"filter-located">f><"datatable-scroll"rt><"bottom"lip><"clear">',
        "autoWidth": false,
        "paging": true,
        "select": true,
        "info": true,
        //searching: true,
        "processing": true,
        "scrollCollapse": false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
        "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
        "pageLength": 10,
        "order": [[ 3, 'asc' ]],
        responsive: {
            details: {
                type: 'column',
                target: 1
            }
        },
        "processing": true,
        "serverSide": true,
        //"deferLoading": 0,
        ajax: {
            url: "/rutaseguimiento/buscar",
        },
        //responsive: true,
        "columnDefs": [
            {
                targets: [0],
                orderable: false,
                searchable: false,
                className: 'dt-center no-wrap',
                visible: coacciones != 'no'
            },
            { targets: [2,3,5,6], className: 'dt-center no-wrap'},
            { targets: [1], orderable: false, className: 'control', searchable: false},
            { targets: [-1,-2,-3,-4], visible: false },
            { targets: '_all', visible: true }
        ],
        "lengthChange": true,
        "drawCallback": function( settings ) {
            let  callback = function (feature) {
                // If you want, check here for some constraints.
                allmap.data.remove(feature);
            };
            if(allmap) {
                allmap.data.forEach(callback);
            }
            var api = this.api();
            api.columns().columns.adjust();
            api.columns().columns.adjust();
            let data = api.data();
            let cgeojson = 0;
            for(const d in data){
                let aux = data[d];
                let geojson = aux[8];
                if(geojson){
                    cgeojson += 1;
                    let auxGeoSjon = JSON.parse(geojson)
                    allmap.data.addGeoJson(auxGeoSjon);

                }
            }
            if(cgeojson > 0){
                //infoWindow.close();
                allmap.data.setStyle((feature) => {
                    return /** @type {google.maps.Data.StyleOptions} */ {
                        fillColor: feature.getProperty("fill"),
                        strokeColor: feature.getProperty("stroke"),
                        strokeWeight: 1,
                    };
                });
                allmap.data.addListener("mouseover", (event) => {
                    let contenidoinfo = event.feature.getProperty("nombre") ?
                        event.feature.getProperty("nombre") : '';
                    infoWindow.setContent(contenidoinfo);
                    infoWindow.setPosition(event.latLng);
                    infoWindow.setOptions({
                        pixelOffset: new google.maps.Size(0, 0)
                    });

                    infoWindow.open(allmap);
                });
                var bounds = new google.maps.LatLngBounds();
                allmap.data.forEach(function(feature){
                    feature.getGeometry().forEachLatLng(function(latlng){
                        bounds.extend(latlng);
                    });
                });

                allmap.fitBounds(bounds);
            }
        }
    });

    /*dataTableRutaRecoleccion = $('#ruta_recoleccion-datatable').DataTable({
        dom: 'rt<"row"<"col-md-6 col-sm-6"i><"col-md-6 col-sm-6"p>>',
        pageLength: -1,
        paging: false,
        info: false,
        responsive: {
            details: {
                type: 'column',
                target: 0
            }
        },
        columnDefs: [
            { targets: 0, className: 'control', orderable: false, searchable: false },
            { targets: 1, orderable: false, searchable: false, className: 'dt-center no-wrap', visible: deleterutrec },
            { targets: [-1,-2,-3,-4,-5,-6], visible: false },
            { targets: [10], orderable: true },
            { targets: '_all', visible: true, orderable: false, searchable: true }
        ],
        language: {
            url: "../../plugins/datatables_new/language.MX.json"
        },
        "order": [
            [10, "asc"]
        ],
    });*/

    /*$("#ruta_recoleccion-datatable").on( 'click', '.delete_ruta_recoleccion', function () {
        $(".popover.confirmation").confirmation("destroy");
        var $btn = $(this);
        var $tr = $(this).closest("tr");
        var id = $(this).attr("data-id");
        //console.log($tr);
        $(this).confirmation({
            rootSelector: "#rutaseguimiento-info-modal",
            container: "#rutaseguimiento-info-modal",
            singleton: true,
            popout: true,
            btnOkLabel: "Si",
            onConfirm: function() {
                //console.log("eliminar");
                let row = dataTableRutaRecoleccion.row($(this).parents('tr'));
                let data = row.data();
                //console.log(data);
                if(parseInt(data[9]) > 0){
                    data[8] = false;
                    deleterutarecoleccion.push(data);
                }
                row.remove().draw();
            },
            onCancel: function() {
                $(this).confirmation('destroy');
            },
        });
        $(this).confirmation("show");
    });*/

    /*$("#rutaseguimiento-info-modal").on("shown.bs.modal", function () {
        dataTableRutaRecoleccion.columns.adjust();
    });*/

});

function clearDataMap(){
    var callback = function (feature) {
        // If you want, check here for some constraints.
        map.data.remove(feature);
    };
    if(map) {
        map.data.forEach(callback);
    }
}
function initMap(){
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: CENTER_MAP_LAT, lng: CENTER_MAP_LON},
        zoom: initZoom,
        disableDefaultUI: true,
        keyboardShortcuts: false,
//                draggable: false,
//                disableDoubleClickZoom: true,
        scrollwheel: true,
        streetViewControl: true,
        mapTypeControl: true,
        scaleControl: true,
        zoomControl: true,
    });
}

function initAllMap(){
    allmap = new google.maps.Map(document.getElementById('allmap'), {
        center: {lat: CENTER_MAP_LAT, lng: CENTER_MAP_LON},
        zoom: initZoom,
        disableDefaultUI: true,
        keyboardShortcuts: false,
//                draggable: false,
//                disableDoubleClickZoom: true,
        scrollwheel: true,
        streetViewControl: true,
        mapTypeControl: true,
        scaleControl: true,
        zoomControl: true,
    });
    infoWindow = new google.maps.InfoWindow();
}

function activate($this, activo){
    $(".popover.confirmation").confirmation("destroy");
    var id = $this.data("id");
    $this.confirmation({
        rootSelector: "body",
        container: "body",
        singleton: true,
        popout: true,
        btnOkLabel: "Si",
        onConfirm: function() {
            $.ajax({
                url: "/rutaseguimiento/delete/" + id + "/" + activo,
                success: function () {
                    showGeneralMessage("La información se guardo correctamente", "success", true);
                    rutaseguimientoDT.draw(false);
                }
            });
        },
        onCancel: function() {
            $this.confirmation('destroy');
        },
    });
    $this.confirmation("show");
}

function loadcollectionroute(id, disable){
    $.ajax({
        url: '/rutaseguimiento/get/'+id,
        beforeSend: function(){
            //dataTableRutaRecoleccion.clear().draw();
            clearDataMap();
        },
        success: function (resp) {
            var json = JSON.parse(resp);
            //dataTableRutaRecoleccion.rows.add(json.days).draw();
            //$(".delete_ruta_recoleccion").prop("disabled", disable);
            if(json.ruta.geojson){
                const ruta = json.ruta;
                const geojson = JSON.parse(ruta.geojson);
                addGeoJsonToMap(geojson);
                const myFile = new File(['Hello World!'], ruta.nombre_archivo, {
                    type: 'text/xml',
                    lastModified: new Date(),
                });
                const fileInput = $("#fileUploadKML")[0];
                const dataTransfer = new DataTransfer();
                dataTransfer.items.add(myFile);
                fileInput.files = dataTransfer.files;

                $("#textKML").val(ruta.kml);
                $("#textGeojson").val(ruta.geojson);

                $("#txtXML").val(ruta.kml);
                FILEXML = {
                    name: ruta.nombre_archivo,
                    size: ruta.size_archivo
                }
            }
        }
    });
}

function addGeoJsonToMap(dataGeojson){
    map.data.addGeoJson(dataGeojson);
    map.data.setStyle({
        strokeColor: 'blue',
        strokeWeight: 2
    });
    var bounds = new google.maps.LatLngBounds();
    map.data.forEach(function(feature){
        feature.getGeometry().forEachLatLng(function(latlng){
            bounds.extend(latlng);
        });
    });

    map.fitBounds(bounds);
}