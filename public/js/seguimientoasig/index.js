unidadAsigada = null;
var isExtern = true;
var adddeletecuadrillarecoleccion = [];
$(document).ready(function (){

    selectRecolectorAsig = $("#addCmbRecolector").selectize();
    RecolectorAsig = selectRecolectorAsig[0].selectize;
    RecolectorAsig.setValue("", false);

    $('.badge').on('click', function(){
        //modulo de cuadrillas
        let dataAux = {
            id: $(this).data("idcuadrilla"),
            nombre: $(this).data("nombrecuadrilla")
        };
        loadCUADSEG(dataAux, false);
    });
    $('.btnConsultarUnidad').on('click', function(){
        printUnidades($(this).data("placa"));
    });
   $(".UnidadDraggable").draggable({
       revert: true,
       //containment: '.wrapper-content'
   });
   $(".droppableDias").droppable({
        classes: {
            "ui-droppable-hover": "ui-state-hover"
        },
        drop: function( event, ui ) {
            let placa = ui.draggable.data("placa");
            let unidad = unidades[placa];
            let cuadrillas = unidad.cuadrilla;
            let $this = $(this);
            let isPlaca = $this.data("placa");
            let ruta = $this.data("ruta");
            let idruta = $this.data("idruta");
            let sigladia = $this.data("sigladia");
            let dia = $this.data("dia");
            let id = $this.data("id");
            let idturno = $this.data("idturno");
            $("#addCmbCuadrilla").html('<option value="">Seleccionar</option>');
            for(const i in cuadrillas){
                let cuad = cuadrillas[i].data;
                $("#addCmbCuadrilla").append('<option value="' + cuad.id +'">' + cuad.nombre +'</option>');
            }

            $("#addIDTD").val($this.attr("id"));

            $("#addID").val(id);
            $("#addDia").val(sigladia);
            $("#addIdRuta").val(idruta);
            $("#addIdCuadrilla").val("");
            $("#addTextDia").val(dia);
            $("#addTextRuta").val(ruta);
            $("#addPlaca").val(placa);
            $("#addCmbTurno").val(idturno);
            if(isPlaca){
                let confirmationACR = bootbox.confirm({
                    title: "Cambio de unidad",
                    message: "Ya existe una unidad asignada.<br>¿Desea cambiar la unidad?",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> NO'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> SI'
                        }
                    },
                    callback: function (result) {
                        $(".bootbox-confirm").on('hidden.bs.modal', function () {
                            if(isABootstrapModalOpen()){
                                $("body").addClass("modal-open");
                            }
                            $(".bootbox-confirm").off('hidden.bs.modal');
                        });
                        if (result) {
                            $("#recolectoresseguimiento-info-modal .modal-title").html("Crear asignación");
                            unidadAsigada = unidad;
                            $("#modal-asignacion").modal({
                                show: true,
                                backdrop: "static"
                            });
                        }
                    }
                });
                confirmationACR.css("z-index","1051");
                confirmationACR.next().css("z-index","1050");
            }
            else {
                $("#modal-asignacion").modal({
                    show: true,
                    backdrop: "static"
                });
            }
        }
    });

   $("body").on('click', '.delete-asignacion-unidad', function (){
       let $thisbtn = $(this);
       let confirmationACR = bootbox.confirm({
           title: "Desactivar recolección",
           message: "¿Desea desactivar la asignación de recolección?",
           buttons: {
               cancel: {
                   label: '<i class="fa fa-times"></i> NO'
               },
               confirm: {
                   label: '<i class="fa fa-check"></i> SI'
               }
           },
           callback: function (result) {
               $(".bootbox-confirm").on('hidden.bs.modal', function () {
                   if(isABootstrapModalOpen()){
                       $("body").addClass("modal-open");
                   }
                   $(".bootbox-confirm").off('hidden.bs.modal');
               });
               if (result) {
                   let $tr = $thisbtn.closest('td');
                   $.ajax({
                       url: '/seguimientoasig/deactivate/' + $thisbtn.data("id"),
                       beforeSend: function (){
                           $thisbtn.html('<i class="fa fa-spin fa-spinner"></i>');
                           $thisbtn.prop('disabled', true);
                       },
                       success: function (){
                           $tr.data('placa', '');
                           $tr.data('idcuadrilla', '');
                           $tr.html("");
                           $tr.removeClass("programado");
                           showGeneralMessage('La información se guardo correctamente', 'success', true);
                       },
                       error:function (resp){
                           showGeneralMessage('No se pudo desactivar la asignación', 'error', true);
                       },
                       complete: function (){
                           $thisbtn.html('<i class="fa fa-remove"></i>');
                           $thisbtn.prop('disabled', false);
                       }
                   })

               }
           }
       });
       confirmationACR.css("z-index","1051");
       confirmationACR.next().css("z-index","1050");
   });

   $("#btnGuardar").on("click", function (){
        let $btn = $(this);
        let idcuadrilla = $("#addCmbCuadrilla").val();
        let idturno = $("#addCmbTurno").val();
        let idruta = $("#addIdRuta").val();
        let id = $("#addID").val();
        let placa = $("#addPlaca").val();
        let dia = $("#addDia").val();
        let $tr = $("#" + $("#addIDTD").val());
        let error = "";
        if(adddataTableCuadrillaRecoleccion.rows().count() == 0){
            error += "- Ingrese al menos un recolector<br>";
        }
        let ischofer = false;
        $.each(adddataTableCuadrillaRecoleccion.data(), function (index, value) {
            if(value[5] == true || value[5] == "true"){
                ischofer = true;
                return false;
            }
        });
        if(!ischofer){
            error += "- Ingrese un chofer";
        }
        if(error != ""){
            showGeneralMessage(error, "warning", true);
            return
        }
        if(idcuadrilla && idturno){
            $.each(adddataTableCuadrillaRecoleccion.data(), function (index, value) {
                adddeletecuadrillarecoleccion.push(value);
            });
            const data = {
                id: id ? id: null,
                idruta: idruta,
                placa: placa,
                dia: dia,
                idturno: idturno,
                idcuadrilla: idcuadrilla,
                cuadrillarecolectorasignacion: adddeletecuadrillarecoleccion
            }
            $.ajax({
                url: '/seguimientoasig/create',
                type: 'POST',
                data: JSON.stringify(data),
                beforeSend: function (){
                    $("#modal-asignacion button.close").prop("disabled", true);
                    $btn.html('<i class="fa fa-spin fa-spinner"></i> Guardar');
                    $btn.prop("disabled", true);
                },
                success: function (html){
                    $("#modal-asignacion").modal("hide");
                    showGeneralMessage('La información se guardo correctamente', 'success', true);
                    $tr.html(html);
                    $tr.data("placa", placa);
                    $tr.data("cuadrilla", idcuadrilla);
                    $tr.addClass("programado");
                },
                error:function (resp){
                    showGeneralMessage('No se pudo desactivar la asignación', 'error', true);
                },
                complete: function (){
                    $("#modal-asignacion button.close").prop("disabled", false);
                    $btn.html('Guardar');
                    $btn.prop("disabled", false);
                }
            })
        }
        else{
            showGeneralMessage('Todos los campos marcados con * son obligatorios', 'warning', true);
        }
   });

    $("#modal-asignacion").on("hidden.bs.modal", function () {
        $("#recolectoresseguimiento-info-modal .modal-title").html("Crear asignación");
        $("#addID").val("");
        $("#addDia").val("");
        $("#addIdRuta").val("");
        $("#addIdCuadrilla").val("");
        $("#addTextDia").val("");
        $("#addTextRuta").val("");
        $("#addPlaca").val("");
        $("#addCmbTurno").val("");
        $("#addCmbCuadrilla").val("").change();
        $("#addchkIsChofer").prop("checked", false);
        adddeletecuadrillarecoleccion = [];
        adddataTableCuadrillaRecoleccion.clear().draw();
    });

    $("#addCmbCuadrilla").on("change", function(){
        adddeletecuadrillarecoleccion = [];
        adddataTableCuadrillaRecoleccion.clear().draw();
        setDataRecolectorCuadrilla("addCmbRecolector", this.value, "");
    });

    $("#addcreate_asignacion_recoleccion").on("click", function () {
        createcudrillarecolectorAsignacion();
    });

    adddataTableCuadrillaRecoleccion = $('#addcuadrilla_recoleccion-datatable').DataTable({
        dom: 'rt<"row"<"col-md-6 col-sm-6"i><"col-md-6 col-sm-6"p>>',
        pageLength: -1,
        paging: false,
        info: false,
        responsive: {
            details: {
                type: 'column',
                target: 0
            }
        },
        columnDefs: [
            { targets: 0, className: 'control', orderable: false, searchable: false },
            { targets: 1, orderable: false, searchable: false, className: 'dt-center no-wrap', visible: deletecuadrec },
            { targets: [-1,-2,-3,-4,-5], visible: false },
            { targets: [2], orderable: true },
            { targets: '_all', visible: true, orderable: false, searchable: true }
        ],
        language: {
            url: "../../plugins/datatables_new/language.MX.json"
        },
        "order": [
            [2, "asc"]
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).attr('id', data[4]);
        }
    });

    $("#addcuadrilla_recoleccion-datatable").on( 'click', '.adddelete_cuadrilla_recoleccion', function () {
        $(".popover.confirmation").confirmation("destroy");
        $(this).confirmation({
            rootSelector: "#modal-asignacion",
            container: "#modal-asignacion",
            singleton: true,
            popout: true,
            btnOkLabel: "Si",
            onConfirm: function() {
                //console.log("eliminar");
                let row = adddataTableCuadrillaRecoleccion.row($(this).parents('tr'));
                let data = row.data();
                //console.log(data);
                if(parseInt(data[7]) > 0){
                    data[6] = false;
                    adddeletecuadrillarecoleccion.push(data);
                }
                row.remove().draw();
            },
            onCancel: function() {
                $(this).confirmation('destroy');
            },
        });
        $(this).confirmation("show");
    });

    $("#modal-asignacion").on("shown.bs.modal", function () {
        adddataTableCuadrillaRecoleccion.columns.adjust();
    });

});

function createcudrillarecolectorAsignacion(){
    //console.log("!--------------------------------------------------------!");
    let idrecolector = $("#addCmbRecolector").val().trim();
    let recolector = $("#addCmbRecolector option:selected").text();
    let ischofer = $("#addchkIsChofer").is(":checked");
    //console.log(recolector + " - " + ischofer);
    var error = "";
    if(idrecolector == "" || idrecolector == null){
        error += "- El recolector no puede estar vacio<br>";
    }
    if(error != ""){
        showGeneralMessage(error, "warning", true);
        return
    }
    var add = true;
    $.each(adddataTableCuadrillaRecoleccion.rows().data(), function (index, value) {
        if(value[4] == idrecolector){
            add = false
            return false;
        }
    });
    if(!add){
        showGeneralMessage("- Ya existe el recolector.<br>", "warning", true);
    } else{
        $.each(adddataTableCuadrillaRecoleccion.rows().data(), function (index, value) {
            if(ischofer == true && value[5] == true){
                add = false
                return false;
            }
        });
        if(!add){
            let confirmationACR = bootbox.confirm({
                title: "Cambiar Chofer",
                message: "Ya existe recolector asignado como chofer,<br>¿Desea cambiar el chofer de la cuadrilla?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> NO'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> SI'
                    }
                },
                callback: function (result) {
                    $(".bootbox-confirm").on('hidden.bs.modal', function () {
                        if(isABootstrapModalOpen()){
                            $("body").addClass("modal-open");
                        }
                        $(".bootbox-confirm").off('hidden.bs.modal');
                    });
                    if (result) {
                        $.each(adddataTableCuadrillaRecoleccion.rows().data(), function (index, value) {
                            if(value[5] == true){
                                var data_editdt = adddataTableCuadrillaRecoleccion.row('#'+value[4]).data();
                                data_editdt[3] = "RECOLECTOR";
                                data_editdt[5] = false;
                                if(parseInt(data_editdt[7]) > -1){
                                    data_editdt[8] = true;
                                }
                                adddataTableCuadrillaRecoleccion.row('#'+value[4]).data(data_editdt);
                            }
                        });
                        adddataTableCuadrillaRecoleccion.draw(false);
                        crearcuadrillaasignacion(idrecolector, recolector, ischofer);
                    }
                }
            });
            confirmationACR.css("z-index","1051");
            confirmationACR.next().css("z-index","1050");
        } else{
            crearcuadrillaasignacion(idrecolector, recolector, ischofer);
        }
    }
    //console.log("¡--------------------------------------------------------¡");
}

function crearcuadrillaasignacion(idrecolector, recolector, ischofer){
    let btn = '<button class="btn btn-sm btn-danger adddelete_cuadrilla_recoleccion" data-id="-1" title="¿Desea eliminar?"><i class="fa fa-remove"></i></button>';
    let array = ["", btn, recolector, (ischofer == true ? "CHOFER" : "RECOLECTOR"), idrecolector, ischofer, true, -1, false];
    adddataTableCuadrillaRecoleccion.row.add(array).draw(false);
    RecolectorAsig.setValue("", false);
    if($("#addchkIsChofer").is(":checked")){
        $("#addchkIsChofer").prop("checked", false);
    }
}

function setDataRecolectorCuadrilla(cmb = "addCmbRecolector", id = "", idset = ""){
    var $select = $("#"+cmb);
    RecolectorAsig.destroy();
    if(id != ""){
        $.ajax({
            url: "/seguimientoasig/getRecolectoresCuadrilla/" + id,
            dataType: "json",
            success: function (resp) {
                console.log(resp);
                adddataTableCuadrillaRecoleccion.rows.add(resp.tabla).draw();
                selectRecolectorAsig = $($select).selectize({
                    maxItems: 1,
                    valueField: 'id',
                    labelField: 'text',
                    searchField: 'text',
                    options: resp.select,
                    create: false
                });
                RecolectorAsig = selectRecolectorAsig[0].selectize;
            },
            complete: function(){
                RecolectorAsig.setValue(idset, false);
            }
        });
    }else{
        selectRecolectorAsig = $($select).selectize({
            maxItems: 1,
            valueField: 'id',
            labelField: 'text',
            searchField: 'text',
            options: [{id: '', text: 'Seleccionar'}],
            create: false
        });
        RecolectorAsig = selectRecolectorAsig[0].selectize;
        RecolectorAsig.setValue("", false);
    }
}

function modificaunidades(json){
    $.each(unidades, function (index, value) {
        console.log(value);
        if(value.cuadrilla.length > 0){
            $.each(value.cuadrilla, function (index2, value2) {
                console.log(index2);
                console.log(value2);
                if(value2.data){
                    if(value2.data.id == json.data.id){
                        value.cuadrilla[index2] = json;
                    }
                }
            });
        }
    });
}

function printUnidades(placa){
    if(placa != "" && placa != null){
        $.ajax({
            url: '/seguimientoasig/consultaunidad/'+placa,
            beforeSend: function(){
            },
            success: function (resp) {
                $("#content-modal-unidades-consulta").html(resp);
                $("#unidadseguimiento-info-modal-UnidadMC").modal({keyboard: false, show: true, backdrop: 'static'});
            },
            complete: function(){
            },
            error: function(xhr, status, error){
                showGeneralMessage("Ocurrió un error, contacte al administrador", "warning", true);
            }
        });
    }
}
