L.Control.Info = L.Control.extend({
    options: {
        position: 'topright',
        prefix: '<a href="http://leafletjs.com" title="A JS library for interactive maps">Leaflet</a>'
    },

    initialize: function (options) {
        L.setOptions(this, options);

        this._attributions = {};
    },

    onAdd: function (map) {
        this._container = L.DomUtil.create('div', 'leaflet-control-info leaflet-topright-control');
        if (L.DomEvent) {
            L.DomEvent.disableClickPropagation(this._container);
        }

        this._container.insertAdjacentHTML('beforeend','<a class="leaflet-control-layers-toggle" href="#" title="Layers"></a>');
        //// TODO ugly, refactor
        //for (var i in map._layers) {
        //    if (map._layers[i].getAttribution) {
        //        this.addAttribution(map._layers[i].getAttribution());
        //    }
        //}
        //
        //this._update();

        return this._container;
    },

    setPrefix: function (prefix) {
        this.options.prefix = prefix;
        this._update();
        return this;
    },

    addAttribution: function (text) {
        if (!text) { return this; }

        if (!this._attributions[text]) {
            this._attributions[text] = 0;
        }
        this._attributions[text]++;

        this._update();

        return this;
    },

    removeAttribution: function (text) {
        if (!text) { return this; }

        if (this._attributions[text]) {
            this._attributions[text]--;
            this._update();
        }

        return this;
    }
    //
    //_update: function () {
    //    if (!this._map) { return; }
    //
    //    var attribs = [];
    //
    //    for (var i in this._attributions) {
    //        if (this._attributions[i]) {
    //            attribs.push(i);
    //        }
    //    }
    //
    //    var prefixAndAttribs = [];
    //
    //    if (this.options.prefix) {
    //        prefixAndAttribs.push(this.options.prefix);
    //    }
    //    if (attribs.length) {
    //        prefixAndAttribs.push(attribs.join(', '));
    //    }
    //
    //    this._container.innerHTML = prefixAndAttribs.join(' | ');
    //}
});

L.Map.mergeOptions({
    info: false
});

L.Map.addInitHook(function () {
    if (this.options.info) {
        this.info = (new L.Control.Info()).addTo(this);
    }
});

L.control.info = function (options) {
    return new L.Control.Info(options);
};