var rutasDT;
var buttons = "";
$(document).ready(function() {
    $("#rutas-colonia-val").selectize();
    coloniaForm = $("#colonias").selectize();
    usuarioForm = $("#usuarios").selectize();


    $(".ruta-filter-clear-do").off("click");
    $(".ruta-filter-clear-do").on("click", function(){
        $(this).next().val("");
    });


    $("#rutas-filter-select").off("change");
    $("#rutas-filter-select").on("change", function(){
        var valor = this.value;

        if(!$("#rutas-filtro-todos").hasClass("oculto"))
            $("#rutas-filtro-todos").toggleClass("oculto");

        if(!$("#rutas-filtro-clave").hasClass("oculto"))
            $("#rutas-filtro-clave").toggleClass("oculto");

        if(!$("#rutas-filtro-nombre").hasClass("oculto"))
            $("#rutas-filtro-nombre").toggleClass("oculto");

        if(!$("#rutas-filtro-colonia").hasClass("oculto"))
            $("#rutas-filtro-colonia").toggleClass("oculto");

        $("#rutas-nombre-val").val("");
        $("#rutas-colonia-val").val("");
        $("#rutas-clave-val").val("");


        if(valor == ""){
            $("#rutas-filtro-todos").toggleClass("oculto");
        }
        else if(valor == "nom"){
            $("#rutas-filtro-nombre").toggleClass("oculto");
        }
        else if(valor == "col"){
            $("#rutas-filtro-colonia").toggleClass("oculto");
        }
        else{
            $("#rutas-filtro-clave").toggleClass("oculto");
        }
    });

    $(".clientes-search-do").off('click');
    $(".clientes-search-val").off('keypress');
    $(".clientes-search-do").on('click', function(){

        fnBuscarRutas();
    });

    $(".cliente-search-val").on('keypress', function(e){
        if(e.which == 13){
            fnBuscarRutas();
        }
    });

    /**
     * Abre el modal para el formulario de Rutas
     */
    $("#openNewRuta").on('click', function() {

        $("#ruta-info-modal").modal({
            show: true,
            backdrop: "static"
        });
        $('.modal-title').html('Ruta Nueva');
        fnLimpiaForm();
        setModeRutaInfo('add');

    });

    $('#frmRuta').on('submit', function (event) {
        event.preventDefault();

        fnSaveRuta();

    });

    $("#btnGuardar").on('click', function() {

        $("#btnGuardaForm").click();
    });

});


function fnSaveRuta() {

    var data = {
        clave: $("#clave").val(),
        activo: $("#activo").is(':checked'),
        nombre: $("#nombre").val(),
        colonia: $("#colonias").val(),
        usuario: $("#usuarios").val()

    }

    $.ajax({
        url: '/ruta/guardar',
        data: JSON.stringify(data),
        type: 'POST',
        dataType: "json",
        beforeSend: function(){
            $('#ruta-container').html('<div style="text-align: center;">' +
                '<i class="fa fa-spinner fa-pulse fa-2x"></i> Guardando...' +
                '</div>');
        },
        success: function(json) {
            console.log(json);
            if (!json.lOk) {
                showGeneralMessage(json.cMensaje, "warning");
                return;
            }
            else
            {
                showGeneralMessage("Registro Guardado", "success");
                $('#ruta-info-modal').modal('hide');
                var $tr = $("#ruta-" + json.id);
                if(!json.nuevo){
                    rutasDT.cell( $tr, 2 ).data( json.activo );
                    rutasDT.cell( $tr, 3 ).data( json.id );
                    rutasDT.cell( $tr, 4 ).data( json.nombre );
                    rutasDT.cell( $tr, 5 ).data( json.colonia );
                    rutasDT.cell( $tr, 6 ).data( json.usuario );

                }
                rutasDT.draw();

            }
        },
        complete: function () {
            $('#ruta-container').html("");
        }
    });

}
function fnBuscarRutas() {

    var nombre = $("#rutas-nombre-val").val();
    var colonia = $("#rutas-colonia-val").val();
    var clave = $("#rutas-clave-val").val();
    var tipoBusqueda = $("#rutas-filter-select").val();

    if(tipoBusqueda == "nom"){
        if(nombre == ""){
            showGeneralMessage("Debe escribir el nombre a buscar", "warning");
            return;
        }
        if(nombre.length < 2){
            showGeneralMessage("Debe escribir almenos dos caracteres", "warning");
            return;
        }
    }
    if(tipoBusqueda == "cla"){
        if(clave == ""){
            showGeneralMessage("Debe escribir la clave a buscar", "warning");
            return;
        }
    }
    if(tipoBusqueda == "col"){
        if(colonia == ""){
            showGeneralMessage("Debe seleccionar una colonia", "warning");
            return;
        }


    }

    fnLimpiarDatos();

    var data = {
        colonia: colonia,
        tipo: tipoBusqueda,
        nombre: nombre,
        clave: clave,


    }
    $.ajax({
        url: '/ruta/search',
        data: JSON.stringify(data),
        type: 'POST',
        dataType: "json",
        beforeSend: function(){

            $('#ruta-container').html('<div style="text-align: center;">' +
                '<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
                '</div>');
        },
        success: function(json){

            rutasDT.clear();
            if(json.length == 0)
            {
                showGeneralMessage("No se encontraron registros", "warning");
                return;
            }



            rutasDT.rows.add(json);
            rutasDT.draw();





        },
        complete: function () {
            $('#ruta-container').html("");
        }
    });

    
}

function fnLimpiarDatos()
{
    selec = new Array();
    uSelec = 0;
    rutasDT.clear();

}

function fnLimpiaForm()
{
    $("#clave").val("");
    $("#activo").prop('checked', true);
    $("#nombre").val("");
    coloniaForm[0].selectize.setValue("");
    usuarioForm[0].selectize.setValue("");
}

function loadRutaInfo($indexRuta, mode) {
    if($indexRuta == null){
        return;
    }

    $.ajax({
        url: "/ruta/get/" + $indexRuta,
        type: 'GET',
        success: function(data){
            $data = jQuery.parseJSON(data);

            fnLimpiaForm();
            setModeRutaInfo(mode);
            setRutaInfo($indexRuta, $data, mode);

        },
        error: function(){
        }
    });
    
}

function setModeRutaInfo(mode) {
    if(mode == "info"){
        validateForm = false;
        $('.modal-title').html('Información');
        $("#btnGuardar").hide();

        $("#nombre").attr("disabled", true);
        coloniaForm[0].selectize.disable();
        usuarioForm[0].selectize.disable();

    }
    else if('edit' || 'add'){
        $('.modal-title').html('Modificar');
        $("#nombre").removeAttr("disabled");
        coloniaForm[0].selectize.enable();
        usuarioForm[0].selectize.enable();
        $("#btnGuardar").show();
    }

}

function setRutaInfo($indexRuta, $data, mode) {
    $("#clave").val($data.id);
    if(mode == "info")
        $("#activo").prop('checked', $data.activo);
    $("#nombre").val($data.nombre);
    coloniaForm[0].selectize.setValue($data.colonia);
    usuarioForm[0].selectize.setValue($data.usuario);
    
}

function eliminarRuta($tr, id){
    $.ajax({
        url: "/ruta/delete/" + id,
        success: function(){
            showGeneralMessage("La información se guardo correctamente", "success", true);
            rutasDT.cell( $tr, 2 ).data( '<i class="fa fa-remove" style="color: red" title="Activo"><\/i>' );
            rutasDT.draw();
        },
        error: function(){
            showGeneralMessage("Ocurrió un error al guardar la información", "error", true);
        }
    });

}