
var deleteunidadcuadrilla = [];

$(document).ready(function () {

    selectCuadrilla = $("#cmbCuadrilla").selectize();
    selectizeCuadrilla = selectCuadrilla[0].selectize;
    selectizeCuadrilla.setValue("", false);

    $(".filtro-unidadseguimiento-clear-do").off("click");
    $(".filtro-unidadseguimiento-clear-do").on("click", function(){
        $(this).next().val("");
    });
    
    $('.Filt').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            $('#unidadseguimiento-search-do').click();
        }
    });    

    $("#unidadseguimiento-search-do").on("click", function () {
        unidadseguimientoDT.column(3).search($("#fnombre").val().trim());
        unidadseguimientoDT.column(4).search($("#fplaca").val().trim());
        unidadseguimientoDT.column(5).search($("#fnoeconomico").val().trim());
        unidadseguimientoDT.column(8).search($("#fvigente").val());
        unidadseguimientoDT.draw();
    });

    $("#addunidadseguimientoDo").on("click", function () {
        $("#unidadseguimiento-info-modal").modal("show");
    });

    $("#btnGuardar").on("click", function () {
        var error = "";
        var id = $("#id").val();
        var nombre = $("#txtNombre").val().trim();
        var placa = $("#txtPlaca").val().trim();
        var numero_economico = $("#txtNoEconomico").val().trim();
        var descripcion = $("#txtDescripcion").val().trim();
        if(nombre == ""){
            error += "- El nombre no puede estar vacio<br>";
        }
        if(placa == ""){
            error += "- La placa no puede estar vacio<br>";
        }
        if(dataTableUnidadCuadrilla.rows().count() == 0){
            error += "- Ingrese al menos una cuadrilla";
        }
        if(error != ""){
            showGeneralMessage(error, "warning", true);
            return
        }
        $.each(dataTableUnidadCuadrilla.data(), function (index, value) {
            deleteunidadcuadrilla.push(value);
        });
        var data = {
            id: id ? id : null,
            nombre: nombre,
            placa: placa,
            numero_economico: numero_economico,
            descripcion: descripcion,
            unidadcuadrilla: deleteunidadcuadrilla,
        }
        $.ajax({
            url: "/unidadseguimiento/save",
            type: "POST",
            data: JSON.stringify(data),
            beforeSend: function(){
                $("#btnGuardar").prop("disabled", true);
            },
            success: function () {
                showGeneralMessage("La información se guardo correctamente", "success", true);
                unidadseguimientoDT.draw(false);
                $("#unidadseguimiento-info-modal").modal("hide");
            },
            complete: function () {
                $("#btnGuardar").prop("disabled", false);
            }
        })
    });

    $("#create_cuadrilla_unidad").on("click", function () {
        let error = "";
        var cuadrilla = $("#cmbCuadrilla").val().trim();
        if(cuadrilla == "" || cuadrilla == null){
            error += "- La cuadrilla no puede estar vacia<br>";
        }
        if(error != ""){
            showGeneralMessage(error, "warning", true);
            return
        }
        let add = true;
        $.each(dataTableUnidadCuadrilla.rows().data(), function (index, value) {
            if(value[3] == cuadrilla){
                add = false
                return false;
            }
        });
        if(!add){
            showGeneralMessage("Ya existe la cuadrilla.", "warning", true);
            return
        }
        let btn = '<button class="btn btn-sm btn-danger delete_cuadrilla_unidad" data-id="-1" title="¿Desea eliminar?"><i class="fa fa-remove"></i></button>';
        let array = ["", btn, $("#cmbCuadrilla option:selected").text(), cuadrilla, true, -1, ""];
        //console.log(array);
        dataTableUnidadCuadrilla.row.add(array).draw();
    });

    $("#unidadseguimiento-info-modal").on("hidden.bs.modal", function () {
        $("#unidadseguimiento-info-modal .modal-title").html("Nueva Unidad");
        $("#txtPlaca").prop("disabled", false);
        $("#id").val("");
        selectizeCuadrilla.setValue("", false);
        $(".txtDis").val("");
        deleteunidadcuadrilla = [];
        dataTableUnidadCuadrilla.clear().draw();
        selectizeCuadrilla.enable();
        $(".txtDis").prop("disabled", false);
        $("#create_cuadrilla_unidad").prop("disabled", false);
        $("#btnGuardar").prop("disabled", false);
    });

    $("#unidadseguimiento-datatable").on('click', '.unidadseguimiento-edit', function () {
        $("#unidadseguimiento-info-modal .modal-title").html("Editar Unidad");
        $("#txtPlaca").prop("disabled", true);
        var $tr = $(this).closest("tr");
        var data = unidadseguimientoDT.row($tr).data();
        $("#id").val(data[4]);
        $("#txtNombre").val(data[3]);
        $("#txtPlaca").val(data[4]);
        loadUnidadcuadrilla(data[4], false);
        $("#txtNoEconomico").val(data[5]);
        $("#txtDescripcion").val(data[9]);
        $("#unidadseguimiento-info-modal").modal("show");
    });

    $("#unidadseguimiento-datatable").on( 'click', '.unidadseguimiento-info', function () {
        $("#unidadseguimiento-info-modal .modal-title").html("Consultar Unidad");
        var $tr = $(this).closest("tr");
        var data = unidadseguimientoDT.row($tr).data();
        $("#id").val(data[4]);
        $("#txtNombre").val(data[3]);
        $("#txtPlaca").val(data[4]);
        loadUnidadcuadrilla(data[4], true);
        $("#txtNoEconomico").val(data[5]);
        $("#txtDescripcion").val(data[9]);
        $(".txtDis").prop("disabled", true);
        $("#create_cuadrilla_unidad").prop("disabled", true);
        selectizeCuadrilla.disable();
        $("#btnGuardar").prop("disabled", true);
        $("#unidadseguimiento-info-modal").modal("show");
    });

    $("#unidadseguimiento-datatable").on( 'click', '.unidadseguimiento-delete', function () {
        activate($(this), false);
    });

    $("#unidadseguimiento-datatable").on( 'click', '.unidadseguimiento-active', function () {
        activate($(this), true);
    });

    unidadseguimientoDT = $("#unidadseguimiento-datatable").DataTable({
        "dom": '<"clear"><"top container-float"<"filter-located">f><"datatable-scroll"rt><"bottom"lip><"clear">',
        "autoWidth": false,
        "paging": true,
        "select": true,
        "info": true,
        //searching: true,
        "processing": true,
        "scrollCollapse": false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
        "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
        "pageLength": 10,
        "order": [[ 3, 'asc' ]],
        responsive: {
            details: {
                type: 'column',
                target: 1
            }
        },
        "processing": true,
        "serverSide": true,
        //"deferLoading": 0,
        ajax: {
            url: "/unidadseguimiento/buscar",
        },
        //responsive: true,
        "columnDefs": [
            {
                targets: [0],
                orderable: false,
                searchable: false,
                className: 'dt-center no-wrap',
                visible: coacciones != 'no'
            },
            { targets: [2,6,7], className: 'dt-center no-wrap'},
            { targets: [1], orderable: false, className: 'control', searchable: false},
            { targets: [-1,-2], visible: false },
            { targets: '_all', visible: true }
        ],
        "lengthChange": true
    });

    dataTableUnidadCuadrilla = $('#cuadrilla_unidad-datatable').DataTable({
        dom: 'rt<"row"<"col-md-6 col-sm-6"i><"col-md-6 col-sm-6"p>>',
        pageLength: -1,
        paging: false,
        info: false,
        responsive: {
            details: {
                type: 'column',
                target: 0
            }
        },
        columnDefs: [
            { targets: 0, className: 'control', orderable: false, searchable: false },
            { targets: 1, orderable: false, searchable: false, className: 'dt-center no-wrap', visible: deletecuadunid },
            { targets: [-1,-2,-3,-4], visible: false },
            { targets: [2], orderable: true },
            { targets: '_all', visible: true, orderable: false, searchable: true }
        ],
        language: {
            url: "../../plugins/datatables_new/language.MX.json"
        },
        "order": [
            [2, "asc"]
        ],
    });

    $("#cuadrilla_unidad-datatable").on( 'click', '.delete_cuadrilla_unidad', function () {
        $(".popover.confirmation").confirmation("destroy");
        $(this).confirmation({
            rootSelector: "#unidadseguimiento-info-modal",
            container: "#unidadseguimiento-info-modal",
            singleton: true,
            popout: true,
            btnOkLabel: "Si",
            onConfirm: function() {
                //console.log("eliminar");
                let row = dataTableUnidadCuadrilla.row($(this).parents('tr'));
                let data = row.data();
                //console.log(data);
                if(parseInt(data[5]) > 0){
                    data[4] = false;
                    deleteunidadcuadrilla.push(data);
                }
                row.remove().draw();
            },
            onCancel: function() {
                $(this).confirmation('destroy');
            },
        });
        $(this).confirmation("show");
    });

    $("#unidadseguimiento-info-modal").on("shown.bs.modal", function () {
        dataTableUnidadCuadrilla.columns.adjust();
    });
    
});

function activate($this, activo){
    $(".popover.confirmation").confirmation("destroy");
    var id = $this.data("id");
    $this.confirmation({
        rootSelector: "body",
        container: "body",
        singleton: true,
        popout: true,
        btnOkLabel: "Si",
        onConfirm: function() {
            $.ajax({
                url: "/unidadseguimiento/delete/" + id + "/" + activo,
                success: function () {
                    showGeneralMessage("La información se guardo correctamente", "success", true);
                    unidadseguimientoDT.draw(false);
                }
            });
        },
        onCancel: function() {
            $this.confirmation('destroy');
        },
    });
    $this.confirmation("show");
}

function loadUnidadcuadrilla(id, disable){
    $.ajax({
        url: '/unidadseguimiento/get/'+id,
        beforeSend: function(){
            dataTableUnidadCuadrilla.clear().draw();
        },
        success: function (resp) {
            var json = JSON.parse(resp);
            dataTableUnidadCuadrilla.rows.add(json).draw();
            $(".delete_cuadrilla_unidad").prop("disabled", disable);
        }
    });
}
