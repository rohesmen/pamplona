mapUbicar = null;
sourVector = new ol.source.Vector();
vectorPoint = new ol.layer.Vector();
mapNameLayers = [];
mapNameSources = [];
mapUbicarMarker = null;
mapInfoMarker = null;
mapInfoMarker2 = null;
saveMarker = false;

mapInfoPadromesMarker = null;
cql = "";
nombreCapaBase = "geotecnologia:merida_base";
nombreCapaPadron = "geotecnologia:view_padron_text";
nombreCQLCapaPadron = "geotecnologia:padron_merida";
countShow = 0;
select2_init = false;
select2_1_init = false;
select2_2_init = false;

var typeGlobalSearch;
var markerLst = [];

var extentMeridaBase = [-89.80181885992522,20.694881965925894,-89.44696059406462,21.196658059527174];
//var extentMeridaBase = [-10021448.21178226, 2363889.604925696, -9932169.762745174, 2412809.303028208];
$(document).ready(function() {
    // setTimeout(function(){ $('#search-category').click(); }, 300);
    
	$.fn.datepicker.defaults.language = "es";
	$( "#txtDCustomerB" ).datepicker({
        format: "dd-mm-yyyy",
        language: "es",
        autoclose: true,
        startView: 1,
        minViewMode: 1,
    });
	$( "#txtDCustomerE" ).datepicker({
        format: "dd-mm-yyyy",
        language: "es",
        autoclose: true,
        startView: 1,
        minViewMode: 1,
    });

	$("#toggleVisibility").on("click", toggleResultados);
	
    $("body").css({'overflow-y' :'hidden'});

    $("#content-container").css({'padding-bottom':0});

    setHeightContainers();

    $(window).on("resize", function(){
        setHeightContainers();
        ineDT.columns.adjust();

        if($("#predio-info").length > 0){
            checkWithDragAndResizeComponent("#predio-info", "#predio-info .tab-content-outer");
        }

        if($("#detail-feature-info").length > 0){
            checkWithDragAndResizeComponent("#detail-feature-info", "#detail-feature-info");
        }
        if($("#map-streetview-container").hasClass("show")){
            checkWithDragAndResizeComponent("#map-streetview-container");
        }
        if($(".summary-geoadmin").hasClass("show")){
            checkWithDragAndResizeComponent(".summary-geoadmin .panel");
        }

        if($(".getlegend-geoadmin").hasClass("show")){
            checkWithDragAndResizeComponent(".summary-geoadmin .panel");
        }
        if(layerSwitcher){
            layerSwitcher.resize();
        }
        //if(summaryControl){
            //summaryControl.resize();
        //}
    });

    loadMap();

    $(".summary-geoadmin .panel, .getlegend-geoadmin .panel").draggable({
        containment: "#analysis-map",
        handle: ".panel-title"
    });

    // if($(window).outerWidth() > 768){
    //     activeDragComponents();
    // }
    // else{
    //     deactiveDragComponents();
    // }


    // $("#search-category").off("select2:select");
	
	updateSelectColonias2(COLONIAS);
	
    $("#search-category").on("change", function(e){
        var category = $("#search-category").val();

        $("#persona-input input").attr('disabled', false);
        $("#btn-buscar").attr('disabled', false);
        switch (category){
            case 'COLONIA':
				infoBuffer._deactivateInfo();
                updateSelect(COLONIAS);
			break;
			case 'ZONA':
				infoBuffer._deactivateInfo();
                updateSelect(ZONAS);
				break;
			case 'COMERCIO':
			case 'NOMBRE':
			case 'CLAVE':
			    infoBuffer._deactivateInfo();
                $("#persona-input input").val("");

                $('#area-select').parent().find('.select2').hide();
				$('.clientes-por-pagar').hide();
                $("#persona-input").show();
				$("#direccion-input").hide();
				
                setTimeout(function(){ $("#persona-input input").get(0).focus(); }, 200);
                break;
			case 'PERSONA':
                infoBuffer._deactivateInfo();
                $("#direccion-input input").val("");

                $('#area-select').parent().find('.select2').hide();
				$('.clientes-por-pagar').hide();
                $("#persona-input").hide();
				$("#direccion-input").show();
				setTimeout(function(){ $("#direccion-calle").focus(); }, 200);
				updateSelectColonias2(COLONIAS);
                break;
            case 'CLIENTES_POR_PAGAR':
				infoBuffer._deactivateInfo();
				
				if($("#persona-input").is(":visible")){
					$("#persona-input").hide();
				}
				
				if($("#direccion-input").is(":visible")){
					$("#direccion-input").hide();
				}
				
				$('#area-select').parent().find('.select2').hide();
				$('.clientes-por-pagar').show();
                setTimeout(function(){ $("#txtDCustomerB").focus(); }, 200);
				updateSelectColonias(COLONIAS);
				break;
        }
    });

	$( "#txtDCustomerB" ).on("change", function(){
		setTimeout(function(){ $("#txtDCustomerE").focus(); }, 200);
	});

    $("#btn-buscar").off("click");
    $("#btn-buscar").on("click", function(e){
        if(getSearchType() === "NOMBRE" 
		|| getSearchType() === "COMERCIO"
		|| getSearchType() === "CLAVE"){
            if ($.trim($("#persona-input input").val()) !== ''){
                busquedaGlobal();
            }
        }else if(getSearchType() === "CLIENTES_POR_PAGAR"){
			busquedaGlobal();
		}else if(getSearchType() === "PERSONA"){
			if ($.trim($("#direccion-input input").val()) !== '' || $("#direccion-colonia").val()){
                busquedaGlobal();
            }
		}else{
            if($("#area-select").val())
                busquedaGlobal();
        }
    });

    $('.has-clear input[type="text"]').on('input propertychange', function() {
        var $this = $(this);
        var visible = Boolean($this.val());
        $this.siblings('.form-control-clear').toggleClass('hidden', !visible);
    }).trigger('propertychange');

    $('.form-control-clear').click(function() {
        $(this).siblings('input[type="text"]').val('')
            .trigger('propertychange').focus();
    });

    $("#ine-infopadrones-modal").on('hidden.bs.modal', function () {
        $(this).find(".modal-body").html("");
    });

	
});