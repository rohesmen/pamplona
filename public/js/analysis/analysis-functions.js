//var summaryControl = null;
var infoBuffer = null;
var layerSwitcher = null;
var infoControl = null;
var infoPredio = null;
var getLegendControl = null;
var map = null;
function buscar(type, query){
    var df = "";
    var dl = "";
    var seccion = "";
    var clave = "";
    var nombre = "";
    var colonia = "";
	var direccion = "";
    var val = "";
	var zona = "";
	var no_cliente = "";
	var numero = "";
	var calle = "";
    

    if(type != "GEOMETRIA" && type != "POINT"){
        if (!type && !query) {
            type = getSearchType();
            query = getSearchValue();
        }
        if(getSearchType() === "CLIENTES_POR_PAGAR"){
            var query = [];
            query.push($("#txtDCustomerB").val());
			query.push($("#txtDCustomerE").val());
			
			var data = $("#colonias-select").select2('data');
			var labels = [];
			for(var i=0; i < data.length; i++){
				labels.push("'%"+data[i].text+"%'");
			}
			
			colonia = labels;
			
            val =  query;
        }else if(type === 'NOMBRE'
		|| type === 'COMERCIO' 
		|| type === 'CLAVE'){
			val = $("#persona-input input").val();
		}else if(type === 'PERSONA'){
			if($("#direccion-calle").val().trim() !== ""){
				calle =  "'%"+$("#direccion-calle").val().trim().replace(/ /g,"").toLowerCase()+"%'";
			}
			
			if($("#direccion-numero").val().trim() !== ""){
				numero =  "'%"+$.trim($("#direccion-numero").val().trim().replace(/ /g,"").toLowerCase())+"%'";
			}
			
			$.each($("#direccion-colonia").select2('data'), function (i, val){
				colonia =  "'%"+val.text.toLowerCase()+"%'";
			});
			
		}
		else{
            var data = $("#area-select").select2('data');
			var labels = [];
			for(var i=0; i < data.length; i++){
				labels.push("'%"+data[i].text.toLowerCase()+"%'");
			}
			val = labels;
        }
        typeGlobalSearch = type;
        switch (type){
            case 'ZONA':
				zona = val;
				break;
			case 'NOMBRE':
				nombre = "'%"+val.toLowerCase()+"%'";
				break;
			case 'COMERCIO':
				nombre = "'%"+val.toLowerCase()+"%'";
				break;
			case 'COLONIA':
                colonia = val;
                break;
			case 'CLIENTES_POR_PAGAR':
				nombre = val;
				break;
			case 'CLAVE':
				no_cliente = "'%"+val+"%'";
				break;
        }
    }

	ineDT.column(3).search(nombre);
	ineDT.column(4).search(direccion);
	ineDT.column(5).search(true);
	ineDT.column(6).search(zona);
	ineDT.column(7).search(no_cliente);
	ineDT.column(8).search(colonia);
	ineDT.column(14).search(calle);
	ineDT.column(15).search(numero);
	
    ineDT.draw();
}

function generateQuery(){
    var df = "";
    var dl = "";
    var seccion = "";
    var clave = "";
    var nombre = "";
    var colonia = "";
	var direccion = "";
    var val = "";
	var zona = "";
	var no_cliente = "";
	var numero = "";
	var calle = "";
    var type = getSearchType();
	var query = getSearchValue();

    if(type != "GEOMETRIA" && type != "POINT"){
        if(getSearchType() === "CLIENTES_POR_PAGAR"){
            var query = [];
            query.push($("#txtDCustomerB").val());
			query.push($("#txtDCustomerE").val());

			
			var data = $("#colonias-select").select2('data');
			var labels = [];
			for(var i=0; i < data.length; i++){
				labels.push(data[i].text);
			}
			
			colonia = labels;
			
            val =  query;
        }else if(type === 'NOMBRE'
		|| type === 'COMERCIO' 
		|| type === 'CLAVE'){
			val = $("#persona-input input").val();
		}else if(type === 'PERSONA'){
			if($("#direccion-calle").val() !== ""){
				calle =  $("#direccion-calle").val().toLowerCase();	
			}
			
			if($("#direccion-numero").val() !== ""){
				numero =  $.trim($("#direccion-numero").val().toLowerCase());
			}
			
			$.each($("#direccion-colonia").select2('data'), function (i, val){
				colonia =  val.text.toLowerCase();
			});
			
		}
		else{
            var data = $("#area-select").select2('data');
			var labels = [];
			for(var i=0; i < data.length; i++){
				labels.push(data[i].text.toLowerCase());
			}
			val = labels;
        }
        switch (type){
            case 'ZONA':
				zona = val;
				break;
			case 'NOMBRE':
				nombre = val.toLowerCase();
				break;
			case 'COMERCIO':
				nombre = val.toLowerCase();
				break;
			case 'COLONIA':
                colonia = val;
                break;
			case 'CLIENTES_POR_PAGAR':
				nombre = val;
				break;
			case 'CLAVE':
				no_cliente = val;
				break;
        }
    }

	return '?nombre='+nombre+'&ubicado=true&zona='+zona+'&no_cliente='+no_cliente+'&colonia='+colonia+'&calle='+calle+'&numero='+numero;
}


function buscar_resp(){
    var df = dfSelectize.val();
    var dl = dlSelectize.val();
    var seccion = seccionSelectize.val();
    var ubicado = $("#ine-ubicado").val();
    var nombre = $("#ine-nombre").val();

    $("#ine-container").html("");

    if(nombre == "" && dl == ""){
        showGeneralMessage("Debe seleccionar DL o escribir un nombre", "warning", true);
        return;
    }
    if(nombre != "" && nombre.length < 3){
        showGeneralMessage("Se requieren almenos 3 letras para realizar la búsqueda", "warning", true);
        return;
    }

    var data = {
        df: df == "" ? null: df,
        dl: dl == "" ? null: dl,
        seccion: seccion == "" ? null: seccion,
        ubicado: ubicado == "" ? null: ubicado,
        nombre: nombre
    };

    $.ajax({
        url: "/ine/search",
        type: "POST",
        data: JSON.stringify(data),
        success: function (resp) {
            $("#ine-container").html(resp);
        },
        beforeSend: function(){
            $("#ine-container").html('<div style="text-align: center;">' +
                '<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
                '</div>');
            clearUbicacion();
        },
        error: function(){
            showGeneralMessage("Ocurrió un error al obtener los resultados", "error", true);
            $("#ine-container").html("");
        }
    });
}

function getGoogleLayer(capa){
    var tipoGoogle = null;
    if(capa.capa == "SATELITE"){
        tipoGoogle = google.maps.MapTypeId.SATELLITE;
    }
    else if(capa.capa == "HYBRID"){
        tipoGoogle = google.maps.MapTypeId.HYBRID;
    }
    else if(capa.capa == "ROADMAP"){
        tipoGoogle = google.maps.MapTypeId.ROADMAP;
    }
    else{
        tipoGoogle = google.maps.MapTypeId.TERRAIN;
    }
    var optionsGoogle = {
        title: capa.nombre,
        mapTypeId: tipoGoogle,
        google: true,
        visible:  capa.inicial,
        problemZoom: true
    };
    if(capa.base){
        optionsGoogle.type = 'base';
    }
    return  new olgm.layer.Google(optionsGoogle);
}

function getImageLayer(capa, ignorame){
    if(ignorame === "undefined"){
        ignorame = false;
    }
    var wmsSource = new ol.source.ImageWMS({
        url: urlGeoserver,
        params: {
            'LAYERS': capa.capa,
            'STYLES': capa.estilo,
            'TILED': false,
            'TOKEN': TOKEN
        },
        serverType: 'geoserver',
		crossOrigin: '*'
    });

    wmsSource.on("imageloadstart", function(){
        showLoading();
    });
    wmsSource.on("imageloadend", function(){
        hideLoading();
    });

    wmsSource.on('imageloaderror', function(event) {
        hideLoading();
    });

    var wmsLayerOptions = {
        source: wmsSource,
        name: capa.name,
        properties: {
            labeled: capa.labeled,
            style: capa.estilo,
            styleLabel: capa.estilo_etiqueta,
            tematico: capa.tematico,
            label: capa.label,
			table: capa.tabla,
			esquema: capa.esquema,
        },
        title: capa.titulo,
        visible: capa.visible,
        ignorame: ignorame
    };
    if(capa.base){
        wmsLayerOptions.type = 'base';
    }

    return new ol.layer.Image(wmsLayerOptions);
}

function getTileLayer(capa){
    var wmsSource = new ol.source.TileWMS({
        url: urlGeoserver,
        params: {
            'LAYERS': capa.capa,
            'VERSION': '1.1.1',
            'FORMAT': 'image/png',
            'TILED': true,
            'STYLE': capa.estilo
        }
    });

    wmsSource.on("imageloadstart", function(){
        showLoading();
    });
    wmsSource.on("imageloadend", function(){
        hideLoading();
    });

    wmsSource.on('imageloaderror', function(event) {
        hideLoading();
    });

    var wmsLayerOptions = {
        source: wmsSource,
        name: capa.name,
        properties: {
            labeled: capa.labeled,
            style: capa.estilo,
            styleLabel: capa.estilo_etiqueta,
            tematico: capa.tematico,
            label: capa.label
        },
        title: capa.titulo,
        visible: capa.visible
    };
    if(capa.base){
        wmsLayerOptions.type = 'base';
    }

    return new ol.layer.Tile(wmsLayerOptions);
}

function loadMap(){
    proj4.defs("EPSG:32616","+proj=utm +zone=16 +datum=WGS84 +units=m +no_defs");
    ol.proj.setProj4(proj4);
    if (!ol.proj.get('EPSG:32616')) {
        console.error("Failed to register projection in OpenLayers");
    }

    var baseMaps = [];
    var overlayMaps = [];
    var CENTER_MAP_LAT = 20.9704, CENTER_MAP_LON = -89.6232;
    var view = new ol.View({
        center: ol.proj.transform([CENTER_MAP_LON, CENTER_MAP_LAT], 'EPSG:4326', 'EPSG:3857'),
        maxZoom: 20,
        zoom: 11
    });

    //var layers = getCapas();

    var googleLayerSATELLITE = new olgm.layer.Google({
        title: 'Satélite',
        type: 'base',
        mapTypeId: google.maps.MapTypeId.SATELLITE,
        google: true,
        visible: true,
        problemZoom: true
    });
    overlayMaps.push(googleLayerSATELLITE);

    var googleLayerHYBRID = new olgm.layer.Google({
        title: 'Híbrido',
        type: 'base',
        mapTypeId: google.maps.MapTypeId.HYBRID,
        google: true,
        problemZoom: true
        //visible: true,
    });
    overlayMaps.push(googleLayerHYBRID);

    var googleLayerROADMAP = new olgm.layer.Google({
        title: 'Carretera',
        type: 'base',
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        google: true,
        //visible: true,
    });
    overlayMaps.push(googleLayerROADMAP);

    var wmsSource = new ol.source.ImageWMS({
        url: urlGeoserver,
        params: {
            'LAYERS': nombreCapaBase,
            'FORMAT': 'image/jpeg',
            'STYLES': '',
            'TILED': false
        },
        serverType: 'geoserver',

    });

    var wmsLayer = new ol.layer.Image({
        title: 'Catastral',
        type: 'base',
        visible: true,
        source: wmsSource
    });
    //overlayMaps.push(wmsLayer);

    mapNameLayers[nombreCapaBase] = wmsLayer;
    mapNameSources[nombreCapaBase] = wmsSource;

    var layerGroups = [];
    for(var i in grupos){
        var grupo = grupos[i];
        var capas = [];
        for(var j in grupo.capas){
            var capa = grupo.capas[j];
            capas.push(getImageLayer(capa));
        }

        overlayMaps.push(
            new ol.layer.Group({
                title: grupo.nombre,
                layers: capas
            })
        );
    }

    var attribution = new ol.control.Attribution({
        collapsible: true,
        label: 'A',
        collapsed: true,
        tipLabel: 'yooo',
        className: 'custom-mouse-position',
    });

    map = new ol.Map({
        loadTilesWhileAnimating: true,
        layers: overlayMaps,
        //layers: layers,
        controls: ol.control.defaults({
            attribution: false
        }).extend([attribution]).extend([
            new ol.control.ZoomToExtent({
                tipLabel: 'Municipio de Mérida',
                label: '',
                extent: ol.extent.applyTransform(extentMeridaBase, ol.proj.getTransform("EPSG:4326", "EPSG:3857"))
                //extent: extentMeridaBase
            })
        ]),
        interactions: olgm.interaction.defaults(/*{ doubleClickZoom: true}*/),
        target: 'analysis-map',
        view: view
    });

    layerSwitcher = new ol.control.LayerSwitcher.Geoadmin({
        tipLabel: 'Manejador de capas'
    });

    var infoPadron = new ol.control.InfoPadron({
        tipLabel: 'Información de padron'
    });

    infoBuffer = new ol.control.InfoBuffer({
        tipLabel: 'Buscar por área de influencia'
    });

    var infoPadrones = new ol.control.InfoPadrones({
        tipLabel: 'Información de padrones'
    });

    getLegendControl = new ol.control.GetLegend({
        tipLabel: 'Simbología',
        recurso: "maps"
    });

    //summaryControl = new ol.control.Summary({
    //    tipLabel: 'Resumen'
    //});

    addStreetViewControl(map, 6.5);

    layerSwitcher.legendControl = getLegendControl;

    var baseLayerSwitcher = new ol.control.BaseLayerSwitcher({
        tipLabel: 'Manejador de capas base'
    });

    infoPredio = new ol.control.InfoPredio({
        tipLabel: 'Información de predio'
    });

    var exportMap = new ol.control.Export({
        tipLabel: 'Exportar mapa'
    });

    map.addControl(layerSwitcher);
    map.addControl(infoBuffer);
    map.addControl(getLegendControl);
    map.addControl(baseLayerSwitcher);
    map.addControl(exportMap);
	
	var element = document.getElementById('popup');
	var popup = new ol.Overlay({
	  element: element,
	  positioning: 'bottom-center',
	  stopEvent: false,
	  offset: [0, -30]
	});
	map.addOverlay(popup);
	
	map.on('click', function(evt) {
		//var element = document.getElementById('popup');
	  var feature = map.forEachFeatureAtPixel(evt.pixel,
		  function(feature) {
			return feature;
		  });
	  if (feature && feature.S.info) {
		var coordinates = feature.getGeometry().getCoordinates();
		popup.setPosition(coordinates);
		var pop = $(element).popover({
			'title': '<span class="close" id="closePop">&times;</span><br>',
			'placement': 'auto',
			'html': true,
			'container': 'body',
			'content': parseRowData(feature.S.info)
		});
		
		pop.on('shown.bs.popover', function(e){
			$("#closePop").click(function(){
				$(element).popover('destroy');
			});
		});
		
		
		$(element).popover('show');
	  } else {
		$(element).popover('destroy');
	  }
	});
	
    infoControl = addInfoControl(map, "singleclick", true, 7);

    infoDrawingCOntrol = addDrawingInfoControl(map, 8.5);

    olGM = new olgm.OLGoogleMaps({map: map});
    olGM.activate();
}

function parseRowData(row){
	var toReturn =
	'<span style="font-weight:bold; font-size:16px;"><i class="fa fa-map-marker"></i> No. Cliente: </span>'+row[2] +'<span style="font-weight:bold; font-size:16px;"> - Origen: </span> '+row[13] +
	'<table class="ui table" style="width:100% !important;">'+
		'<tbody>'+
			'<tr data-dt-row="0" data-dt-column="4">'+
				'<td style="font-weight:bold;">Dirección:</td>'+
				'<td>'+row[4]+'</td>'+
			'</tr>'+
			'<tr data-dt-row="0" data-dt-column="6">'+
				'<td style="font-weight:bold;">Zona:</td>'+
				'<td>'+row[6]+'</td>'+
			'</tr>'+
			'<tr data-dt-row="0" data-dt-column="6">'+
				'<td style="font-weight:bold;">Colonia:</td>'+
				'<td>'+row[8]+'</td>'+
			'</tr>'+
			'<tr data-dt-row="0" data-dt-column="9">'+
				'<td style="font-weight:bold;">Último Pago:</td>'+
				'<td>'+row[9]+'</td>'+
			'</tr>'+
			'<tr data-dt-row="0" data-dt-column="9">'+
				'<td style="font-weight:bold;">Fecha Último Pago:</td>'+
				'<td>'+row[12]+'</td>'+
			'</tr>'+
			'<tr data-dt-row="0" data-dt-column="9">'+
				'<td style="font-weight:bold;">Importe:</td>'+
				'<td>$'+row[16]+'</td>'+
			'</tr>'+
			'<tr data-dt-row="0" data-dt-column="9">'+
				'<td style="font-weight:bold;">Total:</td>'+
				'<td>$'+row[17]+'</td>'+
			'</tr>'+
		'</tbody>'+
	'<table>';
	
	return toReturn;
}

function loadMapUbicar(){
    var baseMaps = [];
    var overlayMaps = [];
    var CENTER_MAP_LAT = 20.9704, CENTER_MAP_LON = -89.6232;
    var view = new ol.View({
        center: ol.proj.transform([CENTER_MAP_LON, CENTER_MAP_LAT], 'EPSG:4326', 'EPSG:3857'),
        zoom: 11
    });

    var wmsSource = new ol.source.ImageWMS({
        url: urlGeoserver,
        params: {
            'LAYERS': nombreCapaBase,
            'STYLES': '',
            'TILED': true
        },
        serverType: 'geoserver',

    });
    wmsSource.on("imageloadstart", function(){
        $("#ine-ubicar-loading").show();
    });
    wmsSource.on("imageloadend", function(){
        $("#ine-ubicar-loading").hide();
    });

    var wmsLayer = new ol.layer.Image({
        title: 'Catastral',
        type: 'base',
        visible: true,
        source: wmsSource
    });
    //baseMaps.push(wmsLayer);


    var attribution = new ol.control.Attribution({
        collapsible: true,
        label: 'A',
        collapsed: true,
        tipLabel: 'yooo',
        className: 'custom-mouse-position',
    });

    mapUbicar = new ol.Map({
        layers: [
            new ol.layer.Group({
                'title': 'Capas base',
                layers: baseMaps
            })
        ],
        controls: ol.control.defaults({
            attribution: false
        }).extend([attribution]),
        target: 'map-ubicar',
        view: view
    });

    mapUbicar.on("click", onMapUbicarClick);
}

function setPointMap(json){
    var pointFeature = new ol.Feature(
        new ol.geom.Point(json.coordinates)
    );

    sourVector.clear();
    sourVector.addFeature(pointFeature);

    var iconStyle = new ol.style.Style({
        image: new ol.style.Icon(({
            anchor: [0.5, 46],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            //opacity: 0.75,
            src: '../img/pin-marker.png'
        }))
    });

//create a [point layer and a source and pass the fetaure to the source
    vectorPoint = new ol.layer.Vector({
        source: sourVector,
        style: iconStyle
    });

    map.addLayer(vectorPoint);
    //var pan = ol.animation.pan({
    //    //duration: 2000,
    //    source: /** @type {ol.Coordinate} */ (map.getView().getCenter())
    //});
    //map.beforeRender(pan);
    ma
}

function loadCQL(){
    var layer = mapNameLayers[nombreCapaPadron];
    var source = mapNameSources[nombreCapaPadron];
    if(!layer.getVisible()){
        layer.setVisible(true);
    }
    if(cql != ""){
        source.updateParams({"CQL_FILTER": cql});
    }
    else{
        delete source.getParams().CQL_FILTER;
        source.updateParams(source.getParams());
    }

    //var aux = cql != "" ? cql : null;

}

function clearUbicacion(){
    sourVector.clear();
}

function onMapUbicarClick(e) {
    var coordinates = e.coordinate;
    if(mapUbicarMarker != null){
        mapUbicar.removeLayer(mapUbicarMarker);
        mapUbicarMarker = null;
    }

    var point_feature = new ol.Feature({
        geometry: new ol.geom.Point(coordinates)
    });

    var iconStyle = new ol.style.Style({
        image: new ol.style.Icon(({
            anchor: [0.5, 30],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            opacity: 0.75,
            src: '../img/ubicar-marker-green.png'
        }))
    });

    mapUbicarMarker = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: [point_feature]
        }),
        style: iconStyle,
        ignorame: true
    });

    mapUbicar.addLayer(mapUbicarMarker);
}

function setPersonData(id, json){
    if(mapUbicarMarker != null){
        mapUbicar.removeLayer(mapUbicarMarker);
        mapUbicarMarker = null;
    }
    $("#ine-ubicar-id").val(id);
    $("#ine-ubicar-modal").modal("show");
    $("#ine-ubicar-calle").val(json.calle);
    $("#ine-ubicar-numero").val(json.numero);
    $("#ine-ubicar-colonia").val(json.colonia);
    $("#ine-ubicar-df").val(json.df);
    $("#ine-ubicar-dl").val(json.dl);
    $("#ine-ubicar-seccion").val(json.seccion);
    $("#ine-ubicar-title").html(json.nombre);

    if(json.ubicado){
        var jsonUbicado = JSON.parse(json.ubicado);
        var point_feature = new ol.Feature({
            geometry: new ol.geom.Point(jsonUbicado.coordinates)
        });

        var iconStyle = new ol.style.Style({
            image: new ol.style.Icon(({
                anchor: [0.5, 30],
                anchorXUnits: 'fraction',
                anchorYUnits: 'pixels',
                opacity: 0.75,
                src: '../img/ubicar-marker.png'
            }))
        });

        mapUbicarMarker = new ol.layer.Vector({
            source: new ol.source.Vector({
                features: [point_feature]
            }),
            style: iconStyle
        });

        mapUbicar.addLayer(mapUbicarMarker);
        mapUbicar.getView().setZoom(19);
        mapUbicar.getView().setCenter(jsonUbicado.coordinates);
    }
    else{
        if(json.secciongeom){
            var f = new ol.format.GeoJSON();
            var jsObject = JSON.parse(json.secciongeom);

            var geometry = new ol.geom.MultiPolygon(jsObject.coordinates);
            var multipolygon_feature = new ol.Feature({
                geometry: geometry
            });

            var sourVector=  new ol.source.Vector({
                features: [multipolygon_feature]
            });

            setTimeout(function(){
                mapUbicar.getView().fit(geometry, mapUbicar.getSize());
            }, 500);
        }
    }
}

function saveDataPerson(){
    if(mapUbicarMarker == null){
        showGeneralMessage("Debe ubicar en el mapa a la persona", "warning", true);
        return;
    }
    var id = $("#ine-ubicar-id").val();
    var format = new ol.format['GeoJSON']();
    var geomJson = format.writeFeatures(mapUbicarMarker.getSource().getFeatures());
    var json = JSON.parse(geomJson);
    var geometry = json.features[0].geometry;
    var data = {
        id: id,
        geom: geometry
    };
    $.ajax({
        url: "/ine/save",
        data: JSON.stringify(data),
        type: "POST",
        beforeSend: function(){
            $("#ine-ubicar-save").attr("disabled", "disabled");
            $("#ine-ubicar-loading").show();
        },
        success: function(){
            saveMarker = true;
            $("tr[id=" + id + "]").find(".ine-zoom").removeAttr("disabled");
            //loadCQL();
            $("#ine-ubicar-modal").modal("hide");
            ubicarEnMapa(id);
            showGeneralMessage("La ubicación se guardo correctamente", "success", true);
        },
        error: function(){
            showGeneralMessage("Debe ubicar en el mapa a la persona", "error", true);
        },
        complete: function(){
            $("#ine-ubicar-loading").hide();
            $("#ine-ubicar-save").removeAttr("disabled");
        }
    });
}

function ubicarEnMapa(id){
    $.ajax({
        url: "/ine/zoom/" + id,
        beforeSend: function(){
            $("#ine-map-loading").show();
            map.removeLayer(vectorPoint);
        },
        success: function(resp){
            var json = JSON.parse(resp);
            setPointMap(json);
        },
        complete: function(){
            $("#ine-map-loading").hide();
        }
    });
}

function closeInfoLayer(){
    clearMarkerPerson();
}

function getInfoLayer(e){
    if(map.getView().getZoom() < 16){
        showGeneralMessage("Se requiere hacer un acercamiento para obtener mas información.", "warning", true);
        return;
    }
    var layer = mapNameLayers[nombreCapaPadron];
    var source = mapNameSources[nombreCapaPadron];

    var coordinates = e.coordinate;
    busquedaGlobal("POINT", coordinates[0] + "," + coordinates[1]);
    initMarkerPerson(coordinates);
}

function getInfo(e){
    if(map.getView().getZoom() < 16){
        showGeneralMessage("Se requiere hacer un acercamiento para obtener mas información.", "warning", true);
        return;
    }
    var layer = mapNameLayers[nombreCapaPadron];
    var source = mapNameSources[nombreCapaPadron];

    var coordinates = e.coordinate;
    busquedaGlobal("POINT", coordinates[0] + "," + coordinates[1]);
    initMarkerPerson(coordinates);
}

function closeInfoLayerPadrones(){
    if(mapInfoPadromesMarker != null){
        map.removeLayer(mapInfoPadromesMarker);
        mapInfoPadromesMarker = null;
    }
}

function getInfoLayerPadrones(e){
    var countVisibleLayers = 0;

    for(var i in map.getLayers().getArray()){
        var l = map.getLayers().getArray()[i];
        if(l.get("type") === "base") continue;
        if(l.getLayers){
            for(var j in l.getLayers().getArray()){
                var l2 = l.getLayers().getArray()[j];
                var source = l2.get('source');
                if(!(source instanceof ol.source.ImageWMS)) continue;
                var layerName = source.getParams().LAYERS;
                if(layerName == nombreCapaPadron) continue;
                if(l2.getVisible()){
                    countVisibleLayers++;
                }
            }
        }
        else{
            var source = l.get('source');
            if(!(source instanceof ol.source.ImageWMS)) continue;
            var layerName = source.getParams().LAYERS;
            if(layerName == nombreCapaPadron) continue;

            if(l.getVisible()){
                countVisibleLayers++;
            }
        }
    }

    if(countVisibleLayers == 0){
        showGeneralMessage("Se requiere activar capas.", "warning", true);
        return;
    }


    var coordinates = e.coordinate;
    if(mapInfoPadromesMarker != null){
        map.removeLayer(mapInfoPadromesMarker);
        mapInfoPadromesMarker = null;
    }

    initMarkerInfoPadrones(coordinates);
    map.addLayer(mapInfoPadromesMarker);

    $("#ine-infopadrones-modal").find(".modal-body").html("");
    $("#ine-infopadrones-modal").modal("show");

    var viewResolution = /** @type {number} */ (map.getView().getResolution());
    for(var i in map.getLayers().getArray()){
        var l = map.getLayers().getArray()[i];
        if(l.get("type") === "base") continue;
        if(l.getLayers){
            for(var j in l.getLayers().getArray()){
                var l2 = l.getLayers().getArray()[j];
                var source = l2.get('source');
                if(!(source instanceof ol.source.ImageWMS)) continue;
                var layerName = source.getParams().LAYERS;
                if(layerName == nombreCapaPadron) continue;
                if(!l2.getVisible()) continue;
                var url = source.getGetFeatureInfoUrl(
                    coordinates, viewResolution, 'EPSG:3857',
                    {'INFO_FORMAT': 'application/json'});

                if (url) {
                    getTable(l2.get("title"), url);
                }
            }
        }
        else{
            var source = l.get('source');
            if(!(source instanceof ol.source.ImageWMS)) continue;
            var layerName = source.getParams().LAYERS;
            if(layerName == nombreCapaPadron) continue;
            if(!l.getVisible()) continue;

            var url = source.getGetFeatureInfoUrl(
                coordinates, viewResolution, 'EPSG:3857',
                {'INFO_FORMAT': 'application/json'});

            if (url) getTable(l.get("title"), url);
        }
    }
}

function getTable(titulo, url){
    var parser = new ol.format.GeoJSON();

    var parseResponse = function(response){
        var result = parser.readFeatures(response);
        //console.log(response);
        //console.log(result);

        var table = '<p style="text-align: center;">Sin resultados</p>';
        if (result.length) {
            var properties = response.features[0].properties;

            var headArray = Object.keys(properties);
            var th = "";
            for(var i in headArray){
                th += '<th>' + headArray[i] + '</th>'
            }
            var thead = '<thead>\
                <tr>' + th + '</tr>\
            </thead>';

            var trs = '';
            for(var i in response.features){
                var properties = response.features[i].properties;
                var headArray = Object.keys(properties);
                var tds = '';
                for(var i in headArray){
                    tds += '<td>' + (properties[headArray[i]] ? properties[headArray[i]] : "") + '</td>'
                }
                trs += '<tr>' + tds + '</tr>';
            }

            var tbody = '<tbody>' + trs + '</tbody>';

            var table = '<table class="table table-striped" style="margin-bottom: 0px;">' + thead + tbody + '</table>';
            $("#ine-infopadrones-modal .modal-body").append(
                $('<div class="panel panel-bordered panel-info">\
                        <div class="panel-heading"><h3 class="panel-title">' + titulo + '</h3></div>\
                        <div class="panel-body" style="overflow-x: auto;">' + table +  '</div>\
                    </div>')
            );
        }
    }

    var surl = url.split("?");
    url = "/ine/infopadrones?" + surl[1] + "&FEATURE_COUNT=1";;
    $.ajax({
        url: url,
        //dataType: 'jsonp',
        //async: false,
        //jsonpCallback: 'parseResponse',
        error: function (xhr, status, error){
            console.log("error");
        },
        //success: function(response){
        //    var result = parser.readFeatures(response);
        //    console.log(response);
        //    console.log(result);
        //
        //    var table = '<p style="text-align: center;">Sin resultados</p>';
        //    if (result.length) {
        //        var properties = response.features[0].properties;
        //
        //        var headArray = Object.keys(properties);
        //        var th = "";
        //        for(var i in headArray){
        //            th += '<th>' + headArray[i] + '</th>'
        //        }
        //        var thead = '<thead>\
        //        <tr>' + th + '</tr>\
        //    </thead>';
        //
        //        var trs = '';
        //        for(var i in response.features){
        //            var properties = response.features[i].properties;
        //            var headArray = Object.keys(properties);
        //            var tds = '';
        //            for(var i in headArray){
        //                tds += '<td>' + (properties[headArray[i]] ? properties[headArray[i]] : "") + '</td>'
        //            }
        //            trs += '<tr>' + tds + '</tr>';
        //        }
        //
        //        var tbody = '<tbody>' + trs + '</tbody>';
        //
        //        var table = '<table class="table table-striped" style="margin-bottom: 0px;">' + thead + tbody + '</table>';
        //        $("#ine-infopadrones-modal .modal-body").append(
        //            $('<div class="panel panel-bordered panel-info">\
        //                <div class="panel-heading"><h3 class="panel-title">' + titulo + '</h3></div>\
        //                <div class="panel-body" style="overflow-x: auto;">' + table +  '</div>\
        //            </div>')
        //        );
        //    }
        //},
        //beforeSend: function(){
        //    //$(this).find(".modal-body").html("");
        //}
    }).then(function(response){
        var result = parser.readFeatures(response);
        //console.log(response);
        //console.log(result);

        var table = '<p style="text-align: center;">Sin resultados</p>';
        if (result.length) {
            response = JSON.parse(response);
            var properties = response.features[0].properties;

            var headArray = Object.keys(properties);
            var th = "";
            for(var i in headArray){
                th += '<th>' + headArray[i] + '</th>'
            }
            var thead = '<thead>\
                <tr>' + th + '</tr>\
            </thead>';

            var trs = '';
            for(var i in response.features){
                var properties = response.features[i].properties;
                var headArray = Object.keys(properties);
                var tds = '';
                for(var i in headArray){
                    tds += '<td>' + (properties[headArray[i]] ? properties[headArray[i]] : "") + '</td>'
                }
                trs += '<tr>' + tds + '</tr>';
            }

            var tbody = '<tbody>' + trs + '</tbody>';

            var table = '<table class="table table-striped" style="margin-bottom: 0px;">' + thead + tbody + '</table>';
            $("#ine-infopadrones-modal .modal-body").append(
                $('<div class="panel panel-bordered panel-info">\
                        <div class="panel-heading"><h3 class="panel-title">' + titulo + '</h3></div>\
                        <div class="panel-body" style="overflow-x: auto;">' + table +  '</div>\
                    </div>')
            );
        }
    });
}

function showLoading(){
    $("#ine-map-loading").show();
    countShow++;
    //console.log(countShow);
}

function hideLoading(){
    countShow--;
    //console.log(countShow);
    if(countShow <= 0){
        countShow = 0;
        $("#ine-map-loading").hide();
    }
}


function clearMarkerPerson(){
    if(mapInfoPadromesMarker != null){
        map.removeLayer(mapInfoPadromesMarker);
        mapInfoPadromesMarker = null;
    }
}

function clearAllMarkers(){
	if(markerLst.length > 0){
		$.each(markerLst, function(i, a){
			map.removeLayer(a);
		});
		markerLst = [];
    }
}

function initMarkerList(data){
	clearAllMarkers();
	var iconStyle = new ol.style.Style({
        image: new ol.style.Icon(({
            anchor: [0.5, 30],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            src: '../img/ubicar-marker-green.png'
        }))
    });

    var pfLst = [];
	$.each(data, function(i, d){
		var coord = [parseFloat(d.longitud), parseFloat(d.latitud)];
		coord = ol.proj.transform(coord, 'EPSG:4326', 'EPSG:3857');
		var point_feature = new ol.Feature({
			geometry: new ol.geom.Point(coord)
		});
		pfLst.push(point_feature);
	});

	var marker = new ol.layer.Group({
		title: 'Capas de información',
		layers: [
			new ol.layer.Vector({
				source: new ol.source.Vector({
					features: pfLst,
					ignorame: true
				}),
				style: iconStyle,
				ignorame: true
			})
		],
		ignorame: true
	});
	markerLst.push(marker);
	map.addLayer(marker);
}

function setMarkers(data){
	clearAllMarkers();
	var iconStyle = new ol.style.Style({
        image: new ol.style.Icon(({
            anchor: [0.5, 30],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            src: '../img/ubicar-marker-green.png'
        }))
    });

    var pfLst = [];
	
	$.each(data, function(i, row){
		if(row[5]){
			var coord = [parseFloat(row[11]), parseFloat(row[10])];
			coord = ol.proj.transform(coord, 'EPSG:4326', 'EPSG:3857');
			var point_feature = new ol.Feature({
				geometry: new ol.geom.Point(coord),
				info: row
			});
			pfLst.push(point_feature);
		}
	});

	var marker = new ol.layer.Group({
		title: 'Capas de información',
		layers: [
			new ol.layer.Vector({
				source: new ol.source.Vector({
					features: pfLst
				}),
				style: iconStyle
			})
		]
	});
	markerLst.push(marker);
	map.addLayer(marker);
}

function initMarkerPerson(coordinates, center){
    clearMarkerPerson();
	coordinates = ol.proj.transform(coordinates, 'EPSG:4326', 'EPSG:3857');
    var point_feature = new ol.Feature({
        geometry: new ol.geom.Point(coordinates)
    });

    var iconStyle = new ol.style.Style({
        image: new ol.style.Icon(({
            anchor: [0.5, 30],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            src: '../img/ubicar-marker-green.png'
        }))
    });

    var circleStyle = new ol.style.Style({
        image: new ol.style.Circle({
            radius: 5,
            snapToPixel: false,
            //fill: new ol.style.Fill({color: 'black', opacity: 0}),
            stroke: new ol.style.Stroke({
                color: 'black', width: 2
            })
        })
    });

    mapInfoPadromesMarker = new ol.layer.Group({
        title: 'Capas de información',
        layers: [
            new ol.layer.Vector({
                source: new ol.source.Vector({
                    features: [point_feature],
                    ignorame: true
                }),
                style: iconStyle,
                ignorame: true
            }),
            new ol.layer.Vector({
                source: new ol.source.Vector({
                    features: [point_feature],
                    ignorame: true
                }),
                style: circleStyle,
                ignorame: true
            })
        ],
        ignorame: true
    });

    map.addLayer(mapInfoPadromesMarker);
    if(center){
        map.getView().setZoom(19);
        map.getView().setCenter(coordinates);
    }
}

function initMarkerInfoPadrones(coordinates){
    var point_feature = new ol.Feature({
        geometry: new ol.geom.Point(coordinates)
    });

    var iconStyle = new ol.style.Style({
        image: new ol.style.Icon(({
            anchor: [0.5, 30],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            src: '../img/ubicar-marker-green.png'
        }))
    });

    var circleStyle = new ol.style.Style({
        image: new ol.style.Circle({
            radius: 5,
            snapToPixel: false,
            //fill: new ol.style.Fill({color: 'black', opacity: 0}),
            stroke: new ol.style.Stroke({
                color: 'black', width: 2
            })
        })
    });

    mapInfoPadromesMarker = new ol.layer.Group({
        title: 'Capas de información',
        layers: [
            new ol.layer.Vector({
                source: new ol.source.Vector({
                    features: [point_feature]
                }),
                style: iconStyle
            }),
            new ol.layer.Vector({
                source: new ol.source.Vector({
                    features: [point_feature]
                }),
                style: circleStyle
            })
        ]
    });

}

function toggleResultados(){
    if(LEFT_HIDE){
        //oculto
        $('#resultados').removeClass("open");
        // $('#resultados').colExpToggle();
        // $('#resultados').css( { "left" : SHOW_POSITION } );
        LEFT_HIDE = false;
    }else{
        // $('#resultados').css( { "left" : HIDE_POSITION } );
        $('#resultados').addClass("open");
        // $('#resultados').colExpToggle();
        LEFT_HIDE = true;
    }
}

function updateSelect(data){
    var select = $("#area-select");
    if(select2_init){
        select.select2('destroy');
    }else{
        select2_init = true;
    }
    select.empty().select2({
        theme: "bootstrap",
        width: '100%',
        placeholder: "Seleccione uno o más",
        language: "es",
        allowClear: true,
        data: data
    });
    select.select2('open');
    if($("#persona-input").is(":visible")){
        $("#persona-input").hide();
        $('#area-select').parent().find('.select2').show();
    }
	if($("#direccion-input").is(":visible")){
        $("#direccion-input").hide();
    }
	if($(".clientes-por-pagar").is(":visible")){
        $(".clientes-por-pagar").hide();
        $('#area-select').parent().find('.select2').show();
    }
}

function updateSelectColonias(data){
    var select = $("#colonias-select");
    if(select2_1_init){
        select.select2('destroy');
    }else{
        select2_1_init = true;
    }
    select.empty().select2({
        theme: "bootstrap",
        width: '100%',
        placeholder: "Seleccione uno o más",
        language: "es",
        allowClear: true,
        data: data
    });
}

function updateSelectColonias2(data){
    var select = $("#direccion-colonia");
    if(select2_2_init){
        select.select2('destroy');
    }else{
        select2_2_init = true;
    }
    select.empty().select2({
        theme: "bootstrap",
        width: '100%',
        placeholder: "Colonia",
        language: "es",
		multiple: true,
		maximumSelectionLength: 1,
        data: data
    });
}



function getSearchType(){
    return $('#search-category').val();
}
function getSearchLabel(){
    return $('#search-category option:selected').text();
}
function getSearchValue(){
    if(getSearchType() === 'NOMBRE' 
	||  getSearchType() === "COMERCIO" 
	||  getSearchType() === "CLAVE"){
        var query = [];
        $.each($("#persona-input input").val().split(' '), function(){
            if($.trim(this) != "")
				query.push('%'+$.trim(this)+'%');
        });
        return query;
    }else if(getSearchType() === "PERSONA"){
        return "%" + $("#persona-input input").val() + "%";
    }else if(getSearchType() === "CLIENTES_POR_PAGAR"){
		var query = [];
        query.push($("#txtDCustomerB").val());
		query.push($("#txtDCustomerE").val());
        return query;
	}else{
        var data = $("#area-select").select2('data');
        var labels = [];
        for(var i=0; i < data.length; i++){
            labels.push("%"+data[i].text.toLowerCase()+"%");
        }
        return labels;
    }
}
function getSearchValueLabel(){
    if(getSearchType() === 'NOMBRE'
	|| getSearchType() === 'PERSONA' 
	|| getSearchType() === "COMERCIO" 
	||  getSearchType() === "CLAVE"){
        return $("#persona-input input").val();
    }else if(getSearchType() === "CLIENTES_POR_PAGAR"){
		return $("#txtDCustomerB").text()+","+$("#txtDCustomerE").text();
	}else{
        var data = $("#area-select").select2('data');
        var labels = [];
        for(var i=0; i < data.length; i++){
            labels.push(data[i].text);
        }
        return labels.join(",");
    }
}

function createTableSummary(id, type, query){
    if($("#dt-data-panel" + id).length > 0){
        $("#dt-data-panel" + id).show();
        return;
    }
    var valGeometry = "";
    if(typeGlobalSearch == "POINT"){

    }
    else if(typeGlobalSearch == "POINT"){

    }

    if(!Array.isArray(query))
        query = query.replace("%", "");
    var data = {
        type: type,
        query: query
    }

    $.ajax({
        url: "/analysis/datatable/" + id,
        data: $.param(data),
        success: function(resp){
            $("#tabla-datos").append(resp);
            $("#dt-data-panel" + id).show();
        }
    });
}
var activeArea = null;
var areas = {};
function busquedaGlobal(type, query) {
    clearSearch();
    var summaryLabel = "";
    if (!type && !query) {
        type = getSearchType();
        query = getSearchValue();
        summaryLabel = "(" + getSearchLabel() + ") " + getSearchValueLabel();
    }else{
        if(type === 'GEOMETRIA'){
            summaryLabel = "Área de influencia"
        }
        if(type == "POINT"){
            summaryLabel = "Predio"
        }
    }
    typeGlobalSearch = type;
    valueGlobalSearch = query;
    buscar(type);
}

function summaryLayerChange(tableid, switchid, layerid){
    var checked = document.getElementById(switchid).checked;

    var row = summaryControl.getLayer(layerid);
    if(row.layer.getVisible() !== checked){
        row.layer.setVisible(checked);

        var table = document.getElementById(tableid);
        var lbl_ubicados = $(table).find('.ubicados');
        var lbl_total = $(table).find('.total');
        if(checked){
            lbl_ubicados.text(parseInt(lbl_ubicados.text()) + row.ubicados);
            lbl_total.text(parseInt(lbl_total.text()) + row.total);
        }else{
            lbl_ubicados.text(parseInt(lbl_ubicados.text()) - row.ubicados);
            lbl_total.text(parseInt(lbl_total.text()) - row.total);
        }
    }
    summaryControl.fitToLayers();
}

function clearSearch(clearMarker){
    clearMarkerPerson();
    $("#tabla-datos .panel").each(function(){
        $(this).remove();
    });
}

function handleKeyPress(e){
    var key=e.keyCode || e.which;
    if (key==13 && $.trim($("#persona-input input").val()) !== ''){
        busquedaGlobal();
    }else if (key==13 && ($.trim($("#direccion-calle").val()) !== '' || $.trim($("#direccion-numero").val()) !== '')){
        busquedaGlobal();
    }
}

function getLocationFilterValue(){
    return $('.filter-located .fa-check').attr('data-value');
}

function clearMapResults(){

    clearSearch();
	clearAllMarkers();
    $('#search-category').val($('#search-category option:first-child').val()).trigger('change');
	var element = document.getElementById('popup');
	$(element).popover('destroy');    

    if(activeArea){
        map.removeLayer(activeArea);
    }

    //summaryControl.clear();
    if(LEFT_HIDE){
        toggleResultados();
    }
    initDT();
    ineDT.column(15).search("ALGOMUYDIFICILDEENCONTRAR");

    //infoPredio._deactivateInfo();
    getLegendControl.hidePanel();
}

function setHeightContainers(){
    var navBarHeight = $("#navbar-container").outerHeight(true);
    var busquedaHeight = $("#resultados").next().outerHeight(true);
    var marginPanelMapa = 90;
    var paddingResultados = 82;
    var topDT = 165;
    var bottomDT = 70;
    $('#analysis-map').height($(window).height() - navBarHeight - busquedaHeight - marginPanelMapa);
    if(map) map.updateSize();
    $('#resultados').height($(window).height() - navBarHeight - paddingResultados);
    $('#ine-datatable_wrapper .dataTables_scrollBody').height($(window).height() - topDT - bottomDT - navBarHeight);
}

function activeDragComponents(component, resizable){
    $(component).draggable("enable");
    if(resizable) $(resizable).resizable( "enable" );
}

function deactiveDragComponents(component, resizable){
    $(component).draggable("disable");
    if(resizable) $(resizable).resizable( "disable" );
}

function checkWithDragAndResizeComponent(component, resizable){
    if($(window).outerWidth() > 767){
        activeDragComponents(component, resizable);
    }
    else{
        deactiveDragComponents(component, resizable);
    }
}