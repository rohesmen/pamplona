$(function() {
    $('.input-group.date').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });

    $('.input-group.date').datepicker('setDate',new Date());

    $("#generarBitacora").off("click");
    $("#generarBitacora").on("click", function(){
		var date = $('#fInicio').val();
		if(date){
			$("#bitacora-datatable").jsGrid("destroy");
			$.ajax({
				url: "/bitacora/bitacora",
				data: JSON.stringify({
					fecha: date
				}),
				type: "POST",
				beforeSend: function(){
					$('#bitacora-datatable-container').html('<div style="text-align: center;">' +
						'<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
						'</div>');
				},
				success: function (resp) {
					var json = JSON.parse(resp);
					initTable(json.bitacora, json.choferes, json.recolectores, json.turnos, json.supervisores, json.coordinadores);
				},
				error: function(){
					showGeneralMessage("Ocurrió un error al generar la bitácora", "error", true);
				},
				complete: function () {
					$('#bitacora-datatable-container').empty();
				}
			});
		}
    });

});