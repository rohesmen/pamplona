var clientesDT;
var historialDT;
var selec = new Array();
var uSelec = -1;
var idCliente = 0;
var marker;
var map;
var geocoder = null;
var mensualidadDT;
var RecibosDT = null;
var RSocialDT = null;
var PagosMasivosDT = null;
var month = new Array();
var idsSelected = [];
var dataidsSelected = [];
var ultimoAnio = 0;
var pdfFacturaBase64 = null;
var isFactura = false;

Dropzone.autoDiscover = false;

$(document).ready(function() {
	initDropzonePDFFactura();
	month[1] = "Enero";
	month[2] = "Febrero";
	month[3] = "Marzo";
	month[4] = "Abril";
	month[5] = "Mayo";
	month[6] = "Junio";
	month[7] = "Julio";
	month[8] = "Agosto";
	month[9] = "Septiembre";
	month[10] = "Octubre";
	month[11] = "Noviembre";
	month[12] = "Diciembre";

	$(".clientes-bitmens").on( 'click', function () {
		if(clientesDT.rows('.selected').data().length == 0){
			showGeneralMessage("Seleccione un registro", "warning", true);
			return ;
		}//fin:if(clientesDT.rows('.selected').data().length == 0)
	
		var rowData = clientesDT.rows('tr.selected').data();
		if(rowData[0].length <= 0){return;}//fin:
	
		$("#loading-cancelar").html('');
		//Obtener el historial de la base de datos
		id = parseInt(rowData[0][0]);
		var _this = this;
		$.ajax({
			url: "/clientes/bitmens/" + id,
			beforeSend: function(){
				$(_this).prop("disabled", true);
				$(_this).html('<i class="fa fa-spin fa-spinner"></i> Bit. mens.');
			},
			success: function (resp) {
				$("body").append(resp);
			},
			error: function () {
				showGeneralMessage("Ocurrió un error al obtener la informacion", "warning", true);
			},
			complete: function(){
				$(_this).html('<i class="fa fa-list"></i> Bit. mens.');
				$(_this).prop("disabled", false);
			}
		});
	});
	

	$("#contenedor-filtros :input[type=text]").on('keypress', function(e){
		if(e.which == 13){
			buscarCliente(true);
		}
	});

	$("#clientes-folio-val").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
	$("#clientes-clave-val").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
	
	//Configuracion el envio del Recibo por email
	$("#edtEmailTo").inputmask({ alias: "email", clearIncomplete: true});
	
	$("#edt-servicio-monto").inputmask('Regex', {regex: "^[0-9]{1,9}(\\.\\d{1,2})?$"});
	$("#edtPerMonto").inputmask('Regex', {regex: "^[0-9]{1,6}(\\.\\d{1,2})?$"});

	$("#chkFactCobro").off("click");
	$("#chkFactCobro").on("click", function(){
		var checked = this.checked;
		$("#folio_factura").val("");
		if(checked){
			$("#folio_factura").prop("disabled", false);
		}
		else{
			$("#folio_factura").prop("disabled", true);
		}
	});
	//-------------------------------------------------------------------------
	clientesDT = $("#clientes-datatable").DataTable({
		"dom": '<"clear"><rt><"bottom"lip><"clear">',
		"autoWidth": false,
		"paging": true,
		"info": true,
		"processing": true,
		"searching": false,
		"select": true,
		"scrollCollapse": false,
		"pagingType": "full",
		"language": {
			"paginate": {
				"next": ">",
				"first": "<<",
				"last": ">>",
				"previous": "<"
			},
			"search": "",
			"searchPlaceholder": "Buscar en los resultados encontrados",
			"info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
			"infoEmpty": "",
			"infoFiltered": " - filtrado de _MAX_",
			"emptyTable": "Sin resultados",
			"sZeroRecords": "Sin resultados",
			processing: "Procesando ...",
			"lengthMenu": "Mostrar _MENU_ registros"
		},
		"lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
		"pageLength": 10,
		responsive: true,
		//"order": [[ 3, 'desc' ]],
        "columnDefs": [
            // { targets: [0,2], className: "no-wrap text-center", orderable: false, searchable: false },
            // { targets: 1, className: 'control', orderable: false, searchable: false },
            // { type: 'date', targets: 7 },
            // { type: 'date', targets: 8 },
            { targets: [8,9,10,11,12,13,14,15,16,17,18,19,20,21,22, 24,26,27,28, 29],  visible: false },
            { targets: '_all',  visible: true, orderable: true, searchable: true }
        ],
		"processing": true,
		"deferLoading": 0
	});
	//-------------------------------------------------------------------------				
	historialDT = $('#historial-table').DataTable({
		"dom": '<"clear"><rt><"bottom"lip><"clear">',		
		"paging": true,
		"autoWidth": true,
		"select": true, 
		"info": true,
		"searching": false,
		"processing": true,		
		"ordering":false,
		"pagingType": "full",
		"language": {
			"paginate": {
				"next": ">",
				"first": "<<",
				"last": ">>",
				"previous": "<"
			},
			"search": "",
			"searchPlaceholder": "Buscar en los resultados encontrados",
			"info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
			"infoEmpty": "",
			"infoFiltered": " - filtrado de _MAX_",
			"emptyTable": "Sin resultados",
			"sZeroRecords": "Sin resultados",
			"processing": "Procesando ...",
			"lengthMenu": "Mostrar _MENU_ registros"
		},
		"pageLength": 10,
		responsive: {
            details: {
                type: 'column',
                target: 0
            }
        },
		"deferLoading": 0,				
		"scrollCollapse": true,
		"scrollY":       "40vh",
		"columnDefs": [
			{ "name": "resp", data: null, defaultContent: "", className: 'control', orderable: false, searchable: false, "targets": 0},
			{ "name": "fecha_pago", "data": "fecha_pago", "targets": 1 },
			{ "name": "idcliente",   		"data": "idcliente", "targets": 2 },
			{ "name": "folio_interno", 	"data": "folio_interno", "targets": 3 },
			{ "name": "usuario", 	"data": "usuario", "targets":  4},
			{ "name": "activo", 		"data": "activo", "targets":  5},
			{ "name": "subtotal", 		"data": "subtotal", "targets":  6},
			{ "name": "comision", 	"data": "comision", "targets":  7},
			{ "name": "total", 	"data": "total", "targets":  8},
			{ "name": "paypal", "data": "paypal", "targets":  9},
			{ "name": "encorte", 	"data": "encorte", "targets":  10},
			{ "name": "idpago", 	"data": "idpago", "targets": 11, visible: false, 'searchable': false,
				'orderable': false},
			{ "name": "pensiondado", 	"data": "pensiondado", "targets":  12},
			{ "name": "iddescuento_pensionado", 	"data": "iddescuento_pensionado", "targets": 13, visible: false, 'searchable': false,
				'orderable': false},
			{ "name": "impresiones", 	"data": "impresiones", "targets":  14},
			{ "name": "isnegociacion", 	"data": "isnegociacion", "targets":  15},
			{
				'targets': 5,
				'searchable': false, 
				'orderable': false,
				'className': 'dt-body-center',
				'render': function (data, type, full, meta){
					if(data == "1"){
						return '<input type=\"checkbox\" checked value="' + data + '" disabled />';
					}//fin:if 
					else{return '<input type=\"checkbox\" value="' + data + '" disabled />';}
				}
			}
        ]    		
	});


	//-------------------------------------------------------------------------
	datoshistorialDT = $('#datos-historial-table').DataTable({
		"dom": '<"clear"><rt><"bottom"lip><"clear">',
		"paging": true,
		"autoWidth": true,
		"select": true,
		"info": true,
		"searching": false,
		"processing": true,
		"ordering":false,
		"pagingType": "full",
		"language": {
			"paginate": {
				"next": ">",
				"first": "<<",
				"last": ">>",
				"previous": "<"
			},
			"search": "",
			"searchPlaceholder": "Buscar en los resultados encontrados",
			"info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
			"infoEmpty": "",
			"infoFiltered": " - filtrado de _MAX_",
			"emptyTable": "Sin resultados",
			"sZeroRecords": "Sin resultados",
			"processing": "Procesando ...",
			"lengthMenu": "Mostrar _MENU_ registros"
		},
		"pageLength": 10,
		"responsive": true,
		"deferLoading": 0,
		"scrollCollapse": true,
		"scrollY":       "40vh",
		"columnDefs": [
			{ "name": "anio", 		"data": "anio", "targets": 0 },
			{ "name": "mes",   		"data": "mes", "targets": 1 },
			{ "name": "descto", 	"data": "descto", "targets":  2},
			{ "name": "total", 		"data": "total", "targets":  3},
		]
	});


    //-------------------------------------------------------------------------
	mensualidadDT = $('#mensualidad-table').DataTable({
		"paging": false,
		"autoWidth": true,
		// "select": true,
		"info": false,
		"searching": false,
		"ordering":false,
        "language": {
			"infoEmpty": "",
			"emptyTable": "Sin resultados",
			"sZeroRecords": "Sin resultados"
		},
		columnDefs: [ { targets: 0, className: 'select-checkbox', orderable: false, searchable: false,
            'render': function (data, type, full, meta){
				return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                    
            }
        }],
         select: {
            style:    'os',
            selector: 'td:first-child'
        },		
		"scrollCollapse": true,
		"scrollY":        "200px",
        "createdRow": function( row, data, dataIndex ) {
            //$(row).css( "background-color", data[2] );
            //$(row).find('td:eq(5)').css('color', data[11]);
            $(row).attr('id', "rowPago"+ dataIndex);
            // var data = data[0][12];


        }
	});
	mensualidadDT.column(5).visible(false);
	mensualidadDT.column(6).visible(false);
	//-------------------------------------------------------------------------
	//DataTable para cobro de Mensualidad por razon social
	RSocialDT = $('#rsocial-table').DataTable({
		"paging": false,
		"autoWidth": true,
		"select": true,
		"info": false,
		"searching": false,
		"ordering":false,
		"responsive": true,
		"language": {
            "infoEmpty": "",
            "emptyTable": "Sin resultados",
			"sZeroRecords": "Sin resultados"
        },
		"scrollCollapse": true,
		"scrollY":        "100px",
		"columnDefs": [
			{ "name": "anio_mes", 	 	"data": "anio_mes", "targets": 0 },
			{ "name": "cliente", 	 	"data": "cliente", "targets": 1 },
			{ "name": "mensualidad",	"data": "mensualidad", "targets": 2 },
			{ "name": "descuento", 		"data": "descuento", "targets": 3 },			
			{ "name": "total", 			"data": "total", "targets":  4},	
			{ "name": "annio", 			"data": "annio", "targets":  5},
			{ "name": "imes", 			"data": "imes", "targets":  6},
			{ "name": "idcliente", 		"data": "idcliente", "targets":  7},
			{ "name": "monto_mes", 		"data": "monto_mes", "targets":  8},
			{ "name": "ultaniopago", 	"data": "ultaniopago", "targets":  9},
			{ "name": "ultmespago", 	"data": "ultmespago", "targets":  10},
			{ "name": "idrazonsocial", 	"data": "idrazonsocial", "targets": 11 }			
        ]
    });
	
	//RSocialDT.column(4).visible(false);
	RSocialDT.column(5).visible(false);	
	RSocialDT.column(6).visible(false);
	RSocialDT.column(7).visible(false);
	RSocialDT.column(8).visible(false);
	RSocialDT.column(9).visible(false);
	RSocialDT.column(10).visible(false);
	RSocialDT.column(11).visible(false);
	//RSocialDT.column(12).visible(false);
	//RSocialDT.column(13).visible(false);
	//RSocialDT.column(14).visible(false);
	//-------------------------------------------------------------------------	
	//DataTable para la reimpresion de recibos
	RecibosDT = $('#table-recibos').DataTable({
		"dom": '<"clear"><rt><"bottom"lip><"clear">',		
        "paging": true,
		"autoWidth": true,
		"select": true, 
        "info": true,
        "searching": false,
		"processing": true,		
		"ordering":false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
			"processing": "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
		},				
		"pageLength": 10,							
		"responsive": true,
		"deferLoading": 0,				
        "scrollCollapse": true,
		"scrollY":       "40vh",
		"columnDefs": [
			{ "name": "idpago", 	 "data": "folio", "targets": 0 },
			{ "name": "fecpago",   	 "data": "fecpago", "targets": 1 },
			{ "name": "feccreacion", "data": "feccreacion", "targets": 2 },
			{ "name": "subtotal", 	 "data": "subtotal", "targets":  3},
			{ "name": "descto", 	 "data": "descto", "targets":  4},
			{ "name": "total", 		 "data": "total", "targets":  5},
			{ "name": "activo", 	 "data": "activo", "targets":  6},						
			{ "name": "tipocobro", 	 "data": "tipocobro", "targets": 7 },
			{ "name": "folio", 	 "data": "folio", "targets": 8 },
			{ "name": "cobratario", 	 "data": "cobratario", "targets": 9 },
			{
				'targets': 6, 
				'searchable': false, 
				'orderable': false,
				'className': 'dt-body-center',
				'render': function (data, type, full, meta){
					if(data == "1"){
						return '<input type=\"checkbox\" checked value="' + data + '" disabled />';
					}//fin:if 
					else{return '<input type=\"checkbox\" value="' + data + '" disabled />';}
				}
			}
		]    		
	});	
	RecibosDT.column(7).visible(false);
	RecibosDT.column(8).visible(false);
	RecibosDT.column(9).visible(false);
	RecibosDT.draw();
	//Configuracion para seleccionar un solo renglon
	$('#table-recibos tbody').on( 'click', 'tr', function (){
		if ($(this).hasClass('selected') ) {
			$(this).removeClass('selected');
			if($(this).rowIndex == 0)
				uSelec = -1;
		}//fin:else
		else{         
			RecibosDT.$('tr.selected').removeClass('selected');
			$(this).toggleClass('selected');
		}//fin:else			
	});	
	
	//configuracion para los controles de Fecha
	var dtNow = new Date();
	var dd_now = dtNow.getDate();
	var mm_now = dtNow.getMonth() + 1;
	var yyyy_now = dtNow.getFullYear();
	var stFechaNow = dd_now.toString() + "/" + mm_now.toString() + "/" + yyyy_now.toString();

	var input_fecini = $('input[name="edt-rprint-fechaini"]');
	input_fecini.datepicker({		
		format: 'dd/mm/yyyy',
		container: "body",
		todayHighlight: true,
		autoclose: true,
		language: "es"		
	});	
				
	var input_fecfin = $('input[name="edt-rprint-fechafin"]');
	input_fecfin.datepicker({
		format: 'dd/mm/yyyy',				
		container: "body",
		todayHighlight: true,
		autoclose: true,
		language: "es"
	});			
	
	//Configurar mascara para la fechas
	$("#edt-rprint-fechaini").inputmask("99/99/9999");
	$("#edt-rprint-fechafin").inputmask("99/99/9999");
	//-------------------------------------------------------------------------
	$('#clientes-datatable tbody').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected') ) {
			$(this).removeClass('selected');
			$("#btnCobComFact").prop("disabled", true);
			$("#btnCobrarCLiente").prop("disabled", true);
			$("#btnCobrarDescuento").prop("disabled", true);
		}
		else {
			clientesDT.$('tr.selected').removeClass('selected');
			$(this).toggleClass('selected');	
			
			var dtsRows = clientesDT.row(this).data();					
			var mlatitud = 0.00;
			var mlongitud = 0.00;			
			
			try{
				mlatitud = parseFloat(dtsRows[10]);
				mlongitud = parseFloat(dtsRows[11]);
				if(isNaN(mlatitud) || isNaN(mlongitud)){			
					$("#btn-ubicacion").removeClass("btn btn-primary btn-sm").addClass("btn btn-danger btn-sm");					
				}//fin:if
				else{
					$("#btn-ubicacion").removeClass("btn btn-danger btn-sm").addClass("btn btn-primary btn-sm");
				}//fin:else
			}//fin:try
			catch(err){$("#btn-ubicacion").removeClass("btn btn-primary btn-sm").addClass("btn btn-danger btn-sm");}

			var isfactura = dtsRows[27];
			$("#btnCobComFact").prop("disabled", true);
			$("#btnCobrarCLiente" ).prop("disabled", true);
			$("#btnCobrarDescuento").prop("disabled", true);
			if(isfactura){
				$("#btnCobComFact." + dtsRows[28]).prop("disabled", false);
			}
			else{
				$("#btnCobrarCLiente." + dtsRows[28]).prop("disabled", false);
				$("#btnCobrarDescuento").prop("disabled", false);
			}

			if(dtsRows[29] == false){
				$("#btnCobComFact").prop("disabled", true);
				$("#btnCobrarCLiente").prop("disabled", true);
				$("#btnCobrarDescuento").prop("disabled", true);
			}

		}//fin:else
	});

	$('#historial-table tbody').on( 'click', 'tr', function (){
		if ($(this).hasClass('selected') ) {
			$(this).removeClass('selected');
			if($(this).rowIndex == 0)
				uSelec = -1;
		}
		else{         
			historialDT.$('tr.selected').removeClass('selected');
            $(this).toggleClass('selected');
		}
	});

	$(".clientes-search-do").off('click');
	$(".clientes-search-val").off('keypress');
	$(".clientes-search-do").on('click', function(){
		buscarCliente(true);
	});

	$(".cliente-search-val").on('keypress', function(e){
		if(e.which == 13){ buscarCliente(true);}
	});

	$("#clientes-filter-select").off("change");
	$("#clientes-filter-select").on("change", function(){

        var valor = this.value;

        $("#clientes-filtro-todos").hide();
		$("#clientes-filtro-nombre").hide();
		$("#clientes-filtro-direccion").hide();
		$("#clientes-filtro-clave").hide();
		$("#clientes-filtro-todos").hide();
		$("#clientes-filtro-folio").hide();
		$("#clientes-filtro-empresa").hide();
        $("#clientes-filtro-privada").hide();

		$("#clientes-nombre-val").val("");
		$("#clientes-calle-val").val("");
		$("#clientes-numero-val").val("");
		$("#clientes-colonia-val").val("");
		$("#clientes-clave-val").val("");
        $("#clientes-empresa-val").val("");
		$("#clientes-folio-val").val("");
        $("#clientes-privada-val").val("");

        switch(valor){
            case "todos":   $("#clientes-filtro-todos").show();break;
			case "nom":	    $("#clientes-filtro-nombre").show();break;
            case "dir":	    $("#clientes-filtro-direccion").show();break;
            case "cla":	    $("#clientes-filtro-clave").show();break;
            case "rsocial": $("#clientes-filtro-empresa").show();break;
			case "folio":   $("#clientes-filtro-folio").show();break;
            case "privada":   $("#clientes-filtro-privada").show();break;
		}//fin:switch(valor)
	});
    //-----------------------------------------------------------------------
    $('#modalUbicacion').on('shown.bs.modal', function() {
        var currentCenter = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(currentCenter);
    });
	//-----------------------------------------------------------------------
	$("#select-basura-fpago").off("change");
	$("#select-basura-fpago").on("change", function(){
		var valor = this.value;
		var mreferencia = $(this).find("option:selected").attr("data-referencia");
		$("#referencia_bancaria").val("");
		$("#referencia_bancaria").prop("disabled", true);
		if(mreferencia == "si"){
			$("#referencia_bancaria").prop("disabled", false);
		}
	});

    $("#select-servicio-fpago").off("change");
	$("#select-servicio-fpago").on("change", function(){
		var valor = this.value;
		var mreferencia = $(this).find("option:selected").attr("data-referencia");
		$("#edt-servicio-ref").val("");
		$("#edt-servicio-ref").prop('disabled', true);
		if(mreferencia == "si"){
			$("#edt-servicio-ref").prop('disabled', false)
		}		
	});
    //-----------------------------------------------------------------------
    //Configuracion de datepicker para la hora
    var date_input=$('input[name="edt-servicio-fecha"]');
    date_input.datepicker({
        format: 'yyyy-mm-dd',
        container: "body",
        todayHighlight: true,
        autoclose: true,
        language: "es"
    });
	//-----------------------------------------------------------------------
	//Iniciar los combos para mostrar aÃ±o y mes de pago
	var iAnnioActual = (new Date()).getFullYear() + 10;
	iMinAnnio = (new Date()).getFullYear() + 1;
	for(idx = iAnnioActual; idx >= iMinAnnio; idx--){
        $("#select-cobro-anio").append($('<option>', {value: idx, text: idx}));
		$("#select-masivo-anio").append($('<option>', {value: idx, text: idx}));
	}//fin:for


	//-----------------------------------------------------------------------
	$('#modal-cobro-cliente').on('hidden.bs.modal', function() {
		rutaDropzoneFactura.removeAllFiles();
		pdfFacturaBase64 = null;
		isdescan = false;
	});
	$('#modal-cobro-cliente').on('shown.bs.modal', function() {
		isdescan = false;
		var rowData = clientesDT.row("tr.selected").data();
		//console.log(rowData);
        idsSelected = [];
        dataidsSelected = [];
        $("#totalPagar").val("");
		mensualidadDT.clear();
		mensualidadDT.draw();
        $("#select-cobro-anio").val(iMinAnnio);

		var cobro_manual = $("#hdd-cobro_manual").val();
		ftMensualidad = 0;
		if(cobro_manual == 'true'){
			$("#group_cobro_manual").show();
			$("#label_cobro_manual").text("SI");
			ftMensualidad = parseFloat($("#hdd-monto_cobro").val());
			$("#monto_cobro").val(ftMensualidad);
		}else{
			ftMensualidad = $("#hdd-mensualidad").val() != "" ? parseFloat($("#hdd-mensualidad").val()) : 0;
			$("#group_cobro_manual").hide();
			$("#label_cobro_manual").text("NO");
		}
		

		// $("#chkDescuento").prop('disabled', true);
		$('#aviso-adeudo').html("");

		//Realizar calculo para verificar si existe un adeudo superior a los 12 meses
		var vannio_ultimo = $("#edt-cobro-ultanio").val() != "" ? parseInt($("#edt-cobro-ultanio").val()) : 0;
		var vmes_ultimo = $("#hdd-ultpago-mes").val() != ""? parseInt($("#hdd-ultpago-mes").val()) : 0;
		//var annio_actual = (new Date()).getFullYear() - 1;
		var annio_actual = (new Date()).getFullYear();
		var mes_actual = (new Date()).getMonth() + 1;

		var iIndice = 0;
		var mesIterare = 1;

		var dtfechaIni = new Date(1900,0,1);
		var dtfechaFin = new Date(1900,0,1);
		var dtUltimoPago = new Date(vannio_ultimo, (vmes_ultimo -1), 1);

		var arrayMesesAux = new Array();

		if(vannio_ultimo > 0 && vmes_ultimo > 0){

			dtfechaIni.setFullYear(vannio_ultimo);
			dtfechaIni.setMonth(vmes_ultimo - 1);

			dtfechaFin.setFullYear(annio_actual);
			dtfechaFin.setMonth(11);

			mesIterare = vmes_ultimo;
			
			if(vannio_ultimo >= annio_actual) {
                annio_actual = vannio_ultimo;
                dtfechaFin.setFullYear(annio_actual);
            }

			for(iIdy = vannio_ultimo; iIdy <= annio_actual; iIdy++){
				if(mesIterare > 12){mesIterare = 1}
				for(iIdx = mesIterare; iIdx <= 12; iIdx++){
					if(rowData[23] == "NO"){
						if((iIdy == 2020 && mesIterare == 4) || (iIdy == 2020 && mesIterare == 5)){
							mesIterare++;
							continue;
						}
					}

					mesIterare++;
					dtfechaIni.setFullYear(iIdy);
					dtfechaIni.setMonth(iIdx - 1);
					if(dtUltimoPago.getTime() < dtfechaIni.getTime() &&
						dtfechaIni.getTime() <= dtfechaFin.getTime()){
						var mesanioaux = month[iIdx]  + " " + iIdy.toString();
						arrayMesesAux[iIndice++] = iIdy.toString() + "-" + iIdx.toString();
                        ultimoAnio = iIdy;
						fnAddMesAtraso(mesanioaux, ftMensualidad, 0, ftMensualidad, iIdy.toString(), iIdx.toString());
					}//fin:
					//else{iIdx = 13}
				}//fin:for
			}//fin:for

			// $("#chkDescuento").prop('disabled', false);
			if(arrayMesesAux.length >= 12){
				//Habilitamos el boton para descuento:

				var htmlContent ="<div class='alert alert-warning alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
				htmlContent += "<strong>Tiene un adeudo superior a los 12 meses</strong></div>";
				$('#aviso-adeudo').html(htmlContent);
			}//fin:
		}//fin:if(vannio_ultimo > 0 && vmes_ultimo)		
	});
    //-----------------------------------------------------------------------
	//Inicializacion del select de colonias
	$('#clientes-colonia-val').selectize({
		create: true,
		sortField: 'text'
	});
	//-----------------------------------------------------------------------
	$('#modalHistorial').on('show.bs.modal', function (){
       $(this).find('.modal-body').css({
              width:'auto', 
              height:'auto', 
			  'padding-top': '10px',
			  'padding-bottom': '10px',
              'max-height':'100%'
       });
	});	
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	//configurando el modal para el envio de correo
	$('#modalEmailRecibo').on('hidden.bs.modal', function (e) {
		if(isABootstrapModalOpen()){
			$("body").addClass("modal-open");
		}//fin:if
	});
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@	
	$('#modalRecibo').on('hidden.bs.modal', function (e) {
		if(isABootstrapModalOpen()){
			$("body").addClass("modal-open");
		}//fin:if
	});		
	
	//Configuracion de Controles para el envio masivo	
	$('#select-masivo-rsocial').selectize({
		persist: false,
		sortField: 'text',		
		create: true,
		render:{
			option: function (data, escape) {			
				return "<div data-fisica='" + data.fisica + "'>" + data.text + "</div>"
			}
		}		
	});
	
	$("#select-masivo-rsocial").off("change");
	$("#select-masivo-rsocial").on("change", function(){
		//var valor = this.value;	
		//var txtvalue = $(this).find("option:selected").text();
		//$("#hdd-rsocial-id").val(valor);
		//$("#edt-masivo-rsocial").val(txtvalue);
	});
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	$('#modalCobroMasivo').on('shown.bs.modal', function() {		
		RSocialDT.clear();
		RSocialDT.draw();				
		//$("#chkDescuento").prop('disabled', true);
		$('#aviso-adeudo-masivo').html("");
					
	});//fin:$('#modalCobroMasivo').on('shown.bs.modal', function()
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	//Configuracion del check para la captura del folio de Factura
	$("#chkmasFactura").off("click");
	$("#chkmasFactura").on("click", function(){
		var checked = this.checked;
		$("#edt-masivo-folfac").val("");
		if(checked){$("#edt-masivo-folfac").prop("disabled", false);}
		else{$("#edt-masivo-folfac").prop("disabled", true);}
	});
	
	//Configuracion del select para la forma de pago en el modal de cobro masivo
	$("#select-masivo-fpago").off("change");
	$("#select-masivo-fpago").on("change", function(){
		var valor = this.value;
		var mref = $(this).find("option:selected").attr("data-referencia");
		$("#edt-masivo-referencia").val("");
		$("#edt-masivo-referencia").prop("disabled", true);
		if(mref == "si"){$("#edt-masivo-referencia").prop("disabled", false);}
	});
	
	//Configuracion del Evento para Calcular el descuento por monto
	$("#chkDesctoMasivo").off("click");
	$("#chkDesctoMasivo").on("click", function(){
		var checked = this.checked;
		//$("#edt-masivo-folfac").val("");
		//if(checked){$("#edt-masivo-folfac").prop("disabled", false);}
		//else{$("#edt-masivo-folfac").prop("disabled", true);}

	});

	$("#chkDescuento").off("click");
	$("#chkDescuento").on("click", function(){
		var checkedChk = this.checked;
		//$("#edt-masivo-folfac").val("");
		//if(checked){$("#edt-masivo-folfac").prop("disabled", false);}
		//else{$("#edt-masivo-folfac").prop("disabled", true);}
		$('#mensualidad-table tbody input[type="checkbox"]:checked').each(function(e){
			var $tr  = $(this).closest("tr");
			var data = mensualidadDT.row($tr).data();
			var id = mensualidadDT.row($tr).index();

			data[3] = checkedChk ? data[2] : 0;
			data[4] = checkedChk ? 0 : data[2];
			mensualidadDT.row($("#rowPago" + id)).data(data);
			$($("#rowPago" + id).find('td input[type="checkbox"]')[0]).prop( "checked", true );

			var cdata = clientesDT.rows("tr.selected").data();
			if(proman && (cdata[0][26] == 'CH' || cdata[0][26] == 'CI' || cdata[0][26] == 'CS')) {
				calcularDescuentoAnualidad();
			}
			calcularTotalPago();
		});
	});

	//Configuracion del Modal para validacion de Permisos
	$('#modalPerDesctoMasivo').on('hidden.bs.modal', function (e) {
		if(isABootstrapModalOpen()){
			$("body").addClass("modal-open");
		}//fin:if
	});

	$('#modalPerDescto').on('hidden.bs.modal', function (e) {
		if(isABootstrapModalOpen()){
			$("body").addClass("modal-open");
		}//fin:if
	});
	//*************************************************************************
	//DataTable para desplegar el historico de pagos masivos	
	PagosMasivosDT = $('#pagosmasivo-table').DataTable({
		"dom": '<"clear"><rt><"bottom"lip><"clear">',		
		"paging": true,
		"autoWidth": true,
		"select": true, 
		"info": true,
		"searching": false,
		"processing": true,		
		"ordering":false,
		"pagingType": "full",
		"language": {
			"paginate": {
				"next": ">",
				"first": "<<",
				"last": ">>",
				"previous": "<"
			},
			"search": "",
			"searchPlaceholder": "Buscar en los resultados encontrados",
			"info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
			"infoEmpty": "",
			"infoFiltered": " - filtrado de _MAX_",
			"emptyTable": "Sin resultados",
			"sZeroRecords": "Sin resultados",
			"processing": "Procesando ...",
			"lengthMenu": "Mostrar _MENU_ registros"
		},
		"pageLength": 10,
		"responsive": true,
		"deferLoading": 0,				
		"scrollCollapse": true,
		"scrollY":       "40vh",		
		"columnDefs": [
			{ "name": "idpagomasivo", 	"data": "idpagomasivo", "targets": 0 },
			{ "name": "razonsocial", 	"data": "razonsocial", "targets": 1 },
			{ "name": "subtotal", 		"data": "subtotal", "targets": 2 },
			{ "name": "descuento", 		"data": "descuento", "targets":  3},
			{ "name": "total", 			"data": "total", "targets":  4},			
			{ "name": "activo", 		"data": "activo", "targets":  5},
			{ "name": "fechapago", 		"data": "fechapago", "targets":  6},
			{ "name": "metodopago", 	"data": "metodopago", "targets":  7},
			{ "name": "referencia", 	"data": "referencia", "targets":  8},
			{ "name": "factura", 		"data": "factura", "targets":  9},
			{ "name": "idrazonsocial", 	"data": "idrazonsocial", "targets":  10},
			{	
				'targets': 5, 
				'searchable': false, 
				'orderable': false,
				'className': 'dt-body-center',
				'render': function (data, type, full, meta){
					if(data == "1"){
						return '<input type=\"checkbox\" checked value="' + data + '" disabled />';
					}//fin:if 
					else{return '<input type=\"checkbox\" value="' + data + '" disabled />';}
				}
			}
        ]    		
	});	
	PagosMasivosDT.column(10).visible(false);
	//PagosMasivosDT.column(11).visible(false);
	//PagosMasivosDT.column(12).visible(false);
	//PagosMasivosDT.column(13).visible(false);
	//PagosMasivosDT.column(14).visible(false);	
	
	//Configuracion para seleccionar un elemento de pagos masivos
	//Configuracion para seleccionar un solo renglon
	$('#pagosmasivo-table tbody').on( 'click', 'tr', function (){
		if ($(this).hasClass('selected') ) {
			$(this).removeClass('selected');
			if($(this).rowIndex == 0) uSelec = -1;
		}//fin:else
		else{         
			PagosMasivosDT.$('tr.selected').removeClass('selected');
			$(this).toggleClass('selected');
		}//fin:else			
	});	
	
	//Configuracion del Fechas 
	var edt_fecini_masivo = $('input[name="edt-rsocial-fechaini"]');
	edt_fecini_masivo.datepicker({		
		format: 'dd/mm/yyyy',		
		todayHighlight: true,	
		autoclose: true,
		language: "es"		
	});	
	
	var edt_fecfin_masivo = $('input[name="edt-rsocial-fechafin"]');
	edt_fecfin_masivo.datepicker({		
		format: 'dd/mm/yyyy',		
		todayHighlight: true,
		autoclose: true,
		language: "es"		
	});
	
	$("#edt-rsocial-fechaini").inputmask("99/99/9999");
	$("#edt-rsocial-fechafin").inputmask("99/99/9999");
	
	$('#modalEmailPagoMasivo').on('hidden.bs.modal', function (e) {
		if(isABootstrapModalOpen()){
			$("body").addClass("modal-open");
		}//fin:if
	});
	
	//configuracion del modal para envio de correo masivo
	$("#edtEmailTo-rsocial").inputmask({ alias: "email", clearIncomplete: true});

	$('#mensualidad-table tbody').on('click', 'input[type="checkbox"]', function(e){
        var $tr  = $(this).closest("tr");
        var data = mensualidadDT.row($tr).data();
        var id = mensualidadDT.row($tr).index();
        var isDescuento = $("#chkDescuento").is(":checked");

        if(this.checked){
            if(id > 0)
            {
                var index = idsSelected.findIndex(x => x.id === id - 1);
                if(index < 0){
                    showGeneralMessage("Existen Mensualidades por cubrir.", "warning", true);
                    this.checked = false;
                    return;

                }
            }
			if(isDescuento){
				data[3] = data[2];
				data[4] = 0;
				mensualidadDT.row($tr).data(data);
				$($tr.find('td input[type="checkbox"]')[0]).prop( "checked", true );
			}

            var index = idsSelected.findIndex(x => x.id === id);
            if (index > -1) {
                idsSelected.splice(index, 1);
                dataidsSelected.splice(index, 1);
            }
            if(index < 0){
                idsSelected.push({id: id});
                dataidsSelected.push(data);
            }
        }
        else {

            $.each(idsSelected.filter(x => x.id >= id), function(key, value){
                index = idsSelected.findIndex(x => x.id === value.id);
                // if(idsSelected.length > 1 && (idsSelected.length - 1) === value.id){
                //     if($("#rowPago" + (value.id - 1)).find('td input[type="checkbox"]')[0]) {
				//
				//
                //         var data = mensualidadDT.row($("#rowPago" + (value.id - 1))).data();
				// 		data[4] = data[2];
				// 		data[3] = 0;
                //         mensualidadDT.row($("#rowPago" + (value.id - 1))).data(data);
                //         $($("#rowPago" + (value.id - 1)).find('td input[type="checkbox"]')[0]).prop( "checked",  true );
                //     }
                // }
                idsSelected.splice(index, 1);
                dataidsSelected.splice(index, 1);
                if($("#rowPago" + value.id).find('td input[type="checkbox"]')[0]) {

                    $($("#rowPago" + value.id).find('td input[type="checkbox"]')[0]).prop( "checked", false );
                    var data = mensualidadDT.row($("#rowPago" + value.id)).data();
					data[4] = data[2];
					data[3] = 0;
                    mensualidadDT.row($("#rowPago" + value.id)).data(data);
                }

            });



        }

		var cdata = clientesDT.rows("tr.selected").data();
		if(proman && (cdata[0][26] == 'CH' || cdata[0][26] == 'CI' || cdata[0][26] == 'CS')) {
			calcularDescuentoAnualidad();
		}
        calcularTotalPago();
    });

	$("#totalPagar").inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        prefix: '$ ', //Space after $, this will not truncate the first character.
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });

	$("#subtotalPagar").inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        prefix: '$ ', //Space after $, this will not truncate the first character.
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });

	$("#ivaPagar").inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        prefix: '$ ', //Space after $, this will not truncate the first character.
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });


    $("#chkcomercio").on("change", function () {
        $("#nombre_comercio").prop("required", this.checked);
        $("#nombre_comercio").prop("disabled", !this.checked);
        $("#nombre_comercio").val("");
    });

    $("#chkprivada").on("change", function () {
        $("#nombre_privada").prop("required", this.checked);
        $("#nombre_privada").prop("disabled", !this.checked);
        $("#nombre_privada").val("");
    });

    $("#folio_interno_sistema").on("change", function () {
		var isCheked = this.checked;
		$("#folio_interno").val("");
		if(isCheked){
			$.ajax({
				url: "/caja/nextfolio",
				beforeSend: function(){
					$("#folio_interno").val("cargando...");
				},
				success: function (resp) {
					if (resp === '0') {
						$("#folio_interno").val("");
						showGeneralMessage("No cuentas con folios disponibles", 'warning', true);
						return;
					}
					$("#folio_interno").val(resp);
				},
				error: function () {
					$("#folio_interno").val("");
				}
			});
		}
	});
});//fin:$(document).ready(function()


function calcularDescuentoAnualidad() {
	tottdescan = 0;
    var datosmes = [];
	var datosidx = 0;
    var dataCliente = clientesDT.rows("tr.selected").data();
    for(var i in dataidsSelected){
        var data = dataidsSelected[i];
        if(datosmes[data[5]]){
            datosmes[data[5]] += 1;
        }
        else{
            datosmes[data[5]] = 1;
        }

        //si ya encontramos los 12 meses vamos a hace un descuento de dos meses
		// se aplica el descuento solo para años mayores a 2018
		var anio = parseInt(data[5]);
        if(anio == proman.anio && parseInt(data[6]) == 1) datosidx = i;
        // if( anio == proman.anio && datosmes[data[5]] == 12){
		// 	isdescan = true;
            // var aux = mensualidadDT.row($("#rowPago" + i)).data();
            // aux[3] = aux[2];
            // aux[4] = 0;
            // mensualidadDT.row($("#rowPago" + i)).data(aux);
            // var aux = mensualidadDT.row($("#rowPago" + (i -1))).data();
            // aux[3] = aux[2];
            // aux[4] = 0;
            // mensualidadDT.row($("#rowPago" + (i-1))).data(aux);

            // $($("#rowPago" + i).find('td input[type="checkbox"]')[0]).prop( "checked", true );
            // $($("#rowPago" + (i-1)).find('td input[type="checkbox"]')[0]).prop( "checked", true );
        // }


    }

    if(datosmes[proman.anio] == 12){
		isdescan = true;
		var dataRows = mensualidadDT.rows().data();
		datosidx = parseInt(datosidx);
		for (let i = datosidx; i < (datosidx+12); i++){
			let aux = dataRows[i];
			if(proman.anio == aux[5] && auxpromanmeses.indexOf(parseInt(aux[6])) !== -1){
				$("#mensualidad-table tfoot tr#container-totaldesc-table").show();
				tottdescan += data[2];
				aux[3] = aux[2];
				aux[4] = 0;
				mensualidadDT.row($("#rowPago" + i)).data(aux);
				$($("#rowPago" + i).find('td input[type="checkbox"]')[0]).prop( "checked", true );
			}
		}
		mensualidadDT.draw();
	}
    else{
		if(isdescan){
			var dataRows = mensualidadDT.rows().data();
			for (let i = 0; i < auxpromanmeses.length; i++) {
				for (let j = 0; j < dataRows.length; j++){
					let aux = dataRows[j];
					if (proman.anio == aux[5] && auxpromanmeses[i] == aux[6]) {
						tottdescan -= aux[2];
						aux[3] = 0;
						aux[4] = aux[2];
						mensualidadDT.row($("#rowPago" + j)).data(aux);
						$($("#rowPago" + j).find('td input[type="checkbox"]')[0]).prop( "checked", true );
						break;
					}
				}
			}
			mensualidadDT.draw();
		}
		isdescan = false;
	}

}
function calcularTotalPago() {
    var totalD = 0;
    var id = 0;
    $.each(mensualidadDT.rows()[0], function (index, value) {
        id = value;
        var data = mensualidadDT.row("#rowPago" + id).data();
        if(data)
        {
            if($($("#rowPago" + id).find('td input[type="checkbox"]')[0]).is(":checked")) {


                totalD += parseFloat(data[4]);
            }
        }
    });

	$("#subtotalPagar").val(totalD);
	let ivaTotalD = isFactura ? (totalD*(0.16)) : 0;
	$("#ivaPagar").val(ivaTotalD);
    $("#totalPagar").val(totalD+ivaTotalD);
}

//-----------------------------------------------------------------------
//Funcion para agregar mensualidades
function fnAddMesAtraso(annio_mes, ftMensualidad, ftDescuento, ftMensualidad, annio_ui, mes_ui) {
	var vannio_ultimo = $("#edt-cobro-ultanio").val() != "" ? parseInt($("#edt-cobro-ultanio").val()) : 0;
	var vmes_ultimo   = $("#hdd-ultpago-mes").val() != ""? parseInt($("#hdd-ultpago-mes").val()) : 0;
	var arrayMesesDT = new Array();
	var iIndice = 0;
	var dataRows = mensualidadDT.rows().data();
	var bExisteRow = false;
	if(dataRows.length >0 ){
		for (var idx = 0; idx < dataRows.length; idx++){
			arrayMesesDT[iIndice++] = dataRows[idx][5] + "-" + dataRows[idx][6];
			if(dataRows[idx][5] == annio_ui && dataRows[idx][6] == mes_ui){
				bExisteRow = true;
				idx = dataRows.length;
			}//fin:if
		}//fin:for
		if(bExisteRow){
			showGeneralMessage("Ya existe el renglon:" + annio_mes, "warning", true);
			return;
		}//fin:if
	}//fin:if(dataRows.length > 0)

	//Variable de Interfaz
    var arrayUltPago;

	var dtFechaHoy = new Date();
	var annioActual = dtFechaHoy.getFullYear();

	
	var dtFechaAdd = new Date(annio_ui, mes_ui - 1, 1);
	var dtFecAux = new Date(annio_ui, mes_ui - 2, 1);

	//1. No se podran seleccionar el mes actual si se deben meses anteriores[ok]
	//2. Se aplicara un descuento si se detecta que se agregaron los meses de enero a diciembre del aÃ±o en curso,
	//	el descuento serÃ¡ aplicado en el mes de diciembre sobre el monto total de la mensualidad, es decir en el historial
	//	de pago se meterÃ¡n 11 registros con el cobro normal (mensualidad x, descuento 0) y el Ãºltimo registro,
	//	correspondiente al mes de diciembre, serÃ¡ con mensualidad x y descuento igual a la cantidad de la mensualidad

	if(vannio_ultimo == 0 && vmes_ultimo == 0){
		if(dataRows.length > 0){
			//Evaluar para obtener el Ultimo pago consideran Interfaz y renglones previamente capturados
			arrayUltPago = fnEvaluarUltimoPago(0, 0);
	
			//comparamos que el Ultimo mes de pago este capturado antes de agregar la nueva mensualidad
			if(parseInt(arrayUltPago["mes"]) == 12){
				dtFecAux.setFullYear(parseInt(arrayUltPago["annio"]) +1);
				dtFecAux.setMonth(0);
			}//fin:if
			else{
				dtFecAux.setFullYear(parseInt(arrayUltPago["annio"]));
				dtFecAux.setMonth(parseInt(arrayUltPago["mes"]));
			}//fin:else

			//validar si la fechas corresponde para agregar el renglon
			if(dtFecAux.getTime() == dtFechaAdd.getTime()){
				mensualidadDT.row.add([
					iIndice,
					annio_mes, 		//Año-Mes
					ftMensualidad, 	//Monto Mensualidad cliente.mensualidad
					0, 				//Monto Descuento
					ftMensualidad,  //Mensualidad
					annio_ui, 		//Hidden AÃ±o Add
					mes_ui			//Hidden Mes Add
				]);
				mensualidadDT.draw();
			}//fin:
			else{
				showGeneralMessage("Existen adeudos de Mensualidades.", "warning", true);
			}//fin:else
		}//fin:
		else{
			mensualidadDT.row.add([
				iIndice,
				annio_mes, 		//Año-Mes
				ftMensualidad, 	//Monto Mensualidad cliente.mensualidad
				0, 				//Monto Descuento
				ftMensualidad,  //Mensualidad
				annio_ui, 		//Hidden AÃ±o Add
				mes_ui			//Hidden Mes Add
			]);
			mensualidadDT.draw();
		}//fin:
	}//fin:if
	else{
		iIndice = 0;
		//Evaluar para obtener el Ultimo pago consideran Interfaz y renglones previamente capturados
		arrayUltPago = fnEvaluarUltimoPago(vannio_ultimo, vmes_ultimo);

		//comparamos que el Ultimo mes de pago este capturado antes de agregar la nueva mensualidad
		if(parseInt(arrayUltPago["mes"]) == 12){
			dtFecAux.setFullYear(parseInt(arrayUltPago["annio"]) +1);
			dtFecAux.setMonth(0);
		}//fin:if
		else{
			dtFecAux.setFullYear(parseInt(arrayUltPago["annio"]));
			dtFecAux.setMonth(parseInt(arrayUltPago["mes"]));
		}//fin:else

		//validar si la fechas corresponde para agregar el renglon
		//if(dtFecAux.getTime() == dtFechaAdd.getTime()){
			mensualidadDT.row.add([
				iIndice,
				annio_mes, 		//Año-Mes
				ftMensualidad, 	//Monto Mensualidad cliente.mensualidad
				0, 				//Monto Descuento
				ftMensualidad,  //Mensualidad
				annio_ui, 		//Hidden AÃ±o Add
				mes_ui			//Hidden Mes Add
			]);
			mensualidadDT.draw();
		//}//fin:
		//else{showGeneralMessage("Existen Mensualidades por cubrir.", "warning", true);}
	}//fin:mes
	mensualidadDT.draw();
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	//Bloque para validar si el cliente cubre la anualidad para descontar el ultimo mes
	var arrayAnual = new Array();
	var arrayMesesDT = new Array();
	var bExistDic = false;
	var bAplicaDescto = false;
	var icountMeses = 0;
	var mesIdx = 0;

	dataRows = mensualidadDT.rows().data();

	if(dataRows.length > 0 && dataRows.length >= 12){
		//Generamos el arreglo con la totalidad de mensualidades
		for(iterate = 0; iterate < 12; iterate++){
			mesIdx = iterate + 1;
			arrayAnual[iterate] = annioActual.toString()+ "-" + mesIdx.toString();
		}//fin:for

		//Iteracion para encontrar la Mensualidad numero 12(Diciembre)
		for(idx = 0; idx < dataRows.length; idx++){
			//Coincidir annio - mes actual para efectuar el descuento
			if(parseInt(dataRows[idx][5]) == 2020 &&
				parseInt(dataRows[idx][6]) == 12){
				bExistDic = true;
				idx = dataRows.length;
			}//fin:
		}//fin:for

		//actualizamos el renglon con mensualidad = descto
		bExistDic = false;
		if(bExistDic){
			
			//Clonar la combinacion annio-mes en un arreglo auxiliar para comparaciones
			for(idz =0; idz < dataRows.length;idz++){
				arrayMesesDT[idz] = dataRows[idz][5] + "-" + dataRows[idz][6];
			}//fin:for

			//Validar que existan la Totalidades de AÃ±o-Mes actual para considerar el pago de la anualidad
			icountMeses = 0;
			for(idy = 0; idy < arrayAnual.length; idy++){
				if(arrayMesesDT.indexOf(arrayAnual[idy]) > -1){
					icountMeses++;
				}//fin:if
			}//fin:for

			if(icountMeses == arrayAnual.length){		
				//$("#modalDesctoAnual").modal({backdrop: 'static', keyboard: false, show: true});							
				//*********************************************************************************
				//aplicacion del Descuento por cubrir los 12 meses
				annioActual = (new Date()).getFullYear();
				dataRows = mensualidadDT.rows().data();

				//Buscar el registro para el año-mes de diciembre y actualizar su valor
				for(idx = 0; idx < dataRows.length; idx++){
                    if(parseInt(dataRows[idx][5]) == annioActual &&
                        parseInt(dataRows[idx][6]) == 11){
                        //Descuento = Total
                        // mensualidadDT.cell(idx, 3).data(dataRows[idx][4]).draw();
                        // mensualidadDT.cell(idx, 4).data(0).draw();
                        // idx = dataRows.length - 1;
                    }//fin:if
					if(/*parseInt(dataRows[idx][5]) == annioActual &&*/
						parseInt(dataRows[idx][6]) == 12){
						//Descuento = Total
						mensualidadDT.cell(idx, 3).data(dataRows[idx][4]).draw();
						mensualidadDT.cell(idx, 4).data(0).draw();
						idx = dataRows.length - 1;
					}//fin:if
				}//fin:for
				//mensualidadDT.draw();
				//*********************************************************************************								
				// showGeneralMessage("Al cubrir la anualidad(12 meses) obtuvo el descuento del ultimo mes(diciembre).", "warning", true);
			}//fin:if(icountMeses == arrayAnual.length)
		}//fin:if
		mensualidadDT.draw();
	}
}
//FIN fnAddMesAtraso(p1,p2,p3,p4,p5,p6)-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: -34.397, lng: 150.644},
		zoom: 8
	});

	map2 = new google.maps.Map(document.getElementById('map2'), {
		center: {lat: -34.397, lng: 150.644},
		zoom: 8
	});
	
	
	geocoder = new google.maps.Geocoder();

    document.getElementById('cliente-search-location').addEventListener('click', function(){
        geocodeAddress(map);
    });

    document.getElementById('cliente-input-gelocation').addEventListener('keypress', function(e) {
        if(e.which == 13){geocodeAddress(map);}
    });

    
    /// nuevo evento de busqueda
    document.getElementById('cliente-search-location2').addEventListener('click', function(){
        geocodeAddress2(map2);
    });

    document.getElementById('cliente-input-gelocation2').addEventListener('keypress', function(e) {
        if(e.which == 13){geocodeAddress2(map2);}
    });	

    /// nuevo evento de busqueda
}//fin:function initMap()
//-----------------------------------------------------------------------------
//Validar y ejecutar la busqueda de Clientes
function buscarCliente(flimpiarpags){
	$(".btnCobroCliente").prop("disabled", true);
    var nombre  = $("#clientes-nombre-val").val();
	var calle   = $("#clientes-calle-val").val();
	var numero  = $("#clientes-numero-val").val();
	var colonia = $("#clientes-colonia-val").val();
	var clave   = $("#clientes-clave-val").val();
	var empresa	= $("#clientes-empresa-val").val();
    var privada	= $("#clientes-privada-val").val();
	var tipoBusqueda = $("#clientes-filter-select").val();
	var folio = $("#clientes-folio-val").val();

	if(tipoBusqueda == "todos"){
    }//fin:if(tipoBusqueda == "todos")

	if(tipoBusqueda == "nom"){
		if(nombre == ""){
            showGeneralMessage("Debe escribir el nombre a buscar", "warning", true);
			return;
		}
		if(nombre.length < 2){
            showGeneralMessage("Debe escribir almenos dos caracteres", "warning", true);
			return;
		}
	}
	if(tipoBusqueda == "cla"){
		if(clave == ""){
            showGeneralMessage("Debe escribir la clave a buscar", "warning", true);
			return;
		}
	}
	if(tipoBusqueda == "dir"){
		if(calle == "" && numero == "" && colonia == ""){
            showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
			return;
		}

		if(calle == "" && numero == "" && colonia == ""){
            showGeneralMessage("Debe escribir almenos dos campos", "warning",true);
			return;
		}

		if(calle == "" && colonia == ""){
            showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
			return;
		}

		if(numero == "" && colonia == ""){
            showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
			return;
		}
	}//fin:if(tipoBusqueda == "dir")

	if(tipoBusqueda == "folio"){
		if(folio == ""){
            showGeneralMessage("Debe escribir folio a buscar", "warning", true);
			return;
		}
	}

    //Validacion para el nombre de la Empresa
    if(tipoBusqueda == "rsocial"){
        if(empresa == ""){
			showGeneralMessage("Debe escribir la empresa a buscar", "warning");
			return;
		}
		if(empresa.length < 2){
			showGeneralMessage("Debe escribir al menos dos caracteres", "warning");
			return;
		}
    }//fin:if(tipoBusqueda == "rsocial")

    //Validacion para el nombre de la privada
    if(tipoBusqueda == "privada"){
        if(privada == ""){
            showGeneralMessage("Debe escribir el nombre de la privada a buscar", "warning", true);
            return;
        }
        if(privada.length < 2){
            showGeneralMessage("Debe escribir al menos dos caracteres", "warning", true);
            return;
        }
    }//fin:if(tipoBusqueda == "rsocial")

	fnLimpiarDatos(flimpiarpags);

	var data = {
		calle: calle,
		numero: numero,
		colonia: colonia,
		tipo: tipoBusqueda,
		nombre: nombre,
		clave: clave,
		empresa:empresa,
		folio: folio,
        privada: privada
	}

	$.ajax({
		url: '/caja/search',
		data: JSON.stringify(data),
		type: 'POST',
		dataType: "json",
		beforeSend: function(){
            $('#caja-container').html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
				'</div>');
		},
		success: function(json){
			if(json.length == 0){
				showGeneralMessage("No se encontraron registros", "warning", true);
				return;
			}
            clientesDT.clear();
			clientesDT.rows.add(json);
			clientesDT.draw();
		},
		complete: function () {$('#caja-container').html("");}
	});
}//fin:buscarCliente
//-----------------------------------------------------------------------------
function fnLimpiarDatos(flimpiarpags){

	clientesDT.clear();
	clientesDT.draw();
	historialDT.clear();
	historialDT.draw();
	if(flimpiarpags) {
		mensualidadDT.clear();
		mensualidadDT.draw();
	}
}
//-----------------------------------------------------------------------------
//Funcion para invocar el modal para realizar el cobro del servicio de basura.
function fnCobrarCliente(){
	$("#container-btns-cobrocliente").show();
	$("#container-btns-desceunto").hide();
	$("#container-btns-facturado").hide();
	$("#container-descuento").hide();
	$("#container-datos-pago").show();
	$('#chkDescuento').prop('disabled', true);
	$("#folio_interno_sistema").prop("checked", false);
	$("#folio_interno").val("");
	// $.ajax({
	// 	url: "/caja/nextfolio",
	// 	success: function (resp) {
	//
	// 		if(resp === '0'){
	// 			showGeneralMessage("No cuentas con folios disponibles", 'warning', true);
	// 			return;
	// 		}
	// 		$("#folio_interno").val(resp);
	// 		$("#folio_interno").prop("disabled", true);
			var idCliente = 0;

			if(clientesDT.rows('.selected').data().length == 0){
				showGeneralMessage("Seleccione un registro", "warning");
				return ;
			}//fin:if(rows('.selected').data().length == 0)

			mensualidadDT.clear();
			mensualidadDT.draw();
			$("#hdd-ultpago-mes").val("");
			$("#hdd-mensualidad").val("");
			$("#hdd-recolecta-ruta").val("");
			$("#hdd-recolecta-direc").val("");
			$("#hdd-recolecta-colonia").val("");
			$("#chkFactCobro").prop("checked", false);

			$("#hdd-cobro-idpago").val("");
			$("#hdd-recibo-email").val("");

			$("#select-cobro-mes").val((new Date()).getMonth() + 1);
			// $("#select-cobro-anio").val((new Date()).getFullYear());

			var rowData = clientesDT.rows("tr.selected").data();
			var	direccionCte = "";
			var dir_cliente = "";
			var imes = "";
			viewTotalFacturas(false);
			$("#container_folio_factura").hide();
			if(rowData[0].length > 0){
				$("#referencia_bancaria").prop("disabled", true);
				//$("#folio_factura").prop("disabled", true);
				$("#referencia_bancaria").val("");
				// $("#folio_interno").val("");
				$("#folio_factura").val("");
				$("#select-basura-fpago option:first").prop("selected", true);

				//dir_cliente = rowData[0][2] + " No. " + rowData[0][3] + " por " + rowData[0][6] + " y " + rowData[0][7];
				dir_cliente = rowData[0][2];
				if(rowData[0][3] != "" )
					dir_cliente += " No. " + rowData[0][3];

				if(rowData[0][8] != "")
					dir_cliente += " por " + rowData[0][8];

				if(rowData[0][20] != "")
					dir_cliente += " y " + rowData[0][20];

				direccionCte = dir_cliente + " colonia " + rowData[0][6];

				$("#hdd-recolecta-direc").val(dir_cliente);
				$("#hdd-recolecta-colonia").val(rowData[0][6]);

				$("#edt-cobro-Nombre").val(rowData[0][1]);
				$("#edt-cobro-direccion").val(direccionCte);
				if(parseInt(rowData[0][4]) > 0){
					$("#edt-cobro-ultanio").val(rowData[0][4]);
					// $("#select-cobro-anio").val(rowData[0][4]);
				}//fin:if
				else{$("#edt-cobro-ultanio").val("");}

				//Ultimo_mes_pago
				if(rowData[0][5] != ""){
					$("#edt-cobro-ultmes").val(rowData[0][5]);
					//$("#select-cobro-mes").val(parseInt(rowData[0][17]) + 1);
					if(parseInt(rowData[0][17]) > 11){
						$("#select-cobro-mes").val(1);
						// $("#select-cobro-anio").val((parseInt(rowData[0][4]) + 1));
					}//fin:if
					else
						$("#select-cobro-mes").val(parseInt(rowData[0][17]) + 1);

					$("#hdd-ultpago-mes").val(parseInt(rowData[0][17]));
				}//fin:if
				else{ $("#edt-cobro-ultmes").val("");}
				//$("#edt-cobro-ultmes").val(rowData[0][5]);

				idCliente = parseInt(rowData[0][0]);
				$("#hdd-recolecta-idcte").val(idCliente);
				$("#hdd-mensualidad").val(parseFloat(rowData[0][16]));
				$("#hdd-recolecta-ruta").val(rowData[0][18]);
				$("#hdd-recibo-email").val(rowData[0][19]);
				$("#hdd-cobro_manual").val(rowData[0][21]);
				$("#hdd-monto_cobro").val(parseFloat(rowData[0][22]));

				//Reiniciar controles editables
				//btn-cobro-nota
				$("#btn-recibo-cliente").prop('disabled', true)
				$("#select-cobro-anio").attr('selected','selected');
				$("#select-cobro-mes").attr('selected','selected');
				$("#btn-cobro-add").prop('disabled', false);
				$("#btn-cobro-mes").prop('disabled', false);
				$('#chkDescuento').prop('checked', false).change();
				$('#aviso-adeudo').html("");
				$("#hdd-servicio-idpago").val("");
				$("#hdd-cobro-idpago").val("");
				//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				//Inicializar los controles para el modal del Recibo de Cobro
				$("#recibo-nombre").html("");
				$("#recibo-direccion").html("");
				$("#recibo-colonia").html("");
				$("#recibo-ruta").html("");
				$("#recibo-idcliente").html("");
				$("#recibo-concepto").html("");
				$("#recibo-meses").html("");
				$("#recibo-numletras").html("");
				$("#recibo-total").html("");
				$("#recibo-folio").html("");
				//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				$("#modal-cobro-cliente").modal({backdrop: 'static', keyboard: false, show: true});
				isdescan = false;
				$("#select-basura-fpago").change();
			}//fin:if(rowData[0].length > 0)
	// 	},
	// });
}//fin:fnCobrarCliente
//-----------------------------------------------------------------------------
function fnModalCobroServicio(){

	var idCliente = 0;

    if(clientesDT.rows('.selected').data().length == 0){
		showGeneralMessage("Seleccione un registro", "warning", true);
        return ;
    }//fin:if(clientesDT.rows('.selected').data().length == 0)

    //Generacion del Json para guardar la informacion
	var rowData = clientesDT.rows('tr.selected').data();
    if(rowData[0].length <= 0){return;}//fin:

    //Obtener el historial de la base de datos
    idCliente = parseInt(rowData[0][0]);

    $("#hdd-servicio-idcte").val(idCliente);
	$("#hdd-servicio-nombre").val(rowData[0][15]);
	$("#hdd-servicio-idpago").val("");
	
	$("#hdd-recibo-email").val("");

	//Reiniciar controles
	$("#edt-servicio-concepto").val("");
	$("#edt-servicio-ref").val("");
	$("#edt-servicio-monto").val("");
	$("#edt-servicio-fecha").val("");
	$("#chkFactServicio").removeAttr("checked");

	$("#btn-servicio-cobro").prop('disabled', false);
	$("#btn-servicio-nota").prop('disabled', true)
	
	$("#hdd-servicio-ruta").val("");
	$("#hdd-servicio-direc").val("");
	$("#hdd-servicio-colonia").val("");	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//Inicializar los controles para el modal del Recibo de Cobro
	$("#recibo-nombre").html("");
	$("#recibo-direccion").html("");
	$("#recibo-colonia").html("");
	$("#recibo-ruta").html("");
	$("#recibo-idcliente").html("");
	$("#recibo-concepto").html("");
	$("#recibo-meses").html("");
	$("#recibo-numletras").html("");
	$("#recibo-total").html("");
	$("#recibo-folio").html("");
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++			
	//Asignacion de valores para la impresion del Recibo
	var dir_cliente = "";	
	//dir_cliente = rowData[0][2] + " No. " + rowData[0][3] + " por " + rowData[0][6] + " y " + rowData[0][7];
	dir_cliente = rowData[0][2];
	if(rowData[0][3] != "" )
		dir_cliente += " No. " + rowData[0][3];
	
	if(rowData[0][8] != "")
		dir_cliente += " por " + rowData[0][8];
	
	if(rowData[0][20] != "")
		dir_cliente += " y " + rowData[0][20];
	
	$("#hdd-servicio-direc").val(dir_cliente);
	$("#hdd-servicio-colonia").val(rowData[0][6]);
	$("#hdd-servicio-ruta").val(rowData[0][18]);
	$("#hdd-recibo-email").val(rowData[0][19]);	
			
	$("#modalCobrarServicio").modal({backdrop: 'static', keyboard: false, show: true});
}//fin:fnModalCobroServicio
//-----------------------------------------------------------------------------
function fnCobrarServicio(){

    //para validar fecha en formato YYYY-MM-DD
    function isValidDate(dateString) {
        var regEx = /^\d{4}-\d{2}-\d{2}$/;
        return dateString.match(regEx) != null;
	}//fin:isValidDate
    //--------------------------------------------------------------------------
    //Validaciones generales
    var v_Concepto = $("#edt-servicio-concepto").val();
    var v_referencia = $("#edt-servicio-ref").val();
    var v_Monto = $("#edt-servicio-monto").val();
    var v_fecha = $("#edt-servicio-fecha").val();

    var v_error = false;
    var v_message ="";

    v_Concepto = v_Concepto.trim();
    v_referencia = v_referencia.trim();
    v_Monto = v_Monto.trim();
    v_fecha = v_fecha.trim();

    if(v_Concepto == null || v_Concepto == ""){
        v_message = "* Es necesario ingresar el Concepto.<br/>";
        v_error = true;
    }//fin:
				
	var mreferencia = $("#select-servicio-fpago").find("option:selected").attr("data-referencia");	
	if(mreferencia == "si"){
        if(v_referencia == null || v_referencia == ""){
			v_message += "* Es necesario ingresar la Referencia para la forma de Pago.<br/>";
            v_error = true;
        }//fin:if
    }//fin:if
		
    if(v_Monto == null || v_Monto == ""){
        v_message += "* Es necesario ingresar el Monto a pagar.<br/>";
        v_error = true;
    }//fin:if
    else{
        if(isNaN(parseFloat(v_Monto))){
            v_message += "* Ingrese un Monto a pagar valido.<br/>";
            v_error = true;
		}//fin:
    }//fin:else

    if(v_fecha == null || v_fecha == ""){
        v_message += "* Es necesario ingresar la Fecha.<br/>";
        v_error = true;
    }//fin:
    else{
        if(!isValidDate(v_fecha)){
            v_message += "* La fecha debe tener formato YYYY-MM-DD.<br/>";
			v_error = true;
        }//fin:if
	}//fin:

    if(v_error){
		showGeneralMessage(v_message, "warning", true);
        return;
    }//fin:
	//--------------------------------------------------------------------------
    //0123456789
    //YYYY-MM-DD
    var dtFecha = $("#edt-servicio-fecha").val();
	var dtAnnioActual = new Date();

    var datajson = {
        idcliente: $("#hdd-servicio-idcte").val(),
		mes:  (dtAnnioActual.getMonth() + 1),
		anio: dtAnnioActual.getFullYear(),
        descuento: 0.00,
        iva: 0.00,
        cantidad: parseFloat($("#edt-servicio-monto").val()),
        fecha_pago: dtFecha,
        idforma_pago: $("#select-servicio-fpago").val(),
        referencia_bancaria: $("#edt-servicio-ref").val(),
        validado: true,
		facturado: $("#chkFactServicio").is(":checked"),
        folio_factura: "",
        idcobratario: 0,
        caja: false,
        idbanco: 0,
        activo: true,
        envio_correo:false,
        recibo_impreso:false,
        incluido_corte: false,
        fecha_corte:"",
        idvisita_cliente: "",
		idtipo_cobro:2,
		idtipo_servicio : parseInt($("#select-servicio-tipo").val()),
		concepto_servicio : v_Concepto
	}//fin:datajson

    //Invocar ajax para realizar el guardado
	$.ajax({
		url: '/caja/savePagoServicio',
		type: 'POST',
		dataType: "json",
		data: JSON.stringify(datajson),
		beforeSend: function(){
			$('#load-guardar').html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Guardando...</div>');
		},
		success: function(json){
            if(Boolean(json["success"])){
				
                $("#btn-servicio-cobro").prop('disabled', true);
				
				//Habilitar boton para la impresion del recibo
				$("#btn-servicio-nota").prop('disabled', false);
				
				//Asignar el idpago al hidden para imprimir en el Folio del Recibo
				$("#hdd-servicio-idpago").val(parseInt(json["idpago"]));
				
				showGeneralMessage("¡Registro guardado con éxito!", "success", true);	
			}//fin:if
		},
		complete: function () {$('#load-guardar').html("");}
	});
}//fin:function
//-----------------------------------------------------------------------------
function fnConsultarUbicacion(){

    var googleLatAndLong;
    var mapLatitud;
    var mapLongitud;

    if(clientesDT.rows('.selected').data().length == 0){
		showGeneralMessage("Seleccione un registro", "warning", true);
        return ;
    }//fin:if(clientesDT.rows('.selected').data().length == 0)

	var rowData = clientesDT.rows('tr.selected').data();

	$("#hdd-map-idcliente").val("");
	
    if(rowData[0].length > 0){
        mapLatitud = rowData[0][10];
		mapLongitud = rowData[0][11];
		googleLatAndLong = new google.maps.LatLng(mapLatitud, mapLongitud);
        var mapOptions = {zoom: 18, center: googleLatAndLong}
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        if(marker) marker.setMap(null);
		marker = new google.maps.Marker({position: googleLatAndLong, map: map, draggable:true});
        marker.setMap(map);
		
		$("#hdd-map-idcliente").val(parseInt(rowData[0][0]));

		$("#modalUbicacion").modal({backdrop: 'static', keyboard: false, show: true});
	}//fin:if(rowData[0].length > 0)
}//fin:fnConsultarUbicacion
//-----------------------------------------------------------------------------
function fnConsultarHistorial(){

    if(clientesDT.rows('.selected').data().length == 0){
		showGeneralMessage("Seleccione un registro", "warning", true);
        return ;
    }//fin:if(clientesDT.rows('.selected').data().length == 0)

    var rowData = clientesDT.rows('tr.selected').data();
	var idCliente = 0;
    if(rowData[0].length <= 0){return;}//fin:

	$("#loading-cancelar").html('');
    //Obtener el historial de la base de datos
    idCliente = parseInt(rowData[0][0]);
	var data = {clave: idCliente}
	$.ajax({
        url: '/caja/buscarHistorial',
		type: 'POST',
		dataType: "json",
		data: JSON.stringify(data),
		beforeSend: function(){
			$('#hist-table-container').html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...</div>');
		},
		success: function(json){
			if(json.length == 0){
				showGeneralMessage("No se encontraron registros", "warning", true);
				return;
			}//fin:if(json.length == 0)
			
			historialDT.clear();
			if(json.length > 0) {
				$.each(json, function(key, value){							
					historialDT.row.add({
						"fecha_pago":value.fecha_pago,
						"idcliente":value.idcliente,
						"folio_interno":value.folio_interno,
						"usuario":value.usuario,
						"activo":value.activo,
						"subtotal":value.subtotal,
						"comision":value.comision,
						"total":value.total,
						"paypal":value.paypal,
						"encorte":value.encorte,
						"idpago": value.idpago,
						"pensiondado": value.pensiondado,
						"iddescuento_pensionado": value.iddescuento_pensionado,
						"impresiones": value.impresiones,
						"isnegociacion": value.isnegociacion
					}).draw();
				});
				setTimeout(function () {historialDT.columns.adjust().draw();},500);
			}//fin:if(json.length > 0)
			$("#modalHistorial").modal({backdrop: 'static', keyboard: false, show: true});
		},
		complete: function () {$('#hist-table-container').html("");}
	});
}//fin:fnConsultarHistorial
//-----------------------------------------------------------------------------
function fnCancelarHistPago(){

	if(historialDT.rows('.selected').data().length == 0){
		showGeneralMessage("Seleccione un registro", "warning", true);
        return ;
	}//fin:if(clientesDT.rows('.selected').data().length == 0)		
	
	var dtsHistorico = historialDT.rows('tr.selected').data().toArray().slice();	
	if(dtsHistorico[0].length <= 0){return;}

	if((dtsHistorico[0]['activo']) == '0'){
		showGeneralMessage("¡El pago ya se encuentra cancelado!", "warning", true);
		return;
	}//fin:if	
	//************************************************************

	if((dtsHistorico[0]['iddescuento_pensionado']) != null){
		showGeneralMessage("¡El pago es de un descuento!", "warning", true);
		return;
	}//fin:if	
	//************************************************************
	$("#modalMotivoCancelacion").modal({
		show: true,
		backdrop: "static"
	});
	//************************************************************
}//fin:fnCancelarServicio

function fnCancelarConMotivo() {
	var dtsHistorico = historialDT.rows('tr.selected').data().toArray().slice();
	if(dtsHistorico[0].length <= 0){return;}
	//Si es un pago reralizado por razon social impedir la cancelacion

	var datapost = {idPago: parseInt(dtsHistorico[0]['idpago'])};
	if($("#txtMotivoCancelacion").val().trim() == ""){
		showGeneralMessage("Debe proporcionar un motivo de cancelación", "warning", true);
		return;
	}
	fncancelarPagoByTipo(dtsHistorico[0], $("#txtMotivoCancelacion").val().trim());
}
//----------------------------------------------
function fnAddMensualidadYear() {
    var vannio_ultimo = $("#edt-cobro-ultanio").val() != "" ? parseInt($("#edt-cobro-ultanio").val()) : 0;
    var vmes_ultimo   = $("#hdd-ultpago-mes").val() != ""? parseInt($("#hdd-ultpago-mes").val()) : 0;

    var lastDatePay = new Date();
    lastDatePay.setFullYear(vannio_ultimo);
    lastDatePay.setMonth(vmes_ultimo);
    lastDatePay.setHours(0, 0, 0, 0);

	//if(parseInt($("#select-cobro-anio").val()) == (ultimoAnio + 1)) {
        ultimoAnio = parseInt($("#select-cobro-anio").val());
        for (var idx = 1; idx <= 12; idx++) {
            var curAdd = new Date();
            curAdd.setFullYear($("#select-cobro-anio").val());
            curAdd.setMonth(idx);
            curAdd.setHours(0, 0, 0, 0);
            if(lastDatePay >= curAdd) continue;
            var mesanioaux = month[idx] + " " + $("#select-cobro-anio").val().toString();
            fnAddMesAtraso(mesanioaux, ftMensualidad, 0, ftMensualidad, $("#select-cobro-anio").val().toString(), idx.toString());
        }
    //}
    //else
        //showGeneralMessage("El año seleccionado no es el correspondiente", "warning", true);
}
//-----------------------------------------------------------------------------
function fnAddMensualidad() {

	//Variable de Interfaz
    var annio_mes = $("#select-cobro-mes :selected").text() + " " + $("#select-cobro-anio :selected").val();
	var vannio_ultimo = $("#edt-cobro-ultanio").val() != "" ? parseInt($("#edt-cobro-ultanio").val()) : 0;
	var vmes_ultimo   = $("#hdd-ultpago-mes").val() != ""? parseInt($("#hdd-ultpago-mes").val()) : 0;

	var ftMensualidad = $("#hdd-mensualidad").val() != "" ? parseFloat($("#hdd-mensualidad").val()) : 0;

	var annio_ui = parseInt($("#select-cobro-anio").val());
	var mes_ui = parseInt($("#select-cobro-mes").val());

	var arrayMesesDT = new Array();
	var iIndice = 0;
	var arrayUltPago;

	//var dtFecUltPago = new Date(vannio_ultimo, vmes_ultimo - 1, 1);
	var dtFecUltPago = new Date();
	var dtFechaHoy = new Date();
	var annioActual = dtFechaHoy.getFullYear();

	if(vannio_ultimo > 0 && vmes_ultimo > 0){
		dtFecUltPago.setFullYear(vannio_ultimo);
		dtFecUltPago.setMonth(vmes_ultimo - 1);
	}//fin:

	var dtFechaAdd = new Date(annio_ui, mes_ui - 1, 1);
	var dtFecAux = new Date(annio_ui, mes_ui - 2, 1);

	var dataRows = mensualidadDT.rows().data();
	var bExisteRow = false;

	if(dataRows.length > 0){
		for (var idx = 0; idx < dataRows.length; idx++){
			arrayMesesDT[iIndice++] = dataRows[idx][4] + "-" + dataRows[idx][5];
			if(dataRows[idx][4] == annio_ui && dataRows[idx][5] == mes_ui){
				bExisteRow = true;
				idx = dataRows.length;
			}//fin:if
		}//fin:for
		if(bExisteRow){
			showGeneralMessage("Ya existe el renglon:" + annio_mes, "warning", true);
			return;
		}//fin:if
	}//fin:if(dataRows.length > 0)

	//1. No se podran seleccionar el mes actual si se deben meses anteriores[ok]
	//2. Se aplicara un descuento si se detecta que se agregaron los meses de enero a diciembre del aÃ±o en curso,
	//	el descuento serÃ¡ aplicado en el mes de diciembre sobre el monto total de la mensualidad, es decir en el historial
	//	de pago se meterÃ¡n 11 registros con el cobro normal (mensualidad x, descuento 0) y el Ãºltimo registro,
	//	correspondiente al mes de diciembre, serÃ¡ con mensualidad x y descuento igual a la cantidad de la mensualidad
	if(vannio_ultimo == 0 && vmes_ultimo == 0){
		if(dataRows.length > 0){
			//Evaluar para obtener el Ultimo pago consideran Interfaz y renglones previamente capturados
			arrayUltPago = fnEvaluarUltimoPago(0, 0);
	
			//comparamos que el Ultimo mes de pago este capturado antes de agregar la nueva mensualidad
			if(parseInt(arrayUltPago["mes"]) == 12){
				dtFecAux.setFullYear(parseInt(arrayUltPago["annio"]) +1);
				dtFecAux.setMonth(0);
			}//fin:if
			else{
				dtFecAux.setFullYear(parseInt(arrayUltPago["annio"]));
				dtFecAux.setMonth(parseInt(arrayUltPago["mes"]));
			}//fin:else

			//validar si la fechas corresponde para agregar el renglon
			if(dtFecAux.getTime() == dtFechaAdd.getTime()){
				mensualidadDT.row.add([
					annio_mes, 		//AÃ±o-Mes
					ftMensualidad, 	//Monto Mensualidad cliente.mensualidad
					0, 				//
					ftMensualidad,  //Mensualidad
					annio_ui, 		//Hidden AÃ±o Add
					mes_ui			//Hidden Mes Add
				]);
				mensualidadDT.draw();
			}//fin:
			else{
				showGeneralMessage("Existen adeudos de Mensualidades.", "warning", true);
			}//fin:else
		}//fin:
		else{
			mensualidadDT.row.add([
				annio_mes,
				ftMensualidad,
				0,
				ftMensualidad,
				annio_ui,
				mes_ui]);
			mensualidadDT.draw();
		}//fin:
	}//fin:if
	else{
		iIndice = 0;
		//Evaluar para obtener el Ultimo pago consideran Interfaz y renglones previamente capturados
		arrayUltPago = fnEvaluarUltimoPago(vannio_ultimo, vmes_ultimo);

		//comparamos que el Ultimo mes de pago este capturado antes de agregar la nueva mensualidad
		if(parseInt(arrayUltPago["mes"]) == 12){
			dtFecAux.setFullYear(parseInt(arrayUltPago["annio"]) +1);
			dtFecAux.setMonth(0);
		}//fin:if
		else{
			dtFecAux.setFullYear(parseInt(arrayUltPago["annio"]));
			dtFecAux.setMonth(parseInt(arrayUltPago["mes"]));
		}//fin:else

		//validar si la fechas corresponde para agregar el renglon
		if(dtFecAux.getTime() == dtFechaAdd.getTime()){
			mensualidadDT.row.add([
				annio_mes,
				ftMensualidad,
				0,
				ftMensualidad,
				annio_ui,
				mes_ui]);
			mensualidadDT.draw();
		}//fin:
		else{showGeneralMessage("Existen Mensualidades por cubrir.", "warning", true);}
	}//fin:mes
	mensualidadDT.draw();
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	//Bloque para validar si el cliente cubre la anualidad para descontar el ultimo mes
	var arrayAnual = new Array();
	var arrayMesesDT = new Array();
	var bExistDic = false;
	var bAplicaDescto = false;
	var icountMeses = 0;
	var mesIdx = 0;

	dataRows = mensualidadDT.rows().data();

	if(dataRows.length > 0 && dataRows.length >= 12){
		//Generamos el arreglo con la totalidad de mensualidades
		for(iterate = 0; iterate < 12; iterate++){
			mesIdx = iterate + 1;
			arrayAnual[iterate] = annioActual.toString()+ "-" + mesIdx.toString();
		}//fin:for

		//Iteracion para encontrar la Mensualidad numero 12(Diciembre)
		for(idx = 0; idx < dataRows.length; idx++){
			//Coincidir annio - mes actual para efectuar el descuento
			if(parseInt(dataRows[idx][4]) == annioActual &&
				parseInt(dataRows[idx][5]) == 12){
				bExistDic = true;
				idx = dataRows.length;
			}//fin:
		}//fin:for

		//actualizamos el renglon con mensualidad = descto
		if(bExistDic){
			
			//Clonar la combinacion annio-mes en un arreglo auxiliar para comparaciones
			for(idz =0; idz < dataRows.length;idz++){
				arrayMesesDT[idz] = dataRows[idz][4] + "-" + dataRows[idz][5];
			}//fin:for

			//Validar que existan la Totalidades de AÃ±o-Mes actual para considerar el pago de la anualidad
			icountMeses = 0;
			for(idy = 0; idy < arrayAnual.length; idy++){
				if(arrayMesesDT.indexOf(arrayAnual[idy]) > -1){
					icountMeses++;
				}//fin:if
			}//fin:for

			if(icountMeses == arrayAnual.length){		
				//$("#modalDesctoAnual").modal({backdrop: 'static', keyboard: false, show: true});							
				//*********************************************************************************
				//aplicacion del Descuento por cubrir los 12 meses
				annioActual = (new Date()).getFullYear();
				dataRows = mensualidadDT.rows().data();

				//Buscar el registro para el año-mes de diciembre y actualizar su valor
				for(idx = 0; idx < dataRows.length; idx++){
					if(parseInt(dataRows[idx][4]) == annioActual &&
						parseInt(dataRows[idx][5]) == 12){
						//Descuento = Total
						mensualidadDT.cell(idx, 2).data(dataRows[idx][3]).draw();
						mensualidadDT.cell(idx, 3).data(0).draw();
						idx = dataRows.length;
					}//fin:if
				}//fin:for
				//mensualidadDT.draw();
				//*********************************************************************************								
				showGeneralMessage("Al cubrir la anualidad(12 meses) obtuvo el descuento del ultimo mes(diciembre).", "warning", true);
			}//fin:if(icountMeses == arrayAnual.length)
		}//fin:if
		mensualidadDT.draw();
	}//fin:if(dataRows.length > 0 && dataRows.length >= 12)
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
}//fin:fnAddMensualidad
//-----------------------------------------------------------------------------





function fnMesToNumber(pmes){

	var v_mes = 0;
	switch(pmes){
		case 'enero' : v_mes = 1;break;
		case 'febrero':v_mes = 2; break;
		case 'marzo': v_mes = 3;break;
		case 'abril': v_mes = 4;break;
		case 'mayo': v_mes = 5;break;
		case 'junio': v_mes = 6;break;
		case 'julio': v_mes = 7;break;
		case 'agosto': v_mes = 8;break;
		case 'septiembre': v_mes = 9;break;
		case 'octubre': v_mes = 10;break;
		case 'noviembre': v_mes = 11;break;
		case 'diciembre': v_mes = 12;break;
	}//fin:switch(pmes)
	return v_mes;
}//fin:fnMesToNumber
//-----------------------------------------------------------------------------
function fnNumberToMes(imes){

	var mes_name = "";
	switch(imes){
		case 1:mes_name  ='Enero'; break;
		case 2:mes_name  ='Febrero'; break;
		case 3:mes_name  ='Marzo'; break;
		case 4:mes_name  ='Abril'; break;
		case 5:mes_name  ='Mayo'; break;
		case 6:mes_name  ='Junio'; break;
		case 7:mes_name  ='Julio'; break;
		case 8:mes_name  ='Agosto'; break;
		case 9:mes_name  ='Septiembre'; break;
		case 10:mes_name ='Octubre'; break;
		case 11:mes_name ='Noviembre'; break;
		case 12:mes_name ='Diciembre'; break;
	}//fin:switch(pmes)
	return mes_name;
}//fin:fnNumberToMes
//-----------------------------------------------------------------------------
function fnEvaluarUltimoPago(u_annio, u_mes){

	var arrayRowsMax = new Array();
	var arrayValues  = new Array();
	var max_annio = u_annio;
	var iIndice = 0;

	var dataRows = mensualidadDT.rows().data();

	//Obtener el maximo valor para el aÃ±o
	jQuery.map(dataRows, function (obj) {
		if (obj[4] > max_annio)
			max_annio = parseInt(obj[4]);
	});
	//Si datable esta vacio consideramos al Ultimo pago como pivote
	if(u_annio == max_annio){
		arrayRowsMax.push({annio: max_annio, mes: u_mes});
	}//fin:

	//Obtener todos los renglones con el max aÃ±o
	for (var idx = 0; idx < dataRows.length; idx++){
		if(parseInt(dataRows[idx][4]) == max_annio ){
			arrayRowsMax.push({annio: max_annio, mes: parseInt(dataRows[idx][5])});
		}//fin:
	}//fin:for

	//Ordenamos descendente para obtener la mensualidad mas Reciente en Interfaz
	arrayRowsMax.sort(function (a, b){return (b.mes - a.mes)});
	if(arrayRowsMax.length > 0){
		arrayValues["annio"] = arrayRowsMax[0].annio;
		arrayValues["mes"] = arrayRowsMax[0].mes;
	}//fin:
	arrayRowsMax = null;
	return arrayValues;
}//fin:fnEvaluarUltimoPago
//-----------------------------------------------------------------------------
function fnCobrarRecolecta() {

	//--------------------------------------------------------------------------
	//Validaciones generales
	if (idsSelected.length <= 0) {
		showGeneralMessage("Es necesario seleccionar al menos un mes para continuar.", "warning", true);
		return;
	}//fin:
	if (!$("#chkDescuento").is(":checked")){
		// if ($("#folio_interno").val().trim() == "") {
		// 	showGeneralMessage("Es necesario proporcionar el folio interno para continuar.", "warning", true);
		// 	return;
		// }
	}
	//--------------------------------------------------------------------------
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	guardarMensualidades();
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
}//fin:function
//-----------------------------------------------------------------------------
function fnAplicarDescto(){

	var UsuarioVal  = $("#edtPerUser").val();
	var PasswordVal = $("#edtPerPass").val();
	var MontoDescto = $("#edtPerMonto").val();
	var motivoDescuento = $("#edtMotivo").val();
	var v_error = false;
	var v_message = "";
	var htmlContent = "";
	
	UsuarioVal  = UsuarioVal.trim();
	PasswordVal = PasswordVal.trim();
	motivoDescuento = motivoDescuento.trim();
	//MontoDescto = MontoDescto.trim();

	//Validaciones
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	if(UsuarioVal == null || UsuarioVal == ""){
		v_message = "* Es necesario ingresar el Usuario.<br/>";
		v_error = true;
	}//fin:

	if(PasswordVal == null || PasswordVal == ""){
		v_message += "* Es necesario ingresar la Contraseñaa.<br/>";
		v_error = true;
	}//fin:

	if(motivoDescuento == null || motivoDescuento == ""){
		v_message += "* Es necesario ingresar el motivo.<br/>";
		v_error = true;
	}//fin:

	/*if(MontoDescto == null || MontoDescto == ""){
		v_message += "* Es necesario ingresar el Monto a descontar.<br/>";
		v_error = true;
	}//fin:if
	else{
		if(isNaN(parseFloat(MontoDescto))){
			v_message += "* Ingrese un Monto valido.<br/>";
			v_error = true;
		}//fin:
	}//fin:else
	 */

	if(v_error){
		htmlContent ="<div class='alert alert-warning alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
		htmlContent += v_message + "</div>";
		$('#descto-message').html(htmlContent);
		return;
	}//fin:
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//Validacion del Permiso
	var dataResquest = { 
		usuario: UsuarioVal, 
		password: PasswordVal,
		motivo: motivoDescuento
	}

	MontoDescto = parseFloat(MontoDescto);
	$('#descto-message').html('');
	
	$.ajax({
		url: '/caja/validardescuento',
		type: 'POST',
		dataType: "json",
		data: JSON.stringify(dataResquest),
		beforeSend: function(){
			$("#wait-permiso").html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Validando Permiso...</div>');
		},
		success: function(json){
			if(Boolean(json["permiso"])){								

				// $("#modalPerDescto .close").click();
				
				//aplicar el algorimto para calcular el descuento desglosado
				//aplicarDescuentoGlobal(MontoDescto);
			
				//Guardar en base de datos los renglones
				guardarDescuento(json["idmotivo"])
												
			}//fin:if
			else{
				htmlContent ="<div class='alert alert-warning alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
				htmlContent += json["message"] + "</div>";
				$('#descto-message').html(htmlContent);
			}//fin:
		},
		complete: function (){ $("#wait-permiso").html("");}
	});
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}//fin:fnAplicarDescto
//-----------------------------------------------------------------------------
function aplicarDescuentoGlobal(pdescto){

	var dtFechaNow  = new Date();
	var annioActual = dtFechaNow.getFullYear();
	var mesActual   = dtFechaNow.getMonth() + 1;

	var annio_ultimo = $("#edt-cobro-ultanio").val() != "" ?
		parseInt($("#edt-cobro-ultanio").val()) : 0;
	var mes_ultimo   = $("#hdd-ultpago-mes").val() != ""? parseInt($("#hdd-ultpago-mes").val()) : 0;

	var iIndice = 0;
	var mesIterate  = 1;
	var icountMeses = 0;

	var dtfechaIni   = new Date(1900, 0, 1);
	var dtfechaFin   = new Date(1900, 0, 1);
	var dtUltimoPago = new Date(annio_ultimo, (mes_ultimo -1), 1);

	var arrayMesesAux = new Array();
	var arrayMesesClone = new Array();

	var dataRows = mensualidadDT.rows().data();
	//Calcular la totalidad de meses de aduedos basados en el ultimo aÃ±o-mes
	dtfechaIni.setFullYear(annio_ultimo);
	dtfechaIni.setMonth(mes_ultimo - 1);

	dtfechaFin.setFullYear(annioActual);
	dtfechaFin.setMonth(mesActual - 1);
	mesIterate = mes_ultimo;
	iIndice = 0;

	for(iIdy = annio_ultimo; iIdy <= annioActual; iIdy++){
		if(mesIterate > 12){mesIterate = 1}
		for(iIdx = mesIterate; iIdx <= 12; iIdx++){
			mesIterate++;
			dtfechaIni.setFullYear(iIdy);
			dtfechaIni.setMonth(iIdx - 1);
			if(dtUltimoPago.getTime() < dtfechaIni.getTime() &&
				dtfechaIni.getTime() <= dtfechaFin.getTime()){
				arrayMesesAux[iIndice++] = iIdy.toString() + "-" + iIdx.toString();
			}//fin:
			//else{iIdx = 13}
		}//fin:for
	}//fin:for
	
	//Clonar el Contenido del datable para comparaciones
	for(idz=0; idz < dataRows.length; idz++){
		arrayMesesClone[idz] = dataRows[idz][4]  + "-" + dataRows[idz][5];
	}//fin:for

	var IndiceSearch = -1;
	//Iterar la totalidad de meses de adeudo para ubicar si ya fueron capturados
	for(idx = 0; idx < arrayMesesAux.length; idx++){
		IndiceSearch = arrayMesesClone.indexOf(arrayMesesAux[idx]);
		if(IndiceSearch > -1){icountMeses++;}
	}//fin:for

	var ftDestoXmes = 0.00;
	var keyAnnioMes = "";
	var ftTotalPagar = 0.00;
	
	var ftMontoMes = $("#hdd-mensualidad").val() != "" ?
		parseFloat($("#hdd-mensualidad").val()) : 0;

	//Validar si todos las mensualidades de adeudo estan capturadas en el datatable
	if(icountMeses == arrayMesesAux.length){
		//Realizar calculos para la asignacion de la parte proporcional del descuento
		ftDestoXmes = (pdescto/icountMeses);
		ftDestoXmes = ftDestoXmes.toFixed(2);

		//Actualizar los renglones la columna descto para concluir el adeudo
		//Iteracion de todos los renglones del datatable
		dataRows = mensualidadDT.rows().data();
		for(idx = 0; idx < dataRows.length; idx++){
			keyAnnioMes = dataRows[idx][4] + "-" + dataRows[idx][5];
			IndiceSearch = arrayMesesAux.indexOf(keyAnnioMes);
			if(IndiceSearch > -1){
				
				//ftTotalPagar = parseFloat(dataRows[idx][3]) - ftDestoXmes;
				ftTotalPagar = ftMontoMes - ftDestoXmes;
				ftTotalPagar = ftTotalPagar.toFixed(2);
				
				mensualidadDT.cell(idx, 2).data(ftDestoXmes);
				mensualidadDT.cell(idx, 3).data(ftTotalPagar);
			}//fin:if
		}//fin:for
		mensualidadDT.draw();
	}//fin:if(icountMeses == arrayMesesAux.length)
}//fin:aplicarDescuentoGlobal
//-----------------------------------------------------------------------------
function guardarMensualidades(idmotivo){

    // if (navigator.geolocation) {
     //    navigator.geolocation.getCurrentPosition(enviarDatosGuardarMensualidades, function(PositionError){
     //    	if(PositionError.code === 1){
     //    		showGeneralMessage("Para poder realizar un pago debe activar la localizacion: " + navigator.userAgent, "warning");
     //            // enviarDatosGuardarMensualidades(null);
	// 		}
	// 	});
    // }
    // else{
     //    enviarDatosGuardarMensualidades(null);
	// }

    enviarDatosGuardarMensualidades(idmotivo);

}//fin:guardarMensualidades


function enviarDatosGuardarMensualidades(idmotivo){
	var latitud = null;
	var longitud = null;
	// if(position){
    //     latitud = position.coords.latitude;
    //     longitud = position.coords.longitude;
	// }

    var boSuccess = false;
    var datajson = new Array();
    var dataRows = mensualidadDT.rows().data();
    var pdfFactura = null;
    var folioFactura = $("#folio_factura").val().trim();
    folioFactura = folioFactura ? folioFactura: null;


    // var folioInterno = $("#folio_interno").val().trim().toUpperCase();
    for (var i = 0; i < idsSelected.length; i++){
        var idx = idsSelected[i].id;
        if(i==0){
			pdfFactura = pdfFacturaBase64;
		}


        var d = {
            idcliente: $("#hdd-recolecta-idcte").val(),
            //mes:  fnNumberToMes(parseInt(dataRows[idx][5])),
            mes: parseInt(dataRows[idx][6]) ,
            anio: dataRows[idx][5],
            descuento: dataRows[idx][3],
            iva: 0.00,
            cantidad: dataRows[idx][4],
            fecha_pago: "",
            idforma_pago: $("#select-basura-fpago").val(),
            referencia_bancaria: $("#referencia_bancaria").val() != "" ? $("#referencia_bancaria").val() : null,
            validado: true,
            facturado: $("#chkFactCobro").is(":checked"),
            folio_factura: folioFactura,
            idcobratario: 0,
            caja: false,
            idbanco: 0,
            activo: true,
            envio_correo:false,
            recibo_impreso:false,
            incluido_corte: false,
            fecha_corte:"",
            idvisita_cliente: "",
            idtipo_cobro:1,
            idtipo_servicio: 0,
            concepto_servicio : "",
			latitud: latitud,
			longitud: longitud,
            folio_interno: $("#folio_interno").val() ? $("#folio_interno").val() : null,
			idmotivo: idmotivo,
			isdescan: isdescan,
			tottdescan: tottdescan,
			pdfFactura: pdfFactura,
			isfactura: isFactura
        };



        datajson.push(d);//fin:datajson.push
    }//fin:for

    var arrayMaxValues;
    var idcliente = parseInt($("#hdd-recolecta-idcte").val());
    var dtsClientes = clientesDT.rows().data();
    var mesLetra = "";



    //Invocar ajax para realizar el guardado
    $.ajax({
        //url: '/caja/guardarHistRecolecta',
        url: '/caja/savePagoMeses',
        type: 'POST',
        dataType: "json",
        data: JSON.stringify(datajson),
        beforeSend: function(){
        	$("#btn-cobro-mes").prop("disabled", true);
        	$("#btn-cobro-mes-facturado").prop("disabled", true);
			$(".btnCloseCobrar").prop("disabled", true);
            $('#load-save-recolecta').html('<div style="text-align: center;">' +
                '<i class="fa fa-spinner fa-pulse fa-2x"></i> Guardando...</div>');
        },
		error: function(jqXHR, textStatus, errorThrown){
        	if(jqXHR.status == 409){
                showGeneralMessage(jqXHR.responseText, "error", true);
			}
		},
        success: function(json){
        	rutaDropzoneFactura.removeAllFiles();
        	pdfFacturaBase64 = null;
            if(Boolean(json["success"])){

                boSuccess = true;
                $("#btn-cobro-mes").prop('disabled', true);
                $("#btn-cobro-mes-facturado").prop('disabled', true);
                $("#btn-cobro-add").prop('disabled', true);
				$(".btnCloseCobrar").prop("disabled", false);

                $.each(mensualidadDT.rows()[0], function (index, value) {
                    id = value;
                    $($("#rowPago" + value).find('td input[type="checkbox"]')[0]).prop( "disabled", true );
                });

                //Actualizar el idpago para la impresion del recibo-cliente
                $("#hdd-cobro-idpago").val(json["folio"]);
                $("#hdd-cobro-idpagop").val(json["idpago"]);

                //Habilitar el boton para la impresion del Recibo de Cobro
                $("#btn-recibo-cliente").prop('disabled', false);
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //sincronizar el año - mes despues de guardar el historial de pago.
                arrayMaxValues = fngetUltimoPagoMes();
                if(arrayMaxValues != null && Object.keys(arrayMaxValues).length > 0){
                    mesLetra = fnNumberToMes(parseInt(arrayMaxValues["mes"]));
					var dtSel = clientesDT.row( { selected: true } );
					var datasel = dtSel.data();
					var idxSelrow = dtSel.index();
                    // for(idx = 0; idx < dtsClientes.length; idx++){
                    //     if(parseInt(dtsClientes[idx][0]) == idcliente){
                    //         clientesDT.cell(idx, 4).data(arrayMaxValues["annio"]).draw();
                    //         clientesDT.cell(idx, 5).data(mesLetra).draw();
                    //         dtsClientes[idx][17] = parseInt(arrayMaxValues["mes"]);
                    //         idx = dtsClientes.length;
                    //     }//fin:if
                    // }//fin:for

					// clientesDT.cell(idxSelrow, 4).data(arrayMaxValues["annio"]).draw();
					// clientesDT.cell(idxSelrow, 5).data(mesLetra).draw();

					datasel[4] = arrayMaxValues["annio"];
					datasel[5] = mesLetra;
					dtSel.data(datasel);
                    clientesDT.draw();
                    buscarCliente(false);
                }//fin:if
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                showGeneralMessage("Registro(s) guardado(s) correctamente.", "success", true);
            }//fin:if
			else{
				$("#btn-cobro-mes").prop("disabled", false);
				$("#btn-cobro-mes-facturado").prop("disabled", false);
				$(".btnCloseCobrar").prop("disabled", false);
			}
        },
        complete: function () {
        	$('#load-save-recolecta').html("");
        }
    });
    return boSuccess;
}
//-----------------------------------------------------------------------------
function fngetUltimoPagoByTipo(ptipocobro){

	var arrayRowsMax = new Array();
	var arrayValues  = new Array();
	var max_annio = 0;
	var dataRows = historialDT.rows().data();
	
	//Obtener el maximo valor para el año
	jQuery.map(dataRows, function (obj) {
		if(parseInt(obj['annio']) > max_annio && 		
			ptipocobro == parseInt(obj['tipocobro'])){
			max_annio = parseInt(obj['annio']);
		}//fin:if
	});
			
	//Obtener todos los renglones con el max año
	for (var idx = 0; idx < dataRows.length; idx++){					
		if(parseInt(dataRows[idx]['annio']) == max_annio && 
			parseInt(ptipocobro) == parseInt(dataRows[idx]['tipocobro'])){
			arrayRowsMax.push({
				annio: max_annio, 
				mes: parseInt(dataRows[idx]['imes'])
			});
		}//fin:
	}//fin:for

	 //Ordenamos descendente para obtener la mensualidad mas Reciente en Interfaz
	 arrayRowsMax.sort(function (a, b){return (b.mes - a.mes)});
	 if(arrayRowsMax.length > 0){
		arrayValues["annio"] = arrayRowsMax[0].annio;
		arrayValues["mes"] = arrayRowsMax[0].mes;
	 }//fin:
	 arrayRowsMax = null;
	 return arrayValues;
}//fin:fngetUltimoPagoByTipo
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//function fnNoAplicarDescto(){$("#modalDesctoAnual").modal('hide');}
//-----------------------------------------------------------------------------
function fngetUltimoPagoMes(){

	var arrayRowsMax = new Array();
	var arrayValues  = new Array();
	var max_annio = 0;
	var iIndice = 0;

	var dtsMeses = mensualidadDT.rows().data();
//if($($("#rowPago" + idx).find('td input[type="checkbox"]')[0]).is(":checked")) {
	//Obtener el maximo valor para el aÃ±o
	jQuery.map(dtsMeses, function (obj, index) {
        if($($("#rowPago" + index).find('td input[type="checkbox"]')[0]).is(":checked")) {
            if (parseInt(obj[5]) > max_annio)
                max_annio = parseInt(obj[5]);
        }
	});

	//Obtener todos los renglones con el max aÃ±o
	for (var idx = 0; idx < dtsMeses.length; idx++){
		if(parseInt(dtsMeses[idx][5]) == max_annio ){
            if($($("#rowPago" + idx).find('td input[type="checkbox"]')[0]).is(":checked"))
			    arrayRowsMax.push({annio: max_annio, mes: parseInt(dtsMeses[idx][6])});
		}//fin:
	}//fin:for

	//Ordenamos descendente para obtener la mensualidad mas reciente
	arrayRowsMax.sort(function (a, b){return (b.mes - a.mes)});
	if(arrayRowsMax.length > 0){
		arrayValues["annio"] = arrayRowsMax[0].annio;
		arrayValues["mes"] = arrayRowsMax[0].mes;
	}//fin:
	arrayRowsMax = null;
	return arrayValues;
}//fin:fngetUltimoPagoMes
//-----------------------------------------------------------------------------
var isMobile = {
	Android: function() {
		return navigator.userAgent.match(/Android/i);
	},
	BlackBerry: function() {
		return navigator.userAgent.match(/BlackBerry/i);
	},
	iOS: function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
	Opera: function() {
		return navigator.userAgent.match(/Opera Mini/i);
	},
	Windows: function() {
		return navigator.userAgent.match(/IEMobile/i);
	},
	any: function() {
		return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	}
};//fin:isMobile
//-----------------------------------------------------------------------------
function fnprintRecibo(){
	
	var divToPrint=document.getElementById('recibo-print-body');
	var tlb1 =  document.getElementById('tblinfocliente').outerHTML;
	var tlb2 =  document.getElementById('tblReciboTotales').outerHTML;
	var req = null;



	//Impresion Recibo de Cobro
	if(isMobile.any()) {	
		req = new XMLHttpRequest();	
		req.open("POST", "/caja/generarpdf", true);
		req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		req.responseType = "blob";
		
		req.onload = function (event){
			if (req.readyState == 4 && req.status == 200) {					
				var blob = new Blob([req.response], { type: 'application/pdf' });
				var objUrl = window.URL.createObjectURL(blob);
				window.open(objUrl);
				window.URL.revokeObjectURL(objUrl);
				//var link=document.createElement('a');
				//link.href=window.URL.createObjectURL(blob);
				//link.target = '_blank';
				//link.click();			
			}//fin:				
		};
		let dataSenReccio = getDataToPrint();
		//var datajson = {correo: emailTo, html : htmlbody}
		req.send(JSON.stringify({tblcliente : dataSenReccio.tblcliente, tbltotales: dataSenReccio.tbltotales}));
	}//fin:if
	else{		
		var newWin=window.open('','Impresion de Recibo');
		newWin.document.open();
		newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
		newWin.document.close();
		setTimeout(function(){newWin.close();},10);					
		
		//Implementacion para generar pdf evitando el pool de impresion
		/*
		req = new XMLHttpRequest();	
		req.open("POST", "/caja/generarpdf", true);
		req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		req.responseType = "blob";
		
		req.onload = function (event){
			if (req.readyState == 4 && req.status == 200) {					
				var blob = new Blob([req.response], { type: 'application/pdf' });
				var objUrl = window.URL.createObjectURL(blob);
				window.open(objUrl);
				window.URL.revokeObjectURL(objUrl);
				//var link=document.createElement('a');
				//link.href=window.URL.createObjectURL(blob);
				//link.target = '_blank';
				//link.click();			
			}//fin:				
		};			
		req.send(JSON.stringify({tblcliente : tlb1, tbltotales: tlb2}));	
		*/		
	}//fin:else		
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@	
}//fin:divprint		
//-----------------------------------------------------------------------------
function fnPrintReciboMes(){
		
	var fechaCreacion = "";
	var dtToday = new Date();
	var dd = dtToday.getDate();
	var mm = dtToday.getMonth() + 1;
	var yyyy = dtToday.getFullYear();
	
	//dd-mm-yyyy	
	//Dia
	fechaCreacion = parseInt(dd) < 10 ? ("0" + dd.toString() + "-"): (dd.toString() + "-");
	//Mes
	fechaCreacion += parseInt(mm) < 10 ? ("0" + mm.toString() + "-"): (mm.toString() + "-");	
	fechaCreacion += yyyy.toString();
	
	$("#recibo-nombre").html($("#edt-cobro-Nombre").val());
	$("#recibo-direccion").html($("#hdd-recolecta-direc").val());
	$("#recibo-colonia").html($("#hdd-recolecta-colonia").val());
	$("#recibo-ruta").html($("#hdd-recolecta-ruta").val());
	$("#recibo-idcliente").html( $("#hdd-recolecta-idcte").val());	
	$("#recibo-concepto").html("RECIBO DE RECOLECCION EN EL DIA: "+ fechaCreacion);		
	$("#recibo-folio").html($("#hdd-cobro-idpago").val());
	
	//iterar para obtener el listado de meses
	var dtsMeses = mensualidadDT.rows().data();
	var mesespag = "";
	var ftTotalRecibo = 0.00;
	var ftDescuento = 0.00;
	var ftTotalMeses = 0.00;
	var cantidaLetras = "";
	var msgMesesPag = "";
	var htmltrContent = "";

	for(idx = 0; idx < dtsMeses.length; idx++){
        if($($("#rowPago" + idx).find('td input[type="checkbox"]')[0]).is(":checked")) {
        	if(idx == 0 || (idx > 0 && dtsMeses[idx][5] != dtsMeses[idx - 1][5])){
				mesespag += '<br>'+ dtsMeses[idx][5] + ': ';
				if(idx > 0){
					mesespag.substr(0, (mesespag.length - 2))
				}
			}
        	else{
				if (mesespag != "") {
					mesespag += ", ";
				}
			}
            mesespag += fnNumberToMes(parseInt(dtsMeses[idx][6]));
            ftTotalRecibo += parseFloat(dtsMeses[idx][4]);
            ftDescuento += parseFloat(dtsMeses[idx][3]);
            ftTotalMeses += parseFloat(dtsMeses[idx][2]);
        }
	}//fin:for(idx = 0; idx < dtsMeses.length; idx++)
	ftTotalRecibo = ftTotalRecibo.toFixed(2);
	//ftDescuento   = ftDescuento.toFixed(2);
	cantidaLetras = NumeroALetras(ftTotalRecibo);
	msgMesesPag = "PAGO CORRESPONDIENTE AL MES (ES) DE: " + mesespag;
	$("#recibo-meses").html(msgMesesPag);
	if($('table#tblReciboTotales tr#row-tot-letras').length > 0){
		$('table#tblReciboTotales tr#row-tot-letras').remove();
	}//fin:
	if($('table#tblReciboTotales tr#row-tot-pagar').length > 0){
		$('table#tblReciboTotales tr#row-tot-pagar').remove();
	}//fin:
	if($('table#tblReciboTotales tr#row-tot-descto').length > 0){
		$('table#tblReciboTotales tr#row-tot-descto').remove();
	}//fin:
	//********************************************************************************************
	if(ftDescuento > 0.00){			
		//Eliminar tr sin descuento
		if($('table#tblReciboTotales tr#row-total2').length > 0){
			$('table#tblReciboTotales tr#row-total2').remove();		
		}//fin:
		
		ftDescuento  = ftDescuento.toFixed(2);
		ftTotalMeses = ftTotalMeses.toFixed(2);
		
		//Agregar contenido dinamico para el recibo
		htmltrContent  = "<tr id='row-tot-letras'><td align='left' width='60%'>Son:  <label id='recibo-cantletras'></label></td>";
		htmltrContent += "<td align='right' width='40%'>Mes(es): <label id='recibo-totmeses'></label></td></tr>";
		htmltrContent += "<tr id='row-tot-descto'><td align='left' width='60%'><label></label></td>";
		htmltrContent += "<td align='right' width='40%'>Descuento:  <label id='recibo-descto'></label></td></tr>";		
		htmltrContent += "<tr id='row-tot-pagar'><td align='left'  width='60%'><label></label></td>"
		htmltrContent += "<td align='right' width='40%'>Total pagado:  <label id='recibo-totalpag'></label></td></tr>";

		$('table#tblReciboTotales tr#row-total1').after(htmltrContent);

		//Asignacion de los valores dinamicos a los tr
		$("#recibo-cantletras").html(cantidaLetras);
		$("#recibo-totmeses").html("$ " + ftTotalMeses.toString());
		$("#recibo-descto").html("$ " + ftDescuento.toString());
		$("#recibo-totalpag").html("$ " + ftTotalRecibo.toString());		
	}//fin:if
	else{
		//Agregar contenido dinamico para el recibo
		htmltrContent  = "<tr id='row-tot-letras'><td align='left' width='60%'>Son:  <label id='recibo-cantletras'></label></td>";
		htmltrContent += "<td align='right' width='40%'>Total pagado:  <label id='recibo-totalpag'></label></td></tr>";

		$('table#tblReciboTotales tr#row-total1').after(htmltrContent);

		//Asignacion de los valores dinamicos a los tr
		$("#recibo-cantletras").html(cantidaLetras);
		$("#recibo-totalpag").html("$ " + ftTotalRecibo.toString());

		$("#recibo-numletras").html(cantidaLetras);
		$("#recibo-total").html("$ " + ftTotalRecibo.toString());		
	}//fin:else
	//$("#recibo-numletras").html(cantidaLetras);
	//$("#recibo-total").html("$ " + ftTotalRecibo.toString());
	//********************************************************************************************	
	//Invocar el Modal para impresion de Recibo
	if(isdescan){
		$("#txtMensajePromAn").show();
	}
	else{
		$("#txtMensajePromAn").hide();
	}

	$("#modalRecibo").modal({backdrop: 'static', keyboard: false, show: true});	
}//fin:fnPrintReciboMes
//-----------------------------------------------------------------------------
function fnprintNotaServicio(){
	
	var ConceptoCobro = "";
	
	var dtFecha = $("#edt-servicio-fecha").val();
	//0 1 2 3 4 5 6 7 8 9
	//Y Y Y Y - M M - D D
	
	//dd-mm-yyyy
	var fechaCreacion = dtFecha.substr(8, 2) + "-" + dtFecha.substr(5, 2) + "-" + dtFecha.substr(0, 4);  	
	//************************************************************
	//Eliminar los nodos de Cobro con descuento en caso de existir	
	if($('table#tblReciboTotales tr#row-tot-letras').length > 0){
		$('table#tblReciboTotales tr#row-tot-letras').remove();		
	}//fin:
	
	if($('table#tblReciboTotales tr#row-tot-descto').length > 0){
		$('table#tblReciboTotales tr#row-tot-descto').remove();		
	}//fin:
	
	if($('table#tblReciboTotales tr#row-tot-pagar').length > 0){
		$('table#tblReciboTotales tr#row-tot-pagar').remove();		
	}//fin:	
	
	var htmltrContent  = "<tr id='row-total2'><td align='left' width='60%'>Son:  <label id='recibo-numletras'></label></td>";
		htmltrContent += "<td align='right' width='40%'>Total pagado:  <label id='recibo-total'></label></td></tr>";
	//Agregamos el nodo para pago sin descuento
	if($('table#tblReciboTotales tr#row-total2').length <= 0){
		$('table#tblReciboTotales tr#row-total1').after(htmltrContent);	
	}//fin:		
	//************************************************************	
	$("#recibo-nombre").html($("#hdd-servicio-nombre").val());
	$("#recibo-direccion").html($("#hdd-servicio-direc").val());
	$("#recibo-colonia").html($("#hdd-servicio-colonia").val());
	$("#recibo-ruta").html($("#hdd-servicio-ruta").val());	
	$("#recibo-idcliente").html($("#hdd-servicio-idcte").val());	
	
	ConceptoCobro = $("#edt-servicio-concepto").val();
	$("#recibo-concepto").html(ConceptoCobro);		
	$("#recibo-folio").html($("#hdd-servicio-idpago").val());
	
	//iterar para obtener el listado de meses
	var ftTotalRecibo = 0.00;
	var cantidaLetras = "";
	var msgMesesPag = "";
		
	ftTotalRecibo = parseFloat($("#edt-servicio-monto").val());
	ftTotalRecibo = ftTotalRecibo.toFixed(2);
	cantidaLetras = NumeroALetras(ftTotalRecibo);	
		
	msgMesesPag = "FECHA ELABORACION:  " + fechaCreacion;
		
	$("#recibo-meses").html(msgMesesPag);	
	$("#recibo-numletras").html(cantidaLetras);
	$("#recibo-total").html("$ " + ftTotalRecibo.toString());
	
	//Invocar el Modal para impresion de Recibo
	$("#modalRecibo").modal({backdrop: 'static', keyboard: false, show: true});		
}//fin:fnprintNotaServicio
//-----------------------------------------------------------------------------
function geocodeAddress(resultsMap) {
    
	var address = document.getElementById('cliente-input-gelocation').value;
    if(address == "") return;
    geocoder.geocode({'address': address}, function(results, status){
        if(status === google.maps.GeocoderStatus.OK){            
			marker = createMarker(map, marker, 
				results[0].geometry.location.lat(), 
				results[0].geometry.location.lng());  			
			
			//map.setZoom(4);
        }//fin:else
		else {
            if(status === google.maps.GeocoderStatus.ZERO_RESULTS){
                showGeneralMessage("No se encontraton resultados", "warning", true);
            }//fin:if
            else{
                showGeneralMessage("Ocurrió un error al realizar la búsqueda", "warning", true);
            }//fin:
        }//fin:else
    });
}//fin:geocodeAddress
//-----------------------------------------------------------------------------
function clearMarker(marker){
	
	if(marker == null) return;
	google.maps.event.clearListeners(marker, 'dragend');
	marker.setMap(null);
	marker = null;
	return null;	
}//fin:clearMarker
//-----------------------------------------------------------------------------
function fitBoundMarker(map, marker){
	var bounds = new google.maps.LatLngBounds();
	bounds.extend(marker.getPosition());
	map.fitBounds(bounds);
}//fin:fitBoundMarker
//-----------------------------------------------------------------------------	
function createMarker(map, marker, lat, lng){
	
	clearMarker(marker);
	google.maps.event.trigger(map, 'resize');
	marker = new google.maps.Marker({
		position: {
			lat: parseFloat(lat),
			lng: parseFloat(lng)
		},
		map: map,
		draggable : true
	});

	google.maps.event.addListener(marker, 'dragend', function(){
		var position = marker.getPosition();
		var lat = position.lat();
		var lng = position.lng();
		//updateLatLng(lat, lng);
	});
	
	fitBoundMarker(map, marker);
	google.maps.event.trigger(map, 'resize');
	return marker;
}//fin:createMarker
//-----------------------------------------------------------------------------	
function fnUpdateUbicacion(){
	
	var mapLat = marker.getPosition().lat();
	var mapLong = marker.getPosition().lng();
	var v_idcliente = parseInt($("#hdd-map-idcliente").val());
	
	var dataJson = {
		idcliente: v_idcliente, 
		latitud:mapLat, 
		longitud:mapLong, 
		ubicado: true
	}

	$.ajax({
		url: '/caja/updateUbicacion',
		data: JSON.stringify(dataJson),
		type: 'POST',
		dataType: "json",
		beforeSend: function(){			
			$('#loading-map').html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Actualizando ubicacion...' +
				'</div>');			
		},
		success: function(json){					
			if(Boolean(json["success"])){
				
				//Actualizar latitud y longitud en datable de clientes
				var dtsClientes = clientesDT.rows().data();
				for(idx = 0; idx < dtsClientes.length; idx++){
					if(parseInt(dtsClientes[idx][0]) == v_idcliente){
						dtsClientes[idx][10] = mapLat;
						dtsClientes[idx][11] = mapLong;			
						idx = dtsClientes.length;
					}//fin:if
				}//fin:for
				clientesDT.draw();
				
				showGeneralMessage("¡La ubicacion se actualizo correctamente!", "success", true);	
			}//fin:if. 					
		},
		complete: function (){$('#loading-map').html("");}
	});	
}//fin:fnUpdateUbicacion
//-----------------------------------------------------------------------------	
function fnshowModalCorreo(){
	
	$("#edtEmailTo").val($("#hdd-recibo-email").val());
	
	$("#modalEmailRecibo").modal({backdrop: 'static', keyboard: false, show: true});	
}//fin:fnshowModalCorreo
//-----------------------------------------------------------------------------	
function fnsendEmailRecibo(){
	
	//var htmlbody = "<html><body>" + document.getElementById('recibo-print-body').innerHTML + "</body></html>";
	var tlb1 =  document.getElementById('tblinfocliente').outerHTML;
	var tlb2 =  document.getElementById('tblReciboTotales').outerHTML;
	var txtPromAnual =  document.getElementById('txtMensajePromAn').outerHTML;
	if(isdescan){
		tlb2 += '<br/>' + txtPromAnual;
	}
	var emailTo = $("#edtEmailTo").val();
	let dataSenReccio = getDataToPrint();
	//var datajson = {correo: emailTo, html : htmlbody}
	//var datajson = {correo: emailTo, tblcliente : dataSenReccio.tblcliente, tbltotales: dataSenReccio.tbltotales}
	var datajson = {
		correo: emailTo,
		idcliente: parseInt($("#hdd-recolecta-idcte").val()),
		idpago: parseInt($("#hdd-cobro-idpagop").val())
	}

	$.ajax({
		url: '/caja/enviarEmail',
		data: JSON.stringify(datajson),
		type: 'POST',
		dataType: "json",
		beforeSend: function(){			
			$('#loading-email').html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Enviando correo...</div>');		
		},
		success: function(json){
			if(Boolean(json["success"])){
				showGeneralMessage("El recibo se envio por correo correctamente.", "success", true);
			}//fin:if
			else{
				showGeneralMessage(json["message"], "warning", true);
			}//fin:else							
		},
		complete: function () {$("#loading-email").html('');}
	});	
}//fin:fnsendEmailRecibo
//-----------------------------------------------------------------------------		
function fnModalReimprimir(){
		
	if(clientesDT.rows('.selected').data().length == 0){
		showGeneralMessage("Seleccione un registro", "warning");
		return ;
	}//fin:if
	
	$("#hdd-reprint-idcliente").val('');
	$("#hdd-reprint-nombre").val('');
	$("#hdd-reprint-direccion").val('');
	$("#hdd-reprint-colonia").val('');
	$("#hdd-reprint-ruta").val('');
	//$("#hdd-reprint-email").val('');
	$("#hdd-recibo-email").val('');
	
	var dtsClientes = clientesDT.rows("tr.selected").data();
	var idCliente = parseInt(dtsClientes[0][0]); 	
	
	var dir_cliente = "";	
	dir_cliente = dtsClientes[0][2];
	if(dtsClientes[0][3] != "" )
		dir_cliente += " No. " + dtsClientes[0][3];
	
	if(dtsClientes[0][6] != "")
		dir_cliente += " por " + dtsClientes[0][8];
	
	if(dtsClientes[0][7] != "")
		dir_cliente += " y " + dtsClientes[0][20];
					
	$("#hdd-reprint-idcliente").val(idCliente);	
	$("#hdd-reprint-nombre").val(dtsClientes[0][1]);
	$("#hdd-reprint-direccion").val(dir_cliente);
	$("#hdd-reprint-colonia").val(dtsClientes[0][6]);
	$("#hdd-reprint-ruta").val(dtsClientes[0][18]);
	//$("#hdd-reprint-email").val(dtsClientes[0][19]);
	
	$("#hdd-recibo-email").val(dtsClientes[0][19]);
	
	RecibosDT.clear();
	RecibosDT.draw();
	
	//$("#edt-rprint-fechaini").val('');
	//$("#edt-rprint-fechafin").val('');
	$("#edt-rprint-fechaini").datepicker("setDate" , getFecNow_ddmmyyyy());
	$("#edt-rprint-fechafin").datepicker("setDate" , getFecNow_ddmmyyyy());
			
	$("#modalReimpresion").modal({backdrop: 'static', keyboard: false, show: true});	
}//fin:fnModalReimprimir
//-----------------------------------------------------------------------------	
function fnbuscarRecibos(){
	
	//Validacion para seleccion de fechas
	var v_error = false;
	var v_message ="";
	var v_fechaini = $("#edt-rprint-fechaini").val();
	var v_fechafin = $("#edt-rprint-fechafin").val();

	//Validacion para la Fecha inicial   
	v_fechaini = v_fechaini.trim();
	v_fechafin = v_fechafin.trim();

	//Validacion de la fecha de inicio
	if(v_fechaini == null || v_fechaini == ""){
		v_message += "* Es necesario ingresar la Fecha Inicial.<br/>";
		v_error = true;
		//alert('* Es necesario ingresar la Fecha.<br/>');
	}//fin:
	else{
		if(!isValidDate_ddmmyyyy(v_fechaini)){
			v_message += "* La fecha debe tener formato dd/mm/yyyy.<br/>";
			v_error = true;
		}//fin:if
	}//fin:

	//Validacion de la fecha Final
	if(v_fechafin == null || v_fechafin == ""){
		v_message += "* Es necesario ingresar la Fecha Final.<br/>";
		v_error = true;               
	}//fin:
	else{
		if(!isValidDate_ddmmyyyy(v_fechafin)){               
			v_message += "* La fecha debe tener formato dd/mm/yyyy.<br/>";
			v_error = true;
		}//fin:if
	}//fin:

	//validar que fechaini <= fechafin
	//dd/mm/yyy
	var arrayfecini = v_fechaini.split("/");
	var arrayfecfin = v_fechafin.split("/");
	var dtfecini = new Date(parseInt(arrayfecini[2]),  (parseInt(arrayfecini[1]) -1), parseInt(arrayfecini[0]));
	var dtfecfin = new Date(parseInt(arrayfecfin[2]),  (parseInt(arrayfecfin[1]) -1), parseInt(arrayfecfin[0]));

	if(dtfecini.getTime() > dtfecfin.getTime()){
	   v_message += "* La fecha inicial no debe ser mayor que la final.<br/>";
	v_error = true;
	}//fin:if

	if(v_error){
		showGeneralMessage(v_message, "warning", true);
		return;
	}//fin:
	//fin validaciones
			
	//Formato fecha: dd/mm/yyyy
	var auxFecha= $("#edt-rprint-fechaini").val();
	var dd = auxFecha.substr(0,2);
	var mm = auxFecha.substr(3,2);
	var yyyy = auxFecha.substr(6,4);
	
	var feciniIso = yyyy + "-" + mm + "-" + dd;
	
	auxFecha = $("#edt-rprint-fechafin").val();
	var dd = auxFecha.substr(0,2);
	var mm = auxFecha.substr(3,2);
	var yyyy = auxFecha.substr(6,4);
	
	var fecfinIso = yyyy + "-" + mm + "-" + dd;
	
	var dataPost = {	
		idcliente: parseInt($("#hdd-reprint-idcliente").val()), 
		fechaini : feciniIso, 
		fechafin : fecfinIso
	}
			
	$.ajax({
		url: '/caja/searchRecibos',
		data: JSON.stringify(dataPost),
		type: 'POST',
		dataType: "json",
		beforeSend: function(){					
			$('#wait-search-recibos').html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Buscando Recibos...</div>');					
		},
		success: function(json){
			if(json.length == 0){
				showGeneralMessage("No se encontraron registros", "warning", true);
				return;
			}//fin:if(json.length == 0)
			
			RecibosDT.clear();
			if(json.length > 0) {
				for(iIdx = 0; iIdx < json.length; iIdx++){					
					RecibosDT.row.add({
						"idpago": json[iIdx]['idpago'],
						"fecpago": json[iIdx]['fecpago'],
						"feccreacion": json[iIdx]['feccreacion'],
						"subtotal":  json[iIdx]['subtotal'],
						"descto":    json[iIdx]['descto'],
						"total":     json[iIdx]['total'],
						"activo":    json[iIdx]['activo'],
						"tipocobro": json[iIdx]['tipocobro'],
						"folio": json[iIdx]['folio'],
						"cobratario": json[iIdx]['cobratario'],
					}).draw();
				}//fin:for							
				setTimeout(function () {RecibosDT.columns.adjust().draw();},500);
			}//fin:if(json.length > 0)						
		},
		complete: function () { $("#wait-search-recibos").html("");}
	});		
}//fin:fnbuscarRecibos
//-----------------------------------------------------------------------------
function fnClearModalRecibo(){
	
	//Limpiar controles del Recibo de Pago
	$("#recibo-nombre").html("");
	$("#recibo-direccion").html("");
	$("#recibo-colonia").html("");
	$("#recibo-ruta").html("");
	$("#recibo-idcliente").html("");
	$("#recibo-concepto").html("");
	$("#recibo-meses").html("");
	
	if($("#recibo-numletras").length > 0){
		$("#recibo-numletras").html("");
	}//fin:if	
	if($("#recibo-total").length > 0){
		$("#recibo-total").html("");
	}//fin:if
		
	$("#recibo-folio").html("");
	//$("#hdd-recibo-email").val('');			
}//fin:fnClearModalRecibo
//-----------------------------------------------------------------------------
function fnReprintRecibo(){
	
	//validaciones
	if(RecibosDT.rows('.selected').data().length == 0){
		showGeneralMessage("Seleccione un registro", "warning");
		return ;
	}//fin:if	
			
	var dtsRecibos = RecibosDT.rows("tr.selected").data();
	var	direccionCte = "";
	var dir_cliente = "";
	var idtipocobro = parseInt(dtsRecibos[0]['tipocobro']);	
	var idpago = (dtsRecibos[0]['idpago']);
	var icountMeses = 0;

	$("#hdd-cobro-idpagop").val(idpago);
	$("#hdd-recolecta-idcte").val($("#hdd-reprint-idcliente").val());

	//iterar para obtener el listado de meses
	var ftTotalRecibo = parseFloat(dtsRecibos[0]['total']);
	var ftDescuento = parseFloat(dtsRecibos[0]['descto']);
	var ftTotalMeses = parseFloat(dtsRecibos[0]['subtotal']);
	var dtFechaPago = dtsRecibos[0]['fecpago'];
	var cantidaLetras = "";
	var msgMesesPag = "";
	var htmltrContent = "";
	var mesespagados = "";
	
	fnClearModalRecibo();
	//************************************************************	
	//Asignacion de los valores para el modal de Recibo de Pago
	$("#recibo-nombre").html($("#hdd-reprint-nombre").val());
	$("#recibo-direccion").html($("#hdd-reprint-direccion").val());
	$("#recibo-colonia").html($("#hdd-reprint-colonia").val());
	$("#recibo-ruta").html($("#hdd-reprint-ruta").val());	
	$("#recibo-idcliente").html($("#hdd-reprint-idcliente").val());	
	$("#recibo-folio").html(dtsRecibos[0]['folio']);
	$("#usuario-cobranza").html(dtsRecibos[0]['cobratario']);
	//************************************************************
	idtipocobro = 1;
	switch(idtipocobro){
		//Cobro de Mensualidades
		case 1:{														
			ftTotalRecibo = ftTotalRecibo.toFixed(2);		
			cantidaLetras = NumeroALetras(ftTotalRecibo);
			if(ftDescuento > 0.00){			
				//Eliminar tr sin descuento
				if($('table#tblReciboTotales tr#row-total2').length > 0){
					$('table#tblReciboTotales tr#row-total2').remove();		
				}//fin:
				
				ftDescuento  = ftDescuento.toFixed(2);
				ftTotalMeses = ftTotalMeses.toFixed(2);
				
				//Agregar contenido dinamico para el recibo
				htmltrContent  = "<tr id='row-tot-letras'><td align='left' width='60%'>Son:  <label id='recibo-cantletras'></label></td>";
				htmltrContent += "<td align='right' width='40%'>Mes(es): <label id='recibo-totmeses'></label></td></tr>";
				htmltrContent += "<tr id='row-tot-descto'><td align='left' width='60%'><label></label></td>";
				htmltrContent += "<td align='right' width='40%'>Descuento:  <label id='recibo-descto'></label></td></tr>";		
				htmltrContent += "<tr id='row-tot-pagar'><td align='left'  width='60%'><label></label></td>"
				htmltrContent += "<td align='right' width='40%'>Total pagado:  <label id='recibo-totalpag'></label></td></tr>";

				if($('table#tblReciboTotales tr#row-tot-letras').length <= 0){			
					$('table#tblReciboTotales tr#row-total1').after(htmltrContent);
				}//fin:						
				//Asignacion de los valores dinamicos a los tr
				$("#recibo-cantletras").html(cantidaLetras);
				$("#recibo-totmeses").html("$ " + ftTotalMeses.toString());
				$("#recibo-descto").html("$ " + ftDescuento.toString());
				$("#recibo-totalpag").html("$ " + ftTotalRecibo.toString());		
			}//fin:if
			else{
				$("#recibo-numletras").html(cantidaLetras);
				$("#recibo-total").html("$ " + ftTotalRecibo.toString());		
			}//fin:else					
			$("#recibo-concepto").html("RECIBO DE RECOLECCION EN EL DIA: "+ dtFechaPago);
			//********************************************************************************************
			//invocar ajax para obtener el detalle del Historial de pago y procesar los meses
			var datajson = {idpago: parseInt(idpago)} 
			
			$.ajax({
				url: '/caja/getHistorialPagos',
				data: JSON.stringify(datajson),
				type: 'POST',
				dataType: "json",
				beforeSend: function(){										
					$('#wait-search-recibos').html('<div style="text-align: center;">' +
						'<i class="fa fa-spinner fa-pulse fa-2x"></i> Generando Recibo...</div>');						
				},
				success: function(json){
					if(json.length > 0){						
						//iterar el resultado para obtener los meses cubiertos
						mesespagados = "";
						for(idx = 0; idx <json.length; idx++){
							icountMeses++;


							if(idx == 0 || (idx > 0 && json[idx]["anio"] != json[idx - 1]["anio"])){
								mesespagados += '<br>'+ json[idx]["anio"] + ': ';
								if(idx > 0){
									mesespagados.substr(0, (mesespagados.length - 2))
								}
							}
							else{
								if (mesespagados != "") {
									mesespagados += ", ";
								}
							}
							mesespagados += json[idx]['mes'] ;
						}//fin:for

						msgMesesPag = "PAGO CORRESPONDIENTE AL MES (ES) DE: " + mesespagados;

						$("#recibo-meses").html(msgMesesPag);
						
						//Invocar el Modal para impresion de Recibo
						$("#modalRecibo").modal({backdrop: 'static', keyboard: false, show: true});
					}//fin:if(json.length == 0)								
				},
				complete: function () {$("#wait-search-recibos").html("");}
			});						
		}//fin:case 
		break;
		
		//Cobro de Servicios
		case 2:{			
			//************************************************************
			//Eliminar los nodos de Cobro con descuento en caso de existir	
			if($('table#tblReciboTotales tr#row-tot-letras').length > 0){
				$('table#tblReciboTotales tr#row-tot-letras').remove();		
			}//fin:
			
			if($('table#tblReciboTotales tr#row-tot-descto').length > 0){
				$('table#tblReciboTotales tr#row-tot-descto').remove();		
			}//fin:
			
			if($('table#tblReciboTotales tr#row-tot-pagar').length > 0){
				$('table#tblReciboTotales tr#row-tot-pagar').remove();		
			}//fin:	
			
			htmltrContent  = "<tr id='row-total2'><td align='left' width='60%'>Son:  <label id='recibo-numletras'></label></td>";
			htmltrContent += "<td align='right' width='40%'>Total pagado:  <label id='recibo-total'></label></td></tr>";
			//Agregamos el nodo para pago sin descuento
			if($('table#tblReciboTotales tr#row-total2').length <= 0){
				$('table#tblReciboTotales tr#row-total1').after(htmltrContent);	
			}//fin:		
			//************************************************************	
			ftTotalRecibo = ftTotalRecibo.toFixed(2);
			cantidaLetras = NumeroALetras(ftTotalRecibo);			
			$("#recibo-numletras").html(cantidaLetras);
			$("#recibo-total").html("$ " + ftTotalRecibo.toString());			
			
			//Invocar ajax para obtener la fecha de creacion y el concepto					
			var datapost = {idpago: parseInt(idpago)}
			
			$.ajax({
				url: '/caja/getHistorialPagos',
				data: JSON.stringify(datapost),
				type: 'POST',
				dataType: "json",
				beforeSend: function(){										
					$('#wait-search-recibos').html('<div style="text-align: center;">' +
						'<i class="fa fa-spinner fa-pulse fa-2x"></i> Generando Recibo...</div>');						
				},
				success: function(json){
					//La relacion entre cliente.pagos e Historial:pagos en uno a uno
					//Por lo que tomanos el primer reglon del resulset
					if(json.length > 0){
						msgMesesPag = "FECHA ELABORACION:  " + json[0]['fechapago'];		
						$("#recibo-meses").html(msgMesesPag);	
						
						var concepto_servicio = json[0]['concepto'];
						$("#recibo-concepto").html(concepto_servicio);		
						
						//Invocar el Modal para impresion de Recibo
						$("#modalRecibo").modal({backdrop: 'static', keyboard: false, show: true});
					}//fin:if(json.length == 0)								
				},
				complete: function () {$("#wait-search-recibos").html("");}
			});																		
		}//fin:case 2
		break;		
	}//fin:switch					
}//fin:function
//-----------------------------------------------------------------------------  
//para validar fecha en formato dd/mm/yyy
function isValidDate_ddmmyyyy(dateString) {
	var regEx = /^\d{2}\/\d{2}\/\d{4}$/;
	return dateString.match(regEx) != null;
}//fin:isValidDate
//------------------------------------------------------------------------------
function getFecNow_ddmmyyyy(){
	
	var dtNow = new Date();
	var dd_now = dtNow.getDate();
	var mm_now = dtNow.getMonth() + 1;
	var yyyy_now = dtNow.getFullYear();
	var stFechaNow = dd_now.toString() + "/" + mm_now.toString() + "/" + yyyy_now.toString();
	
	return stFechaNow;
}//fin:getFecNow_ddmmyyyy
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
function fnModalCobroMasivo(){		
	
	//To Do : Reiniciar Controles para el modal de Cobro Masivo		
	RSocialDT.clear();
	RSocialDT.draw();
	
	//$("#select-masivo-rsocial").val($("#select-masivo-rsocial option:first").val());
	$("#select-masivo-rsocial option:first").prop("selected", true);
	$("#select-masivo-fpago option:first").prop("selected", true);
	$("#chkmasFactura").prop("checked", false).change();
	$('#chkDesctoMasivo').prop("checked", false).change();
	
	$("#select-masivo-mes").val((new Date()).getMonth() + 1);
	$("#select-masivo-anio").val((new Date()).getFullYear());
	
	$("#edt-masivo-referencia").val("");
	$("#edt-masivo-referencia").prop("disabled", true);	
	$("#edt-masivo-folfac").val("");
	$("#edt-masivo-folfac").prop("disabled", true);	
	
	$("#btn-masivo-cobrar").prop('disabled', false);
	$("#btn-masivo-add").prop('disabled', false);	
	$('#aviso-adeudo-masivo').html("");
	
	$("#btn-rsocial-recibo").prop('disabled', true);	
		
	//Controles Hidden
	$("#hdd-idpagomasivo").val("");	
	$("#hdd-idrazonsocial").val("");
	$("#hdd-rsocial-email").val("");	
	
	$("#modalCobroMasivo").modal({backdrop: 'static', keyboard: false, show: true});
}//fin:function
//------------------------------------------------------------------------------
function fnaddMesMasivo(){
	
	//Obtener el idrazon social para consultar los clientes	
	var idrsocial = $("#select-masivo-rsocial").val();
	var ftMensualidad = 0.00;
	var annio_mes = "";	
	var ui_annio = parseInt($("#select-masivo-anio").val());
	var ui_mes = parseInt($("#select-masivo-mes").val());
	var SelectizeRSocial = $('#select-masivo-rsocial').get(0).selectize;
	var optionSel = SelectizeRSocial.options[idrsocial];	
	var pfisica = parseInt(optionSel['fisica']) == 1 ? true : false;
	var dataJson = {idrazon_social: parseInt(idrsocial), fisica: pfisica};	
	var boEvalua = false;
	
	var MesConDescto = 12;
	
	$.ajax({
		url: '/caja/getclientesByrsocial',
		data: JSON.stringify(dataJson),
		type: 'POST',
		dataType: "json",
		beforeSend: function(){													
			$('#loading-rsocial').html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Obteniendo Clientes...</div>');		
		},
		success: function(json){				
			//Agregar los clientes dependiendo del año - mes
			annio_mes = $("#select-masivo-mes :selected").text().substr(0, 3) + " " + $("#select-masivo-anio :selected").val();
			if(json.length == 0){
				showGeneralMessage("No se encontraron registros para evaluar", "warning", true);
				return;
			}//fin:if(json.length == 0)
			
			//Validar que todos los clientes tengan las mismas condiciones de adeudo.
			//Tomamos como pivote el primer clientes para validar las condiciones con respeco a los demas
			var arrayErroresMes = new Array();
			var pivoteCliente = json[0];
			var boDif = false;
			if(json.length > 1){					
				for(iIdx = 1; iIdx < json.length; iIdx++){
					if(parseInt(pivoteCliente['ultimo_aniopago']) !=
						parseInt(json[iIdx]['ultimo_aniopago']) || 
						parseInt(pivoteCliente['ultimo_mespago']) !=
						parseInt(json[iIdx]['ultimo_mespago'])){
						boDif = true;
						iIdx = json.length;
					}//fin:if
				}//fin:for(iIdx = 0; iIdx < ; iIdx++)								
				if(boDif){
					showGeneralMessage("NO es posible agregar la mensualidad.<br/>Existen clientes con adeudos pendientes.", "warning", true);
					return;
				}//fin:if
			}//fin:if(json.length > 1)
					
			if(json.length > 0){
				$.each(json, function(key, value){
					ftMensualidad = fnEvaluarCliente(value);					
					
					//Evaluar si es posible agregar el año-mes a la interfaz
					//[agregar:false, error: message]
					var arrarMesAdd = fnEvaluarMesMasivo(
						value.id_cliente, 
						ui_annio, 
						ui_mes, 
						parseInt(value.ultimo_aniopago), 
						parseInt(value.ultimo_mespago));
						
					if(arrarMesAdd['agregar'] == 'true'){						
						RSocialDT.row.add({
							"anio_mes" : annio_mes,
							"cliente" : value.nombres,
							"mensualidad" : ftMensualidad,
							"descuento" : "0.00",
							"total" : ftMensualidad,
							"annio" : $("#select-masivo-anio").val(),
							"imes" :  $("#select-masivo-mes").val(),
							"idcliente" : value.id_cliente,							
							"monto_mes" : ftMensualidad,
							"ultaniopago": value.ultimo_aniopago,
							"ultmespago": value.ultimo_mespago,
							"idrazonsocial": idrsocial
						}).draw();
						boEvalua = true;
					}//fin:if
					else{
						boEvalua = false;
						//showGeneralMessage(arrarMesAdd['error'], "warning", true);						
						arrayErroresMes.push(arrarMesAdd['error']);
					}//fin:else					
					//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
					//Bloque para validar si el cliente cubre la anualidad para descontar el ultimo mes
					if(boEvalua){
						var arrayAnual = new Array();
						var arrayMesesDT = new Array();						
						var annioActual  = (new Date()).getFullYear();
						var aux_anniomes = "";
						var bExistDic = false;
						var icountMeses = 0;
						var idclienteEval = parseInt(value.id_cliente);

						icountMeses = fngetMesesAddByCliente(idclienteEval);
												
						//Refrescar el contenido del array de datos
						var dtsRsocial = RSocialDT.rows().data().toArray().slice();												
						if(dtsRsocial.length > 0 && icountMeses >= MesConDescto){
							//Generamos el arreglo con la totalidad de mensualidades				
							for(iterate = 1; iterate <= MesConDescto; iterate++){								
								aux_anniomes = annioActual.toString() + "-" + iterate.toString();								
								arrayAnual.push(aux_anniomes);
							}//fin:for(iterate = 1; iterate <= 12; iterate++)

							//Identificar la Mensualidad numero 12(Diciembre)
							for(idx = 0; idx < dtsRsocial.length; idx++){
								//Coincidir annio - mes actual para efectuar el descuento
								if(parseInt(dtsRsocial[idx]['annio']) == annioActual &&
								   //parseInt(dtsRsocial[idx]['imes']) == 12 &&
								   parseInt(dtsRsocial[idx]['imes']) == 2 &&
								   idclienteEval == parseInt(dtsRsocial[idx]['idcliente'])){
									bExistDic = true;
									idx = dtsRsocial.length;
								}//fin:
							}//fin:for

							//actualizamos el renglon con mensualidad = descto
							if(bExistDic){								
								//Clonar la combinacion annio-mes en un arreglo auxiliar para comparaciones
								for(idz =0; idz < dtsRsocial.length;idz++){
									if(idclienteEval == parseInt(dtsRsocial[idz]['idcliente'])){
										arrayMesesDT.push(dtsRsocial[idz]['annio'] + "-" + dtsRsocial[idz]['imes']);
									}//fin:if
								}//fin:for

								//Validar que existan la Totalidades de AÃ±o-Mes actual para considerar el pago de la anualidad
								icountMeses = 0;
								for(idy = 0; idy < arrayAnual.length; idy++){
									if(arrayMesesDT.indexOf(arrayAnual[idy]) > -1){
										icountMeses++;
									}//fin:if
								}//fin:for
								if(icountMeses == arrayAnual.length){						
									//*********************************************************************************
									//aplicacion del Descuento por cubrir los 12 meses					
									var dataRows = RSocialDT.rows().data();

									//Buscar el registro para el año-mes de diciembre y actualizar su valor
									for(idx = 0; idx < dataRows.length; idx++){
										if(parseInt(dataRows[idx]['annio']) == annioActual &&
										   parseInt(dataRows[idx]['imes']) == MesConDescto && 
										   parseInt(dataRows[idx]['idcliente']) == idclienteEval){
											//Descuento = Total
											RSocialDT.cell(idx, 3).data(dataRows[idx]['total']).draw();
											RSocialDT.cell(idx, 4).data(0).draw();
											idx = dataRows.length;
										}//fin:if
									}//fin:for				
									//*********************************************************************************								
									showGeneralMessage("Al cubrir la anualidad(12 meses) obtuvo el descuento del ultimo mes(diciembre).", "success", true);
								}//fin:if(icountMeses == arrayAnual.length)
							}//fin:if
						}//fin:if(dataRows.length > 0 && dataRows.length >= 12)
						//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@		
					}//fin:if(boEvalua)										
				});//fin:$.each(json, function(key, value)	
				setTimeout(function () {RSocialDT.columns.adjust().draw();},500);					
			}//fin:if(json.length > 0)
			//Desplegar notificaciones en caso de errores											
			if(arrayErroresMes.length > 0){
				var msgError = "";
				for(idz = 0; idz < arrayErroresMes.length; idz++){
					msgError = msgError + arrayErroresMes[idz] + "<br/>";			
				}//fin:for
				showGeneralMessage(msgError, "warning", true);
			}//fin:if									
		},
		complete: function () {$("#loading-rsocial").html("");}
	});//fin:$.ajax		
}//fin:fnaddMesMasivo
//------------------------------------------------------------------------------
//Metodo para evuluar la condiciona del cliente y decidir que monto debe cobrarse
function fnEvaluarCliente(arraydata){
	
	var bfisica = (arraydata['fisica']).toLowerCase() == 'true' ? true : false;
	var tipofacturacion = (arraydata['tipo_facturacion']).toString().toLowerCase();
	var idfacturacion_tamboreo = 0;
	var monto_cobrar = 0.00;
	
	//Si es persona fisica tomar el monto de la Colonia	
	if(bfisica){monto_cobrar = parseFloat(arraydata['monto']);}
	//Si es persona moral, considerar la columna tipo facturacion para decidir el monto
	else{			
		switch(tipofacturacion){
			case 'fija':{ monto_cobrar = parseFloat(arraydata['monto_tasa_fija']);} break;
			case 'tamboreo':{
				idfacturacion_tamboreo = parseInt(arraydata['idfacturacion_tamboreo']);
				monto_cobrar = parseFloat(arraydata['monto_iva']);
				/*
				switch(){
					case 1: //Tambormonto_cobrar = ; break;
					case 2: //Bolsa break;
				}//fin:switch
				*/
			}//fin:case
			break;						
		}//fin:switch		
	}//fin:else	
	return monto_cobrar;
}//fin:function
//------------------------------------------------------------------------------
function fnguardarMasivo(){
	
	//Validar existan renglones para el guardado
	var datajson = new Array();
	var idrazonsocial = 0;			
	var dtsMesesDT = RSocialDT.rows().data().toArray().slice();

	for(var iIdx = 0; iIdx < dtsMesesDT.length; iIdx++){
		datajson.push({
			idcliente: dtsMesesDT[iIdx]['idcliente'],		
			mes: parseInt(dtsMesesDT[iIdx]['imes']) ,
			anio: dtsMesesDT[iIdx]['annio'],
			descuento: parseFloat(dtsMesesDT[iIdx]['descuento']),
			iva: 0.00,
			cantidad: parseFloat(dtsMesesDT[iIdx]['total']),
			fecha_pago: "",
			idforma_pago: $("#select-masivo-fpago").val(),
			referencia_bancaria: $("#edt-masivo-referencia").val() != "" ? $("#edt-masivo-referencia").val() : null,
			validado: true,
			facturado: $("#chkmasFactura").is(":checked"),
			folio_factura: $("#edt-masivo-folfac").val() != "" ? $("#edt-masivo-folfac").val() : null,
			idcobratario: 0,
			caja: false,
			idbanco: 0,
			activo: true,
			envio_correo:false,
			recibo_impreso:false,
			incluido_corte: false,
			fecha_corte:"",
			idvisita_cliente: "",
			idtipo_cobro:1,
			idtipo_servicio: 0,
			concepto_servicio : "",
			idrazon_social : dtsMesesDT[iIdx]['idrazonsocial']
		});//fin:datajson.push
		idrazonsocial = dtsMesesDT[iIdx]['idrazonsocial'];
	}//fin:for
	
	//Invocar ajax para realizar el guardado
	$.ajax({
		url: '/caja/savePagoMasivo',
		type: 'POST',
		dataType: "json",
		data: JSON.stringify(datajson),
		beforeSend: function(){			
			$('#loading-rsocial').html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Guardando...</div>');		
		},
        success: function(json){				
			if(Boolean(json["success"])){				
				//boSuccess = true;
				$("#btn-masivo-cobrar").prop('disabled', true);
				$("#btn-masivo-add").prop('disabled', true);
				
				//Actualizar el idpago para la impresion del recibo-cliente
				$("#hdd-idpagomasivo").val(parseInt(json["idpagomasivo"]));
				$("#hdd-idrazonsocial").val(idrazonsocial);
											
				//Habilitar el boton para la impresion del Recibo de Cobro
				$("#btn-rsocial-recibo").prop('disabled', false);						
				
				showGeneralMessage("Registro(s) guardado(s) correctamente.", "success", true);				
			}//fin:if		
		},
		complete: function () {$('#loading-rsocial').html("");}
	});//fin:$.ajax 	
}//fin:fnCobrarMasivo
//------------------------------------------------------------------------------
function fngetUltimoPago_Masivo(array_data, pidcliente, u_annio, u_mes){

	var arrayRowsMax = new Array();
	var arrayValues  = new Array();
	var max_annio = u_annio;
	var iIndice = 0;

	//Obtener el maximo valor para el año
	jQuery.map(array_data, function (obj) {
		if (parseInt(obj['annio']) > max_annio && 
			pidcliente == parseInt(obj['idcliente'])){
			max_annio = parseInt(obj['annio']);
		}//fin:if
	});
	
	//Si datable esta vacio consideramos al Ultimo pago como pivote
	if(u_annio == max_annio){
		arrayRowsMax.push({annio: max_annio, mes: u_mes});
	}//fin:

	//Obtener todos los renglones con el año maximo filtrando por cliente
	for (var idx = 0; idx < array_data.length; idx++){
		if(parseInt(array_data[idx]['annio']) == max_annio && 
			pidcliente == parseInt(array_data[idx]['idcliente'])){
			arrayRowsMax.push({annio: max_annio, mes: parseInt(array_data[idx]['imes'])});
		}//fin:
	}//fin:for

	//Ordenamos descendente para obtener la mensualidad mas Reciente en Interfaz
	arrayRowsMax.sort(function (a, b){return (b.mes - a.mes)});
	if(arrayRowsMax.length > 0){
		arrayValues["annio"] = arrayRowsMax[0].annio;
		arrayValues["mes"] = arrayRowsMax[0].mes;
	}//fin:
	arrayRowsMax = null;
	return arrayValues;
}//fin:fnEvaluarUltimoPago
//------------------------------------------------------------------------------
function fnEvaluarMesMasivo(p_idcliente, p_annioadd, p_mesadd,p_ultannio, p_ultmes){
	
	//Variable de Interfaz
	var annio_mes = $("#select-masivo-mes :selected").text() + " " + $("#select-masivo-anio :selected").val();

	var arrayEval = new Array();
	var message = "";	
	var dtsRsocial = RSocialDT.rows().data().toArray().slice();
	var arrayUltPago = null;	
	var dtFechaAdd = new Date(p_annioadd, p_mesadd - 1, 1);
	var dtFecAux = new Date(p_annioadd, p_mesadd - 2, 1);
	var bExisteRow = false;	
	
	arrayEval['agregar'] = "";
	arrayEval['error'] = "";
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	//Si el dataset ya tiene renglones impedir agregar de nueva la combinacion año-mes
	if(dtsRsocial.length > 0){
		for(idx = 0; idx < dtsRsocial.length; idx++){		
			if(parseInt(dtsRsocial[idx]['annio']) == p_annioadd && 
			   parseInt(dtsRsocial[idx]['imes'])  == p_mesadd && 
			   parseInt(dtsRsocial[idx]['idcliente']) == p_idcliente){
				bExisteRow = true;
				idx = dtsRsocial.length;
			}//fin:if
		}//fin:for
		if(bExisteRow){		
			message = "Ya fue agregado el renglon:" + annio_mes;
			//arrayEval.push({agregar:'false', error: message});
			arrayEval['agregar'] = "false";
			arrayEval['error'] = message;
			return arrayEval;
		}//fin:if
	}//fin:if(dtsRsocial.length > 0)
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	if(p_ultannio == 0 && p_ultmes == 0){
		if(dtsRsocial.length > 0){
			//Evaluar para obtener el Ultimo pago consideran Interfaz y renglones previamente capturados		
			arrayUltPago = fngetUltimoPago_Masivo(dtsRsocial, p_idcliente, 0, 0);
	
			//Comparamos si el Ultimo mes de pago ingresado corresponde a diciembre
			if(parseInt(arrayUltPago["mes"]) == 12){
				dtFecAux.setFullYear(parseInt(arrayUltPago["annio"]) + 1);
				dtFecAux.setMonth(0);
			}//fin:if
			else{
				dtFecAux.setFullYear(parseInt(arrayUltPago["annio"]));
				dtFecAux.setMonth(parseInt(arrayUltPago["mes"]));
			}//fin:else

			//validar si la fechas corresponden para agregar el renglon
			if(dtFecAux.getTime() == dtFechaAdd.getTime()){			
				//arrayEval.push({agregar:'true', error: ''});
				arrayEval['agregar'] = "true";
				arrayEval['error'] = "";
				return arrayEval;
			}//fin:
			else{							
				message = "ID:" + p_idcliente.toString()  + " - Tiene Mensualidades pendientes por pagar.";				
				//arrayEval.push({agregar:'false', error: message});
				arrayEval['agregar'] = "false";
				arrayEval['error'] = message;
				return arrayEval;
			}//fin:else
		}//fin:if(dtsRsocial.length > 0)
		else{
			//arrayEval.push({agregar:'true', error: ''});
			arrayEval['agregar'] = "true";
			arrayEval['error'] = "";
			return arrayEval;
		}//fin:else
	}//fin:if(vannio_ultimo == 0 && vmes_ultimo == 0)
	else{
		//Evaluar para obtener el Ultimo pago consideran Interfaz y renglones previamente capturados
		arrayUltPago = fngetUltimoPago_Masivo(dtsRsocial, p_idcliente, p_ultannio, p_ultmes);

		//comparamos que el Ultimo mes de pago este capturado antes de agregar la nueva mensualidad
		if(parseInt(arrayUltPago["mes"]) == 12){
			dtFecAux.setFullYear(parseInt(arrayUltPago["annio"]) +1);
			dtFecAux.setMonth(0);
		}//fin:if
		else{
			dtFecAux.setFullYear(parseInt(arrayUltPago["annio"]));
			dtFecAux.setMonth(parseInt(arrayUltPago["mes"]));
		}//fin:else

		//validar si la fechas corresponde para agregar el renglon
		if(dtFecAux.getTime() == dtFechaAdd.getTime()){		
			//arrayEval.push({agregar:'true', error: ''});
			arrayEval['agregar'] = "true";
			arrayEval['error'] = '';
			return arrayEval;
		}//fin:if(dtFecAux.getTime() == dtFechaAdd.getTime())
		else{
			//message = p_idcliente.toString()  + ":No es posible agregar el renglon. <br/>Tiene adeudos anteriores <br/>o la mensualidad ya fue cubierta";			
			message = "ID:" + p_idcliente.toString()  + " - Tiene adeudos anteriores o la mensualidad ya fue cubierta.";
			//arrayEval.push({agregar:'false', error: message});
			arrayEval['agregar'] = "false";
			arrayEval['error'] = message;
			return arrayEval;
		}//fin:else
	}//fin else :if(p_ultannio == 0 && p_ultmes == 0)
}//fin:fnEvaluarMesMasivo
//-----------------------------------------------------------------------------
//Funcion para contabilizar si el cliente ya tien capturado los 12 meses necesarios para
//aplicar el descuento por anualidad, retorna el total de Meses capturados
function fngetMesesAddByCliente(pidcliente){

	var iCountMeses = 0;
	var arrayClone = RSocialDT.rows().data().toArray().slice();

	//Obtener todos los renglones con el año maximo filtrando por cliente
	for (var idx = 0; idx < arrayClone.length; idx++){
		if(pidcliente == parseInt(arrayClone[idx]['idcliente'])){iCountMeses++;}
	}//fin:for
	return iCountMeses;
}//fin:fngetMesesAddByCliente
//-----------------------------------------------------------------------------
function fnCobrarPagoMasivo(){
		
	//Validaciones generales
	if(RSocialDT.rows().data().length <= 0){
		showGeneralMessage("Es necesario agregar por lo menos un renglon para continuar.", "warning", true);
		return;
	}//fin:	
	
	//Validaciones
	//Razon social este seleccionado y no permita ingresar otra razon cuando ya tiene capturado
	if($("#chkmasFactura").is(":checked")){
		var txtFolfac =  $("#edt-masivo-folfac").val().trim();
		if(txtFolfac == null || txtFolfac == ""){
			showGeneralMessage("* Es necesario ingresar el folio de factura.", "warning", true);			
			return ;
		}//fin:
	}//fin:if	
	
	var v_referencia = $("#edt-masivo-referencia").val().trim();
	var mreferencia = $("#select-masivo-fpago").find("option:selected").attr("data-referencia");	
	if(mreferencia == "si"){
		if(v_referencia == null || v_referencia == ""){
			showGeneralMessage("* Es necesario ingresar la Referencia para la forma de Pago", "warning", true);				
			return;
		}//fin:if		
	}//fin:if	
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	//Verificar si habilitado la aplicacion del descuento global
	if($("#chkDesctoMasivo").is(":checked")){

		//Iniciar los controles del Modal para Validar Contraseña y Password
		$('#aviso-masivo-permiso').html("");
		$("#edtMasivoUserPer").val("");
		$("#edtMasivoPassPer").val("");
		$("#edtMasivoMontoPer").val("");
				
		$("#modalPerDesctoMasivo").modal({backdrop: 'static', keyboard: false, show: true});
	}//fin:if($("#chkDescuento").is(":checked"))
	//realizar el guardado normal de los registros
	else{ fnguardarMasivo(); }
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@	
}//fin:fnCobrarPagoMasivo
//-----------------------------------------------------------------------------
//[20170113]
function fnCalcularDesctoMasivo(pdescto){

	var dtFechaNow  = new Date();
	var annioActual = dtFechaNow.getFullYear();
	var mesActual   = dtFechaNow.getMonth() + 1;
	
	var mesIterate  = 1;
	var icountMeses = 0;
	var IndiceSearch = -1;

	var dtfechaIni   = new Date(1900, 0, 1);
	var dtfechaFin   = new Date(1900, 0, 1);
	var dtUltimoPago = new Date(1900, 0, 1);

	var arrayMesesAux = null;
	var arrayMesesClone = null;
	
	var arrayClientes = new Array();
	var dtsMesesDT = RSocialDT.rows().data().toArray().slice();
	
	//Iteracion para obtener la totalidad de Clientes diferentes
	var iidCliente = -1;
	var bofound = false;
	
	//Obtener el maximo año-mes capturado en la Interfaz
	var maxAnnio = 0;
	var maxMes = 0;
	var arrayAnnioMes = fngetMaxAnnioMes_UI(dtsMesesDT);
	
	//if(arrayAnnioMes.length > 0){
	if(arrayAnnioMes != null && Object.keys(arrayAnnioMes).length > 0){
		maxAnnio = arrayAnnioMes['annio'];
		maxMes   = arrayAnnioMes['mes']; 
	}//fin:if(arrayAnnioMes.length > 0)
	else{maxAnnio = annioActual; maxMes   = mesActual;}

	for(itera = 0; itera < dtsMesesDT.length; itera++){
		bofound = false;
		if(arrayClientes.length > 0){
			for(indice = 0; indice < arrayClientes.length; indice++){
				if(parseInt(arrayClientes[indice]['idcliente']) == 
				   parseInt(dtsMesesDT[itera]['idcliente'])){
					bofound = true;
					indice = arrayClientes.length;
				}//fin:if				
			}//fin:for
			if(!bofound){
					arrayClientes.push({
					idcliente: dtsMesesDT[itera]['idcliente'],		
					ultaniopago:parseInt(dtsMesesDT[itera]['ultaniopago']), 
					ultmespago:parseInt(dtsMesesDT[itera]['ultmespago']),
					mensualidad: dtsMesesDT[itera]['monto_mes'],
					annio: maxAnnio,
					mes:maxMes
				});							
			}//fin:if
		}//fin:if
		else{
			arrayClientes.push({
				idcliente: dtsMesesDT[itera]['idcliente'],		
				ultaniopago:parseInt(dtsMesesDT[itera]['ultaniopago']), 
				ultmespago:parseInt(dtsMesesDT[itera]['ultmespago']),
				mensualidad: dtsMesesDT[itera]['monto_mes'],
				annio: maxAnnio,
				mes:maxMes				
			});			
		}//fin:else		
	}//fin:for(itera = 0; itera < dtsMesesDT.length; itera++)
	
	//Realizar la evaluacion para cada cliente
	for(iIdx = 0; iIdx  < arrayClientes.length; iIdx++){
		
		//Calcular la totalidad de meses de aduedos basados en el ultimo año-mes
		var ultaniopago_cte = parseInt(arrayClientes[iIdx]['ultaniopago']);  
		var ultmespago_cte = parseInt(arrayClientes[iIdx]['ultmespago']);
		var max_annio = parseInt(arrayClientes[iIdx]['annio']);
		var max_mes = parseInt(arrayClientes[iIdx]['mes']);
		
		iidCliente = parseInt(arrayClientes[iIdx]['idcliente']);
		
		arrayMesesAux = new Array();
		arrayMesesClone = new Array(); 
		
		dtfechaIni.setFullYear(ultaniopago_cte);
		dtfechaIni.setMonth(parseInt(ultmespago_cte - 1));
		
		//dtfechaFin.setFullYear(annioActual);
		//dtfechaFin.setMonth(mesActual - 1);
		dtfechaFin.setFullYear(max_annio);
		dtfechaFin.setMonth(max_mes - 1);
		
		mesIterate = ultmespago_cte;
		
		dtUltimoPago.setFullYear(ultaniopago_cte);
		dtUltimoPago.setMonth(ultmespago_cte - 1);
		
		for(iIdy = ultaniopago_cte; iIdy <= annioActual; iIdy++){
			if(mesIterate > 12){mesIterate = 1}
			for(iIdz = mesIterate; iIdz <= 12; iIdz++){
				mesIterate++;
				dtfechaIni.setFullYear(iIdy);
				dtfechaIni.setMonth(iIdz - 1);
				
				//console.log('dtfechaIni: ' + dtfechaIni.toISOString().substring(0, 10));
				//console.log('dtfechaFin: ' + dtfechaFin.toISOString().substring(0, 10));
				//console.log('dtUltimoPago: ' + dtUltimoPago.toISOString().substring(0, 10));
				
				if(dtUltimoPago.getTime() < dtfechaIni.getTime() &&
					dtfechaIni.getTime() <= dtfechaFin.getTime()){				
					arrayMesesAux.push(iIdy.toString() + "-" + iIdz.toString());
				}//fin:		
			}//fin:for
		}//fin:for
		
		//Generar un array con la combinacion idcliente-año-mes
		for(iterax = 0; iterax < dtsMesesDT.length; iterax++){
			if(parseInt(dtsMesesDT[iterax]['idcliente']) == iidCliente){
				arrayMesesClone.push(dtsMesesDT[iterax]['annio'] + '-' + dtsMesesDT[iterax]['imes']);
			}//fin:if
		}//fin:for(iterax = 0; iterax < dtsMesesDT.length; iterax++)
					
		//Iterar el array de meses de adeudos para comprobar que ya fueron capturados
		IndiceSearch = -1;
		icountMeses = 0;
		for(iteray = 0; iteray < arrayMesesAux.length; iteray++){
			IndiceSearch = arrayMesesClone.indexOf(arrayMesesAux[iteray]);
			if(IndiceSearch > -1){icountMeses++;}
		}//fin:for
		
		var ftDestoXmes = 0.00;
		var keyAnnioMes = "";
		var ftTotalPagar = 0.00;	
		var ftMontoMes = parseFloat(arrayClientes[iIdx]['mensualidad']);

		//Comprobar si todas las mensualidades(año-mes) apartir del ultimo pago
		//estan capturados en el datable para el cliente evalaudo
		if(icountMeses > 0 && icountMeses == arrayMesesAux.length){
			//Realizar calculos para la asignacion de la parte proporcional del descuento
			ftDestoXmes = (pdescto/icountMeses);
			ftDestoXmes = ftDestoXmes.toFixed(2);

			//Actualizar la columna descuento para cada renglon donde cliente evaluado			
			var dtsRowsDT = RSocialDT.rows().data();
			for(idxrow = 0; idxrow < dtsRowsDT.length; idxrow++){
				if(parseInt(dtsRowsDT[idxrow]['idcliente']) == iidCliente){
					ftTotalPagar = ftMontoMes - ftDestoXmes;
					ftTotalPagar = ftTotalPagar.toFixed(2);					
					RSocialDT.cell(idxrow, 3).data(ftDestoXmes).draw();
					RSocialDT.cell(idxrow, 4).data(ftTotalPagar).draw();	
				}//fin:if
			}//fin:for		
		}//fin:if(icountMeses == arrayMesesAux.length)
		//Existen diferencias entre la totalidad de meses adeudados y aplicacion del descuento
		else{}//fin:				
	}//fin:for(iIdx = 0; iIdx  < arrayClientes.length; iIdx++)			
}//fin:aplicarDesctoMontoMasivo		
//-----------------------------------------------------------------------------
function fngetMaxAnnioMes_UI(arrayRows){

	var arrayRowsMax = new Array();
	var arrayValues  = new Array();
	var max_annio = 0;
	
	//Obtener el maximo valor para el año
	jQuery.map(arrayRows, function (obj){
		if (parseInt(obj['annio']) > max_annio){
			max_annio = parseInt(obj['annio']);
		}//fin:if
	});//fin:jQuery.map
	
	//Si datable esta vacio consideramos al Ultimo pago como pivote
	if(max_annio == 0){
		arrayRowsMax.push({
			annio: (new Date()).getFullYear(), 
			mes: ((new Date()).getMonth() + 1) 
		});
	}//fin:
	
	//Obtener todos los renglones con el año maximo filtrando por cliente
	for(idx = 0; idx < arrayRows.length; idx++){
		if(parseInt(arrayRows[idx]['annio']) == max_annio){
			arrayRowsMax.push({annio: max_annio, mes: parseInt(arrayRows[idx]['imes'])});
		}//fin:
	}//fin:for

	//Ordenamos descendente para obtener la mensualidad mas Reciente en Interfaz
	arrayRowsMax.sort(function (a, b){return (b.mes - a.mes)});
	if(arrayRowsMax.length > 0){
		arrayValues["annio"] = arrayRowsMax[0].annio;
		arrayValues["mes"] = arrayRowsMax[0].mes;
	}//fin:
	else{
		arrayValues["annio"] = (new Date()).getFullYear();
		arrayValues["mes"] = ((new Date()).getMonth() + 1);
	}//fin:else
	arrayRowsMax = null;
	return arrayValues;
}//fin:fngetMaxAnnioMes_UI
//-----------------------------------------------------------------------------
function fnvalidarDesctoMasivo(){

	var UsuarioVal  = $("#edtMasivoUserPer").val();
	var PasswordVal = $("#edtMasivoPassPer").val();
	var MontoDescto = $("#edtMasivoMontoPer").val();
	var v_error = false;
	var v_message = "";
	var htmlContent = "";
	
	UsuarioVal  = UsuarioVal.trim();
	PasswordVal = PasswordVal.trim();
	MontoDescto = MontoDescto.trim();

	//Validaciones
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	if(UsuarioVal == null || UsuarioVal == ""){
		v_message = "* Es necesario ingresar el Usuario.<br/>";
		v_error = true;
	}//fin:

	if(PasswordVal == null || PasswordVal == ""){
		v_message += "* Es necesario ingresar la Contraseñaa.<br/>";
		v_error = true;
	}//fin:

	if(MontoDescto == null || MontoDescto == ""){
		v_message += "* Es necesario ingresar el Monto a descontar.<br/>";
		v_error = true;
	}//fin:if
	else{
		if(isNaN(parseFloat(MontoDescto))){
			v_message += "* Ingrese un Monto valido.<br/>";
			v_error = true;
		}//fin:
	}//fin:else

	if(v_error){
		htmlContent ="<div class='alert alert-warning alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
		htmlContent += v_message + "</div>";
		$('#descto-message').html(htmlContent);
		return;
	}//fin:
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//Validacion del Permiso
	//To do: crear el permiso respectivo
	var dataResquest = { 
		usuario: UsuarioVal, 
		password: PasswordVal, 
		recurso:'caja', 
		accion:'descuento_rsocial'
	}
	
	//Variable para decifir el minimo de meses de aduedo hay que considerar
	var MinMesAduedo = 5;	
	
	MontoDescto = parseFloat(MontoDescto);
	$('#aviso-masivo-permiso').html('');
	
	//Sumarizar el Total pagado para evitar que el Descuento exceda las mensualidades.
	var TotalPagosXCliente = 0.00;
	var dtsRowsDT = RSocialDT.rows().data().toArray().slice();
	var rowMesPago = dtsRowsDT[0];
	var idclientepivote = parseInt(rowMesPago['idcliente']);
	for(indx = 0; indx < dtsRowsDT.length; indx++){
		if(parseInt(dtsRowsDT[indx]['idcliente']) == idclientepivote){
			TotalPagosXCliente = TotalPagosXCliente + parseFloat(dtsRowsDT[indx]['monto_mes']);
		}//fin:if					
	}//fin:for(indx = 0; indx < dtsRows.length; indx++)			
	
	if(MontoDescto > TotalPagosXCliente){
		showGeneralMessage("No es posible aplicar un descuento mayor al total de Meses.", "warning", true);
		return;
	}//fin:else
		
	$.ajax({
		url: '/caja/validardescuento',
		type: 'POST',
		dataType: "json",
		data: JSON.stringify(dataResquest),
		beforeSend: function(){
			$("#wait-masivo-permiso").html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Validando Permiso...</div>');
		},
		success: function(json){
			if(Boolean(json["permiso"])){								
				$("#modalPerDesctoMasivo .close").click();
				
				//Validacion para el descuento por monto
				//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				//Realizar calculo para verificar si existe un adeudo superior a los 12 meses
				//Tomar el Primer renglon para validar un idcliente
				var dtsRows = RSocialDT.rows().data().toArray().slice();							
				var boaplicar = false;
				
				var vannio_ultimo = parseInt(rowMesPago['ultaniopago']);
				var vmes_ultimo = parseInt(rowMesPago['ultmespago']);

				var annio_actual = (new Date()).getFullYear() - 1;
				var now_annio = (new Date()).getFullYear();
				var now_mes = (new Date()).getMonth();
				var mesIterare = 1;

				var dtfechaIni = new Date(1900,0,1);
				var dtfechaFin = new Date(now_annio, now_mes, 1);
				var dtfecUltPago = new Date(vannio_ultimo, (vmes_ultimo -1), 1);
				var arrayMesesAux = new Array();
				var arrayMesesClone = new Array();
				
				if(vannio_ultimo > 0 && vmes_ultimo > 0){					
					//dtfechaIni.setFullYear(vannio_ultimo);
					//dtfechaIni.setMonth(vmes_ultimo - 1);	
					mesIterare = vmes_ultimo;										
					for(iIdy = vannio_ultimo; iIdy <= now_annio; iIdy++){
						if(mesIterare > 12){mesIterare = 1}
						for(iIdx = mesIterare; iIdx <= 12; iIdx++){
							mesIterare++;
							dtfechaIni.setFullYear(iIdy);
							dtfechaIni.setMonth(iIdx - 1);
							
							//console.log('dtfechaIni: ' + dtfechaIni.toISOString().substring(0, 10));
							//console.log('dtfechaFin: ' + dtfechaFin.toISOString().substring(0, 10));
							//console.log('dtUltimoPago: ' + dtfecUltPago.toISOString().substring(0, 10));							
							
							if(dtfecUltPago.getTime() < dtfechaIni.getTime() &&
								dtfechaIni.getTime() <= dtfechaFin.getTime()){			
								arrayMesesAux.push(iIdy.toString() + "-" + iIdx.toString());
							}//fin:					
						}//fin:for
					}//fin:for					
										
					//Generar un array con la combinacion idcliente-año-mes
					for(iterax = 0; iterax < dtsRows.length; iterax++){
						if(parseInt(dtsRows[iterax]['idcliente']) == idclientepivote){
							arrayMesesClone.push(dtsRows[iterax]['annio'] + '-' + dtsRows[iterax]['imes']);
						}//fin:if
					}//fin:for(iterax = 0; iterax < dtsMesesDT.length; iterax++)
														
					//Comparar arreglos para validar si cumple las condiciones para el descuento
					var idxSearch = -1;
					var icountMeses = 0;
					for(iteray = 0; iteray < arrayMesesAux.length; iteray++){
						idxSearch = arrayMesesClone.indexOf(arrayMesesAux[iteray]);
						if(idxSearch > -1){icountMeses++;}
					}//fin:for(iteray = 0; iteray < arrayMesesAux.length; iteray++)
																								
					boaplicar = (icountMeses > 0 && icountMeses >= MinMesAduedo);						
				}//fin:if(vannio_ultimo > 0 && vmes_ultimo)		
				
				if(boaplicar){						
					
					//Aplicar el descuento en Interfaz
					fnCalcularDesctoMasivo(MontoDescto);
					
					//Realizar el guardado en base de datos
					fnguardarMasivo();
				}//fin:if				
				else{
					var msgAviso = "NO es posible aplicar el Descuento.<br/>";
						msgAviso = msgAviso + "Se requiere un adeudo de por lo menos: " + MinMesAduedo + " meses."
					showGeneralMessage(msgAviso, "warning", true);
				}//fin:else
				//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++																		
			}//fin:if
			else{
				htmlContent ="<div class='alert alert-warning alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
				htmlContent += json["message"] + "</div>";
				$('#aviso-masivo-permiso').html(htmlContent);
			}//fin:
		},//fin:success
		complete: function (){ $("#wait-masivo-permiso").html("");}
	});//fin:$.ajax()
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}//fin:fnAplicarDescto
//-----------------------------------------------------------------------------
function fncancelarPagoByTipo(arrayrow, motivo){
	
	//Identificar que tipo de renglon es para proceder con la cancelacion	
	var arrayUltimo;
	var idtipocobro = parseInt(arrayrow['tipocobro']);	
	var iannio = parseInt(arrayrow['annio']);
	var imes = parseInt(arrayrow['imes']);
	var idpago = parseInt(arrayrow['idpago']);
	var idcliente = parseInt(arrayrow['idcliente']);
	var dataResquest = { idpago: idpago, idcliente : idcliente, motivo: motivo}

	$.ajax({
		url: '/caja/cancelarPago',
		type: 'POST',
		dataType: "json",
		data: JSON.stringify(dataResquest),
		beforeSend: function(){
			$('#loading-cancelar').html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Cancelando pago...</div>');
			$("#btnDoCancelMotivo").prop("disabled", true);
			$("#btnDoCancelCancelMotivo").prop("disabled", true);
			$("#btnDoCancelMotivo").html('<i class="fa fa-spinner fa-pulse"></i> Cancelando...');
		},
		success: function(json){
			if(Boolean(json["success"])){
				//actualizamos la columna status despues de la cancelacion
				$("#modalHistorial").modal("hide");
				$("#modalMotivoCancelacion").modal("hide");
				$("#txtMotivoCancelacion").val("");
				$(".clientes-search-do").click();
				showGeneralMessage("¡El pago se canceló correctamente!", "success", true);
			}//fin:if
			else{showGeneralMessage(json["message"], "warning", true);}
		},
		complete: function (){
			$('#loading-cancelar').html('');
			$("#btnDoCancelMotivo").prop("disabled", false);
			$("#btnDoCancelCancelMotivo").prop("disabled", false);
			$("#btnDoCancelMotivo").html('Aceptar');
		}
	});

}//fin:fncancelarPago


function fncancelarPagoByTipoOld(arrayrow){

	//Identificar que tipo de renglon es para proceder con la cancelacion
	var arrayUltimo;
	var idtipocobro = parseInt(arrayrow['tipocobro']);
	var iannio = parseInt(arrayrow['annio']);
	var imes = parseInt(arrayrow['imes']);
	var idHistorial = parseInt(arrayrow['id']);
	var idcliente = parseInt(arrayrow['idcliente']);
	var dataResquest = { idhistorial: idHistorial, idcliente : idcliente}

	switch(idtipocobro){
		//Cobro de servicio de Recolecta
		case 1:
			//Validar que sea el ultimo pago del cliente para servicio de recoleccion de basura
			arrayUltimo = fngetUltimoPagoByTipo(idtipocobro);
			if(arrayUltimo != null && Object.keys(arrayUltimo).length > 0){
				if(iannio == parseInt(arrayUltimo["annio"]) &&
					imes == parseInt(arrayUltimo["mes"])){
					//Validar si el pago pertenece algun corte vigente
					$.ajax({
						url: '/caja/validarCorte',
						type: 'POST',
						dataType: "json",
						data: JSON.stringify(dataResquest),
						beforeSend: function(){
							$('#wait-ajax').html('<div style="text-align: center;">' +
								'<i class="fa fa-spinner fa-pulse fa-2x"></i> Validando...</div>');
						},
						success: function(json){
							if(Boolean(json["cancelar"])){
								//Peticion para realizar la cancelacion del pago
								$.ajax({
									url: '/caja/cancelarPago',
									type: 'POST',
									dataType: "json",
									data: JSON.stringify(dataResquest),
									beforeSend: function(){
										$('#loading-cancelar').html('<div style="text-align: center;">' +
											'<i class="fa fa-spinner fa-pulse fa-2x"></i> Cancelando pago...</div>');
									},
									success: function(json){
										if(Boolean(json["success"])){
											//actualizamos la columna status despues de la cancelacion
											var dtsHistorico = historialDT.rows().data();
											for(idy = 0; idy < dtsHistorico.length; idy++){
												if(parseInt(dtsHistorico[idy]['id']) == idHistorial){
													historialDT.cell(idy, 6).data('0').draw(false);
													idy = dtsHistorico.length;
												}//fin:if
											}//fin:for
											//*****************************************************
											//Sincronizar Año-Mes anterior al mes cancelado
											var ultanniopago = parseInt(json["ultanniopago"]);
											var ultmespago = parseInt(json["ultmespago"]);

											var mesLetra = fnNumberToMes(parseInt(ultmespago));
											var dtsRowClientes = clientesDT.rows().data();
											for(idx = 0; idx < dtsRowClientes.length; idx++){
												if(parseInt(dtsRowClientes[idx][0]) == idcliente){
													clientesDT.cell(idx, 4).data(ultanniopago).draw(false);
													clientesDT.cell(idx, 5).data(mesLetra).draw(false);
													dtsRowClientes[idx][17] = ultmespago;
													idx = dtsRowClientes.length;
												}//fin:if
											}//fin:for
											//clientesDT.draw();
											//*****************************************************
											showGeneralMessage("¡El pago se canceló correctamente!", "success", true);
										}//fin:if
										else{showGeneralMessage("¡No fue posible cancelar el pago!", "warning", true);}
									},
									complete: function (){$('#loading-cancelar').html('');}
								});
							}//fin:if
							else{showGeneralMessage(json["message"], "warning", true);}
						},
						complete: function (){$('#wait-ajax').html('');}
					});
				}//fin:if
				else{
					//Realizar la validacion para identificar si no pertenece a algun corte activo.
					showGeneralMessage("Solo es posible cancelar el ultimo pago realizado.", "warning", true);
				}//fin:
			}//fin:if
			break;

		//Cobro de Servicio
		case 2:
			//Validar si el pago pertenece algun corte vigente
			$.ajax({
				url: '/caja/validarCorte',
				type: 'POST',
				dataType: "json",
				data: JSON.stringify(dataResquest),
				beforeSend: function(){
					$('#wait-ajax').html('<div style="text-align: center;">' +
						'<i class="fa fa-spinner fa-pulse fa-2x"></i> Validando...</div>');
				},
				success: function(json){
					if(Boolean(json["cancelar"])){
						//Peticion para realizar la cancelacion del pago
						$.ajax({
							url: '/caja/cancelarPago',
							type: 'POST',
							dataType: "json",
							data: JSON.stringify(dataResquest),
							beforeSend: function(){
								$('#loading-cancelar').html('<div style="text-align: center;">' +
									'<i class="fa fa-spinner fa-pulse fa-2x"></i> Cancelando pago...</div>');
							},
							success: function(json){
								if(Boolean(json["success"])){
									//actualizamos la columna status despues de la cancelacion
									var dtsHist = historialDT.rows().data();
									for(idy = 0; idy < dtsHist.length; idy++){
										if(parseInt(dtsHist[idy]['id']) == idHistorial){
											//actualizamos la columna de activo
											historialDT.cell(idy, 6).data('0').draw(false);
											idy = dtsHist.length;
										}//fin:if
									}//fin:for
									showGeneralMessage("¡El pago se canceló correctamente!", "success", true);
								}//fin:if
								else{showGeneralMessage("¡No fue posible cancelar el pago!", "warning", true);}
							},
							complete: function (){$('#loading-cancelar').html('');}
						});
					}//fin:if
					else{showGeneralMessage(json["message"], "warning", true);}
				},
				complete: function (){$('#wait-ajax').html('');}
			});
			break;
	}//Fin:switch(idtipocobro)
}//fin:fncancelarPago


//-----------------------------------------------------------------------------
function fnModalPagosRazonSocial(){
	//To Do: Implementar el reinicio de controles antes de desplegar el modal
	
	
	PagosMasivosDT.clear();
	PagosMasivosDT.draw();
	
	$("#edt-rsocial-fechaini").datepicker("setDate" , getFecNow_ddmmyyyy());
	$("#edt-rsocial-fechafin").datepicker("setDate" , getFecNow_ddmmyyyy());
	
	$("#modalPagosMasivos").modal({backdrop: 'static', keyboard: false, show: true});		
}//fin:fnModalPagosRazonSocial
//-----------------------------------------------------------------------------
function fnbuscarPagosRazonSocial(){
	
	//Validacion para seleccion de fechas
	var v_error = false;
	var v_message ="";
	var v_fechaini = $("#edt-rsocial-fechaini").val();
	var v_fechafin = $("#edt-rsocial-fechafin").val();

	//Validacion para la Fecha inicial   
	v_fechaini = v_fechaini.trim();
	v_fechafin = v_fechafin.trim();

	//Validacion de la fecha de inicio
	if(v_fechaini == null || v_fechaini == ""){
		v_message += "* Es necesario ingresar la Fecha Inicial.<br/>";
		v_error = true;
		//alert('* Es necesario ingresar la Fecha.<br/>');
	}//fin:
	else{
		if(!isValidDate_ddmmyyyy(v_fechaini)){
			v_message += "* La fecha debe tener formato dd/mm/yyyy.<br/>";
			v_error = true;
		}//fin:if
	}//fin:

	//Validacion de la fecha Final
	if(v_fechafin == null || v_fechafin == ""){
		v_message += "* Es necesario ingresar la Fecha Final.<br/>";
		v_error = true;               
	}//fin:
	else{
		if(!isValidDate_ddmmyyyy(v_fechafin)){               
			v_message += "* La fecha debe tener formato dd/mm/yyyy.<br/>";
			v_error = true;
		}//fin:if
	}//fin:

	//validar que fechaini <= fechafin
	//dd/mm/yyy
	var arrayfecini = v_fechaini.split("/");
	var arrayfecfin = v_fechafin.split("/");
	var dtfecini = new Date(parseInt(arrayfecini[2]),  (parseInt(arrayfecini[1]) -1), parseInt(arrayfecini[0]));
	var dtfecfin = new Date(parseInt(arrayfecfin[2]),  (parseInt(arrayfecfin[1]) -1), parseInt(arrayfecfin[0]));

	if(dtfecini.getTime() > dtfecfin.getTime()){
	   v_message += "* La fecha inicial no debe ser mayor que la final.<br/>";
	v_error = true;
	}//fin:if

	if(v_error){
		showGeneralMessage(v_message, "warning", true);
		return;
	}//fin:
	//fin validaciones
			
	//Formato fecha: dd/mm/yyyy
	//var auxFecha= $("#edt-rprint-fechaini").val();
	var dd = v_fechaini.substr(0,2);
	var mm = v_fechaini.substr(3,2);
	var yyyy = v_fechaini.substr(6,4);
	
	var feciniIso = yyyy + "-" + mm + "-" + dd;
	
	//auxFecha = $("#edt-rprint-fechafin").val();
	dd = v_fechafin.substr(0,2);
	mm = v_fechafin.substr(3,2);
	yyyy = v_fechafin.substr(6,4);
	
	var fecfinIso = yyyy + "-" + mm + "-" + dd;
	
	var dataPost = {fechaini : feciniIso, fechafin : fecfinIso}

	$.ajax({
		url: '/caja/searchPagosMasivos',
		data: JSON.stringify(dataPost),
		type: 'POST',
		dataType: "json",
		beforeSend: function(){					
			$('#loading-rsocial-pagos').html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Buscando Recibos...</div>');					
		},
		success: function(json){
			if(json.length == 0){
				showGeneralMessage("No se encontraron registros", "warning", true);
				return;
			}//fin:if(json.length == 0)
								
			PagosMasivosDT.clear();
			if(json.length > 0) {
				for(iIdx = 0; iIdx < json.length; iIdx++){					
					PagosMasivosDT.row.add({
						"idpagomasivo": json[iIdx]['idpagomasivo'],
						"razonsocial": json[iIdx]['razon_social'],
						"subtotal": json[iIdx]['subtotal'],
						"descuento":  json[iIdx]['descto'],
						"total":    json[iIdx]['total'],
						"activo":    json[iIdx]['activo'],
						"fechapago":     json[iIdx]['fecpago'],						
						"metodopago": json[iIdx]['metodopago'],
						"referencia": json[iIdx]['referencia'],	
						"factura": json[iIdx]['factura'],
						"idrazonsocial" : json[iIdx]['idrazon_social']
					});
				}//fin:for							
				PagosMasivosDT.draw();
				setTimeout(function () {PagosMasivosDT.columns.adjust().draw();},500);
			}//fin:if(json.length > 0)						
		},
		complete: function () { $("#loading-rsocial-pagos").html("");}
	});		
}//fin:fnbuscarRecibos
//-----------------------------------------------------------------------------
function fncancelarPagoMasivo(){

	if(PagosMasivosDT.rows('.selected').data().length == 0){
		showGeneralMessage("Seleccione un registro", "warning", true);
		return ;
	}//fin:if(clientesDT.rows('.selected').data().length == 0)		
	
	var dtsPagosMasivo = PagosMasivosDT.rows('tr.selected').data().toArray().slice();	

	if((dtsPagosMasivo[0]['activo']) == '0'){
		showGeneralMessage("¡El pago ya se encuentra cancelado!", "warning", true);
		return;
	}//fin:if	
	var idpagomasivo = dtsPagosMasivo[0]['idpagomasivo'];
	//************************************************************
	//Si es un pago reralizado por razon social impedir la cancelacion
	var datapost = {idpagomasivo: parseInt(idpagomasivo)}
	
	$.ajax({        
		url: '/caja/cancelarPagoMasivo', 
		type: 'POST',
		dataType: "json",
		data: JSON.stringify(datapost),
		beforeSend: function(){
			$('#loading-rsocial-pagos').html('<div style="text-align: center;">' + 
			'<i class="fa fa-spinner fa-pulse fa-2x"></i> Cancelando pago...</div>');
		},
		success: function(json){								
			if(Boolean(json["cancelado"])){
				//actualizamos la columna status despues de la cancelacion												
				var dtsRowsPagos = PagosMasivosDT.rows().data();
				for(idy = 0; idy < dtsRowsPagos.length; idy++){												
					if(parseInt(dtsRowsPagos[idy]['idpagomasivo']) == idpagomasivo){
						dtsRowsPagos.cell(idy, 5).data('0').draw(false);
						idy = dtsRowsPagos.length;
					}//fin:if												
				}//fin:for
				//*****************************************************												
				showGeneralMessage("¡El pago se canceló correctamente!", "success", true);
			}//fin:if	
			else{showGeneralMessage(json["message"], "warning", true);}																		
		},
		complete: function (){$('#loading-rsocial-pagos').html('');}		
	});//fin:ajax

	//************************************************************
}//fin:fncancelarPagoMasivo
//-----------------------------------------------------------------------------
function fnprintReciboMasivo(){
	
	//var divToPrint = document.getElementById('idrecibo-rsocial-body');
	var divToPrint = $('#idrecibo-rsocial-body').clone();
		
	var tlb1 =  document.getElementById('tblReciboinforsocial').outerHTML;
	//var tlb2 =  document.getElementById('recibo-rsocial-empresas').outerHTML;
	var tlb2 =  document.getElementById('tbl-rsocial-totales').outerHTML;	
	var idpagomasivo_ = $("#hdd-recibo-idpagomasivo").val();

	var req = null;
	
	//Impresion Recibo de Cobro
	if(isMobile.any()) {			
		req = new XMLHttpRequest();	
		req.open("POST", "/caja/pdfPagoMasivo", true);
		req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		req.responseType = "blob";
		
		req.onload = function (event){
			if (req.readyState == 4 && req.status == 200) {					
				var blob = new Blob([req.response], { type: 'application/pdf' });
				var objUrl = window.URL.createObjectURL(blob);
				window.open(objUrl);
				window.URL.revokeObjectURL(objUrl);
				//var link=document.createElement('a');
				//link.href=window.URL.createObjectURL(blob);
				//link.target = '_blank';
				//link.click();			
			}//fin:				
		};			
		req.send(JSON.stringify({idpagomasivo: idpagomasivo_, tblcliente : tlb1, tbltotales: tlb2 }));		
	}//fin:if
	else{		
		var newWin = window.open('','Impresion Recibo de cobro por Razon Social.');
		newWin.document.open();
		newWin.document.write('<html><head><title>Impresion de Recibo</title>');
		newWin.document.write("<link type='text/css' rel='stylesheet' href='css/pamplona/caja.css'/>");
		newWin.document.write("</head><body onload='window.print()' >");
		newWin.document.write(divToPrint[0].innerHTML + '</body></html>');
		newWin.document.close();
		setTimeout(function(){newWin.close();},15);							
		/*
		//Implementacion para generar pdf evitando el pool de impresion	
		req = new XMLHttpRequest();	
		req.open("POST", "/caja/pdfPagoMasivo", true);
		req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		req.responseType = "blob";
		
		req.onload = function (event){
			if (req.readyState == 4 && req.status == 200) {					
				var blob = new Blob([req.response], { type: 'application/pdf' });
				var objUrl = window.URL.createObjectURL(blob);
				window.open(objUrl);
				window.URL.revokeObjectURL(objUrl);
				//var link=document.createElement('a');
				//link.href=window.URL.createObjectURL(blob);
				//link.target = '_blank';
				//link.click();			
			}//fin:				
		};			
		req.send(JSON.stringify({idpagomasivo:idpagomasivo_, tblcliente : tlb1, tbltotales: tlb2}));
		*/
	}//fin:else		
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@	
}//fin:fnprintReciboMasivo		
//-----------------------------------------------------------------------------
function fnshowModalCorreoMasivo(){
	
	$("#edtEmailTo-rsocial").val($("#hdd-rsocial-email").val());
	
	$("#modalEmailPagoMasivo").modal({backdrop: 'static', keyboard: false, show: true});	
}//fin:fnshowModalCorreo
//-----------------------------------------------------------------------------
function fnsendEmailReciboMasivo(){
			
	var tlb1 =  document.getElementById('tblReciboinforsocial').outerHTML;
	var tlb2 =  document.getElementById('tbl-rsocial-totales').outerHTML;
	
	var emailTo = $("#edtEmailTo-rsocial").val();	
	var idpagomasivo_ = $("#hdd-recibo-idpagomasivo").val();
	var datajson = {correo: emailTo, idpagomasivo: idpagomasivo_, tblcliente : tlb1,tbltotales: tlb2 }
	
	$.ajax({
		url: '/caja/enviarEmailPagoMasivo',
		data: JSON.stringify(datajson),
		type: 'POST',
		dataType: "json",
		beforeSend: function(){						
			$('#loading-rsocial-email').html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Enviando correo...</div>');					
		},
		success: function(json){
			if(Boolean(json["success"])){
				showGeneralMessage("El recibo se envio por correo correctamente.", "success", true);
			}//fin:if
			else{
				showGeneralMessage(json["message"], "warning", true);
			}//fin:else							
		},
		complete: function () {$("#loading-rsocial-email").html('');}
	});		
}//fin:fnsendEmailRecibo
//-----------------------------------------------------------------------------
function fngenerarReciboMasivo(){
	
	$('#recibo-rsocial-empresas > tbody').html("");
		
	$("#recibo-rsocial-folio").html('');
	$("#recibo-rsocial-nombre").html('');
	$("#recibo-rsocial-dir").html('');
	$("#recibo-rsocial-colonia").html('');
	$("#recibo-rsocial-clave").html('');
	$("#recibo-rsocial-fecha").html('');	
	$("#recibo-rsocial-letratotal").html('');
	$("#recibo-rsocial-total").html('');
	
	$("#hdd-recibo-idpagomasivo").val('');
		
	var gran_total = 0.00;
	var cantidaLetras = "";			
	var datajson = {idpagomasivo: $("#hdd-idpagomasivo").val() , idrazonsocial: $("#hdd-idrazonsocial").val() }
	
	$("#hdd-recibo-idpagomasivo").val($("#hdd-idpagomasivo").val());
	
	$.ajax({
		url: '/caja/getinfoReciboMasivo',
		data: JSON.stringify(datajson),
		type: 'POST',
		dataType: "json",
		beforeSend: function(){	
			$('#loading-rsocial').html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Generando Recibo...' +
				'</div>');			
		},
		success: function(json){					
			if(json.length == 0){
				showGeneralMessage("No fue posible generar el Recibo", "warning", true);
				return;
			}//fin:if(json.length == 0)
			
			$("#recibo-rsocial-folio").html($("#hdd-idpagomasivo").val());
			$("#recibo-rsocial-nombre").html(json['razonsocial']);
			$("#recibo-rsocial-dir").html(json['direccion']);
			$("#recibo-rsocial-colonia").html(json['colonia']);
			$("#recibo-rsocial-clave").html(json['idrazonsocial']);
			$("#recibo-rsocial-fecha").html(json['fechapago']);	
			$("#hdd-rsocial-email").val(json['correo']);
								
			cantidaLetras = NumeroALetras(parseFloat(json['total']));
			gran_total = parseFloat(json['total']);
			//gran_total = gran_total.toFixed(2);
			$("#recibo-rsocial-letratotal").html(cantidaLetras);
			$("#recibo-rsocial-total").html(gran_total.toFixed(2));
						
			$.each(json['clientes'], function(key, value){		
				//Iteracion para obtener la totalidad de meses pagados
				var td_empresa = $("<td>", {id:""});
				td_empresa.append(value['nombre']);
				
				var td_total = $("<td>", {id:""});
				td_total.append("$ " + value['total']);
				
				var table_meses = $("<table>",{class: "table no-border table-condensed", style:"margin-bottom: 0px" }) ;
				
				var td_meses = $("<td>", {id:""});	
				$.each(value['meses'], function(key_m, value_m){				
					for(itera = 0; itera< value_m.length; itera++){			
						var mesletra = fnNumberToMes(parseInt(value_m[itera]['imes']));						
						var tblmeses_td_mes = $("<td>", {id:""});
						tblmeses_td_mes.append(mesletra);
						var tblmeses_tr = "<tr>" + tblmeses_td_mes[0].outerHTML + "</tr>";
						table_meses.append(tblmeses_tr);						
					}//fin:for(itera = 0; value_m.length; itera++)
					td_meses.append(table_meses[0].outerHTML);
				});
				var tr_row = "<tr>" + td_empresa[0].outerHTML + td_meses[0].outerHTML + td_total[0].outerHTML + "</tr>";				
				$('#recibo-rsocial-empresas > tbody').append(tr_row);
			});						
			$("#modalReciboMasivo").modal({backdrop: 'static', keyboard: false, show: true});
		},
		complete: function () {$('#loading-rsocial').html("");}
	});	
}//fin:fnPrintReciboMes
//-----------------------------------------------------------------------------
function fnreimprimirPagoMasivo(){
	
	if(PagosMasivosDT.rows('.selected').data().length == 0){
		showGeneralMessage("Seleccione un registro", "warning", true);
		return ;
	}//fin:if	
	
	var dtsPagosMasivo = PagosMasivosDT.rows('tr.selected').data().toArray().slice();	

	var idpagomasivo  = dtsPagosMasivo[0]['idpagomasivo'];
	var idrazonsocial = dtsPagosMasivo[0]['idrazonsocial'];
	
	var gran_total = 0.00;
	var cantidaLetras = "";
			
	var datajson = {idpagomasivo: idpagomasivo , idrazonsocial: idrazonsocial }
	
	$('#recibo-rsocial-empresas > tbody').html("");
		
	$("#recibo-rsocial-folio").html('');
	$("#recibo-rsocial-nombre").html('');
	$("#recibo-rsocial-dir").html('');
	$("#recibo-rsocial-colonia").html('');
	$("#recibo-rsocial-clave").html('');
	$("#recibo-rsocial-fecha").html('');	
	$("#recibo-rsocial-letratotal").html('');
	$("#recibo-rsocial-total").html('');
	
	$("#hdd-recibo-idpagomasivo").val('');
	
			
	$.ajax({
		url: '/caja/getinfoReciboMasivo',
		data: JSON.stringify(datajson),
		type: 'POST',
		dataType: "json",
		beforeSend: function(){	
			$('#loading-rsocial-pagos').html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Generando Recibo...' +
				'</div>');			
		},
		success: function(json){					
			if(json.length == 0){
				showGeneralMessage("No fue posible generar el Recibo", "warning", true);
				return;
			}//fin:if(json.length == 0)
			
			$("#recibo-rsocial-folio").html(idpagomasivo);
			$("#recibo-rsocial-nombre").html(json['razonsocial']);
			$("#recibo-rsocial-dir").html(json['direccion']);
			$("#recibo-rsocial-colonia").html(json['colonia']);
			$("#recibo-rsocial-clave").html(json['idrazonsocial']);
			$("#recibo-rsocial-fecha").html(json['fechapago']);	
			$("#hdd-rsocial-email").val(json['correo']);			
			$("#hdd-recibo-idpagomasivo").val(idpagomasivo);
								
			cantidaLetras = NumeroALetras(parseFloat(json['total']));
			gran_total = parseFloat(json['total']);
			//gran_total = gran_total.toFixed(2);
			$("#recibo-rsocial-letratotal").html(cantidaLetras);
			$("#recibo-rsocial-total").html(gran_total.toFixed(2));
						
			$.each(json['clientes'], function(key, value){					
				/* Original
				//Iteracion para obtener la totalidad de meses pagados
				var td_empresa = $("<td>", {id:""});
				td_empresa.append(value['nombre']);			
				var td_total = $("<td>", {id:""});
				td_total.append("$ " + value['total']);				
				var table_meses = $("<table>",{class: "table no-border", style:"border-width:0;margin-bottom: 0px", width:"100%"});				
				var td_meses = $("<td>", {style:"border: 0px"});	
				$.each(value['meses'], function(key_m, value_m){				
					for(itera = 0; itera< value_m.length; itera++){			
						var mesletra = fnNumberToMes(parseInt(value_m[itera]['imes']));						
						var tblmeses_td_mes = $("<td>", {id:""});
						tblmeses_td_mes.append(mesletra);
						var tblmeses_tr = "<tr>" + tblmeses_td_mes[0].outerHTML + "</tr>";
						table_meses.append(tblmeses_tr);						
					}//fin:for(itera = 0; value_m.length; itera++)
					td_meses.append(table_meses[0].outerHTML);
				});
				var tr_row = "<tr>" + td_empresa[0].outerHTML + td_meses[0].outerHTML + td_total[0].outerHTML + "</tr>";
				$('#recibo-rsocial-empresas > tbody').append(tr_row);				
				*/
				//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
				var td_empresa = $("<td>", {id:"", align:"left"});
				td_empresa.append(value['nombre']);
				
				var td_total = $("<td>", {id:"", align:"right"});
				td_total.append("$ " + value['total']);
				
				var td_meses = $("<td>", {id:"", align:"left"});
				var list_meses = "";
				$.each(value['meses'], function(key_m, value_m){				
					list_meses = "";
					for(itera = 0; itera< value_m.length; itera++){			
						var mesletra = fnNumberToMes(parseInt(value_m[itera]['imes']));						
						if(itera > 0){ list_meses += ", ";}
						list_meses += mesletra;										
						//var tblmeses_td_mes = $("<td>", {id:""});
						//tblmeses_td_mes.append(mesletra);
						//var tblmeses_tr = "<tr>" + tblmeses_td_mes[0].outerHTML + "</tr>";
						//table_meses.append(tblmeses_tr);						
					}//fin:for(itera = 0; value_m.length; itera++)
					td_meses.append(list_meses);
				});				
				var tr_row = "<tr>" + td_empresa[0].outerHTML + td_meses[0].outerHTML + td_total[0].outerHTML + "</tr>";
				$('#recibo-rsocial-empresas > tbody').append(tr_row);		
				//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@			
			});						
			$("#modalReciboMasivo").modal({backdrop: 'static', keyboard: false, show: true});
		},
		complete: function () {$('#loading-rsocial-pagos').html("");}
	});	
		
}//fin:fnreimprimirPagoMasivo
//-----------------------------------------------------------------------------

function fnEditarCliente() {

	var dhhlatcliente = 0;
    var dhhlongcliente = 0;
    var llimpiarCampoDir = false;

 

     var rowData = clientesDT.rows("tr.selected").data();
     var idCliente = rowData[0][0];

	 $.ajax({
        url: "/clientes/get/" + idCliente,
        type: 'GET',
        dataType: "json",
        success: function(json){
            if(!json)
            {
                showGeneralMessage(json.cMensaje, "warning", true);
                return ;
            }
            else {


            	var googleLatAndLong;
			    var mapLatitud;
			    var mapLongitud;

		        mapLatitud = json.latitud;
				mapLongitud = json.longitud;
				googleLatAndLong = new google.maps.LatLng(mapLatitud, mapLongitud);
		        var mapOptions = {zoom: 18, center: googleLatAndLong}
		        map = new google.maps.Map(document.getElementById("map2"), mapOptions);
		        if(marker) marker.setMap(null);
				marker = new google.maps.Marker({position: googleLatAndLong, map: map, draggable:true});
		        marker.setMap(map);


            	$("#idcliente").val(json.id_cliente);

            	$("#apepat").val(json.apepat);
            	$("#apemat").val(json.apemat);
            	$("#nombres").val(json.nombres);

            	$("#calle").val(json.calle);
            	$("#calle_letra").val(json.calle_letra);
            	$("#tipo_calle").val(json.tipo_calle);
            	$("#numero").val(json.numero);
            	$("#numero_letra").val(json.numero_letra);
            	$("#tipo_numero").val(json.tipo_numero);
            	$("#cruza1").val(json.cruza1);
            	$("#letra_cruza1").val(json.letra_cruza1);
            	$("#tipo_cruza1").val(json.tipo_cruza1);
            	$("#cruza2").val(json.cruza2);
            	$("#letra_cruza2").val(json.letra_cruza2);
            	$("#tipo_cruza2").val(json.tipo_cruza2);
            	$("#localidad").val(json.localidad);
            	$("#folio_catastral").val(json.folio_catastral);
            	$("#direccion_otro").val(json.direccion_otro);
            	$("#referencia_ubicacion").val(json.referencia_ubicacion);
                
                $("#idcolonia").val(json.idcolonia);

                $("#chkmarginado").prop("checked",json.marginado);
                $("#chkcomercio").prop("checked",json.comercio).change();
                $("#chkabandonado").prop("checked",json.abandonado);
                $("#chkprivada").prop("checked",json.privada).change();
                $("#nombre_comercio").val(json.nombre_comercio);
                $("#nombre_privada").val(json.nombre_privada);


                $("#modalDatosCliente").modal({backdrop: 'static', keyboard: false, show: true});
            }

        },
        /*error: function(){
        }*/
    });
   
    
}

function fnActualizarDatosCliente(){
	var mapLat = marker.getPosition().lat();
	var mapLong = marker.getPosition().lng();
	

	
	var data = {
		id: $("#idcliente").val(),
		apepat: $("#apepat").val(),
		apemat: $("#apemat").val(),
		nombres: $("#nombres").val(),
		calle: $("#calle").val(),
		calle_letra: $("#calle_letra").val(),
		tipo_calle: $("#tipo_calle").val(),
		numero: $("#numero").val(),
		numero_letra: $("#numero_letra").val(),
		tipo_numero: $("#tipo_numero").val(),
		cruza1: $("#cruza1").val(),
		letra_cruza1: $("#letra_cruza1").val(),
		tipo_cruza1: $("#tipo_cruza1").val(),
		cruza2: $("#cruza2").val(),
		letra_cruza2: $("#letra_cruza2").val(),
		tipo_cruza2: $("#tipo_cruza2").val(),
		localidad: $("#localidad").val(),
		folio_catastral: $("#folio_catastral").val(),
		direccion_otro: $("#direccion_otro").val(),

		idcolonia: $("#idcolonia").val(),

		referencia_ubicacion: $("#referencia_ubicacion").val(),
		latitud: mapLat,
		longitud: mapLong,
        abandonado: $("#chkabandonado").is(":checked"),
        comercio: $("#chkcomercio").is(":checked"),
        marginado: $("#chkmarginado").is(":checked"),
        privada: $("#chkprivada").is(":checked"),
        nombre_privada: $("#nombre_privada").val().trim() ? $("#nombre_privada").val().trim() : null,
        nombre_comercio: $("#nombre_comercio").val().trim() ? $("#nombre_comercio").val().trim() : null,

	}

    if (data.calle == '') {
       showGeneralMessage("La calle es obligatoria", "warning", true);
    } else {
      
      if (data.numero == '') {
         showGeneralMessage("El numero es obligatorio", "warning", true);
      } else {
        
         if (data.idcolonia == null || data.idcolonia == '') {
            showGeneralMessage("Debe seleccionar una colonia", "warning", true);
         } else {

         	if($("#chkprivada").is(":checked") && $("#nombre_privada").val().trim() == ""){
                showGeneralMessage("Debe especificar el nombre de la privada", "warning", true);
                return false;
			}

             if($("#chkcomercio").is(":checked") && $("#nombre_comercio").val().trim() == ""){
                 showGeneralMessage("Debe especificar el nombre del comercio", "warning", true);
                 return false;
             }

         	$.ajax({
			url: '/clientes/savecobro',
			data:  $.param(data),
			type: 'POST',
			success: function(resp){

			        var colonia = $("#idcolonia :selected").text();					
								
					//Actualizar latitud y longitud en datable de clientes
					var dtsClientes = clientesDT.rows().data();
					for(idx = 0; idx < dtsClientes.length; idx++){
						if(parseInt(dtsClientes[idx][0]) == data.id){
							dtsClientes[idx][1] = ''+data.apepat+' '+data.apemat+' '+data.nombres;
							dtsClientes[idx][12] = data.apepat;
							dtsClientes[idx][13] = data.apemat;
							dtsClientes[idx][2] = ''+data.calle+' '+data.calle_letra;
							dtsClientes[idx][3] = ''+data.numero+' '+data.numero_letra;
							dtsClientes[idx][6] = colonia;
							dtsClientes[idx][8] = ''+data.cruza1+data.letra_cruza1;
						    dtsClientes[idx][20] = ''+data.cruza2+data.letra_cruza2;

						    dtsClientes[idx][10] = data.latitud;
						    dtsClientes[idx][11] = data.longitud;

	                        var dir_cliente = dtsClientes[idx][2];
							if(dtsClientes[idx][3] != "" )
								dir_cliente += " No. " + dtsClientes[idx][3];
							
							if(dtsClientes[idx][8] != "")
								dir_cliente += " por " + dtsClientes[idx][8];
							
							if(dtsClientes[idx][20] != "")
								dir_cliente += " y " + dtsClientes[idx][20];
										
							var direccionCte = dir_cliente + " colonia " + dtsClientes[idx][6];

	                        $("#edt-cobro-Nombre").val(dtsClientes[idx][1]);
	                        $("#edt-cobro-direccion").val(direccionCte);


							idx = dtsClientes.length;
						}//fin:if
					}//fin:for
	                
	                $("#modalDatosCliente .close").click();
					clientesDT.draw();
					
					showGeneralMessage("¡Se actualizaron los datos corectamente!", "success", true);					
			},  error: function(xhr, status, error){
	                    var colonia = $("#idcolonia :selected").text();
	                    if(xhr.status == 400){
	                        showGeneralMessage("Método no implementado", "error", true);
	                    }
	                    else if(xhr.status == 401){
	                        //showGeneralMessage('No cuenta con permisos, contacte al administrador', "error", true);
                            $("#sesion-modal").modal({
                                backdrop:"static",
                                keyboard: false
                            });
                            return;
	                    }
	                    else if(xhr.status == 404){
	                        showGeneralMessage("No se encontro el cliente con id: " + data.id, "error", true);
	                    }
	                    else if(xhr.status == 409){
	                        var text = "Se encontró coincidencia en la dirección, calle: " + data.calle;
	                        if(data.calle_letra) {
	                            text += " letra: " + data.calle_letra;
	                        }
	                        text += " numero: " + data.numero;
	                        if(data.numero_letra) {
	                            text += " letra: " + data.numero_letra;
	                        }
	                        text += " colonia: " + colonia;
	                        showGeneralMessage(text, "error", true);
	                    }
	                    else{
	                        showGeneralMessage("Ocurrió un error al guardar los datos", "error", true);
	                    }
	                }

		});

         } 
      }
    }


	
	
}

function geocodeAddress2(resultsMap) {
    
	var address = document.getElementById('cliente-input-gelocation2').value;
    if(address == "") return;
    geocoder.geocode({'address': address}, function(results, status){
        if(status === google.maps.GeocoderStatus.OK){            
			marker = createMarker(map, marker, 
				results[0].geometry.location.lat(), 
				results[0].geometry.location.lng());  			
			
			//map.setZoom(4);
        }//fin:else
		else {
            if(status === google.maps.GeocoderStatus.ZERO_RESULTS){
                showGeneralMessage("No se encontraton resultados", "warning", true);
            }//fin:if
            else{
                showGeneralMessage("Ocurrió un error al realizar la búsqueda", "warning", true);
            }//fin:
        }//fin:else
    });
}//fin:geocodeAddress

//Funcion para invocar el modal para realizar el cobro del servicio de basura.
function fnCobrarClienteDescuento(){
	$("#container-btns-facturado").hide();
	$("#container-btns-cobrocliente").hide();
	$("#container-btns-desceunto").show();
	$('#chkDescuento').prop('disabled', true);

	$("#container-descuento").show();
	$("#container-datos-pago").hide();
	viewTotalFacturas(false);
	$("#container_folio_factura").hide();

	$("#folio_interno").val("");
	$("#folio_interno").prop("disabled", true);
	var idCliente = 0;

	if(clientesDT.rows('.selected').data().length == 0){
		showGeneralMessage("Seleccione un registro", "warning");
		return ;
	}//fin:if(rows('.selected').data().length == 0)

	mensualidadDT.clear();
	mensualidadDT.draw();
	$("#hdd-ultpago-mes").val("");
	$("#hdd-mensualidad").val("");
	$("#hdd-recolecta-ruta").val("");
	$("#hdd-recolecta-direc").val("");
	$("#hdd-recolecta-colonia").val("");
	$("#chkFactCobro").prop("checked", false);

	$("#hdd-cobro-idpago").val("");
	$("#hdd-recibo-email").val("");

	$("#select-cobro-mes").val((new Date()).getMonth() + 1);
	// $("#select-cobro-anio").val((new Date()).getFullYear());

	var rowData = clientesDT.rows("tr.selected").data();
	var	direccionCte = "";
	var dir_cliente = "";
	var imes = "";

	if(rowData[0].length > 0){
		$("#referencia_bancaria").prop("disabled", true);
		$("#folio_factura").prop("disabled", true);
		$("#referencia_bancaria").val("");
		// $("#folio_interno").val("");
		$("#folio_factura").val("");
		$("#select-basura-fpago option:first").prop("selected", true);

		//dir_cliente = rowData[0][2] + " No. " + rowData[0][3] + " por " + rowData[0][6] + " y " + rowData[0][7];
		dir_cliente = rowData[0][2];
		if(rowData[0][3] != "" )
			dir_cliente += " No. " + rowData[0][3];

		if(rowData[0][8] != "")
			dir_cliente += " por " + rowData[0][8];

		if(rowData[0][20] != "")
			dir_cliente += " y " + rowData[0][20];

		direccionCte = dir_cliente + " colonia " + rowData[0][6];

		$("#hdd-recolecta-direc").val(dir_cliente);
		$("#hdd-recolecta-colonia").val(rowData[0][6]);

		$("#edt-cobro-Nombre").val(rowData[0][1]);
		$("#edt-cobro-direccion").val(direccionCte);
		if(parseInt(rowData[0][4]) > 0){
			$("#edt-cobro-ultanio").val(rowData[0][4]);
			// $("#select-cobro-anio").val(rowData[0][4]);
		}//fin:if
		else{$("#edt-cobro-ultanio").val("");}

		//Ultimo_mes_pago
		if(rowData[0][5] != ""){
			$("#edt-cobro-ultmes").val(rowData[0][5]);
			//$("#select-cobro-mes").val(parseInt(rowData[0][17]) + 1);
			if(parseInt(rowData[0][17]) > 11){
				$("#select-cobro-mes").val(1);
				// $("#select-cobro-anio").val((parseInt(rowData[0][4]) + 1));
			}//fin:if
			else
				$("#select-cobro-mes").val(parseInt(rowData[0][17]) + 1);

			$("#hdd-ultpago-mes").val(parseInt(rowData[0][17]));
		}//fin:if
		else{ $("#edt-cobro-ultmes").val("");}
		//$("#edt-cobro-ultmes").val(rowData[0][5]);

		idCliente = parseInt(rowData[0][0]);
		$("#hdd-recolecta-idcte").val(idCliente);
		$("#hdd-mensualidad").val(parseFloat(rowData[0][16]));
		$("#hdd-recolecta-ruta").val(rowData[0][18]);
		$("#hdd-recibo-email").val(rowData[0][19]);
		$("#hdd-cobro_manual").val(rowData[0][21]);
		$("#hdd-monto_cobro").val(parseFloat(rowData[0][22]));

		//Reiniciar controles editables
		//btn-cobro-nota
		$("#btn-recibo-cliente").prop('disabled', true)
		$("#select-cobro-anio").attr('selected','selected');
		$("#select-cobro-mes").attr('selected','selected');
		$("#btn-cobro-add").prop('disabled', false);
		$("#btn-cobro-mes-descuento").prop('disabled', false);
		$('#chkDescuento').prop('checked', true).change();
		$('#aviso-adeudo').html("");
		$("#hdd-servicio-idpago").val("");
		$("#hdd-cobro-idpago").val("");
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		//Inicializar los controles para el modal del Recibo de Cobro
		$("#recibo-nombre").html("");
		$("#recibo-direccion").html("");
		$("#recibo-colonia").html("");
		$("#recibo-ruta").html("");
		$("#recibo-idcliente").html("");
		$("#recibo-concepto").html("");
		$("#recibo-meses").html("");
		$("#recibo-numletras").html("");
		$("#recibo-total").html("");
		$("#recibo-folio").html("");
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		$("#modal-cobro-cliente").modal({backdrop: 'static', keyboard: false, show: true});
		$("#select-basura-fpago").change();
	}//fin:if(rowData[0].length > 0)
}//fin:fnCobrarCliente

function fnCobrarDescuento() {
	//--------------------------------------------------------------------------
	//Validaciones generales
	if (idsSelected.length <= 0) {
		showGeneralMessage("Es necesario seleccionar al menos un mes para continuar.", "warning", true);
		return;
	}//fin:
	if (!$("#chkDescuento").is(":checked")){
		// if ($("#folio_interno").val().trim() == "") {
		// 	showGeneralMessage("Es necesario proporcionar el folio interno para continuar.", "warning", true);
		// 	return;
		// }
	}
	//--------------------------------------------------------------------------
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	//Verificar si habilitado la aplicacion del descuento global
		//Iniciar los controles del Modal para Validar Contraseña y Password
		$('#descto-message').html("");
		$("#edtPerUser").val("");
		$("#edtPerPass").val("");
		$("#edtPerMonto").val("");
		$("#edtMotivo").val("");
		//btn-perdescto

		$("#modalPerDescto").modal({backdrop: 'static', keyboard: false, show: true});
}

function guardarDescuento(idmotivo){
	var latitud = null;
	var longitud = null;
	// if(position){
	//     latitud = position.coords.latitude;
	//     longitud = position.coords.longitude;
	// }

	var boSuccess = false;
	var datajson = new Array();
	var dataRows = mensualidadDT.rows().data();


	// var folioInterno = $("#folio_interno").val().trim().toUpperCase();
	for (var i = 0; i < idsSelected.length; i++){
		var idx = idsSelected[i].id;

		var d = {
			idcliente: $("#hdd-recolecta-idcte").val(),
			//mes:  fnNumberToMes(parseInt(dataRows[idx][5])),
			mes: parseInt(dataRows[idx][6]) ,
			anio: dataRows[idx][5],
			descuento: dataRows[idx][3],
			iva: 0.00,
			cantidad: dataRows[idx][4],
			idcobratario: 0,
			caja: false,
			concepto_servicio : "",
			idmotivo: idmotivo
		};



		datajson.push(d);//fin:datajson.push
	}//fin:for

	var arrayMaxValues;
	var idcliente = parseInt($("#hdd-recolecta-idcte").val());
	var dtsClientes = clientesDT.rows().data();
	var mesLetra = "";



	//Invocar ajax para realizar el guardado
	$.ajax({
		url: '/caja/aplicardescuento',
		type: 'POST',
		dataType: "json",
		data: JSON.stringify(datajson),
		beforeSend: function(){
			$('#load-save-recolecta').html('<div style="text-align: center;">' +
				'<i class="fa fa-spinner fa-pulse fa-2x"></i> Guardando...</div>');
		},
		error: function(jqXHR, textStatus, errorThrown){
			if(jqXHR.status == 409){
				showGeneralMessage(jqXHR.responseText, "error", true);
			}
		},
		success: function(json){
			if(Boolean(json["success"])){
				$("#modalPerDescto .close").click();
				boSuccess = true;
				$("#btn-cobro-mes-descuento").prop('disabled', true);
				$("#btn-cobro-add").prop('disabled', true);

				$.each(mensualidadDT.rows()[0], function (index, value) {
					id = value;
					$($("#rowPago" + value).find('td input[type="checkbox"]')[0]).prop( "disabled", true );
				});

				//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
				//sincronizar el año - mes despues de guardar el historial de pago.
				arrayMaxValues = fngetUltimoPagoMes();
				if(arrayMaxValues != null && Object.keys(arrayMaxValues).length > 0){
					mesLetra = fnNumberToMes(parseInt(arrayMaxValues["mes"]));
					for(idx = 0; idx < dtsClientes.length; idx++){
						if(parseInt(dtsClientes[idx][0]) == idcliente){
							clientesDT.cell(idx, 4).data(arrayMaxValues["annio"]).draw();
							clientesDT.cell(idx, 5).data(mesLetra).draw();
							dtsClientes[idx][17] = parseInt(arrayMaxValues["mes"]);
							idx = dtsClientes.length;
						}//fin:if
					}//fin:for
					clientesDT.draw();
				}//fin:if
				//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
				showGeneralMessage("Registro(s) guardado(s) correctamente.", "success", true);
			}//fin:if
			else{
				showGeneralMessage("Ocurrio un error al guardar la informacion.", "danger", true);
			}
		},
		complete: function () {$('#load-save-recolecta').html("");}
	});
	return boSuccess;
}

function fnVerDetalles() {
	if(historialDT.rows('.selected').data().length == 0){
		showGeneralMessage("Seleccione un registro", "warning", true);
		return ;
	}//fin:if(clientesDT.rows('.selected').data().length == 0)

	var dtsHistorico = historialDT.rows('tr.selected').data().toArray().slice();
	if(dtsHistorico[0].length <= 0){return;}

	var idPago = dtsHistorico[0]['idpago'];
	$("#modalDatosPago").modal("show");
	$.ajax({
		url: "/caja/getDatosPago/" + idPago,
		success: function (json) {
			var json = JSON.parse(json);
			if(json.data.length == 0){
				showGeneralMessage("No se encontraron registros", "warning", true);
				return;
			}//fin:if(json.length == 0)

			datoshistorialDT.clear();
			if(json.data.length > 0) {
				$.each(json.data, function(key, value){
					datoshistorialDT.row.add({
						"anio":value.anio,
						"mes":value.mes,
						"descto":value.descto,
						"total":value.total,
					}).draw();
				});
				setTimeout(function () {datoshistorialDT.columns.adjust().draw();},500);
			}//fin:if(json.length > 0)
		}
	});
}

function fnCobrarClienteFactura(){
	$('#chkDescuento').prop('checked', false).change();
	$('#chkDescuento').prop('disabled', true);
	$("#container-descuento").hide();

	$("#container_folio_factura").show();
	$("#folio_factura").prop("disabled", false);

	viewTotalFacturas(true);

	//botones de guardado
	$("#container-btns-cobrocliente").hide();
	$("#container-btns-desceunto").hide();
	$("#container-btns-facturado").show();
	$("#btn-cobro-mes-facturado").prop("disabled", false);


	$("#container-datos-pago").hide();
	$("#folio_interno").val("");
	$("#folio_interno").prop("disabled", true);
	var idCliente = 0;

	if(clientesDT.rows('.selected').data().length == 0){
		showGeneralMessage("Seleccione un registro", "warning");
		return ;
	}//fin:if(rows('.selected').data().length == 0)

	mensualidadDT.clear();
	mensualidadDT.draw();
	$("#hdd-ultpago-mes").val("");
	$("#hdd-mensualidad").val("");
	$("#hdd-recolecta-ruta").val("");
	$("#hdd-recolecta-direc").val("");
	$("#hdd-recolecta-colonia").val("");
	$("#chkFactCobro").prop("checked", false);

	$("#hdd-cobro-idpago").val("");
	$("#hdd-recibo-email").val("");

	$("#select-cobro-mes").val((new Date()).getMonth() + 1);
	// $("#select-cobro-anio").val((new Date()).getFullYear());

	var rowData = clientesDT.rows("tr.selected").data();
	var	direccionCte = "";
	var dir_cliente = "";
	var imes = "";

	if(rowData[0].length > 0){
		$("#referencia_bancaria").prop("disabled", true);
		$("#referencia_bancaria").val("");
		// $("#folio_interno").val("");
		$("#folio_factura").val("");
		$("#select-basura-fpago option:first").prop("selected", true);

		//dir_cliente = rowData[0][2] + " No. " + rowData[0][3] + " por " + rowData[0][6] + " y " + rowData[0][7];
		dir_cliente = rowData[0][2];
		if(rowData[0][3] != "" )
			dir_cliente += " No. " + rowData[0][3];

		if(rowData[0][8] != "")
			dir_cliente += " por " + rowData[0][8];

		if(rowData[0][20] != "")
			dir_cliente += " y " + rowData[0][20];

		direccionCte = dir_cliente + " colonia " + rowData[0][6];

		$("#hdd-recolecta-direc").val(dir_cliente);
		$("#hdd-recolecta-colonia").val(rowData[0][6]);

		$("#edt-cobro-Nombre").val(rowData[0][1]);
		$("#edt-cobro-direccion").val(direccionCte);
		if(parseInt(rowData[0][4]) > 0){
			$("#edt-cobro-ultanio").val(rowData[0][4]);
			// $("#select-cobro-anio").val(rowData[0][4]);
		}//fin:if
		else{$("#edt-cobro-ultanio").val("");}

		//Ultimo_mes_pago
		if(rowData[0][5] != ""){
			$("#edt-cobro-ultmes").val(rowData[0][5]);
			//$("#select-cobro-mes").val(parseInt(rowData[0][17]) + 1);
			if(parseInt(rowData[0][17]) > 11){
				$("#select-cobro-mes").val(1);
				// $("#select-cobro-anio").val((parseInt(rowData[0][4]) + 1));
			}//fin:if
			else
				$("#select-cobro-mes").val(parseInt(rowData[0][17]) + 1);

			$("#hdd-ultpago-mes").val(parseInt(rowData[0][17]));
		}//fin:if
		else{ $("#edt-cobro-ultmes").val("");}
		//$("#edt-cobro-ultmes").val(rowData[0][5]);

		idCliente = parseInt(rowData[0][0]);
		$("#hdd-recolecta-idcte").val(idCliente);
		$("#hdd-mensualidad").val(parseFloat(rowData[0][16]));
		$("#hdd-recolecta-ruta").val(rowData[0][18]);
		$("#hdd-recibo-email").val(rowData[0][19]);
		$("#hdd-cobro_manual").val(rowData[0][21]);
		$("#hdd-monto_cobro").val(parseFloat(rowData[0][22]));

		//Reiniciar controles editables
		//btn-cobro-nota
		$("#btn-recibo-cliente").prop('disabled', true)
		$("#select-cobro-anio").attr('selected','selected');
		$("#select-cobro-mes").attr('selected','selected');
		$("#btn-cobro-add").prop('disabled', false);
		$("#btn-cobro-mes-descuento").prop('disabled', false);
		$('#aviso-adeudo').html("");
		$("#hdd-servicio-idpago").val("");
		$("#hdd-cobro-idpago").val("");
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		//Inicializar los controles para el modal del Recibo de Cobro
		$("#recibo-nombre").html("");
		$("#recibo-direccion").html("");
		$("#recibo-colonia").html("");
		$("#recibo-ruta").html("");
		$("#recibo-idcliente").html("");
		$("#recibo-concepto").html("");
		$("#recibo-meses").html("");
		$("#recibo-numletras").html("");
		$("#recibo-total").html("");
		$("#recibo-folio").html("");
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		$("#modal-cobro-cliente").modal({backdrop: 'static', keyboard: false, show: true});
		$("#select-basura-fpago").change();
	}//fin:if(rowData[0].length > 0)
}//fin:fnCobrarCliente

function initDropzonePDFFactura() {
	rutaDropzoneFactura = new Dropzone("div#rutaFolioFactura",{
		url: "/clientes/upload",
		// acceptedFiles: ".xls, .xlsx, .csv",
		acceptedFiles: "application/pdf",
		maxFiles: 1,
		dictInvalidFileType: "Tipo de archivo no permitido",
		maxFilesize: "10",
		autoProcessQueue: false,
		dictFileTooBig: "Peso máximo: 10M",
		createImageThumbnails: false
	});


	rutaDropzoneFactura.on("addedfile", function(file){
		if(rutaDropzoneFactura.getQueuedFiles().length > 0){
			rutaDropzoneFactura.removeFile(rutaDropzoneFactura.getQueuedFiles()[0]);
		}
		getBase64(file);
	});

	rutaDropzoneFactura.on("sending", function(file, xhr, formData){

	});

	rutaDropzoneFactura.on('success', function (file, response) {
		rutaDropzoneFactura.removeAllFiles();
	});

	rutaDropzoneFactura.on('error', function (file, response) {
		rutaDropzoneFactura.removeAllFiles();
		showGeneralMessage("Ocurrió un error al guardar la imagen", "error", true);
	});
}

function fnCobrarFacturado() {

	//--------------------------------------------------------------------------
	//Validaciones generales
	if (idsSelected.length <= 0) {
		showGeneralMessage("Es necesario seleccionar al menos un mes para continuar.", "warning", true);
		return;
	}//fin:

	var factura = $("#folio_factura").val().trim();
	if(factura == ""){
		showGeneralMessage("Folio factura es obligatorio.", "warning", true);
		return;
	}
	//--------------------------------------------------------------------------
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	guardarMensualidades();
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
}//fin:function

function getBase64(file) {
	var reader = new FileReader();
	reader.readAsDataURL(file);
	reader.onload = function () {
		//console.log(reader.result);
		pdfFacturaBase64 = reader.result;
	};
	reader.onerror = function (error) {
		console.log('Error: ', error);
		pdfFacturaBase64;
		rutaDropzoneFactura.removeAllFiles();
		showGeneralMessage("Ocurrió un error al agregar la factura", "warning", true);
	};
}

function viewTotalFacturas(show) {
	isFactura = show;
	$("#subtotalPagar").val("");
	$("#ivaPagar").val("");
	if(show) {
		$("#divTotalPagar").removeClass("col-xs-offset-9");
		$("#divTotalesFactura").show();
	} else {
		$("#divTotalPagar").addClass("col-xs-offset-9");
		$("#divTotalesFactura").hide();
	}
}

function getDataToPrint(){
	//var htmlbody = "<html><body>" + document.getElementById('recibo-print-body').innerHTML + "</body></html>";
	var tlb1 =  document.getElementById('tblinfocliente').outerHTML;
	var tlb2 =  document.getElementById('tblReciboTotales').outerHTML;
	var txtPromAnual =  document.getElementById('txtMensajePromAn').outerHTML;
	if(isdescan){
		tlb2 += '<br/>' + txtPromAnual;
	}

	return {
		tblcliente : tlb1,
		tbltotales: tlb2
	}
}
