addevents = [];
revertFuncOnCancel = null;
brigadas = [];
allServicesDT = null; //Tabla servicos a guardar
getServicesDT = null;

$(document).ready(function() {

    idcoloniaForm = $("#idcolonia_cliente").selectize();
    $("#btnCrearClienteConfirm").on("click", function () {
        $("#modalConfirmacionCliente").on("hidden.bs.modal", function () {
            $("#modalConfirmacionCliente").off("hidden.bs.modal");
            $("#calle_cliente").val($("#os-calle-val").val());
            $("#numero_cliente").val($("#os-numero-val").val());
            var idcolonia = $("#os-colonia-val").val();
            if(idcolonia){
                idcoloniaForm[0].selectize.setValue(idcolonia, true);
            }
            $("#modalCrearCLienteExterno").modal("show");
        });
        $("#modalConfirmacionCliente").modal("hide");
    });

    $("#btnCrearClienteCancel").on("click", function () {
        $("#modalConfirmacionCliente").modal("hide");
    });

    $("#btnCrearNuevoCliente").on("click", function () {
        var calle = parseInt($("#calle_cliente").val());
        var calle_letra =  getTextValue($("#calle_letra_cliente").val());
        var tipo_calle =  getTextValueUpper($("#tipo_calle_cliente").val());
        var numero =  parseInt($("#numero_cliente").val());
        var numero_letra = getTextValue($("#numero_letra_cliente").val());
        var tipo_numero = getTextValue($("#tipo_numero_cliente").val());
        var cruza1 =  getTextValue($("#cruza1_cliente").val());
        var letra_cruza1 =  getTextValue($("#letra_cruza1_cliente").val());
        var tipo_cruza1 =  getTextValue($("#tipo_cruza1_cliente").val());
        var cruza2 = getTextValue($("#cruza2_cliente").val());
        var letra_cruza2 = getTextValue($("#letra_cruza2_cliente").val());
        var tipo_cruza2 = getTextValue($("#tipo_cruza2_cliente").val());
        var idcolonia = parseInt($("#idcolonia_cliente").val());
        var apepat = getTextValue($("#apepat_cliente").val());
        var apemat = getTextValue($("#apemat_cliente").val());
        var nombres = getTextValue($("#nombre_cliente").val());
        var referencia_ubicacion = getTextValue($("#referencia_ubicacion_cliente").val());

        var telefono = getTextValue($("#telefono_cliente").val());
        var correo =  getTextValue($("#correo_cliente").val());

        if(calle == "" || numero == "" || idcolonia == "" || telefono == "" || calle == null || numero == null || idcolonia == null || telefono == null){
            showGeneralMessage("Los campos marcados con * son obligatorios");
            return;
        }

        var data = {
            calle: calle,
            calle_letra: calle_letra? calle_letra : null,
            tipo_calle: tipo_calle ? tipo_calle : null,
            numero: numero,
            numero_letra: numero_letra ? numero_letra : null,
            tipo_numero : tipo_numero ? tipo_numero : null,
            cruza1: cruza1 ? cruza1 : null,
            letra_cruza1: letra_cruza1 ? letra_cruza1 : null,
            tipo_cruza1: tipo_cruza1 ? tipo_cruza1 : null,
            cruza2: cruza2 ? cruza2 : null,
            letra_cruza2: letra_cruza2 ? letra_cruza2 : null,
            tipo_cruza2: tipo_cruza2 ? tipo_cruza2 : null,
            idcolonia: idcolonia,
            apepat: apepat ? apepat : null,
            apemat: apemat ? apemat : null,
            nombres: nombres ? nombres : null,
            referencia_ubicacion : referencia_ubicacion ? referencia_ubicacion : null,
            telefono: telefono,
            correo: correo ? correo : null
        };

        $.ajax({
            url: '/servicios/savecliente',
            data: $.param(data),
            type: 'POST',
            beforeSend: function(){

            },
            success: function(resp){
                var json = JSON.parse(resp);
                showGeneralMessage('Resultados guardados con éxito!', "success", true);
                $("#modalCrearCLienteExterno").modal("hide");
                // buscarClientesOrden();
                buscarOrden(json.id, correo, telefono, json.direccion, json.persona);

            },
            error: function(xhr, status, error){
                var colonia = $("#idcolonia :selected").text();
                if(xhr.status == 400){
                    showGeneralMessage("Método no implementado", "error", true);
                }
                else if(xhr.status == 401){
                    //showGeneralMessage('No cuenta con permisos, contacte al administrador', "error", true);
                    $("#sesion-modal").modal({
                        backdrop:"static",
                        keyboard: false
                    });
                    return;
                }
                else if(xhr.status == 404){
                    showGeneralMessage("No se encontro el cliente con id: " + id, "error", true);
                }
                else if(xhr.status == 409){
                    var text = "Se encontró coincidencia en la dirección, calle: " + data.calle;
                    if(data.calle_letra) {
                        text += " letra: " + data.calle_letra;
                    }
                    text += " numero: " + data.numero;
                    if(data.numero_letra) {
                        text += " letra: " + data.numero_letra;
                    }
                    text += " colonia: " + colonia;
                    showGeneralMessage(text, "error", true);
                }
                else{
                    showGeneralMessage("Ocurrió un error al guardar los datos", "error", true);
                }
            }
        });
    });

    $("#correo_cliente").inputmask({ alias: "email", clearIncomplete: true});
    $("#calle_cliente").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
    $("#numero_cliente").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
    $("#cruza1_cliente").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
    $("#cruza2_cliente").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
    $("#telefono_cliente").inputmask({ "mask": "(9999) 99-99-99", "clearIncomplete": true});

    horafin = $("#horasfin").timepicker({
        showMeridian:false,
        minuteStep: 30
    });

    horasfinagendar = $("#horasfinagendar").timepicker({
        showMeridian:false,
        minuteStep: 30
    });

    $("#horasfinagendar").on("blur", function () {
        if(this.value == ""){
            var addTime = $("#duracionhorasservicios").val();
            var hora = $("#horas").val();
            if(hora == ""){
                $('#horasfin').timepicker('setTime', "0:00");
                return;
            }
            else{
                var hv = hora.split(":");
                var hav = addTime.split(":");
                $('#horasfin').timepicker('setTime', parseInt(hv[0]) + parseInt(hav[0]) + ":" + parseInt(hv[1]));
            }
        }
    });

    $("#horasfinagendar").on("blur", function () {
       if(this.value == ""){
           var addTime = $("#agendarservicios :selected").attr("data-horadura");
           var hora = $("#horasagendar").val();
           if(hora == ""){
               $('#horasfinagendar').timepicker('setTime', "0:00");
               return;
           }
           else{
               var hv = hora.split(":");
               var hav = addTime.split(":");
               $('#horasfinagendar').timepicker('setTime', parseInt(hv[0]) + parseInt(hav[0]) + ":" + parseInt(hv[1]));
           }
       }
    });
    // $colsel = $('#clientes-colonia-val').selectize();
    // colsel = $colsel[0].selectize;
    // colsel.clear();

    $colselos = $('#os-colonia-val').selectize();
    colselos = $colselos[0].selectize;
    colselos.clear();

    $("#telefono-cliente-agendar").inputmask({ "mask": "(9999) 99-99-99", "clearIncomplete": true});
    $("#os-telefono-val").inputmask({ "mask": "(9999) 99-99-99", "clearIncomplete": true});
    var date = new Date();
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth()+1).toString().length == 1 ? "0"+(date.getMonth()+1).toString() : (date.getMonth()+1).toString();
    var dd  = (date.getDate()).toString().length == 1 ? "0"+(date.getDate()).toString() : (date.getDate()).toString();

    $("#costo").inputmask('Regex', {regex: "^[0-9]{1,9}(\\.\\d{1,2})?$"});
    $("#materialextra").inputmask('Regex', {regex: "^[0-9]{1,9}(\\.\\d{1,2})?$"});
    $("#duracion").inputmask('Regex', {regex: "^0[0-9]{1}(\\:\\d{2})?$"});

    $("#correo-cliente-agendar").inputmask({
        alias: "email",
        greedy: false,
        clearIncomplete: true
    });

    clientesDT = $("#clientes-datatable").DataTable({
        // "dom": "<'row'<'col-sm-6'><'col-sm-6'fB>>" +
        // "<'row'<'col-sm-12'tr>>" +
        // "<'row'<'col-sm-12'i>>" +
        // "<'row'<'col-sm-6'l><'col-sm-6'p>>",
        "dom": '<"clear"><"datatable-scroll"rt><"bottom"ip><"clear">',
        "autoWidth": false,
        "paging": true,
        "select": true,
        "info": true,
        // searching: true,
        "processing": true,
//            "scrollY":        "506px",
        "scrollCollapse": false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
        "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
        "pageLength": 10,
        "order": [[ 1, 'asc' ]],
        responsive: {
            details: {
                type: 'column',
                target: 0
            }
        },
        "processing": true,
        "serverSide": true,
        "deferLoading": 0,
        ajax: {
            url: "/servicios/buscarcliente",
        },
        "columnDefs": [
            {
                targets: [0],
                orderable: false,
                searchable: false,
                className: 'dt-center no-wrap',
            },
            // { targets: [0], orderable: false, className: 'control', searchable: false},
            // { targets: [2], searchable: true, className: 'dt-center'},
            // { targets: [3, 4,5], className: 'dt-center'},
            { targets: '_all', visible: true }
        ],
        "lengthChange": true
    });

    osDT = $("#os-dt").DataTable({
        // "dom": "<'row'<'col-sm-6'><'col-sm-6'fB>>" +
        // "<'row'<'col-sm-12'tr>>" +
        // "<'row'<'col-sm-12'i>>" +
        // "<'row'<'col-sm-6'l><'col-sm-6'p>>",
        "dom": '<"clear"><"datatable-scroll"rt><"bottom"ip><"clear">',
        "autoWidth": false,
        "paging": true,
        "select": true,
        "info": true,
        // searching: true,
        "processing": true,
//            "scrollY":        "506px",
        "scrollCollapse": false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin servicios",
            "sZeroRecords": "Sin servicios",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
        "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
        "pageLength": 5,
        "order": [[ 1, 'desc' ]],
        responsive: {
            details: {
                type: 'column',
                target: 0
            }
        },
        "processing": true,
        "deferLoading": 0,
        "columnDefs": [
            {
                targets: [0],
                orderable: false,
                searchable: false,
                className: 'dt-center no-wrap',
            },
            // { targets: [0], orderable: false, className: 'control', searchable: false},
            // { targets: [2], searchable: true, className: 'dt-center'},
            // { targets: [3, 4,5], className: 'dt-center'},
            { targets: '_all', visible: true }
        ],
        "lengthChange": true,
        "createdRow": function( row, data, dataIndex ) {
            $(row).attr('id', data[1]);
        }
    });

    osClienteDT = $("#oscliente-dt").DataTable({
        // "dom": "<'row'<'col-sm-6'><'col-sm-6'fB>>" +
        // "<'row'<'col-sm-12'tr>>" +
        // "<'row'<'col-sm-12'i>>" +
        // "<'row'<'col-sm-6'l><'col-sm-6'p>>",
        "dom": '<"clear"><"datatable-scroll"rt><"bottom"ip><"clear">',
        "autoWidth": false,
        "paging": true,
        "select": true,
        "info": true,
        // searching: true,
        "processing": true,
//            "scrollY":        "506px",
        "scrollCollapse": false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
        "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
        "pageLength": 5,
        "order": [[ 1, 'desc' ]],
        responsive: {
            details: {
                type: 'column',
                target: 0
            }
        },
        "processing": true,
        "deferLoading": 0,
        "columnDefs": [
            {
                targets: [0],
                orderable: false,
                searchable: false,
                className: 'dt-center no-wrap',
            },
             { targets: [-1,-2,-3, -4], orderable: false, searchable: false, visible: false},
            // { targets: [2], searchable: true, className: 'dt-center'},
            // { targets: [3, 4,5], className: 'dt-center'},
            { targets: '_all', visible: true }
        ],
        "lengthChange": true
    });

    listaserviciosDT = $("#listaservicios-dt").DataTable({
        // "dom": "<'row'<'col-sm-6'><'col-sm-6'fB>>" +
        // "<'row'<'col-sm-12'tr>>" +
        // "<'row'<'col-sm-12'i>>" +
        // "<'row'<'col-sm-6'l><'col-sm-6'p>>",
        "dom": '<"clear"><"datatable-scroll"rt><"bottom"ip><"clear">',
        "autoWidth": false,
        "paging": true,
        "select": true,
        "info": true,
        // searching: true,
        "processing": true,
//            "scrollY":        "506px",
        "scrollCollapse": false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
        "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
        "pageLength": 5,
        "order": [[ 1, 'desc' ]],
        responsive: {
            details: {
                type: 'column',
                target: 0
            }
        },
        "processing": true,
        "deferLoading": 0,
        "columnDefs": [
            { targets: [0], orderable: false, searchable: false, visible: false},
            // { targets: [2], searchable: true, className: 'dt-center'},
            // { targets: [3, 4,5], className: 'dt-center'},
            { targets: '_all', visible: true }
        ],
        "lengthChange": true
    });
    $('#oscliente-dt tbody').on('dblclick','tr',function(e){
        if ($(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            if($(this).rowIndex == 0)
                uSelec = -1;
        }//fin:else
        else{
            osClienteDT.$('tr.selected').removeClass('selected');
            $(this).toggleClass('selected');
        }//fin:else
        var dc = osClienteDT.rows("tr.selected").data();
        if(dc[0][4] === false){
            showGeneralMessage("El cliente con folio " + dc[0][1] + " no se encuentra vigente, favor de comunicarse con cobranza", "warning", true);
            return;
        }
        buscarOrden(dc[0][1], dc[0][5], dc[0][6], dc[0][3], dc[0][2]);
        $("#buscarClientetModal").modal("hide");
    });

    $('#clientes-datatable tbody').on( 'click', 'tr', function (){
        if ($(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            if($(this).rowIndex == 0)
                uSelec = -1;
        }//fin:else
        else{
            clientesDT.$('tr.selected').removeClass('selected');
            $(this).toggleClass('selected');
        }//fin:else
    });


    $(".clientes-search-do").on("click", function () {
        buscar();
    });

    $("#btnGuardarCotizacionServicioModal").off("click");
    $("#btnGuardarCotizacionServicioModal").on("click", function () {
        guardarOrdenServicio();
    });

    $("#btnGuardarServicioModal").on("click", function () {
        guardarOrdenServicio();
    });

    $("#btnGuardarModal").on("click", function () {
        var idos = $("#ordenservicio").val();
        var clave = $("#estadoservicio").val();
        var observaciones = $("#observaaciones").val();
        var duracion = $("#duracion").val();
        var costo = $("#costo").val();
        var materialextra = $("#materialextra").val();
        var confactura = $("#confactura").is(":checked");
        var factura = $("#factura").val();
        var fecha = $("#diaorden").val();
        var hora = $("#horas").val();
        var horafin = $("#horasfin").val();
        var responsable = $("#listbrigadas").val();
        var errores = "";
        if(clave == ""){
            errores += "- Debe seleccionar un estado<br>";
        }

        var data = {
            observaciones: observaciones ? observaciones : null,
            clave: clave
        };

        if(clave == "AG"){
            if(responsable == ""){
                errores += "- Debe seleccionar un responsable<br>";
            }

            if(hora == ""){
                errores += "- Debe seleccionar una hora<br>";
            }
            data.fecha = fecha;
            data.hora = hora;
            data.horafin = horafin;
            data.responsable = responsable;
        }

        if(clave == "FIN"){
            if(duracion == ""){
                errores += "- Debe especificar la duración del servicios<br>";
            }

            if(costo == ""){
                errores += "- Debe especificar el costo<br>";
            }

            if(confactura && factura == ""){
                errores += "- Debe especificar el folio de la factura<br>";
            }

            data.costo = costo;
            data.materialextra = materialextra;
            data.duracion = duracion;
            data.factura = factura;
        }

        if(clave == "CA"){
            if(observaciones == ""){
                errores += "- Observaciones no debe estar vacío<br>";
            }
        }


        if(errores != ""){
            showGeneralMessage(errores, "warning", true);
            return;
        }

        $.ajax({
            url: "/servicios/save/" + idos,
            type: "POST",
            data: JSON.stringify(data),
            beforeSend: function(){
                $("#btnGuardarModal").html("Enviando ...");
                $("#btnGuardarModal").prop("disabled", true);
            },
            success: function (resp) {
                try {
                    var json = JSON.parse(resp);
                    var myevent = $('#calendar').fullCalendar( 'clientEvents', idos )[0];
                    myevent.color = "#" + json.colorestado;
                    myevent.title = json.id + " " + json.hora + " - " + json.horafin;
                    myevent.datose = json;
                    myevent.editable = false;
                    $('#calendar').fullCalendar('updateEvent', myevent);
                    revertFuncOnCancel = null;
                    $("#infoservicio").modal("hide");

                    let row = osDT.row('#' + idos);
                    let rowindex = row.index();
                    if (typeof rowindex !== "undefined") {
                        osDT.cell({row:rowindex, column:9}).data(observaciones);
                        osDT.cell({row:rowindex, column:8}).data(materialextra);
                    }

                    showGeneralMessage("La información se guardo correctamente", "success", true);
                }
                catch (e) {
                    showGeneralMessage("Ocurrio un error", "danger", true);
                }
            },
            error: function () {

            },
            complete: function () {
                $("#btnGuardarModal").html("Guardar");
                $("#btnGuardarModal").prop("disabled", false);
            }
        });
    });

    /*$("#agendarservicios").on("change", function () {
        var valor = this.value;

        //$("#agendarlistbrigadas").prop("disabled", true);
        $("#agendarlistbrigadas").val("");
        $("#horasagendar").prop("disabled", true);
        $("#horasagendar").val("");
        $.ajax({
            url: "servicios/getbrigadas/-1?fechaini=" + $("#hddstartAgendar").val() +"&ids" + valor,
            beforeSend: function(){

                $("#nora-costo-servicio").hide();
                if(valor != ""){
                    $("#nora-costo-servicio").show();
                    for(var i in datoscosto){
                        var dc = datoscosto[i];
                        if(valor == dc.id){
                            $("#costo-servicio-aprox").html("$ " + dc.costo);
                            $("#nora-costo-servicio").html(dc.nota ? dc.nota : "");
                            break;
                        }
                    }
                }
                else{

                }
            },
            success: function (resp) {
                //el[0].style.backgroundColor = "#bcbcbc"
                try {
                    $("#agendarlistbrigadas").html("<option value=''>Seleccionar</option>");
                    var json = JSON.parse(resp);
                    if(json.disp == false){
                        $("#mensaje-disponibilidad").html('<div class="alert alert-danger alert-dismissible" role="alert">' +
                            // '                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                            '                                No hay disponibilidad en el horario deseado, favor de seleccionar otro dia u otra hora' +
                            '                            </div>');
                    }
                    brigadas = json.data;
                    for (var i in json.data){
                        $("#agendarlistbrigadas").append("<option value='" + json.data[i].id + "'>" + json.data[i].responsable + "</option>");
                    }
                    if(json.disp){

                    }

                    $("#agendarlistbrigadas").prop("disabled", false);
                    $("#agendarlistbrigadas").val("");
                    $("#horasagendar").prop("disabled", false);
                    $("#horasagendar").val("");
                }
                catch (e) {

                }

                // $("#infoservicio").modal("show");
            },
            error: function () {
                showGeneralMessage("Ocurrio un error al obtener la información", "danger", true);
            }
        });

    });*/

    $("#clientes-filter-select").on("change", function(){
        var valor = this.value;
        $("#clientes-filtro-direccion").hide();
        $("#clientes-filtro-clave").hide();

        $("#clientes-calle-val").val("");
        $("#clientes-numero-val").val("");
        colsel.clear();
        $("#clientes-colonia-val").val("");
        $("#clientes-clave-val").val("");
        $("#clientes-vigente-val").val("");
        $("#clientes-vigente-val").removeAttr("disabled");

        $("#clientes-filtro-clave-").hide();
        if(valor == ""){
            $("#clientes-filtro-todos").show();
        }
        else if(valor == "nom"){
            $("#clientes-filtro-nombre").show();
        }
        else if(valor == "dir"){
            $("#clientes-filtro-direccion").show();
        }
        else if(valor == "vig"){
            $("#clientes-filtro-vigente").show();
        }
        else{
            $("#clientes-vigente-val").attr("disabled", "disabled");
            $("#clientes-filtro-clave").show();
        }
    });

    $("#os-filter-select").on("change", function(){
        var valor = this.value;
        $("#os-filtro-direccion").hide();
        $("#os-filtro-clave").hide();
        $("#os-filtro-nombre").hide();

        $("#os-calle-val").val("");
        $("#os-numero-val").val("");
        colselos.clear();
        $("#os-colonia-val").val("");
        $("#os-clave-val").val("");
        $("#os-nombre-val").val("");

        $("#os-filtro-clave-").hide();
        if(valor == ""){
            $("#os-filtro-todos").show();
        }
        else if(valor == "dir"){
            $("#os-filtro-direccion").show();
        }
        else if(valor == "nombre"){
            $("#os-filtro-nombre").show();
        }
        else{
            $("#os-filtro-clave").show();
        }
    });

    servicesCalendar = $('#calendar').fullCalendar({
        header: {
            language: 'es',
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek',
        },
        //defaultDate: yyyy+"-"+mm+"-"+dd,
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        selectable: true,
        selectHelper: true,
        height: 550,
        defaultView: "basicWeek",
        timezone: "America/Mexico_City",
        select: function(start, end) {
            generarOrdenServicio(start, false);

            if($("#idclienteforservice").val() == ""){
                // showGeneralMessage("Debe seleccionar un cliente", "warning", true);
                return;
            }
            // editEevent(event, start);
            var today = new Date();
            today.setDate(today.getDate());
            today.setHours(1,0,0,0);
            var check = new Date(start.toISOString());
            check.setDate(check.getDate() + 1);
            check.setHours(1,0,0,0);
            if(check < today){
                // showGeneralMessage("No puedes agendar en dias pasados", "warning", true);
                return;
            }

            $("#addClientModal").modal("show");
            $('#telefono-cliente-agendar').val($("#datosBusquedaTelefonoCliente").html());
            $('#correo-cliente-agendar').val($("#datosBusquedaCorreoCliente").html());
            $('#sumaTotalservicios').val('');
            $('#nuevo-referencia').val('');
            document.getElementById('nuevo-descripcion').value = "";
            $("#hddstartAgendar").val(start.toISOString());
            init_table_servicos();
        },
        viewRender: function(view, element) {
            var fecini = view.start.format();
            var fecfin = view.end.format();
            $.ajax({
                url: "servicios/getall",
                data: $.param({
                    fecini: fecini,
                    fecfin: fecfin
                }),
                success: function (resp) {
                    // console.log('orden servicio: ');
                    // console.log(resp);
                    $('#calendar').fullCalendar('removeEvents');
                    try {
                        var events = [];
                        var json = JSON.parse(resp);
                        if(json.length > 0){
                            for(var i in json){

                                var d = json[i];
                                // if(addevents["e" + d.id]) continue;
                                var e = {
                                    id: d.id,
                                    title: d.id + " " + d.hora + (d.horafin ? " - " +d.horafin : ""),
                                    total: d.total,
                                    color: "#" + d.colorestado,
                                    start: d.fecha,
                                    end: d.fecha,
                                    textColor: "#000",
                                    datose: d,
                                    editable: d.clavestado == "SOL"
                                };
                                events.push(e);
                                // console.log(events);
                                addevents["e" + d.id] = e;
                            }
                            $('#calendar').fullCalendar('renderEvents', events);
                        }
                    }
                    catch (e) {
                        console.log(e);
                    }
                }
            });
        },
        eventRender: function(event, element) {
            var data = '<br><span style="font-weight: bold;">' + event.datose.nomcli + '</span>';
            data += '<br><span style="font-weight: bold;">' + event.datose.direccion + '</span>';
            $(element).find('.fc-title').append(data);
            element.bind('click', function() {
                var el = element;
                editEevent(event, event.start.toISOString());
                get_table_ajax_services(event.id);
            });
        },
        eventDrop: function(event, delta, revertFunc, jsEvent, ui, view) { // si changement de position
            if($("#idclienteforservice").val() == ""){
                showGeneralMessage("Debe seleccionar un cliente", "warning", true);
                return;
            }
            var today = new Date();
            var check = new Date(event.start.toISOString());
            check.setDate(check.getDate() + 1);
            check.setHours(today.getHours(), today.getMinutes(), today.getSeconds(), today.getMilliseconds());
            var today = new Date();
            revertFuncOnCancel = revertFunc;
            if(check < today){
                showGeneralMessage("No puedes agendar en dias pasados", "warning", true);
                revertFunc();
                return;
            }
            editEevent(event, event.start.toISOString());
        },
        eventDragStart: function( event, jsEvent, ui, view ) {
            if(event.datose.clavestado != "SOL"){
                return false;
            }
        }
    });

    $("#infoservicio").on("hidden.bs.modal", function () {
        if(revertFuncOnCancel){
            revertFuncOnCancel();
            revertFuncOnCancel = null;
        }
    });

    $("#addClientModal").on("hidden.bs.modal", function () {
        clientesDT.column(1).search("-1000").draw();
        listaserviciosDT.clear().draw();
        $("#telefono-cliente-agendar").val("");
        $("#correo-cliente-agendar").val("");
        $("#agendarservicios").val("");
        // $("#agendarlistbrigadas").prop("disabled", true);
        $("#agendarlistbrigadas").val("");
        // $("#horasagendar").prop("disabled", true);
        $("#horasagendar").val("");
        $("#nora-costo-servicio").hide();
        $("#clientes-filter-select").val("dir").change();
        // $("#agendarlistbrigadas").html("<option value=''>Seleccionar</option>");
        $('#sumaSubtotalservicios').val(0);
        $('#sumaIVAservicios').val(0);
        $('#sumaTotalservicios').val(0);
        $("#sumaTiemposervicios").val(0);
    });

    $("#confactura").on("change", function () {
        $("#factura").val("");
        if(this.checked){
            $("#factura").prop("disabled", false);
        }
        else{
            $("#factura").prop("disabled", true);
        }
    });

    $("#estadoservicio").on("change", function () {
        var valor = this.value;
        $(".container-finalizado").hide();
        $(".container-cancelado").show();
        $("#confactura").prop("checked", false).change();
        if(valor == "AG"){
        }
        if(valor == "ATE"){
        }
        if(valor == "FIN"){
            $(".container-finalizado").show();
        }
        if(valor == "CA" || valor == "REC" || valor == "PCO"){
            $(".container-cancelado").hide();
        }
    });

    /*$("#listbrigadas").change("change", function () {
        var bri = null;
        var id = this.value;
        for(var i in brigadas){
            var b = brigadas[i];
            if(b.id == id){
                bri = b;
                break;
            }
        }

        $("#horas option").hide();
        for(var i in bri.horas){
            var d = bri.horas[i];
            $("#horas option[value='" + d+ "']").show();
        }
    });*/

    // $("#agendarlistbrigadas").change("change", function () {
    //     var bri = null;
    //     var id = this.value;
    //     for(var i in brigadas){
    //         var b = brigadas[i];
    //         if(b.id == id){
    //             bri = b;
    //             break;
    //         }
    //     }
    //
    //     $("#horasagendar option").hide();
    //     for(var i in bri.horas){
    //         var d = bri.horas[i];
    //         $("#horasagendar option[value='" + d+ "']").show();
    //     }
    // });

    $("#horas").on("change", function () {
        var value = this.value;
        if(value == "") return;
        var addTime = $("#duracionhorasservicios").val();
        var hv = value.split(":");
        var hav = addTime.split(":");
        $('#horasfin').timepicker('setTime', parseInt(hv[0]) + parseInt(hav[0]) + ":" + parseInt(hv[1]));
    });

    $("#horasagendar").on("change", function () {
        var value = this.value;
        if(value == "") return;
        var addTime = $("#agendarservicios :selected").attr("data-horadura");
        var hv = value.split(":");
        var hav = $("#sumaTiemposervicios").val() != '' ? $("#sumaTiemposervicios").val() : 0;
        $('#horasfinagendar').timepicker('setTime', parseInt(hv[0]) + parseInt(hav) + ":" + parseInt(hv[1]));
    });

    $(".os-search-do").on("click", function () {
        buscarClientesOrden();
    });

    $("#btnAddServicioList").on("click", function () {
        var $els = $("#agendarservicios");
        var id = $els.val();
        if(id == ""){
            showGeneralMessage("Debe seleccionar un servicio", "warning", true);
            return;
        }
        var servicio = $els.find(":selected").text();
        var costo = $els.find(":selected").attr("data-costo");
        listaserviciosDT.row.add([id, servicio, "<input type='number' step='.01' min='0' oninput='this.value = Math.abs(this.value)' class='form-control costo-servicio' value='" + costo + "' />", costo]).draw();
        var costototal = $("#total-servicios").html();
        var total = parseFloat(costo) + parseFloat(costototal);
        $("#total-servicios").html(total.toFixed(2));
        $els.val("");
    });

    $(document).on('keyup', '#listaservicios-dt td .costo-servicio', function(){
        var the_row = listaserviciosDT.row($(this).parents('tr'));
        listaserviciosDT.cell({row: the_row.index(), column:3}).data(this.value);
        var data = listaserviciosDT.data();
        var total = 0.00;
        var i = 0;
        for(i = 0; i < data.length; i++ ){
            var costo = parseFloat(data[i][3]);
            total += costo;
        }
        $("#total-servicios").html(total.toFixed(2));
    });



    $('#tableToSaveAllServices tbody').on("change", ".EditCosto", function () {
        var cos = $(this).val();
        var tr = $(this).parents('tr');
        var row = allServicesDT.row( tr );
        var rowindex = row[0];
        var fila = allServicesDT.row($(this).parents("tr")).data();
        var cot_A = allServicesDT.row($(this).parents("tr")).data()[5];
        /*var dato = getServicesDT.$('.EditCosto').val();*/
        //console.log(fila);
        var modalUpCostos = bootbox.confirm({
            title: 'Editar costo de Servicio',
            message: "¿Desea actualizar el costo del servicio?",
            buttons: {
                confirm: {
                    label: 'Si',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                    var totalFinal = 0;
                    allServicesDT.column(2).data().$('input').each(function(_this) {
                        totalFinal = totalFinal + parseFloat($(this).val());
                        console.log(totalFinal);
                    });
                    var iva = parseFloat(0.16 * totalFinal).toFixed(2);
                    $('#sumaSubtotalservicios').val((totalFinal-iva));
                    $('#sumaIVAservicios').val(iva);
                    $('#sumaTotalservicios').val(totalFinal);

                    /*var objdata = {
                        id: fila[0],
                        costo: cos
                    };*/

                    // $.ajax({
                    //     url: "/servicios/updateCostoServicio",
                    //     type: "POST",
                    //     data: JSON.stringify(objdata),
                    //     success: function (resp) {
                    //         var js_data = JSON.parse(resp);
                    //         if(js_data.lError){
                    //             showGeneralMessage(js_data.cMensaje, "error", true);
                    //         }else{
                    //             getServicesDT.draw();
                    //             showGeneralMessage(js_data.cMensaje, "success", true);
                    //             setTimeout(mostrarSumaTotal, 3000);
                    //         }
                    //     },
                    //     error: function(xhr, status, error){
                    //         //code
                    //     }
                    // });
                }else{
                    var btnedit='<input type="text" id="cost" minlength="0" class="form-control EditCosto" value="'+cot_A+'">';
                    allServicesDT.cell({row:rowindex, column:3}).data(btnedit);
                }
            }
        });
        modalUpCostos.on('hidden.bs.modal', function (e) { $('body').addClass('modal-open'); });
    });

    $(".os-cotizar-do").off("click");
    $(".os-cotizar-do").on("click", function(e) {
        abrirCotizar();
    });

    $(".os-exportar-do").off("click");
    $(".os-exportar-do").on("click", function(e) {
        abrirExportar();
    });

    $('#exp_finicio').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });

    $('#exp_ffin').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });

    $("#exp_finicio").datepicker('update', new Date());
    $("#exp_ffin").datepicker('update', new Date());

    $("#btnExportarModal").off("click");
    $("#btnExportarModal").on("click", function() {
        exportarOrdenes();
    });

    $("#clavestaagcot").on("change", function () {
        var val = this.value;
        $(".hiddencotizar").hide();
        if(val != 'COT'){
            $(".hiddencotizar").show();
        }
    });

    $("#clavestaagcot").change();
});

function generarOrdenServicio(start, cotizacion) {
    if($("#idclienteforservice").val() == "") {
        showGeneralMessage("Debe seleccionar un cliente.", "warning", true);
        return;
    }
    if(!cotizacion) {
        // editEevent(event, start);
        var today = new Date();
        today.setDate(today.getDate());
        today.setHours(1,0,0,0);
        var check = new Date(start.toISOString());
        check.setDate(check.getDate() + 1);
        check.setHours(1,0,0,0);
        if(check < today) {
            showGeneralMessage("No puedes agendar en días pasados", "warning", true);
            return;
        }
    }

    $("#addClientModal .modal-title").html(cotizacion ? "Cotizar servicio" : "Agendar servicio");
    $("#addClientModal #hddCotizar").val(cotizacion ? "true" : "false");

    if(cotizacion) {
        $("#btnGuardarServicioModal").hide();
        $("#btnGuardarCotizacionServicioModal").show();
    } else {
        $("#btnGuardarCotizacionServicioModal").hide();
        $("#btnGuardarServicioModal").show();
    }

    $("#addClientModal").modal("show");
    if(!cotizacion) {
        $("#hddstartAgendar").val(start.toISOString());
    }
    init_table_servicos();
}

function abrirCotizar() {
    generarOrdenServicio(null, true);
}

function abrirExportar() {
    $(".exp-data-cliente").hide();

    // var idCliente = $("#idclienteforservice").val();
    // if(idCliente != "") {
    //     var nombre = $("#datosBusquedaNombreCliente").html();
    //
    //     $("#exp_cliente").val(idCliente);
    //     $("#exp_clientelabel").html(nombre);
    //     $(".exp-data-cliente").show();
    // }

    $("#modalExportar").modal("show");
}

function exportarOrdenes() {
    var idCliente = $("#exp_cliente").val();
    var finicio = $("#exp_finicio").val();
    var ffin = $("#exp_ffin").val();
    var estado = $("#exp_estadoservicio").val();

    var surl = "servicios/exportar?cliente=" + idCliente + "&finicio=" + finicio + "&ffin=" + ffin + "&estado=" + estado;

    var $a = $("<a>");
    $a.attr("href",surl);
    $("body").append($a);
    $a.attr("download","ordenes.xls");
    $a[0].click();
    $a.remove();

    //document.location.href = surl;
    //window.open(surl, '_blank');
}

function guardarOrdenServicio() {

    var cotizar = $("#clavestaagcot").val();
    var telefono = $("#telefono-cliente-agendar").val().trim().replace(/\D+/g, '');
    var correo = $("#correo-cliente-agendar").val();
    //var servicio = $("#agendarservicios").val();
    var responsable = $("#agendarlistbrigadas").val();
    var hora = $("#horasagendar").val();
    var horafin = $("#horasfinagendar").val();
    var fecha = $("#hddstartAgendar").val();
    var referencia = $("#nuevo-referencia").val();
    var descripcion = $("#nuevo-descripcion").val();
    var array_servicios = [];
    validarIsSaveServicios=false;

    allServicesDT.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
        array_servicios.push(this.data()); //existentes
    });

    var error = "";
    if($("#idclienteforservice").val() == ""){
        error += "- Debe seleccionar un cliente<br>";
    }
    if(telefono == ""){
        error += "- Debe proporcionar un telefono<br>";
    }
    if(array_servicios.length <= 0){
        validarIsSaveServicios=false;
        error += "- Debe proporcionar minimo un servicio<br>";
    }else{
        validarIsSaveServicios=true;
    }

    if(cotizar != 'COT') {
        if (responsable == "") {
            error += "- Debe seleccionar un responsable<br>";
        }
    }

    if (hora == "") {
        error += "- Debe seleccionar una hora<br>";
    }
    if (error != "") {
        showGeneralMessage(error, "warning", true);
        return;
    }

    // var dc = clientesDT.rows("tr.selected").data();
    // $fecha = $rawBody->fecha;
    // $hora = $rawBody->hora;
    // $idservicio = $rawBody->servicio;
    // $correo = $rawBody->correo;
    // $telefono = $rawBody->telefono;
    var data = {
        fecha: fecha,
        hora: hora,
        horafin: horafin,
        servicios: array_servicios,
        correo: correo ? correo : null,
        telefono: telefono,
        responsable: cotizar != 'COT' ? responsable : null,
        referencia: referencia,
        descripcion: descripcion,
        cotizar: cotizar
    };
    $.ajax({
        url: "/servicios/clientesagendarcita/" + $("#idclienteforservice").val(),
        data: JSON.stringify(data),
        type: "POST",
        success: function (resp) {
            var d = JSON.parse(resp);
            if(d.lError){
                showGeneralMessage(d.cMensaje, "warning", true);
            }else{
                da = d.orden;
                var e = {
                    id: da.id,
                    title: da.id + " " + da.hora + " - " + da.horafin,
                    color: "#" + da.colorestado,
                    start: da.fecha,
                    end: da.fecha,
                    textColor: "#000",
                    datose: da,
                    editable: da.clavestado == "SOL"
                };
                addevents["e" + da.id] = e;
                $('#calendar').fullCalendar('renderEvent', e);
                showGeneralMessage("La informacion se guardo correctamente","success", true);
                $("#addClientModal").modal("hide");
                buscarOrden($("#idclienteforservice").val(), correo, telefono, da.direccion, da.nomcli);
            }
        }
    });
}

function editEevent(event, fecha){
    var datos = event.datose;
    $("#mensaje-disponibilidad").html('');
    $("#estadoservicio").val("");
    $("#horaorden").val("");
    $("#duracion").val("");
    $("#costo").val("");
    $("#materialextra").val("");
    $("#factura").val("");
    $("#observaaciones").val("");
    $("#duracionhorasservicios").val();

    $("#duracion").prop("disabled", false);
    $("#costo").prop("disabled", false);
    $("#materialextra").prop("disabled", false);
    $("#confactura").prop("disabled", false);
    $("#factura").prop("disabled", false);
    $("#observaaciones").prop("disabled", false);
    $("#estadoservicio").prop("disabled", false);
    $("#btnGuardarModal").prop("disabled", false);

    $("#duracionhorasservicios").val(datos.horasservicio);

    $("#nombreservicio").html('Orden de servicio' + " (" + datos.id+ ")");
    $("#ordenservicio").val(datos.id);
    $("#ordenserviciolabel").html(datos.id);
    $("#diaorden").val(fecha);
    $("#nombrecliente").html(datos.idcliente + (datos.nomcli ? " (" + datos.nomcli + ")" : ""));
    $("#direccioncliente").html(datos.direccion);
    $("#referenciacliente").html(datos.referencia);
    $("#horacliente").html(datos.hora);
    $("#horafincliente").html(datos.horafin);
    $("#fechacliente").html(datos.fecha);
    $("#telcliente").html(datos.telefono ? datos.telefono.replace(/(\d{3})(\d{3})(\d{4})/, '($1)-$2-$3') : "");
    $("#correocliente").html(datos.correo);
    $("#estadoservicio option").hide();
    $(".container-finalizado").hide();


    $("#confactura").prop("checked", false).change();
    if(datos.clavestado == "SOL" || datos.clavestado == "COT" || datos.clavestado == 'PCO'){
        $("#container-hora-agendar").show();
        $("#container-hora-agendado").hide();
        $("#container-horafin-agendar").show();
        $("#container-horafin-agendado").hide();
        $("#estadoservicio").val("");
        $("#listbrigadas").show();
        $("#nombreresponsable").hide();

        if(datos.clavestado == "COT"){
            $("#estadoservicio option[value=PCO]").show();
            $("#estadoservicio option[value=REC]").show();
        }

        if(datos.clavestado == "PCO"){
            $("#estadoservicio option[value=AG]").show();
            $("#estadoservicio option[value=REC]").show();
        }

        if(datos.clavestado == "SOL") {
            $("#estadoservicio option[value=AG]").show();
            $("#estadoservicio option[value=CA]").show();
        }
        $.ajax({
            url: "servicios/getbrigadas/" + datos.id + "&fechaini=" + fecha,
            success: function (resp) {
                //el[0].style.backgroundColor = "#bcbcbc"
                try {
                    $("#listbrigadas").html("<option value=''>Seleccionar</option>");
                    var json = JSON.parse(resp);
                    if(json.disp == false){
                        $("#mensaje-disponibilidad").html('<div class="alert alert-danger alert-dismissible" role="alert">' +
                            // '                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                            '                                No hay disponibilidad en el horario deseado, favor de seleccionar otro dia u otra hora' +
                            '                            </div>');
                    }
                    brigadas = json.data;
                    for (var i in json.data){
                        $("#listbrigadas").append("<option value='" + json.data[i].id + "'>" + json.data[i].responsable + "</option>");
                    }
                    if(json.disp){

                    }
                }
                catch (e) {

                }
                $("#infoservicio").modal("show");
            },
            error: function () {
                showGeneralMessage("Ocurrio un error al obtener la información", "danger", true);
            }
        });
    }
    else{
        $("#container-hora-agendar").hide();
        $("#container-hora-agendado").show();
        $("#container-horafin-agendar").hide();
        $("#container-horafin-agendado").show();
        $("#container-hora-agendado").html(datos.hora);
        $("#container-horafin-agendado").html(datos.horafin);
        $("#btnGuardarModal").prop("disabled", false);
        $("#estadoservicio").prop("disabled", false);
        $("#listbrigadas").hide();
        $("#nombreresponsable").show();
        $("#nombreresponsable").html(datos.responsable);
        if(datos.clavestado == "AG"){
            $("#estadoservicio option[value=ATE]").show();
            // $("#estadoservicio option[value=CA]").show();
            $("#container-finalizado").show();
        }
        if(datos.clavestado == "ATE"){
            $("#estadoservicio option[value=FIN]").show();
        }
        if(datos.clavestado == "FIN" || datos.clavestado == "CA" || datos.clavestado == 'REC'){
            $("#estadoservicio").val(datos.clavestado);
            $("#estadoservicio").prop("disabled", true);
            $("#btnGuardarModal").prop("disabled", true);
            if(datos.clavestado == "FIN") {
                $(".container-finalizado").show();
                $("#costo").val(datos.costo);
                //$("#materialextra").val(datos.costo_extra);
                $("#duracion").val(datos.duracion);
                if(datos.factura){
                    $("#confactura").prop("checked", true).change();
                    $("#factura").val(datos.factura);
                }
                $("#duracion").prop("disabled", true);
                $("#costo").prop("disabled", true);
                $("#materialextra").prop("disabled", true);
                $("#confactura").prop("disabled", true);
                $("#factura").prop("disabled", true);
            }
            $("#observaaciones").val(datos.observaciones);
            $("#observaaciones").prop("disabled", true);
        }
        $("#infoservicio").modal("show");
    }
}

function buscar(){
    var nombre = $("#clientes-nombre-val").val();
    var calle = $("#clientes-calle-val").val();
    var numero = $("#clientes-numero-val").val();
    var colonia = $("#clientes-colonia-val").val();
    var clave = $("#clientes-clave-val").val();
    var tipoBusqueda = $("#clientes-filter-select").val();
    var tipoVigente = $("#clientes-vigente-val").val();
    clientesDT.column(1).search("");
    clientesDT.column(2).search("");
    clientesDT.column(3).search("");
    clientesDT.column(6).search("");

    if(tipoBusqueda == "cla"){
        if(clave == ""){
            showGeneralMessage("Debe escribir la clave a buscar", "warning", true);
            return;
        }
        clientesDT.column(1).search(clave);
    }
    if(tipoBusqueda == "dir"){
        if(calle == "" && numero == "" && colonia == ""){
            showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
            return;
        }

        if(calle == "" && numero == "" && colonia == ""){
            showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
            return;
        }

        if(calle == "" && colonia == ""){
            showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
            return;
        }

        if(numero == "" && colonia == ""){
            showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
            return;
        }
        clientesDT.column(2).search(calle);
        clientesDT.column(3).search(numero);
        clientesDT.column(6).search(colonia);
    }



    clientesDT.draw();
}

function buscarOrden(idcliente, correo, telefono, direccion, cliente){
    $.ajax({
        url: "/servicios/getOrdenes/" + idcliente,
        // data: $.param(data),
        beforeSend: function(){
            osDT.clear().draw();
        },
        success: function (resp) {
            $("#idclienteforservice").val(idcliente);
            $("#telefono-cliente-agendar").val(telefono);
            $("#correo-cliente-agendar").val(correo);

            $("#datosBusquedaClaveCliente").html(idcliente);
            $("#datosBusquedaDireccionCliente").html(direccion);
            $("#datosBusquedaTelefonoCliente").html(telefono);
            $("#datosBusquedaCorreoCliente").html(correo);
            $("#datosBusquedaNombreCliente").html(cliente);
            var json = JSON.parse(resp);
            osDT.rows.add(json).draw();
        }

    })
}

function buscarClientesOrden(){
    var calle = $("#os-calle-val").val();
    var numero = $("#os-numero-val").val();
    var colonia = $("#os-colonia-val").val();
    var tel = $("#os-telefono-val").val();
    var nombre = $("#os-nombre-val").val().trim();
    var tipoBusqueda = $("#os-filter-select").val();

    if(tipoBusqueda == "tel"){
        if(tel == ""){
            showGeneralMessage("Debe escribir el teléfono a buscar", "warning", true);
            return;
        };
    }
    if(tipoBusqueda == "dir"){
        if(calle == "" && numero == "" && colonia == ""){
            showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
            return;
        }

        if(calle == "" && numero == "" && colonia == ""){
            showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
            return;
        }

        if(calle == "" && colonia == ""){
            showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
            return;
        }

        if(numero == "" && colonia == ""){
            showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
            return;
        }
    }

    if(tipoBusqueda == "nombre"){
        if(nombre == ""){
            showGeneralMessage("Debe escribir un nombre", "warning", true);
            return;
        }
    }
    var data = {
        tipo: tipoBusqueda,
        calle: calle,
        numero: numero,
        colonia: colonia,
        telefono: tel.trim().replace(/\D+/g, ''),
        nombre: nombre
    }

    $.ajax({
        url: "/servicios/getClientesOrdenes",
        data: $.param(data),
        beforeSend: function(){
            osDT.clear().draw();
            osClienteDT.clear().draw();
        },
        success: function (resp) {
            var json = JSON.parse(resp);
            if(json.length > 1){
                var json = JSON.parse(resp);
                osClienteDT.rows.add(json).draw();
                $("#buscarClientetModal").modal("show");
            }
            else{
                if(json.length == 0){
                    $("#modalConfirmacionCliente").modal("show");
                    //showGeneralMessage("No se enccontraron resultados con estos datos, puede agregar un cliente nuevo", "warning", true);
                }
                else{
                    if(json[0][4] === false && json[0][7] === false){
                        showGeneralMessage("El cliente con folio " + json[0][1] + " no se encuentra vigente, favor de comunicarse con cobranza", "warning", true);
                        return;
                    }
                    buscarOrden(json[0][1], json[0][5], json[0][6], json[0][3], json[0][2]);
                }
            }
            //osDT.rows.add(json).draw();
        }

    });
}



var init_table_servicos = function () {
    if(!allServicesDT){
        allServicesDT = $('#tableToSaveAllServices').DataTable( {
            searching: false,
            bInfo: false,
            paging: false,
            processing: true,
            scrollCollapse: true,
            fixedColumns: true,
            scrollX: false,
            responsive: {
                details: {
                    type: 'column',
                    target: 0
                }
            },
            "columnDefs": [
                { "width": "13%", "targets": 0 },
                { targets: [4,5], visible: false, orderable: false, searchable: false },
                { targets: '_all',  visible: true, orderable: true, searchable: true }
            ],
            colReorder: true,
            select: false,
            language: {
                url: "../../plugins/DataTables/language.MX.json"
            },
            order: [[ 1, "desc" ]],
            "createdRow": function( row, data, dataIndex ) {
                $(row).attr('id', dataIndex);
            }
        });
    }

    allServicesDT.clear().draw();
    allServicesDT.columns.adjust();
    allServicesDT.responsive.recalc();
};

function confirmarRemoverServicio(_this) {
    bootbox.confirm({
        message: "¿Confirma que quiere remover el servicio?",
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if(result) {
                allServicesDT
                    .row( $(_this).parents('tr') )
                    .remove()
                    .draw();
                actualizaCostoTotalServicios();
            }
            $(".bootbox-confirm").on('hidden.bs.modal', function(){
                $("body").addClass("modal-open");
            });

        }
    });
}

function actualizaCostoTotalServicios() {
    var total = 0;
    var total_tiempo = 0;
    allServicesDT.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
        total = total + parseFloat(this.data()[5]);
        total_tiempo = total_tiempo + parseFloat(this.data()[6]);
    });
    var iva = (0.16 * total);
    $('#sumaSubtotalservicios').val(parseFloat(total-iva).toFixed(2));
    $('#sumaIVAservicios').val(parseFloat(iva).toFixed(2));
    $('#sumaTotalservicios').val(total);
    $("#sumaTiemposervicios").val(total_tiempo);
}


/*function mostrarSumaTotal() {
    var totalFinal = 0;
    getServicesDT.column(3).data().$('input').each(function(_this) {
        totalFinal = totalFinal + parseFloat($(this).val());
    });
    $('#fin_sumaTotal_allServicios').val(totalFinal);
}*/

