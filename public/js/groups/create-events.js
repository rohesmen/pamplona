$(document).ready(function () {
	new Switchery(document.getElementById('chkActivo'));
	
	$(".btnSaveGroup").on('click', function(e){
		if(validateForm()){
			var valNombre = $("#txtNombre").val();
			var valDescripcion = $("#txtDescripcion").val();
			var valOrden = $("#txtOrden").val();
			var valActivo = $("#chkActivo").is(":checked");
	
			var data = {
				nombre: 		secureInput(valNombre),
				activo: 		secureInput(valActivo),
				descripcion: 	secureInput(valDescripcion),
				orden: 	secureInput(parseInt(valOrden))
			};

			var bPeticion = false;
			fnAjax("/groups/create", "POST", data, false, function(_json){
	  		if(_json.lError){
		  		showGeneralMessage(_json.cMensaje, "error", true);
		  	}else{
		  		document.location.href = "/groups";
		  	}
	  	});
		}
	});
});