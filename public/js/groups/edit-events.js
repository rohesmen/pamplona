$(document).ready(function(){
	new Switchery(document.getElementById('chkActivo'));
	
	$('.dt-table').DataTable({
		"responsive": true,
		"processing": true,
		"paging":   false,
		"ordering": true,
		"info":     false,
		"autoWidth": false,
		"columnDefs":[{
			"targets": 0,
			"orderable": false,
			"searchable": false,
			"width": "15px"
		}],
		"order": [[ 1, "asc" ]],
		"language": {
			"url": "../../plugins/DataTables/language.MX.json"
		}
	});

    $(".btnEditGroup").on('click', function(e){
		//e.preventDefault();
		if(validateForm()){
			var valId 		 	= parseInt($("#txtClave").val());
			var valNombre = $("#txtNombre").val();
			var valDescripcion = $("#txtDescripcion").val();
			var valOrden = $("#txtOrden").val();
			var valActivo = $("#chkActivo").is(":checked");
			
			var valId 		 	= parseInt($("#txtClave").val());

			var data = {
				id: 				valId,
				nombre: 		secureInput(valNombre),
				activo: 		secureInput(valActivo),
				descripcion: 	secureInput(valDescripcion),
				orden: 	secureInput(parseInt(valOrden))
			};

			fnAjax("/groups/edit/" + valId, "POST", data, false, function(_json){
	  		if(_json.lError){
		  		showGeneralMessage(_json.cMensaje, "error", true);
		  	}else{
		  		document.location.href = "/groups";
		  	}
	  	});
		}
	});
});