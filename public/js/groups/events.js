$(document).ready(function () {
  $(".nav .mainnav-toggle").click();
  $('#treeEstatus').jstree({
		'plugins': ["wholerow", "checkbox"],
		'core': {
			'themes': {
				'name': 'proton',
				'responsive': true
			}
		}
	});

	gruposDT = $('#dt-grupos').DataTable({
      responsive: true,
      processing: true,
      "language": {
        "url": "../../plugins/DataTables/language.MX.json"
      },
      responsive: {
        details: {
          type: 'column',
          target: 1
        }
      },
      "columnDefs": [
        { targets: [0,2], className: "no-wrap text-center", orderable: false, searchable: false },
        { targets: 1, className: 'control', orderable: false, searchable: false, visible: false },
        { targets: [-1, -2],  visible: false },
        { targets: '_all',  visible: true, orderable: true, searchable: true }
      ],
      "dom": 'rtip',
      "order": [[3, "asc"]],
      select: {
        style: 'single'
      }
  });

	gruposDT
    .on( 'select', function ( e, dt, type, indexes ) {
      var rowData = gruposDT.rows( indexes ).data().toArray();
	  
	  console.log(rowData);
	  
	  var activo = rowData[0][8];
	  
      if(activo === "true"){
        $(".grupo-desactivar").prop("disabled", false);
        $(".grupo-desactivar").removeClass('btn-success').addClass('btn-danger');
        $(".grupo-desactivar span").html('Desactivar')
      }else{
        $(".grupo-desactivar").prop("disabled", false);
        $(".grupo-desactivar").removeClass('btn-danger').addClass('btn-success');
        $(".grupo-desactivar span").html('Activar')
      }
    })
    .on( 'deselect', function ( e, dt, type, indexes ) {
      var rowData = gruposDT.rows( indexes ).data().toArray();
      $(".grupo-desactivar").prop("disabled", true);
      
    } );
	
	$("#txtBuscar").on("keyup", function(){
		fnBuscarDT('#dt-grupos', $("#filter-campo").val(), $(this).val());
	});

	$("#btnClear").on("click", function(){
		$('#txtBuscar').val('').focus();
		fnBuscarDT('#dt-grupos', "", '');
	});

	$("#btnBuscar").on("click", function(){
		fnBuscarDT('#dt-grupos', $("#filter-campo").val(), $(this).val());
	});
	
	$("#filter-campo").on("change", function(){
		$('#txtBuscar').val('').focus();
		fnBuscarDT('#dt-grupos', $("#filter-campo").val(), "");
	});

  $("#contenedor-tabla").show();

  $(".btnInfoModal").on("click", function(e){
		e.preventDefault();
		
		var _url = $(this).prop("href");
		fnAjax(_url, "get", {}, false, function(_json){
		  if(_json.lError){
			showGeneralMessage(_json.cMensaje, "error", true);
		  }else{
			$("#ddId").text(_json.resultado.id);
			$("#ddNombre").text(_json.resultado.nombre);
			$("#ddDescripcion").text(_json.resultado.descripcion);
			$("#ddOrden").text(_json.resultado.orden);
			$("#ddFCreacion").text(_json.resultado.fecha_creacion);
			$("#ddActivo").text(_json.resultado.activo);
			$("#btnEditModal").prop("href", "/groups/edit/" + _json.resultado.id);
			$("#info-grupo-modal").modal("show");
		  }
		});
	  });
  
  
  $(".grupo-desactivar").on("click", function () {
    $(".alert-success").css("display", "none");
    var selectedRows = gruposDT.row({ selected: true });
	
	console.log(selectedRows);
	
    var ide = selectedRows.data()[3];
    var activo = selectedRows.data()[8];

    var data = {
      id: ide
    };

    if(activo == "true") {
      var _url = "/groups/deactivate/" + ide;
      var _icono = '<i class="fa fa-times-circle" style="color: red;"></i>';
      var _estado = 'false';
      var _removeClass = "btn-danger";
      var _addClass = "btn-success";
      var _html = "Activar";
    }else{
      var _url = "/groups/activate/" + ide;
      var _icono = '<i class="fa fa-check-circle" style="color: green;"></i>';
      var _estado = 'true';
      var _removeClass = "btn-success";
      var _addClass = "btn-danger";
      var _html = "Desactivar";
    }

    fnAjax(_url, "PUT", data, false, function(_json){
      //console.log(_json);
      if(_json.lError){
        showGeneralMessage(_json.cMensaje, "error", true);
      }else{
        $(".grupo-desactivar").removeClass(_removeClass).addClass(_addClass);
        $(".grupo-desactivar span").html(_html);
        selectedRows.data()[2] = _icono;
        selectedRows.data()[8] = _estado;
        var data = selectedRows.data();
        selectedRows.data(data).draw();
        showGeneralMessage(_json.cMensaje, "success", true);
      }
    });

  });
  
});