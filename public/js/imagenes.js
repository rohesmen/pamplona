Dropzone.autoDiscover = false;
$(document).ready(function () {
    // Identificacion Personal
    rutaDropzone = new Dropzone("div#rutaUpload",{
        url: "/clientes/upload",
        // acceptedFiles: ".xls, .xlsx, .csv",
        acceptedFiles: "image/*,application/pdf",
        maxFiles: 1,
        dictInvalidFileType: "Tipo de archivo no permitido",
        maxFilesize: "10",
        autoProcessQueue: false,
        dictFileTooBig: "Peso máximo: 10M",
        createImageThumbnails: false
    });


    rutaDropzone.on("addedfile", function(file){
        if(rutaDropzone.getQueuedFiles().length > 0){
            rutaDropzone.removeFile(rutaDropzone.getQueuedFiles()[0]);
        }

        $("#imgIden").val(file.name);
    });

    rutaDropzone.on("sending", function(file, xhr, formData){
        formData.append('idcliente', idClienteNuevo);
        formData.append('idtipoiden',$("#ididentificacion").val());
    });

    rutaDropzone.on('success', function (file, response) {
        rutaDropzone.removeAllFiles();
        try {
            if(rutaDropzone0.getQueuedFiles().length > 0) {
                rutaDropzone0.processQueue();
            }
            else if(rutaDropzone2.getQueuedFiles().length > 0) {
                rutaDropzone2.processQueue();
            }
            else {
                clientesDT.draw();
                $("#cliente-info-modal").modal("hide");
            }
        }
        catch (e) {
            showGeneralMessage("Ocurrió un error al guardar la imagen", "error", true);
        }
    });

    rutaDropzone.on('error', function (file, response) {
        rutaDropzone.removeAllFiles();
        showGeneralMessage("Ocurrió un error al guardar la imagen", "error", true);
    });


    //  Comprobante domiciliario
    rutaDropzone0 = new Dropzone("div#rutaUpload0",{
        url: "/clientes/upload",
        // acceptedFiles: ".xls, .xlsx, .csv",
        acceptedFiles: "image/*,application/pdf",
        maxFiles: 1,
        dictInvalidFileType: "Tipo de archivo no permitido",
        maxFilesize: "10",
        autoProcessQueue: false,
        dictFileTooBig: "Peso máximo: 10M",
        createImageThumbnails: false
    });


    rutaDropzone0.on("addedfile", function(file){
        if(rutaDropzone0.getQueuedFiles().length > 0){
            rutaDropzone0.removeFile(rutaDropzone0.getQueuedFiles()[0]);
        }

        $("#imgcomprobante").val(file.name);
    });

    rutaDropzone0.on("sending", function(file, xhr, formData){
        formData.append('idcliente', idClienteNuevo);
        formData.append('idtipocomp',$("#idcomprobante").val());
    });

    rutaDropzone0.on('success', function (file, response) {
        rutaDropzone0.removeAllFiles();

        try {
            var json = JSON.parse(response);
            if(rutaDropzone2.getQueuedFiles().length > 0) {
                rutaDropzone2.processQueue();
            }
            else {
                clientesDT.draw();
                $("#cliente-info-modal").modal("hide");
            }
        }
        catch (e) {
            showGeneralMessage("Ocurrió un error al guardar la imagen", "error", true);
        }
    });

    rutaDropzone0.on('error', function (file, response) {
        rutaDropzone0.removeAllFiles();
        showGeneralMessage("Ocurrió un error al guardar la imagen", "error", true);
    });


    // Fotografia del predio
    rutaDropzone2 = new Dropzone("div#rutaUpload2",{
        url: "/clientes/upload",
        // acceptedFiles: ".xls, .xlsx, .csv",
        acceptedFiles: "image/*,application/pdf",
        maxFiles: 1,
        dictInvalidFileType: "Tipo de archivo no permitido",
        maxFilesize: "10",
        autoProcessQueue: false,
        dictFileTooBig: "Peso máximo: 10M",
        createImageThumbnails: false
    });


    rutaDropzone2.on("addedfile", function(file){
        if(rutaDropzone2.getQueuedFiles().length > 0){
            rutaDropzone2.removeFile(rutaDropzone2.getQueuedFiles()[0]);
        }

        $("#imgfotopredio").val(file.name);
    });

    rutaDropzone2.on("sending", function(file, xhr, formData){
        formData.append('idcliente', idClienteNuevo);
    });

    rutaDropzone2.on('success', function (file, response) {
        rutaDropzone2.removeAllFiles();

        try {
            var json = JSON.parse(response);
            clientesDT.draw();
            $("#cliente-info-modal").modal("hide");
        }
        catch (e) {
            showGeneralMessage("Ocurrió un error al guardar la imagen", "error", true);
        }
    });

    rutaDropzone2.on('error', function (file, response) {
        rutaDropzone2.removeAllFiles();
        showGeneralMessage("Ocurrió un error al guardar la imagen", "error", true);
    });

    $("#btnimgComp").on("click", function(e){
        e.preventDefault();
    });

    $("#btnimgIden").on("click", function(e){
        e.preventDefault();
    });

    $("#btnimgfotopredio").on("click", function(e){
        e.preventDefault();
    });
});