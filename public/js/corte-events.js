validateForm = true;
$(function() {
    
    $("#searchFolio").on("click", function () {
        $("#modalSearchFolio").modal("show");
    });
    $("#modalSearchFolio").on("shown.bs.modal", function () {
        $("#txtFolio").val("");
        $("#modalSearchFolio .row span").html("");
    });

    $("#btnExportCorte").on("click", function () {
        var data = {
            "cC":$('#corte_busqueda_cCobratario option:selected').val(),
            "fI":$('#corte_busqueda_fIni').val(),
            "ff":$('#corte_busqueda_fFin').val(),
            "cT":$('#corte_busqueda_cTipo option:selected').val()
        };
        $.ajax({
            url: "/corte/export",
            type: "POST",
            data: JSON.stringify(data),
            success: function (resp) {
                var json = JSON.parse(resp);
                showGeneralMessage("Los datos han sido exportados correctamente", "success", true);

                var body = [
                    ['Activo', 'Cobratario', 'Monto calculado', 'Monto entregado', 'Fecha inicial', 'Fecha final', 'Usuario creacion',
                        'Fecha creacion', 'Diferencia']
                ];
                $.each(json, function(key, value) {
                    var row = [value.activo ? 'SI' : 'NO',value.cobratario, value.monto_calculado, value.monto_total, value.fecha_inicial, value.fecha_final,
                        value.usuario, value.fecha_creacion, value.diferencia];
                    row.join(',');
                    body.push(row);
                });

                var title = "Cortes";
                var subject = "sheet export";
                var author = "Pamplona";
                var hoja = "Hoja 1";
                var file_name = "cortes";

                exportar_excel(body, title, subject, author, hoja, file_name);
            },
            error: function () {
                showGeneralMessage("Ocurrio un error al obtener la intformacion", "error", true);
            }
        });
    });
    $("#btnExportCortePDF").on("click", function () {
        var fechaInicio = $('#corte_busqueda_fIni').val();
        var fechaFin = $('#corte_busqueda_fFin').val();
        var idCobratario = $('#corte_busqueda_cCobratario option:selected').val();
        var idTipoCliente = $('#corte_busqueda_cTipo option:selected').val();
        if(!idCobratario){
            idCobratario = 0;
        }
        var URLfilt = "/corte/exportpdf/" + fechaInicio + "/" + fechaFin + "/" + idTipoCliente + "/" + idCobratario;
        console.log("fi: " + fechaInicio);
        console.log("ff: " + fechaFin);
        console.log("iC: " + idCobratario);
        console.log("itc: " + idTipoCliente);
        console.log("url: " + URLfilt);

        //window.open("/corte/exportpdf/05-02-2020/05-09-2020/4/0");
        window.open(URLfilt);
        //location.href="/corte/05-02-2020/05-09-2020/4/";
        /*var data = {
            "cC":$('#corte_busqueda_cCobratario option:selected').val(),
            "fI":$('#corte_busqueda_fIni').val(),
            "ff":$('#corte_busqueda_fFin').val(),
            "cT":$('#corte_busqueda_cTipo option:selected').val()
        };
        $.ajax({
            url: "/corte/exportpdf",
            type: "POST",
            data: JSON.stringify(data),
            success: function (resp) {
                //var json = JSON.parse(resp);
                console.log(resp);
                showGeneralMessage("Los datos han sido exportados correctamente", "success", true);
                var blob=new Blob([resp]);
                var link=document.createElement('a');
                link.href=window.URL.createObjectURL(blob);
                link.download="reportecorte.pdf";
                link.click();
            },
            error: function () {
                showGeneralMessage("Ocurrio un error al obtener la intformacion", "error", true);
            }
        });*/
    });
    $("#btnSearchFolio").on("click", function () {
        var folio = $("#txtFolio").val().trim();
        if(folio == ""){
            showGeneralMessage("Debe escribir un folio", "warning", true);
            return;
        }
        $("#modalSearchFolio .row span").html("");
        $.ajax({
            url: "/corte/searchfolio/" + folio,
            success: function (resp) {
                var json = JSON.parse(resp);
                $("#txtNombreFolio").html(json.cliente);
                $("#txtCalleFolio").html(json.calle);
                $("#txtNumeroFolio").html(json.numero);
                $("#txtColoniaFolio").html(json.colonia);
                $("#txtCobratarioFolio").html(json.cobratario);
                $("#txtFechaFolio").html(json.fecha);
                $("#txtMontoFolio").html(json.monto);
                $("#txtCorteFolio").html(json.corte);
                $("#txtMesesFolio").html(json.meses);
            },
            error: function () {
                showGeneralMessage("No se encontraron resultados");
            }
        });
    });
    var dataTable = "";
    var dataTablePagos = "";
    $('#corte_busqueda_fIni').datepicker({
        format: "dd-mm-yyyy",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });
    // datetimepicker({defaultDate: new Date(),format: 'DD-MM-YYYY', maxDate: new Date(), showClose: true});
    $('#corte_busqueda_fFin').datepicker({
        format: "dd-mm-yyyy",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });
    // datetimepicker({defaultDate: new Date(),format: 'DD-MM-YYYY', maxDate: new Date(), showClose: true});
    fnCargaTablaCorte();
    fnCargaTablaCortePagos();
    fnfltrCorte();
    fnConsultaCorte();
    fnEditaCorte();

    $("#corte_busqueda_fIni").on("dp.change", function (e) {
        $('#corte_busqueda_fFin').data("DateTimePicker").minDate(e.date);
    });
    $("#corte_busqueda_fFin").on("dp.change", function (e) {
        $('#corte_busqueda_fIni').data("DateTimePicker").maxDate(e.date);
    });
    $("#corte_add_cCobratario").on("change",function(e){
        cFini = $("#corte_add_fIni").val();
        cFfin = $("#corte_add_fFin").val();
        cCobratario = $("#corte_add_cCobratario").val();
        cTipo = $("#corte_add_cTipo").val();
        $.ajax({
            url: '/corte/firstdate/' + cCobratario,
            success: function (resp) {
                $("#corte_add_fIni").val(resp);
                cFini = $("#corte_add_fIni").val();
                calculaMonto(cFini,cFfin,cCobratario,cTipo);
            }
        });
    });
    $('#corte_add_cTipo').on('change', function() {
        cFini = $("#corte_add_fIni").val();
        cFfin = $("#corte_add_fFin").val();
        cCobratario = $("#corte_add_cCobratario").val();
        cTipo = $("#corte_add_cTipo").val();
        calculaMonto(cFini,cFfin,cCobratario,cTipo);
    });
    $('#corte_add_fIni').datetimepicker({defaultDate: new Date(),format: 'DD-MM-YYYY', maxDate: new Date()});
    $('#corte_add_fFin').datetimepicker({defaultDate: new Date(),format: 'DD-MM-YYYY', maxDate: new Date()});

    $("#corte_add_fIni").on("dp.change", function (e) {
        $('#corte_add_fFin').data("DateTimePicker").minDate(e.date);
        cFini = $("#corte_add_fIni").val();
        cFfin = $("#corte_add_fFin").val();
        cCobratario = $("#corte_add_cCobratario").val();
        cTipo = $("#corte_add_cTipo").val();
        calculaMonto(cFini,cFfin,cCobratario,cTipo);
    });
    $("#corte_add_fFin").on("dp.change", function (e) {
        $('#corte_add_fIni').data("DateTimePicker").maxDate(e.date);
        cFini = $("#corte_add_fIni").val();
        cFfin = $("#corte_add_fFin").val();
        cCobratario = $("#corte_add_cCobratario").val();
        cTipo = $("#corte_add_cTipo").val();
        calculaMonto(cFini,cFfin,cCobratario,cTipo);
    });
});

var fnCargaTablaCorte = function(){
    dataTable = $('#tblCorte').DataTable( {
        "columnDefs": [
            {
            "targets": [ 0 ],
            "searchable": false,
            "orderable": false
            },
            {
            "targets": [ 1 ],
            "searchable": false,
            "orderable": true,
                className: 'control'
            },
            {
            "targets": [ 2 ],
            "bVisible": false,
            "searchable": false,
            "orderable": false
            },
            {
            "targets": [ 3 ],
            "orderable": true,
            "searchable": false
            },
            {
            "targets": [ 4 ],
            "bVisible": false,
            },
            {
            "targets": [ 5 ],
            "searchable": false,
            "orderable": true
            },
            {
            "targets": [ 6 ],
            "searchable": false,
            "orderable": true
            },
            {
            "targets": [ 7 ],
            "searchable": false,
            "orderable": true,
                type: 'date-eu'
            },
            {
            "targets": [ 8 ],
            "searchable": false,
            "orderable": true,
                type: 'date-eu',
                className: "holitas"
            },
            {
            "targets": [ 9 ],
            "searchable": false,
            "orderable": true
            },
            {
            "targets": [ 10 ],
            "searchable": false,
            "orderable": true
            },
            {
            "targets": [ 11 ],
            "bVisible": true,
            },
            {
            "targets": [ 12 ],
            "bVisible": false,
            },
            {
            "targets": [ 13 ],
            "bVisible": false,
            },
            {
            "targets": [ 14 ],
            "bVisible": false,
            }
         ],
        responsive: {
                details: {
                    type: 'column',
                    target: 1
                }
        },
        "bLengthChange": true,
        "processing": true,
        "serverSide": true,
        // "scrollY":        "506px",
        "scrollCollapse": false,
        "pagingType": "full",
        "dom": '<"clear"><rt><"bottom"lip><"clear">',
        "order": [[ 10, 'desc' ]],
        "autoWidth": false,
        "paging": true,
        "info": true,
        "language": {
                "paginate": {
                    "next": ">",
                    "first": "<<",
                    "last": ">>",
                    "previous": "<"
                },
                "search": "",
                "searchPlaceholder": "Buscar en los resultados encontrados",
                "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
                "infoEmpty": "",
                "infoFiltered": " - filtrado de _MAX_",
                "emptyTable": "Sin resultados",
                "sZeroRecords": "Sin resultados",
                processing: "Cargando Cortes",
                "lengthMenu": "Mostrar _MENU_ registros"
            },
        /*"sScrollY" : "200",*/
        "bFilter": false,
        /*"language": {
                        "url": "../controlador/datTab1.10.7/js/espanol.json"},*/
        "ajax":{
            /*data: { "sEle": "cMev", "cM":$('#cM').val()},*/
            data:function ( d ) {
                return $.extend( {}, d, {
                    "cC":$('#corte_busqueda_cCobratario option:selected').val(),
                    "fI":$('#corte_busqueda_fIni').val(),
                    "ff":$('#corte_busqueda_fFin').val(),
                    "cT":$('#corte_busqueda_cTipo option:selected').val()
                });
            },
            url :  '/corte/busca/',
            type: "post",
            error: function(){  // error handling
                $( "#monto-total-label" ).html("$0.00");
                $( "#monto-diferencia-label" ).html("$0.00");
                $( "#monto-entregado-label" ).html("$0.00");
                $('#tblCorte').append('<tbody class="employee-grid-error"><tr><th colspan="3">No se encontró información</th></tr></tbody>');
            }
        },
        "drawCallback": function(settings) {
            // console.log(settings.json);
            var resp = settings.json;
            $( "#monto-total-label" ).html(resp.monto);
            $( "#monto-diferencia-label" ).html(resp.diferencia);
            $( "#monto-entregado-label" ).html(resp.entregado);
            //do whatever
        },
    });

    // dataTable = $('#tblCorte').DataTable();
    $('#tblEventos tbody').on( 'click', 'tr', function () {
        var cE = (dataTable.row( this ).data());
        //fnCargaFormEv(cE);
    });
};

var fnCargaTablaCortePagos = function(){
    dataTablePagos = $('#tblPagos').DataTable( {
        "dom": 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5,6 ]
                },
                text: "Exportar a Excel"
            },
            {
                extend: 'pdf',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5,6 ]
                },
                text: "Exportar a PDF"
            }
        ],
        "columnDefs": [
            {
            "targets": [ 0 ],
            "searchable": false,
            "orderable": false,
            "visible": true
            },
            {
            "targets": [ 1 ],
            "searchable": false,
            "orderable": false,
                "visible": true
            },
            {
            "targets": [ 2 ],
            "searchable": false,
            "orderable": false,
            className: "dt-body-center"
            },
            {
            "targets": [ 3 ],
            "searchable": false,
            "orderable": false,
            className: "dt-body-right"
            },
            {
            "targets": [ 4 ],
            "searchable": false,
            "orderable": false,
            className: "dt-body-center"
            },
            {
                "targets": [ 5 ],
                "searchable": false,
                "orderable": false,
                className: "dt-body-center"
            },
            {
                "targets": [ 6 ],
                "searchable": false,
                "orderable": false,
                className: "dt-body-center"
            }
         ],
        responsive: {
                details: {
                    type: 'column',
                    target: 1
                }
        },
        "bLengthChange": false,
        "bPaginate":false,
        "bServerSide":true,
        "iDeferLoading": 0,
        "processing": true,
        "serverSide": false,
        "scrollY":        "506px",
        "scrollCollapse": false,
        "pagingType": "full",
        "autoWidth": false,
        "paging": false,
        "info": false,
        "language": {
                "search": "",
                "searchPlaceholder": "Buscar en los resultados encontrados",
                "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
                "infoEmpty": "",
                "infoFiltered": " - filtrado de _MAX_",
                "emptyTable": "Sin resultados",
                "sZeroRecords": "Sin resultados",
                processing: "Cargando Pagos",
                "lengthMenu": "Mostrar _MENU_ registros"
            },
        /*"sScrollY" : "200",*/
        "bFilter": false,
        /*"language": {
                        "url": "../controlador/datTab1.10.7/js/espanol.json"},*/
        "ajax":{
            /*data: { "sEle": "cMev", "cM":$('#cM').val()},*/
            data:function ( d ) {
                return $.extend( {}, d, {
                    "id":$('#idBusquedaPagos').val()
                });
            },
            url :  '/corte/pagos/',
            type: "post",
            error: function(){  // error handling
                $('#tblPagos').append('<tbody class="employee-grid-error"><tr><th colspan="3">No se encontró información</th></tr></tbody>');
            }
        }
    });
}

var fnfltrCorte = function(){
    $(".corte-search-do").on('click',function(e){
        dataTable.ajax.reload();
    });
};

var fnConsultaCorte = function(){  
};

var fnEditaCorte = function(){  
};

var deshabilitaForm = function(){
    $("#corte_add_fIni, #corte_add_fFin, #corte_add_cCobratario, #corte_add_cTipo, #corte_add_montoE, #corte_add_montoS, #corte_add_montoP").prop('disabled', true);
};

var habilitaForm = function(){
    $("#corte_add_fFin, #corte_add_cCobratario, #corte_add_cTipo, #corte_add_montoE, #corte_add_montoS, #corte_add_montoP").prop('disabled', false);
};

var reiniciaForm = function(){
    var today = new Date();var dd = today.getDate();var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();
    if(dd<10)
        dd='0'+dd
    if(mm<10)
        mm='0'+mm
    var h = dd+'-'+mm+'-'+yyyy;

    $('#idcorte').val("");
    $('#lP').val("");
    $('#corte_add_fIni').val(h);
    $('#corte_add_fFin').val(h);
    var select = $("#corte_add_cCobratario").selectize();
    var selectize = select[0].selectize;
    selectize.setValue("", false);
    selectize.enable();
    $('#corte_add_cCobratario').val("");
    $('#corte_add_cTipo').val("2");
    /*$('#corte_add_cTipo [value="2"]').prop('checked', true);*/
    $("#corte_add_montoE, #corte_add_montoP, #corte_add_montoS, #corte_add_montoC, #corte_add_diff").val("00.00");
};