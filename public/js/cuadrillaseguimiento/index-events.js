
var isExtern = false;

$(document).ready(function () {

    $(".filtro-cuadrillaseguimiento-clear-do").off("click");
    $(".filtro-cuadrillaseguimiento-clear-do").on("click", function(){
        $(this).next().val("");
    });
    
    $('.Filt').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            $('#cuadrillaseguimiento-search-do').click();
        }
    });    

    $("#cuadrillaseguimiento-search-do").on("click", function () {
        cuadrillaseguimientoDT.column(4).search($("#fcuadrillaseguimiento").val().trim());
        cuadrillaseguimientoDT.column(7).search($("#fvigente").val());
        cuadrillaseguimientoDT.draw();
    });

    $("#cuadrillaseguimiento-datatable").on('click', '.cuadrillaseguimiento-edit', function () {
        var $tr = $(this).closest("tr");
        var data = cuadrillaseguimientoDT.row($tr).data();
        let dataAux = {
            id: data[3],
            nombre: data[4]
        };
        loadCUADSEG(dataAux, false);
    });

    $("#cuadrillaseguimiento-datatable").on( 'click', '.cuadrillaseguimiento-info', function () {
        var $tr = $(this).closest("tr");
        var data = cuadrillaseguimientoDT.row($tr).data();
        let dataAux = {
            id: data[3],
            nombre: data[4]
        };
        loadCUADSEG(dataAux, true);
    });

    $("#cuadrillaseguimiento-datatable").on( 'click', '.cuadrillaseguimiento-delete', function () {
        activate($(this), false);
    });

    $("#cuadrillaseguimiento-datatable").on( 'click', '.cuadrillaseguimiento-active', function () {
        activate($(this), true);
    });

    cuadrillaseguimientoDT = $("#cuadrillaseguimiento-datatable").DataTable({
        "dom": '<"clear"><"top container-float"<"filter-located">f><"datatable-scroll"rt><"bottom"lip><"clear">',
        "autoWidth": false,
        "paging": true,
        "select": true,
        "info": true,
        //searching: true,
        "processing": true,
        "scrollCollapse": false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
        "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
        "pageLength": 10,
        "order": [[ 3, 'asc' ]],
        responsive: {
            details: {
                type: 'column',
                target: 1
            }
        },
        "processing": true,
        "serverSide": true,
        //"deferLoading": 0,
        ajax: {
            url: "/cuadrillaseguimiento/buscar",
        },
        //responsive: true,
        "columnDefs": [
            {
                targets: [0],
                orderable: false,
                searchable: false,
                className: 'dt-center no-wrap',
                visible: coacciones != 'no'
            },
            { targets: [2,3,5,6], className: 'dt-center no-wrap'},
            { targets: [1], orderable: false, className: 'control', searchable: false},
            { targets: [-1], visible: false },
            { targets: '_all', visible: true }
        ],
        "lengthChange": true
    });

});

function activate($this, activo){
    $(".popover.confirmation").confirmation("destroy");
    var id = $this.data("id");
    $this.confirmation({
        rootSelector: "body",
        container: "body",
        singleton: true,
        popout: true,
        btnOkLabel: "Si",
        onConfirm: function() {
            $.ajax({
                url: "/cuadrillaseguimiento/delete/" + id + "/" + activo,
                success: function () {
                    showGeneralMessage("La información se guardo correctamente", "success", true);
                    cuadrillaseguimientoDT.draw(false);
                }
            });
        },
        onCancel: function() {
            $this.confirmation('destroy');
        },
    });
    $this.confirmation("show");
}
