
var deletecuadrillarecoleccion = [];

$(document).ready(function () {

    selectRecolectorCUADSEG = $("#cmbRecolectoCUADSEG").selectize();
    RecolectorCUADSEG = selectRecolectorCUADSEG[0].selectize;
    RecolectorCUADSEG.setValue("", false);

    $("#addCuadrillaSeguimientoDo").on("click", function () {
        $("#cuadrillaseguimiento-info-modal").modal("show");
    });

    $("#btnGuardarCUADSEG").on("click", function () {
        let id = $("#idCUADSEG").val();
        let nombre = $("#txtNombreCUADSEG").val().trim();
        let numerorecolectores = dataTableCuadrillaRecoleccion.rows().count();
        let error = "";
        if(nombre == ""){
            error += "- El nombre no puede estar vacio<br>";
        }
        if(numerorecolectores == 0){
            error += "- Ingrese al menos un recolector<br>";
        }
        let ischofer = false;
        $.each(dataTableCuadrillaRecoleccion.data(), function (index, value) {
            if(value[5] == true || value[5] == "true"){
                ischofer = true;
                return false;
            }
        });
        if(!ischofer){
            error += "- Ingrese un chofer";
        }
        if(error != ""){
            showGeneralMessage(error, "warning", true);
            return
        }
        //console.log(deletecuadrillarecoleccion);
        $.each(dataTableCuadrillaRecoleccion.data(), function (index, value) {
            deletecuadrillarecoleccion.push(value);
        });
        //console.log(deletecuadrillarecoleccion);
        var data = {
            isExtern: isExtern,
            id: id ? id : null,
            nombre: nombre,
            numerorecolectores: numerorecolectores,
            cuadrillarecolector: deletecuadrillarecoleccion
        }
        $.ajax({
            url: "/cuadrillaseguimiento/save",
            type: "POST",
            data: JSON.stringify(data),
            beforeSend: function(){
                $("#btnGuardarCUADSEG").prop("disabled", true);
            },
            success: function (resp) {
                showGeneralMessage("La información se guardo correctamente", "success", true);
                if(!isExtern){
                    cuadrillaseguimientoDT.draw(false);
                }else{
                    let json = JSON.parse(resp);
                    console.log(json);
                    $(".nombrecuadrilla-"+data.id).html(data.nombre);
                    $(".numerocuadrilla-"+data.id).html(data.numerorecolectores);
                    modificaunidades(json);
                }
                $("#cuadrillaseguimiento-info-modal").modal("hide");
            },
            complete: function () {
                $("#btnGuardarCUADSEG").prop("disabled", false);
            }
        })
    });

    $("#create_cuadrilla_recoleccion").on("click", function () {
        createcudrillarecolector();
    });

    $("#cuadrillaseguimiento-info-modal").on("hidden.bs.modal", function () {
        $("#cuadrillaseguimiento-info-modal .modal-title").html("Nueva Cuadrilla");
        $("#idCUADSEG").val("");
        $("#txtNombreCUADSEG").val("");
        RecolectorCUADSEG.setValue("", false);
        $("#chkIsChoferCUADSEG").prop("checked", false);
        $("#txtNombreCUADSEG").prop("disabled", false);
        $("#create_cuadrilla_recoleccion").prop("disabled", false);
        $(".delete_cuadrilla_recoleccion").prop("disabled", false);
        $("#btnGuardarCUADSEG").prop("disabled", false);
        $("#chkIsChoferCUADSEG").prop("disabled", false);
        RecolectorCUADSEG.enable();
        deletecuadrillarecoleccion = [];
        dataTableCuadrillaRecoleccion.clear().draw();
    });

    dataTableCuadrillaRecoleccion = $('#cuadrilla_recoleccion-datatable').DataTable({
        dom: 'rt<"row"<"col-md-6 col-sm-6"i><"col-md-6 col-sm-6"p>>',
        pageLength: -1,
        paging: false,
        info: false,
        responsive: {
            details: {
                type: 'column',
                target: 0
            }
        },
        columnDefs: [
            { targets: 0, className: 'control', orderable: false, searchable: false },
            { targets: 1, orderable: false, searchable: false, className: 'dt-center no-wrap', visible: deletecuadrec },
            { targets: [-1,-2,-3,-4,-5], visible: false },
            { targets: [2], orderable: true },
            { targets: '_all', visible: true, orderable: false, searchable: true }
        ],
        language: {
            url: "../../plugins/datatables_new/language.MX.json"
        },
        "order": [
            [2, "asc"]
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).attr('id', data[4]);
        }
    });

    $("#cuadrilla_recoleccion-datatable").on( 'click', '.delete_cuadrilla_recoleccion', function () {
        $(".popover.confirmation").confirmation("destroy");
        $(this).confirmation({
            rootSelector: "#cuadrillaseguimiento-info-modal",
            container: "#cuadrillaseguimiento-info-modal",
            singleton: true,
            popout: true,
            btnOkLabel: "Si",
            onConfirm: function() {
                //console.log("eliminar");
                let row = dataTableCuadrillaRecoleccion.row($(this).parents('tr'));
                let data = row.data();
                //console.log(data);
                if(parseInt(data[7]) > 0){
                    data[6] = false;
                    deletecuadrillarecoleccion.push(data);
                }
                row.remove().draw();
            },
            onCancel: function() {
                $(this).confirmation('destroy');
            },
        });
        $(this).confirmation("show");
    });

    $("#cuadrillaseguimiento-info-modal").on("shown.bs.modal", function () {
        dataTableCuadrillaRecoleccion.columns.adjust();
    });
    
});

function createcudrillarecolector(){
    //console.log("!--------------------------------------------------------!");
    let idrecolector = $("#cmbRecolectoCUADSEG").val().trim();
    let recolector = $("#cmbRecolectoCUADSEG option:selected").text();
    let ischofer = $("#chkIsChoferCUADSEG").is(":checked");
    //console.log(recolector + " - " + ischofer);
    var error = "";
    if(idrecolector == "" || idrecolector == null){
        error += "- El recolector no puede estar vacio<br>";
    }
    if(error != ""){
        showGeneralMessage(error, "warning", true);
        return
    }
    var add = true;
    $.each(dataTableCuadrillaRecoleccion.rows().data(), function (index, value) {
        if(value[4] == idrecolector){
            add = false
            return false;
        }
    });
    if(!add){
        showGeneralMessage("- Ya existe el recolector.<br>", "warning", true);
    } else{
        $.each(dataTableCuadrillaRecoleccion.rows().data(), function (index, value) {
            if(ischofer == true && value[5] == true){
                add = false
                return false;
            }
        });
        if(!add){
            let confirmationACR = bootbox.confirm({
                title: "Cambiar Chofer",
                message: "Ya existe recolector asignado como chofer,<br>¿Desea cambiar el chofer de la cuadrilla?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> NO'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> SI'
                    }
                },
                callback: function (result) {
                    $(".bootbox-confirm").on('hidden.bs.modal', function () {
                        if(isABootstrapModalOpen()){
                            $("body").addClass("modal-open");
                        }
                        $(".bootbox-confirm").off('hidden.bs.modal');
                    });
                    if (result) {
                        $.each(dataTableCuadrillaRecoleccion.rows().data(), function (index, value) {
                            if(value[5] == true){
                                var data_editdt = dataTableCuadrillaRecoleccion.row('#'+value[4]).data();
                                data_editdt[3] = "RECOLECTOR";
                                data_editdt[5] = false;
                                if(parseInt(data_editdt[7]) > -1){
                                    data_editdt[8] = true;
                                }
                                dataTableCuadrillaRecoleccion.row('#'+value[4]).data(data_editdt);
                            }
                        });
                        dataTableCuadrillaRecoleccion.draw(false);
                        crearcuadrillarecoleccion(idrecolector, recolector, ischofer);
                    }
                }
            });
            confirmationACR.css("z-index","1051");
            confirmationACR.next().css("z-index","1050");
        } else{
            crearcuadrillarecoleccion(idrecolector, recolector, ischofer);
        }
    }
    //console.log("¡--------------------------------------------------------¡");
}

function loadcuadrillarecolector(id, disable){
    $.ajax({
        url: '/cuadrillaseguimiento/get/'+id,
        beforeSend: function(){
            dataTableCuadrillaRecoleccion.clear().draw();
        },
        success: function (resp) {
            var json = JSON.parse(resp);
            //console.log(json);
            dataTableCuadrillaRecoleccion.rows.add(json).draw();
            $(".delete_cuadrilla_recoleccion").prop("disabled", disable);
        }
    });
}

//funcion para abrir modal
function loadCUADSEG(data, modoinfo = false){
    let modo = "Consultar";
    if(modoinfo){
        $("#btnGuardarCUADSEG").prop("disabled", true);
        $("#txtNombreCUADSEG").prop("disabled", true);
        $("#create_cuadrilla_recoleccion").prop("disabled", true);
        RecolectorCUADSEG.disable();
        $("#chkIsChoferCUADSEG").prop("disabled", true);
    } else{
        modo = "Editar";
    }
    $("#cuadrillaseguimiento-info-modal .modal-title").html(modo + " Cuadrilla");
    $("#idCUADSEG").val(data.id);
    $("#txtNombreCUADSEG").val(data.nombre);
    loadcuadrillarecolector(data.id, modoinfo);
    $("#cuadrillaseguimiento-info-modal").modal("show");
}

function crearcuadrillarecoleccion(idrecolector, recolector, ischofer){
    let btn = '<button class="btn btn-sm btn-danger delete_cuadrilla_recoleccion" data-id="-1" title="¿Desea eliminar?"><i class="fa fa-remove"></i></button>';
    let array = ["", btn, recolector, (ischofer == true ? "CHOFER" : "RECOLECTOR"), idrecolector, ischofer, true, -1, false];
    //console.log("!--array--!");
    //console.log(array);
    //console.log("¡--array--¡");
    dataTableCuadrillaRecoleccion.row.add(array).draw(false);
    RecolectorCUADSEG.setValue("", false);
    if($("#chkIsChoferCUADSEG").is(":checked")){
        $("#chkIsChoferCUADSEG").prop("checked", false);
    }
}
