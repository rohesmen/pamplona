$reportsContainer = null;
$(document).ready(function() {
    //$('.reportes-get').off('click');
    //$('.reportes-get').on('click', function(e){
    //    e.preventDefault();
    //    var url = $(this).attr('href');
    //    var name = $(this).attr('data-name');
    //    //getReport2(url, name);
    //    getReport(url, name);
    //});
    //$("#reports-select").off("change");
    //$("#reports-select").on("change", function(){
    //    var idReport = this.value;
    //    if(idReport != ""){
    //        getReport(idReport, $('#distritos-select').val(), $('#poligonos-select').val());
    //    }
    //});

    $("#update-report").off("click");
    $("#update-report").on("click", function(){
        var idReport = $("#reports-select").val();
        if(idReport != ""){
            getReport(idReport);
        }
    });

    reportSelectize = $('#reports-select').selectize();
    reportSelectize.off("change");
    reportSelectize.on("change", function(value){
        var idReport = this.value;
        if(idReport != "") {
            getReport(idReport)
        }
    });
});

function isInt(n)
{
    return n != "" && !isNaN(n) && Math.round(n) == n;
}

function getReport(idReport){
    $.ajax({
        url: "/reports/get/" + idReport,
        type: 'GET',
        beforeSend: function () {
            $("#reports-container-filter").html("");
            $('#reports-container').html('<div style="text-align: center;">' +
                '<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
                '</div>');
        },
        success: function(resp){
            $('#reports-container').html(resp);
            $("#reports-filter-text-do-1").off("click");
            $("#reports-filter-text-do-1").on("click", function(){
                var valor = "";
                var columnFilter = [];

                var $elInput1 = $("#reports-filter-text-1");
                var $elInput2 = $("#reports-filter-text-2");
                var valor1 = secureInput($elInput1.val().trim());
                var valor2 = secureInput($elInput2.val().trim());

                var columnFilter1 = $("#reports-filter-column-select-1").val();
                var columnFilter2 = $("#reports-filter-column-select-2").val();

                var columnFiterOld = $elInput1.attr('data-column');
                if(columnFilter1 != -1 && valor1 != ''){
                    columnFilter.push(columnFilter1);
                    if(columnFiterOld != ''){
                        $reportsDT.columns( columnFiterOld )
                            .search( '' )
                            .draw();
                        $elInput1.attr('data-column', '');
                    }
                    if(isInt(valor1)){
                        valor = "^\\s*"+valor1+"\\s*$";
                    }
                    else{
                        valor = valor1;
                    }

                    $reportsDT.columns( columnFilter1 )
                        .search(valor, true)
                        .draw();
                    if(columnFilter2 != -1 && valor2 != ''){
                        columnFilter.push(columnFilter2);
                        var aux = ""
                        if(isInt(valor2)){
                            valor = "^\\s*"+valor2+"\\s*$";
                        }
                        else{
                            valor = valor2;
                        }
                        valor = valor + aux;
                        $elInput2.attr('data-column', columnFilter2);
                        $reportsDT.columns( columnFilter2 )
                            .search(valor, true)
                            .draw();
                    }
                    $elInput1.attr('data-column', columnFilter1);
                    //$reportsDT.columns( columnFilter )
                    //    .search(valor, true)
                    //    .draw();
                }
            });
            $("#reports-filter-clear-do-1").off("click");
            $("#reports-filter-clear-do-1").on("click", function(){
                var valor ="";
                var $elInput1 = $("#reports-filter-text-1");
                var columnFilter1 = $elInput1.attr('data-column');
                var columnFilter = columnFilter1;
                if(columnFilter1 != ""){
                    valor = "";

                    var $elInput2 = $("#reports-filter-text-2");
                    var valor2 = secureInput($elInput2.val().trim());
                    var columnFilter2 = $("#reports-filter-column-select-2").val();

                    //if(columnFilter2 != -1 && valor2 != ''){
                    //    valor = "^\\s*" + valor2 + "\\s*$";
                    //    columnFilter = columnFilter2;
                    //}
                    $reportsDT.columns( columnFilter )
                        .search( valor, true)
                        .draw();
                    $elInput1.attr('data-column', '');

                    $("#reports-filter-column-select-1").val('-1');
                    $elInput1.val('');
                }
            });

            $("#reports-filter-text-do-2").off("click");
            $("#reports-filter-text-do-2").on("click", function(){
                var valor = "";
                var columnFilter = [];

                var $elInput1 = $("#reports-filter-text-1");
                var $elInput2 = $("#reports-filter-text-2");
                var valor1 = secureInput($elInput1.val().trim());
                var valor2 = secureInput($elInput2.val().trim());

                var columnFilter1 = $("#reports-filter-column-select-1").val();
                var columnFilter2 = $("#reports-filter-column-select-2").val();

                var columnFiterOld = $elInput2.attr('data-column');
                if(columnFilter2 != -1 && valor2 != ''){
                    columnFilter.push(columnFilter2);
                    if(columnFiterOld != ''){
                        $reportsDT.columns( columnFiterOld )
                            .search( '' )
                            .draw();
                        $elInput2.attr('data-column', '');
                    }
                    if(isInt(valor2)){
                        valor = "^\\s*"+valor2+"\\s*$";
                    }
                    else{
                        valor = valor2;
                    }

                    $reportsDT.columns( columnFilter2 )
                        .search(valor, true)
                        .draw();
                    if(columnFilter1 != -1 && valor1 != ''){
                        columnFilter.push(columnFilter1);
                        valor = "^\\s*" + valor1 + "\\s*$|^\\s*" + valor2 + "\\s*$";
                        $elInput1.attr('data-column', columnFilter1);
                        if(isInt(valor1)){
                            valor = "^\\s*"+valor1+"\\s*$";
                        }
                        else{
                            valor = valor1;
                        }

                        $reportsDT.columns( columnFilter1 )
                            .search(valor, true)
                            .draw();
                    }
                    $elInput2.attr('data-column', columnFilter2);
                    //$reportsDT.columns( columnFilter )
                    //    .search(valor, true)
                    //    .draw();
                }
            });
            $("#reports-filter-clear-do-2").off("click");
            $("#reports-filter-clear-do-2").on("click", function(){
                var valor = "";

                var $elInput1 = $("#reports-filter-text-2");
                var columnFilter1 = $elInput1.attr('data-column');
                var columnFilter = columnFilter1;
                if(columnFilter1 != ""){
                    valor = "";

                    var $elInput2 = $("#reports-filter-text-1");
                    var valor2 = secureInput($elInput2.val().trim());
                    var columnFilter2 = $("#reports-filter-column-select-1").val();

                    //if(columnFilter2 != -1 && valor2 != ''){
                    //    valor = "^\\s*" + valor2 + "\\s*$";
                    //    columnFilter = columnFilter2;
                    //}
                    $reportsDT.columns( columnFilter )
                        .search( valor, true)
                        .draw();
                    $elInput1.attr('data-column', '');

                    $("#reports-filter-column-select-2").val('-1');
                    $elInput1.val('');
                }
            });
        },
        /*error: function(){
            $('#reports-container').html("Ocurrió un error al obtener la vista");
        }*/
    });
}