var mymap = null;
var markercob = [];
var markercobseg = null;
var rutacobseg = null;
var url = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
var typeId = 'mapbox.streets';
var attr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';

$(document).ready(function () {
    selectfcobratario = $("#fcobratario").selectize();
    selectizefcobratario = selectfcobratario[0].selectize;
    selectizefcobratario.setValue("", false);

    selectfcobratarioseg = $("#fcobratarioseg").selectize();
    selectizefcobratarioseg = selectfcobratarioseg[0].selectize;
    selectizefcobratarioseg.setValue("", false);

    $('#ffecini').datetimepicker({defaultDate: new Date(),format: 'DD-MM-YYYY', maxDate: new Date(), showClose: true});
    $('#ffecfin').datetimepicker({defaultDate: new Date(),format: 'DD-MM-YYYY', maxDate: new Date(), showClose: true});

    $("#ffecini").on("dp.change", function (e) {
        $('#ffecfin').data("DateTimePicker").minDate(e.date);
    });
    $("#ffecfin").on("dp.change", function (e) {
        $('#ffecini').data("DateTimePicker").maxDate(e.date);
    });

    $("#ffecini").on("blur", function (e) {
        if(this.value == ""){
            $('#ffecini').focus();
        }
    });

    $("#ffecfin").on("blur", function (e) {
        if(this.value == ""){
            $('#ffecfin').focus();
        }
    });

    $("#ftipobusqueda").on("change", function(){
        var valor = this.value;
        $("#filtro-dispositivo").hide();
        // $("#filtro-clave").hide();
        $("#filtro-cobratario").hide();

        $("#fdispositivo").val("");
        selectizefcobratario.setValue("", false);
        $("#fclave").val("");
        $("#filtro-" + valor).show();
    });
    
    $("#btnFiltrarCobs").on("click", function () {
        var cobratario = selectfcobratario.val();
        var dispositivo = $("#fdispositivo").val().trim()
        
        $.ajax({
            url: "/trackcob/buscar?cob=" + cobratario + "&disp=" + dispositivo,
            beforeSend: function(){
                if(markercob.length > 0){
                    $.each(markercob, function(idx, val){
                        if(val.marker) mymap.removeLayer(val.marker);
                    });
                    markercob = [];
                }
                $("#menu-rutas").html("<p class='text-center'>Cargando...</p>");
            },
            success: function (resp) {
                var json = JSON.parse(resp);
                var mainStructure = '';
                if(json.length > 0){
                    for(var i in json){
                        var data = json[i];
                        var btnIrMapa = '';
                        if(data.latitud && data.longitud){
                            var contenido = '<b>Dispositivo:</b> ' + data.dispositivo + '<br>' +
                                '<b>Usuario:</b> ' + data.usuario + '<br>';
                            var marker = new L.marker([data.latitud, data.longitud], {}).addTo(mymap).bindPopup(contenido);
                            data.marker = marker;
                            btnIrMapa = '<button class="btn btn-primary" onclick="irMapaCob('+data.latitud + ',' + data.longitud + ')"><i class="fa fa-map-marker"></i> Ir a mapa</button>';
                        }
                        else data.marker = null;
                        mainStructure += '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<b>Dispositivo:</b> ' + data.dispositivo + '<br>' +
                                '<b>Usuario:</b> ' + data.usuario + '<br>' +
                                btnIrMapa +
                            '</div>' +
                        '</div>';


                        markercob.push(data);
                    }
                }
                else{
                    mainStructure = '<p class="text-center">SIN RESULTADOS</p>';
                }
                $("#menu-rutas").html(mainStructure);
            }
        });
    });

    $('#speedMarkerSeg').on("change", function() {
        if(markercobseg != null)
            markercobseg.setSpeed(this.value);
    });

    $("#btn-Play-Seguimiento").on("click", function() {
        efectoBotonesPlay();
    });

    $("#btn-Pause-Seguimiento").on("click", function() {
        efectoBotonesPlay();
    });

    $("#btn-Search-Seguimiento").on("click", function () {
        var dispositivo = selectfcobratarioseg.val();
        var fecini = $("#ffecini").val();
        var fecfin = $("#ffecfin").val();

        var error = "";
        if(dispositivo == ""){
            error += "- Debe selecionar un dispositivo";
        }

        if(fecini == ""){
            error += "- Debe sele cionar una fecha inicial";
        }

        if(fecfin == ""){
            error += "- Debe sele cionar una fecha final";
        }

        if(error != ""){
            showGeneralMessage(error, "warning", true);
            return;
        }

        var data = {
            dispositivo: dispositivo,
            fecini: fecini,
            fecfin: fecfin
        }
        $.ajax({
            url: "/trackcob/seguimiento",
            data: $.param(data),
            beforeSend: function(){
                $("#cargando-ruras").html("Espere un momento porfavor");
                $("#play-content").hide();
                $("#btn-Play-Seguimiento").show();
                $("#btn-Pause-Seguimiento").hide();
                $("#total-cobros").html("");
                if(markercobseg != null){
                    markercobseg.pause();
                    mymap.removeLayer(markercobseg);
                    mymap.removeLayer(rutacobseg);
                    markercobseg = null;
                    rutacobseg = null;
                }
                cobrosDT.clear().draw();
            },
            success: function (resp) {
                $("#cargando-ruras").html("");
                var data = JSON.parse(resp);
                if (data.data.length > 0) {

                    var arrRutaSeguimiento = [];

                    $("#total-cobros").html(data.cobros.length);
                    cobrosDT.rows.add(data.cobros).draw();
                    for(var i in data.data){
                        var seguimiento = data.data[i];
                        arrRutaSeguimiento.push(new L.LatLng(seguimiento.latitud, seguimiento.longitud));
                    };

                    //Se agrega el icono del camion unidad

                    markercobseg = L.Marker.movingMarker(arrRutaSeguimiento,
                        500000, {autostart: false}).addTo(mymap);
                    rutacobseg = L.polyline(arrRutaSeguimiento, {color: 'green'}).addTo(mymap);

                    // markerBuss.bindPopup('<b>Unidad: ' + _iIdUnidad +'. Ruta: ' + iIdRuta +' </b>', {closeOnClick: false}).openPopup();
                    //
                    // markerBuss.on('move', function () {
                    //     markerBuss.setPopupContent('<b>Ubicación:</b> ' + markerBuss.getLatLng().lat.toFixed(4) + ', ' + markerBuss.getLatLng().lng.toFixed(4), {closeOnClick: false}).openPopup();
                    // });

                    markercobseg.on('end', function() {
                        // markerBuss.bindPopup('<b>Fin del recorrido</b>', {closeOnClick: false})
                        //     .openPopup();

                        if($("#btn-Play-Seguimiento").length)
                            $("#btn-Play-Seguimiento").show();

                        if($("#btn-Pause-Seguimiento").length)
                            $("#btn-Pause-Seguimiento").hide();
                    });
                    $("#btn-Play-Seguimiento").show();
                    $("#play-content").show();
                } else {
                    showGeneralMessage("Sin resultados para mostrar", "warning", true);
                }
            }
        });
    });

    cobrosDT = $('#dt-cobros').DataTable({
        "dom": '<"clear"><rt><"bottom"lip><"clear">',
        "paging": true,
        "lengthChange": false,
        "autoWidth": true,
        "select": false,
        "info": true,
        "searching": false,
        "processing": true,
        "ordering":false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            "processing": "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
        "pageLength": 5,
        "responsive": true,
        "deferLoading": 0,
        "scrollCollapse": true,
        "scrollY":       "40vh",
        "columnDefs": [
            { targets: 0, className: 'control', orderable: false, searchable: false },
            { targets: '_all',  visible: true, orderable: true, searchable: true }
        ],
        "columns": [
            {
                data: null,
                className: "no-wrap text-center", orderable: false, searchable: false,
                render: function( data, type, full, meta ){
                    return '';
                }

            },

            { "name": "calle", 		"data": "calle"},
            { "name": "numero",   		"data": "numero"},
            { "name": "colonia", 	"data": "colonia"},
            { "name": "fecha", 	"data": "fecha"},
        ],
        "drawCallback": function( settings ) {
            var api = this.api();
            api.columns().columns.adjust();
            api.columns().columns.adjust();
        }
    });

    mymap = L.map('map',{zoomControl: false}).setView([20.9784451, -89.6187523], 12);

    //Agregamos un control de escala
    L.control.scale({
        imperial: false
    }).addTo(mymap);

    //agregamos control de zoom
    L.control.zoom({
        position:'topright'
    }).addTo(mymap);

    var sidebar = L.control.sidebar('sidebar').addTo(mymap);

    L.easyPrint({
        title: 'Imprimir Mapa',
        position: 'topright',
        sizeModes: ['Current', 'A4Portrait', 'A4Landscape'],
        filename: 'mapa',
        exportOnly: true,
        hideControlContainer: true
    }).addTo(mymap);

    var capa = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(mymap);
});

function irMapaCob(latitud, longitud){
    mymap.setView([latitud, longitud], 14);
}

function efectoBotonesPlay() {
    if(markercobseg){
        if (markercobseg.isRunning()) {
            $("#btn-Play-Seguimiento").show();
            $("#btn-Pause-Seguimiento").hide();
            markercobseg.pause();
            // markercobseg.setPopupContent('<b>En pausa! </b>', {closeOnClick: false}).openPopup();

        }
        else
        {
            $("#btn-Play-Seguimiento").hide();
            $("#btn-Pause-Seguimiento").show();
            markercobseg.start();
        }
    }

}