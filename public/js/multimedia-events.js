tipoMetoddo = "add";
$(function() {
    multimediDT = $("#multimedia-datatable").DataTable({
        "dom": '<"clear"><rt><"bottom"lip><"clear">',
        "autoWidth": false,
        "paging": true,
        "info": true,
        "processing": true,
        "searching": false,
        "select": true,
        "scrollCollapse": false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
        "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
        "pageLength": 10,
        responsive: {
            details: {
                type: 'column',
                target: 1
            }
        },
        "order": [[ 3, 'desc' ]],
        "processing": true,
        "deferLoading": 0,
        "columnDefs": [
            {
                targets: [0],
                orderable: false,
                searchable: false,
                className: 'control',
            },
            { targets: [1], orderable: false, className: 'dt-center no-wrap', searchable: false},
            { targets: [-1], orderable: false, visible: false, searchable: false},
            { targets: '_all', visible: true, orderable: true, searchable: true }
        ],
    });

    $("#multimedia-datatable").on("click", "tbody .datatable-edit", function () {
        var id = $(this).attr("data-id");
        tipoMetoddo = "edit";
        $.ajax({
            url: "/multimedia/get/" + id,
            success: function (resp) {
                var json = JSON.parse(resp);
                $("#clave").val(json.id);
                $("#nombre").val(json.nombre);
                $("#ruta_hide").val(json.ruta);
                $("#tipo").val(json.tipo);
                if(json.tipo == "video"){
                    $("#containerIMG").html('<video width="400" controls>' +
                    '<source src="img/multimedia/' + json.ruta + '">' +
                        'Your browser does not support HTML5 video.' +
                    '</video>');
                }
                else{
                    $("#containerIMG").html("<img src='img/multimedia/" + json.ruta + "' class='img-responsive' />");
                }
                $("#tipo").prop("disabled", true);
                $("#fecha").val(json.fecha);
                $("#descripcion").val(json.descripcion);
                $("#btnGuardarModal").prop("disabled", false);
                $("#conatainer-dropzone").removeClass("col-md-offset-3");
                $("#conatainer-dropzone").next().show();
                $("#addMultimediaModal").modal("show");
            }
        });
    });

    $("#multimedia-datatable").on("click", "tbody .datatable-info", function () {
        var id = $(this).attr("data-id");
        tipoMetoddo = "info";
        $.ajax({
            url: "/multimedia/get/" + id,
            success: function (resp) {
                var json = JSON.parse(resp);
                $("#clave").val(json.id);
                $("#nombre").val(json.nombre);
                $("#ruta_hide").val(json.ruta);
                $("#tipo").val(json.tipo);
                if(json.tipo == "video"){
                    $("#containerIMG").html('<video width="400" controls>' +
                        '<source src="img/multimedia/' + json.ruta + '">' +
                        'Your browser does not support HTML5 video.' +
                        '</video>');
                }
                else{
                    $("#containerIMG").html("<img src='img/multimedia/" + json.ruta + "' class='img-responsive' />");
                }
                $("#fecha").val(json.fecha);
                $("#descripcion").val(json.descripcion);
                $("#addMultimediaModal").modal("show");

                $("#nombre").prop("disabled", true);
                $("#tipo").prop("disabled", true);
                $("#fecha").prop("disabled", true);
                $("#descripcion").prop("disabled", true);
                $("#btnGuardarModal").prop("disabled", true);
                rutaDropzone.disable();

                $("#conatainer-dropzone").removeClass("col-md-offset-3");
                $("#conatainer-dropzone").next().show();
            }
        });
    });

    $("#multimedia-datatable").on("click", "tbody .datatable-delete", function () {
        var id = $(this).attr("data-id");
        $.ajax({
            url: "/multimedia/deactivate/" + id,
            success: function (resp) {
                search();
                showGeneralMessage("Registro desactivado satisfactoriamente","success", true);
            }
        });
    });

    $("#multimedia-datatable").on("click", "tbody .datatable-active", function () {
        var id = $(this).attr("data-id");
        $.ajax({
            url: "/multimedia/activate/" + id,
            success: function (resp) {
                search();
                showGeneralMessage("Registro activado satisfactoriamente","success", true);
            }
        });
    });

    $("#addMultimediaModal").on("hidden.bs.modal", function () {
        $("#clave").val("");
        $("#nombre").val("");
        $("#tipo").val("noticia");
        $("#fecha").val("");
        $("#descripcion").val("");
        $("#containerIMG").html("<img src='img/no-image.png' />");

        $("#nombre").prop("disabled", false);
        $("#tipo").prop("disabled", false);
        $("#fecha").prop("disabled", false);
        $("#descripcion").prop("disabled", false);
        $("#btnGuardarModal").prop("disabled", false);

        rutaDropzone.enable();
        rutaDropzone.removeAllFiles(true);
        $("#addMultimediaModal").modal("hide");
        $("#conatainer-dropzone").addClass("col-md-offset-3");
        $("#conatainer-dropzone").next().hide();
    });
    
    $("#addMultimediaDo").on("click", function () {
        tipoMetoddo = "add";
        $("#addMultimediaModal").modal("show");
    });

    $("#btnGuardarModal").on("click", function () {
        var clave = $("#clave").val();
        var nombre = $("#nombre").val();
        var tipo = $("#tipo").val();
        var fecha = $("#fecha").val();
        var descripcion = $("#descripcion").val();

        var error = "";
        if(nombre == ""){
            error += "- Nombre es obligatorio <br>";
        }

        if(tipo == ""){
            error += "- Tipo es obligatorio <br>";
        }

        if(fecha == ""){
            error += "- Fecha es obligatorio <br>";
        }

        if(tipoMetoddo == "add" && rutaDropzone.getQueuedFiles().length == 0){
            error += "- La imagen es obligatorio <br>";
        }

        if(error != ""){
            showGeneralMessage(error, "warning", true);
            return;
        }

        if(tipoMetoddo == "add"){
            rutaDropzone.processQueue();
        }
        else{
            if(rutaDropzone.getQueuedFiles().length == 0){
                var data = {
                    clave: clave,
                    nombre: nombre,
                    fecha: fecha,
                    descripcion: descripcion
                };
                $.ajax({
                    url: "/multimedia/save",
                    type: "POST",
                    data: $.param(data),
                    success: function () {
                        $("#addMultimediaModal").modal("hide");
                        showGeneralMessage("Registro guardado satisfactoriamente.", "success", true);
                        search();
                    }
                })
            }
            else{
                rutaDropzone.processQueue();
            }
        }
    });

    $("#btnCancelarModal").on("click", function () {
        $("#addMultimediaModal").modal("hide");
    });

    $(".clientes-search-do").on("click", function () {
        search();
    });

    $("#container-fecha .input-group.date").datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true,
        // language: "es"
    });

    $("#fecha").inputmask({
        mask: "99/99/9999",
        clearIncomplete: true,
        "greedy" : false
    });

    $(".filter-clear-do").on("click", function(){
        $("#filtro-nombre").val("");
    });

    $("#tipo").on("change", function () {
        var tipo = this.value;
        rutaDropzone.destroy();
        if(tipo == "video"){
            initializaDropzone("video");
        }
        else{
            initializaDropzone("image");
        }
    });

    Dropzone.autoDiscover = false;
    initializaDropzone("image");
});

function search(){
    var nombre = $("#filtro-nombre").val();
    var vigente = $("#filtro-vigente").val();
    var data = {
        nombre: nombre,
        vigente: vigente
    }
    $.ajax({
        url: "/multimedia/index",
        data: $.param(data),
        success: function (resp) {
            var json = JSON.parse(resp);
            multimediDT.clear();
            multimediDT.rows.add(json);
            multimediDT.draw();
        }
    });
}

function initializaDropzone(tipo) {
    rutaDropzone = new Dropzone("div#rutaUpload",{
        url: "/multimedia/save",
        // acceptedFiles: ".xls, .xlsx, .csv",
        acceptedFiles: tipo + "/*",
        maxFiles: 1,
        dictInvalidFileType: "Tipo de archivo no permitido",
        maxFilesize: "15",
        dictFileTooBig: "Peso máximo: 15M",
        autoProcessQueue: false
    });

    rutaDropzone.on('maxfilesreached', function() {
        rutaDropzone.removeEventListeners();
    });

    rutaDropzone.on('removedfile', function (file) {
        rutaDropzone.setupEventListeners();
    });

    rutaDropzone.on('error', function (file, error, response) {
        rutaDropzone.removeFile(file);
        if(typeof response !== "undefined") {
            showGeneralMessage(response.statusText, "error", false);
            $("#btnGuardarModal").prop("disabled", false);
            $("#btnCancelarModal").prop("disabled", false);
        }
    });

    rutaDropzone.on('success', function (file, response) {
        $("#dz-remove-btn").prop('disabled', false);
        if(tipoMetoddo == "add"){
            var json = JSON.parse(response);
            var id = json.id;
            var ruta = json.ruta;
            var tipo = json.tipo;
            $("#clave").val(id);
            $("#ruta_hide").val(ruta);
            $("#btnGuardarModal").prop("disabled", false);
            $("#btnCancelarModal").prop("disabled", false);
            rutaDropzone.removeAllFiles(true);
        }
        else{
            var json = JSON.parse(response);
            var ruta = json.ruta;
        }
        $("#addMultimediaModal").modal("hide");
        showGeneralMessage("Registro guardado satisfactoriamente.", "success", true);
        search();
    });

    rutaDropzone.on('complete', function (file, response) {
        $("#dz-remove-btn").prop('disabled', false);
        $("#btnGuardarModal").prop("disabled", false);
        $("#btnCancelarModal").prop("disabled", false);
    });

    rutaDropzone.on("sending", function(file, xhr, formData){
        formData.append("nombre", $("#nombre").val());
        formData.append("clave", $("#clave").val());
        formData.append("tipo", $("#tipo").val());
        formData.append("fecha", $("#fecha").val());
        formData.append("descripcion", $("#descripcion").val());

        $("#btnGuardarModal").prop("disabled", true);
        $("#btnCancelarModal").prop("disabled", true);
    });
}