$(document).ready(function () {
  $(".nav .mainnav-toggle").click();
  $('#treeEstatus').jstree({
		'plugins': ["wholerow", "checkbox"],
		'core': {
			'themes': {
				'name': 'proton',
				'responsive': true
			}
		}
	});

	capasDT = $('#dt-capas').DataTable({
		processing: true,
		"language": {
		"url": "../../plugins/DataTables/language.MX.json"
		},
		"scrollY": "100%",
		"scrollX": false,
		"responsive": true,
		"columnDefs": [
			{ targets: [0,2], className: "no-wrap text-center", orderable: false, searchable: false },
			{ targets: 1, className: 'control', orderable: false, searchable: false, visible: false },
			{ targets: [-1, -2],  visible: false },
			{ targets: '_all',  visible: true, orderable: true, searchable: true }
		],
		"dom": 'rtip',
		"order": [[3, "asc"]],
		select: {
			style: 'single'
		},
		complete: function(){
			setTimeout(function(){
				$("#dt-capas").DataTable().columns.adjust();
			},400);
		}
	});

	capasDT
    .on( 'select', function ( e, dt, type, indexes ) {
      var rowData = capasDT.rows( indexes ).data().toArray();
	  var activo = rowData[0][8];
	  console.log(activo);
      if(activo === "true"){
        $(".capa-desactivar").prop("disabled", false);
        $(".capa-desactivar").removeClass('btn-success').addClass('btn-danger');
        $(".capa-desactivar span").html('Desactivar')
      }else{
        $(".capa-desactivar").prop("disabled", false);
        $(".capa-desactivar").removeClass('btn-danger').addClass('btn-success');
        $(".capa-desactivar span").html('Activar')
      }
    })
    .on( 'deselect', function ( e, dt, type, indexes ) {
      var rowData = capasDT.rows( indexes ).data().toArray();
      $(".capa-desactivar").prop("disabled", true);
      
    } );
	
	$("#txtBuscar").on("keyup", function(){
		fnBuscarDT('#dt-capas', $("#filter-campo").val(), $(this).val());
	});

	$("#btnClear").on("click", function(){
		$('#txtBuscar').val('').focus();
		fnBuscarDT('#dt-capas', "", '');
	});

	$("#btnBuscar").on("click", function(){
		fnBuscarDT('#dt-capas', $("#filter-campo").val(), $(this).val());
	});
	
	$("#filter-campo").on("change", function(){
		fnBuscarDT('#dt-capas', $("#filter-campo").val(), "");
		$('#txtBuscar').val('').focus();		
	});

  $("#contenedor-tabla").show();

  $(".btnInfoModal").on("click", function(e){
		e.preventDefault();
		
		var _url = $(this).prop("href");
		fnAjax(_url, "get", {}, false, function(_json){
		  if(_json.lError){
			showGeneralMessage(_json.cMensaje, "error", true);
		  }else{
			$("#ddId").text(_json.resultado.id);
			$("#ddNombre").text(_json.resultado.nombre);
			$("#ddDescripcion").text(_json.resultado.descripcion);
			$("#ddCapa").text(_json.resultado.capa);
			$("#ddEstilo").text(_json.resultado.estilo);
			$("#ddEstiloEtiqueta").text(_json.resultado.estilo_etiqueta);
			$("#ddModulo").text(_json.resultado.modulo);
			$("#ddServidor").text(_json.resultado.servidor);
			$("#ddCamposInfo").text(_json.resultado.camposinfo);
			$("#ddOrden").text(_json.resultado.orden);
			$("#ddGrupo").text(_json.resultado.grupo_capa);
			$("#ddInicial").text(_json.resultado.inicial);
			$("#ddBase").text(_json.resultado.base);
			$("#ddConEtiqueta").text(_json.resultado.conetiqueta);
			$("#ddGoogle").text(_json.resultado.google);
			$("#ddTematico").text(_json.resultado.tematico);
			$("#ddCql").text(_json.resultado.cql);
			
			$("#ddFCreacion").text(_json.resultado.fecha_creacion);
			$("#ddActivo").text(_json.resultado.activo);
			$("#btnEditModal").prop("href", "/layers/edit/" + _json.resultado.id);
			$("#info-capa-modal").modal("show");
		  }
		});
	  });
  
  
  $(".capa-desactivar").on("click", function () {
    $(".alert-success").css("display", "none");
    var selectedRows = capasDT.row({ selected: true });
    var ide = selectedRows.data()[7];
    var activo = selectedRows.data()[8];

    var data = {
      id: ide
    };

    if(activo == "true") {
      var _url = "/layers/deactivate/" + ide;
      var _icono = '<i class="fa fa-times-circle" style="color: red;"></i>';
      var _estado = 'false';
      var _removeClass = "btn-danger";
      var _addClass = "btn-success";
      var _html = "Activar";
    }else{
      var _url = "/layers/activate/" + ide;
      var _icono = '<i class="fa fa-check-circle" style="color: green;"></i>';
      var _estado = 'true';
      var _removeClass = "btn-success";
      var _addClass = "btn-danger";
      var _html = "Desactivar";
    }

    fnAjax(_url, "PUT", data, false, function(_json){
      //console.log(_json);
      if(_json.lError){
        showGeneralMessage(_json.cMensaje, "error", true);
      }else{
        $(".capa-desactivar").removeClass(_removeClass).addClass(_addClass);
        $(".capa-desactivar span").html(_html);
        selectedRows.data()[2] = _icono;
        selectedRows.data()[8] = _estado;
        var data = selectedRows.data();
        selectedRows.data(data).draw();
        showGeneralMessage(_json.cMensaje, "success", true);
      }
    });

  });
  
});