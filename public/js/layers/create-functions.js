function validateForm(){
	var countErrors = 0;

	$(".tab-content .has-error").removeClass("has-error");

	//validar datos de generales
	$(".tab-content #tab-nuevo :input.save-required").each(function () {
		var $el = $(this);
		var val = secureInput($el.val().trim());
		if(val == ""){
			countErrors++;
			$(this).parents("div.form-group").find("div").addClass("has-error");
		}
	});
	
	$(".tab-content #tab-nuevo .slcSelect.save-required").each(function () {
		var $el = $(this);
		var val = secureInput($el.val().trim());
		if(val == ""){
			countErrors++;
			$(this).parents("div.form-group:first").addClass("has-error");
		}
	});

	if(countErrors > 0){
		showGeneralMessage("Campos marcado con * son obligatorios", "warning", true);
		$("#tab-nuevo").tab("show");
		return false;
	}

	return true;
}

function enableButtons(){
		$("#titulo-acciones button").prop("disabled", false);
		$("#titulo-acciones a").removeClass("disabled");
		$("#btnAddColumnDo").prop("disabled", false);
}
function disableButtons(){
		$("#titulo-acciones button").prop("disabled", true);
		$("#titulo-acciones a").addClass("disabled");
		$("#btnAddColumnDo").prop("disabled", true);
}