$(document).ready(function(){
	new Switchery(document.getElementById('chkActivo'));
	//new Switchery(document.getElementById('chkInicial'));
	new Switchery(document.getElementById('chkConEtiqueta'));
	//new Switchery(document.getElementById('chkGoogle'));
	//new Switchery(document.getElementById('chkTematico'));
	//new Switchery(document.getElementById('chkCql'));

	var objPermisosMod = [];
	var objPerfilesMod = [];
	var objAplicacionesMod = [];

	$('.dt-table').DataTable({
		"responsive": true,
		"processing": true,
		"paging":   false,
		"ordering": true,
		"info":     false,
		"autoWidth": false,
		"columnDefs":[{
			"targets": 0,
			"orderable": false,
			"searchable": false,
			"width": "15px"
		}],
		"order": [[ 1, "asc" ]],
		"language": {
			"url": "../../plugins/DataTables/language.MX.json"
		}
	});

    $("input[name=chkAplicacion]").on("click", function(){
        //alert($(this).is(":checked"));
        var bIgnora = false;
        bSelecAplicacion = $(this).is(":checked");
        iIdAplicacion = parseInt($(this).val());

        $.each(objAplicacionesMod, function(key, val){
			/*alert(key);
			 console.log(val);*/
            if(iIdAplicacion == val.id){
                bIgnora = true;
                val.activo = bSelecAplicacion
            }
        });

        if(!bIgnora)
            objAplicacionesMod.push({"id":iIdAplicacion, "activo":bSelecAplicacion});

        //console.log(objPermisosMod);
    });

    $(".btnEditLayer").on('click', function(e){
		//e.preventDefault();
		if(validateForm()){
			var valId 		 	= parseInt($("#txtClave").val());
			var valNombre = $("#txtNombre").val();
			var valDescripcion = $("#txtDescripcion").val();
			var valCapa = $("#txtCapa").val();
			var valEstilo = $("#txtEstilo").val();
			//var valServidor = $("#txtServidor").val();
			//var valModulo = $("#txtModulo").val();
			//var valCamposInfo = $("#txtCamposInfo").val();
			var valOrden = $("#txtOrden").val();
			var valGrupo = $("#slcGrupos").val();
			var valEsquema = $("#txtEsquema").val();
			var valTabla = $("#txtTabla").val();
			var valEstiloEtiqueta = $("#txtEstiloEtiqueta").val();
			
			var valActivo = $("#chkActivo").is(":checked");
			//var valInicial = $("#chkInicial").is(":checked");
			var valConEtiqueta = $("#chkConEtiqueta").is(":checked");
			//var valGoogle = $("#chkGoogle").is(":checked");
			//var valTematico = $("#chkTematico").is(":checked");
			//var valCql = $("#chkCql").is(":checked");
			
			var valId 		 	= parseInt($("#txtClave").val());

			var data = {
				id: 				valId,
				nombre: 		secureInput(valNombre),
				//inicial: 		secureInput(valInicial),
				activo: 		secureInput(valActivo),
				estilo_etiqueta: 	valEstiloEtiqueta,
				conetiqueta: 	valConEtiqueta,
				//google: 		secureInput(valGoogle),
				//tematico: 		secureInput(valTematico),
				//cql: 		valCql,
				con_etiqueta: 		secureInput(valConEtiqueta),
				descripcion: 	secureInput(valDescripcion),
				capa: 	valCapa,
				estilo: 	valEstilo,
				//servidor: 	secureInput(valServidor),
				//modulo: 	secureInput(valModulo),
				//campos_info: 	secureInput(valCamposInfo),
				orden: 	secureInput(parseInt(valOrden)),
				esquema: 	secureInput(valEsquema),
				tabla: 	secureInput(valTabla),
				grupo_capa: 	secureInput(parseInt(valGrupo))
			};

			fnAjax("/layers/edit/" + valId, "POST", data, false, function(_json){
	  		if(_json.lError){
		  		showGeneralMessage(_json.cMensaje, "error", true);
		  	}else{
		  		document.location.href = "/layers";
		  	}
	  	});
		}
	});
});