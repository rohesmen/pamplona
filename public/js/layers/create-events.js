$(document).ready(function () {
	new Switchery(document.getElementById('chkActivo'));
	//new Switchery(document.getElementById('chkInicial'));
	new Switchery(document.getElementById('chkConEtiqueta'));
	//new Switchery(document.getElementById('chkGoogle'));
	//new Switchery(document.getElementById('chkTematico'));
	//new Switchery(document.getElementById('chkCql'));

	$(".btnSaveLayer").on('click', function(e){
		if(validateForm()){
			var valNombre = $("#txtNombre").val();
			var valDescripcion = $("#txtDescripcion").val();
			var valCapa = $("#txtCapa").val();
			var valEstilo = $("#txtEstilo").val();
			//var valServidor = $("#txtServidor").val();
			//var valModulo = $("#txtModulo").val();
			//var valCamposInfo = $("#txtCamposInfo").val();
			var valEsquema = $("#txtEsquema").val();
			var valTabla = $("#txtTabla").val();
			var valOrden = $("#txtOrden").val();
			var valGrupo = $("#slcGrupos").val();
			var valEstiloEtiqueta = $("#txtEstiloEtiqueta").val();
			
			var valActivo = $("#chkActivo").is(":checked");
			//var valInicial = $("#chkInicial").is(":checked");
			var valConEtiqueta = $("#chkConEtiqueta").is(":checked");
			//var valGoogle = $("#chkGoogle").is(":checked");
			//var valTematico = $("#chkTematico").is(":checked");
			//var valCql = $("#chkCql").is(":checked");

			var data = {
				nombre: 		secureInput(valNombre),
				//inicial: 		secureInput(valInicial),
				activo: 		secureInput(valActivo),
				estilo_etiqueta: 	valEstiloEtiqueta,
				conetiqueta: 	valConEtiqueta,
				//google: 		secureInput(valGoogle),
				//tematico: 		secureInput(valTematico),
				//cql: 		secureInput(valCql),
				descripcion: 	secureInput(valDescripcion),
				capa: 	valCapa,
				estilo: 	valEstilo,
				//servidor: 	secureInput(valServidor),
				//modulo: 	secureInput(valModulo),
				//campos_info: 	secureInput(valCamposInfo),
				esquema: 	secureInput(valEsquema),
				tabla: 	secureInput(valTabla),
				orden: 	secureInput(parseInt(valOrden)),
				grupo_capa: 	secureInput(parseInt(valGrupo))
			};

			var bPeticion = false;
			fnAjax("/layers/create", "POST", data, false, function(_json){
	  		if(_json.lError){
		  		showGeneralMessage(_json.cMensaje, "error", true);
		  	}else{
		  		document.location.href = "/layers";
		  	}
	  	});
		}
	});
});