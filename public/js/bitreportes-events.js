$(function() {
    $('.input-group.date.fInicio').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });
	
	$('.input-group.date.fFin').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true
    });
	
	setTimeout(function(){
		var today =  new Date();
		var endDate = sumarDias(new Date(), 7);
		$('.input-group.date.fInicio').datepicker('setDate', today);
		changeDate(today, endDate, new Date());
	},1000);
	
	
	$("#fInicio").on("change", function(){
		var dateSelected = $('.input-group.date.fInicio').datepicker('getDate');
		var endDate = sumarDias($('.input-group.date.fInicio').datepicker('getDate'), 7);
		changeDate(dateSelected, endDate, sumarDias($('.input-group.date.fInicio').datepicker('getDate'), 7));
	});
	
    $("#consultarBitacora").off("click");
    $("#consultarBitacora").on("click", function(){
		var startDate = $('#fInicio').val();
        var endDate = $('#fFin').val();
        var idTipo = $("#sTipoData").val();
        var tipo = $("#sTipo").val();

        $("#menu-dias").empty();
		$("#tab-contenido").empty();
		if(startDate && endDate){
			$.ajax({
				url: "/bitreportes/bitacora",
				data: JSON.stringify({
					fechaI: startDate,
                    fechaF: endDate,
					tipo: tipo,
					idTipo: idTipo
				}),
				type: "POST",
				beforeSend: function(){
					$('#bitacora-datatable-container').html('<div style="text-align: center;">' +
						'<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
						'</div>');
				},
				success: function (resp) {
					var json = JSON.parse(resp);
                   
					for(var i in json.bitacora){
                        var bitacora = json.bitacora[i];
                        var classActive = "";
                        var idContainer = "container_"+i;

                        if(i == 0){
                            classActive = "active";
                        }

                        $("#menu-dias").append('<li class='+classActive+'><a data-toggle="tab" href="#'+idContainer+'">'+bitacora.dia+'</a></li>');

                        var div = '<div id='+idContainer+' class="tab-pane fade in '+classActive+'"></div>';
                        $("#tab-contenido").append(div);


                        var table = '<table id="'+idContainer+'_table" class="dt display table-striped table-hover" style="width: 100% !important;">'+
                            '<thead>'+
                                '<tr>'+
                                    '<th></th>'+
									'<th>Fecha</th>'+
                                    '<th>Unidad</th>'+
                                    '<th>Turno</th>'+
                                    '<th>Hora Salida</th>'+
                                    '<th>Hora Regreso</th>'+
                                    '<th>Total Horas</th>'+
                                    '<th>Kilometraje</th>'+
                                    '<th>Vueltas Relleno</th>'+
                                    '<th>Tiempo Muerto</th>'+
                                    '<th>Supervisor</th>'+
                                    '<th>Coordinador</th>'+
                                    '<th>Chofer</th>'+
                                    '<th>Recolector 1</th>'+
                                    '<th>Recolector 2</th>'+
                                    '<th>Recolector 3</th>'+
                                    '<th>Usuario</th>'+
                                    '<th>Observaciones</th>'+
                                '</tr>'+
                            '</thead>'+
                        '<tbody>';
                        $.each(bitacora.bitacora, function (index, val) {
                            table += '<tr style="background: '+val.color+' !important; color:#000000 !important; width: 100% !important;">'+
                                        '<td></td>'+
										'<td>'+val.fecha+'</td>'+
                                        '<td>'+val.unidad+'</td>'+
                                        '<td>'+val.turno+'</td>'+
                                        '<td>'+val.horasalida+'</td>'+
                                        '<td>'+val.horaregreso+'</td>'+
                                        '<td>'+val.totalhoras+'</td>'+
                                        '<td>'+val.kilometraje+'</td>'+
                                        '<td>'+val.vueltasrelleno+'</td>'+
                                        '<td>'+val.tiempomuerto+'</td>'+
                                        '<td>'+val.supervisor+'</td>'+
                                        '<td>'+val.coordinador+'</td>'+
                                        '<td>'+val.chofer+'</td>'+
                                        '<td>'+val.recolector1+'</td>'+
                                        '<td>'+val.recolector2+'</td>'+
                                        '<td>'+val.recolector3+'</td>'+
                                        '<td>'+val.usuario+'</td>'+
                                        '<td>'+val.observaciones+'</td>'+
                                    '</tr>';
                        });
                        table += '</tbody></table>';
                        $("#"+idContainer).append(table);
						var t = $("#"+idContainer+"_table").DataTable({
							"dom": 'rti',
							"language": {
								"info": "Registros totales: _TOTAL_",
								"infoEmpty": "",
								"emptyTable": "Sin resultados",
								"sZeroRecords": "Sin resultados",
								processing: "Procesando ..."
							},
							"processing": true,
							"scrollX": "250px",
							"autoWidth": false,
							"columnDefs": [
								{ targets: [0], orderable: false, searchable: false, visible: false},
								{targets: [1], responsivePriority: 1},
								{targets: [2], responsivePriority: 2},
								{targets: [3], responsivePriority: 3},
								{targets: [10], responsivePriority: 4, className: "no-wrap text-center"},
								{targets: [11], responsivePriority: 5, className: "no-wrap text-center"},
								{targets: [12], responsivePriority: 6, className: "no-wrap text-center"},
								{ targets: '_all',  visible: true, orderable: false, searchable: false }
							],
							"fnDrawCallback": function( oSettings ) {
								if(t){
									t.columns.adjust();
								}
							}
						});
                    }

				},
				error: function(){
					showGeneralMessage("Ocurrió un error al consultar la bitácora", "error", true);
				},
				complete: function () {
					$('#bitacora-datatable-container').empty();
				}
			});
		}
    });

	$("#sTipo").on("change", function(e, v){
		if($(this).val() != "Todos"){
			setSelect($(this).val());
			$("#option-select").show();
		}else{
			$("#sTipoData").empty();
			$("#option-select").hide();	
		}
		
	});
});

function changeDate(start, end, today){
	$('.input-group.date.fFin').datepicker('setStartDate',  start);
	$('.input-group.date.fFin').datepicker('setEndDate', end);
	$('.input-group.date.fFin').datepicker('setDate', today);
}

/* Función que suma o resta días a una fecha, si el parámetro
   días es negativo restará los días*/
function sumarDias(fecha, dias){
  fecha.setDate(fecha.getDate() + dias);
  return fecha;
}

function setSelect(type){
    $.ajax({
        url: "/bitreportes/getdata",
        data: JSON.stringify({type:type}),
        type: "POST",
        beforeSend: function(){
            $('#bitacora-datatable-container').html('<div style="text-align: center;">' +
                '<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
                '</div>');
        },
        success: function (resp) {
            var resp2 = JSON.parse(resp);
            if(resp2.ok){
            	$("#sTipoData").empty();
                $.each(resp2.data, function (i, val) {
                    $("#sTipoData").append("<option value='"+val.id+"'>"+val.nombre+"</option>");
                });
            }else{
                showGeneralMessage("Ocurrió un error al consultar los datos", "error", true);
            }
        },
        error: function(){
            showGeneralMessage("Ocurrió un error al consultar los datos", "error", true);
        },
        complete: function () {
            $('#bitacora-datatable-container').empty();
        }
    });
}