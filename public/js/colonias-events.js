// script colonias - russel
coloniasDT = null;
selectize = null;
selectize2 = null;
$(document).ready(function() {
    initModalColonia();

    var $select2 = $('#municipio-select-filt').selectize({
		create: false,
		sortField: 'text'
    });

    selectize2 = $select2[0].selectize;

    selectize2.setValue(50, false);


    var $select = $('#municipio-select').selectize({
		create: false,
		sortField: 'text'
    });

    selectize = $select[0].selectize;
    
    $(".btn-colonia-modal-close").off("click");
    $(".btn-colonia-modal-close").on("click", function(){
        $("#colonia-info-modal").modal('hide');
        if(coloniasDT != null)
            coloniasDT.$('tr.selected').removeClass('selected');
    });

    //coloniasDT = null;
    $("#colonias-search-do").off('click');
    $("#colonias-search-val").off('keypress');
    $("#colonias-search-do").on('click', function(){

        buscarColonias($('#colonias-search-val').val());
        
    });

    $("#colonias-search-do2").off('click');
    $("#colonias-search-do2").on('click', function(){

        //console.log( selectize2.getValue() + 'search' );
        buscarColonias(selectize2.getValue());

    });

    $("#colonias-search-val").on('keypress', function(e){
        if(e.which == 13){
            var valor = this.value;
            buscarColonias(valor);
        }
    });

    $('#colonias-filter-column-select').on('change',function() {
        var value = $("#colonias-filter-column-select").val();
        var select = $('#colonias-search-val');
        var prefix = "Ej. ";
        switch (value){
            case 'cla':

                $("#filt2").css("display", "none");
                $("#filt1").css("display", "block");
                select.val('');
                select.attr("placeholder",prefix+'11');
                
                break;
            case 'nom':

                $("#filt2").css("display", "none");
                $("#filt1").css("display", "block");
                select.val('');
                select.attr("placeholder",prefix+'FRACC. HACIENDA INN');

                break;
            case 'loc':

                $("#filt2").css("display", "none");
                $("#filt1").css("display", "block");
                select.val('');
                select.attr("placeholder",prefix+'MERIDA');

                break;
            case 'mun':

                $("#filt1").css("display", "none");
                $("#filt2").css("display", "block");

                break;
            case '-1':

                $("#filt2").css("display", "none");
                $("#filt1").css("display", "block");
                select.val('');
                select.attr("placeholder",'');

                break;
        }
    });

    $("#colonias-filter-clear-do").off("click");
    $("#colonias-filter-clear-do").on("click", function(){
        $('#colonias-search-val').val('');
    });
    
    /**
     * [description]
     * @return {[type]}   [description]
     */
    $("#openNewColonia").on('click', function() {
        loadNewColonia();
        $("#colonia-info-modal").modal('show');
        $('.modal-title').html('Nueva colonia');
    });

    // unidad habi solo numeros
    // $("#colonia-info-idunidadhabi").keyup(function () {
    //     this.value = (this.value).replace(/[^0-9]/g, '');
    // });


}); // .\end $document ready


var buscarColonias = function(valor){
    var valor = secureInput(valor.trim());
    var columnFilter = $("#colonias-filter-column-select").val();
    var valido = true;
    if(columnFilter == 'cla') {
        valido = validarSiNumero(valor);
    }
    if(columnFilter == 'nom' || columnFilter == 'loc') {
        valido = validarSiLetras(valor);
    }
    if(columnFilter == 'mun'){
        if(selectize2.getValue() == "" || selectize2.getValue() == null){
            valido = false;
            toastr.warning('Debe de ingresar un municipio');
        } else{
            valido = true;
        }
    }

    if(valido) {
        if(columnFilter == -1 || (columnFilter != -1 && valor != '')){
            var data = {
                valor: valor,
                filtro: columnFilter
            }
            $.ajax({
                url: '/colonias/search',
                data: JSON.stringify(data),
                type: 'POST',
                beforeSend: function(){
                    $('#colonias-datatable-container').html('<div style="text-align: center;">' +
                        '<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
                        '</div>');
                },
                success: function(html){
                    $('#colonias-datatable-container').html(html);
                }
            });
        }
    }
};

var validarSiNumero = function(numero){
    var v = false;
    if(!/^([0-9])*$/.test(numero)){
        toastr.warning('El valor: ' + numero + ' no es un número');
    }else{
        v = true;
    }
    return v;
};

var validarSiLetras = function(letra) {
    var v=false;
    if(!/^([0-9])*$/.test(letra)){
        //es letra
        v=true;
    }else{
        //es num
        v=false;
        toastr.warning('El valor: ' + letra + ' no es una cadena.');
    }
    return v;
}

var initModalColonia = function() {
    /*$('#colonia-info-monto').inputmask('999999999.99', {
        numericInput: true,
        "placeholder": "0",
        showMaskOnHover: false,
        greedy: false
    });*/
    $("#colonia-info-monto").inputmask('Regex', {regex: "^[0-9]{1,9}(\\.\\d{1,2})?$"});
    loadEventosModalColonia();
};

var loadNewColonia = function() {
    $(".ocultar-moral").hide();
    $indexcolonia = null;
    setModeColoniaInfo('n');
    clearColoniaInfo();
    loadDefaultColoniaInfo();
    showMessageModalColonia("Datos requeridos <span class='colonia-required'>marcados</span>", false);
};

var loadReadColonia = function($indexcolonia) {
    var callback = function() {
        setModeColoniaInfo('r');
    };
    selectTableRow($indexcolonia);
    loadEventsBeforeNextColonia($indexcolonia);
    loadColoniaInfo($indexcolonia, callback);
};

var openReadColonia = function($indexcolonia) {
    var callback = function() {
        setModeColoniaInfo('r');
        if(!$("#colonia-info-activo").is(":checked")){
            // $("#colonia-reset-password-do").hide();
        }
        $("#colonia-info-modal").modal('show');
    };
    selectTableRow($indexcolonia);
    loadEventsBeforeNextColonia($indexcolonia);
    loadColoniaInfo($indexcolonia, callback);
};

var openEditColonia = function($indexcolonia) {
    var callback = function() {
        showMessageModalColonia("Datos requeridos <span class='colonia-required'>marcados</span>", false);
        $("#colonia-info-modal").modal('show');
        /*if(!$("#colonia-info-activo").is(":checked")){}*/
    };
    selectTableRow($indexcolonia);
    loadEventsBeforeNextColonia($indexcolonia);
    loadColoniaInfo($indexcolonia, callback);
};

var deleteColonia = function($tr, $indexcolonia){
    $.ajax({
        url: "/colonias/delete/" + $indexcolonia,
        success: function(){
            showGeneralMessage("La información se guardo correctamente", "success", true);
            coloniasDT.cell( $tr, 1 ).data( '<i class="fa fa-remove" style="color: red" title="Activo"><\/i>' );
            coloniasDT.draw();
        },
        /*error: function(){
            showGeneralMessage("Ocurrió un error al guardar la información", "error", true);
        }*/
    });
};

var selectTableRow = function($indexcolonia) {
    var $row = $("#name-colonia-"+$indexcolonia).closest('tr');
    if ( $row.hasClass('selected') ) {
        $row.removeClass('selected');
    }
    else {
        coloniasDT.$('tr.selected').removeClass('selected');
        $row.addClass('selected');
    }
}

var loadEventosModalColonia = function() {
    $(".btn-colonia-modal-close").off("click");
    $(".btn-colonia-modal-close").on("click", function(){
        $("#colonia-info-modal").modal('hide');
    });

    $('#colonia-info-modal').on('hidden.bs.modal', function (e) {
        if(coloniasDT != null)
            coloniasDT.$('tr.selected').removeClass('selected');
    });
    $('#colonia-info-modal').on('shown.bs.modal', function (e) {
        $("#colonia-info-nombre").focus();
    });

    $(".colonia-tool-new").off('click');
    $(".colonia-tool-new").on('click', function() {
        loadNewColonia();
    });

    $(".colonia-tool-edit").off('click');
    $(".colonia-tool-edit").on('click', function() {
        setModeColoniaInfo('w');
        // if(!$("#colonia-info-activo").is(":checked")){}
        showMessageModalColonia("Datos requeridos <span class='colonia-required'>marcados</span>", false);
    });
    $(".colonia-tool-save").off('click');
    $(".colonia-tool-save").on('click', function() {
        if(validateDataColoniaInfo()) {
            saveDataColoniaInfo(true);
        }
        else {
            showErrorModalColonia("Campos requeridos vacíos y/o formato de datos incorrectos");
        }
    });
    $(".colonia-tool-cancel").off('click');
    $(".colonia-tool-cancel").on('click', function() {
        cancelSaveDataColoniaInfo();
    });
};

var loadEventsBeforeNextColonia = function($indexcolonia) {
    if($indexcolonia == null || $indexcolonia == undefined) {
        $(".btn-before-colonia").off('click');
        $(".btn-next-colonia").off('click');
        return;
    }
    var $row = $("#name-colonia-"+$indexcolonia).closest('tr');
    var idx = coloniasDT.row( $row ).index();
    var indexes = coloniasDT.rows( {order:'current', search:'applied'})[0];
    var beforeidx = null;
    var nextidx = null;
    var currentrowidx = idx;
    for(index in indexes) {
        var rowidx = indexes[index];
        if(rowidx == idx) {
            currentrowidx = parseInt(index);
            if(indexes[parseInt(index)-1] != undefined) {
                beforeidx = indexes[parseInt(index)-1];
            }
            if(indexes[parseInt(index)+1] != undefined) {
                nextidx = indexes[parseInt(index)+1];
            }
            break;
        }
    }
    var pageinfo = coloniasDT.page.info();
    $(".btn-before-colonia").off('click');
    if(beforeidx != null) {
        var beforeIndexColonia = $(coloniasDT.row(beforeidx).node())[0].getAttribute("data-index");
        $(".btn-before-colonia").on('click', function() {
            if(currentrowidx == pageinfo.start) {
                coloniasDT.page('previous').draw('page');
            }
            loadReadColonia(beforeIndexColonia);
        });
        $(".btn-before-colonia").removeClass("disabled");
    } else {
        if(!$(".btn-before-colonia").hasClass("disabled")) {
            $(".btn-before-colonia").addClass("disabled");
        }
    }
    $(".btn-next-colonia").off('click');
    if(nextidx != null) {
        var nextIndexColonia = $(coloniasDT.row(nextidx).node())[0].getAttribute("data-index");
        $(".btn-next-colonia").on('click', function() {
            if(currentrowidx == (pageinfo.end-1)) {
                coloniasDT.page('next').draw('page');
            }
            loadReadColonia(nextIndexColonia);
        });
        $(".btn-next-colonia").removeClass("disabled");
    } else {
        if(!$(".btn-next-colonia").hasClass("disabled")) {
            $(".btn-next-colonia").addClass("disabled");
        }
    }
};

var setModeColoniaInfo = function($mode) {
    var mode = $mode;
    $("#colonia-info-clave").attr("disabled", true);
    $("#colonia-info-nombre").attr("disabled", true);
    switch($mode) {
        case 'n':
            $("#colonia-info-nombre").removeAttr("disabled");
            $("#colonia-info-alias").removeAttr("disabled");
            $("#colonia-info-localidad").removeAttr("disabled");
            // $("#colonia-info-idunidadhabi").removeAttr("disabled");
            $("#colonia-info-monto").removeAttr("disabled");
            $("#colonia-info-activo").removeAttr("disabled");
            //$("#municipio-select").removeAttr("disabled");
            selectize.enable();

            $(".colonia-toolbar-read").hide();
            $(".colonia-toolbar-edit").show();

            //console.log('n');
            selectize.setValue("50", false);
            break;
        case 'w':
            //$("#colonia-info-nombre").removeAttr("disabled");
            $("#colonia-info-alias").removeAttr("disabled");
            $("#colonia-info-localidad").removeAttr("disabled");
            // $("#colonia-info-idunidadhabi").removeAttr("disabled");
            $("#colonia-info-monto").removeAttr("disabled");
            $("#colonia-info-activo").removeAttr("disabled");
            //$("#municipio-select").removeAttr("disabled");
            selectize.enable();

            $(".colonia-toolbar-read").hide();
            $(".colonia-toolbar-edit").show();

            //console.log('w');
            break;
        case 'r':
        default:
            //$("#colonia-info-nombre").attr("disabled", true);
            $("#colonia-info-alias").attr("disabled", true);
            $("#colonia-info-localidad").attr("disabled", true);
            // $("#colonia-info-idunidadhabi").attr("disabled", true);
            $("#colonia-info-monto").attr("disabled", true);
            $("#colonia-info-activo").attr("disabled", true);
            //$("#municipio-select").attr("disabled", true);
            selectize.disable();

            $(".colonia-toolbar-edit").hide();
            $(".colonia-toolbar-read").show();

            //console.log('r');
            break;
    }
};

var loadColoniaInfo = function($indexcolonia, callback) {
    if($indexcolonia == null) {
        return;
    }
    $.ajax({
        url: "/colonias/get/" + $indexcolonia,
        type: 'GET',
        success: function(data){
            $data = jQuery.parseJSON(data);

            setModeColoniaInfo('w');
            clearColoniaInfo();

            setColoniaInfo($indexcolonia, $data);

            if(callback) {
                callback();
            }
        },
        /*error: function(){
        }*/
    });
};

var cancelSaveDataColoniaInfo = function() {
    var callback = function() {
        setModeColoniaInfo('r');
    };
    if($indexcolonia != undefined && $indexcolonia != null) {
        loadColoniaInfo($indexcolonia, callback); //pendiente
    } else {
        clearColoniaInfo();
        setModeColoniaInfo('r');
    }
    if(!$("#colonia-info-activo").is(":checked")){
        // $("#colonia-reset-password-do").hide(); //no va
    }
};

var setColoniaInfo = function($indexcolonia, $data) {
    $("#colonia-info-clave").val($indexcolonia);
    $("#colonia-info-nombre").val(formatValue($data["nombre"]));
    $("#colonia-info-alias").val(formatValue($data["alias"]));
    $("#colonia-info-localidad").val(formatValue($data["localidad"]));
    // $("#colonia-info-idunidadhabi").val(formatValue($data["idunidadhabi"]));
    $("#colonia-info-monto").val(formatValue($data["monto"]));
    //console.log($data["idmunicipio"]);
    selectize.setValue($data["idmunicipio"], false);
    $("#colonia-info-activo").prop("checked", true);
    if(!$data["activo"]){
        $("#colonia-info-activo").prop("checked", false);
    }
};

var saveDataColoniaInfo = function(overwritteAddress) {
    var nombre = getTextValueUpper($("#colonia-info-nombre").val());
    var alias = getTextValueUpper($("#colonia-info-alias").val());
    var localidad = getTextValueUpper($("#colonia-info-localidad").val());
    var idunidadhabi = null; // parseInt($("#colonia-info-idunidadhabi").val());
    var monto = ($('#colonia-info-monto').val() != "") ? parseFloat($('#colonia-info-monto').val()) : null;
    var activo = $("#colonia-info-activo").is(":checked");
    var idmunicipio = selectize.getValue();
    //console.log(idmunicipio);
    var data = {
        id: $indexcolonia,
        nombre: nombre,
        alias: alias,
        localidad: localidad,
        idunidadhabi: idunidadhabi,
        monto: monto,
        activo: activo,
        idmunicipio: idmunicipio
    };

    $.ajax({
        url: '/colonias/save',
        data: JSON.stringify(data),
        type: 'POST',
        beforeSend: function(){
            showMessageModalColonia('<div style="text-align: center; display: inline-block;">' +
                '<i class="fa fa-spinner fa-pulse"></i> Cargando...' +
                '</div>');
        },
        success: function (resp) {
            var jsonResp = JSON.parse(resp);
            $indexcolonia = jsonResp.id;
            clearErrorsColoniaInfo();
            setModeColoniaInfo('r');
            if(!$("#colonia-info-activo").is(":checked")){
                // $("#user-reset-password-do").hide(); no va
            }
            showMessageModalColonia('Se guardaron los datos del usuario', true);
            if(!jsonResp.edit){
                if(coloniasDT){
                    $tr = $(jsonResp.tr);
                    coloniasDT.row.add($tr).draw(false);
                }
                else{

                }
                $('#colonia-info-modal').off('hidden.bs.modal');
                $('#colonia-info-modal').on('hidden.bs.modal', function (e) {
                    $('#colonia-info-modal').off('hidden.bs.modal');
                    // $("#modal-user-profile-confirm").modal("show");
                });
                $("#colonia-info-modal").modal("hide");
            } else {
                var $tr = $("tr#tr-"+$indexcolonia);
                var data = coloniasDT.row($tr).data();
                data[1] = (jsonResp.activo) ? "<i class='fa fa-check' style='color: green' title='Activo'></i>" : "<i class='fa fa-remove' style='color: red' title='Inactivo'></i>";
                data[3] = jsonResp.nombre;
                data[4] = jsonResp.alias;
                data[6] = jsonResp.monto;
                data[5] = jsonResp.localidad;
                data[7] = jsonResp.fecha_modificacion;
                coloniasDT.row($tr).data(data).draw(false);
            }
        },
        error: function (xhr, status, error){
            if(xhr.status == 409){
                showErrorModalColonia('Ya existe la colonia: ' + nombre + ' y localidad: ' + localidad);
            }
            else if(xhr.status == 404){
                showErrorModalColonia('No se encontro la colonia con id: ' + $indexcolonia);
            }
            else if(xhr.status == 401){
                $("#sesion-modal").modal({
                    backdrop:"static",
                    keyboard: false
                });
                return;
            }
            else{
                showErrorModalColonia('Ocurrió un error al guardar los datos');
            }
        }
    });
};

var clearColoniaInfo = function() {
    $('#colonia-info-modal').find('input[type="text"]').val("");
    $('#colonia-info-modal').find('input[type="number"]').val(null);
    $('#colonia-info-modal').find('select').val("");

    showErrorModalColonia('');
    clearErrorsColoniaInfo();
};

var showMessageModalColonia = function(message, temporal) {
    $(".colonia-info-error").html('');
    $(".colonia-info-aviso").html(message);
    if(temporal) {
        setTimeout(function () {
            $(".colonia-info-aviso").html('');
        }, 2500);
    }
};

var showErrorModalColonia = function(error) {
    $(".colonia-info-aviso").html('');
    $(".colonia-info-error").html(error);
};

var clearErrorsColoniaInfo = function() {
    $("#colonia-info-nombre").closest( ".form-group-sm").removeClass("has-error");
    $("#colonia-info-alias").closest( ".form-group-sm").removeClass("has-error");
    $("#colonia-info-localidad").closest( ".form-group-sm").removeClass("has-error");
    // $("#colonia-info-idunidadhabi").closest( ".form-group-sm").removeClass("has-error");
    $("#colonia-info-monto").closest( ".form-group-sm").removeClass("has-error");
    $("#municipio-select").closest( ".form-group-sm").removeClass("has-error");
};

function loadDefaultColoniaInfo() {
    $("#colonia-info-activo").prop("checked", true);
}

var validateDataColoniaInfo = function(){
    var $nombre = $("#colonia-info-nombre");
    var $alias = $("#colonia-info-alias");
    var $localidad = $("#colonia-info-localidad");
    // var $idunidadhabi = $("#colonia-info-idunidadhabi");
    var $monto = $("#colonia-info-monto");

    var valid = true;
    if(!validateTextRequired($nombre.val()) || !validateTextWithWhitespaces($nombre.val())) {
        valid = false;
        //console.log('validate 1');
        if(!$nombre.closest( ".form-group-sm").hasClass("has-error")) {
            $nombre.closest( ".form-group-sm").addClass("has-error");
        }
    }
    /*if(!validateTextRequired($alias.val()) || !validateTextWithWhitespaces($alias.val())) {
     valid = false;
     if(!$alias.closest( ".form-group-sm").hasClass("has-error")) {
     $alias.closest( ".form-group-sm").addClass("has-error");
     }
     }*/
    if(!validateTextRequired($localidad.val()) || !validateTextWithWhitespaces($localidad.val())) {
        valid = false;
        //console.log('validate 2');
        if(!$localidad.closest( ".form-group-sm").hasClass("has-error")) {
            $localidad.closest( ".form-group-sm").addClass("has-error");
        }
    }
    /*if($idunidadhabi.val().trim() != '' && !validateNumber($idunidadhabi.val())) {
     valid = false;
     if(!$idunidadhabi.closest( ".form-group-sm").hasClass("has-error")) {
     $idunidadhabi.closest( ".form-group-sm").addClass("has-error");
     }
     }*/
    /*if($monto.val().trim() != '' && !validateNumber($monto.val())) {
     valid = false;
     if(!$monto.closest( ".form-group-sm").hasClass("has-error")) {
     $monto.closest( ".form-group-sm").addClass("has-error");
     }
     }*/

    //console.log(selectize.getValue());
    //console.log($("#municipio-select").val());

    //if($("#municipio-select").val() == "" || $("#municipio-select").val() == null){
    if(selectize.getValue() == "" || selectize.getValue() == null){
        valid = false;
        //console.log('validate 3');
        if(!$("#municipio-select").closest( ".form-group-sm").hasClass("has-error")) {
            $("#municipio-select").closest( ".form-group-sm").addClass("has-error");
        }
    }
    return valid;
};

var validateTextRequired = function(value) {
    if(value == null || value == undefined || value.trim() == "") {
        return false;
    }
    return true;
};

var validateTextWithWhitespaces = function(value) {
    var pattern=/[a-z|A-Z|\s]/;
    if(value != null && value != undefined && value.trim() != "" && !value.trim().match(pattern)) {
        return false;
    }
    return true;
};

/**
 * [getTextValueUpper Convertir texto a upperCase]
 * @param  {[String]} value [texto]
 * @return {[String]}       [texto uppercase]
 */
var getTextValueUpper = function(value) {
    var valueWithFormat = value;
    if(value == null || value == undefined || value.trim() == "") {
        valueWithFormat = null;
    } else {
        valueWithFormat = value.trim().toUpperCase();
    }
    return valueWithFormat;
};

var formatValue = function(value) {
    var valueWithFormat = value;
    if(value == null || value == undefined) {
        valueWithFormat = "";
    } else {
        valueWithFormat = value.trim().toUpperCase()
    }
    return valueWithFormat;
};

var validateNumber = function(value) {
    var pattern=/[0-9]/;
    if(value != null && value != undefined && value.trim() != "" && !value.match(pattern)) {
        return false;
    }
    return true;
};