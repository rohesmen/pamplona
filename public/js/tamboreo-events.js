var tamboreosDT;
var buttons = "";
var idMenu = "";
$(document).ready(function() {



    $(".tamboreo-filter-clear-do").off("click");
    $(".tamboreo-filter-clear-do").on("click", function(){
        $(this).next().val("");
    });


    $("#tamboreos-filter-select").off("change");
    $("#tamboreos-filter-select").on("change", function(){
        var valor = this.value;

        if(!$("#tamboreos-filtro-todos").hasClass("oculto"))
            $("#tamboreos-filtro-todos").toggleClass("oculto");

        if(!$("#tamboreos-filtro-clave").hasClass("oculto"))
            $("#tamboreos-filtro-clave").toggleClass("oculto");

        if(!$("#tamboreos-filtro-nombre").hasClass("oculto"))
            $("#tamboreos-filtro-nombre").toggleClass("oculto");

        if(!$("#tamboreos-filtro-colonia").hasClass("oculto"))
            $("#tamboreos-filtro-colonia").toggleClass("oculto");

        $("#tamboreos-nombre-val").val("");
        //$("#rutas-colonia-val").val("");
        $("#tamboreos-clave-val").val("");


        if(valor == ""){
            $("#tamboreos-filtro-todos").toggleClass("oculto");
        }
        else if(valor == "nom"){
            $("#tamboreos-filtro-nombre").toggleClass("oculto");
        }
        else if(valor == "col"){
            $("#tamboreos-filtro-colonia").toggleClass("oculto");
        }
        else{
            $("#tamboreos-filtro-clave").toggleClass("oculto");
        }
    });



    $(".tamboreos-search-do").off('click');
    $(".tamboreos-search-val").off('keypress');
    $(".tamboreos-search-do").on('click', function(){

        fnBuscarTamboreos();
    });

    $(".tamboreos-search-val").on('keypress', function(e){
        if(e.which == 13){
            fnBuscarTamboreos();
        }
    });


    /**
     * Abre el modal para el formulario de Tamooreo
     */
    $("#openNewTamboreo").on('click', function() {

        $("#tamboreo-info-modal").modal({
            show: true,
            backdrop: "static"
        });

        fnLimpiaForm();
        setModeTamboreoInfo('add');
        $('.modal-titleAux').html('Nuevo Tamboreo');

    });



    $('#frmTamboreo').on('submit', function (event) {
        event.preventDefault();

        fnSaveTamboreo();

    });

    $("#btnGuardar").on('click', function() {

        $("#btnGuardaForm").click();
    });



});

function fnSaveTamboreo() {



    var data = {
        clave: $("#clave").val(),
        activo: $("#activo").is(':checked'),
        nombre: $("#titulo").val(),
        descripcion: $("#descripcion").val(),
        monto: $("#monto").val(),



    };

    $.ajax({
        url: '/tamboreo/guardar',
        data: JSON.stringify(data),
        type: 'POST',
        dataType: "json",
        beforeSend: function(){
            $('#tamboreo-container').html('<div style="text-align: center;">' +
                '<i class="fa fa-spinner fa-pulse fa-2x"></i> Guardando...' +
                '</div>');
        },
        success: function(json) {

            if (!json.lOk) {
                showGeneralMessage(json.cMensaje, "warning", true);
                return;
            }
            else
            {
                showGeneralMessage("Registro Guardado", "success", true);
                $('#tamboreo-info-modal').modal('hide');
                var $tr = $("#tamboreo-" + json.id);
                if(!json.nuevo){
                    tamboreosDT.cell( $tr, 2 ).data( json.vigente );
                    tamboreosDT.cell( $tr, 3 ).data( json.id );
                    tamboreosDT.cell( $tr, 4 ).data( json.nombre );
                    tamboreosDT.cell( $tr, 5 ).data( json.descripcion );


                }
                else {
                    var data = tamboreosDT
                        .rows();
                    data = $(data)[0]
                    console.log(data);
                    $(data).each(function( index ) {
                        //console.log(tamboreosDT.row(index).id());


                        var $tr = $("#" + tamboreosDT.row(index).id());
                         console.log(tamboreosDT.row(index).data());
                         if(tamboreosDT.row(index).data()[4] == json.nombre)
                             tamboreosDT.row(index).data()[2] ='<i class="fa fa-remove" style="color: red" title="Activo"></i>';


                    });

                    tamboreosDT.rows()
                        .invalidate()
                        .draw('page');


                    $row = tamboreosDT.row.add( [
                        null,
                        null,
                        json.vigente,
                        json.id,
                        json.nombre,
                        json.descripcion,
                        json.fechaCreacion
                    ] ).draw(false);

                    //tamboreosDT.draw('page');

                    $($row).attr("id", json.DT_RowId);
                }
               // tamboreosDT.draw('page');

            }
        },
        complete: function () {
            $('#tamboreo-container').html("");
        }
    });

}

function fnBuscarTamboreos() {

    var nombre = $("#tamboreos-nombre-val").val();
    var clave = $("#tamboreos-clave-val").val();
    var tipoBusqueda = $("#tamboreos-filter-select").val();

    if(tipoBusqueda == "nom"){
        if(nombre == ""){
            showGeneralMessage("Debe escribir el nombre a buscar", "warning", true);
            return;
        }
        if(nombre.length < 2){
            showGeneralMessage("Debe escribir almenos dos caracteres", "warning", true);
            return;
        }
    }
    if(tipoBusqueda == "cla"){
        if(clave == ""){
            showGeneralMessage("Debe escribir la clave a buscar", "warning", true);
            return;
        }
    }
    if(tipoBusqueda == "col"){
        if(colonia == ""){
            showGeneralMessage("Debe seleccionar una colonia", "warning", true);
            return;
        }


    }

    fnLimpiarDatos();

    var data = {
        tipo: tipoBusqueda,
        titulo: nombre,
        clave: clave,


    }
    $.ajax({
        url: '/tamboreo/search',
        data: JSON.stringify(data),
        type: 'POST',
        dataType: "json",
        beforeSend: function(){

            $('#tamboreo-container').html('<div style="text-align: center;">' +
                '<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
                '</div>');
        },
        success: function(json){

            tamboreosDT.clear();
            if(json.length == 0)
            {
                showGeneralMessage("No se encontraron registros", "warning", true);
                return;
            }



            tamboreosDT.rows.add(json);
            tamboreosDT.draw();


            $(window).resize();


        },
        complete: function () {
            $('#tamboreo-container').html("");
        }
    });

    
}

function fnLimpiarDatos()
{
    selec = new Array();
    uSelec = 0;
    tamboreosDT.clear();

}


function fnLimpiaForm()
{
    $("#clave").val("");
    $("#activo").prop('checked', true);
    $("#titulo").val("");
    $("#descripcion").val("");
    $("#monto").val("");


}

function loadTamboreoInfo($indexTamboreo, mode) {
    if($indexTamboreo == null){
        return;
    }

    $.ajax({
        url: "/tamboreo/get/" + $indexTamboreo,
        type: 'GET',
        success: function(data){
            $data = jQuery.parseJSON(data);

            fnLimpiaForm();
            setModeTamboreoInfo(mode);
            setTamboreoInfo($indexTamboreo, $data, mode);

        },
        /*error: function(){
        }*/
    });
    
}

function setModeTamboreoInfo(mode) {

    if(mode == "info"){
        validateForm = false;
        $('.modal-titleAux').html('Información');
        $("#btnGuardar").hide();


        $("#titulo").attr("disabled", true);
        $("#descripcion").attr("disabled", true);


    }
    else if('edit' || 'add'){




        $('.modal-titleAux').html('Modificar');
        $("#titulo").removeAttr("disabled");
        $("#descripcion").removeAttr("disabled");

        $("#btnGuardar").show();
    }

}

function setTamboreoInfo($indexTamboreo, $data, mode) {
    $("#clave").val($data.id);
    if(mode == "info")
        $("#activo").prop('checked', $data.activo);
    $("#titulo").val($data.nombre);
    $("#descripcion").val($data.descripcion);
    $("#monto").val($data.monto);



    
}

function eliminarTamboreo($tr, id){
    $.ajax({
        url: "/tamboreo/delete/" + id,
        success: function(){
            showGeneralMessage("La información se guardo correctamente", "success", true);
            tamboreosDT.cell( $tr, 2 ).data( '<i class="fa fa-remove" style="color: red" title="Activo"><\/i>' );
            tamboreosDT.draw();
        },
        /*error: function(){
            showGeneralMessage("Ocurrió un error al guardar la información", "error", true);
        }*/
    });

}