validateForm = true;
var map = null, marker = null, geocoder = null;
var CENTER_MAP_LAT = 20.9704, CENTER_MAP_LON = -89.6232,initZoom = 11;
var idClienteDescuento = 0;
var idClienteNuevo = 0;
var mesClienteDescuento = "";
var anioClienteDescuento = "";

$(function() {
    $.validator.messages.required = 'Requerido';
    $("#clientes-colonia-val").selectize({
        plugins: ["clear_button"]
    });
    $("#clientes-ruta-filter-val").selectize({
        plugins: ["clear_button"]
    });
    $("#clientes-clave-val").inputmask({ "mask": "9", "repeat": 15, "greedy": false});


    /**
     * Modal para guarado de descuento de pensionado
     */
    $("#btnGuardarModal").on('click', function() {
        var clave = $("#clave").val();
        var oficio = $("#cOficio").val().trim();
        var fini = $("#fInicio").val();
        var ffin = $("#fFin").val();

        var error = "";
        if(oficio == ""){
            error += "- El oficio es obligatorio <br>";
        }

        if(fini == ""){
            error += "- Fecha inicio es obligatorio <br>";
        }

        if(ffin == ""){
            error += "- Fecha fin es obligatorio <br>";
        }

        if(rutaDropzonePensionado.getQueuedFiles().length == 0){
            error += "- La imagen es obligatorio <br>";
        }

        if(error != ""){
            showGeneralMessage(error, "warning", true);
            return;
        }
        rutaDropzonePensionado.processQueue();
    });

    $('#fInicio').datepicker({
        format: "mm/yyyy",
        language: "es",
        autoclose: true,
        startView: 1,
        minViewMode: 1,
        todayHighlight: true
    });

    $('#fFin').datepicker({
        format: "mm/yyyy",
        language: "es",
        autoclose: true,
        startView: 1,
        minViewMode: 1,
        todayHighlight: true
    });

    /*$('#fInicio').datepicker()
        .on('changeDate', function(e) {
            // `e` here contains the extra attributes
            $('#fFin').datepicker('setStartDate', $('#fInicio').val());
            $('#fFin').val("");
        });*/


    $(".clientes-filter-clear-do").off("click");
    $(".clientes-filter-clear-do").on("click", function(){
        $(this).next().val("");
    });

    $(".clientes-search-do").off("click");
    $(".clientes-search-do").on("click", function(){
        buscar();
    });

    $("#clientes-filter-select").off("change");
    $("#clientes-filter-select").on("change", function(){
        var valor = this.value;
        $("#clientes-filtro-todos").hide();
        $("#clientes-filtro-nombre").hide();
        $("#clientes-filtro-direccion").hide();
        $("#clientes-filtro-clave").hide();
        $("#clientes-filtro-vigente").hide();
        $("#clientes-filtro-rutavalidada").hide();

        $("#clientes-nombre-val").val("");
        $("#clientes-calle-val").val("");
        $("#clientes-numero-val").val("");
        $("#clientes-rutavalidada-filter-val").val("");

        var $select = $('#clientes-colonia-val').selectize();
        var control = $select[0].selectize;
        control.clear();
        $("#clientes-colonia-val").val("");

        var $selectRuta = $('#clientes-ruta-filter-val').selectize();
        var controlRuta = $selectRuta[0].selectize;
        controlRuta.clear();
        $("#clientes-ruta-filter-val").val("");

        $("#clientes-clave-val").val("");
        $("#clientes-vigente-val").val("");
        $("#clientes-vigente-val").removeAttr("disabled");

        $("#clientes-filtro-clave-").hide();
        if(valor == ""){
            $("#clientes-filtro-todos").show();
        }
        else if(valor == "nom"){
            $("#clientes-filtro-nombre").show();
        }
        else if(valor == "dir"){
            $("#clientes-filtro-direccion").show();
        }
        else if(valor == "vig"){
            $("#clientes-filtro-vigente").show();
        }
        else if(valor == "ruta"){
            $("#clientes-filtro-rutavalidada").show();
        }
        else{
            $("#clientes-vigente-val").attr("disabled", "disabled");
            $("#clientes-filtro-clave").show();
        }
    });

    var ClientePhalcon = {};
    ClientePhalcon.personaFisica = true;
    ClientePhalcon.isUbicado = false;

    var form = $("#wizard-form");
    form.validate({
        rules: {
            mensualidad: {
                min: 1, number: true, required: true
            }},
        messages: {
            mensualidad: {
                min: "Costo mayor a 0"
            }
        }
    });
    
    form.steps({
        headerTag: "h1",
        // bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanged: function(event, currentIndex, prioriIndex){
            if(currentIndex == 1){
                var dhhlatcliente = 0;
                var dhhlongcliente = 0;
                var llimpiarCampoDir = false;

                dhhlatcliente = parseFloat($("#latitud").val()) != 0 ? parseFloat($("#latitud").val()) : 0;
                dhhlongcliente = parseFloat($("#longitud").val()) != 0 ? parseFloat($("#longitud").val()) : 0;

                google.maps.event.trigger(map, 'resize');
                map.setCenter({
                    lat: CENTER_MAP_LAT,
                    lng: CENTER_MAP_LON
                });
                map.setZoom(initZoom);
                if(validateForm){
                    initMapClickListener();
                }
                else{
                    clearMapClickListener();
                }
                if(marker != null){
                    fitBoundMarker(map, marker);
                    // setCenterMap(map, marker.getPosition().lat(), marker.getPosition().lng(), initZoom);
                    marker.setDraggable(validateForm);
                }
                google.maps.event.trigger(map, 'resize');

                var cDirecBuscar = null;
                if (!dhhlatcliente || !dhhlongcliente || parseFloat(dhhlatcliente) == 0)
                {

                    if ($("#calle").val() != null && $("#numero").val() != null)
                    {
                        cDirecBuscar = 'CALLE ' + $("#calle").val()  + ' ' + $("#numero").val() + ' ' + $("#idcolonia :selected").text();
                    }
                    else
                    {
                        cDirecBuscar = $("#idcolonia :selected").text();
                    }
                }
                else
                {
                    cDirecBuscar = dhhlatcliente + ',' + dhhlongcliente ;
                    llimpiarCampoDir = true;
                }

                $("#cliente-input-gelocation").val(cDirecBuscar);
                geocodeAddress(map);
                if (llimpiarCampoDir)
                    $("#cliente-input-gelocation").val('');

            }
            /*else if(currentIndex == 3){
                if($("input[name=tipo_cliente]:checked").val() == "fisica"){
                    if(prioriIndex == 2) $("#wizard-form").steps("next");
                    if(prioriIndex == 4) $("#wizard-form").steps("previous");
                }
            }*/
            else if(currentIndex == 4){
                if($("input[name=tipo_cliente]:checked").val() == "fisica"){
                    if(prioriIndex == 3) $("#wizard-form").steps("next");
                    if(prioriIndex == 5) $("#wizard-form").steps("previous");
                }
            }
        },
        onStepChanging: function (event, currentIndex, newIndex)
        {
            if(currentIndex > newIndex) return true;

            if(validateForm){
                if($("input[name=tipo_cliente]:checked").val() == "fisica"){
                    $(".validate-required").removeAttr("required");
                    $(".validate-required").removeAttr("required");
                    $(".ocultar-moral-for-validate :input").addClass("do-not-validate-moral");
                }
                else{
                    $(".ocultar-moral :input").removeClass("do-not-validate-moral");
                    $(".ocultar-moral-for-validate :input").removeClass("do-not-validate-moral");
                }
                form.validate().settings.ignore = ":disabled,:hidden";
                form.validate().settings.messages.required = "Campo requerido";

                if(currentIndex == 0){
                    if(form.valid() && $("#idcolonia").val() != ""){
                        if($("#chkprivada").is(":checked") && $("#nombre_privada").val().trim() ==""){
                            form.validate({
                                rules: {mensualidad: {min: 1, number: true, required: true}},
                                messages: {
                                    mensualidad: {
                                        min: "Costo mayor a 0"
                                    }
                                }
                            }).settings.ignore = ":disabled";
                            form.valid();
                            return false;
                        }
                        if($("#chMontoManual").is(":checked") && parseInt($("#mensualidad").val()) < 1){
                            form.validate({
                                rules: {mensualidad: {min: 1, number: true, required: true}},
                                messages: {
                                    mensualidad: {
                                        min: "Costo mayor a 0"
                                    }
                                }
                            }).settings.ignore = ":disabled";
                                form.valid();
                                return false;
                        }
                        return true;
                    }
                    else{
                        if($("#idcolonia").val() == ""){
                            form.validate({
                                rules: {mensualidad: {min: 1, number: true, required: true}},
                                messages: {
                                    mensualidad: {
                                        min: "Costo mayor a 0"
                                    }
                                }
                            }).settings.ignore = ":disabled";
                            form.valid();
                        }
                        if($("#chMontoManual").is(":checked") != "" && parseInt($("#mensualidad").val()) < 1){
                            form.validate({
                                rules: {mensualidad: {min: 1, number: true, required: true}},
                                messages: {
                                    mensualidad: {
                                        min: "Costo mayor a 0"
                                    }
                                }
                            }).settings.ignore = ":disabled";
                                form.valid();
                                return false;
                        }
                        return false;
                    }
                    
                }
                else if(currentIndex == 1){
                    if(marker == null){
                        showGeneralMessage("Debe ubicar el predio", "warning", true);
                        return false;
                    }
                    return true;
                }
                /*else if(currentIndex == 2){
                    /*if($("input[name=tipo_cliente]:checked").val() == "fisica"){
                        form.valid();
                        return true;
                    }
                    if(form.valid() && $("#cliente_rs").val() != ""){
                        return true;
                    }
                    else{
                        if($("#cliente_rs").val() == ""){
                            form.validate().settings.ignore = ":disabled";
                            form.valid();
                        }
                        return false;
                    } //aca debe terminar comentado

                    if(form.valid()) {
                        return true;
                    }
                    else {
                        form.validate().settings.ignore = ":disabled";
                        form.valid();

                        return false;
                    }

                }*/
                else if(currentIndex == 3) {
                    if($("input[name=tipo_cliente]:checked").val() == "fisica"){
                        return form.valid();
                        // return true;
                    }
                    if(form.valid() && $("#cliente_rs").val() != ""){
                        return true;
                    }
                    else{
                        if($("#cliente_rs").val() == ""){
                            form.validate().settings.ignore = ":disabled";
                            form.valid();
                        }
                        return false;
                    }
                }
                else if(currentIndex == 4){
                    if($("input[name=tipo_cliente]:checked").val() == "fisica"){
                        form.valid();
                        return true;
                    }

                     /*var valTipoFact = $("#tipo_facturacion").val();
                     if(form.valid() && $("#fiscal_idcolonia").val() != ""){
                         return true;
                     }
                     else{
                         if($("#fiscal_idcolonia").val() == ""){
                             form.validate().settings.ignore = ":disabled";
                             form.valid();
                         }
                         return false;
                     }*/
                }
                else{
                    return form.valid();
                }

                // return form.valid();

            }
            else {
                return true;
            }
        },
        onCanceled: function (event)
        {
            // $('#wizard-form')[0].reset();
            $('#wizard-form input:not([name=tipo_cliente],#idcliente)').val("");
            setFirstStep();
            $("#cliente-info-modal").modal("hide");

        },
        onFinishing: function (event, currentIndex)
        {
            if(validateForm){
                if($("input[name=tipo_cliente]:checked").val() == "fisica"){
                    $(".ocultar-moral :input").addClass("do-not-validate-moral");
                    $(".ocultar-moral-for-validate :input").addClass("do-not-validate-moral");
                }
                else{
                    $(".ocultar-moral :input").removeClass("do-not-validate-moral");
                    $(".ocultar-moral-for-validate :input").removeClass("do-not-validate-moral");
                }
                form.validate().settings.ignore = ":disabled,.do-not-validate-moral";

                if($("#chkprivada").is(":checked") && $("#nombre_privada").val().trim() ==""){
                    form.validate().settings.ignore = ":disabled";
                    form.valid();
                    return false;
                }

                if($("#idestatus_cliente").val() != "" && $("#idestatus_cliente").val() != null){
                    if($("#idestatus_cliente option:selected").attr("data") == 'true' && $("#nombre_comercio").val().trim() == ""){
                        form.validate().settings.ignore = ":disabled";
                        form.valid();
                        return false;
                    }
                }

                return form.valid();
            }
            else {
                return true;
            }
        },
        onFinished: function (event, currentIndex)
        {
            // if(marker == null){
            //     showGeneralMessage("Debe ubicar el predio", "warning", true);
            //     return false;
            // }
            addNewCliente();
        },
        labels: {
                cancel: "Cerrar",
                current: "Actual:",
                pagination: "Paginación",
                finish: "<i class='fa fa-save'></i>",
                next: ">>",
                previous: "<<",
                loading: "Cargando ..."
            }
    }).validate({
        rules: {
            mensualdiad: {
                min: 1
            }
        }
    });

    var loadNewCliente = function() {
        $(".ocultar-moral").hide();
        var d = new Date();
        var anio = d.getFullYear();
        $("#ultimo_anio_pago").val(anio);
        // $("#ultimo_mes_pago").val(1);
        //setModeClienteInfo('n');
        //clearClienteInfo();
        //loadDefaultClienteInfo();
        //showMessageModalCliente("Datos requeridos <span class='cliente-required'>marcados</span>", false);
        $("#chMontoManual").prop("disabled", false);
        $("#chMontoManual").prop("checked", false).change();
        //$("#mensualidad").attr('disabled', 'disabled');
    };

    /**
     * Abre el modal para el formulario de Cliente
     */
    $("#openNewCliente").on('click', function() {
        loadNewCliente();
        $("#cliente-info-modal").modal({
            show: true,
            backdrop: "static"
        });
        $('.modal-title').html('Cliente nuevo');
        toggleFinish(true);
    });

    /**
     * [Guardar nuevo cliente]
     */
    var addNewCliente = function() {

        /*var fileInput = document.getElementById('file-comprobante');
        var file = fileInput.files[0];*/
        // var formData = new FormData();
        // formData.append('file', file);

        var clienteFormModal = $("#wizard-form");
        if (clienteFormModal.validate().form()) {
            var data = {
                id: $("#idcliente").val() != "" ? $("#idcliente").val() : null,
                calle: parseInt($("#calle").val()),
                calle_letra: getTextValue($("#calle_letra").val()),
                tipo_calle: getTextValueUpper($("#tipo_calle").val()),
                numero: parseInt($("#numero").val()),
                numero_letra: getTextValue($("#numero_letra").val()),
                tipo_numero: getTextValue($("#tipo_numero").val()),
                cruza1: getTextValue($("#cruza1").val()),
                letra_cruza1: getTextValue($("#letra_cruza1").val()),
                tipo_cruza1: getTextValue($("#tipo_cruza1").val()),
                cruza2: getTextValue($("#cruza2").val()),
                letra_cruza2: getTextValue($("#letra_cruza2").val()),
                tipo_cruza2: getTextValue($("#tipo_cruza2").val()),
                idcolonia: parseInt($("#idcolonia").val()),
                localidad: getTextValue($("#localidad").val()),
                apepat: getTextValue($("#apepat").val()),
                apemat: getTextValue($("#apemat").val()),
                nombres: getTextValue($("#nombres").val()),
                apepat_propietario: getTextValue($("#apepat_propietario").val()),
                apemat_propietario: getTextValue($("#apemat_propietario").val()),
                nombres_propietario: getTextValue($("#nombres_propietario").val()),
                folio_catastral: $("#folio_catastral").val(),
                activo: true,
                fecha_creacion: null,
                fecha_modificacion: null,
                latitud: $("#latitud").val(),
                longitud: $("#longitud").val(),
                // ubicado: ClientePhalcon.isUbicado,
                idtarifa_colonia: $("#idtarifa_colonia").val()!= "" ? parseInt($("#idtarifa_colonia").val()):null,
                idruta: $("#idruta").val()!= "" ? parseInt($("#idruta").val()):null,
                idtipo_contrato: $("#idtipo_contrato").val()!= "" ? parseInt($("#idtipo_contrato").val()):null,
                referencia_ubicacion: getTextValue($("#referencia_ubicacion").val()),
                fisica: $("input[name=tipo_cliente]:checked").val() == "fisica" ? true : false,
                nombre_moral: getTextValue($("#nombre_moral").val()),

                telefono: getTextValue($("#telefono").val()),
                correo: getTextValue($("#correo").val()),
                idtipo_descuento: $("#idtipo_descuento").val()!= "" ? parseInt($("#idtipo_descuento").val()):null,
                // descuento: $("#descuento").val()!= "" ? parseInt($("#descuento").val()):null,
                mensualidad: ($("#mensualidad").val() != "") ? ($("#mensualidad").val()) : null,
                monto_cobro: ($("#mensualidad").val() != "") ? ($("#mensualidad").val()) : 0,

                cobro_manual: $("#chMontoManual").is(':checked') == true ? true : false,

                ultimo_anio_pago: parseInt($("#ultimo_anio_pago").val()),
                ultimo_mes_pago: getTextValue($("#ultimo_mes_pago").val()),
                observacion: getTextValue($("#observacion").val()),
                direccion_otro: getTextValue($("#direccion_otro").val()),
                // mensualidad: parseInt($("#mensualidad").val())

                //datos persona moral
                nombre_sucursal: getTextValue($("#nombre_sucursal").val()),
                contacto_servicio: getTextValue($("#contacto_servicio").val()),
                contacto_servicio_tel: getTextValue($("#contacto_servicio_tel").val()),
                contacto_servicio_ext: getTextValue($("#contacto_servicio_ext").val()),
                contacto_pago1: getTextValue($("#contacto_pago1").val()),
                contacto_pago1_tel: getTextValue($("#contacto_pago1_tel").val()),
                contacto_pago1_ext: getTextValue($("#contacto_pago1_ext").val()),
                contacto_pago2: getTextValue($("#contacto_pago2").val()),
                contacto_pago2_tel: getTextValue($("#contacto_pago2_tel").val()),
                contacto_pago2_ext: getTextValue($("#contacto_pago2_ext").val()),
                metodo_pago: $("#metodo_pago").val() != "" ? $("#metodo_pago").val() : null,
                numero_contrato: getTextValue($("#numero_contrato").val()),
                periodicidad_facturacion: $("#periodicidad_facturacion").val() != "" ? $("#periodicidad_facturacion").val() : "",
                tipo_facturacion: $("#tipo_facturacion").val() != "" ? $("#tipo_facturacion").val() : "",
                monto_tasa_fija: getTextValue($("#monto_tasa_fija").val()),
                idfacturacion_tamboreo: $("#facturacion_tamboreo").val() != "" ? $("#facturacion_tamboreo").val() : null,
                cantidad_tamboreo: getTextValue($("#cantidad_tamboreo").val()),
                idrazon_social: $("#cliente_rs").val() != "" ? parseInt($("#cliente_rs").val()) : null,
                // direccion_moral: getTextValue($("#direccion_moral").val()),

                abandonado: $("#chkabandonado").is(":checked"),
                idestatuscliente: ($("#idestatus_cliente").val() != "" && $("#idestatus_cliente").val() != null) ? parseInt($("#idestatus_cliente").val()) : null,
                comercio: ($("#idestatus_cliente").val() != "" && $("#idestatus_cliente").val() != null) ? ($("#idestatus_cliente option:selected").attr("data") == 'true' ? true : false) : false,
                marginado: $("#chkmarginado").is(":checked"),
                privada: $("#chkprivada").is(":checked"),
                nombre_privada: $("#nombre_privada").val().trim() ? $("#nombre_privada").val().trim() : "",
                nombre_comercio: $("#nombre_comercio").val().trim() ? $("#nombre_comercio").val().trim() : "",
                // file: file

                id_tipo_comprobante : getTextValue($("#idcomprobante").val()),
                id_tipo_identificacion : getTextValue($("#ididentificacion").val()),
                telefono_contacto: getTextValue($("#tel_contacto").val()),

            };

            var formData = new FormData();
            Object.keys(data).forEach(key => formData.append(key, data[key]));

            //formData.append('comprobante', file);

            $.ajax({
                url: '/clientes/save',
                data: formData,
                type: 'POST',
                processData: false,
                contentType: false,
                beforeSend: function(){

                },
                success: function(resp){


                    var json = JSON.parse(resp);
                    showGeneralMessage('Resultados guardados con éxito!', "success", true);
                    // toastr.success('','Resultados guardados con éxito!', {timeOut: 5000});
                    // $('#wizard-form')[0].reset();
                    $('#wizard-form input:not([name=tipo_cliente],#idcliente)').val("");
                    $(".ocultar-moral").hide();
                    setFirstStep();

                    var $tr = $("#cliente-" + json.id);
                    idClienteNuevo = json.id;

                    var lPinta = true;
                    if(rutaDropzone.getQueuedFiles().length > 0) {
                        lPinta = false;
                        rutaDropzone.processQueue();
                    }
                    else if(rutaDropzone0.getQueuedFiles().length > 0) {
                        lPinta = false;
                        rutaDropzone0.processQueue();
                    }
                    else if(rutaDropzone2.getQueuedFiles().length > 0) {
                        lPinta = false;
                        rutaDropzone2.processQueue();
                    }


                    if(!json.nuevo){
                        clientesDT.cell( $tr, 2 ).data( json.vigente );
                        clientesDT.cell( $tr, 3 ).data( json.id );
                        clientesDT.cell( $tr, 4 ).data( json.calle );
                        clientesDT.cell( $tr, 5 ).data( json.numero );
                        clientesDT.cell( $tr, 6 ).data( json.cruz1 );
                        clientesDT.cell( $tr, 7 ).data( json.cruz2 );
                        clientesDT.cell( $tr, 8 ).data( json.colonia );
                        clientesDT.cell( $tr, 9 ).data( json.persona );
                        clientesDT.cell( $tr, 10 ).data( json.propietario );
                    }

                    if(lPinta) {
                        clientesDT.draw();
                        $("#cliente-info-modal").modal("hide");
                    }



                },
                error: function(xhr, status, error){
                    var colonia = $("#idcolonia :selected").text();
                    if(xhr.status == 400){
                        showGeneralMessage("Método no implementado", "error", true);
                    }
                    else if(xhr.status == 401){
                        //showGeneralMessage('No cuenta con permisos, contacte al administrador', "error", true);
                        $("#sesion-modal").modal({
                            backdrop:"static",
                            keyboard: false
                        });
                        return;
                    }
                    else if(xhr.status == 404){
                        showGeneralMessage("No se encontro el cliente con id: " + id, "error", true);
                    }
                    else if(xhr.status == 409){
                        var text = "Se encontró coincidencia en la dirección, calle: " + data.calle;
                        if(data.calle_letra) {
                            text += " letra: " + data.calle_letra;
                        }
                        text += " numero: " + data.numero;
                        if(data.numero_letra) {
                            text += " letra: " + data.numero_letra;
                        }
                        text += " colonia: " + colonia;
                        showGeneralMessage(text, "error", true);
                    }
                    else{
                        showGeneralMessage("Ocurrió un error al guardar los datos", "error", true);
                    }
                },
                complete: function(data) {

                }
            });

        } else {
            toastr.warning('Todos los campos marcados son obligatorios');
        }
    };

    var loadClientes = function() {
        // $("#loading").show();
        var data = {
            parameters: $('#client-search-val').val()
        }
        $.ajax({
            url: '/clientes/search',
            data: $.param(data),
            type: 'POST',
            beforeSend: function(){
                /*$('#users-datatable-container').html('<div style="text-align: center;">' +
                    '<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
                    '</div>');*/
            },
            success: function(html){
                $('#clientes-datatable-container').html(html);
            }
        });
    };

    /**
     * [Buscar clientes por valores ingresados]
     */
    $("#client-search-do").on('click', function() {
        loadClientes();
    });

    // folio solo acepta numeros
    $("#folio_catastral").keyup(function () {
        this.value = (this.value).replace(/[^0-9]/g, '');
    });

    // mensualidad solo acepta numero
    // $("#mensualidad").keyup(function () {
    //     this.value = (this.value).replace(/[^0-9]/g, '');
    // });

    $('#cliente-info-modal').on('hidden.bs.modal', function () {
        validateForm = true;
        setFirstStep();
        clientesDT.$('tr.selected').removeClass('selected');
        $('#wizard-form input:not([name=tipo_cliente])').val("");
        idcoloniaForm[0].selectize.setValue("");
        idrutaSelectize[0].selectize.setValue("");
        map.setCenter({
            lat: CENTER_MAP_LAT,
            lng: CENTER_MAP_LON
        });
        map.setZoom(initZoom);
        marker = clearMarker(marker);
        $(".ocultar-fisica").show();
        $(".ocultar-moral").hide();
        /*$("#wizard-form-t-3").hide();
        $("#wizard-form-t-4 .number").html("4.");
        $("#wizard-form-t-5 .number").html("5.");*/

        $("#wizard-form-t-4").hide();
        $("#wizard-form-t-5 .number").html("5.");
        $("#wizard-form-t-6 .number").html("6.");

        $(".ocultar-tamboreo").hide();
        $(".ocultar-fija").hide();

        $("#chkabandonado")[0].checked = false;
        $("#chkmarginado").prop("disabled", false);
        $("#chkmarginado")[0].checked = false;
        $("#chkmarginado").prop("disabled", true);
        $("#idestatus_cliente").val($("#idestatus_cliente option:first-child").val()).change();
        $("#idestatus_cliente").prop("disabled", false);
        $("#chkprivada")[0].checked = false;

        $("#nombre_privada").prop("required", false);
        $("#nombre_comercio").prop("required", false);

        $("#nombre_privada").prop("disabled", true);
        $("#nombre_comercio").prop("disabled", true);

    });

    $('#cliente-info-modal').on('hide.bs.modal', function () {
        showDefautlsForm();
    });

    $('#cliente-info-modal').on('shown.bs.modal', function () {
        initMap();
    });

    /**
     * [togglePrevious Habilita o deshabilita los botones (previous, next, finish) para el modal de cliente ]
     * @param  {[boolean]} enable) boleano para habilitar o desabilitar
     */
    var toggleFinish = function(enable) { toggleButton("finish",   enable); };
    var toggleButton = function(buttonId, enable)
    {
        if (enable)
        {
            // Enable disabled button
            var button = $("#wizard-form").find('a[href="#' + buttonId + '-disabled"]');
                button.attr("href", '#' + buttonId);
                button.parent().removeClass();
        }
        else
        {
            // Disable enabled button
            var button = $("#wizard-form").find('a[href="#' + buttonId + '"]');
                button.attr("href", '#' + buttonId + '-disabled');
                button.parent().addClass("disabled");
        }
    };

    $("body").on("click", ".btnInfoRutaValidar", function (){
        var $tr = $(this).closest("tr");
        var idAttr = $tr.attr("id").split("-");

        var idestatus = $(this).data("id");
        var data = clientesDT.row($tr).data();
        var id = data[3];
        var idruta = data[30];
        var isValidado = data[35];
        var usuarioValida = data[33];
        var fechaValida = data[34];
        var observaciones = data[36];
        if ( $tr.hasClass('selected') ) {
            //$tr.removeClass('selected');
        }
        else {
            clientesDT.$('tr.selected').removeClass('selected');
            $tr.addClass('selected');
        }

        $("#idClienteValidarRuta").val(id);
        $("#idruta-validacion").val(idruta);
        $("#dateFechaVaidacion").val(fechaValida);
        $("#fechaVaidacion").val(usuarioValida);
        rutaValidacionSelect2[0].selectize.setValue(idruta, false);
        $("#observacionesValidaRuta").prop("disabled", isValidado);
        $("#btnGuardarValidacion").prop("disabled", isValidado);
        $("#observacionesValidaRuta").val(observaciones);

        if(isValidado){
            rutaValidacionSelect2[0].selectize.disable();
            $("#data-validacion").show();
            $("#modalPorValidar").html("Los datos de la validación son");
        }
        else{
            rutaValidacionSelect2[0].selectize.enable();
            $("#data-validacion").hide();
            $("#modalPorValidar").html("Se realizara la validación de la ruta");
        }
        $("#valida-ruta-modal").modal("show");
    });
    
    $("#btnGuardarValidacion").on("click", function () {
        var idRuta = $("#idruta-validacion").val();
        var idcliente = $("#idClienteValidarRuta").val();
        var observaciones = $("#observacionesValidaRuta").val();

        if(!idRuta){
            showGeneralMessage("Debe seleccionar una ruta", "warning", true);
            return;
        }

        var data = {
            idcliente: idcliente,
            idruta: idRuta,
            observaciones: observaciones
        }

        $.ajax({
            data: JSON.stringify(data),
            url: "/clientes/validaruta/" + idcliente,
            type: "POST",
            success: function (resp){
                showGeneralMessage("La información se guardo correctamente", "success", true);
                clientesDT.draw();
                $("#valida-ruta-modal").modal("hide");
            },
            beforeSend: function (){
                $("#btnGuardarValidacion").html('<i class="fa fa-spin fa-spinner"></i> Guardar');
                $("#btnGuardarValidacion").prop("disabled", true);
            },
            complete: function (){
                $("#btnGuardarValidacion").html('Guardar');
                $("#btnGuardarValidacion").prop("disabled", false);
            }
        })
    });

    rutaValidacionSelect2 = null;
    rutaValidacionSelect2 = $("#idruta-validacion").selectize();

    $('#valida-ruta-modal').on('shown.bs.modal', function() {
        if(!rutaValidacionSelect2) {
            //rutaValidacionSelect2 = $("#idruta-validacion").selectize();
        }
    });
});


/**
 * [getTextValueUpper Convertir texto a upperCase]
 * @param  {[String]} value [texto]
 * @return {[String]}       [texto uppercase]
 */
var getTextValueUpper = function(value) {
    var valueWithFormat = value;
    if(value == null || value == undefined || value.trim() == "") {
        //valueWithFormat = null;
        valueWithFormat = "";
    } else {
        valueWithFormat = value.trim().toUpperCase();
    }
    return valueWithFormat;
};

/**
 * [getTextValueLower Convertir texto a minusculas]
 * @param  {[String]} value [texto]
 * @return {[String]}       [texto lower]
 */
var getTextValueLower = function(value) {
    var valueWithFormat = value;
    if(value == null || value == undefined || value.trim() == "") {
        //valueWithFormat = null;
        valueWithFormat = "";
    } else {
        valueWithFormat = value.trim().toLowerCase();
    }
    return valueWithFormat;
};



function clearErrorFomr(){
    $("#wizard-form .form-control.error").removeClass("error");
    $("#wizard-form .content label.error").remove();
}
function setFirstStep(){
    $("#wizard-form .first a").click();
    $("#wizard-form .steps li:not(:first)").addClass("disabled").removeClass("done").removeClass("current").removeClass("error");
    $("#wizard-form .steps li").removeClass("error");
    clearErrorFomr();
}

function buscar(){
    $(".popover.confirmation").confirmation("destroy");
    var nombre = $("#clientes-nombre-val").val();
    var calle = $("#clientes-calle-val").val();
    var numero = $("#clientes-numero-val").val();
    var colonia = $("#clientes-colonia-val").val();
    var clave = $("#clientes-clave-val").val();
    var tipoBusqueda = $("#clientes-filter-select").val();
    var tipoVigente = $("#clientes-vigente-val").val();
    clientesDT.column(2).search("");
    clientesDT.column(3).search("");
    clientesDT.column(4).search("");
    clientesDT.column(5).search("");
    clientesDT.column(19).search("");
    clientesDT.column(9).search("");
    clientesDT.column(30).search("");

    if(tipoBusqueda != ""){
        if(tipoBusqueda == "nom"){
            if(nombre == ""){
                showGeneralMessage("Debe escribir el nombre a buscar", "warning", true);
                return;
            }
            if(nombre.length < 2){
                showGeneralMessage("Debe escribir almenos dos caracteres", "warning", true);
                return;
            }
            clientesDT.column(9).search(nombre);
        }
        if(tipoBusqueda == "cla"){
            if(clave == ""){
                showGeneralMessage("Debe escribir la clave a buscar", "warning", true);
                return;
            }
            clientesDT.column(3).search(clave);
        }
        if(tipoBusqueda == "dir"){
            if(calle == "" && numero == "" && colonia == ""){
                showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
                return;
            }

            if(calle == "" && numero == "" && colonia == ""){
                showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
                return;
            }

            if(calle == "" && colonia == ""){
                showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
                return;
            }

            if(numero == "" && colonia == ""){
                showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
                return;
            }
            if(calle){
                calle = calle.replace(/\s/g, '');
            }
            if(numero){
                numero = numero.replace(/\s/g, '');
            }
            clientesDT.column(4).search(calle);
            clientesDT.column(5).search(numero);
            clientesDT.column(19).search(colonia);
        }
    }

    if(tipoBusqueda == "ruta"){
        clientesDT.column(30).search($("#clientes-ruta-filter-val").val().trim());
        clientesDT.column(32).search($("#clientes-rutavalidada-filter-val").val().trim());
    }

    clientesDT.column(2).search(tipoVigente);


    clientesDT.draw();

    // if(!$("#colapse-busqueda").hasClass("collapsed")){
    //     $("#colapse-busqueda").click();
    // }
}

function initMap() {
    if(map == null){
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: CENTER_MAP_LAT, lng: CENTER_MAP_LON},
            zoom: initZoom
        });
    }

    geocoder = new google.maps.Geocoder();

    document.getElementById('cliente-search-location').addEventListener('click', function() {
        geocodeAddress(map);
    });

    document.getElementById('cliente-input-gelocation').addEventListener('keypress', function(e) {
        if(e.which == 13){
            geocodeAddress(map);
        }
    });

    google.maps.event.trigger(map, 'resize');
    google.maps.event.trigger(map, 'resize');
}

function geocodeAddress(resultsMap) {
    var address = document.getElementById('cliente-input-gelocation').value;
    if(address == "") return;
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            marker = createMarker(map, marker, results[0].geometry.location.lat(), results[0].geometry.location.lng(), initZoom);
            // resultsMap.setCenter(results[0].geometry.location);
            // var marker = new google.maps.Marker({
            //     map: resultsMap,
            //     position: results[0].geometry.location
            // });
        } else {
            if(status === google.maps.GeocoderStatus.ZERO_RESULTS){
                showGeneralMessage("No se encontraton resultados", "warning", true);
            }
            else{
                showGeneralMessage("Ocurrió un error al realizar la búsqueda", "warning", true);
            }
        }
    });
}

function initMapClickListener(){
    map.addListener('click', function(e) {
        var lat = e.latLng.lat();
        var lng = e.latLng.lng();
        updateLatLng(lat, lng);
        setCenterMap(map, lat, lng, initZoom);
        marker = createMarker(map, marker, e.latLng.lat(), e.latLng.lng(), initZoom, true);
    });
}

function clearMapClickListener(){
    google.maps.event.clearListeners(map, 'click');
}
function eliminarCLiente(id){
    $.ajax({
        url: "/clientes/delete/" + id,
        success: function(){
            showGeneralMessage("La información se guardo correctamente", "success", true);
            clientesDT.draw();
        },
        /*error: function(){
            showGeneralMessage("Ocurrió un error al guardar la información", "error", true);
        }*/
    });
}

function showDefautlsForm(){
    initMap();
    var d = new Date();
    var mes = d.getMonth();
    // var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    $("#ultimo_mes_pago").val(1);
    var anio = d.getFullYear();
    $("#ultimo_anio_pago").val(anio);
    $("#tipo_cliente1")[0].checked = true;

    $(".validate-required").removeAttr("required");
    $(".validate-required").each(function(){
        var text = $(this).prev().text();
        $(this).prev("label").html(text.replace(" *", ""));
    });
    clientes_rs[0].selectize.clearOptions();
    for(var i in razones_sociales){
        var rs = razones_sociales[i];
        if(rs.fisica == "true"){
            clientes_rs[0].selectize.addOption(rs);
        }
    }
    // clientes_rs[0].selectize.clearOptions();
}

function calcularTamboreo(){
    var valorTamboreo = $("#facturacion_tamboreo").val();
    var valorCantidad = $("#cantidad_tamboreo").val();
    if(valorCantidad != "" && valorTamboreo != ""){
        var $elOption = $("#facturacion_tamboreo :selected");
        var iva = $elOption.attr("data-iva");
        var siniva = $elOption.attr("data-siniva");
        var total = (parseFloat(valorCantidad) * parseFloat(iva)).toFixed(2);
        $("#tamboreo_iva").val(total);
        $("#tamboreo_sin_iva").val((parseFloat(valorCantidad) * parseFloat(siniva)).toFixed(2));
        $("#mensualidad").val(total);
    }
    else{
        $("#tamboreo_iva").val("");
        $("#tamboreo_sin_iva").val("");
        $("#mensualidad").val(0.00);
    }
}

function loadModalDescuento() {
    if(clientesDT.rows('.selected').data().length == 0){
        showGeneralMessage("Seleccione un registro", "warning", true);
        return ;
    }

    var rowdata = clientesDT.row("tr.selected").data();
    if(rowdata[26] == false){
        showGeneralMessage("El cliente no esta vigente", "warning", true);
        return ;
    }

    if(rowdata[25] == true){
        showGeneralMessage("El descuento solo se puede aplicar a casa habitación", "warning", true);
        return ;
    }
    
    mesClienteDescuento = rowdata[11];
    anioClienteDescuento = rowdata[12];

    var rowid = clientesDT.row("tr.selected").node().id;
    idClienteDescuento = rowid.split("-")[1];

    $.ajax({
        url: "/clientes/getdescuento/" + idClienteDescuento,
        type: 'GET',
        dataType: "json",
        success: function(json){
            if(!json.lOk)
            {
                showGeneralMessage(json.cMensaje, "warning", true);
                return ;
            }
            else {
                fnLimpiaFormModal();
                $("#clave").val(idClienteDescuento);
                setDateMesAnio(mesClienteDescuento, anioClienteDescuento);
                $("#clientes-descuento-modal").modal({backdrop: 'static', keyboard: false, show: true});
            }

        },
        /*error: function(){
        }*/
    });

}

/**
 * Setea los datos del cliente en los campos del formulario
 */
var setClienteInfo = function($indexCliente, $data) {
    $('#ultimo_anio_pago').val($data['ultimo_anio_pago']);
    $('#ultimo_mes_pago').val($data['ultimo_mes_pago']);
    //$('#mensualidad').val($data['mensualidad']);
    $('#calle').val($data['calle']);
    $('#calle_letra').val($data['calle_letra']);
    $('#tipo_calle').val($data['tipo_calle']);
    $('#numero').val($data['numero']);
    $('#numero_letra').val($data['numero_letra']);
    $('#tipo_numero').val($data['tipo_numero']);
    $('#cruza1').val($data['cruza1']);
    $('#letra_cruza1').val($data['letra_cruza1']);
    $('#tipo_cruza1').val($data['tipo_cruza1']);
    $('#cruza2').val($data['cruza2']);
    $('#letra_cruza2').val($data['letra_cruza2']);
    $('#tipo_cruza2').val($data['tipo_cruza2']);

    var dmonotoCobro = parseFloat($data['monto_cobro']);

    //adcionales
    if ($data['cobro_manual'] == true)
    {
        if ($('#cliente-info-modal .modal-title').html() != 'Información') //Si es modo nuevo, modificar
        {
            $("#mensualidad").removeAttr("disabled");             //quita la clase
        }
        $("#chMontoManual:checkbox").attr('checked', true);   //marca check
        $("#mensualidad").val(dmonotoCobro);                  //Importe Manual
    }
    else
    {
        setColoniaRuta($data['idcolonia'], $data['idruta']);
        $("#chMontoManual:checkbox").attr('checked', false);   //marca check
        $("#mensualidad").attr('disabled', 'disabled');        //Vuelve a deshabilitar el control
    }
    idcoloniaForm[0].selectize.setValue($data['idcolonia'], true);
    idrutaSelectize[0].selectize.setValue($data['idruta'], true);
//        $('#idcolonia').val($data['colonias'][0].id);
    $("#chMontoManual").prop("disabled", true);
    $("#mensualidad").attr('disabled', 'disabled');

    $('#localidad').val($data['localidad']);
    $('#apepat').val($data['apepat']);
    $('#apemat').val($data['apemat']);
    $('#nombres').val($data['nombres']);

    $('#apepat_propietario').val($data['apepat_propietario']);
    $('#apemat_propietario').val($data['apemat_propietario']);
    $('#nombres_propietario').val($data['nombres_propietario']);
    $('#folio_catastral').val($data['folio_catastral']);
    updateLatLng($data['latitud'], $data['longitud']);

    $('#referencia_ubicacion').val($data['referencia_ubicacion']);

    $('#direccion_otro').val($data['direccion_otro']);
    $('#telefono').val($data['telefono']);
    $('#correo').val($data['correo']);
    $('#observacion').val($data['observacion']);

    $('#idestatus_cliente').val($data['idestatuscliente']).change();
    $('#idtarifa_colonia').val($data['idtarifa_colonia']);
        $('#idruta').val($data['idruta']);
    $('#idtipo_contrato').val($data['idtipo_contrato']);
    $('#idtipo_descuento').val($data['idtipo_descuento']);


    $("#nombre_sucursal").val($data['nombre_sucursal']);
    $("#contacto_servicio").val($data['contacto_servicio']);
    $("#contacto_servicio_tel").val($data['contacto_servicio_tel']);
    $("#contacto_servicio_ext").val($data['contacto_servicio_ext']);
    $("#contacto_pago1").val($data['contacto_pago1']);
    $("#contacto_pago1_tel").val($data['contacto_pago1_tel']);
    $("#contacto_pago1_ext").val($data['contacto_pago1_ext']);
    $("#contacto_pago2").val($data['contacto_pago2']);
    $("#contacto_pago2_tel").val($data['contacto_pago2_tel']);
    $("#contacto_pago2_ext").val($data['contacto_pago2_ext']);
    $("#metodo_pago").val($data['idmetodo_pago']);
    $("#numero_contrato").val($data['numero_contrato']);
    $("#periodicidad_facturacion").val($data['periodicidad_facturacion']);
    $("#tipo_facturacion").val($data['tipo_facturacion']);
    $("#monto_tasa_fija").val($data['monto_tasa_fija']);
    $("#facturacion_tamboreo").val($data['idfacturacion_tamboreo']);
    $("#cantidad_tamboreo").val($data['cantidad_tamboreo']);
    if($data['tipo_facturacion'] == "Fija"){
        $(".ocultar-fija").show();
        $(".ocultar-tamboreo").hide();

    }
    else{
        $(".ocultar-fija").hide();
        $(".ocultar-tamboreo").show();
        $("#cantidad_tamboreo").keyup();
    }

    if($data["fisica"]){
        $("input[name=tipo_cliente][value=fisica]")[0].checked = true;
        $(".ocultar-moral").hide();
        $(".ocultar-fisica").show();
        /*$("#wizard-form-t-3").hide();
        $("#wizard-form-t-4 .number").html("4.");
        $("#wizard-form-t-5 .number").html("5.");*/
        $("#wizard-form-t-4").hide();
        $("#wizard-form-t-5 .number").html("5.");
        $("#wizard-form-t-6 .number").html("6.");
    }
    else{
        $("input[name=tipo_cliente][value=moral]")[0].checked = true;
        $(".ocultar-moral").show();
        $(".ocultar-fisica").hide();
        /*$("#wizard-form-t-3").show();
        $("#wizard-form-t-3 .number").html("4.");
        $("#wizard-form-t-4 .number").html("5.");
        $("#wizard-form-t-5 .number").html("6.");*/
        $("#wizard-form-t-4").show();
        $("#wizard-form-t-4 .number").html("4.");
        $("#wizard-form-t-5 .number").html("5.");
        $("#wizard-form-t-6 .number").html("6.");
    }

    $("#chkmarginado").prop("disabled", false);
    $("#chkmarginado").prop("checked",$data['marginado']);
    $("#chkmarginado").prop("disabled", true);
    $("#idestatus_cliente").prop("disabled", true);
    $("#chkabandonado").prop("checked",$data['abandonado']);
    $("#chkprivada").prop("checked",$data['privada']).change();
    $("#nombre_comercio").val($data['nombre_comercio']);
    //$("#nombre_comercio").prop("disabled", true);
    $("#nombre_privada").val($data['nombre_privada']);

    clientes_rs[0].selectize.setValue($data['idrazon_social']);

    if($data['latitud'] != null && $data['latitud'] != '' && $data['longitud'] != null && $data['longitud'] != '')
    {
        $("#latitud").val($data['latitud']);
        $("#longitud").val($data['longitud']);
    }

    if(map != null){
//            initMap();
        if($data['latitud'] && $data['longitud'])
        {
            marker = createMarker(map, marker, $data['latitud'], $data['longitud'], initZoom);
        }
    }

    if($data["activo"] && $data["vigente"]) {
        if($data["ultimo_mes_pago"] && $data["ultimo_anio_pago"]){
            $("#ultimo_anio_pago").attr("disabled", true);
            $("#ultimo_mes_pago").attr("disabled", true);
        }

        if(isEditMesAnio){
            $("#ultimo_anio_pago").attr("disabled", false);
            $("#ultimo_mes_pago").attr("disabled", false);
        }
    }
    else{
        if(!$data["conpago"]) {
            $("#ultimo_anio_pago").attr("disabled", false);
            $("#ultimo_mes_pago").attr("disabled", false);
        }
        else{
            $("#ultimo_anio_pago").attr("disabled", true);
            $("#ultimo_mes_pago").attr("disabled", true);
        }
    }

    $("#idcomprobante").val($data['id_tipo_comprobante']);
    $("#ididentificacion").val($data['id_tipo_identificacion']);
    $('#tel_contacto').val($data['telefono_contacto']);


        //$data['ruta_foto_predio'].split("/");
    if($data['ruta_comprobante'] != null && $data['ruta_comprobante'] != "") {
        //$("#imgcomprobanteinfodiv").html("<a href=" + $data['ruta_comprobante'] + " target='_blank' style='color: #337ab7; font-weight: 500;'>" + $data['ruta_comprobante'].split("/")[4] + "</a>");
        $("#imgcomprobanteinfodiv").html("<a class='btn btn-sm btn-primary' href=" + $data['ruta_comprobante'] + " target='_blank' style='color: #337ab7; font-weight: 500;' role='button'><i class='fa fa-eye' style='color: #FFFFFF;'></i></a>");
        $("#imgcomprobante").val($data['ruta_comprobante'].split("/")[4]);
    }
    else {
        $("#imgcomprobanteinfodiv").html("");
        $("#imgcomprobante").val("");
        $(".infocomp").css("display","none");
    }

    if($data['ruta_identificacion'] != null && $data['ruta_identificacion'] != "") {
        //$("#imgidentificacioninfodiv").html("<a href=" + $data['ruta_identificacion'] + " target='_blank' style='color: #337ab7; font-weight: 500;'>" + $data['ruta_identificacion'].split("/")[4] + "</a>");
        $("#imgidentificacioninfodiv").html("<a class='btn btn-sm btn-primary' href=" + $data['ruta_identificacion'] + " target='_blank' style='color: #337ab7; font-weight: 500;' role='button'><i class='fa fa-eye' style='color: #FFFFFF;'></i></a>");
        $("#imgIden").val($data['ruta_identificacion'].split("/")[4]);
    }
    else {
        $("#imgidentificacioninfodiv").html("");
        $("#imgIden").val("");
        $(".infoiden").css("display","none");
    }

    if($data['ruta_foto_predio'] != null && $data['ruta_foto_predio'] != "") {
        //$("#imgfotopredioinfodiv").html("<a href=" + $data['ruta_foto_predio'] + " target='_blank' style='color: #337ab7; font-weight: 500;'>" + $data['ruta_foto_predio'].split("/")[4] + "</a>");
        $("#imgfotopredio").val($data['ruta_foto_predio'].split("/")[4]);
        $("#imgfotopredioinfodiv").html("<a class='btn btn-sm btn-primary' href=" + $data['ruta_foto_predio'] + " target='_blank' style='color: #337ab7; font-weight: 500;' role='button'><i class='fa fa-eye' style='color: #FFFFFF;'></i></a>");
    }
    else {
        //$("#imgfotopredioinfodiv").html("");
        $("#imgfotopredio").val("");
        $("#imgfotopredioinfodiv").html("");
        $(".infofoto").css("display","none");
    }
};

function fnLimpiaFormModal()
{
    $("#clave").val("");
    $("#cOficio").val("");
    $("#fInicio").val("");
    $("#fFin").val("");
    $("#activo").prop('checked', true);
    rutaDropzonePensionado.removeAllFiles(true);
}

function convertMesToNumber(mestxt){
    let mesUpper = mestxt.toUpperCase();
	let v_mes = 0;
	switch(mesUpper){
		case 'ENERO' : v_mes = 1;break;
		case 'FEBRERO':v_mes = 2; break;
		case 'MARZO': v_mes = 3;break;
		case 'ABRIL': v_mes = 4;break;
		case 'MAYO': v_mes = 5;break;
		case 'JUNIO': v_mes = 6;break;
		case 'JULIO': v_mes = 7;break;
		case 'AGOSTO': v_mes = 8;break;
		case 'SEPTIEMBRE': v_mes = 9;break;
		case 'OCTUBRE': v_mes = 10;break;
		case 'NOVIEMBRE': v_mes = 11;break;
		case 'DICIEMBRE': v_mes = 12;break;
	}//fin:switch(mesUpper)
	return v_mes;
}//fin:convertMesToNumber

function convertMesNumberToString(mes){
	return mes < 10 ? "0"+mes : mes;
}//fin:convertMesToNumber

function setDateMesAnio(strMes, strAnio){
    let mesTxt = "DICIEMBRE";
    let anioTxt = "2009"
    if(strMes != "" && strMes != null){
        mesTxt = strMes;
    }
    if(strAnio != "" && strAnio != null){
        anioTxt = strAnio;
    }
    let anioNum = parseInt(anioTxt);
    let mesNum = convertMesToNumber(mesTxt);
    if(mesNum == 12){
        mesNum = 1;
        anioNum += 1;
    } else {
        mesNum += 1;
    }
    let mesFormat = convertMesNumberToString(mesNum);
    let fechIni = mesFormat + "/" + anioNum;
    $('#fInicio').datepicker('setStartDate', fechIni);
    $('#fInicio').datepicker('setDate', fechIni);
    $("#fInicio").val(fechIni);
    $('#fFin').datepicker('setStartDate', fechIni);
    $('#fFin').datepicker('setDate', fechIni);
    $("#fFin").val(fechIni);
}//fin:setDateMesAnio

//Funcion que habilita o des habilita el textboxt de monto
$(document).ready(function() {
    $("#chMontoManual").click(function () {
        $("#mensualidad").val();
        if ($("#chMontoManual").is(':checked')) {
            $("#mensualidad").removeAttr("disabled");
            $("#mensualidad").attr("required", "required");
        } else {
            $("#mensualidad").removeClass("error");
            $("#mensualidad").removeClass("error");
            $("#mensualidad-error").html("");
            $("#mensualidad-error").hide();
            $("#mensualidad").attr('disabled', 'disabled');
        }
    });

    $("#idestatus_cliente").on("change", function () {
        let $checkIscmoercio = false;
        if(this.value != "" && this.value != null)
        {
            $checkIscmoercio = $("#idestatus_cliente option:selected").attr("data") == 'true' ? true : false;
            //console.log($checkIscmoercio);
        }
        //console.log($checkIscmoercio);
        $("#nombre_comercio").prop("required", $checkIscmoercio);
        $("#nombre_comercio").prop("disabled", !$checkIscmoercio);
        $("#nombre_comercio").val("");
        if(!$checkIscmoercio){
            $("#nombre_comercio").removeClass("error");
            $("#nombre_comercio-error").remove();
        }
    });

    $("#chkprivada").on("change", function () {
        $("#nombre_privada").prop("required", this.checked);
        $("#nombre_privada").prop("disabled", !this.checked);
        $("#nombre_privada").val("");
        if(!this.checked){
            $("#nombre_privada").removeClass("error");
            $("#nombre_privada-error").remove();
        }
    });
});
