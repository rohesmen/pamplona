$(document).ready(function () {
    selectfcobratario = $("#fcobratario").selectize();
    selectizefcobratario = selectfcobratario[0].selectize;
    selectizefcobratario.setValue("", false);

    selectcobratario = $("#cobratario").selectize();
    selectizecobratario = selectcobratario[0].selectize;
    selectizecobratario.setValue("", false);

    $("#fclave").inputmask({ "mask": "9", "repeat": 15, "greedy": false});

    $("#ftipobusqueda").on("change", function(){
        var valor = this.value;
        $("#filtro-dispositivo").hide();
        $("#filtro-clave").hide();
        $("#filtro-cobratario").hide();

        $("#fdispositivo").val("");
        selectizefcobratario.setValue("", false);
        $("#fclave").val("");
        $("#filtro-" + valor).show();
    });
    
    $("#dispositivo-search-do").on("click", function () {
        var dispisitivo = $("#fdispositivo").val().trim();
        var cobratario = selectfcobratario.val();
        var id = $("#fclave").val().trim();

        dispositivosDT.column(3).search(id);
        dispositivosDT.column(4).search(dispisitivo);
        dispositivosDT.column(7).search(cobratario);
        dispositivosDT.column(8).search($("#fvigente").val());
        dispositivosDT.draw();
    });

    $("#addDispositivoDo").on("click", function () {
        $("#dispositivo-info-modal").modal("show");
    });

    $("#btnGuardar").on("click", function () {
        var id = $("#id").val();
        var dispisitivo = $("#dispositivo").val().trim();
        var cobratario = $("#cobratario").val().trim();
        var descripcion = $("#descripcion").val().trim();

        var error = "";
        if(dispisitivo == ""){
            error += "- Dispositivo no puede estar vacio<br>";
        }

        if(cobratario == ""){
            error += "- Debe seleccionar un cobratario<br>";
        }

        if(error != ""){
            showGeneralMessage(error, "warning", true);
            return
        }

        var data = {
            id: id ? id : null,
            dispisitivo: dispisitivo,
            cobratario: cobratario,
            descripcion: descripcion ? descripcion : null
        }
        $.ajax({
            url: "/dispositivos/save",
            type: "POST",
            data: JSON.stringify(data),
            beforeSend: function(){
                $("#btnGuardar").prop("disabled", true);
            },
            success: function () {
                showGeneralMessage("La información se guardo correctamente", "success", true);
                dispositivosDT.draw(false);
                $("#dispositivo-info-modal").modal("hide");
            },
            complete: function () {
                $("#btnGuardar").prop("disabled", false);
            }
        })
    });

    $("#dispositivo-info-modal").on("hidden.bs.modal", function () {
        $("#dispositivo-info-modal .modal-title").html("Nuevo dispositivo");
        $("#id").val("");
        $("#dispositivo").val("");
        selectizecobratario.setValue("", false);
        $("#descripcion").val("");

        $("#dispositivo").prop("disabled", false);
        selectizecobratario.enable();
        $("#descripcion").prop("disabled", false);
        $("#btnGuardar").prop("disabled", false);
    });

    $("#dispositivos-datatable").on( 'click', '.dispositivos-edit', function () {
        $("#dispositivo-info-modal .modal-title").html("Editar dispositivo");
        var $tr = $(this).closest("tr");
        var data = dispositivosDT.row($tr).data();
        $("#id").val(data[3]);
        $("#dispositivo").val(data[4]);
        selectizecobratario.setValue(data[7], false);
        $("#descripcion").val(data[6]);
        $("#dispositivo-info-modal").modal("show");
    });

    $("#dispositivos-datatable").on( 'click', '.dispositivos-info', function () {
        $("#dispositivo-info-modal .modal-title").html("Consultar dispositivo");
        var $tr = $(this).closest("tr");
        var data = dispositivosDT.row($tr).data();
        $("#id").val(data[3]);
        $("#dispositivo").val(data[4]);
        selectizecobratario.setValue(data[7], false);
        $("#descripcion").val(data[6]);

        $("#dispositivo").prop("disabled", true);
        selectizecobratario.disable();
        $("#descripcion").prop("disabled", true);

        $("#btnGuardar").prop("disabled", true);

        $("#dispositivo-info-modal").modal("show");
    });

    $("#dispositivos-datatable").on( 'click', '.dispositivos-delete', function () {
        $(".popover.confirmation").confirmation("destroy");
        var $tr = $(this).closest("tr");
        var id = $(this).data("id");
        $(this).confirmation({
            rootSelector: "body",
            container: "body",
            singleton: true,
            popout: true,
            btnOkLabel: "Si",
            onConfirm: function() {
                $.ajax({
                    url: "/dispositivos/delete/" + id,
                    success: function () {

                        showGeneralMessage("La información se guardo correctamente", "success", true);
                        dispositivosDT.draw(false);
                    }
                });
            },
            onCancel: function() {
                $(this).confirmation('destroy');
            },
        });

        $(this).confirmation("show");
    });

    $("#dispositivos-datatable").on( 'click', '.dispositivos-history', function () {
        var $tr = $(this).closest("tr");
        var data = dispositivosDT.row($tr).data();
        $.ajax({
            url: "/dispositivos/history/" + data[4],
            beforeSend: function(){
                dispositivosSeguiDT.clear().draw();
            },
            success: function (resp) {
                var json = JSON.parse(resp);
                dispositivosSeguiDT.rows.add(json).draw();
                $("#dispositivo-usuarios-info-modal .modal-title").html("Usuarios de: " + data[4]);
                $("#dispositivo-usuarios-info-modal").modal("show");
            }
        });
    });

    dispositivosDT = $("#dispositivos-datatable").DataTable({
        "dom": '<"clear"><"top container-float"<"filter-located">f><"datatable-scroll"rt><"bottom"lip><"clear">',
        "autoWidth": false,
        "paging": true,
        "select": true,
        "info": true,
        // searching: true,
        "processing": true,
//            "scrollY":        "506px",
        "scrollCollapse": false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
        "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
        "pageLength": 10,
        "order": [[ 3, 'desc' ]],
        responsive: {
            details: {
                type: 'column',
                target: 1
            }
        },
        "processing": true,
        "serverSide": true,
        // "deferLoading": 0,
        ajax: {
            url: "/dispositivos/buscar",
        },
        //responsive: true,
        "columnDefs": [
            {
                targets: [0],
                orderable: false,
                searchable: false,
                className: 'dt-center no-wrap',
                visible: coacciones != 'no'
            },
            { targets: [1], orderable: false, className: 'control', searchable: false},
            { targets: [-1, -2], visible: false },
            { targets: '_all', visible: true }
        ],
        "lengthChange": true
    });

    dispositivosSeguiDT = $("#dispositivos-seguimiento-datatable").DataTable({
        "dom": '<"clear"><"top container-float"<"filter-located">f><"datatable-scroll"rt><"bottom"lip><"clear">',
        "autoWidth": false,
        "paging": true,
        "select": true,
        "info": true,
        // searching: true,
        "processing": true,
//            "scrollY":        "506px",
        "scrollCollapse": false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
        "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
        "pageLength": 10,
        "order": [],
        "processing": true,
        "columnDefs": [
            { targets: '_all', visible: true }
        ],
        "lengthChange": true
    });
});