$(document).ready(function(){
    const SIGLA_CASA_HABITACION = 'CH';
    $("#clientes-datatable").off( 'click', '.cliestat-change-status');
    $("#clientes-datatable").on( 'click', '.cliestat-change-status', function () {
        var $tr = $(this).closest("tr");
        var idAttr = $tr.attr("id").split("-");
        var id = idAttr[1];
        var idestatus = $(this).data("id");
        var sigla = $(this).data("sigla");
        var data = clientesDT.row($tr).data();
        if ( $tr.hasClass('selected') ) {
            //$tr.removeClass('selected');
        }
        else {
            clientesDT.$('tr.selected').removeClass('selected');
            $tr.addClass('selected');
        }

        $("#cliestat-idcliente").val(id);
        $("#cliestat-idestatus").val(idestatus);
        $("#cliestat-sigla").val(sigla);
        $("#cliestat-nomcom").prop("disabled", false);
        if(sigla == SIGLA_CASA_HABITACION){
            $("#cliestat-nomcom").prop("disabled", true);
        }
        if(data["25"]){
            $("#cliestat-nomcom").val(data[20]);
        }
        $("#cliente-cliestat-modal").modal("show");
    });

    $("#cliente-cliestat-modal").on("hidden.bs.modal", function () {
        $("#cliestat-nomcom").val("");
        $("#cliestat-motivo").val("");
    });
    
    $("#cliestat-btnGuardar").on("click", function () {
        var sigla = $("#cliestat-sigla").val();
        var nomcom = $("#cliestat-nomcom").val().trim();
        var idcliente = $("#cliestat-idcliente").val();
        var idestatus = $("#cliestat-idestatus").val();
        var motivo = $("#cliestat-motivo").val().trim();
        var errores = '';
        if(sigla != SIGLA_CASA_HABITACION && nomcom == "") {
            errores += '- Nombre comercio es obligatorio<br>'
        }
        if(sigla == SIGLA_CASA_HABITACION) {
            nomcom = null;
        }
        if(motivo == ""){
            errores += '- Motivo es obligatorio'
        }
        if(errores){
            showGeneralMessage(errores, 'warning', true)
            return;
        }
        var data = {
            idcliente: idcliente,
            idestatus: idestatus,
            motivo: motivo,
            nomcom: nomcom
        }
        $.ajax({
            url: '/clientes/editsta',
            type: "POST",
            data: JSON.stringify(data),
            beforeSend: function(){
                $("#cliestat-btnGuardar").prop("disabled", true);
                $("#cliestat-btnGuardar").html('<i class="fa fa-spinner fa-spin"></i> Espere...');
                $("#cliestat-btnGuardar").prev().prop("disabled", true);
            },
            success: function () {
                buscar();
                showGeneralMessage('La información se guardo correctamente', 'success', true);
                $("#cliente-cliestat-modal").modal("hide");
            },
            error: function () {
                showGeneralMessage("Ocurrió un error al guardar la información", "danger", true);
            },
            complete: function () {
                $("#cliestat-btnGuardar").prop("disabled", false);
                $("#cliestat-btnGuardar").html('Guardar');
                $("#cliestat-btnGuardar").prev().prop("disabled", false);
            }
        })
    });
});