
var dataTable = "";

var filtro = {
    fechaInicio: "",
    fechaFin: "",
    idCobratario: ""
};

$(function() {

    $("#btnExportCorte").on("click", function () {
        $.ajax({
            url: "/bitmens/export",
            type: "POST",
            data: JSON.stringify(filtro),
            success: function (resp) {
                var json = JSON.parse(resp);
                showGeneralMessage("Los datos han sido exportados correctamente", "success", true);

                var body = [
                    ['Cliente', 'Mensualidad', 'Cobratario', 'Fecha', 'Origen', 'Motivo']
                ];
                $.each(json, function(key, value) {
                    var row = [value.cliente, value.mensualidad, value.nombre_completo_usuario, value.fecha, value.origen, value.motivo];
                    row.join(',');
                    body.push(row);
                });

                var title = "Mensualidades";
                var subject = "sheet export";
                var author = "Pamplona";
                var hoja = "Hoja 1";
                var file_name = "Mensualidades";

                exportar_excel(body, title, subject, author, hoja, file_name);
            },
            error: function () {
                showGeneralMessage("Ocurrio un error al obtener la intformacion", "error", true);
            }
        });
    });
    
    /*var dataTable = "";
    var dataTablePagos = "";*/

    $('#corte_busqueda_fIni').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });
    // datetimepicker({defaultDate: new Date(),format: 'DD-MM-YYYY', maxDate: new Date(), showClose: true});

    $('#corte_busqueda_fFin').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });
    // datetimepicker({defaultDate: new Date(),format: 'DD-MM-YYYY', maxDate: new Date(), showClose: true});

    fnCargaTabla();

    fnfltros();

    $("#corte_busqueda_fIni").on("change", function (e) {
    });

    $("#corte_busqueda_fFin").on("change", function (e) {
    });

});

var fnCargaTabla = function(){
    dataTable = $('#dt-mensualidades').DataTable( {
        // "dom": "<'row'<'col-sm-6'><'col-sm-6'fB>>" +
        // "<'row'<'col-sm-12'tr>>" +
        // "<'row'<'col-sm-12'i>>" +
        // "<'row'<'col-sm-6'l><'col-sm-6'p>>",
        "dom": '<"clear"><"top container-float"<"filter-located">f><rt><"bottom"lip><"clear">',
        "autoWidth": false,
        "paging": true,
        "select": true,
        "info": true,
        // searching: true,
        "processing": true,
        //"scrollY":        "506px",
        "scrollCollapse": false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
        "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
        "pageLength": 10,
        "order": [[ 1, 'desc' ], [ 3, 'desc' ], [ 5, 'desc' ]],
        responsive: {
            details: {
                type: 'column',
                target: 0
            }
        },
        "processing": true,
        "serverSide": true,
        "deferLoading": 0,
        ajax: {
            url: "/bitmens/buscar",
            "dataSrc": function ( response ) {
                if(response.lError) {
                    showGeneralMessage(response.cMensaje, "warning", true);
                    // return {"draw":2,"recordsTotal":0,"recordsFiltered":0,"data":[],"latlngs":[],"ids":[]};
                }
                else
                    return response.data;
            },
            error: function(xhr, status, error){
                var textError = "";
                var tipoError = "";
                $(window).resize();
                $(window).resize();
                if(xhr.status == 401) {
                    window.location = "/";
                }
                else {
                    showGeneralMessage("Ocurrió un error, contacte al administrador", "warning", true);
                }
            }
        },
        //responsive: true,
        "columnDefs": [
            { targets: [0], orderable: false, className: 'control', searchable: false},
            { targets: [1,2,4,5,6,7], visible: true, searchable: true, className: 'text-center' },
            { targets: [8,9,3], visible: false, searchable: true },
            { targets: '_all', visible: true }
        ],
        "lengthChange": true
    });
};

var fnfltros = function(){
    $("#btnSearch").on('click',function(e){

        let lerrorFecha = false;
        dataTable.search("");
        dataTable.column(3).search("");
        dataTable.column(8).search("");
        dataTable.column(9).search("");
        
        if(($('#corte_busqueda_fIni').val() != "") && ($('#corte_busqueda_fFin').val() != "")) {
            lerrorFecha = validaRangoFecha($('#corte_busqueda_fIni').val(), $('#corte_busqueda_fFin').val());
        }

        if(!lerrorFecha) {
            filtro.fechaInicio = $('#corte_busqueda_fIni').val();
            filtro.fechaFin = $('#corte_busqueda_fFin').val();
            filtro.idCobratario = $('#corte_busqueda_cCobratario option:selected').val();
            dataTable.column(3).search(filtro.idCobratario);
            dataTable.column(8).search(filtro.fechaInicio);
            dataTable.column(9).search(filtro.fechaFin);
            dataTable.draw();
        } else {
            showGeneralMessage("La fecha final es menor a la fecha inicial.", "warning", true);
        }

    });
};

function validaRangoFecha(fechaMenor, fechaMayor) {
    let fechaMayorF = formatdDateDDMMYYYY(String(fechaMayor));
    let fechaMenorF = formatdDateDDMMYYYY(String(fechaMenor));
    return (fechaMayorF < fechaMenorF) ? true : false;
}

function formatdDateDDMMYYYY(date) {
    let parts = date.split("/");
    return new Date(parts[2], parts[1] -1, parts[0]);
}