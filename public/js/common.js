$.ajaxSetup({
    // beforeSend: function(){
    //     onBeforeSendAjax();
    // },
    error: function (xhr, status, error) {
        onErrorAjax(xhr, status, error);
    },
    // complete: function(){
    //     onCompleteAjax();
    // }
});

function onErrorAjax(xhr, status, error) {
    var textError = "";
    var tipoError = "";
    if(xhr.status == 400) {
        textError = (error == "Bad Request" ? "Petición incorrecta." : error);
        tipoError = "error";
    }
    else if(xhr.status == 401) {
        $("#sesion-modal").modal({
            backdrop:"static",
            keyboard: false
        });
        return;
    }
    else if(xhr.status == 403) {
        textError = (error == "Forbidden" ? "No cuenta con el permiso." : error);
        tipoError = "error";
    }
    else if(xhr.status == 404){
        textError = (error == "Not Found" ? "Dato no encontrado." : error);
        tipoError = "warning";
    }
    else if(xhr.status == 405) {
        textError = (error == "Method Not Allowed" ? "Método no permitido." : error);
        tipoError = "error";
    }
    else if(xhr.status == 409) {
        textError = (error == "Conflict" ? "Dato con caracteristicas similares fue encontrado." : error);
        tipoError = "warning";
    }
    else if(xhr.status == 413) {
        textError = (error == "Request Entity Too Large" ? "Solicitud demasiado grande." : error);
        tipoError = "error";
    }
    else if(xhr.status == 501) {
        textError = (error == "Not Implemented" ? "Método no implementado." : error);
        tipoError = "error";
    }
    else{
        textError = (error == "Internal Server Error" ? "Ocurrió un error, contacte al administrador" : error);
        tipoError = "error";
    }
    showGeneralMessage(textError, tipoError, true);
}

var updateIndicators = function(){
    $.ajax({
        url: '/militants/indicators',
        type: 'GET',
        success: function(data){
            parent = $('.dropdown-tasks').parent();
            $('.dropdown-tasks').remove();
            parent.append(data);
        },
        error: function(){}
    });
}
var updateMessages = function(){
    $.ajax({
        url: '/militants/messages',
        type: 'GET',
        success: function(data){
            parent = $('.dropdown-messages').parent();
            $('.dropdown-messages').remove();
            parent.append(data);
        },
        error: function(){}
    });
}

var clearFormChangePassword = function(){
    $("#user-password-before").val("");
    $("#user-password-new").val("");
    $("#user-password-confirm").val("");
    $("#user-password-change-error").html("");
    $("#user-password-change-error").removeClass("has-success");
    $("#user-password-change-error").removeClass("has-warning");
    $("#user-password-change-error").removeClass("has-error");
}
var changePassword = function(){
    var minCharacters = 6;
    $("#user-password-change-error").html("");
    $("#user-password-change-error").removeClass("has-success");
    $("#user-password-change-error").removeClass("has-warning");
    $("#user-password-change-error").removeClass("has-error");
    var actualPassword = secureInput($("#user-password-before").val().trim());
    var newPassword = secureInput($("#user-password-new").val().trim());
    var confirmPassword = secureInput($("#user-password-confirm").val().trim());
    if(actualPassword == ''){
        $("#user-password-change-error").addClass("has-warning");
        $("#user-password-change-error").html("<label class='control-label'>Contraseña actual no debe estar vacío</label>");
        return;
    }
    if(newPassword == ''){
        $("#user-password-change-error").addClass("has-warning");
        $("#user-password-change-error").html("<label class='control-label'>Contraseña nueva no debe estar vacío</label>");
        return;
    }
    if(minCharacters > newPassword.length){
        $("#user-password-change-error").addClass("has-warning");
        $("#user-password-change-error").html("<label class='control-label'>El mínimo de caracteres es " + minCharacters + "</label>");
        return;
    }
    if(confirmPassword == ''){
        $("#user-password-change-error").addClass("has-warning");
        $("#user-password-change-error").html("<label class='control-label'>Confirmación de contraseña no debe estar vacío</label>");
        return;
    }
    if(newPassword != confirmPassword){
        $("#user-password-change-error").addClass("has-warning");
        $("#user-password-change-error").html("<label class='control-label'>Confirmación de contraseña no coincide</label>");
        return;
    }

    var data = {
        actual: actualPassword,
        new: newPassword
    }
    $.ajax({
        url: "/users/changePassword",
        type: "POST",
        data: JSON.stringify(data),
        beforeSend: function(){
            $("#user-password-change-error").removeClass("has-success");
            $("#user-password-change-error").removeClass("has-warning");
            $("#user-password-change-error").removeClass("has-error");
            $("#user-password-change-error").html('<i class="fa fa-spinner fa-pulse"></i> Cargando...');
        },
        success: function(){
            $("#user-password-change-error").addClass("has-success");
            $("#user-password-change-error").html("<label class='control-label'>La contraseña se cambio exitosamente</label>");
        },
        error: function(xhr, status, error){
            $("#user-password-change-error").addClass("has-error");
            $("#user-password-change-error").html("<label class='control-label'>" + xhr.responseText + "</label>");
        }
    });
}

function stripTag(content,tag) {
    var div = document.createElement('div');
    div.innerHTML = content;
    var scripts = div.getElementsByTagName(tag);
    var i = scripts.length;
    while (i--) {
        scripts[i].parentNode.removeChild(scripts[i]);
    }
    return div.innerHTML;
}

function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

function secureInput(content){
    content = stripTag(content, 'script');
    content = decodeHtml(content);
    content = content.replace(/[;:'"<>\{\}\[\]\\\/]/gi, '');

    return content;
}

function messages(msg,opts){
    if(opts.dom){
        var fa = opts.fa? '<i class="'+opts.fa+'"></i> ' : ' ';
        parent = document.createElement('div');
        parent.insertAdjacentHTML('beforeend','<div class="msg '+(opts.class? opts.class : '')+'">'+fa+msg+'</div>');
        child = parent.removeChild(parent.firstChild);
        opts.dom.append(child);

        var remove = function(dom){
            dom.parentNode.removeChild(dom);
        }
        obj = {};
        obj.remove = remove.bind(obj,child);

        if(opts.timeout){
            setTimeout(function(){ obj.remove() },opts.timeout);
        }
        return obj;
    }
    return {};
}

$(document).ready(function() {
    $("#sesion-modal").on('shown.bs.modal', function (e) {
        var display = document.querySelector('#countDownSession');
        var timer = 9, minutes, seconds;
        var countDownR = setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.textContent = seconds;

            if (--timer < 0) {
                window.clearInterval(countDownR);
                window.location = "/session/logout";
            }
        }, 1000);
    });

    $("#main-modal").on("show.bs.modal", function (e) {
        var btn = $(e.relatedTarget);
        $(this).find(".modal-content").html('<div class="modal-body text-center"><i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...</div>');
        $(this).find(".modal-content").load(btn.attr("data-src"));
    });

    $("#user-password-change-modal").on("show.bs.modal", function (e) {
        clearFormChangePassword();
        $("#btn-user-password-change").off("click");
        $("#btn-user-password-change").on("click", function(){
            changePassword();
        });
    });
    $("#btn-user-password-change-modal").off('click');
    $("#btn-user-password-change-modal").on('click', function(){
        $("#user-password-before").val("");
        $("#user-password-new").val("");
        $("#user-password-confirm").val("");
        $("#user-password-change-modal").modal('show');
    });

    $('#general-modal').off('hidden.bs.modal');
    $('#general-modal').on('hidden.bs.modal', function (e) {
        if(isABootstrapModalOpen()){
            $("body").addClass("modal-open");
        }
    });
    var url = window.location;
    var element = $('ul.metismenu a').filter(function() {
        var href = (typeof this.href == 'string') ? (this.href.replace('#','')) : (this.href);
        var urlhref = (typeof url.href == 'string') ? (url.href.replace('#','')) : (url.href);
        return this.href == url || urlhref == href; //url.href.indexOf(this.href) == 0;
    })
    if(!$(element[1]).hasClass("dropdown-toggle")){
        $(element[1]).parent().addClass('active').parent().addClass("collapse in").parents("li:first").addClass("active");
    }
});

function inputOnlyNumbers(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]/;
    if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}

function inputOnlyNumbers2(evt, field){
    field.value = field.value.replace(/[^0-9]/g,'');
}

function getTextValueUpper(value) {
    var valueWithFormat = value;
    if(value == null || value == undefined || value.trim() == "") {
        //valueWithFormat = null;//"";
        valueWithFormat = "";
    } else {
        valueWithFormat = value.trim().toUpperCase();
    }
    return valueWithFormat;
}

function validateClaveElector(value) {
    var pattern=/[a-z|A-Z]{6}\d{6}\d{2}[a-z|A-Z]{1}\d{1}[a-z|A-Z|\d]{2}$/;
    if(value != null && value != undefined && value.trim() != "" && !value.match(pattern)) {
        return false;
    }
    return true;
}

function validateDate(value) {
    var pattern=/^\d{4}([/-])\d{2}\1\d{2}$/;
    if(value != null && value != undefined && value.trim() != "" && !value.match(pattern)) {
        return false;
    }
    return true;
}

function validateTextRequired(value) {
    if(value == null || value == undefined || value.trim() == "") {
        return false;
    }
    return true;
}

function validateText(value) {
    var pattern=/[a-z|A-Z]/;
    if(value != null && value != undefined && value.trim() != "" && !value.trim().match(pattern)) {
        return false;
    }
    return true;
}

function validateTextWithWhitespaces(value) {
    var pattern=/[a-z|A-Z|\s]/;
    if(value != null && value != undefined && value.trim() != "" && !value.trim().match(pattern)) {
        return false;
    }
    return true;
}

function validateNumber(value) {
    var pattern=/[0-9]/;
    if(value != null && value != undefined && value.trim() != "" && !value.match(pattern)) {
        return false;
    }
    return true;
}

function validateEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function getTextValue(value) {
    var valueWithFormat = value;
    if(value == null || value == undefined || value.trim() == "") {
        //valueWithFormat = null;//"";
        valueWithFormat = "";
    } else {
        valueWithFormat = value.trim();
    }
    return valueWithFormat;
}

function isABootstrapModalOpen() {
    return $('.modal.in').length > 0;
}

function showGeneralMessage(mensaje, tipo, temporal){
    toastr.options.closeButton = true;
    toastr.options.positionClass = "toast-top-center";
    toastr.options.timeOut = 0;
    toastr.options.preventDuplicates = true;
    if(temporal){
        toastr.options.timeOut = 3000;
        toastr.options.progressBar = true;
    }
    if(tipo == "success"){
        toastr.success(mensaje);
    }
    else if(tipo == "warning"){
        toastr.warning(mensaje);
    }
    else if(tipo == "info"){
        toastr.info(mensaje);
    }
    else{
        toastr.error(mensaje);
    }
}
function getTodayddmmyy(){
    var today = new Date();
    today = today.toISOString().substring(0, 10);
    var auxdate = today.split("-");
    auxdate = auxdate.reverse();
    today = auxdate.join("/");
    return today;
}

var fnBuscarDT = function(_DataTable, index, val){
    if(index === ""){
        $(_DataTable).DataTable().search(val).draw();
    }else{
        $(_DataTable).DataTable().column(index).search(val).draw();
    }
}

var fnAjax = function(_url, _type, _data, _successGen, _callback){
    _successGen = typeof _successGen == "undefined" ? true : _successGen;
    _json = {"lError": true, "cError": "Error indesperado."};

    if(_type.toLowerCase() == "post"){
        $.ajax({
            url: _url,
            type: _type,
            data: JSON.stringify(_data),
            beforeSend: function(){
                // disableButtons();
                // $(".tab-content :input").prop("disabled", true);
            },
            success: function(data){
                if(_successGen){
                    showGeneralMessage("La información se guardo correctamente", "success");
                    setTimeout(function () {
                        // window.location.href = "/importcensus";
                    }, 2000);
                    //return true;
                }else{
                    //console.log(data);
                    _json = data;
                    if (typeof _callback === "function") {
                        _callback(_json);
                    }
                    //return data;
                }
            },
            error: function(xhr, status, error){
                var textError = "";
                var tipoError = "";

                if(xhr.status == 400) {
                    textError = (error == "Bad Request" ? "Petición incorrecta." : error);
                    tipoError = "error";
                }
                else if(xhr.status == 401) {
                    $("#sesion-modal").modal({
                        backdrop:"static",
                        keyboard: false
                    });
                    return;
                }
                else if(xhr.status == 403) {
                    textError = (error == "Forbidden" ? "No cuenta con el permiso." : error);
                    tipoError = "error";
                }
                else if(xhr.status == 404){
                    textError = (error == "Not Found" ? "Dato no encontrado." : error);
                    tipoError = "warning";
                }
                else if(xhr.status == 405) {
                    textError = (error == "Method Not Allowed" ? "Método no permitido." : error);
                    tipoError = "error";
                }
                else if(xhr.status == 409) {
                    textError = (error == "Conflict" ? "Dato con caracteristicas similares fue encontrado." : error);
                    tipoError = "warning";
                }
                else if(xhr.status == 413) {
                    textError = (error == "Request Entity Too Large" ? "Solicitud demasiado grande." : error);
                    tipoError = "error";
                }
                else if(xhr.status == 501) {
                    textError = (error == "Not Implemented" ? "Método no implementado." : error);
                    tipoError = "error";
                }
                else{
                    textError = (error == "Internal Server Error" ? "Ocurrió un error, contacte al administrador" : error);
                    tipoError = "error";
                }
                showGeneralMessage(textError, tipoError, true);
                enableButtons();
                // $(".tab-content :input").prop("disabled", false);
            }
        });
    }else{
        $.ajax({
            url: _url,
            type: _type,
            //data: JSON.stringify(_data),
            beforeSend: function(){
                // disableButtons();
                // $(".tab-content :input").prop("disabled", true);
            },
            success: function(data){
                if(_successGen){
                    showGeneralMessage("La información se guardo correctamente", "success");
                    setTimeout(function () {
                        // window.location.href = "/importcensus";
                    }, 2000);
                    //return true;
                }else{
                    //console.log(data);
                    _json = data;
                    if (typeof _callback === "function") {
                        _callback(_json);
                    }
                    //return data;
                }
            },
            error: function(xhr, status, error){
                var textError = "";
                var tipoError = "";

                if(xhr.status == 400) {
                    textError = (error == "Bad Request" ? "Petición incorrecta." : error);
                    tipoError = "error";
                }
                else if(xhr.status == 401) {
                    $("#sesion-modal").modal({
                        backdrop:"static",
                        keyboard: false
                    });
                    return;
                }
                else if(xhr.status == 403) {
                    textError = (error == "Forbidden" ? "No cuenta con el permiso." : error);
                    tipoError = "error";
                }
                else if(xhr.status == 404){
                    textError = (error == "Not Found" ? "Dato no encontrado." : error);
                    tipoError = "warning";
                }
                else if(xhr.status == 405) {
                    textError = (error == "Method Not Allowed" ? "Método no permitido." : error);
                    tipoError = "error";
                }
                else if(xhr.status == 409) {
                    textError = (error == "Conflict" ? "Dato con caracteristicas similares fue encontrado." : error);
                    tipoError = "warning";
                }
                else if(xhr.status == 413) {
                    textError = (error == "Request Entity Too Large" ? "Solicitud demasiado grande." : error);
                    tipoError = "error";
                }
                else if(xhr.status == 501) {
                    textError = (error == "Not Implemented" ? "Método no implementado." : error);
                    tipoError = "error";
                }
                else{
                    textError = (error == "Internal Server Error" ? "Ocurrió un error, contacte al administrador" : error);
                    tipoError = "error";
                }
                showGeneralMessage(textError, tipoError, true);
                enableButtons();
                // $(".tab-content :input").prop("disabled", false);
            }
        });
    }



    //return false;
}

function unLoading(){

}

function onCompleteAjax() {
    unLoading();
}

function exportar_excel(data, title, subject, author, hoja, file_name) {
    var wb = XLSX.utils.book_new();
    wb.Props = {
        Title: title,
        Subject: subject,
        Author: author,
        CreatedDate: new Date()
    };
    wb.SheetNames.push(hoja);
    var ws_data = data;
    var ws = XLSX.utils.aoa_to_sheet(ws_data);
    wb.Sheets[hoja] = ws;
    var wbout = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });

    saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), file_name + '.xlsx');
}

/*
 * export excel
 */
function s2ab(s) {
    var buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
    var view = new Uint8Array(buf); //create uint8array as viewer
    for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xFF; //convert to octet
    return buf;
}