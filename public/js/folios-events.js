$(document).ready(function () {

    $("#foliousado").inputmask({ "mask": "9", "repeat": 8, "greedy": false});
    $("#cantidadfolios").inputmask({ "mask": "9", "repeat": 5, "greedy": false});
    // $("#num_fin").inputmask({ "mask": "9", "repeat": 5, "greedy": false});
    $("#num_fin_inventario").inputmask({ "mask": "9", "repeat": 10, "greedy": false});
    $("#serie").inputmask({ "mask": "a", "repeat": 5, "greedy": false});
    $("#filtro-serie").inputmask({ "mask": "a", "repeat": 5, "greedy": false});

    selectfcobratario = $("#fcobratario").selectize();
    selectizefcobratario = selectfcobratario[0].selectize;
    selectizefcobratario.setValue("", false);

    selectrcobratario = $("#tcobratario").selectize();
    selectizetcobratario = selectrcobratario[0].selectize;
    selectizetcobratario.setValue("", false);

    selectcobratario = $("#cobratario").selectize();
    selectizecobratario = selectcobratario[0].selectize;
    selectizecobratario.setValue("", false);

    dataTable = $('#folios-datatable').DataTable({
        "serverSide": true,
        // "deferLoading": 0,
        ajax: {
            url: "/folios/index",
            "data": function ( d ) {
                d.tipo_busqueda = $("#clientes-filter-select").val();
            },
            error: function(xhr, status, error){
                var textError = "";
                var tipoError = "";
                $(window).resize();
                $(window).resize();
                if(xhr.status == 401) {
                    window.location = "/";
                }
                else {
                    showGeneralMessage("Ocurrió un error, contacte al administrador", "warning", true);
                }
            }
        },
        dom: 'rt<"row"<"col-md-6 col-sm-6"i><"col-md-6 col-sm-6"lip>>',
        pageLength: 10,
        responsive: {
            details: {
                type: 'column',
                target: 0
            }
        },
        columnDefs: [
            { targets: 0, className: 'control center', width: "20px", orderable: false, searchable: false },
            {
                targets: [1],
                orderable: false,
                searchable: false,
                // defaultContent: buttons,
                className: 'dt-center no-wrap',
                "render": function ( data, type, row, meta ) {
                    var buttons = "";
                    buttons += '<button class="btn btn-primary btn-sm clientes-info" data-id="' + row[3] + '" type="button" title="Consultar">' +
                        '<i class="fa fa-info"></i>' +
                        '</button> ';
                    // buttons += '<button class="btn btn-primary btn-sm clientes-edit" data-id="' + row[3] + '" type="button" title="Modificar">' +
                    //     '<i class="fa fa-pencil"></i>' +
                    //     '</button> ';
                    buttons += '<button class="btn btn-danger btn-sm clientes-delete" ' + (row[14] === true ? '' : 'disabled') + ' data-id="' + row[3] + '" type="button" title="¿Desea eliminar?">' +
                        '<i class="fa fa-remove"></i>' +
                        '</button> ';
                    return buttons;
                }
                // visible: false
            },
            { targets: 7, defaultContent: "0" },
            { targets: [2], searchable: true, orderable: false, className: 'dt-center'},
            { targets: [-1,-2], visible: false },
            { targets: '_all', visible: true, orderable: true, searchable: true }
        ],
        language: {
            url: "../../plugins/datatables_new/language.MX.json"
        },
        "order": [
            [3, "desc"]
        ],
        "drawCallback": function(settings) {
            // console.log(settings.json);
            var resp = settings.json;
            $( "#monto-transito-label" ).html(resp.sum[0].transito);
            $( "#monto-monto-porliquidar-label" ).html(resp.sum[0].monto_porliquidar);
            $( "#monto-porliquidar-label" ).html(resp.sum[0].porliquidar);
            $( "#monto-disponibles-label" ).html(resp.sum[0].disponibles);
            //do whatever
        },
        // columns: [
        //     {
        //         data: null,
        //         defaultContent: ""
        //     },
        //     {
        //         data: null,
        //         render: function( data, type, full, meta ){
        //             var botones = '';
        //             if(isInfo){
        //                 botones += '<button class="btn btn-primary btn-sm folios-info" type="button" title="Consultar">' +
        //                     '<i class="fa fa-info"></i>' +
        //                     '</button> ';
        //             }
        //
        //             if(isEdit) {
        //                 botones += '<button class="btn btn-primary btn-sm folios-edit" type="button" title="Modificar">' +
        //                     '<i class="fa fa-pencil"></i>' +
        //                     '</button> ';
        //             }
        //
        //             if(isDelete) {
        //                 botones += '<button class="btn btn-danger btn-sm folios-delete" type="button" title="¿Desea eliminar?">' +
        //                     '<i class="fa fa-remove"></i>' +
        //                     '</button> ';
        //             }
        //             return botones;
        //         }
        //     },
        //     {data: "activo", defaultContent: ""},
        //     {data: "id", defaultContent: ""},
        //     {data: "serie", defaultContent: ""},
        //     {data: "num_ini", defaultContent: ""},
        //     {data: "num_fin", defaultContent: ""},
        //     {data: "cobratario", defaultContent: ""},
        //     {data: "usados", defaultContent: ""},
        //     {data: "disponibles", defaultContent: ""},
        //     {data: "total", defaultContent: ""},
        // ]
    });

    $("#folios-datatable").off( 'click', '.counsul-liquidar');
    $("#folios-datatable").on( 'click', '.counsul-liquidar', function () {
        $.ajax({
            url: "/folios/getxliquidar/" + $(this).attr("data-cob") + "?idrf=" + $(this).attr("data-id"),
            success: function (resp) {
                var json = JSON.parse(resp);
                var folios = "";
                for(var i in json){
                    var folio = json[i];
                    folios += '<div class="col-md-2"><div style="border: 1px solid #cdcdcd; margin-bottom: 5px; text-align: center">' + folio.folio + '</div></div>'
                }
                $("#container-folios-xliquidar").html(folios);
                $("#modal-xliquidar").modal({
                    backdrop: 'static',
                    keyboard: false
                });

            }
        });
    });

    $("#folios-datatable").off( 'click', '.counsul-disponibles');
    $("#folios-datatable").on( 'click', '.counsul-disponibles', function () {
        $.ajax({
            url: "/folios/getdisponibles/" + $(this).attr("data-cob") + "?id=" + $(this).attr("data-id"),
            success: function (resp) {
                var json = JSON.parse(resp);
                var folios = "";
                for(var i in json){
                    var folio = json[i];
                    folios += '<div class="col-md-2">' +
                        '<div style="border: 1px solid #cdcdcd; margin-bottom: 5px; text-align: center">' +
                            '<label>' +
                                '<input type="checkbox" class="chkFoliosDisponibles" data-id="' + folio.id + '">' +
                                folio.folio +
                            '</label>' +
                        '</div>' +
                    '</div>'
                }
                $("#container-folios-disponibles").html(folios);
                $("#modal-disponibles").modal({
                    backdrop: 'static',
                    keyboard: false
                });

            }
        });
    });

    $("#btnCancelFolios").on("click", function (e) {
        var $btn = $(this);
        var $tr = $(this).closest("tr");
        var id = $(this).attr("data-id");
        var data = [];
        $("#container-folios-disponibles :input[type=checkbox]:checked").each(function () {
            data.push($(this).attr('data-id'));
        });

        if(data.length == 0){
            showGeneralMessage("Debe seleccionar almenos un folio", "warning", true);
            $(".popover.confirmation").confirmation("destroy");
            return;
        }
        var objdata = {
            data: data
        };

        $("#modal-confirmation-cancel").modal("show");
        // $(".popover.confirmation").confirmation("destroy");
        // $(this).confirmation({
        //     rootSelector: "#modal-disponibles",
        //     container: "#modal-disponibles",
        //     singleton: true,
        //     popout: true,
        //     btnOkLabel: "Si",
        //     onConfirm: function() {
        //         $.ajax({
        //             url: '/folios/cancelar',
        //             type: 'DELETE',
        //             data: JSON.stringify(objdata),
        //             beforeSend: function(){
        //                 $("#btnCancelFolios").prop("disabled", true);
        //                 $("#btnTransferir").prop("disabled", true);
        //             },
        //             success: function () {
        //                 showGeneralMessage("La informacion se guardo correctamente", 'success', true);
        //                 dataTable.draw(false);
        //                 $("#modal-disponibles").modal("hide");
        //             },
        //             complete: function () {
        //                 $("#btnCancelFolios").prop("disabled", false);
        //                 $("#btnTransferir").prop("disabled", false);
        //                 $(".popover.confirmation").confirmation("destroy");
        //             }
        //         });
        //     },
        //     onCancel: function() {
        //         $(".popover.confirmation").confirmation("destroy");
        //     },
        // }).confirmation("show");
    });

    $("#btnGuardarCancelacion").on("click", function () {
        var data = [];
        $("#container-folios-disponibles :input[type=checkbox]:checked").each(function () {
            data.push($(this).attr('data-id'));
        });

        var observaciones = $("#observacionesCancel").val().trim();
        if(observaciones == ""){
            showGeneralMessage("Debe especificar un motivo", 'warning', true);
            return;
        }
        var objdata = {
            data: data,
            observaciones: observaciones
        };

        $.ajax({
            url: '/folios/cancelar',
            type: 'DELETE',
            data: JSON.stringify(objdata),
            beforeSend: function(){
                $("#btnCancelFolios").prop("disabled", true);
                $("#btnTransferir").prop("disabled", true);
            },
            success: function () {
                showGeneralMessage("La informacion se guardo correctamente", 'success', true);
                dataTable.draw(false);
                $("#modal-disponibles").modal("hide");
                $("#modal-confirmation-cancel").modal("hide");
            },
            complete: function () {
                $("#btnCancelFolios").prop("disabled", false);
                $("#btnTransferir").prop("disabled", false);
                $(".popover.confirmation").confirmation("destroy");
            }
        });
    });
    $("#btnTransferir").on("click", function () {
        var data = [];
        selectizetcobratario.setValue("", false);
        $("#container-folios-disponibles :input[type=checkbox]:checked").each(function () {
            data.push($(this).attr('data-id'));
        });

        if(data.length == 0){
            showGeneralMessage("Debe seleccionar almenos un folio", "warning", true);
            $(".popover.confirmation").confirmation("destroy");
            return;
        }
        var objdata = {
            data: data
        }
        $("#modal-transferira").modal("show");
        $("#tipotransferencia").val("TES");
    });

    $("#btnGuardarTransferir").on("click", function () {
        var cobratario = $("#tcobratario").val();
        var data = [];
        $("#container-folios-disponibles :input[type=checkbox]:checked").each(function () {
            data.push($(this).attr('data-id'));
        });

        if(data.length == 0){
            showGeneralMessage("Debe seleccionar almenos un folio", "warning", true);
            $(".popover.confirmation").confirmation("destroy");
            return;
        }

        var tipotrans = $("#tipotransferencia").val();
        if(tipotrans != "TES"){
            if(!cobratario){
                showGeneralMessage("Debe seleccionar un cobratario", "warning", true);
                return;
            }
        }
        else{
            cobratario = null
        }
        var objdata = {
            data: data,
            tipo: tipotrans,
            cobratario: cobratario
        }



        $.ajax({
            url: "/folios/transferira",
            type: "POST",
            data: JSON.stringify(objdata),
            success: function () {
                showGeneralMessage("La informacion se guardo correctamente", "success", true);
                $("#modal-transferira").modal("hide");
                $("#modal-disponibles").modal("hide");
                dataTable.draw(false);
            }
        });
    });

    $("#tipotransferencia").on("click", function () {
        var value = this.value;
        if(value == "TES"){
            $("#container-cob-trans").hide();
        }
        else{
            $("#container-cob-trans").show();
        }
    });

    dataTableInv = $('#foliosinv-datatable').DataTable({
        dom: 'rt<"row"<"col-md-6 col-sm-6"i><"col-md-6 col-sm-6"p>>',
        pageLength: 10,
        responsive: {
            details: {
                type: 'column',
                target: 0
            }
        },
        columnDefs: [
            { targets: 0, className: 'control center', width: "20px", orderable: false, searchable: false },
            { targets: 1, orderable: false, searchable: false },
            { targets: [2], searchable: true, orderable: false, className: 'dt-center'},
            { targets: [-2], visible: false },
            { targets: '_all', visible: true, orderable: true, searchable: true }
        ],
        language: {
            url: "../../plugins/datatables_new/language.MX.json"
        },
        "order": [
            [3, "desc"]
        ],
    });



    $("#foliosinv-datatable").off( 'click', '.deleteinv');
    $("#foliosinv-datatable").on( 'click', '.deleteinv', function () {
        $(".popover.confirmation").confirmation("destroy");
        var $btn = $(this);
        var $tr = $(this).closest("tr");
        var id = $(this).attr("data-id");
        $(this).confirmation({
            rootSelector: "#modal-addfolioinventario",
            container: "#modal-addfolioinventario",
            singleton: true,
            popout: true,
            btnOkLabel: "Si",
            onConfirm: function() {
                $.ajax({
                    url: '/folios/saveinv?id=' + id,
                    type: 'DELETE',
                    success: function () {
                        showGeneralMessage("La informacion se guardo correctamente", 'success', true);
                        $btn.prop("disabled", true);
                        $tr.find(".fa-check").removeClass("fa-check").addClass("fa-remove").css("color", "red");
                        // dataTableInv.draw();
                    }
                });
            },
            onCancel: function() {
                $(this).confirmation('destroy');
            },
        });

        $(this).confirmation("show");
    });

    $("#folios-datatable").off( 'click', '.clientes-info');
    $("#folios-datatable").on( 'click', '.clientes-info', function () {
        var $tr = $(this).closest("tr");
        var id = $(this).attr("data-id");
        $("#modal-addfolio").modal({
            backdrop: 'static',
            keyboard: false
        });
        $("#id").val(id);
        loadRangoFolio(id, "info");
        /* poner aqui la seccion para la consulta */
    });

    $("#folios-datatable").off( 'click', '.clientes-edit');
    $("#folios-datatable").on( 'click', '.clientes-edit', function () {
        var $tr = $(this).closest("tr");
        var id = $(this).attr("data-id");
        $("#modal-addfolio").modal({
            backdrop: 'static',
            keyboard: false
        });
        $("#id").val(id);
        loadRangoFolio(id, "edit");
        /* poner aqui la seccion para la consulta */
    });

    $("#folios-datatable").off( 'click', '.clientes-delete');
    $("#folios-datatable").on( 'click', '.clientes-delete', function () {
        $(".popover.confirmation").confirmation("destroy");
        var $tr = $(this).closest("tr");
        var id = $(this).attr("data-id");
        $(this).confirmation({
            rootSelector: "body",
            container: "body",
            singleton: true,
            popout: true,
            btnOkLabel: "Si",
            onConfirm: function() {
                $.ajax({
                    url: '/folios/save?id=' + id,
                    type: 'DELETE',
                    success: function () {
                        showGeneralMessage("La informacion se guardo correctamente", 'success', true);
                        dataTable.draw();
                    }
                });
            },
            onCancel: function() {
                $(this).confirmation('destroy');
            },
        });

        $(this).confirmation("show");
    });


    $("#clientes-search-do").on("click", function () {
        var vigente = $("#folios-vigente-val").val();
        var cobratario = $("#fcobratario").val();
        var serie = $("#filtro-serie").val().trim().toUpperCase();

        dataTable.column(2).search("");
        dataTable.column(4).search("");
        dataTable.column(13).search("");

        if(vigente)
            dataTable.column(2).search(vigente);
        if(serie)
            dataTable.column(4).search(serie);
        if(cobratario)
            dataTable.column(13).search(cobratario);

        dataTable.draw();
    });

    
    $(".clientes-filter-clear-do").on("click", function () {
        $("#filtro-serie").val("");
    });

    $("#modal-addfolio").on("hidden.bs.modal", function () {
        $("#modal-addfolio .modal-body :input:not([type=hidden])").prop("disabled", false);
        selectizecobratario.enable();

        $("#modal-addfolio .modal-body :input").val("");
        selectizecobratario.setValue("", false);
        $("#modal-addfolio .modal-body :input.has-error").removeClass("has-error");
        $("#titleFolio").html("Agregar folio");
    });

    $("#modal-addfolioinventario").on("shown.bs.modal", function () {
        $("#num_fin_inventario").val("");
        $("#txtObservInven").val("");
        dataTableInv.columns.adjust();
    });

    $("#addFolioInventario").on("click", function () {
        $.ajax({
            url: '/folios/getInventario',
            beforeSend: function(){
                dataTableInv.clear().draw();
            },
            success: function (resp) {
                var json = JSON.parse(resp);
                $("#num_ini_inventario").val(json.nextFolio);
                dataTableInv.rows.add(json.inventario).draw();
                $("#modal-addfolioinventario").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
        });
    });

    $("#addFolio").on("click", function () {
        $.ajax({
            url: '/folios/disponibles',
            success: function (resp) {
                var json = JSON.parse(resp);
                if(resp === '0'){
                    showGeneralMessage("No existen folios disponibles para asignar");
                }
                else{
                    $("#iniFolioAsig").val(json.numini);
                    $("#num_ini").val(json.numini);
                    $("#num_ini").prop("disabled", true);
                    $("#iniFolioAsig").prop("disabled", true);
                    $("#modal-addfolio").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            }
        });

    });

    $("#btnGuardarFolio").on("click", function () {
        var valid = 0;
        $("#modal-addfolio .modal-body :input.has-error").removeClass("has-error");

        $("#modal-addfolio .modal-body :input.required").each(function () {
            if(this.value == ""){
                $(this).closest(".form-group").addClass("has-error");
                valid++;
            }
        });

        if(valid > 0){
            showGeneralMessage("Los campos marcados con * son obligaotrios", 'warning', true);
            return;
        }

        var cantfol = $("#cantidadfolios").val();
        if(parseInt(cantfol) == 0){
            showGeneralMessage("El numero inicial debe ser mayor a 0", 'warning', true);
            return;
        }

        var cobratario = $("#cobratario").val();
        var data = {
            id: $("#id").val(),
            cantfol: cantfol,
            cobratario: cobratario ? cobratario : null
        }
        $.ajax({
            url: '/folios/save',
            type: 'POST',
            data: JSON.stringify(data),
            success: function (resp) {
                showGeneralMessage("La informacion se guardo correctamente", 'success', true);
                $("#modal-addfolio").modal("hide");
                dataTable.draw();
            }
        });
    });

    $("#btnGuardarFolioInv").on("click", function () {
        var numini = $("#num_ini_inventario").val();
        var numfin = $("#num_fin_inventario").val();
        if(numfin == ""){
            showGeneralMessage("Los campos marcados con * son obligaotrios", 'warning', true);
            return;
        }
        if(parseInt(numini) > parseInt(numfin)){
            showGeneralMessage("El numero final debe ser mayor al inicial", 'warning', true);
            return;
        }

        var observaciones = $("#txtObservInven").val().trim();

        var data = {
            numini: numini,
            numfin: numfin,
            observaciones: observaciones ? observaciones : null
        }
        $.ajax({
            url: '/folios/saveinv',
            type: 'POST',
            data: JSON.stringify(data),
            beforeSend:function(){
                $("#btnGuardarFolioInv").prop("disabled", true);
            },
            success: function (resp) {
                var json = JSON.parse(resp);
                showGeneralMessage("La informacion se guardo correctamente", 'success', true);
                // $("#modal-addfolioinventario").modal("hide");
                dataTableInv.row.add(json.data[0]).draw();

                $("#num_ini_inventario").val(json.folioactual);
                $("#num_fin_inventario").val("");
                $("#txtObservInven").val("");
            },
            complete: function () {
                $("#btnGuardarFolioInv").prop("disabled", false);
            }
        });
    });

    $("#addFoliosUsados").on("click", function () {
        $("#foliousado").val("");
        $("#observaciones_foliousado").val("");
        $("#modal-addFoliosUsados").modal("show");
    });

    $("#btnSaveFoliosUsados").on("click", function () {
        var folio = $("#foliousado").val().trim();
        var observaciones = $("#observaciones_foliousado").val().trim();
        if(folio == "" || observaciones == ""){
            showGeneralMessage("Todos los campos son obligatorios", 'warning', true);
            return;
        }

        var data = {
            folio: folio,
            observaciones: observaciones
        };
        $.ajax({
            url: '/folios/usados',
            type: "POST",
            data: JSON.stringify(data),
            beforeSend: function(){
                $("#btnSaveFoliosUsados").html('<i class="fa fa-spin fa-spinner"></i> Guardar');
                $("#btnSaveFoliosUsados").prop("disabled", true);
                $("#btnCancelFoliosUsados").prop("disabled", true);
            },
            success: function () {
                $("#modal-addFoliosUsados").modal("hide");
                showGeneralMessage("La informacion se guardo correctamente", "success", true);
            },
            complete: function () {
                $("#btnSaveFoliosUsados").html('Guardar');
                $("#btnSaveFoliosUsados").prop("disabled", false);
                $("#btnCancelFoliosUsados").prop("disabled", false);
            }
        });
    });
});

function loadRangoFolio(id, tipo){
    $("#modal-addfolio .modal-body :input:not([type=hidden])").prop("disabled", false);

    selectizecobratario.enable();
    $.ajax({
        url: '/folios/get/' + id,
        success: function (resp) {
            var json = JSON.parse(resp);
            $("#num_ini").val(json.num_ini);
            $("#num_fin").val(json.num_fin);
            $("#serie").val(json.serie);
            selectizecobratario.setValue(json.idcobratario, false);
            if(tipo == "info"){
                $("#modal-addfolio .modal-body :input:not([type=hidden])").prop("disabled", true);

                selectizecobratario.disable();
            }
            else if(tipo == "edit"){
                $("#modal-addfolio .modal-body :input:not([type=hidden])").prop("disabled", true);
            }
        }
    })
}