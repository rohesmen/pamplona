var quejasDT;
var buttons = "";
var idMenu = "";
$(document).ready(function() {
     $("#clientes-colonia-val").selectize();
    // coloniaForm = $("#colonias").selectize();
    // usuarioForm = $("#usuarios").selectize();

    $('#fReporte').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        todayHighlight: true,
        endDate: getTodayddmmyy()
    });
    $('#fAtencion').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });

    $(".queja-filter-clear-do").off("click");
    $(".queja-filter-clear-do").on("click", function(){
        $(this).next().val("");
    });


    $("#quejas-filter-select").off("change");
    $("#quejas-filter-select").on("change", function(){
        var valor = this.value;

        if(!$("#quejas-filtro-todos").hasClass("oculto"))
            $("#quejas-filtro-todos").toggleClass("oculto");

        if(!$("#quejas-filtro-clave").hasClass("oculto"))
            $("#quejas-filtro-clave").toggleClass("oculto");

        if(!$("#quejas-filtro-nombre").hasClass("oculto"))
            $("#quejas-filtro-nombre").toggleClass("oculto");

        if(!$("#quejas-filtro-colonia").hasClass("oculto"))
            $("#quejas-filtro-colonia").toggleClass("oculto");

        if(!$("#quejas-filtro-etapa").hasClass("oculto"))
            $("#quejas-filtro-etapa").toggleClass("oculto");

        $("#quejas-nombre-val").val("");
        //$("#rutas-colonia-val").val("");
        $("#quejas-clave-val").val("");


        if(valor == ""){
            $("#quejas-filtro-todos").toggleClass("oculto");
        }
        else if(valor == "nom"){
            $("#quejas-filtro-nombre").toggleClass("oculto");
        }
        else if(valor == "col"){
            $("#quejas-filtro-colonia").toggleClass("oculto");
        }
        else if(valor == "etapa"){
            $("#quejas-filtro-etapa").toggleClass("oculto");
        }
        else {
            $("#quejas-filtro-clave").toggleClass("oculto");
        }
    });

    $("#origen").off("change");
    $("#origen").on("change", function(){
        var valor = this.value;

        if(!$("#ayuntamiento").hasClass("oculto"))
            $("#ayuntamiento").toggleClass("oculto");



        $("#nomAyunta").val("");
        $("#aPaternoAyunta").val("");
        $("#aMaternoAyunta").val("");




        if(valor == "AYUNTAMIENTO"){
            $("#ayuntamiento").toggleClass("oculto");
            $("#folio").attr("required", true);
        }
        else
            $("#folio").removeAttr("required");


    });

    $(".quejas-search-do").off('click');
    $(".quejas-search-val").off('keypress');
    $(".quejas-search-do").on('click', function(){

        fnBuscarQuejas();
    });

    $(".quejas-search-val").on('keypress', function(e){
        if(e.which == 13){
            fnBuscarQuejas();
        }
    });

    $(".clientes-search-do").off('click');
    $(".cliente-search-val").off('keypress');
    $(".clientes-search-do").on('click', function(){

        fnBuscarClientes();
    });

    $(".cliente-search-val").on('keypress', function(e){
        if(e.which == 13){
            fnBuscarClientes();
        }
    });

    /**
     * Abre el modal para el formulario de Rutas
     */
    $("#openNewQueja").on('click', function() {

        $("#queja-info-modal").modal({
            show: true,
            backdrop: "static"
        });

        fnLimpiaForm();
        setModeQuejaInfo('add');
        $('.modal-titleAux').html('Nueva Queja');

    });

    $("#buscaCliente").off("click");
    $("#buscaCliente").on("click", function(){
        fnLimpiarDatosCliente(true);
        $("#cliente-selec-modal").modal({
            show: true,
            backdrop: "static"
        });
    });

    $("#cliente-selec-modal").on("hidden.bs.modal", function () {
        $("body").addClass("modal-open");
    });
    $("#btnSeleccionar").on('click', function() {

        fnSeleccionaCliente();
    });

    $('#frmQueja').on('submit', function (event) {
        event.preventDefault();

        fnSaveQueja();

    });

    $("#btnGuardar").on('click', function() {

        $("#btnGuardaForm").click();
    });


    $("#clientes-filter-select").off("change");
    $("#clientes-filter-select").on("change", function(){
        var valor = this.value;
        $("#clientes-filtro-todos").hide();
        $("#clientes-filtro-nombre").hide();
        $("#clientes-filtro-direccion").hide();
        $("#clientes-filtro-clave").hide();
        $("#clientes-filtro-vigente").hide();

        $("#clientes-nombre-val").val("");
        $("#clientes-calle-val").val("");
        $("#clientes-numero-val").val("");
        $("#clientes-colonia-val").val("");
        $("#clientes-clave-val").val("");
        $("#clientes-vigente-val").val("");
        $("#clientes-vigente-val").removeAttr("disabled");

        $("#clientes-filtro-clave-").hide();
        if(valor == ""){
            $("#clientes-filtro-todos").show();
        }
        else if(valor == "nom"){
            $("#clientes-filtro-nombre").show();
        }
        else if(valor == "dir"){
            $("#clientes-filtro-direccion").show();
        }
        else if(valor == "vig"){
            $("#clientes-filtro-vigente").show();
        }
        else{
            $("#clientes-vigente-val").attr("disabled", "disabled");
            $("#clientes-filtro-clave").show();
        }
    });

    clientesDT = $("#clientes-datatable").DataTable({
        "dom": '<"clear"><rt><"bottom"lip><"clear">',
        "autoWidth": false,
        "paging": true,
        "select": true,
        "info": true,
        "processing": true,
//            "scrollY":        "506px",
        "scrollCollapse": false,
        "pagingType": "full",
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
        "lengthMenu": [[5, 10, 20, 30, 40], [5, 10, 20, 30, 40]],
        "pageLength": 5,
        "order": [[ 0, 'desc' ], [ 1, 'desc' ]],
        responsive: {
            details: {
                type: 'column',
                target: 1
            }
        },
        "processing": true,
        "serverSide": false,
        "deferLoading": 0,
        "lengthChange": true
    });

    $('#clientes-datatable tbody').on( 'click', 'tr', function () {

        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');

        }
        else {
            clientesDT.$('tr.selected').removeClass('selected');
            $(this).toggleClass('selected');

        }
    });





    $.contextMenu({
        selector: '#btnMenu',
        position: function(opt, x, y){
            opt.$menu.css({left: event.pageX - 160, top: event.pageY});
        },
        build: function($triggerElement, e){
            var row = quejasDT.row(idMenu);


            var id = row.data()[3];

            if(!row.data()[7]) {
                itemsData = [];
                $.ajax({
                    url: "/queja/etapas/" + id,
                    type: 'GET',
                    async: false,
                    success: function (data) {
                        $data = jQuery.parseJSON(data);
                        itemsData = $data;
                    }
                });

                return {
                    callback: function (key, options) {
                        var m = "clicked: " + key;

                        var row = quejasDT.row(idMenu);


                        var id = row.data()[3];

                        var data = {
                            clave: id,
                            id_etapa: key
                        };

                        $.ajax({
                            url: '/queja/cambiaretapa',
                            data: JSON.stringify(data),
                            type: 'POST',
                            dataType: "json",
                            beforeSend: function () {
                                $('#queja-container').html('<div style="text-align: center;">' +
                                    '<i class="fa fa-spinner fa-pulse fa-2x"></i> Guardando...' +
                                    '</div>');
                            },
                            success: function (json) {

                                if (!json.lOk) {
                                    showGeneralMessage(json.cMensaje, "warning", true);
                                    return;
                                }
                                else {
                                    showGeneralMessage("Registro Guardado", "success", true);
                                    $('#queja-info-modal').modal('hide');
                                    var $tr = $("#queja-" + json.id);
                                    if (!json.nuevo) {

                                        quejasDT.cell($tr, 6).data(json.etapa);
                                        quejasDT.cell($tr, 7).data(json.final);
                                        if(json.final)
                                            quejasDT.cell($tr, 8).data("");


                                    }
                                    quejasDT.draw(false);

                                }
                            },
                            complete: function () {
                                $('#queja-container').html("");
                            }
                        });
                    },
                    items: itemsData
                };
            }


        }
    });



});

function cargaMenu(value)
{

    idMenu = "#queja-" + value;
    var row = quejasDT.row(idMenu);


    var id = row.data()[3];


    if(!row.data()[7])
        $('#btnMenu').contextMenu();
}
function fnSaveQueja() {

    var fechaReporte = $("#fReporte").val();
    var fechaAtencion = $("#fAtencion").val();
    if(fechaAtencion != "" && fechaReporte != "" && fechaAtencion > fechaReporte){
        showGeneralMessage("Fecha de atención no puede ser mayor a fecha de reporte", "warning", true);
        return;
    }
    if($("#cliente").val() == "")
    {
        showGeneralMessage("Por favor realice la búsqueda de un cliente válido", "warning", true);
        return;
    }

    var data = {
        clave: $("#clave").val(),
        activo: $("#activo").is(':checked'),
        titulo: $("#titulo").val(),
        descripcion: $("#descripcion").val(),
        idCliente: $("#claveCliente").val(),
        nomReporta: $("#nomReporta").val(),
        aPaternoReporta: $("#aPaternoReporta").val(),
        aMaternoReporta: $("#aMaternoReporta").val(),
        origen: $("#origen").val(),
        folio: $("#folio").val(),
        nomAyunta: $("#nomAyunta").val(),
        aPaternoAyunta: $("#aPaternoAyunta").val(),
        aMaternoAyunta: $("#aMaternoAyunta").val(),
        fReporte: $("#fReporte").val(),
        fAtencion: $("#fAtencion").val(),
		correo: $("#correo_q").val(),
        telefono: $("#telefono_q").val(),

    };

    $.ajax({
        url: '/queja/guardar',
        data: JSON.stringify(data),
        type: 'POST',
        dataType: "json",
        beforeSend: function(){
            $('#queja-container').html('<div style="text-align: center;">' +
                '<i class="fa fa-spinner fa-pulse fa-2x"></i> Guardando...' +
                '</div>');
        },
        success: function(json) {

            if (!json.lOk) {
                showGeneralMessage(json.cMensaje, "warning", true);
                return;
            }
            else
            {
                showGeneralMessage("Registro Guardado", "success", true);
                $('#queja-info-modal').modal('hide');
                var $tr = $("#queja-" + json.id);
                if(!json.nuevo){
                    quejasDT.cell( $tr, 2 ).data( json.vigente );
                    quejasDT.cell( $tr, 3 ).data( json.id );
                    quejasDT.cell( $tr, 4 ).data( json.titulo );
                    quejasDT.cell( $tr, 5 ).data( json.cliente );
                }
                else {
                    $row = quejasDT.row.add( [
                        null,
                        null,
                        json.vigente,
                        json.id,
                        json.titulo,
                        json.cliente,
                        json.etapa,
                        json.final,
                        json.btnEtapa
                    ] ).draw( true );

                    quejasDT.draw();

                    $($row).attr("id", json.DT_RowId);
                }
                quejasDT.draw();

            }
        },
        complete: function () {
            $('#queja-container').html("");
        }
    });

}

function fnBuscarQuejas() {

    var nombre = $("#quejas-nombre-val").val();
    var clave = $("#quejas-clave-val").val();
    var etapa = $("#queja-etapa-val").val();
    var tipoBusqueda = $("#quejas-filter-select").val();

    if(tipoBusqueda == "nom"){
        if(nombre == ""){
            showGeneralMessage("Debe escribir el nombre a buscar", "warning", true);
            return;
        }
        if(nombre.length < 2){
            showGeneralMessage("Debe escribir almenos dos caracteres", "warning", true);
            return;
        }
    }
    if(tipoBusqueda == "cla"){
        if(clave == ""){
            showGeneralMessage("Debe escribir la clave a buscar", "warning", true);
            return;
        }
    }
    if(tipoBusqueda == "col"){
        if(colonia == ""){
            showGeneralMessage("Debe seleccionar una colonia", "warning", true);
            return;
        }
    }

    fnLimpiarDatos();

    var data = {
        tipo: tipoBusqueda,
        titulo: nombre,
        clave: clave,
        etapa: etapa
    }
    $.ajax({
        url: '/queja/search',
        data: JSON.stringify(data),
        type: 'POST',
        dataType: "json",
        beforeSend: function(){

            $('#queja-container').html('<div style="text-align: center;">' +
                '<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
                '</div>');
        },
        success: function(json){

            quejasDT.clear();
            if(json.length == 0)
            {
                showGeneralMessage("No se encontraron registros", "warning", true);
                quejasDT.draw();
                return;
            }



            quejasDT.rows.add(json);
            quejasDT.draw();


            $(window).resize();


        },
        complete: function () {
            $('#queja-container').html("");
        }
    });

    
}

function fnLimpiarDatos()
{
    selec = new Array();
    uSelec = 0;
    quejasDT.clear();

}

function fnSeleccionaCliente() {
    if(clientesDT.rows('.selected').data().length == 0){
        showGeneralMessage("Seleccione un registro", "warning", true);
        return ;
    }

    var rowData = clientesDT.rows("tr.selected").data();
    $("#claveCliente").val(rowData[0][0]);
    $("#cliente").val(rowData[0][1]);
	$("#correo").val(rowData[0][18]);
	$("#telefono").val(rowData[0][19]);
    $('#cliente-selec-modal').modal('hide');
    
}
function fnBuscarClientes() {

    var nombre = $("#clientes-nombre-val").val();
    var calle = $("#clientes-calle-val").val();
    var numero = $("#clientes-numero-val").val();
    var colonia = $("#clientes-colonia-val").val();
    var clave = $("#clientes-clave-val").val();
    var tipoBusqueda = $("#clientes-filter-select").val();
    var tipoVigente = $("#clientes-vigente-val").val();
    if(tipoBusqueda != ""){
        if(tipoBusqueda == "nom"){
            if(nombre == ""){
                showGeneralMessage("Debe escribir el nombre a buscar", "warning", true);
                return;
            }
            if(nombre.length < 2){
                showGeneralMessage("Debe escribir almenos dos caracteres", "warning", true);
                return;
            }
        }
        if(tipoBusqueda == "cla"){
            if(clave == ""){
                showGeneralMessage("Debe escribir la clave a buscar", "warning", true);
                return;
            }
        }
        if(tipoBusqueda == "dir"){
            if(calle == "" && numero == "" && colonia == ""){
                showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
                return;
            }

            if(calle == "" && numero == "" && colonia == ""){
                showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
                return;
            }

            if(calle == "" && colonia == ""){
                showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
                return;
            }

            if(numero == "" && colonia == ""){
                showGeneralMessage("Debe escribir almenos dos campos", "warning", true);
                return;
            }
        }
    }

    fnLimpiarDatosCliente(false);

    var data = {
        calle: calle,
        numero: numero,
        colonia: colonia,
        tipo: tipoBusqueda,
        nombre: nombre,
        clave: clave


    }
    $.ajax({
        url: '/queja/searchCliente',
        data: JSON.stringify(data),
        type: 'POST',
        dataType: "json",
        beforeSend: function(){

            $('#queja-container').html('<div style="text-align: center;">' +
                '<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
                '</div>');
        },
        success: function(json){

            clientesDT.clear();
            if(json.length == 0)
            {
                showGeneralMessage("No se encontraron registros", "warning", true);
                return;
            }



            clientesDT.rows.add(json);
            clientesDT.draw();





        },
        complete: function () {
            $('#queja-container').html("");
        }
    });


}

function fnLimpiarDatosCliente(todo)
{
    if(todo) {
        $("#clientes-nombre-val").val("");
        $("#clientes-calle-val").val("");
        $("#clientes-numero-val").val("");
        $("#clientes-colonia-val").val("");
    }
    clientesDT.clear();
    clientesDT.draw();

}

function fnLimpiaForm()
{
    $("#clave").val("");
    $("#activo").prop('checked', true);
    $("#titulo").val("");
    $("#descripcion").val("");
    $("#claveCliente").val("");
    $("#cliente").val("");
    $("#nomReporta").val("");
    $("#aPaternoReporta").val("");
    $("#aMaternoReporta").val("");
    $("#origen").val("");
    $("#folio").val("");
    $("#nomAyunta").val("");
    $("#aPaternoAyunta").val("");
    $("#aMaternoAyunta").val("");
	$("#correo").val("");
    $("#telefono").val("");
	$("#correo_q").val("");
    $("#telefono_q").val("");
    $("#fReporte").datepicker('update', new Date());
    $("#fAtencion").val("");

}

function loadQuejaInfo($indexQueja, mode) {
    if($indexQueja == null){
        return;
    }

    $.ajax({
        url: "/queja/get/" + $indexQueja,
        type: 'GET',
        success: function(data){
            $data = jQuery.parseJSON(data);

            fnLimpiaForm();
            setModeQuejaInfo(mode);
            setQuejaInfo($indexQueja, $data, mode);
			$("#queja-info-modal").modal({
                show: true,
                backdrop: "static"
            });
        }
    });
    
}

function setModeQuejaInfo(mode) {

    if(mode == "info"){
        validateForm = false;
        $('.modal-titleAux').html('Información');
        $("#btnGuardar").hide();


        $("#titulo").attr("disabled", true);
        $("#descripcion").attr("disabled", true);
        $("#claveCliente").attr("disabled", true);
        $("#buscaCliente").attr("disabled", true);
        $("#nomReporta").attr("disabled", true);
        $("#aPaternoReporta").attr("disabled", true);
        $("#aMaternoReporta").attr("disabled", true);
        $("#origen").attr("disabled", true);
        $("#folio").attr("disabled", true);
        $("#nomAyunta").attr("disabled", true);
        $("#aPaternoAyunta").attr("disabled", true);
        $("#aMaternoAyunta").attr("disabled", true);
        $("#fReporte").attr("disabled", true);
        $("#fAtencion").attr("disabled", true);
		$("#correo_q").attr("disabled", true);
		$("#telefono_q").attr("disabled", true);

    }
    else if('edit' || 'add'){
        $('.modal-titleAux').html('Modificar');
        $("#titulo").removeAttr("disabled");
        $("#descripcion").removeAttr("disabled");
        $("#claveCliente").removeAttr("disabled");
        $("#buscaCliente").removeAttr("disabled");
        $("#nomReporta").removeAttr("disabled");
        $("#aPaternoReporta").removeAttr("disabled");
        $("#aMaternoReporta").removeAttr("disabled");
        $("#origen").removeAttr("disabled");
        $("#folio").removeAttr("disabled");
        $("#nomAyunta").removeAttr("disabled");
        $("#aPaternoAyunta").removeAttr("disabled");
        $("#aMaternoAyunta").removeAttr("disabled");
        $("#fReporte").removeAttr("disabled");
        $("#fAtencion").removeAttr("disabled");
		$("#correo_q").removeAttr("disabled");
		$("#telefono_q").removeAttr("disabled");
		
        $("#btnGuardar").show();
    }

}

function setQuejaInfo($indexQueja, $data, mode) {
    $("#clave").val($data.id);
    if(mode == "info"){
		$("#activo").prop('checked', $data.activo);
	}
	
	$("#correo").val($data.correo);
	$("#telefono").val($data.telefono);
        
    $("#titulo").val($data.titulo);
    $("#descripcion").val($data.descripcion);
    $("#claveCliente").val($data.id_cliente);
    $("#cliente").val($data.cliente);
    $("#nomReporta").val($data.nomReporta);
    $("#aPaternoReporta").val($data.aPaternoReporta);
    $("#aMaternoReporta").val($data.aMaternoReporta);
    $("#origen").val($data.origen);
    $("#folio").val($data.folio);
    $("#nomAyunta").val($data.nomAyunta);
    $("#aPaternoAyunta").val($data.aPaternoAyunta);
    $("#aMaternoAyunta").val($data.aMaternoAyunta);

	$('#fReporte').datepicker('update', $data.fecha_reporte);
	$("#fAtencion").datepicker('update', $data.fecha_atencion);
	$("#correo_q").val($data.correo_q);
	$("#telefono_q").val($data.telefono_q);
	
    $("#origen").change();
}

function eliminarQueja($tr, id){
    $.ajax({
        url: "/queja/delete/" + id,
        success: function(){
            showGeneralMessage("La información se guardo correctamente", "success", true);
            quejasDT.cell( $tr, 2 ).data( '<i class="fa fa-remove" style="color: red" title="Activo"><\/i>' );
            quejasDT.draw();
        }
    });

}