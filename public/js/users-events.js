var selectUser = function(idUser, nameUser) {
    idUserSelected = idUser;
    $("#label-quiz-user-name").html(nameUser);
}

$(document).ready(function() {
    usersDT = null;
    initModalUser();

    $(".btn-user-modal-close").off("click");
    $(".btn-user-modal-close").on("click", function(){
        $("#user-info-modal").modal('hide');
        if(usersDT != null)
            usersDT.$('tr.selected').removeClass('selected');
    });

    //usersDT = null;
    $("#users-search-do").off('click');
    $("#users-search-val").off('keypress');
    $("#users-search-do").on('click', function(){
        buscarUsuarios($('#users-search-val').val());
    });

    $("#users-search-val").on('keypress', function(e){
        if(e.which == 13){
            var valor = this.value;
            buscarUsuarios(valor);
        }
    });
    $('#users-filter-column-select').on('change',function(){
        var value = $("#users-filter-column-select").val();
        var select = $('#users-search-val');
        var prefix = "Ej. ";
        switch (value){
            case 'cla':
                select.attr("placeholder",prefix+'11');
                break;
            case 'nom':
                select.attr("placeholder",prefix+'CASTILLO ACOSTA MARCOS');
                break;
            case 'usr':
                select.attr("placeholder",prefix+'marcos.castillo');
                break;
            case 'per':
                select.attr("placeholder",prefix+'Capturista');
                break;
            case '-1':
                select.attr("placeholder",'');
                break;
        }
    });
    $("#users-filter-clear-do").off("click");
    $("#users-filter-clear-do").on("click", function(){
        //var columnFilter = $("#users-filter-column-select").val('-1');
        $('#users-search-val').val('');
    });
    $("#user-profile-confirm-no").off("click");
    $("#user-profile-confirm-no").on("click", function(){
        $('#modal-user-profile-confirm').off('hidden.bs.modal');
        $('#modal-user-profile-confirm').on('hidden.bs.modal', function (e) {
            //$("body").addClass("modal-open");
            $('#modal-user-profile-confirm').off('hidden.bs.modal');
        });
        $("#modal-user-profile-confirm").modal("hide");
    });

    $("#user-profile-confirm-yes").off("click");
    $("#user-profile-confirm-yes").on("click", function(){
        $('#modal-user-profile-confirm').off('hidden.bs.modal');
        $('#modal-user-profile-confirm').on('hidden.bs.modal', function (e) {
            $('#modal-user-profile-confirm').off('hidden.bs.modal');
            openEditProfilesUser($indexuser);
        });
        $("#modal-user-profile-confirm").modal("hide");
    });
});
function openEditProfilesUser(idUser){
    $.ajax({
        url: "/users/getProfiles?id=" + idUser,
        success: function(resp){
            $("#user-edit-proflies-body").html(resp);
            $("#user-edit-proflies-modal").modal("show");
        },
        /*error: function(){

        }*/
    })
}
function buscarUsuarios(valor){
    var valor = secureInput(valor.trim());
    var columnFilter = $("#users-filter-column-select").val();
    if(columnFilter == -1 || (columnFilter != -1 && valor != '')){
        var data = {
            valor: valor,
            filtro: columnFilter
        }
        $.ajax({
            url: '/users/search',
            data: JSON.stringify(data),
            type: 'POST',
            beforeSend: function(){
                $('#users-datatable-container').html('<div style="text-align: center;">' +
                    '<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
                    '</div>');
            },
            success: function(html){
                $('#users-datatable-container').html(html);
            }
        });
    }
}

function initModalUser() {
    //loadTerritories();

    $("#user-info-tel").inputmask({
        mask: '(999) 999-9999'
    });

    loadEventosModalUser();
}

function loadNewUser() {
    $indexuser = null;
    setModeUserInfo('n');
    clearUserInfo();
    loadDefaultUserInfo();
    showMessageModalUser("Datos requeridos <span class='user-required'>marcados</span>", false);
}

function loadReadUser($indexuser) {
    var callback = function() {
        setModeUserInfo('r');
    };
    selectTableRow($indexuser);
    loadEventsBeforeNextUser($indexuser);
    loadUserInfo($indexuser, callback);
}

function openNewUser() {
    loadNewUser();
    $("#user-info-modal").modal('show');
}

function openReadUser($indexuser) {
    var callback = function() {
        setModeUserInfo('r');
        if(!$("#user-info-activo").is(":checked")){
            $("#user-reset-password-do").hide();
        }
        $("#user-info-modal").modal('show');
    };
    selectTableRow($indexuser);
    loadEventsBeforeNextUser($indexuser);
    loadUserInfo($indexuser, callback);
}

function openEditUser($indexuser) {
    var callback = function() {
        showMessageModalUser("Datos requeridos <span class='user-required'>marcados</span>", false);
        $("#user-info-modal").modal('show');
        if(!$("#user-info-activo").is(":checked")){
            $("#user-reset-password-do").hide();
        }
    };
    selectTableRow($indexuser);
    loadEventsBeforeNextUser($indexuser);
    loadUserInfo($indexuser, callback);
}

function selectTableRow($indexuser) {
    var $row = $("#name-user-"+$indexuser).closest('tr');
    if ( $row.hasClass('selected') ) {
        $row.removeClass('selected');
    }
    else {
        usersDT.$('tr.selected').removeClass('selected');
        $row.addClass('selected');
    }
}

function loadEventosModalUser() {
    $(".btn-user-modal-close").off("click");
    $(".btn-user-modal-close").on("click", function(){
        $("#user-info-modal").modal('hide');
    });

    $('#user-info-modal').on('hidden.bs.modal', function (e) {
        if(usersDT != null)
            usersDT.$('tr.selected').removeClass('selected');
    });
    $('#user-info-modal').on('shown.bs.modal', function (e) {
        $("#user-info-usuario").focus();
    });

    $(".user-tool-new").off('click');
    $(".user-tool-new").on('click', function() {
        loadNewUser();
    });

    $(".user-tool-edit").off('click');
    $(".user-tool-edit").on('click', function() {
        setModeUserInfo('w');
        if(!$("#user-info-activo").is(":checked")){
            $("#user-reset-password-do").hide();
        }
        showMessageModalUser("Datos requeridos <span class='user-required'>marcados</span>", false);
    });
    $(".user-tool-save").off('click');
    $(".user-tool-save").on('click', function() {
        if(validateDataUserInfo()) {
            saveDataUserInfo(true);
        }
        else {
            showErrorModalPerson("Campos requeridos vacíos y/o formato de datos incorrectos");
        }
    });
    $(".user-tool-cancel").off('click');
    $(".user-tool-cancel").on('click', function() {
        cancelSaveDataUserInfo();
    });
    // $("#user-reset-password-do").off("click");
    // $("#user-reset-password-do").on("click", function(){
    //     resetPassword();
    // });
}

function loadEventsBeforeNextUser($indexuser) {
    if($indexuser == null || $indexuser == undefined) {
        $(".btn-before-user").off('click');
        $(".btn-next-user").off('click');
        return;
    }
    var $row = $("#name-user-"+$indexuser).closest('tr');
    var idx = usersDT.row( $row ).index();
    var indexes = usersDT.rows( {order:'current', search:'applied'})[0];
    var beforeidx = null;
    var nextidx = null;
    var currentrowidx = idx;
    for(index in indexes) {
        var rowidx = indexes[index];
        if(rowidx == idx) {
            currentrowidx = parseInt(index);
            if(indexes[parseInt(index)-1] != undefined) {
                beforeidx = indexes[parseInt(index)-1];
            }
            if(indexes[parseInt(index)+1] != undefined) {
                nextidx = indexes[parseInt(index)+1];
            }
            break;
        }
    }
    var pageinfo = usersDT.page.info();
    $(".btn-before-user").off('click');
    if(beforeidx != null) {
        var beforeIndexUser = $(usersDT.row(beforeidx).node())[0].getAttribute("data-index");
        $(".btn-before-user").on('click', function() {
            if(currentrowidx == pageinfo.start) {
                usersDT.page('previous').draw('page');
            }
            loadReadUser(beforeIndexUser);
        });
        $(".btn-before-user").removeClass("disabled");
    } else {
        if(!$(".btn-before-user").hasClass("disabled")) {
            $(".btn-before-user").addClass("disabled");
        }
    }
    $(".btn-next-user").off('click');
    if(nextidx != null) {
        var nextIndexUser = $(usersDT.row(nextidx).node())[0].getAttribute("data-index");
        $(".btn-next-user").on('click', function() {
            if(currentrowidx == (pageinfo.end-1)) {
                usersDT.page('next').draw('page');
            }
            loadReadUser(nextIndexUser);
        });
        $(".btn-next-user").removeClass("disabled");
    } else {
        if(!$(".btn-next-user").hasClass("disabled")) {
            $(".btn-next-user").addClass("disabled");
        }
    }
}

function loadUserInfo($indexuser, callback) {
    if($indexuser == null) {
        return;
    }
    $.ajax({
        url: "/users/get/" + $indexuser,
        type: 'GET',
        success: function(data){
            $data = jQuery.parseJSON(data);

            setModeUserInfo('w');
            clearUserInfo();

            setUserInfo($indexuser, $data);

            if(callback) {
                callback();
            }
        },
        /*error: function(){
        }*/
    });
}

function saveDataUserInfo(overwritteAddress) {
    //if(validateDataUserInfo()) {
    var nombre = getTextValueUpper($("#user-info-nombre").val());
    var usuario = getTextValue($("#user-info-usuario").val());
    var paterno = getTextValueUpper($("#user-info-paterno").val());
    var materno = getTextValueUpper($("#user-info-materno").val());
    var usuario = getTextValue($("#user-info-usuario").val());
    var correo = getTextValue($("#user-info-correo").val());
    var tel = getTextValue($("#user-info-tel").val());
    var activo = $("#user-info-activo").is(":checked");

    var data = {
        id: $indexuser,
        nombre: nombre,
        paterno: paterno,
        materno: materno,
        tel: tel,
        correo: correo,
        activo: activo,
        usuario: usuario
    };

    $.ajax({
        url: '/users/save',
        data: JSON.stringify(data),
        type: 'POST',
        beforeSend: function(){
            showMessageModalUser('<div style="text-align: center; display: inline-block;">' +
                '<i class="fa fa-spinner fa-pulse"></i> Cargando...' +
            '</div>');
        },
        success: function (resp) {
            var jsonResp = JSON.parse(resp);
            $indexuser = jsonResp.id;
            clearErrorsUserInfo();
            setModeUserInfo('r');
            if(!$("#user-info-activo").is(":checked")){
                $("#user-reset-password-do").hide();
            }
            showMessageModalUser('Se guardaron los datos del usuario', true);
            if(!jsonResp.edit){
                if(usersDT){
                    $tr = $(jsonResp.tr);
                    usersDT.row.add($tr).draw(false);
                }
                else{

                }
                $('#user-info-modal').off('hidden.bs.modal');
                $('#user-info-modal').on('hidden.bs.modal', function (e) {
                    $('#user-info-modal').off('hidden.bs.modal');
                    $("#modal-user-profile-confirm").modal("show");
                });
                $("#user-info-modal").modal("hide");
            }
            else{
                var $tr = $("tr#tr-"+$indexuser);
                var data = usersDT.row($tr).data();
                data[1] = (jsonResp.activo) ? "<i class='fa fa-check' title='Activo'></i>" : "<i class='fa fa-remove' title='Inactivo'></i>";
                data[4] = jsonResp.nombre;
                data[7] = jsonResp.actualizacion;
                usersDT.row($tr).data(data).draw(false);
            }
        },
        error: function (xhr, status, error){
            if(xhr.status == 409){
                showErrorModalUser('Ya existe el usuario: ' + usuario);
            }
            else if(xhr.status == 404){
                showErrorModalUser('No se encontro el usuario con id: ' + $indexuser);
            }
            else if(xhr.status == 401){
                $("#sesion-modal").modal({
                    backdrop:"static",
                    keyboard: false
                });
                return;
            }
            else{
                showErrorModalUser('Ocurrió un error al guardar los datos');
            }
        }
    });

    /*} else {
     showErrorModalUser("Campos requeridos vacíos y/o formato de datos incorrectos");
     }*/
}

function cancelSaveDataUserInfo() {
    var callback = function() {
        setModeUserInfo('r');
    };
    if($indexuser != undefined && $indexuser != null) {
        loadUserInfo($indexuser, callback);
    } else {
        clearUserInfo();
        setModeUserInfo('r');
    }
    if(!$("#user-info-activo").is(":checked")){
        $("#user-reset-password-do").hide();
    }
}

function setUserInfo($indexuser, $data) {
    $("#user-info-clave").val($indexuser);
    $("#user-info-usuario").val(formatValue($data["usuario"]));
    $("#user-info-paterno").val(formatValue($data["apepat"]));
    $("#user-info-materno").val(formatValue($data["apemat"]));
    $("#user-info-nombre").val(formatValue($data["nombre"]));
    $("#user-info-tel").val(formatValue($data["telefono"]));
    $("#user-info-correo").html(formatValue($data["direccion"]));
    $("#user-reset-password-do").show();
    $("#user-info-activo").prop("checked", true);
    if(!$data["activo"]){
        $("#user-reset-password-do").hide();
        $("#user-info-activo").prop("checked", false);
    }
}

function loadDefaultUserInfo() {
    $("#user-info-activo").prop("checked", true);
}

function clearUserInfo() {
    $('#user-info-modal').find('input[type="text"]').val("");
    $('#user-info-modal').find('input[type="number"]').val(null);
    $('#user-info-modal').find('select').val("");

    showErrorModalUser('');
    clearErrorsUserInfo();
}

function clearErrorsUserInfo() {
    $("#user-info-usuario").closest( ".form-group-sm").removeClass("has-error");
    $("#user-info-nombre").closest( ".form-group-sm").removeClass("has-error");
    $("#user-info-paterno").closest( ".form-group-sm").removeClass("has-error");
    $("#user-info-materno").closest( ".form-group-sm").removeClass("has-error");
    $("#user-info-correo").closest( ".form-group-sm").removeClass("has-error");
    $("#user-info-tel").closest( ".form-group-sm").removeClass("has-error");

    $("#user-info-territorio").closest( "div").removeClass("has-error");

    $("#user-info-perfil").closest( "div").removeClass("has-error");
}

function setModeUserInfo($mode) {
    mode = $mode;
    $("#user-info-clave").attr("disabled", true);
    $("#user-info-usuario").attr("disabled", true);
    switch($mode) {
        case 'n':
            $("#user-reset-password-do").hide();
            $("#user-info-usuario").removeAttr("disabled");
            $("#user-info-nombre").removeAttr("disabled");
            $("#user-info-paterno").removeAttr("disabled");
            $("#user-info-materno").removeAttr("disabled");
            $("#user-info-correo").removeAttr("disabled");
            $("#user-info-tel").removeAttr("disabled");
            $("#user-info-activo").removeAttr("disabled");

            $(".user-toolbar-read").hide();
            $(".user-toolbar-edit").show();

            break;
        case 'w':
            //$("#user-info-usuario").removeAttr("disabled");
            $("#user-reset-password-do").show();
            $("#user-reset-password-do").removeAttr("disabled");
            $("#user-info-nombre").removeAttr("disabled");
            $("#user-info-paterno").removeAttr("disabled");
            $("#user-info-materno").removeAttr("disabled");
            $("#user-info-correo").removeAttr("disabled");
            $("#user-info-tel").removeAttr("disabled");
            $("#user-info-activo").removeAttr("disabled");

            $(".user-toolbar-read").hide();
            $(".user-toolbar-edit").show();

            break;
        case 'r':
        default:
            $("#user-reset-password-do").show();
            $("#user-reset-password-do").attr("disabled", true);
            //$("#user-info-usuario").attr("disabled", true);
            $("#user-info-nombre").attr("disabled", true);
            $("#user-info-paterno").attr("disabled", true);
            $("#user-info-materno").attr("disabled", true);
            $("#user-info-correo").attr("disabled", true);
            $("#user-info-tel").attr("disabled", true);
            $("#user-info-activo").attr("disabled", true);

            $(".user-toolbar-edit").hide();
            $(".user-toolbar-read").show();

            break;
    }
}

function showMessageModalUser(message, temporal) {
    $(".user-info-error").html('');
    $(".user-info-aviso").html(message);
    if(temporal) {
        setTimeout(function () {
            $(".user-info-aviso").html('');
        }, 2500);
    }
}

function showErrorModalUser(error) {
    $(".user-info-aviso").html('');
    $(".user-info-error").html(error);
}

function validateTextRequired(value) {
    if(value == null || value == undefined || value.trim() == "") {
        return false;
    }
    return true;
}
function validateText(value) {
    var pattern=/[a-z|A-Z]/;
    if(value != null && value != undefined && value.trim() != "" && !value.trim().match(pattern)) {
        return false;
    }
    return true;
}
function validateTextWithWhitespaces(value) {
    var pattern=/[a-z|A-Z|\s]/;
    if(value != null && value != undefined && value.trim() != "" && !value.trim().match(pattern)) {
        return false;
    }
    return true;
}
function validateNumber(value) {
    var pattern=/[0-9]/;
    if(value != null && value != undefined && value.trim() != "" && !value.match(pattern)) {
        return false;
    }
    return true;
}
function validateDate(value) {
    var pattern=/^\d{4}([/-])\d{2}\1\d{2}$/;
    if(value != null && value != undefined && value.trim() != "" && !value.match(pattern)) {
        return false;
    }
    return true;
}
function validateClaveElector(value) {
    var pattern=/[a-z|A-Z]{6}\d{6}\d{2}[a-z|A-Z]{1}\d{1}[a-z|A-Z|\d]{2}$/;
    if(value != null && value != undefined && value.trim() != "" && !value.match(pattern)) {
        return false;
    }
    return true;
}

function getTextValue(value) {
    var valueWithFormat = value;
    if(value == null || value == undefined || value.trim() == "") {
        //valueWithFormat = null;//"";
        valueWithFormat = "";
    } else {
        valueWithFormat = value.trim();
    }
    return valueWithFormat;
}
function getTextValueUpper(value) {
    var valueWithFormat = value;
    if(value == null || value == undefined || value.trim() == "") {
        //valueWithFormat = null;//"";
        valueWithFormat = "";
    } else {
        valueWithFormat = value.trim().toUpperCase();
    }
    return valueWithFormat;
}

function formatValue(value) {
    var valueWithFormat = value;
    if(value == null || value == undefined) {
        valueWithFormat = "";
    } else {
        valueWithFormat = value.trim().toUpperCase()
    }
    return valueWithFormat;
}

function validateEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function validateDataUserInfo(){
    var $usuario = $("#user-info-usuario");
    var $nombre = $("#user-info-nombre");
    var $paterno = $("#user-info-paterno");
    var $materno = $("#user-info-materno");
    var $tel = $("#user-info-tel");
    var $correo = $("#user-info-correo");

    var valid = true;
    if(!validateTextRequired($usuario.val()) || !validateTextWithWhitespaces($usuario.val())) {
        valid = false;
        if(!$usuario.closest( ".form-group-sm").hasClass("has-error")) {
            $usuario.closest( ".form-group-sm").addClass("has-error");
        }
    }
    if(!validateTextRequired($nombre.val()) || !validateTextWithWhitespaces($nombre.val())) {
        valid = false;
        if(!$nombre.closest( ".form-group-sm").hasClass("has-error")) {
            $nombre.closest( ".form-group-sm").addClass("has-error");
        }
    }
    if(!validateTextRequired($paterno.val()) || !validateTextWithWhitespaces($paterno.val())) {
        valid = false;
        if(!$paterno.closest( ".form-group-sm").hasClass("has-error")) {
            $paterno.closest( ".form-group-sm").addClass("has-error");
        }
    }
    if(!validateTextRequired($materno.val()) || !validateTextWithWhitespaces($materno.val())) {
        valid = false;
        if(!$materno.closest( ".form-group-sm").hasClass("has-error")) {
            $materno.closest( ".form-group-sm").addClass("has-error");
        }
    }
    if($tel.val().trim() != '' && !validateNumber($tel.val())) {
        valid = false;
        if(!$tel.closest( ".form-group-sm").hasClass("has-error")) {
            $tel.closest( ".form-group-sm").addClass("has-error");
        }
    }
    if($correo.val().trim() != '' && !validateEmail($correo.val())) {
        valid = false;
        if(!$correo.closest( ".form-group-sm").hasClass("has-error")) {
            $correo.closest( ".form-group-sm").addClass("has-error");
        }
    }

    return valid;
}

function resetPassword(id) {
    // if($indexuser == null) {
    //     return;
    // }
    $.ajax({
        url: "/users/resetPassword/" + id,
        type: 'PUT',
        beforeSend: function(){
            // showMessageModalUser('<div style="text-align: center; display: inline-block">' +
            //     '<i class="fa fa-spinner fa-pulse"></i> Cargando...' +
            //     '</div>');
        },
        success: function(){
            showGeneralMessage("El password se reinicio correctamente", "success", true);
            // showMessageModalUser('El password se reinicio correctamente', true);
        },
        error: function(xhr, status, error){
            if(xhr.status == 501){
                showGeneralMessage("Método no implementado", "error", true);
                // showErrorModalUser('Método no implementado');
            }
            else if(xhr.status == 404){
                showGeneralMessage("No se encontro el usuario con id: " + id, "error", true);
                // showErrorModalUser('No se encontro el usuario con id: ' + $indexuser);
            }
            else{
                showGeneralMessage("Ocurrió un error al guardar los datos", "error", true);
                // showErrorModalUser('Ocurrió un error al guardar los datos');
            }
        }
    });
}