---------------------------------------------------------------------------------------------------
-- Tabla para guardar los tipos de Servicios a Cobrar
CREATE TABLE cliente.tipo_servicio (
    id integer NOT NULL PRIMARY KEY,
    nombre varchar(32) NOT NULL,
    descripcion varchar(256) NOT NULL,
    activo boolean DEFAULT true,
    fecha_creacion timestamp without time zone DEFAULT now(),
    fecha_modificacion timestamp without time zone DEFAULT now()
);

-- Ingresamos datos para el catalogo de tipo de servicio
BEGIN;
	INSERT INTO cliente.tipo_servicio ( id, nombre, descripcion,activo)
	VALUES (1, 'desalojo', 'Concepto de Desalojo para cobrar el Cliente', true);
COMMIT;

---------------------------------------------------------------------------------------------------
-- Tabla para indentificar los tipos de cobros en la tabla de Historial de pago
CREATE TABLE cliente.tipo_cobro_historial (
    id integer NOT NULL PRIMARY KEY,
    nombre varchar(32) NOT NULL,
    descripcion varchar(256) NOT NULL,
    activo boolean DEFAULT true,
    fecha_creacion timestamp without time zone DEFAULT now(),
    fecha_modificacion timestamp without time zone DEFAULT now()
);

-- Ingresamos los conceptos para identificar el cobro de servicios en historial_pago
BEGIN;
	INSERT INTO cliente.tipo_cobro_historial ( id, nombre, descripcion, activo)
	VALUES (1, 'cobrocliente', 'Concepto por Cobro de cliente', true);
	
	INSERT INTO cliente.tipo_cobro_historial ( id, nombre, descripcion, activo)
	VALUES (2, 'cobroservicio', 'Concepto por Cobro de Servicio', true);	
COMMIT;

-- Agregar la columna tipo_cobro para identificar los renglones del historial
ALTER TABLE cliente.historial_pago
  ADD COLUMN "idtipo_cobro" integer NOT NULL DEFAULT 1;

---------------------------------------------------------------------------------------------------
-- Colunma para almacenar el tipo de servicio 
ALTER TABLE cliente.historial_pago
  ADD COLUMN "idtipo_servicio" integer NOT NULL DEFAULT 0;

-- Colunma para almacenar los conceptos de servicio
ALTER TABLE cliente.historial_pago
  ADD COLUMN "concepto_servicio" varchar(256) NOT NULL DEFAULT '';
  
 -- Modificamos el valor por defecto
ALTER TABLE cliente.historial_pago
  ALTER COLUMN "concepto_servicio" SET DEFAULT '';
  
ALTER TABLE cliente.historial_pago
  ALTER COLUMN "idtipo_servicio" SET DEFAULT 0;
 
