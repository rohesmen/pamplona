create or replace view servicio.view_serviciosdetalles as
SELECT sd.id, sd.idorden_servicio, sd.costo, sd.costo_final, sd.activo, sd.idservicio, 
s.id as clave_servicio, s.nombre, s.idgrupo
FROM servicio.orden_servicio_detalle sd
left join servicio.servicios s on s.id = sd.idservicio;