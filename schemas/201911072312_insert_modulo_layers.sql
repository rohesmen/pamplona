with gm as (
	INSERT INTO "usuario"."grupomodulo"("siglas", "nombre", "descripcion", "orden", "activo", "fechacreacion", "fechamodificacion", "icono", "ruta", "panelclass") VALUES ('LAYERS', 'Capas', 'Capas', (select max(orden) from usuario.grupomodulo), 't', now(), now(), 'fa-map', '/layers', 'panel-primary') RETURNING id
),
m1 as (
 INSERT INTO "usuario"."modulo"("idgrupomodulo", "siglas", "nombre", "descripcion", "iconomodulo", "directorio", "activo", "fechamodificacion", "fechacreacion", "orden")
 select gm.id, 'MODLAYERS', 'Capas', 'Capas', 'fa-map', '/layers', 't', now(), now(), 1 from gm RETURNING id
),
p1 as (
	INSERT INTO "usuario"."permiso"(id, "nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo")
	select (select max(id) + 1 from usuario.permiso ),
	'Capas', 'Capas', 'layers', 'index', 't', now(), now(), m1.id from m1 RETURNING id
),
m2 as (
 INSERT INTO "usuario"."modulo"("idgrupomodulo", "siglas", "nombre", "descripcion", "iconomodulo", "directorio", "activo", "fechamodificacion", "fechacreacion", "orden")
 select gm.id, 'MODGROUPS', 'Grupos', 'Grupos', 'fa-map', '/groups', 't', now(), now(), 1 from gm RETURNING id
),
p2 as (
	INSERT INTO "usuario"."permiso"(id, "nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo")
	select (select max(id) + 2 from usuario.permiso ),
	'Grupos', 'Grupos', 'groups', 'index', 't', now(), now(), m2.id from m2 RETURNING id
),
pp1 as (
	INSERT INTO "usuario"."perfil_permiso"("idperfil", "idpermiso", "activo", "fecha_creacion", "fecha_modificacion")
	select 1, p1.id, true, now(), now() from p1
),
pp2 as (
	INSERT INTO "usuario"."perfil_permiso"("idperfil", "idpermiso", "activo", "fecha_creacion", "fecha_modificacion")
	select 1, p2.id, true, now(), now() from p2
)
select * from gm, m1, p1, m2, p2