

CREATE OR REPLACE VIEW monitoreo.view_ruta
    AS
SELECT
    v.id,
    upper(v.nombre) AS nombre,
    v.the_geom,
    v.activo,
    v.fecha_creacion,
    v.fecha_modificacion,
    CASE WHEN v.activo THEN 'SI' ELSE 'NO' END AS activo_f,
    CASE WHEN v.fecha_creacion IS NOT NULL THEN to_char(v.fecha_creacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_creacion_f,
    CASE WHEN v.fecha_modificacion IS NOT NULL THEN to_char(v.fecha_modificacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_modificacion_f,
    v.geojson,
    upper(v.descripcion) AS descripcion,
    kml
FROM monitoreo.ruta v;

