ALTER TABLE "cliente"."estatus_cliente"
  ADD COLUMN "permcobro" varchar(255),
  ADD COLUMN "permmen" varchar(255),
  ADD COLUMN "isfactura" bool DEFAULT false;