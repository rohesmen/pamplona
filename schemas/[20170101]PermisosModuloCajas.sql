--Creacion de Permisos y la relacion entre el perfil para el modulo de cajas
-----------------------------------------------------------------------------
BEGIN;
insert into usuario.permiso(
	id,
	nombre,
	descripcion,
	recurso,
	accion,
	activo,
	fecha_modificacion,
	fecha_creacion,
	idmodulo)
values(
	40,
	'Cobrar Recoleccion a Cliente',
	'Cobrar Recoleccion a Cliente',
	'caja',
	'cobrar_basura',
	true,
	now(),
	now(),
	3	
);
COMMIT;
-- Relacion entre el perfil y el permiso
BEGIN;
insert into usuario.perfil_permiso(
	idperfil,
	idpermiso,
	activo,
	fecha_creacion,
	fecha_modificacion)
values(
	1,
	40,
	true,
	now(),
	now()
);
COMMIT;
-----------------------------------------------------------------------------
BEGIN;
insert into usuario.permiso(
	id,
	nombre,
	descripcion,
	recurso,
	accion,
	activo,
	fecha_modificacion,
	fecha_creacion,
	idmodulo)
values(
	41,
	'Cobrar Servicio a Cliente',
	'Cobrar Servicio a Cliente',
	'caja',
	'cobrar_servicio',
	true,
	now(),
	now(),
	3	
);
COMMIT;
-- Relacion entre el perfil y el permiso
BEGIN;
insert into usuario.perfil_permiso(
	idperfil,
	idpermiso,
	activo,
	fecha_creacion,
	fecha_modificacion)
values(
	1,
	41,
	true,
	now(),
	now()
);
COMMIT;
-----------------------------------------------------------------------------
BEGIN;
insert into usuario.permiso(
	id,
	nombre,
	descripcion,
	recurso,
	accion,
	activo,
	fecha_modificacion,
	fecha_creacion,
	idmodulo)
values(
	42,
	'Mostrar Ubicacion de Cliente',
	'Mostrar Ubicacion de Cliente',
	'caja',
	'ver_ubicacion',
	true,
	now(),
	now(),
	3	
);
COMMIT;
-- Relacion entre el perfil y el permiso
BEGIN;
insert into usuario.perfil_permiso(
	idperfil,
	idpermiso,
	activo,
	fecha_creacion,
	fecha_modificacion)
values(
	1,
	42,
	true,
	now(),
	now()
);
COMMIT;
-----------------------------------------------------------------------------
BEGIN;
insert into usuario.permiso(
	id,
	nombre,
	descripcion,
	recurso,
	accion,
	activo,
	fecha_modificacion,
	fecha_creacion,
	idmodulo)
values(
	43,
	'Mostrar Historial de Pagos',
	'Mostrar Historial de Pagos',
	'caja',
	'ver_historial',
	true,
	now(),
	now(),
	3	
);
COMMIT;
-- Relacion entre el perfil y el permiso
BEGIN;
insert into usuario.perfil_permiso(
	idperfil,
	idpermiso,
	activo,
	fecha_creacion,
	fecha_modificacion)
values(
	1,
	43,
	true,
	now(),
	now()
);
COMMIT;
-----------------------------------------------------------------------------