CREATE TABLE folios.secuencia_folios (
  id serial,
	idusuario integer,
  activo bool DEFAULT true,
  fecha_creacion timestamp WITHOUT time zone DEFAULT now(),
  fecha_modificacion timestamp WITHOUT time zone DEFAULT now(),
  CONSTRAINT pku_secuencia_folios PRIMARY KEY (id)
);

insert into secuencia_folios (idusuario) values (1);