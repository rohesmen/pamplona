drop view geodata.clientes_view;
create or replace view geodata.clientes_view as
  with meses as (
	select c.id_cliente,
		date_part('year', age(now(), ('01/' || c.ultimo_mes_pago || '/' || c.ultimo_anio_pago)::date)) + date_part('month', age(now(), ('01/' || c.ultimo_mes_pago || '/' || c.ultimo_anio_pago)::date)) meses_a_pagar,
		case WHEN cobro_manual THEN
			COALESCE(mensualidad,0)
		ELSE
				COALESCE(col.monto,0)	END 
		mensualidad,
	col.nombre || COALESCE(', ' || col.localidad, '') colonia
	FROM cliente.cliente c
	LEFT JOIN cliente.colonia col ON c.idcolonia = col.id
 ),
 clientesss as (
		SELECT c.id_cliente,
    c.id_cliente AS no_cliente,
    ((((COALESCE(('C. '::text || c.calle), ''::text) || COALESCE(c.calle_letra, ''::text)) || COALESCE((' #'::text || c.numero), ''::text)) || COALESCE(c.numero_letra, ''::text)) || COALESCE((' COl. '::text || meses.colonia), ''::text)) AS descripcion,
    '01/' || lpad(c.ultimo_mes_pago,2,'0') || '/' || c.ultimo_anio_pago AS ultimo_pago,
    p.fecha_pago::date fecha_ultimo_pago,
    meses.meses_a_pagar AS total_meses,
    meses.mensualidad * meses.meses_a_pagar AS importe,
    meses.mensualidad AS total,
    meses.meses_a_pagar,
    c.the_geom,
    c.latitud,
    c.longitud,
    c.ubicado,
    COALESCE(c.nombres, ''::text) || 
		COALESCE(' ' || c.apepat, '') || 
		COALESCE(' '::text || c.apemat, '') AS nombre,
    c.calle || COALESCE(calle_letra, '') calle,
    c.numero  || COALESCE(numero_letra, '') numero,
    c.cruza1,
    c.cruza2,
    meses.colonia,
    c.activo,
    c.fecha_creacion,
    c.fecha_modificacion,
    NULL::text AS zona,
    c.folio_catastral,
    NULL::text AS origen
   FROM cliente.cliente c
   LEFT JOIN meses on c.id_cliente = meses.id_cliente
	 left join (
	SELECT distinct on (idcliente) usuario, idcliente, fecha_pago
	FROM cliente.historial_pago hp
	left join usuario.usuario u on u.id = hp.idusuario
	order by idcliente, hp.fecha_creacion desc
) p on c.id_cliente = p.idcliente
  WHERE c.ubicado AND c.activo
	ORDER BY c.id_cliente
)
select * from clientesss