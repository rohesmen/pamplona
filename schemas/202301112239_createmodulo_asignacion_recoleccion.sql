DO $$
DECLARE
id_grupomodulo integer;
    id_modulo integer;
    id_permiso integer;
BEGIN


SELECT id INTO id_grupomodulo FROM usuario.grupomodulo WHERE siglas = 'MODSEG' AND nombre = 'Seguimiento' AND ruta = '/seguimiento' AND activo = true;


INSERT INTO usuario.modulo(idgrupomodulo, siglas, nombre, descripcion, iconomodulo, directorio, activo, fechamodificacion, fechacreacion, orden)
VALUES (id_grupomodulo, 'ASIGSEGUI', 'Asignación', 'Módulo de Asignación', 'fa-calendar', '/seguimientoasig', true, now(), now(), (select max(orden) + 1 from usuario.modulo)) RETURNING id INTO id_modulo;


INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
values ('Asignación seguimiento de recolección', 'Acceso al modulo de Asignación de seguimineto de recoleccion', 'seguimientoasig', 'index', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;

INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
values ('Crear seguimiento de recolección', 'Permiso para crear seguimiento de recolección', 'seguimientoasig', 'create', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;

INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
values ('Editar seguimiento de recolección', 'Permiso para editar seguimiento de recolección', 'seguimientoasig', 'edit', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;

INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
values ('Desactivar seguimiento de recolección', 'Permiso para desactivar seguimiento de recolección', 'seguimientoasig', 'deactivate', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;

INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


END $$;


