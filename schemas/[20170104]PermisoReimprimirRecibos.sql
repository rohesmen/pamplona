--Creacion del Permiso para la Reimpresion de Recibos
-----------------------------------------------------------------------------
BEGIN;
insert into usuario.permiso(
	id,
	nombre,
	descripcion,
	recurso,
	accion,
	activo,
	fecha_modificacion,
	fecha_creacion,
	idmodulo)
values(
	44,
	'Reimprimir Recibo',
	'Reimprimir Recibo',
	'caja',
	'reimprimir_recibo',
	true,
	now(),
	now(),
	3	
);
COMMIT;
-- Relacion entre el perfil y el permiso
BEGIN;
insert into usuario.perfil_permiso(
	idperfil,
	idpermiso,
	activo,
	fecha_creacion,
	fecha_modificacion)
values(
	1,
	44,
	true,
	now(),
	now()
);
COMMIT;