CREATE OR REPLACE VIEW comun.view_bitacora_mensualidades AS 
select b.identificador cliente, (cambios::json)->>'mensualidad' mensualidad, 
u.usuario usuario, u.id idusuario, to_char(b.fecha_creacion, 'DD/MM/YYYY HH24:MI'::text) fecha,
		b.apartado origen,
		b.motivo
from comun.bitacora b
left join usuario.usuario u on b.idusuario = u.id
join (
	select count(1), b.identificador
	from comun.bitacora b
	left join usuario.usuario u on b.idusuario = u.id
	where b.accion ilike '%ACTUALIZAR MENSUALIDAD%'
	GROUP BY b.identificador HAVING count(1) > 1
) c on c.identificador = b.identificador
where 
--b.accion = 'ACTUALIZAR MENSUALIDAD' and 
b.idusuario != 1
order by b.identificador, b.fecha_creacion desc;