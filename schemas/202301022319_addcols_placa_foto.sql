ALTER TABLE "monitoreo"."unidad"
    ADD COLUMN "placa" varchar(255),
  ADD COLUMN "numero_economico" varchar(255),
  ADD CONSTRAINT "uq_placa" UNIQUE ("placa");


ALTER TABLE "monitoreo"."chofer"
    ADD COLUMN "foto" varchar(255),
  ADD COLUMN "ruta_foto" varchar(255);

--truncate "monitoreo"."unidad" restart IDENTITY;

ALTER TABLE "monitoreo"."unidad"
DROP CONSTRAINT "pk_unidad",
  DROP COLUMN "id",
  ALTER COLUMN "placa" SET NOT NULL,
  ADD CONSTRAINT "pk_unidad" PRIMARY KEY ("placa");

ALTER TABLE "monitoreo"."recolector"
    ADD COLUMN "foto" varchar,
  ADD COLUMN "ruta_foto" varchar(255);

CREATE TABLE "monitoreo"."cuadrilla" (
                                         "id" serial,
                                         nombre varchar,
                                         idusuario integer,
                                         "activo" bool DEFAULT true,
                                         "fecha_creacion" timestamp(6) DEFAULT now(),
                                         "fecha_modificacion" timestamp(6) DEFAULT now(),
                                         CONSTRAINT "pku_cuadrilla" PRIMARY KEY ("id"),
                                         CONSTRAINT fk_idusuario FOREIGN KEY (idusuario)
                                             REFERENCES usuario.usuario (id) MATCH SIMPLE
                                             ON UPDATE NO ACTION
                                             ON DELETE NO ACTION
);

CREATE TABLE "monitoreo"."cuadrilla_recolector" (
                                                    "id" serial,
                                                    idcuadrilla integer,
                                                    idrecolector integer,
                                                    ischofer bool default false,
                                                    idusuario integer,
                                                    "activo" bool DEFAULT true,
                                                    "fecha_creacion" timestamp(6) DEFAULT now(),
                                                    "fecha_modificacion" timestamp(6) DEFAULT now(),
                                                    CONSTRAINT "pku_cuadrilla_recolector" PRIMARY KEY ("id"),
                                                    CONSTRAINT fk_idrecolector FOREIGN KEY (idrecolector)
                                                        REFERENCES monitoreo.recolector (id) MATCH SIMPLE
                                                        ON UPDATE NO ACTION
                                                        ON DELETE NO ACTION,
                                                    CONSTRAINT fk_idusuario FOREIGN KEY (idusuario)
                                                        REFERENCES usuario.usuario (id) MATCH SIMPLE
                                                        ON UPDATE NO ACTION
                                                        ON DELETE NO ACTION,
                                                    CONSTRAINT fk_idcuadrilla FOREIGN KEY (idcuadrilla)
                                                        REFERENCES "monitoreo"."cuadrilla" (id) MATCH SIMPLE
                                                        ON UPDATE NO ACTION
                                                        ON DELETE NO ACTION
);

CREATE TABLE "monitoreo"."cuadrilla_unidad" (
                                                "id" serial,
                                                idcuadrilla integer,
                                                placa varchar,
                                                idusuario integer,
                                                "activo" bool DEFAULT true,
                                                "fecha_creacion" timestamp(6) DEFAULT now(),
                                                "fecha_modificacion" timestamp(6) DEFAULT now(),
                                                CONSTRAINT "pku_cuadrilla_unidad" PRIMARY KEY ("id"),
                                                CONSTRAINT fk_unidad FOREIGN KEY (placa)
                                                    REFERENCES "monitoreo"."unidad" (placa) MATCH SIMPLE
                                                    ON UPDATE NO ACTION
                                                    ON DELETE NO ACTION
);

CREATE TABLE "monitoreo"."unidad_ruta" (
                                           "id" serial,
                                           placa varchar,
                                           idruta integer,
                                           idusuario integer,
                                           "activo" bool DEFAULT true,
                                           "fecha_creacion" timestamp(6) DEFAULT now(),
                                           "fecha_modificacion" timestamp(6) DEFAULT now(),
                                           CONSTRAINT "pku_unidad_ruta" PRIMARY KEY ("id"),
                                           CONSTRAINT fk_idusuario FOREIGN KEY (idusuario)
                                               REFERENCES usuario.usuario (id) MATCH SIMPLE
                                               ON UPDATE NO ACTION
                                               ON DELETE NO ACTION,
                                           CONSTRAINT fk_idruta FOREIGN KEY (idruta)
                                               REFERENCES "monitoreo"."ruta" (id) MATCH SIMPLE
                                               ON UPDATE NO ACTION
                                               ON DELETE NO ACTION,
                                           CONSTRAINT fk_unidad FOREIGN KEY (placa)
                                               REFERENCES "monitoreo"."unidad" (placa) MATCH SIMPLE
                                               ON UPDATE NO ACTION
                                               ON DELETE NO ACTION
);

ALTER TABLE "monitoreo"."dias_recoleccion"
    ADD COLUMN "dia_bd" int4;

update monitoreo.dias_recoleccion set dia_bd = 1 where sigla = 'LUNES';
update monitoreo.dias_recoleccion set dia_bd = 2 where sigla = 'MARTES';
update monitoreo.dias_recoleccion set dia_bd = 3 where sigla = 'MIERCOLES';
update monitoreo.dias_recoleccion set dia_bd = 4 where sigla = 'JUEVES';
update monitoreo.dias_recoleccion set dia_bd = 5 where sigla = 'VIERNES';
update monitoreo.dias_recoleccion set dia_bd = 6 where sigla = 'SABADO';
update monitoreo.dias_recoleccion set dia_bd = 0 where sigla = 'DOMINGO';