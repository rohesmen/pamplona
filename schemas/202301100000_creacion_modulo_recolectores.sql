



DO $$ 
DECLARE
	id_grupomodulo integer;
    id_modulo integer;
    id_permiso integer;
BEGIN


    SELECT id INTO id_grupomodulo FROM usuario.grupomodulo WHERE siglas = 'MODSEG' AND nombre = 'Seguimiento' AND ruta = '/seguimiento' AND activo = true;


    INSERT INTO usuario.modulo(idgrupomodulo, siglas, nombre, descripcion, iconomodulo, directorio, activo, fechamodificacion, fechacreacion, orden)
	    VALUES (id_grupomodulo, 'RECOLECTSEG', 'Recolectores', 'Módulo de Recolectores', 'fa-share', '/recolectoresseguimiento', true, now(), now(), (select max(orden) + 1 from usuario.modulo)) RETURNING id INTO id_modulo;


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Recolectores', 'Acceso al modulo de recolectores', 'recolectoresseguimiento', 'index', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Crear Recolector', 'Permiso para crear recolector', 'recolectoresseguimiento', 'create', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());
	

	INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Editar Recolector', 'Permiso para editar recolector', 'recolectoresseguimiento', 'edit', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Consultar Recolector', 'Permiso para consultar recolector', 'recolectoresseguimiento', 'info', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Activar Recolector', 'Permiso para activar recolector', 'recolectoresseguimiento', 'activate', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


	INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Desactivar Recolector', 'Permiso para desactivar recolector', 'recolectoresseguimiento', 'deactivate', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


END $$;


--DROP VIEW monitoreo.view_recolector;
CREATE OR REPLACE VIEW monitoreo.view_recolector
    AS
    SELECT
        v.id,
		upper(v.nombres) AS nombres,
		upper(v.apepat) AS apepat,
		upper(v.apemat) AS apemat,
        v.foto,
		v.ruta_foto,
        v.activo,
        v.fechacreacion,
        v.fechamodificacion,
        CASE WHEN v.activo THEN 'SI' ELSE 'NO' END AS activo_f,
        CASE WHEN v.fechacreacion IS NOT NULL THEN to_char(v.fechacreacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_creacion_f,
        CASE WHEN v.fechamodificacion IS NOT NULL THEN to_char(v.fechamodificacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_modificacion_f
    FROM monitoreo.recolector v;

