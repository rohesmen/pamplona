ALTER TABLE "usuario"."perfil_permiso"
  ALTER COLUMN "activo" SET DEFAULT true,
  ALTER COLUMN "fecha_creacion" SET DEFAULT now(),
  ALTER COLUMN "fecha_modificacion" SET DEFAULT now();