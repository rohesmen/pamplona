/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 100010
 Source Host           : localhost:5432
 Source Catalog        : sana
 Source Schema         : comun

 Target Server Type    : PostgreSQL
 Target Server Version : 100010
 File Encoding         : 65001

 Date: 07/11/2019 22:42:46
*/


-- ----------------------------
-- Table structure for capas
-- ----------------------------
DROP TABLE IF EXISTS "comun"."capas";
CREATE TABLE "comun"."capas" (
  "id" serial,
  "nombre" text COLLATE "pg_catalog"."default",
  "descripcion" text COLLATE "pg_catalog"."default",
  "capa" text COLLATE "pg_catalog"."default",
  "estilo" text COLLATE "pg_catalog"."default",
  "estilo_etiqueta" text COLLATE "pg_catalog"."default",
  "orden" int4,
  "servidor" text COLLATE "pg_catalog"."default",
  "inicial" bool DEFAULT false,
  "base" bool DEFAULT false,
  "cql" bool DEFAULT false,
  "modulo" varchar COLLATE "pg_catalog"."default",
  "google" bool DEFAULT false,
  "camposinfo" text COLLATE "pg_catalog"."default",
  "idgrupo_capa" int4,
  "tematico" bool DEFAULT true,
  "conetiqueta" bool DEFAULT true,
  "activo" bool DEFAULT true,
  "fecha_creacion" timestamp(6) DEFAULT now(),
  "fecha_modificacion" timestamp(6) DEFAULT now(),
  "tabla" varchar(100) COLLATE "pg_catalog"."default",
  "esquema" varchar(100) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Primary Key structure for table capas
-- ----------------------------
ALTER TABLE "comun"."capas" ADD CONSTRAINT "pku_capas" PRIMARY KEY ("id");
