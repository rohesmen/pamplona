
ALTER TABLE cliente.estatus_cliente
ADD COLUMN iscomercio boolean DEFAULT false;

ALTER TABLE cliente.colonia
ADD COLUMN ismarginado boolean DEFAULT false;

UPDATE cliente.colonia
SET ismarginado = true
WHERE monto IS NULL OR monto = 0;

UPDATE cliente.estatus_cliente
SET iscomercio = true
WHERE upper(trim(nombre)) like upper(trim('%comercio%'));
