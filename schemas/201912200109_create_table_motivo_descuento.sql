CREATE TABLE cliente.motivo_descuento (
  id serial,
	idusuario_solicito integer,
	idusuario_valido integer,
	idpago integer,
	motivo text,
  activo boolean default false,
  fecha_creacion timestamp WITHOUT time zone default now(),
  fecha_modificacion timestamp WITHOUT time zone default now(),
  CONSTRAINT pku_motivo_descuento PRIMARY KEY (id)
)