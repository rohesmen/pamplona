CREATE or replace view geodata.clientes_pago_anual_view as
 SELECT c.id_cliente,
    c.no_cliente,
    c.descripcion,
    c.ultimo_pago,
    c.fecha_ultimo_pago,
    c.total_meses,
    c.importe,
    c.total,
    c.meses_a_pagar,
    c.the_geom,
    c.latitud,
    c.longitud,
    c.ubicado,
    c.nombre,
    c.calle,
    c.numero,
    c.cruza1,
    c.cruza2,
    c.colonia,
    c.activo,
    c.fecha_creacion,
    c.fecha_modificacion,
    c.zona,
    c.folio_catastral,
    c.origen
   FROM geodata.clientes_view c
  WHERE c.ultimo_pago is not null and c.fecha_ultimo_pago is not null and
	c.ultimo_pago::date = to_date(('01/12/'::text || date_part('year'::text, now())), 'DD/MM/YYYY'::text)
	AND c.fecha_ultimo_pago::date >=
		to_date(('01/01/'::text || date_part('year'::text, now())), 'DD/MM/YYYY'::text)
	AND c.fecha_ultimo_pago::date
		<= to_date(('31/01/'::text || date_part('year'::text, now())), 'DD/MM/YYYY'::text)