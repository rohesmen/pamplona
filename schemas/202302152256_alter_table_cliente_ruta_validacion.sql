ALTER TABLE "cliente"."cliente"
    ADD COLUMN "ruta_validada" bool DEFAULT false,
  ADD COLUMN "idusaurio_valida_ruta" int4,
  ADD COLUMN "observaciones_valida_ruta" varchar,
  ADD CONSTRAINT "fk_idusuario_ruta_valida" FOREIGN KEY ("idusaurio_valida_ruta") REFERENCES "usuario"."usuario" ("id");

ALTER TABLE "cliente"."cliente"
    ADD COLUMN "fecha_ruta_validada" timestamp(6);


create or replace view cliente.view_clientes as
SELECT c.id_cliente,
       c.calle_letra,
       c.tipo_calle,
       c.numero_letra,
       c.tipo_numero,
       c.cruza1,
       c.letra_cruza1,
       c.tipo_cruza1,
       c.cruza2,
       c.letra_cruza2,
       c.tipo_cruza2,
       c.idcolonia,
       c.localidad,
       c.apepat,
       c.apemat,
       c.nombres,
       c.apepat_propietario,
       c.apemat_propietario,
       c.nombres_propietario,
       c.folio_catastral,
       c.idestatuscliente,
       c.activo,
       c.fecha_creacion,
       c.fecha_modificacion,
       c.latitud,
       c.longitud,
       c.ubicado,
       c.idtarifa_colonia,
       c.idruta,
       c.idtipo_contrato,
       c.referencia_ubicacion,
       c.fisica,
       c.nombre_moral,
       c.rfc,
       c.direccion_moral,
       c.telefono,
       c.correo,
       c.idtipo_descuento,
       c.descuento,
       c.mensualidad,
       c.ultimo_anio_pago,
       c.ultimo_mes_pago_old,
       c.observacion,
       c.calle,
       c.numero,
       c.direccion_otro,
       c.vigente,
       c.numero_contrato,
       c.id_usuario,
       c.razon_social,
       c.monto_tasa_fija,
       c.cantidad_tamboreo,
       c.codigo_contpaqi,
       c.nombre_comercial,
       c.nombre_sucursal,
       c.contacto_servicio,
       c.contacto_servicio_tel,
       c.contacto_servicio_ext,
       c.contacto_pago1,
       c.contacto_pago1_tel,
       c.contacto_pago1_ext,
       c.contacto_pago2,
       c.contacto_pago2_tel,
       c.contacto_pago2_ext,
       c.fiscal_calle,
       c.fiscal_calle_letra,
       c.fiscal_calle_tipo,
       c.fiscal_numero,
       c.fiscal_numero_letra,
       c.fiscal_numero_tipo,
       c.fiscal_cruz1,
       c.fiscal_cruz1_letra,
       c.fiscal_cruz1_tipo,
       c.fiscal_cruz2,
       c.fiscal_cruz2_letra,
       c.fiscal_cruz2_tipo,
       c.dias_servicio,
       c.metodo_pago,
       c.contrato,
       c.periodicidad_facturacion,
       c.idfacturacion_tamboreo,
       c.fiscal_idcolonia,
       c.idmetodo_pago,
       c.tipo_facturacion,
       c.cp,
       c.idrazon_social,
       c.ultimo_mes_pago,
       c.cobro_manual,
       c.monto_cobro,
       (COALESCE(btrim(c.calle), ''::text) || COALESCE(btrim(c.calle_letra), ''::text)) AS calle_completa,
       (COALESCE(btrim(c.numero), ''::text) || COALESCE(btrim(c.numero_letra), ''::text)) AS numero_completa,
       (COALESCE(btrim((c.cruza1)::text), ''::text) || COALESCE(btrim((c.letra_cruza1)::text), ''::text)) AS cruza1_completa,
       (COALESCE(btrim((c.cruza2)::text), ''::text) || COALESCE(btrim((c.letra_cruza2)::text), ''::text)) AS cruza2_completa,
       ((((COALESCE(c.apepat, ''::text) || ' '::text) || COALESCE(c.apemat, ''::text)) || ' '::text) || COALESCE(c.nombres, ''::text)) AS persona,
    ((((COALESCE(c.apepat_propietario, ''::text) || ' '::text) || COALESCE(c.apemat_propietario, ''::text)) || ' '::text) || COALESCE(c.nombres_propietario, ''::text)) AS propietario,
    ((COALESCE(col.nombre, ''::text) || ', '::text) || COALESCE(col.localidad, ''::text)) AS colonia,
    c.abandonado,
    c.comercio,
    c.marginado,
    c.privada,
    c.nombre_comercio,
    c.nombre_privada,
        CASE
            WHEN (cp.id_cliente IS NOT NULL) THEN true
            ELSE false
END AS pensionado,
		ec.nombre estatus,
		ec.sigla estatus_sigla,
		ruta_validada,
		r.nombre ruta,
		u.usuario usuario_valida_ruta,
		c.idusaurio_valida_ruta,
		c.fecha_ruta_validada,
c.observaciones_valida_ruta
   FROM cliente.cliente c
     LEFT JOIN cliente.colonia col ON col.id = c.idcolonia
     LEFT JOIN cliente.cliente_descuento_pensionado cp ON c.id_cliente = cp.id_cliente and cp.fecha_inicio::date <= now()::date  and cp.fecha_fin::date >= now()::date
		 left join cliente.estatus_cliente ec on ec.id = c.idestatuscliente
		 left join monitoreo.ruta r on c.idruta = r.id
		 left join usuario.usuario u on c.idusaurio_valida_ruta = u.id;

with pevalidaruta as (
INSERT INTO "usuario"."permiso"("nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo") VALUES (
    'Validar ruta cliente', 'Validar ruta cliente', 'clientes', 'valru', 't', now(), now(),
    (select id from usuario.modulo where siglas = 'CLI' limit 1)
    ) RETURNING id
    ),
    perper as (
insert into usuario.perfil_permiso (idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
select 1, id, true, now(), now() from pevalidaruta
    )
select f.id from pevalidaruta f