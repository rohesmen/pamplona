--Creacion del Permiso y la relacion entre el perfil para aplicar
-- descuento para el modulo de cajas, al cobrar las mensualidades
-- que exceden los 12 meses
BEGIN;
insert into usuario.permiso(
	id,
	nombre,
	descripcion,
	recurso,
	accion,
	activo,
	fecha_modificacion,
	fecha_creacion,
	idmodulo)
values(
	29,
	'Descuento Caja',
	'Descuento Caja',
	'descuento',
	'index',
	true,
	now(),
	now(),
	3	
);
COMMIT;

-- Relacion entre el perfil y el permiso
BEGIN;
insert into usuario.perfil_permiso(
	idperfil,
	idpermiso,
	activo,
	fecha_creacion,
	fecha_modificacion)
values(
	1,
	29,
	true,
	now(),
	now()
);
COMMIT;