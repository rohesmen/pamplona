--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.0
-- Dumped by pg_dump version 9.6.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: usuario; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA usuario;


ALTER SCHEMA usuario OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = usuario, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: aplicacion; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE aplicacion (
    id integer NOT NULL,
    nombre text,
    descripcion text,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE aplicacion OWNER TO postgres;

--
-- Name: aplicacion_usuario; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE aplicacion_usuario (
    idaplicacion integer NOT NULL,
    idusuario integer NOT NULL,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE aplicacion_usuario OWNER TO postgres;

--
-- Name: grupomodulo; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE grupomodulo (
    id integer NOT NULL,
    siglas character varying(50) NOT NULL,
    nombre character varying(100) NOT NULL,
    descripcion character varying(250),
    orden integer NOT NULL,
    activo boolean DEFAULT true NOT NULL,
    fechacreacion timestamp(0) without time zone DEFAULT now(),
    fechamodificacion timestamp(0) without time zone DEFAULT now(),
    icono text NOT NULL,
    ruta text,
    panelclass character varying DEFAULT 'panel-primary'::character varying
);


ALTER TABLE grupomodulo OWNER TO postgres;

--
-- Name: TABLE grupomodulo; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON TABLE grupomodulo IS 'Catálogo en el que se relaciona a los grupos de modulo en los que se puede clasificar o dividir un sistema.';


--
-- Name: grupomodulo_idgrupomodulo_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE grupomodulo_idgrupomodulo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE grupomodulo_idgrupomodulo_seq OWNER TO postgres;

--
-- Name: grupomodulo_idgrupomodulo_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: postgres
--

ALTER SEQUENCE grupomodulo_idgrupomodulo_seq OWNED BY grupomodulo.id;


--
-- Name: modulo; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE modulo (
    id integer NOT NULL,
    idgrupomodulo integer NOT NULL,
    siglas character varying(50) NOT NULL,
    nombre character varying(150) NOT NULL,
    descripcion character varying(250),
    iconomodulo text,
    directorio text NOT NULL,
    activo boolean DEFAULT true NOT NULL,
    fechamodificacion timestamp(0) without time zone DEFAULT now() NOT NULL,
    fechacreacion timestamp(0) without time zone DEFAULT now() NOT NULL,
    orden integer DEFAULT 0 NOT NULL
);


ALTER TABLE modulo OWNER TO postgres;

--
-- Name: TABLE modulo; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON TABLE modulo IS 'Catálogo en el que se relaciona a los modulos en los que se puede clasificar o dividir un sistema.';


--
-- Name: modulo_idmodulo_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE modulo_idmodulo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE modulo_idmodulo_seq OWNER TO postgres;

--
-- Name: modulo_idmodulo_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: postgres
--

ALTER SEQUENCE modulo_idmodulo_seq OWNED BY modulo.id;


--
-- Name: perfil; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE perfil (
    id integer NOT NULL,
    nombre text,
    descripcion text,
    activo boolean DEFAULT true,
    fecha_creacion timestamp without time zone DEFAULT now(),
    fecha_modificacion timestamp without time zone DEFAULT now()
);


ALTER TABLE perfil OWNER TO postgres;

--
-- Name: perfil_indicador; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE perfil_indicador (
    idperfil integer NOT NULL,
    idindicador integer NOT NULL,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE perfil_indicador OWNER TO postgres;

--
-- Name: perfil_permiso; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE perfil_permiso (
    idperfil integer NOT NULL,
    idpermiso integer NOT NULL,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE perfil_permiso OWNER TO postgres;

--
-- Name: perfil_usuario; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE perfil_usuario (
    idperfil integer NOT NULL,
    idusuario integer NOT NULL,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE perfil_usuario OWNER TO postgres;

--
-- Name: permiso; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE permiso (
    id integer NOT NULL,
    nombre text,
    descripcion text,
    recurso text,
    accion text,
    activo boolean DEFAULT true,
    fecha_modificacion timestamp without time zone DEFAULT now(),
    fecha_creacion timestamp without time zone DEFAULT now(),
    idmodulo integer
);


ALTER TABLE permiso OWNER TO postgres;

--
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario_id_seq OWNER TO postgres;

--
-- Name: usuario; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario (
    id integer DEFAULT nextval('usuario_id_seq'::regclass) NOT NULL,
    usuario text,
    clave character varying(64),
    correo text,
    activo boolean,
    fecha_modificacion timestamp without time zone,
    fecha_creacion timestamp without time zone,
    idestructura integer,
    general boolean DEFAULT true NOT NULL,
    nombre text,
    apellido_paterno text,
    apellido_materno text,
    idterritorio json,
    tipo_territorio text,
    telefono text
);


ALTER TABLE usuario OWNER TO postgres;

--
-- Name: grupomodulo id; Type: DEFAULT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY grupomodulo ALTER COLUMN id SET DEFAULT nextval('grupomodulo_idgrupomodulo_seq'::regclass);


--
-- Name: modulo id; Type: DEFAULT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY modulo ALTER COLUMN id SET DEFAULT nextval('modulo_idmodulo_seq'::regclass);


--
-- Data for Name: aplicacion; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY aplicacion (id, nombre, descripcion, activo, fecha_creacion, fecha_modificacion) FROM stdin;
\.


--
-- Data for Name: aplicacion_usuario; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY aplicacion_usuario (idaplicacion, idusuario, activo, fecha_creacion, fecha_modificacion) FROM stdin;
\.


--
-- Data for Name: grupomodulo; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY grupomodulo (id, siglas, nombre, descripcion, orden, activo, fechacreacion, fechamodificacion, icono, ruta, panelclass) FROM stdin;
1	USR	Administrativo	USUARIOS	1	t	2016-10-12 20:21:42	2016-10-12 20:21:42	fa-user	/users	panel-primary
\.


--
-- Name: grupomodulo_idgrupomodulo_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('grupomodulo_idgrupomodulo_seq', 1, false);


--
-- Data for Name: modulo; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY modulo (id, idgrupomodulo, siglas, nombre, descripcion, iconomodulo, directorio, activo, fechamodificacion, fechacreacion, orden) FROM stdin;
1	1	USR	USUARIOS	USUARIOS	fa-user	/users	t	2016-10-12 20:22:59	2016-10-12 20:22:59	1
\.


--
-- Name: modulo_idmodulo_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('modulo_idmodulo_seq', 1, false);


--
-- Data for Name: perfil; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY perfil (id, nombre, descripcion, activo, fecha_creacion, fecha_modificacion) FROM stdin;
1	administrador	administrador	t	2016-10-12 20:13:41.974336	2016-10-12 20:13:41.974336
\.


--
-- Data for Name: perfil_indicador; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY perfil_indicador (idperfil, idindicador, activo, fecha_creacion, fecha_modificacion) FROM stdin;
\.


--
-- Data for Name: perfil_permiso; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY perfil_permiso (idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion) FROM stdin;
1	1	t	2016-10-12 20:24:31.650621	2016-10-12 20:24:40.403093
1	2	t	2016-10-12 21:00:13.380486	2016-10-12 21:00:13.380486
1	3	t	2016-10-12 21:01:06.352622	2016-10-12 21:01:06.352622
1	4	t	2016-10-12 21:05:02.132355	2016-10-12 21:05:02.132355
1	5	t	2016-10-12 21:08:54.839453	2016-10-12 21:08:54.839453
1	6	t	2016-10-12 21:13:50.833608	2016-10-12 21:13:50.833608
\.


--
-- Data for Name: perfil_usuario; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY perfil_usuario (idperfil, idusuario, activo, fecha_creacion, fecha_modificacion) FROM stdin;
1	1	t	2016-10-12 20:56:20.896899	2016-10-12 20:56:20.896899
1	2	t	2016-10-13 02:15:10	2016-10-13 02:15:10
\.


--
-- Data for Name: permiso; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY permiso (id, nombre, descripcion, recurso, accion, activo, fecha_modificacion, fecha_creacion, idmodulo) FROM stdin;
1	Usuario	Usuarios	users	index	t	2016-10-12 20:18:16.652923	2016-10-12 20:18:16.652923	1
2	Busquedas	busquedas	users	search	t	2016-10-12 20:59:22.904334	2016-10-12 20:59:22.904334	1
3	Agregar usuarios	Agregar usuario	users	add	t	2016-10-12 21:01:46.631078	2016-10-12 21:01:46.631078	1
4	Editar	Editar	users	edit	t	2016-10-12 21:05:42.402932	2016-10-12 21:05:42.402932	1
5	Agregar perfiles	Agregar perfiles	users	saveProfiles	t	2016-10-12 21:08:28.596796	2016-10-12 21:08:28.596796	1
6	Editar perfiles	Editar Perfiles	users	editProfiles	t	2016-10-12 21:14:43.704039	2016-10-12 21:14:43.704039	1
\.


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario (id, usuario, clave, correo, activo, fecha_modificacion, fecha_creacion, idestructura, general, nombre, apellido_paterno, apellido_materno, idterritorio, tipo_territorio, telefono) FROM stdin;
1	admin	$2a$08$Wv25Q1NEjnuJL0GNnX0M5OSBaTCTD/szsU6CNzSlKEJfIU.MjBtb.	\N	t	2016-10-12 20:47:00.725523	2016-10-12 20:47:00.725523	\N	t	admin	admin	admin	\N	\N	\N
2	test	$2y$08$dEY2dkdsVWFXWEpzU0hZTerCYDMriz.XQr6fMkpCPd.VOw8Qljepq	\N	t	2016-10-13 02:03:40	2016-10-13 02:03:40	\N	t	TEST	TEST	TEST	\N	\N	\N
\.


--
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('usuario_id_seq', 2, true);


--
-- Name: grupomodulo grupomodulo_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY grupomodulo
    ADD CONSTRAINT grupomodulo_pkey PRIMARY KEY (id);


--
-- Name: modulo modulo_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY modulo
    ADD CONSTRAINT modulo_pkey PRIMARY KEY (id);


--
-- Name: perfil_permiso perfil_permiso_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY perfil_permiso
    ADD CONSTRAINT perfil_permiso_pkey PRIMARY KEY (idperfil, idpermiso);


--
-- Name: perfil perfil_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT perfil_pkey PRIMARY KEY (id);


--
-- Name: perfil_usuario perfil_usuario_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY perfil_usuario
    ADD CONSTRAINT perfil_usuario_pkey PRIMARY KEY (idperfil, idusuario);


--
-- Name: permiso permiso_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY permiso
    ADD CONSTRAINT permiso_pkey PRIMARY KEY (id);


--
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

