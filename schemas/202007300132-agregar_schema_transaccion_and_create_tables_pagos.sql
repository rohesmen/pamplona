
--agregar columnas para referencias de pagos con paypal
ALTER TABLE cliente.pagos
   ADD COLUMN payment_id text;

ALTER TABLE cliente.pagos
   ADD COLUMN payer_id text;

ALTER TABLE cliente.pagos
   ADD COLUMN facilitator_access_token text;

ALTER TABLE cliente.pagos
   ADD COLUMN id_captures text;
   
ALTER TABLE cliente.pagos
   ADD COLUMN payer_email text;
   
ALTER TABLE cliente.pagos
   ADD COLUMN payer_name text;

--crear schema transaccion para tablas auxiliares de pagos
CREATE SCHEMA transaccion
    AUTHORIZATION postgres;

--crear tabla pagos(auxiliar de cobros por medio de paypal)
CREATE TABLE transaccion.pagos
(
    idpago serial,
    idforma_pago integer Default 0,
    activo boolean DEFAULT true,
    fecha_pago timestamp without time zone DEFAULT now(),
    fecha_creacion timestamp without time zone DEFAULT now(),
    fecha_modificacion timestamp without time zone DEFAULT now(),
    idcliente integer NOT NULL DEFAULT 0,
	folio_interno character varying,
    folio_factura character varying(64) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    referencia_bancaria character varying(32) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    latitud double precision,
    longitud double precision,
    idmotivo integer,
    idusuario integer,
    cantidad numeric,
	payment_id text,
	payer_id text,
	facilitator_access_token text,
	id_captures text,
	payer_email text,
    payer_name text,
    CONSTRAINT pagos_pkey PRIMARY KEY (idpago),
    CONSTRAINT fk_idcliente FOREIGN KEY (idcliente)
        REFERENCES cliente.cliente (id_cliente) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

ALTER TABLE transaccion.pagos
    OWNER to postgres;
	
	
-- crear tabla historial_pago(auxiliar para cobros con paypal)
CREATE TABLE transaccion.historial_pago
(
    id serial,
    idcliente integer,
    mes text,
    anio text,
    descuento numeric,
    iva numeric,
    cantidad numeric,
    fecha_pago timestamp without time zone,
    idforma_pago integer,
    referencia_bancaria text,
	validado boolean DEFAULT false,
    idusuario integer,
    facturado boolean,
    folio_factura text,
    caja boolean,
    idbanco integer,
	idcobratario integer,
	activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone,
    envio_correo boolean,
    recibo_impreso boolean,
    incluido_corte boolean,
    fecha_corte timestamp without time zone,
    idvisita_cliente integer,
	idtipo_cobro integer NOT NULL DEFAULT 1,
    concepto_servicio character varying(256) NOT NULL DEFAULT ''::character varying,
    idtipo_servicio integer NOT NULL DEFAULT 0,
    idpago integer NOT NULL DEFAULT 0,
	CONSTRAINT pk_historial_pago PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE transaccion.historial_pago
    OWNER to postgres;
