CREATE OR REPLACE FUNCTION "cliente"."set_ultimo_mes_anio_pago"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
	DECLARE valor varchar;
	DECLARE imeses_pagar integer;
	DECLARE dmensualidad double precision;
BEGIN
 --IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
	dmensualidad = 0;
	if NEW.cobro_manual then
		dmensualidad = NEW.mensualidad;
	else
		if NEW.idcolonia is not null then
			select coalesce(monto, 0) into dmensualidad from cliente.colonia where id = NEW.idcolonia;
		end if;
		NEW.mensualidad = dmensualidad;
	end if;

	if NEW.ultimo_mes_pago is not null and NEW.ultimo_anio_pago is not null then
		if ((NEW.ultimo_mes_pago) = '1')  THEN valor = 'Enero';			end if;
		if ((NEW.ultimo_mes_pago) = '2') 	THEN valor = 'Febrero'; 		end if;
		if ((NEW.ultimo_mes_pago) = '3') 	THEN valor = 'Marzo';			end if;
		if ((NEW.ultimo_mes_pago) = '4') 	THEN valor = 'Abril'; 			end if;
		if ((NEW.ultimo_mes_pago) = '5') 	THEN valor = 'Mayo'; 			end if;
		if ((NEW.ultimo_mes_pago) = '6') 	THEN valor = 'Junio'; 			end if;
		if ((NEW.ultimo_mes_pago) = '7') 	THEN valor = 'Julio'; 			end if;
		if ((NEW.ultimo_mes_pago) = '8') 	THEN valor = 'Agosto'; 		end if;
		if ((NEW.ultimo_mes_pago) = '9') 	THEN valor = 'Septiembre'; end if;
		if ((NEW.ultimo_mes_pago) = '10') THEN valor = 'Octubre'; 		end if;
		if ((NEW.ultimo_mes_pago) = '11') THEN valor = 'Noviembre'; 	end if;
		if ((NEW.ultimo_mes_pago) = '12') THEN valor = 'Diciembre';  end if;

		--raise notice 'valor %', COALESCE(valor, 'nada');
		valor = valor || ' ' || NEW.ultimo_anio_pago::varchar;
		--raise notice 'valor %', COALESCE(valor, 'nada');
		NEW.ultimo_mes_anio_pago = valor;

		imeses_pagar = ((date_part(
				'year',
				age(
					now(),
					(NEW.ultimo_anio_pago || '-'  || lpad(NEW.ultimo_mes_pago,2,'0') || '-' || '01')::timestamp with time zone
				)
			) * 12 ) +

			date_part(
				'month',
				age(
					now(),
					(NEW.ultimo_anio_pago || '-'  || lpad(NEW.ultimo_mes_pago,2,'0') || '-' || '01')::timestamp with time zone
				)
			));

		NEW.meses_a_pagar = imeses_pagar;


		NEW.saldo = imeses_pagar * dmensualidad;

		if OLD.ultimo_mes_pago != NEW.ultimo_mes_pago or OLD.ultimo_anio_pago != NEW.ultimo_anio_pago then
			SELECT
					hp.fecha_pago into NEW.fecha_ultimo_pago
			FROM cliente.historial_pago hp
			where idcliente = NEW.id_cliente
			ORDER BY hp.fecha_creacion DESC limit 1;
		end if;
	ELSE
		NEW.ultimo_mes_anio_pago = null;
		NEW.saldo = null;
	END IF;
 --END IF;

 RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE TRIGGER "set_ultimo_mes_anio_pago" BEFORE INSERT OR UPDATE ON "cliente"."cliente"
FOR EACH ROW
EXECUTE PROCEDURE "cliente"."set_ultimo_mes_anio_pago"();