

DO $$ 
DECLARE
    id_modulo integer;
    id_permiso integer;
BEGIN

    SELECT id INTO id_modulo FROM usuario.modulo WHERE siglas = 'RUTSEG' AND nombre = 'Rutas' AND directorio = '/rutaseguimiento' AND activo = true;


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Consultar Ruta', 'Permiso para consultar ruta', 'rutaseguimiento', 'info', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Activar Ruta', 'Permiso para activar ruta', 'rutaseguimiento', 'activate', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());
    

END $$;


