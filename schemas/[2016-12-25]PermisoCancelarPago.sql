-- actualizar el recurso y la accion para aplicar descuento en caja
begin; 
	update usuario.permiso set recurso = 'caja', accion = 'descuento' 
	where id = 29;
commit;

-- Creacion del Permiso para aplicar la cancelacion de los renglones
-- del historico de cajas
BEGIN;
insert into usuario.permiso(
	id,
	nombre,
	descripcion,
	recurso,
	accion,
	activo,
	fecha_modificacion,
	fecha_creacion,
	idmodulo)
values(
	34,
	'Cancelar Pago Cliente',
	'Cancelar Pago Cliente',
	'caja',
	'cancelar',
	true,
	now(),
	now(),
	3	
);
COMMIT;

-- Relacion entre el perfil y el permiso
BEGIN;
insert into usuario.perfil_permiso(
	idperfil,
	idpermiso,
	activo,
	fecha_creacion,
	fecha_modificacion)
values(
	1,
	34,
	true,
	now(),
	now()
);
COMMIT;