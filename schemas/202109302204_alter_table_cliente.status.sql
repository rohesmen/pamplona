ALTER TABLE "cliente"."estatus_cliente"
  ADD COLUMN "estatus_cliente_supervisor" text,
  ADD COLUMN "estatus_cliente_cobratario" text,
  ADD COLUMN "alta_app" bool;

COMMENT ON COLUMN "cliente"."estatus_cliente"."estatus_cliente_supervisor" IS 'almacenara los estatus a los que podra ser cambiado por el supervisor';

COMMENT ON COLUMN "cliente"."estatus_cliente"."estatus_cliente_cobratario" IS 'almacenara los estatus a los que podra ser cambiado por el cobratario';

COMMENT ON COLUMN "cliente"."estatus_cliente"."alta_app" IS 'indica si el estatus estara disponible para setear en la aplicacion movil';

update cliente.estatus_cliente
set estatus_cliente_supervisor = 'CF,CS',
	estatus_cliente_cobratario = 'CI'
where sigla = 'CH';
update cliente.estatus_cliente
set alta_app = true
where sigla != 'CF';