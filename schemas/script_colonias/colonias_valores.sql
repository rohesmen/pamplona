-- Catalogo colonias 
-- russel
-- Sequence: usuario.modulo_idmodulo_seq
-- ALTER SEQUENCE usuario.modulo_idmodulo_seq RESTART WITH 4;
-- usuario.modulo --
INSERT INTO usuario.modulo(
    idgrupomodulo, siglas, nombre, descripcion, iconomodulo, directorio, activo, fechamodificacion, fechacreacion, orden)
    VALUES (1, 'COL', 'Colonias', 'Colonias', '', '/colonias', true, 'now', 'now', (select max(orden) + 1 from usuario.modulo) );

-- usuario.permiso --
INSERT INTO usuario.permiso(
    id, nombre, descripcion, recurso, accion, activo, fecha_modificacion, fecha_creacion, idmodulo)
    VALUES 
    (16, 'Colonias', 'Colonias', 'colonias', 'index', true, 'now', 'now', 5),
    (17, 'Busqudas', 'Busqudas', 'colonias', 'search', true, 'now', 'now', 5),
    (18, 'Agregar colonias', 'Agregar colonias', 'colonias', 'add', true, 'now', 'now', 5),
    (19, 'Editar colonias', 'Editar colonias', 'colonias', 'edit', true, 'now', 'now', 5),
    ( (select(max(id) + 1) from usuario.permiso), 'Cancelar colonias', 'Cancelar colonias', 'colonias', 'cancelar', true, 'now', 'now', 5),
    ( (select(max(id) + 1) from usuario.permiso), 'Consultar colonias', 'Consultar colonias', 'colonias', 'info', true, 'now', 'now', 5),
    ( (select(max(id) + 1) from usuario.permiso), 'Eliminar colonias', 'Eliminar colonias', 'colonias', 'delete', true, 'now', 'now', 5);

-- usuario.perfil_permiso --
INSERT INTO usuario.perfil_permiso(
    idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
    VALUES (1, 16, true, 'now', 'now'),
    (1, 17, true, 'now', 'now'),
    (1, 18, true, 'now', 'now'),
    (1, 19, true, 'now', 'now'),
    (1, (select(max(id)) from usuario.permiso), true, 'now', 'now'),
    (1, (select(max(id)) from usuario.permiso), true, 'now', 'now'),
    (1, (select(max(id)) from usuario.permiso), true, 'now', 'now');
