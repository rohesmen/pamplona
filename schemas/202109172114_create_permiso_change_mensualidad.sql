with ch as (
	INSERT INTO "usuario"."permiso"("nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo") VALUES (
	'Cambiar mensualidad casa habitacion', 'Cambiar mensualidad casa habitacion', 'clientes', 'chgmench', 't', now(), now(),
		(select id from usuario.modulo where siglas = 'CLI' limit 1)
	) RETURNING id
),
pch as (
	insert into usuario.perfil_permiso (idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
	select 1, id, true, now(), now() from ch
),
cs as (
	INSERT INTO "usuario"."permiso"("nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo") VALUES (
	'Cambiar mensualidad comercio supervisor', 'Cambiar mensualidad comercio supervisor', 'clientes', 'chgmencs', 't', now(), now(),
		(select id from usuario.modulo where siglas = 'CLI' limit 1)
	) RETURNING id
),
pcs as (
	insert into usuario.perfil_permiso (idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
	select 1, id, true, now(), now() from cs
),
cf as (
	INSERT INTO "usuario"."permiso"("nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo") VALUES (
	'Cambiar mensualidad comercio facturado', 'Cambiar mensualidad comercio facturado', 'clientes', 'chgmencf', 't', now(), now(),
		(select id from usuario.modulo where siglas = 'CLI' limit 1)
	) RETURNING id
),
pcf as (
	insert into usuario.perfil_permiso (idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
	select 1, id, true, now(), now() from cf
)
,
ci as (
	INSERT INTO "usuario"."permiso"("nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo") VALUES (
	'Cambiar mensualidad comercio informal', 'Cambiar mensualidad comercio informal', 'clientes', 'chgmenci', 't', now(), now(),
		(select id from usuario.modulo where siglas = 'CLI' limit 1)
	) RETURNING id
),
pci as (
	insert into usuario.perfil_permiso (idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
	select 1, id, true, now(), now() from ci
)
select * from ch, cs,cf, ci