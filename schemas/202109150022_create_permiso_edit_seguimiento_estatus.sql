with pedites as (
	INSERT INTO "usuario"."permiso"("nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo") VALUES (
	'Cambiar estatus cliente', 'Cambiar estatus cliente', 'clientes', 'editsta', 't', now(), now(),
		(select id from usuario.modulo where siglas = 'CLI' limit 1)
	) RETURNING id
),
peredites as (
	insert into usuario.perfil_permiso (idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
	select 1, id, true, now(), now() from pedites
),
pinfseg as (
	INSERT INTO "usuario"."permiso"("nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo") VALUES (
	'Seguimiento estatus cliente', 'Seguimiento estatus cliente', 'clientes', 'infosegsta', 't', now(), now(),
		(select id from usuario.modulo where siglas = 'CLI' limit 1)
	) RETURNING id
),
perdel as (
	insert into usuario.perfil_permiso (idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
	select 1, id, true, now(), now() from pinfseg
)
select f.id, g.id from pedites f, pinfseg g