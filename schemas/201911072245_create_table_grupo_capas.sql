/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 100010
 Source Host           : localhost:5432
 Source Catalog        : sana
 Source Schema         : comun

 Target Server Type    : PostgreSQL
 Target Server Version : 100010
 File Encoding         : 65001

 Date: 07/11/2019 22:43:32
*/


-- ----------------------------
-- Table structure for grupo_capas
-- ----------------------------
DROP TABLE IF EXISTS "comun"."grupo_capas";
CREATE TABLE "comun"."grupo_capas" (
  "id" serial,
  "nombre" text COLLATE "pg_catalog"."default",
  "descripcion" text COLLATE "pg_catalog"."default",
  "orden" int4,
  "activo" bool DEFAULT true,
  "fecha_creacion" timestamp(6) DEFAULT now(),
  "fecha_modificacion" timestamp(6) DEFAULT now()
)
;

-- ----------------------------
-- Primary Key structure for table grupo_capas
-- ----------------------------
ALTER TABLE "comun"."grupo_capas" ADD CONSTRAINT "pku_grupo_capas" PRIMARY KEY ("id");
