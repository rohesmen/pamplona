ALTER TABLE "cliente"."pagos"
  ADD COLUMN "isnegociacion" bool default false,
  ADD COLUMN descuento_negociacion decimal default 0,
  ADD COLUMN total_pago decimal default 0;