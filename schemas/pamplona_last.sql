--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.0
-- Dumped by pg_dump version 9.6.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: cliente; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA cliente;


ALTER SCHEMA cliente OWNER TO postgres;

--
-- Name: comun; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA comun;


ALTER SCHEMA comun OWNER TO postgres;

--
-- Name: usuario; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA usuario;


ALTER SCHEMA usuario OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = cliente, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: banco; Type: TABLE; Schema: cliente; Owner: postgres
--

CREATE TABLE banco (
    id integer NOT NULL,
    nombre text,
    descripcion text,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE banco OWNER TO postgres;

--
-- Name: cliente; Type: TABLE; Schema: cliente; Owner: postgres
--

CREATE TABLE cliente (
    id_cliente integer NOT NULL,
    calle integer,
    calle_letra text,
    tipo_calle text,
    numero integer,
    numero_letra text,
    tipo_numero text,
    cruza1 character varying(50),
    letra_cruza1 character varying(50),
    tipo_cruza1 character varying(50),
    cruza2 character varying(50),
    letra_cruza2 character varying(50),
    tipo_cruza2 character varying(50),
    idcolonia integer,
    localidad character varying(150),
    apepat text,
    apemat text,
    nombres text,
    apepat_propietario text,
    apemat_propietario text,
    nombres_propietario text,
    folio_catastral integer,
    idestatuscliente integer,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone,
    latitud double precision,
    longitud double precision,
    ubicado boolean,
    idtarifa_colonia integer,
    idruta integer,
    idtipo_contrato integer,
    referencia_ubicacion text,
    fisica boolean,
    nombre_moral character varying(150),
    rfc character varying(100),
    direccion_moral character varying(250),
    telefono character varying(10),
    correo character varying(50),
    idtipo_descuento integer,
    descuento numeric,
    mensualidad integer,
    ultimo_anio_pago integer,
    ultimo_mes_pago character varying(50),
    observacion text
);


ALTER TABLE cliente OWNER TO postgres;

--
-- Name: cliente_id_cliente_seq; Type: SEQUENCE; Schema: cliente; Owner: postgres
--

CREATE SEQUENCE cliente_id_cliente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cliente_id_cliente_seq OWNER TO postgres;

--
-- Name: cliente_id_cliente_seq; Type: SEQUENCE OWNED BY; Schema: cliente; Owner: postgres
--

ALTER SEQUENCE cliente_id_cliente_seq OWNED BY cliente.id_cliente;


--
-- Name: cobratario; Type: TABLE; Schema: cliente; Owner: postgres
--

CREATE TABLE cobratario (
    id integer NOT NULL,
    idusuario integer,
    idzona integer,
    en_sitio boolean,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE cobratario OWNER TO postgres;

--
-- Name: colonia; Type: TABLE; Schema: cliente; Owner: postgres
--

CREATE TABLE colonia (
    id integer NOT NULL,
    nombre text,
    alias text,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE colonia OWNER TO postgres;

--
-- Name: estatus_cliente; Type: TABLE; Schema: cliente; Owner: postgres
--

CREATE TABLE estatus_cliente (
    id integer NOT NULL,
    nombre text,
    descripcion text,
    activo boolean,
    fechacreacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE estatus_cliente OWNER TO postgres;

--
-- Name: forma_pago; Type: TABLE; Schema: cliente; Owner: postgres
--

CREATE TABLE forma_pago (
    id integer NOT NULL,
    nombre text,
    descripcion text,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE forma_pago OWNER TO postgres;

--
-- Name: historial_pago; Type: TABLE; Schema: cliente; Owner: postgres
--

CREATE TABLE historial_pago (
    id integer NOT NULL,
    idcliente integer,
    mes text,
    anio text,
    descuento numeric,
    iva numeric,
    cantidad numeric,
    fecha_pago timestamp without time zone,
    idforma_pago integer,
    referencia_bancaria text,
    validado boolean,
    idusuario integer,
    facturado boolean,
    folio_factura text,
    idcobratario integer,
    caja boolean,
    idbanco integer,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone,
    envio_correo boolean,
    recibo_impreso boolean,
    incluido_corte boolean,
    fecha_corte timestamp without time zone,
    idvisita_cliente integer
);


ALTER TABLE historial_pago OWNER TO postgres;

--
-- Name: ruta; Type: TABLE; Schema: cliente; Owner: postgres
--

CREATE TABLE ruta (
    id integer NOT NULL,
    nombre text,
    idcolonia integer,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificaccion timestamp without time zone
);


ALTER TABLE ruta OWNER TO postgres;

--
-- Name: ruta_horario; Type: TABLE; Schema: cliente; Owner: postgres
--

CREATE TABLE ruta_horario (
    id integer NOT NULL,
    idruta integer,
    turno text,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone,
    vehiculo text,
    chofer text
);


ALTER TABLE ruta_horario OWNER TO postgres;

--
-- Name: tarifa_colonia; Type: TABLE; Schema: cliente; Owner: postgres
--

CREATE TABLE tarifa_colonia (
    id integer NOT NULL,
    idcolonia integer,
    tarifa numeric,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE tarifa_colonia OWNER TO postgres;

--
-- Name: tipo_contrato; Type: TABLE; Schema: cliente; Owner: postgres
--

CREATE TABLE tipo_contrato (
    id integer NOT NULL,
    nombre text,
    descripcion text,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE tipo_contrato OWNER TO postgres;

--
-- Name: tipo_descuento; Type: TABLE; Schema: cliente; Owner: postgres
--

CREATE TABLE tipo_descuento (
    id integer NOT NULL,
    nombre text,
    descripcion text,
    porcentaje numeric,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE tipo_descuento OWNER TO postgres;

--
-- Name: visita_cliente; Type: TABLE; Schema: cliente; Owner: postgres
--

CREATE TABLE visita_cliente (
    id integer NOT NULL,
    idcobratario integer,
    fecha_visita timestamp without time zone,
    idcliente integer,
    por_qr boolean,
    por_coordenadas boolean,
    latitud double precision,
    longitud boolean,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone,
    cobro_realizado boolean,
    motivo_no_cobro text
);


ALTER TABLE visita_cliente OWNER TO postgres;

--
-- Name: zona_colonia; Type: TABLE; Schema: cliente; Owner: postgres
--

CREATE TABLE zona_colonia (
    id integer NOT NULL,
    idcolonia integer,
    nombre text,
    descripcion text,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE zona_colonia OWNER TO postgres;

SET search_path = comun, pg_catalog;

--
-- Name: parametro; Type: TABLE; Schema: comun; Owner: postgres
--

CREATE TABLE parametro (
    id integer NOT NULL,
    clave character varying(50),
    valor text,
    tipodato character varying(50),
    descripcion character varying(250),
    activo boolean,
    fechacreacion timestamp(0) without time zone,
    fechamodificacion timestamp(0) without time zone
);


ALTER TABLE parametro OWNER TO postgres;

SET search_path = usuario, pg_catalog;

--
-- Name: aplicacion; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE aplicacion (
    id integer NOT NULL,
    nombre text,
    descripcion text,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE aplicacion OWNER TO postgres;

--
-- Name: aplicacion_usuario; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE aplicacion_usuario (
    idaplicacion integer NOT NULL,
    idusuario integer NOT NULL,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE aplicacion_usuario OWNER TO postgres;

--
-- Name: grupomodulo; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE grupomodulo (
    id integer NOT NULL,
    siglas character varying(50) NOT NULL,
    nombre character varying(100) NOT NULL,
    descripcion character varying(250),
    orden integer NOT NULL,
    activo boolean DEFAULT true NOT NULL,
    fechacreacion timestamp(0) without time zone DEFAULT now(),
    fechamodificacion timestamp(0) without time zone DEFAULT now(),
    icono text NOT NULL,
    ruta text,
    panelclass character varying DEFAULT 'panel-primary'::character varying
);


ALTER TABLE grupomodulo OWNER TO postgres;

--
-- Name: TABLE grupomodulo; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON TABLE grupomodulo IS 'Catálogo en el que se relaciona a los grupos de modulo en los que se puede clasificar o dividir un sistema.';


--
-- Name: grupomodulo_idgrupomodulo_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE grupomodulo_idgrupomodulo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE grupomodulo_idgrupomodulo_seq OWNER TO postgres;

--
-- Name: grupomodulo_idgrupomodulo_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: postgres
--

ALTER SEQUENCE grupomodulo_idgrupomodulo_seq OWNED BY grupomodulo.id;


--
-- Name: modulo; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE modulo (
    id integer NOT NULL,
    idgrupomodulo integer NOT NULL,
    siglas character varying(50) NOT NULL,
    nombre character varying(150) NOT NULL,
    descripcion character varying(250),
    iconomodulo text,
    directorio text NOT NULL,
    activo boolean DEFAULT true NOT NULL,
    fechamodificacion timestamp(0) without time zone DEFAULT now() NOT NULL,
    fechacreacion timestamp(0) without time zone DEFAULT now() NOT NULL,
    orden integer DEFAULT 0 NOT NULL
);


ALTER TABLE modulo OWNER TO postgres;

--
-- Name: TABLE modulo; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON TABLE modulo IS 'Catálogo en el que se relaciona a los modulos en los que se puede clasificar o dividir un sistema.';


--
-- Name: modulo_idmodulo_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE modulo_idmodulo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE modulo_idmodulo_seq OWNER TO postgres;

--
-- Name: modulo_idmodulo_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: postgres
--

ALTER SEQUENCE modulo_idmodulo_seq OWNED BY modulo.id;


--
-- Name: perfil; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE perfil (
    id integer NOT NULL,
    nombre text,
    descripcion text,
    activo boolean DEFAULT true,
    fecha_creacion timestamp without time zone DEFAULT now(),
    fecha_modificacion timestamp without time zone DEFAULT now()
);


ALTER TABLE perfil OWNER TO postgres;

--
-- Name: perfil_indicador; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE perfil_indicador (
    idperfil integer NOT NULL,
    idindicador integer NOT NULL,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE perfil_indicador OWNER TO postgres;

--
-- Name: perfil_permiso; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE perfil_permiso (
    idperfil integer NOT NULL,
    idpermiso integer NOT NULL,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE perfil_permiso OWNER TO postgres;

--
-- Name: perfil_usuario; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE perfil_usuario (
    idperfil integer NOT NULL,
    idusuario integer NOT NULL,
    activo boolean,
    fecha_creacion timestamp without time zone,
    fecha_modificacion timestamp without time zone
);


ALTER TABLE perfil_usuario OWNER TO postgres;

--
-- Name: permiso; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE permiso (
    id integer NOT NULL,
    nombre text,
    descripcion text,
    recurso text,
    accion text,
    activo boolean DEFAULT true,
    fecha_modificacion timestamp without time zone DEFAULT now(),
    fecha_creacion timestamp without time zone DEFAULT now(),
    idmodulo integer
);


ALTER TABLE permiso OWNER TO postgres;

--
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario_id_seq OWNER TO postgres;

--
-- Name: usuario; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario (
    id integer DEFAULT nextval('usuario_id_seq'::regclass) NOT NULL,
    usuario text,
    clave character varying(64),
    correo text,
    activo boolean,
    fecha_modificacion timestamp without time zone,
    fecha_creacion timestamp without time zone,
    idestructura integer,
    general boolean DEFAULT true NOT NULL,
    nombre text,
    apellido_paterno text,
    apellido_materno text,
    idterritorio json,
    tipo_territorio text,
    telefono text
);


ALTER TABLE usuario OWNER TO postgres;

SET search_path = cliente, pg_catalog;

--
-- Name: cliente id_cliente; Type: DEFAULT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY cliente ALTER COLUMN id_cliente SET DEFAULT nextval('cliente_id_cliente_seq'::regclass);


SET search_path = usuario, pg_catalog;

--
-- Name: grupomodulo id; Type: DEFAULT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY grupomodulo ALTER COLUMN id SET DEFAULT nextval('grupomodulo_idgrupomodulo_seq'::regclass);


--
-- Name: modulo id; Type: DEFAULT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY modulo ALTER COLUMN id SET DEFAULT nextval('modulo_idmodulo_seq'::regclass);


SET search_path = cliente, pg_catalog;

--
-- Data for Name: banco; Type: TABLE DATA; Schema: cliente; Owner: postgres
--

COPY banco (id, nombre, descripcion, activo, fecha_creacion, fecha_modificacion) FROM stdin;
\.


--
-- Data for Name: cliente; Type: TABLE DATA; Schema: cliente; Owner: postgres
--

COPY cliente (id_cliente, calle, calle_letra, tipo_calle, numero, numero_letra, tipo_numero, cruza1, letra_cruza1, tipo_cruza1, cruza2, letra_cruza2, tipo_cruza2, idcolonia, localidad, apepat, apemat, nombres, apepat_propietario, apemat_propietario, nombres_propietario, folio_catastral, idestatuscliente, activo, fecha_creacion, fecha_modificacion, latitud, longitud, ubicado, idtarifa_colonia, idruta, idtipo_contrato, referencia_ubicacion, fisica, nombre_moral, rfc, direccion_moral, telefono, correo, idtipo_descuento, descuento, mensualidad, ultimo_anio_pago, ultimo_mes_pago, observacion) FROM stdin;
\.


--
-- Name: cliente_id_cliente_seq; Type: SEQUENCE SET; Schema: cliente; Owner: postgres
--

SELECT pg_catalog.setval('cliente_id_cliente_seq', 1, false);


--
-- Data for Name: cobratario; Type: TABLE DATA; Schema: cliente; Owner: postgres
--

COPY cobratario (id, idusuario, idzona, en_sitio, activo, fecha_creacion, fecha_modificacion) FROM stdin;
\.


--
-- Data for Name: colonia; Type: TABLE DATA; Schema: cliente; Owner: postgres
--

COPY colonia (id, nombre, alias, activo, fecha_creacion, fecha_modificacion) FROM stdin;
\.


--
-- Data for Name: estatus_cliente; Type: TABLE DATA; Schema: cliente; Owner: postgres
--

COPY estatus_cliente (id, nombre, descripcion, activo, fechacreacion, fecha_modificacion) FROM stdin;
\.


--
-- Data for Name: forma_pago; Type: TABLE DATA; Schema: cliente; Owner: postgres
--

COPY forma_pago (id, nombre, descripcion, activo, fecha_creacion, fecha_modificacion) FROM stdin;
\.


--
-- Data for Name: historial_pago; Type: TABLE DATA; Schema: cliente; Owner: postgres
--

COPY historial_pago (id, idcliente, mes, anio, descuento, iva, cantidad, fecha_pago, idforma_pago, referencia_bancaria, validado, idusuario, facturado, folio_factura, idcobratario, caja, idbanco, activo, fecha_creacion, fecha_modificacion, envio_correo, recibo_impreso, incluido_corte, fecha_corte, idvisita_cliente) FROM stdin;
\.


--
-- Data for Name: ruta; Type: TABLE DATA; Schema: cliente; Owner: postgres
--

COPY ruta (id, nombre, idcolonia, activo, fecha_creacion, fecha_modificaccion) FROM stdin;
\.


--
-- Data for Name: ruta_horario; Type: TABLE DATA; Schema: cliente; Owner: postgres
--

COPY ruta_horario (id, idruta, turno, activo, fecha_creacion, fecha_modificacion, vehiculo, chofer) FROM stdin;
\.


--
-- Data for Name: tarifa_colonia; Type: TABLE DATA; Schema: cliente; Owner: postgres
--

COPY tarifa_colonia (id, idcolonia, tarifa, activo, fecha_creacion, fecha_modificacion) FROM stdin;
\.


--
-- Data for Name: tipo_contrato; Type: TABLE DATA; Schema: cliente; Owner: postgres
--

COPY tipo_contrato (id, nombre, descripcion, activo, fecha_creacion, fecha_modificacion) FROM stdin;
\.


--
-- Data for Name: tipo_descuento; Type: TABLE DATA; Schema: cliente; Owner: postgres
--

COPY tipo_descuento (id, nombre, descripcion, porcentaje, activo, fecha_creacion, fecha_modificacion) FROM stdin;
\.


--
-- Data for Name: visita_cliente; Type: TABLE DATA; Schema: cliente; Owner: postgres
--

COPY visita_cliente (id, idcobratario, fecha_visita, idcliente, por_qr, por_coordenadas, latitud, longitud, activo, fecha_creacion, fecha_modificacion, cobro_realizado, motivo_no_cobro) FROM stdin;
\.


--
-- Data for Name: zona_colonia; Type: TABLE DATA; Schema: cliente; Owner: postgres
--

COPY zona_colonia (id, idcolonia, nombre, descripcion, activo, fecha_creacion, fecha_modificacion) FROM stdin;
\.


SET search_path = comun, pg_catalog;

--
-- Data for Name: parametro; Type: TABLE DATA; Schema: comun; Owner: postgres
--

COPY parametro (id, clave, valor, tipodato, descripcion, activo, fechacreacion, fechamodificacion) FROM stdin;
\.


SET search_path = usuario, pg_catalog;

--
-- Data for Name: aplicacion; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY aplicacion (id, nombre, descripcion, activo, fecha_creacion, fecha_modificacion) FROM stdin;
\.


--
-- Data for Name: aplicacion_usuario; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY aplicacion_usuario (idaplicacion, idusuario, activo, fecha_creacion, fecha_modificacion) FROM stdin;
\.


--
-- Data for Name: grupomodulo; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY grupomodulo (id, siglas, nombre, descripcion, orden, activo, fechacreacion, fechamodificacion, icono, ruta, panelclass) FROM stdin;
1	USR	Administrativo	USUARIOS	1	t	2016-10-12 20:21:42	2016-10-12 20:21:42	fa-user	/users	panel-primary
2	MODCLI	Clientes	Módulo de Clientes	2	t	2016-11-02 22:14:16	2016-11-02 22:14:16	fa-users	/clientes	panel-primary
\.


--
-- Name: grupomodulo_idgrupomodulo_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('grupomodulo_idgrupomodulo_seq', 1, false);


--
-- Data for Name: modulo; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY modulo (id, idgrupomodulo, siglas, nombre, descripcion, iconomodulo, directorio, activo, fechamodificacion, fechacreacion, orden) FROM stdin;
1	1	USR	USUARIOS	USUARIOS	fa-user	/users	t	2016-10-12 20:22:59	2016-10-12 20:22:59	1
2	2	CLI	Clientes	Clientes	fa-users	/clientes	t	2016-11-02 22:15:25	2016-11-02 22:15:25	2
\.


--
-- Name: modulo_idmodulo_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('modulo_idmodulo_seq', 2, true);


--
-- Data for Name: perfil; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY perfil (id, nombre, descripcion, activo, fecha_creacion, fecha_modificacion) FROM stdin;
1	administrador	administrador	t	2016-10-12 20:13:41.974336	2016-10-12 20:13:41.974336
\.


--
-- Data for Name: perfil_indicador; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY perfil_indicador (idperfil, idindicador, activo, fecha_creacion, fecha_modificacion) FROM stdin;
\.


--
-- Data for Name: perfil_permiso; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY perfil_permiso (idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion) FROM stdin;
1	1	t	2016-10-12 20:24:31.650621	2016-10-12 20:24:40.403093
1	2	t	2016-10-12 21:00:13.380486	2016-10-12 21:00:13.380486
1	3	t	2016-10-12 21:01:06.352622	2016-10-12 21:01:06.352622
1	4	t	2016-10-12 21:05:02.132355	2016-10-12 21:05:02.132355
1	5	t	2016-10-12 21:08:54.839453	2016-10-12 21:08:54.839453
1	6	t	2016-10-12 21:13:50.833608	2016-10-12 21:13:50.833608
1	9	t	2016-11-02 22:18:22.993159	2016-11-02 22:18:22.993159
1	7	t	2016-11-02 22:18:22.993159	2016-11-02 22:18:22.993159
1	8	t	2016-11-02 22:18:22.993159	2016-11-02 22:18:22.993159
1	10	t	2016-11-02 22:18:22.993159	2016-11-02 22:18:22.993159
\.


--
-- Data for Name: perfil_usuario; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY perfil_usuario (idperfil, idusuario, activo, fecha_creacion, fecha_modificacion) FROM stdin;
1	1	t	2016-10-12 20:56:20.896899	2016-10-12 20:56:20.896899
1	2	t	2016-10-13 02:15:10	2016-10-13 02:15:10
\.


--
-- Data for Name: permiso; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY permiso (id, nombre, descripcion, recurso, accion, activo, fecha_modificacion, fecha_creacion, idmodulo) FROM stdin;
1	Usuario	Usuarios	users	index	t	2016-10-12 20:18:16.652923	2016-10-12 20:18:16.652923	1
2	Busquedas	busquedas	users	search	t	2016-10-12 20:59:22.904334	2016-10-12 20:59:22.904334	1
3	Agregar usuarios	Agregar usuario	users	add	t	2016-10-12 21:01:46.631078	2016-10-12 21:01:46.631078	1
4	Editar	Editar	users	edit	t	2016-10-12 21:05:42.402932	2016-10-12 21:05:42.402932	1
5	Agregar perfiles	Agregar perfiles	users	saveProfiles	t	2016-10-12 21:08:28.596796	2016-10-12 21:08:28.596796	1
6	Editar perfiles	Editar Perfiles	users	editProfiles	t	2016-10-12 21:14:43.704039	2016-10-12 21:14:43.704039	1
7	Cliente	Clientes	clientes	index	t	2016-11-02 22:17:18.596742	2016-11-02 22:17:18.596742	2
8	Busqudas	Busqudas	clientes	search	t	2016-11-02 22:17:53.107255	2016-11-02 22:17:53.107255	2
9	Agregar clientes	Agregar clientes	clientes	add	t	2016-11-02 22:18:07.916482	2016-11-02 22:18:07.916482	2
10	Editar clientes	Editar clientes	clientes	edit	t	2016-11-02 22:18:17.040448	2016-11-02 22:18:17.040448	2
\.


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario (id, usuario, clave, correo, activo, fecha_modificacion, fecha_creacion, idestructura, general, nombre, apellido_paterno, apellido_materno, idterritorio, tipo_territorio, telefono) FROM stdin;
1	admin	$2a$08$Wv25Q1NEjnuJL0GNnX0M5OSBaTCTD/szsU6CNzSlKEJfIU.MjBtb.	\N	t	2016-10-12 20:47:00.725523	2016-10-12 20:47:00.725523	\N	t	admin	admin	admin	\N	\N	\N
2	test	$2y$08$dEY2dkdsVWFXWEpzU0hZTerCYDMriz.XQr6fMkpCPd.VOw8Qljepq	\N	t	2016-10-13 02:03:40	2016-10-13 02:03:40	\N	t	TEST	TEST	TEST	\N	\N	\N
\.


--
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('usuario_id_seq', 2, true);


SET search_path = cliente, pg_catalog;

--
-- Name: banco pku_banco; Type: CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY banco
    ADD CONSTRAINT pku_banco PRIMARY KEY (id);


--
-- Name: cliente pku_cliente; Type: CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY cliente
    ADD CONSTRAINT pku_cliente PRIMARY KEY (id_cliente);


--
-- Name: colonia pku_colonia; Type: CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY colonia
    ADD CONSTRAINT pku_colonia PRIMARY KEY (id);


--
-- Name: estatus_cliente pku_estatuscliente; Type: CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY estatus_cliente
    ADD CONSTRAINT pku_estatuscliente PRIMARY KEY (id);


--
-- Name: forma_pago pku_forma_pago; Type: CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY forma_pago
    ADD CONSTRAINT pku_forma_pago PRIMARY KEY (id);


--
-- Name: historial_pago pku_historial; Type: CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY historial_pago
    ADD CONSTRAINT pku_historial PRIMARY KEY (id);


--
-- Name: ruta pku_ruta; Type: CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY ruta
    ADD CONSTRAINT pku_ruta PRIMARY KEY (id);


--
-- Name: ruta_horario pku_rutahorario; Type: CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY ruta_horario
    ADD CONSTRAINT pku_rutahorario PRIMARY KEY (id);


--
-- Name: tarifa_colonia pku_tarifa_colonia; Type: CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY tarifa_colonia
    ADD CONSTRAINT pku_tarifa_colonia PRIMARY KEY (id);


--
-- Name: tipo_contrato pku_tipo_contrato; Type: CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY tipo_contrato
    ADD CONSTRAINT pku_tipo_contrato PRIMARY KEY (id);


--
-- Name: tipo_descuento pku_tipo_descuento; Type: CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY tipo_descuento
    ADD CONSTRAINT pku_tipo_descuento PRIMARY KEY (id);


--
-- Name: visita_cliente pku_visita_cliente; Type: CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY visita_cliente
    ADD CONSTRAINT pku_visita_cliente PRIMARY KEY (id);


--
-- Name: cobratario pku_zona; Type: CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY cobratario
    ADD CONSTRAINT pku_zona PRIMARY KEY (id);


--
-- Name: zona_colonia pku_zonacolonia; Type: CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY zona_colonia
    ADD CONSTRAINT pku_zonacolonia PRIMARY KEY (id);


SET search_path = comun, pg_catalog;

--
-- Name: parametro pku_parametro; Type: CONSTRAINT; Schema: comun; Owner: postgres
--

ALTER TABLE ONLY parametro
    ADD CONSTRAINT pku_parametro PRIMARY KEY (id);


--
-- Name: parametro uk_parametro; Type: CONSTRAINT; Schema: comun; Owner: postgres
--

ALTER TABLE ONLY parametro
    ADD CONSTRAINT uk_parametro UNIQUE (clave);


SET search_path = usuario, pg_catalog;

--
-- Name: grupomodulo grupomodulo_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY grupomodulo
    ADD CONSTRAINT grupomodulo_pkey PRIMARY KEY (id);


--
-- Name: modulo modulo_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY modulo
    ADD CONSTRAINT modulo_pkey PRIMARY KEY (id);


--
-- Name: perfil_permiso perfil_permiso_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY perfil_permiso
    ADD CONSTRAINT perfil_permiso_pkey PRIMARY KEY (idperfil, idpermiso);


--
-- Name: perfil perfil_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT perfil_pkey PRIMARY KEY (id);


--
-- Name: perfil_usuario perfil_usuario_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY perfil_usuario
    ADD CONSTRAINT perfil_usuario_pkey PRIMARY KEY (idperfil, idusuario);


--
-- Name: permiso permiso_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY permiso
    ADD CONSTRAINT permiso_pkey PRIMARY KEY (id);


--
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


SET search_path = cliente, pg_catalog;

--
-- Name: cliente fk_cliente_colonia; Type: FK CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY cliente
    ADD CONSTRAINT fk_cliente_colonia FOREIGN KEY (idcolonia) REFERENCES colonia(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cliente fk_cliente_estatus_cliente; Type: FK CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY cliente
    ADD CONSTRAINT fk_cliente_estatus_cliente FOREIGN KEY (idestatuscliente) REFERENCES estatus_cliente(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cliente fk_cliente_ruta; Type: FK CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY cliente
    ADD CONSTRAINT fk_cliente_ruta FOREIGN KEY (idruta) REFERENCES ruta(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cliente fk_cliente_tarifa_colonia; Type: FK CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY cliente
    ADD CONSTRAINT fk_cliente_tarifa_colonia FOREIGN KEY (idtarifa_colonia) REFERENCES tarifa_colonia(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cliente fk_cliente_tipo_contrato; Type: FK CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY cliente
    ADD CONSTRAINT fk_cliente_tipo_contrato FOREIGN KEY (idtipo_contrato) REFERENCES tipo_contrato(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cliente fk_cliente_tipo_descuento; Type: FK CONSTRAINT; Schema: cliente; Owner: postgres
--

ALTER TABLE ONLY cliente
    ADD CONSTRAINT fk_cliente_tipo_descuento FOREIGN KEY (idtipo_descuento) REFERENCES tipo_descuento(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--

