

DO $$ 
DECLARE
    id_modulo integer;
    id_permiso integer;
BEGIN

    SELECT id INTO id_modulo FROM usuario.modulo WHERE siglas = 'RUTSEG' AND nombre = 'Rutas' AND directorio = '/rutaseguimiento' AND activo = true;


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Eliminar Ruta Recoleccion', 'Permiso para eliminar ruta de recoleccion', 'rutaseguimiento', 'deleterutrec', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Agregar Ruta Recoleccion', 'Permiso para ageragr ruta de recoleccion', 'rutaseguimiento', 'addrutrec', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());
    

END $$;

