﻿CREATE TABLE comun.tipo_documento
(
  id serial NOT NULL,
  tipo_documento text,
  lcomprobante boolean DEFAULT true,
  activo boolean DEFAULT true,
  fecha_creacion timestamp without time zone DEFAULT now(),
  fecha_modificacion timestamp(0) without time zone DEFAULT now(),
  CONSTRAINT pku_tipo_documento PRIMARY KEY (id)
)