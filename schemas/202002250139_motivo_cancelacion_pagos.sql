ALTER TABLE "cliente"."pagos"
  ADD COLUMN "idusuario_cancelo" int4,
  ADD COLUMN "fecha_cancelacion" timestamp(0),
  ADD COLUMN "motivo_cancelacion" text;