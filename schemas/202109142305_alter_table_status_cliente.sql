ALTER TABLE "cliente"."estatus_cliente" RENAME COLUMN "fechacreacion" TO "fecha_creacion";
ALTER TABLE "cliente"."estatus_cliente" 
  ADD COLUMN "orden" int4,
  ADD COLUMN "sigla" varchar(255),
  ADD COLUMN "disable_edit_app" bool DEFAULT false,
  ADD COLUMN "disable_edit_web" bool DEFAULT false,
  ALTER COLUMN "fecha_creacion" SET DEFAULT now(),
  ALTER COLUMN "activo" SET DEFAULT true,
  ALTER COLUMN "fecha_modificacion" SET DEFAULT now();

truncate cliente.estatus_cliente RESTART IDENTITY;
CREATE SEQUENCE "cliente"."estatus_cliente_id_seq";
ALTER TABLE "cliente"."estatus_cliente" 
  ALTER COLUMN "id" SET DEFAULT nextval('"cliente".estatus_cliente_id_seq'::regclass);
insert into cliente.estatus_cliente (nombre, descripcion, orden, sigla, disable_edit_app) values
('Casa habitación', 'Casa habitación', 1, 'CH', false),
('Comercio supervisor', 'Comercio supervisor', 2, 'CS', false),
('Comercio facturado', 'Comercio habitación', 3, 'CF', true),
('Comercio informal', 'Comercio informal', 4, 'CI', false);

ALTER TABLE "cliente"."cliente" DISABLE TRIGGER "set_ultimo_mes_anio_pago";

update cliente.cliente
set idestatuscliente = 1 
where comercio = false;
update cliente.cliente
set idestatuscliente = 2 
where comercio = true;

ALTER TABLE cliente.cliente
ADD CONSTRAINT fk_cliente_estatus_cliente
FOREIGN KEY (idestatuscliente) 
REFERENCES cliente.estatus_cliente (id);

ALTER TABLE "cliente"."cliente" ENABLE TRIGGER "set_ultimo_mes_anio_pago";