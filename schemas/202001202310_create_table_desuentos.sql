CREATE TABLE "cliente"."cliente_descuento" (
  "id" serial,
  "idcliente" int4 NOT NULL,
  "idusuario" int4 NOT NULL,
	idmotivo_descuento integer,
	cantidad double precision default 0,
	meses_descuento integer default 0,
  "activo" bool default true,
  "fecha_creacion" timestamp(6) default now(),
  "fecha_modificacion" timestamp(6) default now(),
  CONSTRAINT "pku_cliente_descuento" PRIMARY KEY ("id")
);

CREATE TABLE "cliente"."cliente_descuento_detalle" (
  "id" serial,
  "idcliente_descuento" int4 NOT NULL,
	"idcliente" int4 NOT NULL,
	"idusuario" int4 NOT NULL,
  mes integer,
	anio integer,
	cantidad double precision default 0,
 "activo" bool default true,
  "fecha_creacion" timestamp(6) default now(),
  "fecha_modificacion" timestamp(6) default now(),
  CONSTRAINT "pku_cliente_descuento_detalle" PRIMARY KEY ("id")
);