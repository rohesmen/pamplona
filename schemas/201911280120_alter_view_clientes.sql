create or replace view geodata.clientes_view as
SELECT 
	c.id_cliente,
	c.id_cliente AS no_cliente,
	((((COALESCE(('C. '::text || c.calle), ''::text) || COALESCE(c.calle_letra, ''::text)) || COALESCE((' #'::text || c.numero), ''::text)) || COALESCE(c.numero_letra, ''::text)) || COALESCE((' COl. '::text || col.nombre), ''::text)) AS descripcion,
	((('01/'::text || lpad((c.ultimo_mes_pago)::text, 2, '0'::text)) || '/'::text) || c.ultimo_anio_pago) AS ultimo_pago,
	c.fecha_ultimo_pago,
	c.meses_a_pagar::double precision AS total_meses,
	c.saldo::double precision AS importe,
	c.mensualidad total,
	c.meses_a_pagar::double precision,
	c.the_geom_pam AS the_geom,
	c.latitud,
	c.longitud,
	c.ubicado,
	((COALESCE(c.nombres, ''::text) || COALESCE((' '::text || c.apepat), ''::text)) || COALESCE((' '::text || c.apemat), ''::text)) AS nombre,
	(c.calle || COALESCE(c.calle_letra, ''::text)) AS calle,
	(c.numero || COALESCE(c.numero_letra, ''::text)) AS numero,
	c.cruza1,
	c.cruza2,
	col.nombre colonia,
	c.activo,
	c.fecha_creacion,
	c.fecha_modificacion,
	NULL::text AS zona,
	c.folio_catastral,
	NULL::text AS origen,
	c.ruta,
	c.ultimo_mes_anio_pago::text AS ultimo_pago_txt
 FROM cliente.cliente c
 LEFT JOIN cliente.colonia col ON c.idcolonia = col.id
WHERE c.ubicado AND c.activo AND c.comercio = false
ORDER BY c.id_cliente