

ALTER TABLE "monitoreo"."unidad_cuadrilla"
    ADD COLUMN "idusuario_desactivo" int4,
    ADD CONSTRAINT "fk_idusuario_desactivo" FOREIGN KEY ("idusuario_desactivo") REFERENCES "usuario"."usuario" ("id");


ALTER TABLE "monitoreo"."cuadrilla"
    ADD COLUMN "idusuario_desactivo" int4,
    ADD CONSTRAINT "fk_idusuario_desactivo" FOREIGN KEY ("idusuario_desactivo") REFERENCES "usuario"."usuario" ("id");


ALTER TABLE "monitoreo"."cuadrilla_recolector"
    ADD COLUMN "idusuario_desactivo" int4,
    ADD CONSTRAINT "fk_idusuario_desactivo" FOREIGN KEY ("idusuario_desactivo") REFERENCES "usuario"."usuario" ("id");

