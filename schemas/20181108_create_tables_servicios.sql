CREATE TABLE servicio.orden_servicio
(
  id serial NOT NULL,
  idcliente integer,
  idservicio integer,
  fecha date,
  hora varchar,
  correo varchar,
  telefono varchar(10),
  cantidad integer,
  idestado integer,
  idbrigada integer,
  fecha_asignacion timestamp without time zone,
  califiacion integer,
  idusuario_asignacion integer,
  activo boolean DEFAULT true,
  fecha_creacion timestamp without time zone DEFAULT now(),
  fecha_modificacion timestamp(0) without time zone DEFAULT now(),
  CONSTRAINT pku_orden_servicio PRIMARY KEY (id)
);


CREATE TABLE servicio.brigadas
(
  id serial NOT NULL,
  clave integer,
  celular varchar(10),
  responsable varchar,
  activo boolean DEFAULT true,
  fecha_creacion timestamp without time zone DEFAULT now(),
  fecha_modificacion timestamp(0) without time zone DEFAULT now(),
  CONSTRAINT pku_brigadas PRIMARY KEY (id)
);

CREATE TABLE servicio.estado_orden_servicio
(
  id serial NOT NULL,
  clave varchar,
  nombre varchar,
  descripcion varchar,
  color varchar,
  activo boolean DEFAULT true,
  fecha_creacion timestamp without time zone DEFAULT now(),
  fecha_modificacion timestamp(0) without time zone DEFAULT now(),
  CONSTRAINT pku_estado_orden_servicio PRIMARY KEY (id)
);

create type servicio.tipo_cobro as enum('Fijo', 'Dinámico');

ALTER TABLE servicio.servicios
  ADD COLUMN tipo_cobro servicio.tipo_cobro;
