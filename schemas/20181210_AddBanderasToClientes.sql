ALTER TABLE cliente.cliente
  ADD COLUMN abandonado boolean DEFAULT false;
ALTER TABLE cliente.cliente
  ADD COLUMN comercio boolean DEFAULT false;
ALTER TABLE cliente.cliente
  ADD COLUMN marginado boolean DEFAULT false;
ALTER TABLE cliente.cliente
  ADD COLUMN nombre_comercio character varying;
ALTER TABLE cliente.cliente
  ADD COLUMN privada boolean DEFAULT false;
ALTER TABLE cliente.cliente
  ADD COLUMN nombre_privada character varying;
