with pcon as (
	INSERT INTO "usuario"."permiso"("nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo") VALUES (
	'Consultar historial descuento pensionados', 'Consultar historial descuento pensionados', 'clientes', 'segdescpen', 't', now(), now(),
		(select id from usuario.modulo where siglas = 'CLI' limit 1)
	) RETURNING id
),
percon as (
	insert into usuario.perfil_permiso (idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
	select 1, id, true, now(), now() from pcon
),
pdel as (
	INSERT INTO "usuario"."permiso"("nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo") VALUES (
	'Desactivar descuento pensionados', 'Desactivar descuento pensionados', 'clientes', 'deldescpen', 't', now(), now(),
		(select id from usuario.modulo where siglas = 'CLI' limit 1)
	) RETURNING id
),
perdel as (
	insert into usuario.perfil_permiso (idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
	select 1, id, true, now(), now() from pdel
)
select f.id, g.id from pcon f, pdel g