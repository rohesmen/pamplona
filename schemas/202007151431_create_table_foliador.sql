CREATE TABLE "folios"."foliador" (
  "id" serial,
  "anio" int4,
  "folio" int4,
	folio_anio varchar UNIQUE,
  "idusuario" int4,
  "activo" bool DEFAULT true,
  "fecha_creacion" timestamp(6) DEFAULT now(),
  "fecha_modificacion" timestamp(6) DEFAULT now(),
  CONSTRAINT "pku_foliador" PRIMARY KEY ("id")
);