with gm as (
	INSERT INTO "usuario"."grupomodulo"("siglas", "nombre", "descripcion", "orden", "activo", "fechacreacion", "fechamodificacion", "icono", "ruta", "panelclass") VALUES ('ANALISIS', 'Analisis', 'Analisis', 2, 't', now(), now(), 'fa-tachometer', '/analysis', 'panel-primary') RETURNING id
),
m as (
 INSERT INTO "usuario"."modulo"("idgrupomodulo", "siglas", "nombre", "descripcion", "iconomodulo", "directorio", "activo", "fechamodificacion", "fechacreacion", "orden")
 select gm.id, 'MODANALYSIS', 'Analisis', 'Analisis', 'fa-tachometer', '/analysis', 't', now(), now(), 1 from gm RETURNING id
),
p as (
	INSERT INTO "usuario"."permiso"(id, "nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo")
	select (select max(id) + 1 from usuario.permiso ),
	'Analisis', 'Analisis', 'analysis', 'index', 't', now(), now(), m.id from m RETURNING id
),
pp as (
	INSERT INTO "usuario"."perfil_permiso"("idperfil", "idpermiso", "activo", "fecha_creacion", "fecha_modificacion")
	select 1, p.id, true, now(), now() from p
)
select * from gm, m, p