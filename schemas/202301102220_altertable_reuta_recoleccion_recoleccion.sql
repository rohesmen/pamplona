ALTER TABLE monitoreo.ruta_recoleccion
    RENAME TO recoleccion;

ALTER TABLE "monitoreo"."recoleccion"
    ADD COLUMN "idcuadrilla" int4,
  ADD COLUMN "placa" varchar(255),
  ADD CONSTRAINT "fk_idcuadrilla" FOREIGN KEY ("idcuadrilla") REFERENCES "monitoreo"."cuadrilla" ("id"),
  ADD CONSTRAINT "fk_placa" FOREIGN KEY ("placa") REFERENCES "monitoreo"."unidad" ("placa");