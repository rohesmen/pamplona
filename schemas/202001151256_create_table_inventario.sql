CREATE TABLE folios.inventario (
  id serial,
	num_ini integer default 0,
	num_fin integer default 0,
	usados integer default 0,
	disponibles integer default 0,
	total integer default 0,
	foliador integer,
	idusuario integer,
  activo bool DEFAULT true,
  fecha_creacion timestamp WITHOUT time zone DEFAULT now(),
  fecha_modificacion timestamp WITHOUT time zone DEFAULT now(),
  CONSTRAINT pku_inventario PRIMARY KEY (id)
);