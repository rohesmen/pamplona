

DO $$ 
DECLARE
	id_grupomodulo integer;
    id_modulo integer;
    id_permiso integer;
BEGIN


    SELECT id INTO id_grupomodulo FROM usuario.grupomodulo WHERE siglas = 'MODSEG' AND nombre = 'Seguimiento' AND ruta = '/seguimiento' AND activo = true;


    INSERT INTO usuario.modulo(idgrupomodulo, siglas, nombre, descripcion, iconomodulo, directorio, activo, fechamodificacion, fechacreacion, orden)
	    VALUES (id_grupomodulo, 'UNIDSEG', 'Unidades', 'Módulo de Unidades', 'fa-share', '/unidadseguimiento', true, now(), now(), (select max(orden) + 1 from usuario.modulo)) RETURNING id INTO id_modulo;


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Unidades', 'Acceso al modulo de unidades', 'unidadseguimiento', 'index', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Crear Unidad', 'Permiso para crear unidad', 'unidadseguimiento', 'create', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());
	

	INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Editar Unidad', 'Permiso para editar unidad', 'unidadseguimiento', 'edit', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Consultar Unidad', 'Permiso para consultar unidad', 'unidadseguimiento', 'info', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Activar Unidad', 'Permiso para activar unidad', 'unidadseguimiento', 'activate', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


	INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Desactivar Unidad', 'Permiso para desactivar unidad', 'unidadseguimiento', 'deactivate', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


END $$;


--DROP VIEW monitoreo.view_unidad;
CREATE OR REPLACE VIEW monitoreo.view_unidad
    AS
    SELECT
        v.placa AS id,
		upper(v.nombre) AS nombre,
		upper(v.placa) AS placa,
		upper(v.numero_economico) AS numero_economico,
        upper(v.descripcion) AS descripcion,
        v.activo,
        v.fechacreacion,
        v.fechamodificacion,
        CASE WHEN v.activo THEN 'SI' ELSE 'NO' END AS activo_f,
        CASE WHEN v.fechacreacion IS NOT NULL THEN to_char(v.fechacreacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_creacion_f,
        CASE WHEN v.fechamodificacion IS NOT NULL THEN to_char(v.fechamodificacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_modificacion_f
    FROM monitoreo.unidad v;

