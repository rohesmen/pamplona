ALTER TABLE "transaccion"."pagos"
  ADD COLUMN "ispagoanual" bool DEFAULT false;

ALTER TABLE "cliente"."pagos"
  ADD COLUMN "ispagoanual" bool DEFAULT false;

ALTER TABLE "transaccion"."pagos"
  ADD COLUMN "totpagoanual" numeric;

ALTER TABLE "cliente"."pagos"
  ADD COLUMN "totpagoanual" numeric;