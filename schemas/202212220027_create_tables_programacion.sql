CREATE TABLE "monitoreo"."dias_recoleccion" (
                                                "sigla" text not null,
                                                "dia" text not null,
                                                "activo" bool DEFAULT true,
                                                "fechacreacion" timestamp(6) DEFAULT now(),
                                                "fechamodificacion" timestamp(6) DEFAULT now(),
                                                CONSTRAINT "pk_dias_recoleccion" PRIMARY KEY ("sigla")
);

insert into monitoreo.dias_recoleccion (sigla, dia) values
                                                        ('LUNES', 'Lunes'), ('MARTES','Martes'), ('MIERCOLES', 'Miércoles'), ('JUEVES','Jueves'), ('VIERNES','Viernes'), ('SABADO','Sábado'), ('DOMINGO','DOmingo');

CREATE TABLE "monitoreo"."tipo_recoleccion" (
                                                "sigla" text,
                                                "tipo" text not null,
                                                "activo" bool DEFAULT true,
                                                "fechacreacion" timestamp(6) DEFAULT now(),
                                                "fechamodificacion" timestamp(6) DEFAULT now(),
                                                CONSTRAINT "pk_tipo_recoleccion" PRIMARY KEY ("sigla")
);

insert into monitoreo.tipo_recoleccion (sigla, tipo) values
                                                         ('ORGANICA', 'Orgánica'), ('INORGANICA', 'Inorgánica');

CREATE TABLE "monitoreo"."ruta_recoleccion" (
                                            id serial,
                                            idruta integer,
                                            dia varchar,
                                            tipo_recoleccion varchar,
                                            idturno integer,
                                            "activo" bool DEFAULT true,
                                            "fechacreacion" timestamp(6) DEFAULT now(),
                                            "fechamodificacion" timestamp(6) DEFAULT now(),
                                            CONSTRAINT "pk_ruta_recoleccion" PRIMARY KEY ("id")
);

CREATE TABLE "monitoreo"."ruta" (
                                    "id" serial,
                                    "nombre" text,
                                    the_geom geometry,
                                    "activo" bool DEFAULT true,
                                    "fecha_creacion" timestamp(6) DEFAULT now(),
                                    "fecha_modificacion" timestamp(6) DEFAULT now(),
                                    CONSTRAINT "pku_ruta" PRIMARY KEY ("id")
);

ALTER TABLE "monitoreo"."ruta_recoleccion"
    ADD CONSTRAINT "fk_dia" FOREIGN KEY ("dia") REFERENCES "monitoreo"."dias_recoleccion" ("sigla"),
  ADD CONSTRAINT "fk_tipo_recoleccion" FOREIGN KEY ("tipo_recoleccion") REFERENCES "monitoreo"."tipo_recoleccion" ("sigla"),
  ADD CONSTRAINT "fk_ruta" FOREIGN KEY ("idruta") REFERENCES "monitoreo"."ruta" ("id"),
  ADD CONSTRAINT "fk_idturno" FOREIGN KEY ("idturno") REFERENCES "monitoreo"."turno" ("id");

ALTER TABLE "monitoreo"."dias_recoleccion"
    ADD COLUMN "abreviatura" varchar(255);

update monitoreo.dias_recoleccion set abreviatura = 'L' where sigla = 'LUNES';
update monitoreo.dias_recoleccion set abreviatura = 'MA' where sigla = 'MARTES';
update monitoreo.dias_recoleccion set abreviatura = 'MI' where sigla = 'MIERCOLES';
update monitoreo.dias_recoleccion set abreviatura = 'J' where sigla = 'JUEVES';
update monitoreo.dias_recoleccion set abreviatura = 'V' where sigla = 'VIERNES';
update monitoreo.dias_recoleccion set abreviatura = 'S' where sigla = 'SABADO';
update monitoreo.dias_recoleccion set abreviatura = 'D' where sigla = 'DOMINGO';