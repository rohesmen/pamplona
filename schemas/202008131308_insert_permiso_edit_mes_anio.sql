with pfol as (
	INSERT INTO "usuario"."permiso"(id, "nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo") VALUES (
	(select max(id) + 1 from usuario.permiso ),
	'Editar meses y años de clientes', 'Editar meses y años de clientes', 'clientes', 'edmesanio', 't', now(), now(),
		(select id from usuario.modulo where siglas = 'CLI' limit 1)
	) RETURNING id
),
perfil as (
	insert into usuario.perfil_permiso (idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
	select 1, id, true, now(), now() from pfol
)
select f.id from pfol f