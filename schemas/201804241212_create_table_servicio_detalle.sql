-- Table: servicio.orden_servicio

-- DROP TABLE servicio.orden_servicio;

CREATE TABLE servicio.orden_servicio_detalle
(
  id serial NOT NULL,
  idorden_servicio integer,
  costo numeric,
  activo boolean DEFAULT true,
  fecha_creacion timestamp without time zone DEFAULT now(),
  fecha_modificacion timestamp(0) without time zone DEFAULT now(),
  CONSTRAINT pku_orden_servicio_detalle PRIMARY KEY (id)
)
