CREATE TABLE comun.bitacora_estatus (
  id serial,
  idcliente integer NOT NULL,
  idestatus integer not null,
	idestatus_anterior integer not null,
  idusuario integer NOT NULL,
	nomcom_anterior text,
	nomcom text,
	motivo text not null,
	origen varchar default 'APP',
  activo boolean DEFAULT true,
  fecha_creacion timestamp without time zone DEFAULT now(),
  fecha_modificacion timestamp without time zone DEFAULT now(),
  CONSTRAINT bitacora_estatus_pkey PRIMARY KEY (id),
  CONSTRAINT fk_bitacora_estatus_cliente FOREIGN KEY (idcliente)
      REFERENCES cliente.cliente (id_cliente) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_bitacora_estatus_estatus_cliente FOREIGN KEY (idestatus)
      REFERENCES cliente.estatus_cliente (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_bitacora_estatus_anterior_estatus_cliente FOREIGN KEY (idestatus_anterior)
      REFERENCES cliente.estatus_cliente (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_bitacora_estatus_usurio FOREIGN KEY (idusuario)
      REFERENCES usuario.usuario (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);