

CREATE TABLE monitoreo.cuadrilla_asignacion
(
    id serial,
    idrecoleccion integer,
    idcuadrilla integer,
    idrecolector integer,
    ischofer boolean DEFAULT false,
    idusuario integer,
    activo boolean DEFAULT true,
    fecha_creacion timestamp(6) without time zone DEFAULT now(),
    fecha_modificacion timestamp(6) without time zone DEFAULT now(),
    idusuario_desactivo integer,
    CONSTRAINT pku_cuadrilla_asignacion PRIMARY KEY (id),
    CONSTRAINT fk_idrecoleccion FOREIGN KEY (idrecoleccion)
        REFERENCES monitoreo.recoleccion (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_idcuadrilla FOREIGN KEY (idcuadrilla)
        REFERENCES monitoreo.cuadrilla (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_idrecolector FOREIGN KEY (idrecolector)
        REFERENCES monitoreo.recolector (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_idusuario FOREIGN KEY (idusuario)
        REFERENCES usuario.usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_idusuario_desactivo FOREIGN KEY (idusuario_desactivo)
        REFERENCES usuario.usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

