create or replace view folios.view_folios as
 SELECT rf.id,
    rf.serie,
    rf.num_ini,
    rf.num_fin,
    rf.idcobratario,
    u.usuario AS cobratario,
    rf.porliquidar,
    rf.disponibles,
    (rf.porliquidar + rf.disponibles) AS transito,
    rf.activo,
    to_char(rf.fecha_creacion, 'DD/MM/YYYY'::text) AS fecha_asignacion,
    a.usuario,
    rf.monto_porliquidar
   FROM ((folios.rango_folio rf
     LEFT JOIN usuario.usuario u ON ((rf.idcobratario = u.id)))
     LEFT JOIN usuario.usuario a ON ((rf.idusuario = a.id)))