
DO $$ 
DECLARE
BEGIN
	INSERT INTO cliente.metodo_pago (id, nombre, descripcion, activo, fecha_creacion, fecha_modificacion, orden, referencia)
			values (2, 'PAYPAL', null, true, now(), now(), 2, true);


	INSERT INTO cliente.forma_pago (id, nombre, descripcion, activo, fecha_creacion, fecha_modificacion)
			values (2, 'PAYPAL', null, true, now(), now());

END $$;

