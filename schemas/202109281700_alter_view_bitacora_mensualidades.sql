
--modificar vista de bitacora agregar datos del nombre del usuario
CREATE OR REPLACE VIEW comun.view_bitacora_mensualidades AS 
select b.identificador cliente, (cambios::json)->>'mensualidad' mensualidad, 
u.usuario usuario, u.id idusuario, to_char(b.fecha_creacion, 'DD/MM/YYYY HH24:MI'::text) fecha,
		b.apartado origen,
		b.motivo,
        upper(u.apellido_materno) apemat_usuario,
        upper(u.apellido_paterno) apepat_usuario,
        upper(u.nombre) nombre_usuario,
        upper((u.nombre::text || ' ' || u.apellido_paterno::text || ' ' || u.apellido_materno::text)) nombre_completo_usuario
from comun.bitacora b
left join usuario.usuario u on b.idusuario = u.id
join (
	select count(1), b.identificador
	from comun.bitacora b
	left join usuario.usuario u on b.idusuario = u.id
	where b.accion ilike '%ACTUALIZAR MENSUALIDAD%'
	GROUP BY b.identificador HAVING count(1) > 1
) c on c.identificador = b.identificador
where 
--b.accion = 'ACTUALIZAR MENSUALIDAD' and 
b.idusuario != 1
order by b.identificador, b.fecha_creacion desc;

--agregar permiso exportar reporte bitacora mensualidades
with pfol as (
	INSERT INTO "usuario"."permiso"(id, "nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo") VALUES (
	(select max(id) + 1 from usuario.permiso ),
	'Exportar bitacora mensualidades', 'Exportar bitacora mensualidades', 'bitmens', 'exportexcel', 't', now(), now(),
		(select id from usuario.modulo where siglas = 'MODBITMENS' limit 1)
	) RETURNING id
),
perfil as (
	insert into usuario.perfil_permiso (idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
	select 1, id, true, now(), now() from pfol
)
select f.id from pfol f

