

DO $$ 
DECLARE
	id_grupomodulo integer;
    id_modulo integer;
    id_permiso integer;
BEGIN


    INSERT INTO usuario.grupomodulo(siglas, nombre, descripcion, orden, activo, fechacreacion, fechamodificacion, icono, ruta, panelclass)
        VALUES ('MODSEG', 'Seguimiento', 'Módulo de Seguimiento', (select max(orden) + 1 from usuario.grupomodulo), true, now(), now(), 'fa-share', '/seguimiento', 'panel-primary') RETURNING id INTO id_grupomodulo;


    INSERT INTO usuario.modulo(idgrupomodulo, siglas, nombre, descripcion, iconomodulo, directorio, activo, fechamodificacion, fechacreacion, orden)
	    VALUES (id_grupomodulo, 'RUTSEG', 'Rutas', 'Módulo de Rutas', 'fa-share', '/rutaseguimiento', true, now(), now(), (select max(orden) + 1 from usuario.modulo)) RETURNING id INTO id_modulo;


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Rutas', 'Acceso al modulo de rutas', 'rutaseguimiento', 'index', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Crear Ruta', 'Permiso para crear ruta', 'rutaseguimiento', 'create', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());
	

	INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Editar Ruta', 'Permiso para editar ruta', 'rutaseguimiento', 'edit', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


	INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Desactivar Ruta', 'Permiso para desactivar ruta', 'rutaseguimiento', 'deactivate', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


END $$;