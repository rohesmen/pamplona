-- ---------------------------------------------------------------------------------------------
-- Creacion del Permiso Cancelacion de Pagos masivos
BEGIN;
insert into usuario.permiso(
	id,
	nombre,
	descripcion,
	recurso,
	accion,
	activo,
	fecha_modificacion,
	fecha_creacion,
	idmodulo)
values(
	51,
	'Cancelar Cobro por Razon Social',
	'Cancelar Cobro por Razon Social',
	'caja',
	'cancelar_rsocial',
	true,
	now(),
	now(),
	3	
);
COMMIT;
-- Relacion entre el perfil y el permiso
BEGIN;
insert into usuario.perfil_permiso(
	idperfil,
	idpermiso,
	activo,
	fecha_creacion,
	fecha_modificacion)
values(
	1,
	51,
	true,
	now(),
	now()
);
COMMIT;