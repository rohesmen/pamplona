ALTER TABLE "cliente"."pagos"
  ADD COLUMN "isfacturado" bool DEFAULT false,
  ADD COLUMN "ruta_factura" text;