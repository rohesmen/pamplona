INSERT INTO usuario.permiso(id, nombre, descripcion, recurso, accion, activo, fecha_modificacion, fecha_creacion, idmodulo)
values ((SELECT MAX(id)+1 FROM usuario.permiso),'Módulo servicio edit', 'Módulo servicio editar', 'servicios', 'edit', true, now(), now(), (SELECT id FROM usuario.modulo where siglas='SERVICIOS'));

INSERT INTO usuario.perfil_permiso(idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
values (1, (select id from usuario.permiso where nombre='Módulo servicio edit'), true, now(), now());