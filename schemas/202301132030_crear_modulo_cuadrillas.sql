

DO $$ 
DECLARE
	id_grupomodulo integer;
    id_modulo integer;
    id_permiso integer;
BEGIN


    SELECT id INTO id_grupomodulo FROM usuario.grupomodulo WHERE siglas = 'MODSEG' AND nombre = 'Seguimiento' AND ruta = '/seguimiento' AND activo = true;


    INSERT INTO usuario.modulo(idgrupomodulo, siglas, nombre, descripcion, iconomodulo, directorio, activo, fechamodificacion, fechacreacion, orden)
	    VALUES (id_grupomodulo, 'CUADRISEG', 'Cuadrillas', 'Módulo de Cuadrillas', 'fa-share', '/cuadrillaseguimiento', true, now(), now(), (select max(orden) + 1 from usuario.modulo)) RETURNING id INTO id_modulo;


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Cuadrillas', 'Acceso al modulo de cuadrillas', 'cuadrillaseguimiento', 'index', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Crear Cuadrilla', 'Permiso para crear cuadrilla', 'cuadrillaseguimiento', 'create', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());
	

	INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Editar Cuadrilla', 'Permiso para editar cuadrilla', 'cuadrillaseguimiento', 'edit', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Consultar Cuadrilla', 'Permiso para consultar cuadrilla', 'cuadrillaseguimiento', 'info', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Activar Cuadrilla', 'Permiso para activar cuadrilla', 'cuadrillaseguimiento', 'activate', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


	INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Desactivar Cuadrilla', 'Permiso para desactivar cuadrilla', 'cuadrillaseguimiento', 'deactivate', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Eliminar Cuadrilla Recolector', 'Permiso para eliminar recolector', 'cuadrillaseguimiento', 'deletecuadrec', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Agregar Cuadrilla Recolector', 'Permiso para agergar recolector', 'cuadrillaseguimiento', 'addcuadrec', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


END $$;


--DROP VIEW monitoreo.view_cuadrilla;
CREATE OR REPLACE VIEW monitoreo.view_cuadrilla
    AS
    SELECT
        v.id,
		upper(v.nombre) AS nombre,
		upper(u.usuario) AS usuario,
        v.idusuario,
        v.activo,
        v.fecha_creacion,
        v.fecha_modificacion,
        CASE WHEN v.activo THEN 'SI' ELSE 'NO' END AS activo_f,
        CASE WHEN v.fecha_creacion IS NOT NULL THEN to_char(v.fecha_creacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_creacion_f,
        CASE WHEN v.fecha_modificacion IS NOT NULL THEN to_char(v.fecha_modificacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_modificacion_f
    FROM monitoreo.cuadrilla v
    LEFT JOIN usuario.usuario u ON u.id = v.idusuario;


--DROP VIEW monitoreo.view_cuadrilla_recolector;
CREATE OR REPLACE VIEW monitoreo.view_cuadrilla_recolector
    AS
    SELECT
        v.id,
        v.idcuadrilla,
        v.idrecolector,
        v.ischofer,
		upper(c.nombre) AS cuadrilla,
        (COALESCE(upper(r.nombres::text), ''::text)) || COALESCE(' '::text || upper(r.apepat::text), ''::text) || COALESCE(' '::text || upper(r.apemat::text), ''::text) AS recolector,
        upper(u.usuario) AS usuario,
        CASE WHEN v.ischofer THEN 'CHOFER' ELSE 'RECOLECTOR' END AS tipo,
        v.idusuario,
        v.activo,
        v.fecha_creacion,
        v.fecha_modificacion,
        CASE WHEN v.activo THEN 'SI' ELSE 'NO' END AS activo_f,
        CASE WHEN v.fecha_creacion IS NOT NULL THEN to_char(v.fecha_creacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_creacion_f,
        CASE WHEN v.fecha_modificacion IS NOT NULL THEN to_char(v.fecha_modificacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_modificacion_f
    FROM monitoreo.cuadrilla_recolector v
    LEFT JOIN monitoreo.cuadrilla c ON c.id = v.idcuadrilla
    LEFT JOIN monitoreo.recolector r ON r.id = v.idrecolector
    LEFT JOIN usuario.usuario u ON u.id = v.idusuario;

