ALTER TABLE "monitoreo"."ruta"
    ADD COLUMN "nombre_archivo" varchar,
  ADD COLUMN "size_archivo" float8,
  ADD COLUMN "geojson" text,
  ADD COLUMN "kml" text;