------------------------------------------------------------------------------------
-- Modificamos el valor por defecto para columnas en cliente.historial_pago
ALTER TABLE cliente.historial_pago
  ALTER COLUMN "concepto_servicio" SET DEFAULT '';
  
ALTER TABLE cliente.historial_pago
  ALTER COLUMN "idtipo_servicio" SET DEFAULT 0;
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- Funcion para realizar la actualizacion de los valores para las columnas ultimo_anio_pago y ultimo_mes_pago
CREATE or replace FUNCTION update_PagoCliente() RETURNS trigger AS $update_PagoCliente$	
	BEGIN		
		IF NEW.idtipo_cobro = 1 THEN
			UPDATE cliente.cliente set 
				ultimo_anio_pago = cast(NEW.anio as integer),
				ultimo_mes_pago  = NEW.mes
			WHERE id_cliente = NEW.idcliente;		
		END IF;		  
	  RETURN NEW;
	END
	$update_PagoCliente$
LANGUAGE plpgsql;
------------------------------------------------------------------------------------
-- Desabilitar el Trigger
ALTER TABLE cliente.historial_pago DISABLE TRIGGER tg_updatePagocliente;

-- Eliminar el Trigder
DROP TRIGGER IF EXISTS tg_updatePagocliente ON cliente.historial_pago;

--Generar el Trigger
CREATE TRIGGER tg_updatePagocliente AFTER INSERT ON cliente.historial_pago 
FOR EACH ROW EXECUTE PROCEDURE update_PagoCliente(); 
