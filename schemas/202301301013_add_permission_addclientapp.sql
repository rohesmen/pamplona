with anclar as (
INSERT INTO "usuario"."permiso"("nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo") VALUES (
    'Agregar cliente desde APP', 'Agregar cliente desde APP', 'clientes', 'percliaddapp', 't', now(), now(),
    (select id from usuario.modulo where siglas = 'CLI' limit 1)
    ) RETURNING id
    ),
    panclar as (
insert into usuario.perfil_permiso (idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
select 1, id, true, now(), now() from anclar
    )
select * from anclar