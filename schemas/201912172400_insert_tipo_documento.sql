﻿INSERT INTO "comun"."tipo_documento"("tipo_documento", "lcomprobante", "activo", "fecha_creacion", "fecha_modificacion") 
VALUES ('PAGO DE PREDIAL', true, true, now(), now());

INSERT INTO "comun"."tipo_documento"("tipo_documento", "lcomprobante", "activo", "fecha_creacion", "fecha_modificacion") 
VALUES ('JAPAY', true, true, now(), now());

INSERT INTO "comun"."tipo_documento"("tipo_documento", "lcomprobante", "activo", "fecha_creacion", "fecha_modificacion") 
VALUES ('CFE', true, true, now(), now());

INSERT INTO "comun"."tipo_documento"("tipo_documento", "lcomprobante", "activo", "fecha_creacion", "fecha_modificacion") 
VALUES ('INE', false, true, now(), now());

INSERT INTO "comun"."tipo_documento"("tipo_documento", "lcomprobante", "activo", "fecha_creacion", "fecha_modificacion") 
VALUES ('LICENCIA DE CONDUCIR', false, true, now(), now());

INSERT INTO "comun"."tipo_documento"("tipo_documento", "lcomprobante", "activo", "fecha_creacion", "fecha_modificacion") 
VALUES ('PASAPORTE', false, true, now(), now());