-- Table: cliente.cliente --russel
-- 01/Nov/2016
-- DROP TABLE cliente.cliente;

/*CREATE TABLE cliente.cliente
(
  id_cliente SERIAL,
  calle integer,
  calle_letra text,
  tipo_calle text,
  numero integer,
  numero_letra text,
  tipo_numero text,
  cruza1 character varying(50),
  letra_cruza1 character varying(50),
  tipo_cruza1 character varying(50),
  cruza2 character varying(50),
  letra_cruza2 character varying(50),
  tipo_cruza2 character varying(50),
  idcolonia integer,
  localidad character varying(150),
  apepat text,
  apemat text,
  nombres text,
  apepat_propietario text,
  apemat_propietario text,
  nombres_propietario text,
  folio_catastral integer,
  idestatuscliente integer,
  activo boolean,
  fecha_creacion timestamp without time zone,
  fecha_modificacion timestamp without time zone,
  latitud double precision,
  longitud double precision,
  ubicado boolean,
  idtarifa_colonia integer,
  idruta integer,
  idtipo_contrato integer,
  referencia_ubicacion text,
  fisica boolean,
  nombre_moral character varying(150),
  rfc character varying(100),
  direccion_moral character varying(250),
  telefono character varying(10),
  correo character varying(50),
  idtipo_descuento integer,
  descuento numeric,
  mensualidad integer,
  ultimo_anio_pago integer,
  ultimo_mes_pago character varying(50),
  observacion text,
  CONSTRAINT pku_cliente PRIMARY KEY (id_cliente),
  CONSTRAINT fk_cliente_colonia FOREIGN KEY (idcolonia)
    REFERENCES cliente.colonia (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT fk_cliente_estatus_cliente FOREIGN KEY (idestatuscliente)
      REFERENCES cliente.estatus_cliente (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT fk_cliente_tarifa_colonia FOREIGN KEY (idtarifa_colonia)
      REFERENCES cliente.tarifa_colonia (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT fk_cliente_ruta FOREIGN KEY (idruta)
      REFERENCES cliente.ruta (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT fk_cliente_tipo_contrato FOREIGN KEY (idtipo_contrato)
      REFERENCES cliente.tipo_contrato (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT fk_cliente_tipo_descuento FOREIGN KEY (idtipo_descuento)
      REFERENCES cliente.tipo_descuento (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);*/


--### Jueves 10-Nov-2016 00:58 am
-- Actualizada la estructura de cliente
-- Table: cliente.cliente

-- DROP TABLE cliente.cliente;

CREATE TABLE cliente.cliente
(
  id_cliente serial NOT NULL,
  calle_letra text,
  tipo_calle text,
  numero_letra text,
  tipo_numero text,
  cruza1 character varying(50),
  letra_cruza1 character varying(50),
  tipo_cruza1 character varying(50),
  cruza2 character varying(50),
  letra_cruza2 character varying(50),
  tipo_cruza2 character varying(50),
  idcolonia integer,
  localidad character varying(150),
  apepat text,
  apemat text,
  nombres text,
  apepat_propietario text,
  apemat_propietario text,
  nombres_propietario text,
  folio_catastral integer,
  idestatuscliente integer,
  activo boolean DEFAULT true,
  fecha_creacion timestamp without time zone DEFAULT now(),
  fecha_modificacion timestamp without time zone DEFAULT now(),
  latitud double precision,
  longitud double precision,
  ubicado boolean DEFAULT false,
  idtarifa_colonia integer,
  idruta integer,
  idtipo_contrato integer,
  referencia_ubicacion text,
  fisica boolean DEFAULT true,
  nombre_moral character varying(150),
  rfc character varying(100),
  direccion_moral character varying(250),
  telefono character varying(10),
  correo character varying(50),
  idtipo_descuento integer,
  descuento numeric,
  mensualidad integer,
  ultimo_anio_pago integer,
  ultimo_mes_pago character varying(50),
  observacion text,
  calle text,
  numero text,
  direccion_otro text,
  vigente boolean DEFAULT false,
  numero_contrato text,
  id_usuario integer,
  CONSTRAINT pku_cliente PRIMARY KEY (id_cliente)
);