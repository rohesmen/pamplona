﻿-- Table: cliente.ruta

--DROP TABLE cliente.cliente_descuento_pensionado;

CREATE TABLE cliente.cliente_descuento_pensionado
(
  id serial,
  id_cliente integer NOT NULL REFERENCES cliente.cliente(id_cliente),
  idusuario integer NOT NULL REFERENCES usuario.usuario(id),
  oficio text NOT NULL,
  fecha_inicio timestamp without time zone,
  fecha_fin timestamp without time zone,
  activo boolean,
  fecha_creacion timestamp without time zone,
  fecha_modificacion timestamp without time zone,
  CONSTRAINT pku_cliente_descuento_pensionado PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cliente.cliente_descuento_pensionado
  OWNER TO postgres;
