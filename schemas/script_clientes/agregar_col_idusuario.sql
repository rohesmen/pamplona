--Se agrega la columna idusuario = id de la sesión
-- 07/11/2016
ALTER TABLE cliente.cliente
    ADD COLUMN id_usuario integer,
    CONSTRAINT fk_cliente_usuario FOREIGN KEY (id_usuario)
      REFERENCES usuario.usuario (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;