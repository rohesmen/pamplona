﻿
-- DROP TABLE queja.seguimiento;

CREATE TABLE queja.seguimiento
(
  id serial NOT NULL,
  id_origen INTEGER NOT NULL,
  id_etapa INTEGER NOT NULL,
  id_etapa_anterior INTEGER ,
  id_usuario INTEGER NOT NULL,
  activo boolean NOT NULL,
  fecha_creacion timestamp without time zone NOT NULL,
  fecha_modificacion timestamp without time zone,
  CONSTRAINT pku_seguimiento PRIMARY KEY (id),
  CONSTRAINT seguimiento_id_etapa_fkey FOREIGN KEY (id_etapa)
      REFERENCES queja.etapa (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT seguimiento_id_etapa_anterior_fkey FOREIGN KEY (id_etapa_anterior)
      REFERENCES queja.etapa (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT seguimiento_id_usuario_fkey FOREIGN KEY (id_usuario)
      REFERENCES usuario.usuario (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE queja.seguimiento
  OWNER TO postgres; 

 