-- Clientes - actualización valores --
-- russel --
-- Módulo de clientes --

-- grupomodulo --
INSERT INTO usuario.grupomodulo(
	id, siglas, nombre, descripcion, orden, activo, fechacreacion, fechamodificacion, icono, ruta, panelclass)
	VALUES ((select max(idperfil) + 1 from usuario.grupomodulo), 'MODCLI', 'Clientes', 'Módulo de Clientes', 2, true, 'now', 'now', 'fa-users', '/clientes', 'panel-primary');

-- modulo --
-- idgrupomodulo : que se creo
INSERT INTO usuario.modulo(
	id, idgrupomodulo, siglas, nombre, descripcion, iconomodulo, directorio, activo, fechamodificacion, fechacreacion, orden)
	VALUES (2, 2, 'CLI', 'Clientes', 'Clientes', 'fa-users', '/clientes', true, 'now', 'now', 2);

-- permiso --
INSERT INTO usuario.permiso(
	id, nombre, descripcion, recurso, accion, activo, fecha_modificacion, fecha_creacion, idmodulo)
	VALUES ((select max(id) + 1 from usuario.permiso), 'Cliente', 'Clientes', 'clientes', 'index', true, 'now', 'now', 2),
	((select max(id) + 1 from usuario.permiso), 'Busqudas', 'Busqudas', 'clientes', 'search', true, 'now', 'now', 2),
	((select max(id) + 1 from usuario.permiso), 'Agregar clientes', 'Agregar clientes', 'clientes', 'add', true, 'now', 'now', 2),
	((select max(id) + 1 from usuario.permiso), 'Editar clientes', 'Editar clientes', 'clientes', 'edit', true, 'now', 'now', 2),
	((select max(id) + 1 from usuario.permiso), 'Guardar clientes', 'Guardar clientes', 'clientes', 'save', true, 'now', 'now', 2);

-- perfil_permiso --
INSERT INTO usuario.perfil_permiso(
	idperfil, idpermiso, activo, fecha_creacion, fecha_modificacion)
	VALUES ((select max(idperfil) + 1 from usuario.perfil_permiso), 7, true, 'now', 'now'),
	((select max(idperfil) + 1 from usuario.perfil_permiso), 8, true, 'now', 'now'),
	((select max(idperfil) + 1 from usuario.perfil_permiso), 9, true, 'now', 'now'),
	((select max(idperfil) + 1 from usuario.perfil_permiso), 10, true, 'now', 'now'),
	((select max(idperfil) + 1 from usuario.perfil_permiso), 11, true, 'now', 'now');