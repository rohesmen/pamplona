﻿CREATE SCHEMA queja;


-- DROP TABLE queja.etapa;

CREATE TABLE queja.etapa
(
  id serial NOT NULL,
  id_etapa_padre integer,
  orden integer,
  nombre text NOT NULL,
  descripcion text NOT NULL,
  inicial boolean NOT NULL,
  final boolean NOT NULL,
  activo boolean,
  fecha_creacion timestamp without time zone,
  fecha_modificacion timestamp without time zone,
  CONSTRAINT pku_etapa PRIMARY KEY (id),
  CONSTRAINT etapa_id_fkey FOREIGN KEY (id_etapa_padre)
      REFERENCES queja.etapa (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE queja.etapa
  OWNER TO postgres; 

  
-- DROP TABLE queja.queja;

CREATE TABLE queja.queja
(
  id serial NOT NULL,
  id_cliente integer NOT NULL,
  id_etapa integer NOT NULL,
  titulo text NOT NULL,
  descripcion text NOT NULL,
  apepat_reporta text,
  apemat_reporta text,
  nombres_reporta text,
  origen text NOT NULL,
  apepat_ayuntamiento text,
  apemat_ayuntamiento text,
  nombres_ayuntamiento text,
  folio text,
  activo boolean,
  fecha_creacion timestamp without time zone,
  fecha_modificacion timestamp without time zone,
  CONSTRAINT pku_queja PRIMARY KEY (id),
  CONSTRAINT queja_idcliente_fkey FOREIGN KEY (id_cliente)
      REFERENCES cliente.cliente (id_cliente) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT queja_idetapa_fkey FOREIGN KEY (id_etapa)
      REFERENCES queja.etapa (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE queja.queja
  OWNER TO postgres; 