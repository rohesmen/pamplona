﻿-- Table: cliente.ruta

DROP TABLE cliente.ruta;

CREATE TABLE cliente.ruta
(
  id serial,
  nombre text NOT NULL,
  idcolonia integer NOT NULL,
  idusuario integer NOT NULL REFERENCES usuario.usuario(id),
  activo boolean,
  fecha_creacion timestamp without time zone,
  fecha_modificacion timestamp without time zone,
  CONSTRAINT pku_ruta PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cliente.ruta
  OWNER TO postgres;
