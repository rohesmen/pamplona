﻿ALTER TABLE queja.queja
  ADD COLUMN fecha_reporte timestamp without time zone;
ALTER TABLE queja.queja
  ADD COLUMN fecha_atencion timestamp without time zone;
