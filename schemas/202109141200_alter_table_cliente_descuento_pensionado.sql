ALTER TABLE "cliente"."cliente_descuento_pensionado" 
  ADD COLUMN "archivo" varchar(255),
  ADD COLUMN "tipo_archivo" varchar(255),
  ADD COLUMN "idpago" int4,
  ADD COLUMN "fecha_cancelacion" timestamp(255),
  ADD COLUMN "motivo_cancelacion" text,
  ADD COLUMN "idusuario_cancelacion" int4;

ALTER TABLE "cliente"."pagos" 
  ADD COLUMN "iddescuento_pensionado" int4;