create or replace view geodata.clientes_view as
 SELECT c.id_cliente,
    c.id_cliente no_cliente,
    coalesce('C. ' || calle, '') || coalesce(calle_letra, '') || coalesce(' #' || numero, '') || coalesce(numero_letra, '') || coalesce(' COl. ' || col.nombre, '') descripcion,
     null ultimo_pago,
    null fecha_ultimo_pago,
    null total_meses,
    null importe,
    null total,
		date_part('year', age(now(), ('01/' || ultimo_mes_pago || '/' || ultimo_anio_pago)::TIMESTAMP)) +
		date_part('month', age(now(), ('01/' || ultimo_mes_pago || '/' || ultimo_anio_pago)::TIMESTAMP)) meses_a_pagar,
    c.the_geom,
    c.latitud,
    c.longitud,
    c.ubicado,
    coalesce(nombres, '') || coalesce(' ' || apepat, '') || coalesce(' ' || apemat, '') nombre,
    c.calle,
    c.numero,
    c.cruza1,
    c.cruza2,
    col.nombre colonia,
    c.activo,
    c.fecha_creacion,
    c.fecha_modificacion,
    null zona,
    c.folio_catastral,
    null origen
   FROM cliente.cliente c
	 left join cliente.colonia col on c.idcolonia = col.id
  WHERE ubicado and c.activo
  order BY c.id_cliente