ALTER TABLE comun.gstrackme_camiones_ubicacion
  ADD COLUMN nombre_conductor character varying;
ALTER TABLE comun.gstrackme_camiones_ubicacion
  ADD COLUMN placas character varying;
ALTER TABLE comun.gstrackme_camiones_ubicacion
  ADD COLUMN colonias character varying;
ALTER TABLE comun.gstrackme_camiones_ubicacion
  ADD COLUMN foto_conductor character varying;

ALTER TABLE comun.gstrackme_camiones_ubicacion
  DROP CONSTRAINT pk_gstrackme_camiones_ubicacion;
ALTER TABLE comun.gstrackme_camiones_ubicacion
   ALTER COLUMN uuid DROP NOT NULL;
ALTER TABLE comun.gstrackme_camiones_ubicacion
  ADD CONSTRAINT pk_gstrackme_camiones PRIMARY KEY (idgeo);

ALTER TABLE comun.gstrackme_camiones_ubicacion
   ALTER COLUMN uuid SET DEFAULT uuid_generate_v4();
