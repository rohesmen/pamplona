

CREATE TABLE "monitoreo"."unidad_cuadrilla" (
    "id" serial,
    "placa" character varying,
    "idcuadrilla" integer,
    "idusuario" integer,
    "activo" boolean DEFAULT true,
    "fecha_creacion" timestamp(6) without time zone DEFAULT now(),
    "fecha_modificacion" timestamp(6) without time zone DEFAULT now(),
    CONSTRAINT "pku_unidad_cuadrilla" PRIMARY KEY ("id"),
    CONSTRAINT fk_idunidad_unidad_cuadrilla FOREIGN KEY (placa)
        REFERENCES "monitoreo"."unidad" (placa) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_idcuadrilla_unidad_cuadrilla FOREIGN KEY (idcuadrilla)
        REFERENCES "monitoreo"."cuadrilla" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_idusuario_unidad_cuadrilla FOREIGN KEY (idusuario)
        REFERENCES "usuario"."usuario" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);


--DROP VIEW monitoreo.view_unidad_cuadrilla;
CREATE OR REPLACE VIEW monitoreo.view_unidad_cuadrilla
    AS
    SELECT
        v.id,
        v.idcuadrilla,
        v.placa,
		upper(c.nombre) AS cuadrilla,
        upper(u.nombre) AS unidad,
        upper(u.numero_economico) AS numero_economico,
        upper(s.usuario) AS usuario,
        v.idusuario,
        v.activo,
        v.fecha_creacion,
        v.fecha_modificacion,
        CASE WHEN v.activo THEN 'SI' ELSE 'NO' END AS activo_f,
        CASE WHEN v.fecha_creacion IS NOT NULL THEN to_char(v.fecha_creacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_creacion_f,
        CASE WHEN v.fecha_modificacion IS NOT NULL THEN to_char(v.fecha_modificacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_modificacion_f
    FROM monitoreo.unidad_cuadrilla v
    LEFT JOIN monitoreo.cuadrilla c ON c.id = v.idcuadrilla
    LEFT JOIN monitoreo.unidad u ON u.placa = v.placa
    LEFT JOIN usuario.usuario s ON s.id = v.idusuario;


DO $$ 
DECLARE
    id_modulo integer;
    id_permiso integer;
BEGIN

    SELECT id INTO id_modulo FROM usuario.modulo WHERE siglas = 'UNIDSEG' AND nombre = 'Unidades' AND directorio = '/unidadseguimiento' AND activo = true;


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Eliminar Cuadrilla', 'Permiso para eliminar cuadrilla', 'unidadseguimiento', 'deletecuadunid', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());


    INSERT INTO usuario.permiso (nombre, descripcion, recurso, accion, activo, fecha_creacion, fecha_modificacion, idmodulo)
		values ('Agregar Cuadrilla', 'Permiso para ageragr cuadrilla', 'unidadseguimiento', 'addcuadunid', true, now(), now(), id_modulo) RETURNING id INTO id_permiso;
	
	INSERT INTO usuario.perfil_permiso values (1, id_permiso, true, now(), now());
    

END $$;

