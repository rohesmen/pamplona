CREATE SEQUENCE "usuario"."permiso_id_seq";
ALTER TABLE "usuario"."permiso"
  ALTER COLUMN "id" SET DEFAULT nextval('"usuario".permiso_id_seq'::regclass);

SELECT setval('"usuario"."permiso_id_seq"', (select max(id) from usuario.permiso), true);