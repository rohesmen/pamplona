ALTER TABLE "usuario"."usuario"
  ADD COLUMN "supervisor" bool DEFAULT false,
  ADD COLUMN "cobratario" bool DEFAULT false;

update usuario.usuario
set cobratario = true
where id in (
	select idusuario from usuario.perfil_usuario where idperfil in (
		select id from usuario.perfil where nombre = 'cobratario'
	) and activo
);

update usuario.usuario
set supervisor = true
where id in (
	select idusuario from usuario.perfil_usuario where idperfil in (
		select id from usuario.perfil where nombre = 'supervisor'
	) and activo
);