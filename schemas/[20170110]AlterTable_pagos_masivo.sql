-- Agregar columna para registrar el id de pago
ALTER TABLE cliente.pagos_masivo ADD COLUMN "idrazon_social" INTEGER NOT NULL DEFAULT 0;

-- ---------------------------------------------------------------------------------------------
-- Creacion del Permiso para aplicar el Descuento por Monto en el modal de Cobro por Razon social
BEGIN;
insert into usuario.permiso(
	id,
	nombre,
	descripcion,
	recurso,
	accion,
	activo,
	fecha_modificacion,
	fecha_creacion,
	idmodulo)
values(
	47,
	'Descuento en Cobro por Razon Social',
	'Descuento en Cobro por Razon Social',
	'caja',
	'descuento_rsocial',
	true,
	now(),
	now(),
	3	
);
COMMIT;
-- Relacion entre el perfil y el permiso
BEGIN;
insert into usuario.perfil_permiso(
	idperfil,
	idpermiso,
	activo,
	fecha_creacion,
	fecha_modificacion)
values(
	1,
	47,
	true,
	now(),
	now()
);
COMMIT;