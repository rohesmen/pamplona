INSERT INTO cliente.tipo_descuento(
	id, nombre, descripcion, porcentaje, activo, fecha_creacion, fecha_modificacion)
	VALUES (1, 'Discapacitado', 'Discapacitado', 10, true, now(), now());

INSERT INTO cliente.tipo_descuento(
	id, nombre, descripcion, porcentaje, activo, fecha_creacion, fecha_modificacion)
	VALUES (2, 'Adulto mayor', 'Adulto mayor', 5, true, now(), now());

INSERT INTO cliente.tipo_descuento(
	id, nombre, descripcion, porcentaje, activo, fecha_creacion, fecha_modificacion)
	VALUES (3, 'Empleado', 'Empleado', 5, true, now(), now());


INSERT INTO cliente.tarifa_colonia(
	id, idcolonia, tarifa, activo, fecha_creacion, fecha_modificacion)
select id, id, 20, true, now(), now() from cliente.colonia;