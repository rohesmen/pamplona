﻿ALTER TABLE "cliente"."cliente"
  ADD COLUMN "id_tipo_comprobante" integer;;

ALTER TABLE "cliente"."cliente"
  ADD COLUMN "ruta_comprobante" text;

ALTER TABLE "cliente"."cliente"
  ADD COLUMN "id_tipo_identificacion" integer;;

ALTER TABLE "cliente"."cliente"
  ADD COLUMN "ruta_identificacion" text;

  ALTER TABLE "cliente"."cliente"
  ADD COLUMN "ruta_foto_predio" text;