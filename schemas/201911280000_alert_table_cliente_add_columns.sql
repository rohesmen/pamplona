ALTER TABLE "cliente"."cliente"
  ADD COLUMN "ultimo_mes_anio_pago" varchar(255);

ALTER TABLE "cliente"."cliente"
  ADD COLUMN "meses_a_pagar" int4;

ALTER TABLE "cliente"."cliente"
  ADD COLUMN "saldo" decimal;

ALTER TABLE "cliente"."cliente"
  ADD COLUMN "fecha_ultimo_pago" date;