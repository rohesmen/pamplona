INSERT INTO usuario.permiso(
	id, nombre, descripcion, recurso, accion, activo, fecha_modificacion, fecha_creacion, idmodulo)
	VALUES (  (select(max(id) + 1) from usuario.permiso) , 'Cambiar contraseña', 'Cambiar contraseña', 'users', 'changePassword', true, now(), now(), 2);
INSERT INTO usuario.permiso(
	id, nombre, descripcion, recurso, accion, activo, fecha_modificacion, fecha_creacion, idmodulo)
	VALUES (  (select(max(id) + 1) from usuario.permiso) , 'Reiniciar contraseña', 'Reiniciar contraseña', 'users', 'resetPassword', true, now(), now(), 2);

--agregar lospermioss al perfil dependiendo del id que les genero