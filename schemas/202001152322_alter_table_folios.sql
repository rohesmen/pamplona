ALTER TABLE "folios"."folios"
  ADD COLUMN "idinventario" int4,
  ADD COLUMN "cancelado" bool DEFAULT false,
  ADD COLUMN "observaciones" text,
  ADD COLUMN "tipo_cancelado" varchar(255),
  ADD COLUMN "liquidado" bool DEFAULT false,
  ADD COLUMN "fecha_asignacion" timestamp(0),
  ADD COLUMN "idusuario_asigno" int4;