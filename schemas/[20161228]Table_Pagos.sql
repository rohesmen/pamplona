-- Creacion de la tabla de cliente.pagos para guardar la relacion con historial_pago
-- DROP TABLE cliente.pagos;

CREATE TABLE cliente.pagos (
    idpago SERIAL PRIMARY KEY,
	fecha_pago timestamp without time zone DEFAULT now(),
	folio_factura varchar(64) DEFAULT NULL,
	referencia_bancaria varchar(32) DEFAULT NULL,
	idforma_pago INTEGER DEFAULT 0,
	activo boolean DEFAULT true,
    fecha_creacion timestamp without time zone DEFAULT now(),
    fecha_modificacion timestamp without time zone DEFAULT now()	   
);

-- Agregar columna para registrar el id de pago
ALTER TABLE cliente.historial_pago ADD COLUMN "idpago" INTEGER NOT NULL DEFAULT 0;
