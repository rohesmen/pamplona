-- Creacion de la tabla de cliente.pagosmasivo para guardar
-- entre el folio de un cobro masivo y los pagos realizados por cliente.
-- DROP TABLE cliente.pagos;
CREATE TABLE cliente.pagos_masivo (
    idpagomasivo SERIAL PRIMARY KEY,
	fecha_pago timestamp without time zone DEFAULT now(),
	folio_factura varchar(64) DEFAULT NULL,
	referencia_bancaria varchar(32) DEFAULT NULL,
	idforma_pago INTEGER DEFAULT 0,
	activo boolean DEFAULT true,
    fecha_creacion timestamp without time zone DEFAULT now(),
    fecha_modificacion timestamp without time zone DEFAULT now()	   
);

-- Agregar columna para registrar el id de pago
ALTER TABLE cliente.pagos ADD COLUMN "idpagomasivo" INTEGER NOT NULL DEFAULT 0;

-- Creacion de Permisos para el boton de cobro por razon social
-----------------------------------------------------------------------------
BEGIN;
insert into usuario.permiso(
	id,
	nombre,
	descripcion,
	recurso,
	accion,
	activo,
	fecha_modificacion,
	fecha_creacion,
	idmodulo)
values(
	45,
	'Cobrar Recoleccion por Razon Social',
	'Cobrar Recoleccion por Razon Social',
	'caja',
	'cobrar_rsocial',
	true,
	now(),
	now(),
	3	
);
COMMIT;
-- Relacion entre el perfil y el permiso
BEGIN;
insert into usuario.perfil_permiso(
	idperfil,
	idpermiso,
	activo,
	fecha_creacion,
	fecha_modificacion)
values(
	1,
	45,
	true,
	now(),
	now()
);
COMMIT;