
--id de transaccion.pagos
ALTER TABLE cliente.pagos
   ADD COLUMN idpago_aux integer;

--id de transaccion.pagos
ALTER TABLE cliente.historial_pago
   ADD COLUMN idpago_aux integer;

--id de transaccion.historial_pago
ALTER TABLE cliente.historial_pago
   ADD COLUMN idhistorial_aux integer;
