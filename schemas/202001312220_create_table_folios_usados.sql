CREATE TABLE "folios".folios_usados (
  "id" serial,
	idpago integer,
  "folio" int4,
  "idcobratario" int4,
	fecha_cobro timestamp(6),
	cantidad float4,
  "observaciones" text,
	"idusuario" int4,
  "activo" bool DEFAULT true,
  "fecha_creacion" timestamp(6) DEFAULT now(),
  "fecha_modificacion" timestamp(6) DEFAULT now(),
  CONSTRAINT "pku_folios_usados" PRIMARY KEY ("id")
)