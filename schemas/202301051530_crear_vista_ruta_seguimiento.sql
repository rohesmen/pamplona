

--DROP VIEW monitoreo.view_ruta_recoleccion;
CREATE OR REPLACE VIEW monitoreo.view_ruta_recoleccion
    AS
    SELECT
        v.id,
        v.idruta,
		upper(r.nombre) AS ruta,
        v.dia AS iddia,
        upper(d.dia) AS dia,
        v.tipo_recoleccion AS idtipo_recoleccion,
        upper(t.tipo) AS tipo_recoleccion,
        v.idturno,
        upper(n.nombre) AS turno,
        v.activo,
        v.fechacreacion,
        v.fechamodificacion,
        CASE WHEN v.activo THEN 'SI' ELSE 'NO' END AS activo_f,
        CASE WHEN v.fechacreacion IS NOT NULL THEN to_char(v.fechacreacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_creacion_f,
        CASE WHEN v.fechamodificacion IS NOT NULL THEN to_char(v.fechamodificacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_modificacion_f
    FROM monitoreo.ruta_recoleccion v
    LEFT JOIN monitoreo.ruta r ON r.id = v.idruta
    LEFT JOIN monitoreo.dias_recoleccion d ON d.sigla = v.dia
    LEFT JOIN monitoreo.tipo_recoleccion t ON t.sigla = v.tipo_recoleccion
    LEFT JOIN monitoreo.turno n ON n.id = v.idturno;


--DROP VIEW monitoreo.view_ruta;
CREATE OR REPLACE VIEW monitoreo.view_ruta
    AS
    SELECT
        v.id,
		upper(v.nombre) AS nombre,
        v.the_geom,
        v.activo,
        v.fecha_creacion,
        v.fecha_modificacion,
        CASE WHEN v.activo THEN 'SI' ELSE 'NO' END AS activo_f,
        CASE WHEN v.fecha_creacion IS NOT NULL THEN to_char(v.fecha_creacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_creacion_f,
        CASE WHEN v.fecha_modificacion IS NOT NULL THEN to_char(v.fecha_modificacion, 'DD/MM/YYYY  HH:MI:SS PM') ELSE '' END AS fecha_modificacion_f
    FROM monitoreo.ruta v;

