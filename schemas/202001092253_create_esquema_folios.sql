CREATE SCHEMA "folios";

CREATE TABLE folios.rango_folio (
  id serial,
	serie varchar,
	num_ini integer default 0,
	num_fin integer default 0,
	usados integer default 0,
	disponibles integer default 0,
	total integer default 0,
	idcobratario integer,
	idusuario integer,
  activo bool DEFAULT true,
  fecha_creacion timestamp WITHOUT time zone DEFAULT now(),
  fecha_modificacion timestamp WITHOUT time zone DEFAULT now(),
  CONSTRAINT pku_rango_folio PRIMARY KEY (id)
);

CREATE TABLE folios.folios (
  id serial,
	idrango_folio integer,
	idpago integer,
	serie varchar,
	folio integer,
	serie_folio varchar,
	idcobratario integer,
	idusuario integer,
  activo bool DEFAULT true,
  fecha_creacion timestamp WITHOUT time zone DEFAULT now(),
  fecha_modificacion timestamp WITHOUT time zone DEFAULT now(),
  CONSTRAINT pku_folios PRIMARY KEY (id)
);

with m as (
 INSERT INTO "usuario"."modulo"("idgrupomodulo", "siglas", "nombre", "descripcion", "iconomodulo", "directorio", "activo", "fechamodificacion", "fechacreacion", "orden")
 select 1, 'MODFOLIOS', 'Folios', 'Folios', 'fa-ticket', '/folios', 't', now(), now(), 5 RETURNING id
),
p as (
	INSERT INTO "usuario"."permiso"(id, "nombre", "descripcion", "recurso", "accion", "activo", "fecha_modificacion", "fecha_creacion", "idmodulo")
	select (select max(id) + 1 from usuario.permiso ),
	'Folios', 'Folios', 'folios', 'index', 't', now(), now(), m.id from m RETURNING id
),
pp as (
	INSERT INTO "usuario"."perfil_permiso"("idperfil", "idpermiso", "activo", "fecha_creacion", "fecha_modificacion")
	select 1, p.id, true, now(), now() from p
)
select * from m, p;

create or replace view folios.view_folios as
select rf.id, rf.serie, rf.num_ini, rf.num_fin, rf.idcobratario, u.usuario cobratario, usados, disponibles, total, rf.activo
from folios.rango_folio rf
left join usuario.usuario u on rf.idcobratario = u.id;