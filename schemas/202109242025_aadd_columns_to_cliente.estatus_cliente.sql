
ALTER TABLE cliente.estatus_cliente
    ADD COLUMN iscobro_app boolean DEFAULT true;

UPDATE cliente.estatus_cliente
SET iscobro_app = false
WHERE sigla = 'CF';
