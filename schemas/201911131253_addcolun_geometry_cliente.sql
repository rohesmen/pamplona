ALTER TABLE cliente.cliente ADD COLUMN the_geom geometry(Multipolygon, 32616);

update cliente.cliente c
set the_geom = p.geom
from (
 select folio, geom from geodata.predios
) p
where c.folio_catastral = p.folio