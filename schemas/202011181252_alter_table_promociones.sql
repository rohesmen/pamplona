ALTER TABLE "comun"."promociones"
  ADD COLUMN "fecini_vigencia" date DEFAULT CURRENT_DATE,
  ADD COLUMN "fecfin_vigencia" date DEFAULT (now() + '30 days'::interval);

ALTER TABLE "comun"."promociones"
  ADD COLUMN "anio" int4,
  ADD COLUMN "meses" varchar,
  ADD COLUMN "tipo" varchar(255);

COMMENT ON COLUMN "comun"."promociones"."anio" IS 'año al cual se aplicara el descuento';

COMMENT ON COLUMN "comun"."promociones"."meses" IS 'meses a los cuales se aplicara el descuento separados por comas';

COMMENT ON COLUMN "comun"."promociones"."tipo" IS 'tipo de promocion, anual, normal';