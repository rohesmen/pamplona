ALTER TABLE "cliente"."pagos"
  ADD COLUMN "impresiones" integer;

CREATE TABLE comun.bitacora_impresiones (
  id serial,
  idcliente integer NOT NULL,
  idpago integer not null,
  idusuario integer NOT NULL,
	origen varchar default 'APP',
  activo boolean DEFAULT true,
  fecha_creacion timestamp without time zone DEFAULT now(),
  fecha_modificacion timestamp without time zone DEFAULT now(),
  CONSTRAINT bitacora_impresiones_pkey PRIMARY KEY (id),
  CONSTRAINT fk_bitacora_impresiones_cliente FOREIGN KEY (idcliente)
      REFERENCES cliente.cliente (id_cliente) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_bitacora_impresiones_pago FOREIGN KEY (idpago)
      REFERENCES cliente.pagos (idpago) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_bitacora_impresiones_usuario FOREIGN KEY (idusuario)
      REFERENCES usuario.usuario (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);