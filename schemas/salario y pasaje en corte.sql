ALTER TABLE corte.historial_corte
   ADD COLUMN monto_salario numeric;
COMMENT ON COLUMN corte.historial_corte.monto_salario
  IS 'Monto del salario';

ALTER TABLE corte.historial_corte
   ADD COLUMN monto_pasaje numeric;
COMMENT ON COLUMN corte.historial_corte.monto_pasaje
  IS 'Monto del pasaje';