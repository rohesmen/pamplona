ALTER TABLE "monitoreo"."unidad"
    ADD COLUMN "idusuario" int4,
  ADD COLUMN "idusuario_desactivo" int4,
  ADD CONSTRAINT "fk_idusuario" FOREIGN KEY ("idusuario") REFERENCES "usuario"."usuario" ("id"),
  ADD CONSTRAINT "fk_idusuario_desactivo" FOREIGN KEY ("idusuario_desactivo") REFERENCES "usuario"."usuario" ("id");

ALTER TABLE "monitoreo"."recoleccion"
    ADD COLUMN "idusuario" int4,
  ADD COLUMN "idusuario_desactivo" int4,
  ADD CONSTRAINT "fk_idusuario" FOREIGN KEY ("idusuario") REFERENCES "usuario"."usuario" ("id"),
  ADD CONSTRAINT "fk_idusuario_desactivo" FOREIGN KEY ("idusuario_desactivo") REFERENCES "usuario"."usuario" ("id");