create or replace view folios.view_folios as
select rf.id, rf.serie, rf.num_ini, rf.num_fin, rf.idcobratario, u.usuario cobratario, porliquidar, disponibles, (rf.porliquidar + rf.disponibles) AS transito, rf.activo
from folios.rango_folio rf
left join usuario.usuario u on rf.idcobratario = u.id