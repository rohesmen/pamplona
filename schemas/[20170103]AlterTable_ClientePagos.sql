-- Agregar columna para registrar el id de pago
ALTER TABLE cliente.pagos ADD COLUMN "idcliente" INTEGER NOT NULL DEFAULT 0;

-- asignamos un valor de referencia que exista para el cliente y evitar error de referencia integral
--update cliente.pagos set idcliente = 1;

--Agregamos la llave foranea
ALTER TABLE cliente.pagos ADD CONSTRAINT fk_idcliente FOREIGN KEY (idcliente) REFERENCES cliente.cliente(id_cliente);