create or replace view servicio.view_serviciosdetalles as
SELECT sd.id,
    sd.idorden_servicio,
    sd.costo,
    sd.costo_final,
    sd.activo,
    sd.idservicio,
    s.id AS clave_servicio,
    s.nombre,
    s.idgrupo,
    o.idestado,
	e.clave as clave_estado,
	e.nombre as nombre_estado
   FROM servicio.orden_servicio_detalle sd
     LEFT JOIN servicio.servicios s ON s.id = sd.idservicio
     LEFT JOIN servicio.orden_servicio o ON o.id = sd.idorden_servicio
	 LEFT JOIN servicio.estado_orden_servicio e ON e.id = o.idestado;