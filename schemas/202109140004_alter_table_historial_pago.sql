ALTER TABLE "cliente"."historial_pago" 
  ALTER COLUMN "mes" TYPE int4 USING "mes"::int4,
  ALTER COLUMN "anio" TYPE int4 USING "anio"::int4;