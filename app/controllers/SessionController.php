<?php
namespace Vokuro\Controllers;

use Vokuro\Forms\LoginForm;
use Vokuro\Forms\SignUpForm;
use Vokuro\Forms\ForgotPasswordForm;
use Vokuro\Auth\Exception as AuthException;
use Vokuro\Models\Users;
use Vokuro\Models\ResetPasswords;

use Phalcon\Mvc\View;

/**
 * Controller used handle non-authenticated session actions like login/logout, user signup, and forgotten passwords
 */
class SessionController extends ControllerBase
{

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function initialize()
    {
        //$this->view->setTemplateBefore('public');
    }

    public function indexAction()
    {

    }

    /**
     * Allow a user to signup to the system
     */
    public function signupAction()
    {
        $form = new SignUpForm();

        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost()) != false) {

                $user = new Users();

                $user->assign(array(
                    'name' => $this->request->getPost('name', 'striptags'),
                    'email' => $this->request->getPost('email'),
                    'password' => $this->security->hash($this->request->getPost('password')),
                    'profilesId' => 2
                ));

                if ($user->save()) {
                    return $this->dispatcher->forward(array(
                        'controller' => 'index',
                        'action' => 'index'
                    ));
                }

                $this->flash->error($user->getMessages());
            }
        }

        $this->view->form = $form;
    }

    /**
     * Starts a session in the admin backend
     */
    public function loginAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $form = new LoginForm();

        try {

            if (!$this->request->isPost()) {

                /*if ($this->auth->hasRememberMe()) {
                    return $this->auth->loginWithRememberMe();
                }*/
            } else {

                if ($form->isValid($this->request->getPost()) == false) {
                    foreach ($form->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                } else {

                    $this->auth->check(array(
                        'user' => $this->request->getPost('user'),
                        'password' => $this->request->getPost('password'),
                        /*'remember' => $this->request->getPost('remember')*/
                    ));



//                    if (!$this->acl->isAllowedUser('militants', 'index') and !$this->acl->isAllowedUser('persons', 'index') and !$this->acl->isAllowedUser('dashboard', 'index')) {
////                    echo "ambos";
//                        $dispatcher->forward(array(
//                            'controller' => 'error',
//                            'action' => 'index'
//                        ));
//                        return false;
//                    }
//                    if ($this->acl->isAllowedUser('attendance', 'index')) {
//                        return $this->response->redirect('/attendance');
//                    } else if ($this->acl->isAllowedUser('militants', 'index')) {
//                        return $this->response->redirect('/militants');
//                    } else if ($this->acl->isAllowedUser('persons', 'index')) {
//                        return $this->response->redirect('/persons');
//                    }else if ($this->acl->isAllowedUser('dashboard', 'index')) {
//                        return $this->response->redirect('/dashboard');
//                    }else{
//                        return $this->response->redirect('/error');
//                    }

                    return $this->response->redirect('/');
                }
            }
        } catch (AuthException $e) {
            $this->flash->error($e->getMessage());
        }

        $this->view->form = $form;
    }

    /**
     * Shows the forgot password form
     */
    public function forgotPasswordAction()
    {
        $form = new ForgotPasswordForm();

        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost()) == false) {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            } else {

                $user = Users::findFirstByEmail($this->request->getPost('email'));
                if (!$user) {
                    $this->flash->success('There is no account associated to this email');
                } else {

                    $resetPassword = new ResetPasswords();
                    $resetPassword->usersId = $user->id;
                    if ($resetPassword->save()) {
                        $this->flash->success('Success! Please check your messages for an email reset password');
                    } else {
                        foreach ($resetPassword->getMessages() as $message) {
                            $this->flash->error($message);
                        }
                    }
                }
            }
        }

        $this->view->form = $form;
    }

    /**
     * Closes the session
     */
    public function logoutAction()
    {
        $this->auth->remove();
        return $this->response->redirect('session/login');
    }
}
