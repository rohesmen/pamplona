<?php
namespace Vokuro\Controllers;
use Vokuro\DT\SSPGEO;
use Vokuro\GenericSQL\GenericSQL;
use Vokuro\Models\Cuadrilla;
use Vokuro\Models\CuadrillaRecolector;
use Vokuro\Models\Recolector;
use Vokuro\Models\BitacoraCambios;

/**
 * Display the default index page.
 */
class CuadrillaseguimientoController extends ControllerBase {

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function indexAction() {
        $this->view->setTemplateBefore('public');

        $conacciones = 'no';
        if($this->acl->isAllowedUser('cuadrillaseguimiento', 'edit') or $this->acl->isAllowedUser('cuadrillaseguimiento', 'deactivate') or $this->acl->isAllowedUser('cuadrillaseguimiento', 'activate') or $this->acl->isAllowedUser('cuadrillaseguimiento', 'info')){
            $conacciones = 'si';
        }
        $this->view->setVar('coacciones', $conacciones);

        $recolectores = Recolector::find([
            "activo = true",
            "order" => "nombres, apepat, apemat"
        ]);
        $this->view->setVar('recolectores', $recolectores);

    }

    public function buscarAction(){
        $request = $this->request;
        $response = $this->response;

        $columns = array(
            array( 'db' => '', 'dt' => 0,
                'formatter' => function( $d, $row ) {
                    $buttons = '';
                    if($this->acl->isAllowedUser('cuadrillaseguimiento', 'info')){
                        $buttons .= '<button class="btn btn-primary btn-sm cuadrillaseguimiento-info" data-id="'.$row["id"].'" type="button" title="Consultar">
                            <i class="fa fa-info"></i>
                        </button> ';
                    }
                    if($this->acl->isAllowedUser('cuadrillaseguimiento', 'edit') && $row["activo"] === true){
                        $buttons .= '<button class="btn btn-primary btn-sm cuadrillaseguimiento-edit" data-id="'.$row["id"].'" type="button" title="Editar">
                            <i class="fa fa-pencil"></i>
                        </button> ';
                    }
                    if($this->acl->isAllowedUser('cuadrillaseguimiento', 'deactivate') && $row["activo"] === true){
                        $buttons .= '<button class="btn btn-danger btn-sm cuadrillaseguimiento-delete" data-id="'.$row["id"].'" type="button" title="¿Desea desactivar?">
                            <i class="fa fa-times"></i>
                        </button> ';
                    }
                    if($this->acl->isAllowedUser('cuadrillaseguimiento', 'activate') && $row["activo"] === false){
                        $buttons .= '<button class="btn btn-info btn-sm cuadrillaseguimiento-active" data-id="'.$row["id"].'" type="button" title="¿Desea activar?">
                            <i class="fa fa-check"></i>
                        </button> ';
                    }
                    return $buttons;
                }
            ),
            array( 'db' => '', 'dt' => 1),
            array( 'db' => 'activo', 'datatype' => 'boolean', 'dt' => 2,
                'formatter' => function( $d, $row ) {
                    $vigente = $d ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                        : '<i class="fa fa-remove" style="color: red" title="Inactivo"></i>';
                    return $vigente;
                }
            ),
            array( 'db' => 'id', 'dt' => 3, 'datatype' => 'number'),
            array( 'db' => 'nombre', 'dt' => 4),
            array( 'db' => 'fecha_creacion_f', 'datatype' => 'date', 'dt' => 5),
            array( 'db' => 'fecha_modificacion_f', 'datatype' => 'date', 'dt' => 6),
            array( 'db' => 'activo', 'datatype' => 'boolean', 'dt' => 7),
        );

        $data = SSPGEO::complex_geo($this->request->get(), "monitoreo.view_cuadrilla", "id", $columns);

        $response->setContent(json_encode($data));
        return $response;
    }

    public function saveAction(){

        $rawBody = $this->request->getJsonRawBody();

        $isExtern = $rawBody->isExtern;
        $id = $rawBody->id;
        $nombre = mb_strtoupper(trim($rawBody->nombre));
        $cuadrillarecolector = $rawBody->cuadrillarecolector;

        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];

        if($nombre == ""){
            $this->response->setStatuscode(400, "No se ingresó nombre de la cuadrilla");
            return $this->response;
        }

        $sql = "replace(upper(nombre), ' ', '') = replace(upper('$nombre'), ' ', '')";

        if(intval($id) > 0){
            $sql .= " AND id <> $id";
        }

        $busqueda = Cuadrilla::findFirst($sql);

        if($busqueda){
            $this->response->setStatuscode(409, "Ya existe un registro ".($busqueda->activo ? "activo" : "inactivo")." con el mismo nombre");
            return $this->response;
        }

        $dataOrigin = null;
        $accion = "CREACION";

        if(intval($id) > 0){
            $data = Cuadrilla::findFirstById($id);
            $dataOrigin = json_encode($data);
            $accion = "EDICION";
        }
        else{
            $data = new Cuadrilla();
            $data->fecha_creacion = date("c");
        }

        $data->idusuario = $idUser;
        $data->activo = true;
        $data->nombre = $nombre;
        $data->fecha_modificacion = date("c");

        $this->db->begin();
        if($data->save()){

            $data->refresh();

            $dataB = new BitacoraCambios();
            $dataB->identificador = $data->id;
            $dataB->modulo = 'CUADRISEG';
            $dataB->idusuario = $idUser;
            $dataB->tabla = "monitoreo.cuadrilla";
            $dataB->cambios = json_encode($data);
            $dataB->original = $dataOrigin;
            $dataB->accion = $accion . " MONITOREO CUADRILLA";

            if($dataB->save()){
                foreach ($cuadrillarecolector as $valor) {

                    $lSave = false;
                    $detalleOrigin = null;
                    $detalleAccion = "CREACION";

                    //$this->logger->info("valor: ".json_encode($valor));

                    if($valor[6] && $valor[6] == true && intval($valor[7]) == -1) {
                        $detalle = new CuadrillaRecolector();
                        $detalle->idcuadrilla = $data->id;
                        $detalle->idrecolector = $valor[4];
                        $detalle->ischofer = $valor[5];
                        $detalle->idusuario = $idUser;
                        $detalle->activo = true;
                        $detalle->fecha_creacion = 'NOW()';
                        $detalle->fecha_modificacion = 'NOW()';
                        $lSave = true;
                    }else if((!$valor[6]) && $valor[6] == false && intval($valor[7]) > 0) {
                        $detalleAccion = "ELIMINACION";
                        $detalle = CuadrillaRecolector::findFirstById($valor[7]);
                        $detalleOrigin = json_encode($detalle);
                        $detalle->idusuario = $idUser;
                        $detalle->idusuario_desactivo = $idUser;
                        $detalle->activo = false;
                        $detalle->fecha_modificacion = 'NOW()';
                        $lSave = true;
                    }else if(($valor[6]) && $valor[6] == true && $valor[8] == true && intval($valor[7]) > 0) {
                        $detalleAccion = "EDICION";
                        $detalle = CuadrillaRecolector::findFirstById($valor[7]);
                        $detalleOrigin = json_encode($detalle);
                        $detalle->idusuario = $idUser;
                        $detalle->ischofer = $valor[5];
                        $detalle->fecha_modificacion = 'NOW()';
                        $lSave = true;
                    }

                    if($lSave) {
    
                        if(!$detalle->save()){
                            $this->db->rollback();
                            foreach ($detalle->getMessages() as $message) {
                                $this->logger->info("(create-delete-update-detalle-cuadrillarecolectorseguimiento): " . $message);
                            }
                            $mensaje = "Ocurrió un error al crear/eliminar/editar cudrilla-recolector.";
                            $this->logger->error($mensaje);
                            $this->response->setStatusCode(500, $mensaje);
                            return $this->response;
                        }

                        $detalle->refresh();

                        $dataB = new BitacoraCambios();
                        $dataB->identificador = $detalle->id;
                        $dataB->modulo = 'CUADRISEG';
                        $dataB->idusuario = $idUser;
                        $dataB->tabla = "monitoreo.cuadrilla_recolector";
                        $dataB->cambios = json_encode($detalle);
                        $dataB->original = $detalleOrigin;
                        $dataB->accion = $detalleAccion . " MONITOREO CUADRILLA RECOLECTOR";
    
                        if (!$dataB->save()) {
                            $this->db->rollback();
                            foreach ($dataB->getMessages() as $message) {
                                $this->logger->info("(save-bitacora-detalle-cuadrillarecolectorseguimiento): " . $message);
                            }
                            $mensaje = "Ocurrió un error al guardar la bitacora de cudrilla-recolector.";
                            $this->logger->error($mensaje);
                            $this->response->setStatusCode(500, $mensaje);
                            return $this->response;
                        }
                    }
                }
                $this->db->commit();
                $this->response->setStatusCode(200, "La información se guardo correctamente");
                if($isExtern){
				    $this->response->setContent(json_encode($this->getdata($data->id)));
                }
            }else{
                $this->db->rollback();
                foreach ($dataB->getMessages() as $message) {
                    $this->logger->error("save-bitacora-cuadrillaseguimiento: ".$message->getMessage());
                }
                $mensaje = "Ocurrió un error al guardar la bitacora.";
                $this->logger->error($mensaje);
                $this->response->setStatusCode(500, $mensaje);
            }            
        }else{
            $this->db->rollback();
            foreach ($data->getMessages() as $message) {
                $this->logger->error("save-cuadrillaseguimiento: ".$message->getMessage());
            }
            $mensaje = "Ocurrió un error al guardar el registro.";
            $this->logger->error($mensaje);
            $this->response->setStatusCode(500, $mensaje);
        }
        return $this->response;
    }

    public function deleteAction($id, $activo){
        if(!empty($id)){
            $d = Cuadrilla::findFirstById($id);
            if ($d) {

                $identity = $this->auth->getIdentity();
                $idUser = $identity["id"];

                $this->db->begin();

                $dataOrigin = json_encode($d);
                $accion = "DESACTIVACION";
                if($activo === true || $activo == "true"){
                    $accion = "ACTIVACION";
                    $d->idusuario_desactivo = null;
                } else{
                    $d->idusuario_desactivo = $idUser;
                }
                $d->idusuario = $idUser;
                $d->activo = $activo;
                $d->fecha_modificacion = date("c");
                if(!$d->save()){
                    $this->db->rollback();
                    foreach ($d->getMessages() as $message) {
                        $this->logger->info("(delete-cuadrillaseguimiento): " . $message);
                    }
                    $mensaje = "Ocurrió un error al guardar el registro.";
                    $this->logger->error($mensaje);
                    $this->response->setStatusCode(500, $mensaje);
                }else{
                    $d->refresh();
                    $dataB = new BitacoraCambios();
                    $dataB->identificador = $d->id;
                    $dataB->modulo = 'CUADRISEG';
                    $dataB->idusuario = $idUser;
                    $dataB->tabla = "monitoreo.cuadrilla";
                    $dataB->cambios = json_encode($d);
                    $dataB->original = $dataOrigin;
                    $dataB->accion = $accion . " MONITOREO CUADRILLA";
                    if (!$dataB->save()) {
                        $this->db->rollback();
                        foreach ($dataB->getMessages() as $message) {
                            $this->logger->info("(save-bitacora-delete-cuadrillaseguimiento): " . $message);
                        }
                        $mensaje = "Ocurrió un error al guardar la bitacora.";
                        $this->logger->error($mensaje);
                        $this->response->setStatusCode(500, $mensaje);
                    }
                    $this->db->commit();
                }
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(400, "Bad Request");
        }
        return $this->response;
    }

    public function getAction($id){
        if($this->request->isGet()){
            if(intval($id) > 0){
                $sql = "SELECT id, idrecolector, recolector, tipo, ischofer
                FROM monitoreo.view_cuadrilla_recolector
                WHERE activo AND idcuadrilla = $id";
                $data = GenericSQL::getBySQL($sql);
                $resp = array();
                foreach ($data as $i){
                    array_push($resp, array(
                        "",
                        '<button class="btn btn-sm btn-danger delete_cuadrilla_recoleccion" data-id="'.$i->id.'" title="¿Desea eliminar?"><i class="fa fa-remove"></i></button>',
                        $i->recolector,
                        $i->tipo,
                        $i->idrecolector,
                        $i->ischofer,
                        true,
                        $i->id,
                        false
                    ));
                }
                $this->response->setContent(json_encode($resp));
            }else{
                $this->response->setStatusCode(400);
            }
        }else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    public function getdata($id){
        $sqlCua = "select c.*
        from monitoreo.cuadrilla c
        where c.id = $id";
        $cuad = GenericSQL::getBySQL($sqlCua);
        $recs = null;
        if(count($cuad)){
            $cuad = $cuad[0];
            $sqlRec = "select r.*, cr.ischofer
            from monitoreo.cuadrilla_recolector cr 
            join monitoreo.recolector r on cr.idrecolector = r.id
            where cr.idcuadrilla = $cuad->id and r.activo and cr.activo
            order by cr.ischofer desc, r.apepat, r.apemat, r.nombres";
            $recs = GenericSQL::getBySQL($sqlRec);
        }
        return array(
            "data" => $cuad,
            "recolectores" => $recs
        );
    }

}
