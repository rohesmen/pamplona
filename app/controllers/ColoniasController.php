<?php
/**
 * Clase controller para el Catalogo de colonias
 * @author [russel] <[<email address>]>
 */
namespace Vokuro\Controllers;

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Vokuro\Forms\ColoniasForm;
// use Vokuro\Models\Profiles;
// use Vokuro\Models\UserProfiles;
use Vokuro\Models\Colonias;
use Vokuro\Models\MunicipiosYucatan;
use Phalcon\Mvc\View;

class ColoniasController extends ControllerBase
{

    /**
     * [indexAction  Default action. Set the public layout (layouts/public.volt)]
     * @author [russel] <[<email address>]>
     * @return [view] [Vista para Colonias]
     */
    public function indexAction()
    {
        $municipios = MunicipiosYucatan::find([
            "activo = true"
        ]);

        $this->view->setTemplateBefore('public');
        $this->view->setVar('doSearch', $this->acl->isAllowedUser('colonias', 'search'));
        // $this->view->setVar('colonias', Colonias::findByActivo());
        $this->view->setVar("municipios", $municipios);
    }

    public function searchAction(){
        $view = clone $this->view;
        $this->view->disable();

        $view->start();
        if ($this->request->isPost() == true) {
            $rawBody = $this->request->getJsonRawBody();
            $value = strtolower(trim($rawBody->valor));
            $filter = strtolower(trim($rawBody->filtro));
            $results = $this->filterSearch($value, $filter);
            
            $view->setVar('colonias', $results);
        }
        else{
            $view->setVar('NotImplemented', true);
        }

        $datosColonia = $this->auth->getIdentity();
        $localidad = $datosColonia['localidad'];
        $alias = $datosColonia['alias'];
        if($localidad != null and $alias != null){
            $view->setVar('pageLength', -1);
        }
        else{
            $view->setVar('pageLength', 15);
        }

        $hasButtons = true;
        $buttons = array();

        if($this->acl->isAllowedUser('colonias', 'exportExcel')){
            array_push($buttons,'{"text": "<i class='."\'fa fa-file-excel-o\'".'></i>", "title": "Colonias", "extend": "excel","exportOptions": {"columns": ":visible"}}');
            $hasButtons = true;
        }
        if($this->acl->isAllowedUser('colonias', 'exportPdf')){
            array_push($buttons,'{"text": "<i class='."\'fa fa-file-pdf-o\'".'></i>", "title": "Colonias", "extend": "pdf","exportOptions": {"columns": ":visible"}}');
            $hasButtons = true;
        }
        if($this->acl->isAllowedUser('colonias', 'print')){
            $jsonPrint = '{"text": "<i class='."\'fa fa-print\'".'></i>", "title": "Colonias", "extend": "print","exportOptions": {"columns": ":visible"}}';
            array_push($buttons, $jsonPrint);

            $hasButtons = true;
        }
        array_push($buttons,'{"text": "<i class='."\'fa fa-gear\'".'></i>", "extend": "colvis"}');

        $view->setVar('hasButtons', $hasButtons);
        $view->setVar('buttons', $buttons);
        $view->setVar('edit', $this->acl->isAllowedUser("colonias","edit"));
        $view->setVar('info', $this->acl->isAllowedUser("colonias","info"));
        $view->setVar('delete', $this->acl->isAllowedUser("colonias","delete"));

        $view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $view->render('colonias', 'search');
        $view->finish();

        $this->response->setContent($view->getContent());
        return $this->response;
    }

    private function filterSearch($value, $filter){
        $valor = preg_replace('/\s+/', ' ',$value);

        $datosColonia = $this->auth->getIdentity();
        $activo = $datosColonia['activo'];
        $nombre = $datosColonia['nombre'];
        $alias = $datosColonia['alias'];
        $results = array();

        if($filter == -1) {
            // if ($activo == true) {
                $results = Colonias::findAll();
            // } elseif ($alias != null and $nombre != null) {
                // $results = Colonias::findByAliasAndNombre($this->getNameColumnId($nombre), $nombre);
            // }
        }elseif($filter == 'cla' and $value != ''){
            try{
                $busqueda = intval($valor);
                // if ($activo == true) {
                    $results = Colonias::findFromColumnAndValueNumber("id", $valor);
                // } elseif ($alias != null and $nombre != null) {
                    // $results = Colonias::findFromColumnAndValueNumber("id", $valor,$this->getNameColumnId($alias), $nombre);
                // }
            }
            catch(Exception $e){}
        }elseif($filter == 'nom' and $valor != ''){
            // if($activo == true){
                $results = $this->findByNombre($valor, true);
            // }
            // else{
                // $results = $this->findByNombre($valor, false, $this->getNameColumnId($alias), $nombre);
            // }
        }elseif($filter == 'mun' and $value != ''){
            $results = Colonias::find([ "cve_mun = $value"]);
        }
        elseif($filter == 'loc' and $valor != ''){
            // if($activo == true){
                $results = Colonias::findFromColumnAndValueString("localidad", $valor);
            // }
            // else{
                // $results = Colonias::findFromColumnAndValueString("nombre",$valor, $this->getNameColumnId($alias), $nombre);
            // }
        }

        $rows = array();
        if(count($results) > 0){
            foreach($results as $t) {
                $municipio = MunicipiosYucatan::findFirst([
                    "cve_mun = $t->cve_mun"
                ]);
                // if($t->id == 1)
                //     continue;
                $row = new \stdClass();
                $row->id = $t->id;
                $row->nombre = strtoupper(trim($t->nombre)) . ' - ' . $municipio->nombre;
                $row->alias = $t->alias;
                $row->activo = $t->activo;
                $row->creacion = date("d/m/Y H:i A", strtotime(str_replace('/', '-',$t->fecha_creacion)));
                $row->modificacion = date("d/m/Y H:i A", strtotime(str_replace('/', '-',$t->fecha_modificacion)));
                $row->localidad = $t->localidad;
                $row->idunidadhabi = $t->idunidadhabi;
                $row->monto = $t->monto;
                array_push($rows,$row);
            }
        }
        return $rows;
    }

    public function getAction($id){
        $this->view->disable();
        if ($this->request->isGet() == true) {
            $colonia = Colonias::findFirstById($id);

            if($colonia != false){

                $row = new \stdClass();
                $row->ok = true;
                $row->status = 200;
                $row->id = $colonia->id;
                $row->nombre = $colonia->nombre;
                $row->alias = $colonia->alias;
                $row->localidad = $colonia->localidad;
                $row->idunidadhabi = $colonia->idunidadhabi;
                $row->monto = $colonia->monto;
                $row->activo = $colonia->activo;
                $row->idmunicipio = $colonia->cve_mun;
                //$this->logger->info('123 #'. $colonia->cve_mun);

                $this->response->setContent(json_encode($row));
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    /**
     * [addAction Crea una nueva Colonia]
     * @author [russel] <[<email address>]>
     */
    public function saveAction()
    {
        $this->view->disable();
        if ($this->request->isPost() == true){
            $rawBody = $this->request->getJsonRawBody();
            $id = ($rawBody->id != null and $rawBody->id != "") ? intval($rawBody->id) : (null);
            $nombre = ($rawBody->nombre != null and $rawBody->nombre != "") ? $rawBody->nombre : "";
            $alias = ($rawBody->alias != null and $rawBody->alias != "") ? $rawBody->alias : "";
            $localidad = ($rawBody->localidad != null and $rawBody->localidad != "") ? $rawBody->localidad : "";
            $idunidadhabi = ($rawBody->idunidadhabi != null and $rawBody->idunidadhabi != "") ? intval($rawBody->idunidadhabi) : null;
            $monto = ($rawBody->monto != null and $rawBody->monto != "") ? $rawBody->monto : null;
            $activo = $rawBody->activo;
            $idmunicipio = $rawBody->idmunicipio;

            $municipioF = MunicipiosYucatan::findFirst([
                "cve_mun = $idmunicipio"
            ]);
            $municipio = $municipioF->nombre;
            //$this->logger->info(' ' .$municipio);
            $saveResult = new \stdClass();
            $now = date("c");
            if($id != null){
                $colonia = Colonias::findFirstById($id);
                if($colonia != false){
                    $colonia->nombre = $nombre;
                    $colonia->alias = $alias;
                    $colonia->fecha_modificacion = $now;
                    $colonia->localidad = $localidad;
//                    $colonia->idunidadhabi = $idunidadhabi;
                    $colonia->monto = $monto;
                    $colonia->ismarginado = floatval($monto) > 0 ? false : true;
                    $colonia->activo = $activo;
                    $colonia->cve_mun = $idmunicipio;

                    if(!$colonia->save()){

                        foreach ($colonia->getMessages() as $message) {
                            $this->logger->info("(edit-colonia): " . $message);
                        }
                        $this->response->setStatusCode(500, "Internal Server Error");
                    }
                    else{
                        $saveResult->response = true;
                        $saveResult->ok = true;
                        $saveResult->status = 200;
                        $saveResult->id = $colonia->id;
                        $saveResult->edit = true;
                        $saveResult->nombre = strtoupper(trim($colonia->nombre) .' - ' . $municipio);
                        $saveResult->alias = strtoupper(trim($colonia->alias));
                        $saveResult->fecha_modificacion = $now;
                        $saveResult->localidad = strtoupper(trim($colonia->localidad));
                        $saveResult->idunidadhabi = $colonia->idunidadhabi;
                        $saveResult->monto = $colonia->monto;
                        $saveResult->activo = $colonia->activo;
                        //$saveResult->cve_num = $colonia->cve_num;
                        $this->response->setContent(json_encode($saveResult));
                    }
                    
                }
                else{
                    $this->response->setStatusCode(404, "Not Found");
                }
            } else {
                $nombre_aux = $this->dropAccents($nombre);
                $nombre_aux = strtoupper(trim($nombre_aux));

                $localidad_aux = $this->dropAccents($localidad);
                $localidad_aux = strtoupper(trim($localidad_aux));

                $coloniaExist = Colonias::validaByNombre($nombre_aux, $localidad_aux);

                if($coloniaExist->valid()){
                    $this->response->setStatusCode(409, "Conflict");
                } else {
                    $colonia = new Colonias();
                    $colonia->nombre = $nombre;
                    $colonia->alias = $alias;
                    $colonia->fecha_creacion = $now;
                    $colonia->fecha_modificacion = $now;
                    $colonia->localidad = $localidad;
//                    $colonia->idunidadhabi = $idunidadhabi;
                    $colonia->monto = $monto;
                    $colonia->ismarginado = floatval($monto) > 0 ? false : true;
                    $colonia->activo = $activo;
                    $colonia->cve_mun = $idmunicipio;

                    if($colonia->save()){
                        $saveResult->response = true;
                        $saveResult->ok = true;
                        $saveResult->status = 200;
                        $saveResult->id = $colonia->id;
                        $saveResult->edit = false;
                        $saveResult->data = $colonia;

                        $btninfo = "";
                        if($this->acl->isAllowedUser("colonias","info")){
                            $btninfo = "<button class='btn btn-sm btn-info read' title='Consultar datos de la colonia' data-index='".$colonia->id."'>
                                <i class='fa fa-info'></i>
                            </button> ";
                        }
                        $btnEdit = "";
                        if($this->acl->isAllowedUser("colonias","edit")){
                            $btnEdit = "<button class='btn btn-sm btn-primary edit' title='Editar datos de la colonia' data-index='".$colonia->id."'>
                                <i class='fa fa-pencil'></i>
                            </button> ";
                        }
                        $btnDelete = "";
                        if($this->acl->isAllowedUser("colonias","delete")){
                            $btnDelete = "<button class='btn btn-sm btn-danger delete' title=¿Desea eliminar colonia?' data-index='".$colonia->id."'>
                                <i class='fa fa-remove'></i>
                            </button> ";
                        }
                        $strNow = date("d/m/Y H:i A", strtotime(str_replace('/', '-',$now)));
                        $tr = "<tr data-index='".$colonia->id."' id='tr-".$colonia->id."''>
                            <td>".
                            $btninfo.
                            $btnEdit.
                            $btnDelete.
                            "</td>
                            <td><i class='fa fa-check' title='Activo'></i></td>
                            <td>".$colonia->id."</td>
                            <td><div id='name-colonia-".$colonia->id."' data-index='".$colonia->id."'>".$colonia->nombre . ' - ' . $municipio . "</div></td>
                            <td>". $colonia->alias ."</td>
                            <td>".$colonia->localidad ."</td>
                            <td>". $colonia->monto ."</td>
                            <td>".$strNow."</td>
                            <td>".$strNow."</td>
                        </tr>";
                        $saveResult->tr = $tr;
                        //$saveResult->cve_num = $colonia->cve_num;
                        $this->response->setContent(json_encode($saveResult));
                    } else {
                        $this->response->setStatusCode(500, "Internal Server Error");
                    }
                }
            }
        } else {
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    /**
     * [editAction Editar colonia por clave]
     * @author [russel] <[<email address>]>
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editAction($id)
    {
        $colonia = Colonias::findFirstById($id);
        if (!$colonia) {
            $this->flash->error("Colony was not found");
            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        }

        if ($this->request->isPost()) {

            $colonia->assign(array(
                'nombre' => $this->request->getPost('nombre', 'striptags'),
                'alias' => $this->request->getPost('alias', 'striptags'),
                'localidad' => $this->request->getPost('localidad', 'striptags'),
                'idunidadhabi' => $this->request->getPost('idunidadhabi', 'int'),
                'monto' => $this->request->getPost('monto', 'float'),
                'active' => $this->request->getPost('active')
            ));

            if (!$colonia->save()) {
                $this->flash->error($colonia->getMessages());
            } else {

                $this->flash->success("Colony was updated successfully");

                Tag::resetInput();
            }
        }

        $this->view->colonias = $colonia;

        $this->view->form = new ColoniasForm($colonia, array(
            'edit' => true
        ));
    }

    /**
     * [deleteAction Dar de baja a la colonia seteando activo=0]
     * @author [russel] <[<email address>]>
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function deleteAction($id)
    {
        if($id != null and $id != ""){
            $colonia = Colonias::findFirstById($id);
            if ($colonia) {
                $colonia->activo = false;
                $colonia->fecha_modificacion = date("c");
                $colonia->save();
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(400, "Bad Request");
        }
        return $this->response;
    }

    /**
     * [validarNombre description]
     * @return [type] [description]
     */
    /* public function validarNombre()
     {

     }*/

    private function findByNombre($valor, $general, $userTerritory, $userIdTerritory){
        $results = array();
        $params = explode(' ', $valor);
        if(count($params) > 1){
            $newParams = $this->eliminarPreposiciones($params);
        }
        else{
            $newParams = $params;
        }
        if(count($newParams) > 0){
            if($general == true){
                $results = Colonias::findByNombre($newParams);
            }
            else{
                $results = Colonias::findByNombre($newParams, $this->getNameColumnId($userTerritory), $userIdTerritory);
            }
        }
        return $results;
    }

    function eliminarPreposiciones($datos){
        $preposiciones = array('de', 'los', 'la', 'y');
        $newData = array();
        foreach ($datos as $valor) {
            if(!in_array($valor, $preposiciones)){
                array_push($newData,$valor);
            }
        }
        return $newData;
    }

    function dropAccents($incoming_string){
        $tofind = "ÀÁÂÄÅàáâäÒÓÔÖòóôöÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ";
        $replac = "AAAAAaaaaOOOOooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn";
        return utf8_encode(strtr(utf8_decode($incoming_string),
            utf8_decode($tofind),
            $replac));
    }

}