<?php
/**
 * Clase controller para el Módulo de los folios
 * @author [russel] <[<email address>]>
 */
namespace Vokuro\Controllers;

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Vokuro\DT\SSPGEO;
use Vokuro\GenericSQL\GenericSQL;
use Vokuro\Models\Folios;
use Vokuro\Models\FolioUsado;
use Vokuro\Models\InventarioFolios;
use Vokuro\Models\Pagos;
use Vokuro\Models\Profiles;
use Vokuro\Models\RangoFolio;
use Vokuro\Models\SecuenciaFolios;
use Vokuro\Models\UserProfiles;
use Phalcon\Mvc\View;
use Vokuro\Models\Users;

class FoliosController extends ControllerBase
{

    /**
     * [indexAction  Default action. Set the public layout (layouts/public.volt)]
     * @return [view] [Vista para Clientes]
     */
    public function indexAction()
    {
        $where = "";
        if ($this->request->isAjax()) {
            $this->view->disable();

            $request = $this->request;
            $response = $this->response;

            $columns = array(
                array( 'db' => '', 'dt' => 0,
                    'formatter' => function( $d, $row ) {
                        return "";
                    }
                ),
                array( 'db' => '', 'dt' => 1,
                    'formatter' => function( $d, $row ) {
                        return "";
                    }
                ),
                array( 'db' => 'activo', 'datatype' => 'boolean', 'dt' => 2,
                    'formatter' => function( $d, $row ) {
                        $vigente = $d ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                            : '<i class="fa fa-remove" style="color: red" title="Activo"></i>';

                        return $vigente;
                    }
                ),
                array( 'db' => 'id', 'dt' => 3, 'datatype' => 'number'),
                array( 'db' => 'num_ini', 'dt' => 4, 'datatype' => 'number'),
                array( 'db' => 'num_fin', 'dt' => 5, 'datatype' => 'number'),
                array( 'db' => 'cobratario', 'dt' => 6, 'separator' => ','),
                array( 'db' => 'porliquidar', 'dt' => 7, 'datatype' => 'number',
                    'formatter' => function( $d, $row ) {
                        $vigente = $d > 0 ? '<button class="btn btn-primary btn-sm counsul-liquidar" data-id="'.$row["id"].'" data-cob="'.$row["idcobratario"].'" title="Consultar folios por liquidar">'.$d.'</button>'
                            : '0';

                        return $vigente;
                    }
                ),
                array( 'db' => 'monto_porliquidar', 'dt' => 8, 'datatype' => 'number'),
                array( 'db' => 'disponibles', 'dt' => 9, 'datatype' => 'number',
                    'formatter' => function( $d, $row ) {
                        $vigente = $d > 0 ? '<button class="btn btn-primary btn-sm counsul-disponibles" data-id="'.$row["id"].'" data-cob="'.$row["idcobratario"].'" title="Consultar folios disponibles">'.$d.'</button>'
                            : '0';

                        return $vigente;
                    }
                ),
                array( 'db' => 'transito', 'dt' => 10, 'datatype' => 'number'),
                array( 'db' => 'fecha_asignacion', 'dt' => 11, 'datatype' => 'date'),
                array( 'db' => 'usuario', 'dt' => 12, 'datatype' => 'number'),
                array( 'db' => 'idcobratario', 'dt' => 13, 'datatype' => 'number'),
                array( 'db' => 'activo', 'datatype' => 'boolean', 'dt' => 14),
                array(
                    'db' => 'id',
                    'dt' => 'DT_RowId',
                    'formatter' => function( $d, $row ) {
                        // Technically a DOM id cannot start with an so we prefix
                        // a string. This can also be useful if you have multiple tables
                        // to ensure that the id is unique with a different prefix
                        return 'cliente-'.$d;
                    }
                )
            );

            $data = SSPGEO::complex_geo($this->request->get(), "folios.view_folios", "id", $columns, "", "", true);

//        $where = SSPGEO::filter_join_data( $this->request->get(), $columns, $bindings );



            $response->setContent(json_encode($data));
            return $response;
        }
        else{
            $this->view->setTemplateBefore('public');
            $this->view->setVar('cobratarios', Users::findByActivo());
        }
    }

    public function saveAction(){
        $this->view->disable();
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];
        if($this->request->isPost()) {
            $rawBody = $this->request->getJsonRawBody();
            $id = $rawBody->id;
//            $serie = $rawBody->serie;
            $cantfol = $rawBody->cantfol;
            $cobratario = $rawBody->cobratario;

            $rowCount = $this->db->query("select count(1) count from 
            folios.folios 
            where activo and cancelado = false and idcobratario is null ");

            if($rowCount->fetch()[0] < $cantfol){
                $this->response->setStatusCode(409, "No existen folios suficientes para la asignacion");
                return $this->response;
            }

            $this->db->begin();
            $rFolio = new RangoFolio();
            $rFolio->idusuario = $idUser;
            $objSecfol = SecuenciaFolios::findFirst([
                "activo = true",
                "order" => "id desc"
            ]);
            $secfol = 1;
            if($objSecfol){
                $secfol = $objSecfol->id;
            }

            $objFolIni = Folios::findFirst([
                "activo and cancelado = false and idcobratario is null and folio >= ".$this->config->application->folioInternoInicial,
                "order" => "folio asc"
            ]);

            $folini = 1;
            if($objFolIni){
                $folini = $objFolIni->folio;
            }
//            $rFolio->serie = $serie;
            $rFolio->cantidad_folios = $cantfol;
            $rFolio->idcobratario = $cobratario;
            $rFolio->idinventario = $secfol;
            $rFolio->total = $cantfol;
            $rFolio->disponibles = $cantfol;
            $rFolio->num_ini = $folini;
            $rFolio->num_fin = $folini + $cantfol - 1;

            if (!$rFolio->save()) {
                foreach ($rFolio->getMessages() as $message) {
                    $this->logger->info("(save-rango-folio): " . $message);
                }
                $this->response->setStatusCode(500, "Internal Servere Error");
                $this->db->rollback();
            } else {
                $this->db->query("update folios.folios 
                set idrango_folio = ".$rFolio->id.", idcobratario = $cobratario, 
                    fecha_asignacion = now(), idusuario_asigno = $idUser
                where id in (
                    select id from folios.folios 
                    where activo and cancelado = false and idcobratario is null 
                    and folio >= ".$this->config->application->folioInternoInicial." 
                    order by folio asc
                    limit $cantfol
                )");
                $this->db->commit();
            }
        }
        else if ($this->request->isDelete()){
            $id = $this->request->get("id");
            $rFolio = RangoFolio::findFirstById($id);
            if($rFolio->usados > 0){
                $this->response->setStatusCode(409, "Ya existen folios asignados a pagos");
            }
            else{
                $rFolio->activo = false;
                $this->db->begin();
                if (!$rFolio->save()) {
                    foreach ($rFolio->getMessages() as $message) {
                        $this->logger->info("(save-rango-folio): " . $message);
                    }
                    $this->response->setStatusCode(500, "Internal Servere Error");
                    $this->db->rollback();
                }
                else{
                    $exc = $this->db->query("update folios.folios 
                    set activo = false, fecha_modificacion= now()
                    where idrango_folio = $id");
                    if($exc) {

                        $exc = $this->db->query("insert into folios.folios (folio, idinventario, secuencia, idusuario) 
                        SELECT folio, idinventario, secuencia, $idUser from folios.folios where idrango_folio = $id");
                        if($exc) {
                            $this->db->commit();
                        }
                        else{
                            $this->response->setStatusCode(500, "Internal Servere Error");
                            $this->db->rollback();
                        }
                    }
                    else{
                        $this->response->setStatusCode(500, "Internal Servere Error");
                        $this->db->rollback();
                    }
                }
            }
        }
        return $this->response;
    }

    public function disponiblesAction(){
        $this->view->disable();
        $rowcount = Folios::count("activo = true and cancelado = false and idpago is null");
        $objSecfol = SecuenciaFolios::findFirst([
            "activo = true",
            "order" => "id desc"
        ]);
        $secfol = 1;
        if($objSecfol){
            $secfol = $objSecfol->id;
        }

        $objFolio =  Folios::findFirst([
            "secuencia = $secfol and activo = true and cancelado = false and idcobratario is null 
            and origen = 'APPV1' and folio >".$this->config->application->folioInternoInicial,
            "order" => "folio asc"
        ]);
        $numini = 1;
        if($objFolio)
                $numini = $objFolio->folio;
        $resp = array(
            "disponibles" => $rowcount,
            "numini" => $numini
        );
        $this->response->setContent(json_encode($resp));
        return $this->response;
    }

    public function getAction($id){
        $rFolio = RangoFolio::findFirstById($id);
        $this->response->setContent(json_encode($rFolio));
        return $this->response;
    }

    public function getInventarioAction(){
        $this->view->disable();
        $inv = $this->request->get("inv");
        $next = 1;
        $folio = InventarioFolios::findFirst([
            "activo = true",
            "order" => "id desc"
        ]);

        if($folio){
            $next = $folio->num_fin + 1;
        }

        $this->response->setContent(json_encode(
            array(
                "nextFolio" => $next,
                "inventario" => $this->getInventario()
            )
        ));
        return $this->response;
    }

    public function saveinvAction(){
        $this->view->disable();
        if($this->request->isPost()) {
            $rawBody = $this->request->getJsonRawBody();
            $numini = $rawBody->numini;
            $numfin = $rawBody->numfin;
            $observaciones = $rawBody->observaciones;

            $identity = $this->auth->getIdentity();
            $idUser = $identity["id"];

            $this->db->begin();
            $total = $numfin - $numini + 1;
            $rFolio = new InventarioFolios();
            $rFolio->idusuario = $idUser;
            $rFolio->num_ini = $numini;
            $rFolio->num_fin = $numfin;
            $rFolio->total = $total;
            $rFolio->disponibles = $total;
            $rFolio->observaciones = $observaciones;

            $objSecfol = SecuenciaFolios::findFirst([
                "activo = true",
                "order" => "id desc"
            ]);
            $secfol = 1;
            if($objSecfol){
                $secfol = $objSecfol->id;
            }
            $rFolio->foliador = $secfol;

            if (!$rFolio->save()) {
                foreach ($rFolio->getMessages() as $message) {
                    $this->logger->info("(save-inventario-folio): " . $message);
                }
                $this->response->setStatusCode(500, "Internal Servere Error");
                $this->db->rollback();
            } else {
                $this->db->query("insert into folios.folios (folio, idusuario, idinventario, secuencia) 
                SELECT s, $idUser, ".$rFolio->id.",$secfol FROM generate_series($numini, $numfin) s");
                $this->db->commit();

                $this->response->setContent(json_encode(
                    array(
                        "data" => $this->getInventario($rFolio->id),
                        "folioactual" => $numfin + 1
                    )
                ));
            }
        }
        else if ($this->request->isDelete()){
            $id = $this->request->get("id");
            $rFolio = InventarioFolios::findFirstById($id);
            $rFolioMay = InventarioFolios::findFirst([
                "id > ".$id." and activo"
            ]);
            if($rFolio->usados > 0){
                $this->response->setStatusCode(409, "Ya existen folios asignados a pagos");
            }
            else if($rFolioMay){
                $this->response->setStatusCode(409, "Existe un inventario posterior al actual");
            }
            else{
                $rFolio->activo = false;
                if (!$rFolio->save()) {
                    foreach ($rFolio->getMessages() as $message) {
                        $this->logger->info("(save-rango-folio): " . $message);
                    }
                    $this->response->setStatusCode(500, "Internal Servere Error");
                }
                else{
                    GenericSQL::getBySQL("update folios.folios set activo = false where secuencia = ".$id);
                }
            }
        }
        return $this->response;
    }

    public function getxliquidarAction($id){
        $this->view->disable();
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];
        $idrf = $this->request->get("idrf");

        $objSecfol = SecuenciaFolios::findFirst([
            "activo = true",
            "order" => "id desc"
        ]);

        //TODO considerar que sucede cuando se reinicie y queden folio de la secuencia anterior
        $secfol = 1;
        if($objSecfol){
            $secfol = $objSecfol->id;
        }

        $objNextFolio = Folios::find([
            "idcobratario = ".$id." and idrango_folio = ".$idrf." and activo = true and idpago is not null and liquidado = false and secuencia = ".$secfol,
            "order" => "folio asc",
            "columns" => "folio"
        ]);

        $this->response->setContent(json_encode($objNextFolio));
        return $this->response;
    }

    public function getdisponiblesAction($id){
        $this->view->disable();
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];
        $idrf = $this->request->get("id");

        $objSecfol = SecuenciaFolios::findFirst([
            "activo = true",
            "order" => "id desc"
        ]);

        //TODO considerar que sucede cuando se reinicie y queden folio de la secuencia anterior
        $secfol = 1;
        if($objSecfol){
            $secfol = $objSecfol->id;
        }

        $objNextFolio = Folios::find([
            "idrango_folio = $idrf and idcobratario = ".$id." and activo = true and cancelado = false and idpago is null and secuencia = ".$secfol,
            "order" => "folio asc",
            "columns" => "id, folio"
        ]);

        $this->response->setContent(json_encode($objNextFolio));
        return $this->response;
    }

    public function cancelarAction(){
        $this->view->disable();
        $rawBody = $this->request->getJsonRawBody();
        $data = $rawBody->data;
        $observaciones = $rawBody->observaciones;
        $ids = implode(",", $data);

        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];

        $this->db->begin();
        $exc = $this->db->query("update folios.folios 
                set cancelado = true, tipo_cancelado = 'POR TESORERIA', observaciones = '$observaciones',
                idusuario_cancela = $idUser, fecha_cancela = now()
                where id in ($ids)");

        if(!$exc){
            $this->response->setStatusCode(500, "Internal Servere Error");
            $this->db->rollback();
        }
        else{
            $exc = $this->db->query("update folios.rango_folio 
                set disponibles = disponibles - ".count($data)."
                where id in (select idrango_folio from folios.folios where id in ($ids))");
            if(!$exc){
                $this->response->setStatusCode(500, "Internal Servere Error");
                $this->db->rollback();
            }
            else{
                $this->db->commit();
            }
        }
        return $this->response;
    }

    public function transferiraAction(){
        $this->view->disable();
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];
        $rawBody = $this->request->getJsonRawBody();
        $data = $rawBody->data;
        $id = $rawBody->cobratario;
        $tipo = $rawBody->tipo;
        $ids = implode(",", $data);

        $this->db->begin();

        $exc = $this->db->query("update folios.folios 
                set activo = false, fecha_modificacion= now(), observaciones = 'Transferido a ".($tipo == "TES" ? "TESORERIA" : "COBRATARIO")."'
                where id in ($ids)");

        if(!$exc){
            $this->response->setStatusCode(500, "Internal Servere Error");
            $this->db->rollback();
        }
        else{
            $exc = $this->db->query("update folios.rango_folio 
                set disponibles = disponibles - ".count($data)."
                where id in (select idrango_folio from folios.folios where id in ($ids))");
            if(!$exc){
                $this->response->setStatusCode(500, "Internal Servere Error");
                $this->db->rollback();
            }
            else{
                if($tipo == "TES"){
                    $exc = $this->db->query("insert into folios.folios (folio, idinventario, secuencia, idusuario) 
                        SELECT folio, idinventario, secuencia, $idUser from folios.folios where id in ($ids)");

                    if($exc){
                        $this->db->commit();
                    }
                    else{
                        $this->db->rollback();
                        $this->response->setStatusCode(500, "Internal Servere Error");
                    }
                }
                else{
                    $rFolio = new RangoFolio();
                    $rFolio->idusuario = $idUser;
                    $objSecfol = SecuenciaFolios::findFirst([
                        "activo = true",
                        "order" => "id desc"
                    ]);
                    $secfol = 1;
                    if($objSecfol){
                        $secfol = $objSecfol->id;
                    }

                    $total = count($data);
                    $rFolio->cantidad_folios = $total;
                    $rFolio->idcobratario = $id;
                    $rFolio->foliador = $secfol;
                    $rFolio->total = $total;
                    $rFolio->disponibles = $total;
                    if (!$rFolio->save()) {
                        foreach ($rFolio->getMessages() as $message) {
                            $this->logger->info("(save-rango-folio): " . $message);
                        }
                        $this->response->setStatusCode(500, "Internal Servere Error");
                        $this->db->rollback();
                    } else {
                        $exc = $this->db->query("insert into folios.folios (folio, idrango_folio, idinventario, secuencia, idusuario, idcobratario) 
                    SELECT folio, ".$rFolio->id.", idinventario, secuencia, $idUser, $id from folios.folios where id in ($ids)");
                        if($exc){
                            $this->db->commit();
                        }
                        else{
                            $this->db->rollback();
                            $this->response->setStatusCode(500, "Internal Servere Error");
                        }
                    }
                }

            }
        }
        return $this->response;
    }

    public function usadosAction(){
        $this->view->disable();
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];
        $rawBody = $this->request->getJsonRawBody();
        $folio = $rawBody->folio;
        $observaciones = $rawBody->observaciones;

        $this->db->begin();
        $fp = Pagos::findFirstByFolio_interno($folio);
        if($fp){
            $fp->folio_interno = $fp->folio_interno."X";
            if($fp->save()){
                $fu = new FolioUsado();
                $fu->folio = $folio;
                $fu->idpago = $fp->idpago;
                $fu->idcobratario = $fp->idusuario;
                $fu->idusuario = $idUser;
                $fu->fecha_cobro = $fp->fecha_pago;
                $fu->observaciones = $observaciones;
                if(!$fu->save()){
                    foreach ($fu->getMessages() as $message) {
                        $this->logger->info("(save folio usado): " . $message);
                    }
                    $this->db->rollback();
                    $this->response->setStatusCode("500", "Ocurrio un error al guardar la informacion");
                }
                else{
                    $this->db->commit();
                }
            }
            else{
                foreach ($fp->getMessages() as $message) {
                    $this->logger->info("(update folio usado): " . $message);
                }
                $this->db->rollback();
                $this->response->setStatusCode("404", "No se encontro el folio seleccionado");
            }
        }
        else{
            $this->db->rollback();
            $this->response->setStatusCode("404", "Nose encontro el folio seleccionado");
        }
        return $this->response;
    }

    private function getInventario($id = null){
        $where = "";
        if($id){
            $where = "id = ".$id;
        }
        $inventario = InventarioFolios::find([
            $where,
            "order" => "fecha_creacion desc"
        ]);

        $resp = array();
        foreach ($inventario as $i){
            $objUsuario = Users::findFirstById($i->idusuario);
            array_push($resp, array(
                "",
                '<button class="btn btn-sm btn-danger deleteinv" '.($i->activo ? "":"disabled").' data-id="'.$i->id.'" title="¿Desea eliminar?"><i class="fa fa-remove"></i></button>',
                $i->activo ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                    : '<i class="fa fa-remove" style="color: red" title="Activo"></i>',
                $i->id,
                $i->num_ini,
                $i->num_fin,
                $i->usados,
                $i->disponibles,
                $i->total,
                $i->foliador,
                $i->observaciones,
                date("d/m/Y", strtotime($i->fecha_creacion)),
                $objUsuario->usuario,
            ));
        }
        return $resp;
    }

    private function getActualFolio(){
        $objSecfol = SecuenciaFolios::findFirst([
            "activo = true",
            "order" => "id desc"
        ]);
        $secfol = 1;
        if($objSecfol){
            $secfol = $objSecfol->id;
        }

        $objFolio =  Folios::findFirst([
            "secuencia = $secfol and activo = true and cancelado = false and idcobratario is null",
            "order" => "folio asc"
        ]);
        $numini = 1;
        if($objFolio)
            $numini = $objFolio->folio;

        return $numini;
    }
}