<?php
namespace Vokuro\Controllers;

//use Phalcon\Di;
use Exception;
use GuzzleHttp\Client;
use Vokuro\GenericSQL\GenericSQL;

use Phalcon\Mvc\View;

use Vokuro\Models\Dispositivos;
use Vokuro\Models\Tracking;
use Vokuro\Models\TrackingStatus;
use Vokuro\Models\Users\Application;
use Vokuro\Models\Users\Device;
use Vokuro\Models\Users\Session;
use Vokuro\Models\Users\Users;
use Vokuro\Models\Users\Permissions;
use Phalcon\Mvc\Model\Query;

use Vokuro\Auth\Exception as AuthException;
/**
 * Display the default index page.
 */
class ApicobController extends ControllerBase
{
    public function initialize()
    {
        $this->view->disable();
    }

    public function loginAction()
    {
        if($this->request->isPost()) {//login
            $credentials = $this->request->getJsonRawBody();
            $this->logger->info($id.' <=login cob=> '.$this->session->getId());
            try {
                $this->auth->check([
                    'user' => $credentials->user,
                    'password' => $credentials->password
                ]);
                $this->response->setContent(json_encode(array(
                    "user" => $credentials->user
                )));
                $this->logger->info($id.' <=8=> '.$this->session->getId());
            } catch (AuthException $e) {
                $this->response->setStatusCode(401);
            }

            $this->response->setHeader("Token", session_id());
            $this->logger->info($id.' <=9=> '.$this->session->getId());
        }else{
            $this->response->setStatusCode(405);
        }

        return $this->response;
    }

    public function ubicacionAction()
    {
        $data = $this->request->getJsonRawBody();
        $lat = $data->lat;
        $lng = $data->lng;
        $androidid = $data->uuid;
        $idusuario = null;
        $usuario = null;

        if(!empty($androidid)){
            $md = Dispositivos::findFirstByDispositivo($androidid);
            if(!$md){
                $md = new Dispositivos();
                $md->dispositivo = $androidid;
                $md->origen = 'MOVIL';
                if(!$md->save()){
                    foreach ($md->getMessages() as $message) {
                        $this->logger->error("save-api dispositivo: ".$message->getMessage());
                    }
                }
            }
            else{
                if(!$md->activo){
                    $md->activo = true;
                    $md->fecha_modificacion = date("c");
                    $md->idusuario = null;
                    $md->usuario = null;
                    if(!$md->save()){
                        foreach ($md->getMessages() as $message) {
                            $this->logger->error("save-api dispositivo-update: ".$message->getMessage());
                        }
                    }
                }
                else{
                    if($md->idusuario){
                        $idusuario = $md->idusuario;
                        $usuario = $md->usuario;
                    }
                }
            }
        }
        $t = new Tracking();
        $t->latitud = $lat;
        $t->longitud = $lng;
        $t->dispositivo = $androidid;
        $t->idusuario = $idusuario;
        $t->usuario = $usuario;
        if(!$t->save()){
            foreach ($t->getMessages() as $message) {
                $this->logger->error("save-apicobranza tracking: ".$message->getMessage());
            }
        }
    }

    public function ubicacionstatusAction()
    {
        $data = $this->request->getJsonRawBody();
        $lat = $data->lat;
        $lng = $data->lng;
        $androidid = $data->uuid;
        $status = $data->status;
        $idusuario = null;
        $usuario = null;

        if(!empty($androidid)){
            $md = Dispositivos::findFirstByDispositivo($androidid);
            if(!$md){
                $md = new Dispositivos();
                $md->dispositivo = $androidid;
                $md->origen = 'MOVIL';
                if(!$md->save()){
                    foreach ($md->getMessages() as $message) {
                        $this->logger->error("save-api dispositivo: ".$message->getMessage());
                    }
                }
            }
            else{
                if(!$md->activo){
                    $md->activo = true;
                    $md->fecha_modificacion = date("c");
                    $md->idusuario = null;
                    $md->usuario = null;
                    if(!$md->save()){
                        foreach ($md->getMessages() as $message) {
                            $this->logger->error("save-api dispositivo-update: ".$message->getMessage());
                        }
                    }
                }
                else{
                    if($md->idusuario){
                        $idusuario = $md->idusuario;
                        $usuario = $md->usuario;
                    }
                }
            }
        }
        $t = new TrackingStatus();
        $t->latitud = $lat;
        $t->longitud = $lng;
        $t->dispositivo = $androidid;
        $t->idusuario = $idusuario;
        $t->status = $status;
        $t->usuario = $usuario;
        if(!$t->save()){
            foreach ($t->getMessages() as $message) {
                $this->logger->error("save-apicobranza tracking-status: ".$message->getMessage());
            }
        }
    }

}
