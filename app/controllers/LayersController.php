<?php
namespace Vokuro\Controllers;

use Phalcon\Tag;
use Vokuro\Forms\LayersForm;
use Vokuro\Models\Layers;
use Vokuro\Models\LayersGroup;

/**
 * Vokuro\Controllers\LayersController
 * CRUD to manage layers
 
 */
class LayersController extends ControllerBase
{

	public function initialize(){
	    
	}

	/**
	 * Default action, shows the search form
	 */
	public function indexAction(){
		$this->view->setTemplateBefore('public');
		$layers = array();
		$capas = Layers::find();
		foreach ($capas as $key=>$value){
			array_push($layers, $capas[$key]);
		}
		$this->view->setVar("capas", $layers);
	}

	public function infoAction($id){
		$id = intval($id);
		$this->view->setTemplateBefore('public');
		$identity = $this->auth->getIdentity();
		
		$this->response->setContentType("application/json");
		
		if(empty($id)){
			$this->response->setStatusCode(501);
			$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"Capa no especificada.")));
			return $this->response;
		}else{
			$bdLayer = Layers::find($id);
			if(!$bdLayer){
				$this->response->setStatusCode(404);
				$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"La capa que intenta consultar no se encuentra registrado.")));
				return $this->response;
			}else{
				$aux = new \stdClass();
				foreach($bdLayer as $layer){
					$aux->id 							= $layer->id;
					$aux->nombre 					= $layer->nombre;
					$aux->descripcion 					= $layer->descripcion;
					$aux->capa 					= $layer->capa;
					$aux->estilo 					= $layer->estilo;
					$aux->estilo_etiqueta 					= $layer->estilo_etiqueta;
					$aux->orden 					= $layer->orden;
					$aux->servidor 					= $layer->servidor;
					$aux->inicial 					= $layer->inicial ? "Si" : "No";
					//$aux->cql 					= $layer->cql ? "Si" : "No";
					//$aux->modulo 					= $layer->modulo;
					//$aux->google 					= $layer->google ? "Si" : "No";
					//$aux->camposinfo 					= $layer->camposinfo;
					//$aux->tematico 					= $layer->tematico ? "Si" : "No";
					$aux->conetiqueta 					= $layer->conetiqueta ? "Si" : "No";
					$aux->fecha_creacion 	= $layer->fecha_creacion;
					$aux->fecha_modificacion 	= $layer->fecha_modificacion;
					$aux->activo 					= $layer->activo ? "Si" : "No";
					
					$dbLayerGroup = LayersGroup::find([
						'columns' => 'nombre',
						'id = '.$layer->idgrupo_capa
					]);
					foreach($dbLayerGroup as $LayersGroup){
						$aux->grupo_capa = $LayersGroup->nombre;
					}
				}
				$this->response->setStatusCode(200);
				$this->response->setContent(json_encode(array("lError"=>false, "cMensaje"=>"Capa consultada.", "resultado"=>$aux)));
				return $this->response;
			}
			return $this->response;
		}
	}

	public function createAction(){
		$identity = $this->auth->getIdentity();
		$idUsuario = $identity["id"];

		if($this->request->isGet() == true) {
			$this->view->setTemplateBefore('public');
			$grupos_capa = LayersGroup::find([
				'activo = TRUE'
			]);
			$this->view->setVar("grupo_capa", $grupos_capa);
			
			$this->view->form = new LayersForm();
		}elseif ($this->request->isPost() == true){
			try{
				$this->view->disable();
		
				$rawBody = $this->request->getJsonRawBody();
				
				$aux = new Layers();
				$nombre   					= $rawBody->nombre;
				$aux->nombre 					= $rawBody->nombre;
				$aux->descripcion 					= $rawBody->descripcion;
				$aux->capa 					= $rawBody->capa;
				$aux->estilo 					= $rawBody->estilo;
				$aux->estilo_etiqueta 					= $rawBody->estilo_etiqueta;
				$aux->conetiqueta 					= $rawBody->conetiqueta;
				$aux->orden 					= $rawBody->orden;
				//$aux->servidor 					= $rawBody->servidor;
				//$aux->inicial 					= $rawBody->inicial;
				//$aux->cql 					= $rawBody->cql;
				//$aux->google 					= $rawBody->google;
				$aux->modulo 					= $rawBody->modulo;
				//$aux->camposinfo 					= $rawBody->campos_info;
				$aux->esquema 					= $rawBody->esquema;
				$aux->tabla 					= $rawBody->tabla;
				//$aux->tematico 					= $rawBody->tematico;
				$aux->idgrupo_capa 					= $rawBody->grupo_capa;
				$aux->activo		= $rawBody->activo;
				
				$bdLayer = Layers::findFirstByNombre($nombre);
				if($bdLayer){
					$this->response->setStatusCode(409, "La capa $nombre que intenta crear ya se encuentra registrado.");
					return $this->response;
				}

				$this->db->begin();

				if($aux->save() === false){
					$this->db->rollback();
					
					$messages = $objUser->getMessages();
							$cError = "";
					foreach ($messages as $message) {
						$cError .= "[".$message."]";
					}

					$this->response->setStatusCode(501, $cError);
					return $this->response;
				}else{
					$this->db->commit();
					$this->response->setStatusCode(201, "Capa registrada satisfactoriamente.");
					$this->flash->error("Capa registrada satisfactoriamente.");
					return $this->response;
				}
			}catch(Exception $e){
				$this->response->setStatusCode(500, $e->getMessage());
				return $this->response;
			}
		}
	}

	public function editAction($id){
		$id = intval($id);
		$infolayer = null;

		if($this->request->isGet() == true) {
			$this->view->setTemplateBefore('public');

			$dbinfolayer = Layers::find($id);
			foreach($dbinfolayer as $layer){
				$info = new \stdClass();

				$info->id = $layer->id;
				$info->activo = $layer->activo;
				$info->nombre = $layer->nombre;
				$info->descripcion = $layer->descripcion;
				$info->capa = $layer->capa;
				$info->estilo = $layer->estilo;
				$info->estilo_etiqueta = $layer->estilo_etiqueta;
				$info->conetiqueta 	= $layer->conetiqueta;
				$info->orden = $layer->orden;
				//$info->servidor = $layer->servidor;
				//$info->inicial = $layer->inicial;
				//$info->cql = $layer->cql;
				//$info->modulo = $layer->modulo;
				//$info->google = $layer->google;
				//$info->camposinfo = $layer->camposinfo;
				$info->idgrupo_capa = $layer->idgrupo_capa;
				//$info->tematico = $layer->tematico;
				$info->esquema 					= $layer->esquema;
				$info->tabla 					= $layer->tabla;
				$info->conetiqueta = $layer->conetiqueta;
			}
			$this->view->setVar("infolayer", $info);

			$this->view->form = new LayersForm($info, [
									'edit' => true,
								]);
		}elseif ($this->request->isPost() == true){
			try{
				$this->view->disable();

				$rawBody = $this->request->getJsonRawBody();

				$id 			= $rawBody->id;
				$nombre 	= $rawBody->nombre;
				
				$bdLayer = Layers::findFirstById($id);
				
				if(!$bdLayer){
					$this->response->setStatusCode(409, "La capa $nombre que intenta modificar no se encuentra registrado.");
					return $this->response;
				}
				
				
				$bdLayer->nombre = $rawBody->nombre;
				$bdLayer->descripcion = $rawBody->descripcion;
				$bdLayer->capa = $rawBody->capa;
				$bdLayer->activo = $rawBody->activo;
				$bdLayer->estilo = $rawBody->estilo;
				$bdLayer->estilo_etiqueta = $rawBody->estilo_etiqueta;
				$bdLayer->conetiqueta 	= $rawBody->conetiqueta;
				$bdLayer->orden = $rawBody->orden;
				//$bdLayer->servidor = $rawBody->servidor;
				//$bdLayer->inicial = $rawBody->inicial;
				//$bdLayer->cql = $rawBody->cql;
				//$bdLayer->modulo = $rawBody->modulo;
				//$bdLayer->google = $rawBody->google;
				//$bdLayer->camposinfo = $rawBody->campos_info;
				$bdLayer->idgrupo_capa = $rawBody->grupo_capa;
				//$bdLayer->tematico = $rawBody->tematico;
				$bdLayer->conetiqueta = $rawBody->con_etiqueta;
				$bdLayer->esquema 					= $rawBody->esquema;
				$bdLayer->tabla 					= $rawBody->tabla;
				
				$this->db->begin();

				if($bdLayer->save() === false){
					$this->db->rollback();

					$messages = $bdLayer->getMessages();
					$cError = "";
					foreach ($messages as $message) {
						$cError .= "[".$message."]";
					}

					$this->response->setStatusCode(501, $cError);
					return $this->response;
				}else{
					$this->db->commit();
					$this->response->setStatusCode(201, "Capa actualizada satisfactoriamente.");
					$this->flashSession->success("Capa \"$bdLayer->nombre\" actualizada satisfactoriamente.");
					return $this->response;
				}
			}catch(Exception $e){
				$this->response->setStatusCode(500, $e->getMessage());
				return $this->response;
			}
		}
	}


	function deactivateAction($id){
		$this->view->setTemplateBefore('public');
		$identity = $this->auth->getIdentity();
		$idUser = $identity["id"];
		$this->response->setContentType("application/json");
		if ($this->request->isPut() == true) {
			if(empty($id)){
				$this->response->setStatusCode(505);
				$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"Capa no especificada.")));
				return $this->response;
			}else{
				$bdLayer = Layers::findFirstById($id);
				if(!$bdLayer){
					$this->response->setStatusCode(404);
					$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"La capa que intenta desactivar no se encuentra registrada.")));
					return $this->response;
				}else{
					$this->db->begin();
					$bdLayer->fecha_modificacion = date("c");
					$bdLayer->activo = false;
					if($bdLayer->save()){
						$this->db->commit();
						$this->response->setStatusCode(200);
						$this->response->setContent(json_encode(array("lError"=>false, "cMensaje"=>"Capa desactivada satisfactoriamente.")));
						return $this->response;
					}else{
						$this->db->rollback();
						$this->response->setStatusCode(200);
						$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"Ocurrio un error al desactivar la capa.")));
						return $this->response;
					}
				}
				return $this->response;
			}
		}else{
			$this->response->setStatusCode(405);
			return $this->response;
		}
	}

	function activateAction($id){
		$this->view->setTemplateBefore('public');
		$identity = $this->auth->getIdentity();
		$idUser = $identity["id"];
		$this->response->setContentType("application/json");
		if ($this->request->isPut() == true) {
			if(empty($id)){
				$this->response->setStatusCode(505);
				$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"Capa no especificada.")));
				return $this->response;
			}else{
				$bdLayer = Layers::findFirstById($id);
				if(!$bdLayer){
					$this->response->setStatusCode(404);
					$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"La capa que intenta activar no se encuentra registrada.")));
					return $this->response;
				}else{
					$this->db->begin();
					$bdLayer->fecha_modificacion = date("c");
					$bdLayer->activo = true;
					if($bdLayer->save()){
						$this->db->commit();
						$this->response->setStatusCode(200);
						$this->response->setContent(json_encode(array("lError"=>false, "cMensaje"=>"Capa activada satisfactoriamente.")));
						return $this->response;
					}else{
						$this->db->rollback();
						$this->response->setStatusCode(200);
						$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"Ocurrio un error al activar la capa.")));
						return $this->response;
					}
				}
				return $this->response;
			}
		}else{
			$this->response->setStatusCode(405);
			return $this->response;
		}
	}
}
