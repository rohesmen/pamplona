<?php
namespace Vokuro\Controllers;

//use Phalcon\Di;
use Exception;
use GuzzleHttp\Client;
use Vokuro\GenericSQL\GenericSQL;

use Phalcon\Mvc\View;

use Phalcon\Mvc\Model\Query;

use Vokuro\Auth\Exception as AuthException;

use Swift_Message as Message;
use Swift_SmtpTransport as Smtp;
use Vokuro\Mail\Mail;
use Vokuro\Models\BitacoraActualizarPago;
use Vokuro\Models\BitacoraEstatus;
use Vokuro\Models\BitacoraImpresiones;
use Vokuro\Models\Clientes;
use Vokuro\Models\Colonias;
use Vokuro\Models\BitacoraCambios;
use Vokuro\Models\Estatus_cliente;
use Vokuro\Models\Foliador;
use Vokuro\Models\Folios;
use Vokuro\Models\HistorialPago;
use Vokuro\Models\InventarioFolios;
use Vokuro\Models\MotioDescuento;
use Vokuro\Models\MunicipiosYucatan;
use Vokuro\Models\Pagos;
use Vokuro\Models\Promociones;
use Vokuro\Models\RangoFolio;
use Vokuro\Models\Ruta;
use Vokuro\Models\SecuenciaFolios;
use Vokuro\Models\Users;
use Vokuro\Util\NumeroLetras;
use Vokuro\Util\Util;

use html2pdf;

/**
 * Display the default index page.
 */
class Apiv2Controller extends ApicontrollerBase
{
    public function initialize()
    {
        $this->view->disable();
    }

    public function sesionesAction($id = ''){
        $single = !empty($id);
        $device = null;
        if($single){
            $this->logger->info($id.' <=5=> '.$this->session->getId());
            if($this->request->isHead()){
                $identity = $this->auth->getIdentity();

                if($id == $this->session->getId() && is_array($identity)){
                    $this->logger->info($id.' bien session head');
                    $this->response->setStatusCode(204);
                }else{
                    $this->logger->info($id.' mal session head');
                    $this->response->setStatusCode(404);
                }
            }
            elseif($this->request->isGet()){
                if($id == $this->session->getId()){
                    $this->logger->info($id.' bien session get');
                    $this->response->setJsonContent($_SESSION);
                }else{
                    $this->logger->info($id.' mal session get');
                    $this->response->setStatusCode(404);
                }
            }
            elseif($this->request->isDelete()){
                if($id == $this->session->getId()){
                    $this->logger->info($id.' <=6=> '.$this->session->getId());
                    $this->session->destroy();
                    $this->response->setStatusCode(204);
                }else{
                    $this->response->setStatusCode(404);
                }
            }
        }
        else{
            if($this->request->isPost()) {//login
                $credentials = $this->request->getJsonRawBody();
                try {
                    $this->auth->apiv2check([
                        'user' => $credentials->user,
                        'password' => $credentials->password,
                        'application' => $credentials->application,
                        'device' => $credentials->device
                    ]);
                    $identity = $this->auth->getIdentity();
                    $this->response->setContent($identity["id"]);
                } catch (AuthException $e) {
                    $this->response->setStatusCode(401);
                }

                $this->response->setHeader("Token", session_id());
            }else{
                $this->response->setStatusCode(405);
            }
        }

        return $this->response;
    }

    public function clientesAction(){
        if($this->request->isGet()){
            list($token, $strDev, $strIdUser) = $this->getUserdata();
            $user = Users::findFirst($strIdUser);
            $tipo = $this->request->get("tipo");
            $clave = $this->request->get("clave");
            $calle = $this->request->get("calle");
            $numero = $this->request->get("numero");
            $calle_letra = $this->request->get("calle_letra");
            $numero_letra = $this->request->get("numero_letra");
            $colonia = $this->request->get("colonia");
            $idmunicipio = $this->request->get("idmunicipio");

            if($tipo == 'dir'){
                $where = " and regexp_replace(trim(lower(calle)), ' ', '', 'g') = regexp_replace('".strtolower ($calle)."', '\s', '', 'g') 
                and regexp_replace(trim(lower(numero)), ' ', '', 'g') = regexp_replace('".strtolower  ($numero)."', '\s', '', 'g') ";
                if($this->request->has("calle_letra") && !empty(trim($calle_letra))){
                    $where .= " and regexp_replace(trim(lower(calle_letra)), ' ', '', 'g') = regexp_replace('".strtolower( $calle_letra)."', '\s', '', 'g')";
                }
//                else{
//                    $where .= ' and calle_letra is null';
//                }
                if($this->request->has("numero_letra") && !empty($numero_letra)){
                    $where .= " and regexp_replace(trim(lower(numero_letra)), ' ', '', 'g') = regexp_replace('".strtolower ($numero_letra)."', '\s', '', 'g')";
                }
//                else{
//                    $where .= ' and numero_letra is null';
//                }
                if($this->request->has("colonia") && !empty($colonia)){
                    $where .= " and idcolonia = $colonia";
                }
            }
//            validacion de clave
            else {
                $where = " and id_cliente = ".$clave;
            }

            $sql = "select 
                id_cliente id, 
                coalesce(upper(trim(calle)), '') || coalesce(' ' || upper(trim(calle_letra)), '') calle,
                coalesce(upper(trim(numero)), '') || coalesce(' ' || upper(trim(numero_letra)), '') || coalesce(' ' || upper(trim(tipo_numero)), '') numero,
                coalesce(upper(trim(cruza1)), '') || coalesce(' ' || upper(trim(letra_cruza1)), '') cruz1,
                coalesce(upper(trim(cruza2)), '') || coalesce(' ' || upper(trim(letra_cruza2)), '') cruz2,
                col.nombre colonia,
                c.idcolonia,
                case 
                    when ultimo_mes_pago = '1' then 'Enero'
                    when ultimo_mes_pago = '2' then 'Febrero'
                    when ultimo_mes_pago = '3' then 'Marzo'
                    when ultimo_mes_pago = '4' then 'Abril'
                    when ultimo_mes_pago = '5' then 'Mayo'
                    when ultimo_mes_pago = '6' then 'Junio'
                    when ultimo_mes_pago = '7' then 'Julio'
                    when ultimo_mes_pago = '8' then 'Agosto'
                    when ultimo_mes_pago = '9' then 'Septiembre'
                    when ultimo_mes_pago = '10' then 'Octubre'
                    when ultimo_mes_pago = '11' then 'Noviembre'
                    when ultimo_mes_pago = '12' then 'Diciembre'
                     else '' end txtultimomes, 
                coalesce (c.ultimo_mes_pago::integer,0) ultimomes,
                coalesce(c.ultimo_anio_pago,0) ultimoanio,
                c.saldo adeudo,
                c.saldo::money adeudof, 
                c.comercio,
                c.nombre_comercio,
                concat_ws(' ', trim(c.nombres), trim(apepat), trim(apemat)) persona,
                case WHEN cobro_manual THEN
                    COALESCE(mensualidad,0)::double precision
                ELSE
                    COALESCE(col.monto,0)::double precision	END 
                mensualidad,
                case WHEN cobro_manual THEN
                    COALESCE(mensualidad,0)::money
                ELSE
                    COALESCE(col.monto,0)::money	END 
                mensualidadf,
                case when c.ultimo_anio_pago = 1900 or 
                    (select count(1) from cliente.pagos where idcliente =  c.id_cliente and activo) = 0 or
                    (select date(fecha_pago) from cliente.pagos where idcliente = c.id_cliente and activo order by fecha_pago desc limit 1) < '2020-07-14'
                then true else false end actualizar_fecha_pago,
                c.vigente,
                c.idmunicipio,
                c.privada,
                c.nombre_privada,
                ec.nombre estatus_cliente,
                c.idestatuscliente,
                ec.sigla sigla_estatus_cliente,
                ec.permcobro,
                ec.iscobro_app,
                ec.permmen,
                ec.estatus_cliente_supervisor,
                ec.estatus_cliente_cobratario,
                c.negociable,
                c.correo,
                c.idruta,
                my.nombre municipio
            from cliente.cliente c
            left join cliente.colonia col on c.idcolonia = col.id 
            left join cliente.estatus_cliente ec on c.idestatuscliente = ec.id
            left join territorio.municipios_yucatan my on c.idmunicipio = my.cve_mun
            where c.activo = true and c.idmunicipio = $idmunicipio $where 
            order by calle, calle_letra, numero, numero_letra, colonia";
//                    $this->logger->info($sql);
            $qs = GenericSQL::getBySQL($sql);
            $perfiles = $this->getProfiles($strIdUser);
            foreach ($qs as $r){
                $isBlocked = in_array($r->id, (array)$this->config->application->blockedclient);
                $ResultadoAdeudo = 0;
                //si hay ultimo año y ultimo mes se calcula el adeudo
                if ($r->ultimoanio > 0 && $r->ultimomes > 0) {
                    if($r->ultimoanio == date("Y")) {
                        $ResultadoAdeudo = (date("m") - $r->ultimomes) * $r->mensualidad;
                    }
                    else {
                        $iMeseTranscurridos = 0;
                        $aniosTranscurridos = (date("Y") - $r->ultimoanio) + 1;
                        for ($i = 1; $i <= $aniosTranscurridos; $i++) {
                            //Año inicial
                            if ($i == 1) {
                                $iMeseTranscurridos = $iMeseTranscurridos + (12 - $r->ultimomes);
                            }
                            else {
                                //Año final
                                if ($i== $aniosTranscurridos) {
                                    $iMeseTranscurridos = $iMeseTranscurridos + (date("m"));
                                }
                                //Años Intemedios
                                else {
                                    $iMeseTranscurridos = $iMeseTranscurridos + 12;
                                }
                            }
                        }
                        $ResultadoAdeudo = $iMeseTranscurridos * $r->mensualidad;
                    }
                }

                $calle = ($r->calle != null and $r->calle != "") ? trim($r->calle) : "";
                $calleLetra = ($r->calle_letra != null and $r->calle_letra != "") ? " ".trim($r->calle_letra) : "";
                $calle = $calle.$calleLetra;

                $numero = ($r->numero != null and $r->numero != "") ? trim($r->numero) : "";
                $numeroLetra = ($r->numero_letra != null and $r->numero_letra != "") ? " ".trim($r->numero_letra) : "";
                $numero = $numero.$numeroLetra;

                $cruz1 = $r->cruz1;
                $cruz2 = $r->cruz2;

                $colonia = $r->colonia;

                $cruzjoin = "";
                if($cruz1 && $cruz2){
                    $cruzjoin .= " X ".$cruz1." Y ".$cruz2." ";
                }
                else{
                    if($cruz1){
                        $cruzjoin .= " X ".$cruz1." ";
                    }
                    if($cruz2){
                        $cruzjoin .= " X ".$cruz2." ";
                    }
                }
                $dir = "C. ".$calle." #".$numero.$cruzjoin.$colonia.
                    ($isBlocked ? '. Nota: Favor de pasar a la oficina matriz (Melitón Salazar) a pagar' : '');

                $r->direccion = $dir;
                $r->adeudo = $ResultadoAdeudo;
                $r->tarifar = $this->acl->isAllowedUserNosesion('clientes', $r->permmen, $perfiles);
                $r->actualizar_fecha_pago = $r->actualizar_fecha_pago || $this->acl->isAllowedUserNosesion('clientes', 'permavmesanio', $perfiles);
                $r->cobrar = !$isBlocked && $r->iscobro_app == true && $this->acl->isAllowedUserNosesion('caja', $r->permcobro, $perfiles);
                $r->adeudof = Util::formatToCurrency($ResultadoAdeudo);

                $wherees = "";
                $siglas = "";
                $estatuses = array();
                if($user->supervisor) {
                    if ($r->estatus_cliente_supervisor) {
                        $siglas .= $r->estatus_cliente_supervisor;
                    }
                }

                if($user->cobratario) {
                    if ($r->estatus_cliente_cobratario) {
                        $siglas .= ($siglas ? "," : "") . $r->estatus_cliente_cobratario;
                    }
                }

                $r->estatus_cambiar = false;
                if($siglas){
                    $auxa = explode(",", $siglas);
                    foreach ($auxa as $a){
                        $wherees .= "'".$a."'".",";
                    }
                    $wherees =  " and sigla in (".trim($wherees, ",").")";

                    $estatuses = Estatus_cliente::find([
                        "columns" => "id, nombre, sigla, permcobro, permmen",
                        "activo = true".$wherees,
                        "order" => "orden"
                    ]);
                    $r->estatus_cambiar = true;
                }
                $r->estatus_a_cambiar = $estatuses;
                $r->anclar = $this->acl->isAllowedUserNosesion('clientes', "permmanclar", $perfiles);
            }
            $this->response->setContent(json_encode($qs));
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    public function coloniasAction($idmun){
        if($this->request->isGet()){
            if(isset($idmun)){
                if(!empty($idmun) && is_numeric($idmun)){
                    $this->response->setContent(json_encode(Colonias::find([
                        "cve_mun = ".$idmun." and activo = true",
                        "order" => "nombre"
                    ])));
                }
                else{
                    $this->response->setStatusCode(404);
                }
            }
            else{
                $this->response->setContent(json_encode(Colonias::findByActivo()));
            }
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    public function historialAction($id){
        $resp = $this->getHistorial($id);
        $this->response->setContent(json_encode($resp));

        return $this->response;
    }

    //apiv2/cobrar/idcliente ejemplo: apiv2/cobrar/80259
    public function cobrarAction($idcliente){
        ignore_user_abort(false);
        $this->view->disable();

        $arrayId = array();
        $bSuccess = TRUE;
        $idPago = 0;
        $resp = new \stdClass();

        if ($this->request->isPost() == true){
            list($token, $strDev, $strIdUser) = $this->getUserdata();
            $oUser = Users::findFirst($strIdUser);
            if($oUser){
                if(!$oUser->activo){
                    $this->response->setStatusCode(401);
                    return $this->response;
                }
            }
            $oCli = Clientes::findFirst($idcliente);
            $rawBody = $this->request->getJsonRawBody();
            $meses = $rawBody->mesesaPagar;
            $rFolio = $rawBody->folio;
            $pago = $rawBody->pago;
            $descanual = isset($rawBody->descanual) ? $rawBody->descanual : false;
            $totalPromoAnual = isset($rawBody->totalPromoAnual) ? $rawBody->totalPromoAnual : 0;


            //negociacion
            $isnegociacion = isset($rawBody->isnegociacion) ? $rawBody->isnegociacion : false;
            $totalDescNegociacion = isset($rawBody->totalDescNegociacion) ? $rawBody->totalDescNegociacion : 0;
            $totalPago = isset($rawBody->totalPago) ? $rawBody->totalPago : 0;

            $actualizarMensualidad = isset($rawBody->actualizarMensualidad) ? $rawBody->actualizarMensualidad : false;
            $newMensualidad = isset($rawBody->actualizarMensualidad) ? $rawBody->newMensualidad : 0;

            list($token, $strDev, $strIdUser) = $this->getUserdata();

            $this->db->begin();
            //Generar el objeto para guardar el Pago
            $oPago = new Pagos();
            $oPago->fecha_modificacion = date("c");
            $oPago->fecha_creacion = date("c");
            $oPago->fecha_pago = date("c");
            $oPago->idusuario = $strIdUser;
            $oPago->idmotivo = null;
            $oPago->ispagoanual = $descanual;
            $oPago->totpagoanual = $totalPromoAnual;

            $oPago->isnegociacion = $isnegociacion;
                //Obtener los datos para gaurdar el pago
            if(count($meses) > 0){
                if($rFolio){
                    $folioInterno = $rFolio;
                }
                else{
                    $foliador = Foliador::getNextFolio($strIdUser, $idcliente);
                    $folioInterno = $foliador->folio_anio;
                }

                $folfac = null;
                $referencia = null;
                $idformapago = 1;
                $idcliente = intval($idcliente);

                $objFolio = new Folios();
                $objFolio->folio = $folioInterno;
                $objFolio->serie_folio = $folioInterno;
                $objFolio->idusuario = $strIdUser;
                $objFolio->idusuario_asigno = $strIdUser;
                $objFolio->idcobratario = $strIdUser;
                $objFolio->origen = 'APPV2';
//                $this->logger->info('1');

                if(!$objFolio->save()){
                    $this->db->rollback();
                    $this->response->setStatusCode(500, "Internal Server Error");
                    foreach ($objFolio->getMessages() as $message) {
                        $this->logger->info("(save-folio): " . $message);
                    }
                    return $this->response;
                }
//                $this->logger->info('2');
                $objFolio->refresh();
                //Asignacion de valores para realizar el guardado del Pago
                $oPago->assign(array(
                    "idcliente" => $idcliente,
                    "folio_factura" => $folfac,
                    "referencia_bancaria" => $referencia,
                    "idforma_pago" => $idformapago,
                    "activo" => TRUE,
                    "latitud" => $rawBody->latitud,
                    "longitud" => $rawBody->longitud,
                    "folio_interno" => $folioInterno
                ));
//                $this->logger->info('3');

                //Guardado del renglon de pago
                if($oPago->save()) {

                    $idPago = $oPago->idpago;
                    $resp->success = true;

                    $cantidadLiquidar = 0;
                    for($iIdx = 0; $iIdx < count($meses); $iIdx++){
//                        $this->logger->info('4');
                        $rowJson = $meses[$iIdx];

                        //Generar el objeto para la forma de pago
                        $oHistorialPago = new HistorialPago();
                        $oHistorialPago->fecha_modificacion = date("c");
                        $oHistorialPago->fecha_creacion = date("c");

                        $mes 	   = ($rowJson->mes) ? $rowJson->mes: null;
                        $annio 	   = ($rowJson->anio) ? $rowJson->anio : null;

                        $descuento = ($rowJson->descuento) ? $rowJson->descuento : 0.00;
                        $iva = ($rowJson->iva) ? $rowJson->iva : 0.00;
                        $cantidad = ($rowJson->pago) ? $rowJson->pago : 0.00;
                        $fecha = date( 'c');
                        $formapago = $rowJson->idforma_pago ? $rowJson->idforma_pago: 0;
                        $refbancaria = $rowJson->referencia_bancaria ? $rowJson->referencia_bancaria: null;
                        $idtipo_cobro = 1;
                        $idvisita_cliente = $rowJson->idvisita_cliente ? $rowJson->idvisita_cliente: null;
                        $idbanco = $rowJson->idbanco ? $rowJson->idbanco: null;
                        $idcobratario = $strIdUser;
                        $folio_factura = $rowJson->folio_factura ? $rowJson->folio_factura : null;
                        $facturado = $rowJson->facturado ? $rowJson->facturado : false;

                        if($oCli->comercio){
                            if($iIdx == (count($meses) - 1)){
                                $cantidad = $pago;
                            }
                            else{
                                $cantidad = 0;
                            }
                        }
                        $cantidadLiquidar += $cantidad;

                        $oHistorialPago->assign(array(
                            "idcliente" => $idcliente,
                            "mes"=> $mes,
                            "anio"=> $annio,
                            "descuento"=> $descuento,
                            "iva"=> $iva,
                            "cantidad"=> $cantidad,
                            "fecha_pago"=> $fecha,
                            "idforma_pago"=> $formapago,
                            "referencia_bancaria" => $refbancaria,
                            "validado" => TRUE,
                            "idusuario" => $strIdUser,
                            "facturado" => $facturado,
                            "folio_factura" => $folio_factura,
                            "idcobratario" => $idcobratario,
                            "caja" => FALSE,
                            "idbanco" => $idbanco,
                            "activo" => TRUE,
                            "envio_correo" => FALSE,
                            "recibo_impreso" => FALSE,
                            "incluido_corte" => FALSE,
                            "fecha_corte" => null,
                            "idvisita_cliente" => $idvisita_cliente,
                            "idtipo_cobro" => $idtipo_cobro,
                            "idtipo_servicio" => 0,
                            "concepto_servicio" => '',
                            "idpago" => $idPago
                        ));

                        if($oHistorialPago->save()){
                            $bSuccess = ($bSuccess && TRUE);
                            $arrayId[$iIdx] = $oHistorialPago->id;
                        }
                        else{
                            $bSuccess = ($bSuccess && FALSE);
                            break;
                        }//fin:else
                    }//fin:for($iIdx = 0; $iIdx < $countItems; $iIdx++)

                    $oPago->refresh();
                    $oPago->cantidad = $cantidadLiquidar;


                    $oPago->descuento_negociacion = 0;
                    $oPago->total_pago = $cantidadLiquidar;
                    if($isnegociacion){
                        $oPago->descuento_negociacion = $totalDescNegociacion;
                        $oPago->total_pago = $totalPago;
                    }
//                    $this->logger->info('5');
                    if($oPago->save()){
                        $bSuccess = ($bSuccess && TRUE);
                    }
                    else{
                        foreach ($oPago->getMessages() as $message) {
                            $this->logger->error("(save-update motno pago): " . $message);
                        }
                        $bSuccess = ($bSuccess && FALSE);
                    }//fin:else

//                    $this->logger->info('6');
                    $objFolio->idpago = $oPago->idpago;
                    $objFolio->monto = $cantidadLiquidar;
                    if($objFolio->save()){
                        $bSuccess = ($bSuccess && TRUE);
                    }
                    else{
                        foreach ($objFolio->getMessages() as $message) {
                            $this->logger->error("(save-folio): " . $message);
                        }
                        $bSuccess = ($bSuccess && FALSE);
                    }//fin:else

                    $rowCount = HistorialPago::findFirst([
                        "idcliente = ".$idcliente." and activo = true",
                        "order" => "anio desc, mes desc"
                    ]);
                    // $rowCount = $this->db->query("select * from cliente.historial_pago where idcliente = $idcliente and activo 
                        // order by anio::integer desc, mes::integer desc limit 1");
                    // $rowCount = $rowCount->fetch();
                    if($rowCount){
                        $oCli = Clientes::findFirst($idcliente);
                        $dataOrigin = json_encode($oCli);
                        $oCli->ultimo_mes_pago = $rowCount->mes;
                        $oCli->ultimo_anio_pago = $rowCount->anio;
                        // $oCli->ultimo_mes_pago = $rowCount["mes"];
                        // $oCli->ultimo_anio_pago = $rowCount["anio"];
                        if($isnegociacion) {
                            $oCli->negociable = false;
                            if($actualizarMensualidad){
                                $oCli->mensualidad = $newMensualidad;
                                $oCli->monto_cobro = $newMensualidad;
                                $oCli->cobro_manual = true;
                                $oCli->fecha_modificacion = date("c");
                            }
                        }
                        if(!$oCli->save()){
                            foreach ($oCli->getMessages() as $message) {
                                $this->logger->error("(save-folio-ultimo-pago-cliente): " . $message);
                            }
                            $bSuccess = ($bSuccess && FALSE);
                        }

                        if($isnegociacion) {
                            $dataB = new BitacoraCambios();
                            $dataB->identificador = $idcliente;
                            $dataB->modulo = 'NEGOCIACION';
                            $dataB->accion = 'ACTUALIZAR MENSUALIDAD';
                            $dataB->idusuario = $strIdUser;
                            $dataB->tabla = "cliente.cliente";
                            $dataB->cambios = json_encode($oCli);
                            $dataB->original = $dataOrigin;
                            $dataB->apartado = "APP";

                            if (!$dataB->save()) {
                                foreach ($dataB->getMessages() as $message) {
                                    $this->logger->info("(apiv2-actualizar-mensualdiad-bitacora): " . $message);
                                }
                                $bSuccess = ($bSuccess && FALSE);
                            }
                        }
                    }

//                    $this->logger->info('7');
                    $historial = $this->getHistorial($idcliente);
                    $this->response->setContent(json_encode($historial));
                    $oPago->refresh();
                    if($bSuccess) {
                        if($isnegociacion){
                            $mail = new Mail();
                            $emailJuridico = $this->config->application->emailJuridico;
                            $subject = 'Negociación';
                            $params = [
                                "fecha" => Util::formatDateTimeForView($oPago->fecha_creacion),
                                "cobratario" => $oUser->getFullName(),
                                "total" => Util::formatToCurrency($oPago->total_pago),
                                "descuento" => Util::formatToCurrency($oPago->descuento_negociacion),
                                "totalpagado" => Util::formatToCurrency($oPago->cantidad),
                                "idcliente" => $idcliente,
                            ];
                            $mail->send($emailJuridico, $subject, "negociado", $params);
                        }
                        $this->db->commit();
                    }
                    else {
                        $this->db->rollback();
                        $this->response->setStatusCode(500, "Internal Server Error");
                    }
                }//fin:if($oPago->save())
                else{
                    $this->db->rollback();
                    $this->response->setStatusCode(500, "Internal Server Error");
                }//fin:else
            }//fin:if($countItems > 0)
            else{
                $this->logger->info('options <=1=> count');
                $this->db->rollback();
                $this->response->setStatusCode(400);
            }
        }//fin:if ($this->request->isPost() == true)
        else{
            $this->logger->info('options <=3=> post');
            $this->response->setStatusCode(400);
        }
        return $this->response;
    }

    public function actualizarpagoAction($idcliente){
        $oCliente = Clientes::findFirstById_cliente($idcliente);
        $rawBody = $this->request->getJsonRawBody();
        list($token, $strDev, $strIdUser) = $this->getUserdata();
        $this->db->begin();

        $oBit = new BitacoraActualizarPago();
        $oBit->idcliente = $idcliente;
        $oBit->mes_actual = $oCliente->ultimo_mes_pago;
        $oBit->anio_actual = $oCliente->ultimo_anio_pago;
        $oBit->anio_nuevo = $rawBody->anio;
        $oBit->mes_nuevo = $rawBody->mes;
        $montoCobro = $oCliente->mensualidad;
        if($oCliente->cobro_manual){
            $oColonia = Colonias::findFirstById($oCliente->idcolonia);
            $montoCobro = $oColonia->monto;
        }
        $oBit->pago = $montoCobro;
        $oBit->idusuario = $strIdUser;

        if(!$oBit->save()){
            $this->db->rollback();
            $this->response->setStatusCode(500, "Internal Server Error");
            foreach ($oBit->getMessages() as $message) {
                $this->logger->info("(save-bitacora-fecha-pago): " . $message);
            }
            return $this->response;
        }

        $oCliente->ultimo_anio_pago = $rawBody->anio;
        $oCliente->ultimo_mes_pago = $rawBody->mes;
        $oCliente->fecha_modificacion = date("c");

        if(!$oCliente->save()){
            $this->db->rollback();
            $this->response->setStatusCode(500, "Internal Server Error");
            foreach ($oCliente->getMessages() as $message) {
                $this->logger->info("(save-actualizar-fecha-pago): " . $message);
            }
            return $this->response;
        }

        $this->db->commit();

        $sql = "select 
                id_cliente id, 
                coalesce(upper(trim(calle)), '') || coalesce(' ' || upper(trim(calle_letra)), '') calle,
                coalesce(upper(trim(numero)), '') || coalesce(' ' || upper(trim(numero_letra)), '') || coalesce(' ' || upper(trim(tipo_numero)), '') numero,
                coalesce(upper(trim(cruza1)), '') || coalesce(' ' || upper(trim(letra_cruza1)), '') cruz1,
                coalesce(upper(trim(cruza2)), '') || coalesce(' ' || upper(trim(letra_cruza2)), '') cruz2,
                col.nombre colonia,
                c.idcolonia,
                case 
                    when ultimo_mes_pago = '1' then 'Enero'
                    when ultimo_mes_pago = '2' then 'Febrero'
                    when ultimo_mes_pago = '3' then 'Marzo'
                    when ultimo_mes_pago = '4' then 'Abril'
                    when ultimo_mes_pago = '5' then 'Mayo'
                    when ultimo_mes_pago = '6' then 'Junio'
                    when ultimo_mes_pago = '7' then 'Julio'
                    when ultimo_mes_pago = '8' then 'Agosto'
                    when ultimo_mes_pago = '9' then 'Septiembre'
                    when ultimo_mes_pago = '10' then 'Octubre'
                    when ultimo_mes_pago = '11' then 'Noviembre'
                    when ultimo_mes_pago = '12' then 'Diciembre' end txtultimomes, 
                c.ultimo_mes_pago::integer ultimomes,
                c.ultimo_anio_pago ultimoanio,
                c.saldo adeudo,
                c.saldo::money adeudof, 
                c.comercio,
                c.nombre_comercio,
                concat_ws(' ', trim(c.nombres), trim(apepat), trim(apemat)) persona,
                case WHEN cobro_manual THEN
                    COALESCE(mensualidad,0)::double precision
                ELSE
                    COALESCE(col.monto,0)::double precision	END 
                mensualidad,
                case WHEN cobro_manual THEN
                    COALESCE(mensualidad,0)::money
                ELSE
                    COALESCE(col.monto,0)::money	END 
                mensualidadf,
                case when c.ultimo_anio_pago = 1900 or 
                    (select date(fecha_pago) from cliente.pagos where idcliente = c.id_cliente and activo order by fecha_pago desc limit 1) < '2020-07-14'
                then true else false end actualizar_fecha_pago
            from cliente.cliente c
            left join cliente.colonia col on c.idcolonia = col.id 
            where c.activo = true and c.vigente = true and id_cliente = $idcliente 
            order by calle, calle_letra, numero, numero_letra, colonia";
//                    $this->logger->info($sql);
        $qs = GenericSQL::getBySQL($sql);

        $this->response->setContent(json_encode($qs[0]));

        return $this->response;
    }

    public function getPersonaAction($id) {
        $this->view->disable();
        if ($this->request->isGet() == true) {
            $cliente = Clientes::findFirstById_cliente($id);
            if($cliente != false){

                $row = new \stdClass();
//                direccion de servicio
                $row->id = $cliente->id_cliente;
                $row->calle = $cliente->calle;
                $row->calle_letra = $cliente->calle_letra;
                $row->numero = $cliente->numero;
                $row->numero_letra = $cliente->numero_letra;
                $row->cruza1 = $cliente->cruza1;
                $row->cruza1_letra = $cliente->letra_cruza1;
                $row->cruza2 = $cliente->cruza2;
                $row->cruza2_letra = $cliente->letra_cruza2;
                $row->idcolonia = $cliente->idcolonia;
                $row->referencia_ubicacion = $cliente->referencia_ubicacion;
                $row->cp = $cliente->cp;
                $row->mensualidad = $cliente->mensualidad;
                $row->ultimo_anio_pago = $cliente->ultimo_anio_pago;
                $row->ultimo_mes_pago = $cliente->ultimo_mes_pago;
                $row->cobro_manual = $cliente->cobro_manual;
                $row->marginado = $cliente->marginado;
                $row->comercio = $cliente->comercio;
                $row->abandonado = $cliente->abandonado;
                $row->privada = $cliente->privada;
                $row->nombre_privada = $cliente->nombre_privada;
                $row->nombre_comercio = $cliente->nombre_comercio;

//                ubicacion
                $row->latitud = $cliente->latitud;
                $row->longitud = $cliente->longitud;

//                persona
                $row->apepat = $cliente->apepat;
                $row->apemat = $cliente->apemat;
                $row->nombres = $cliente->nombres;
                $row->fisica = $cliente->fisica;
                $row->nombre_moral = $cliente->nombre_moral;
                $row->rfc = $cliente->rfc;
                $row->direccion_moral = $cliente->direccion_moral;

//                propietario
                $row->apepat_propietario = $cliente->apepat_propietario;
                $row->apemat_propietario = $cliente->apemat_propietario;
                $row->nombres_propietario = $cliente->nombres_propietario;
                $row->folio_catastral = $cliente->folio_catastral;

//                requisitos
                $row->id_tipo_comprobante = $cliente->id_tipo_comprobante;
                $row->ruta_comprobante = $cliente->ruta_comprobante;
                $row->id_tipo_identificacion = $cliente->id_tipo_identificacion;
                $row->ruta_identificacion = $cliente->ruta_identificacion;
                $row->ruta_foto_predio = $cliente->ruta_foto_predio;
                $row->telefono_contacto = $cliente->telefono;
                $row->telefono = $cliente->telefono;
                $row->correo = $cliente->correo;
                $row->idestatuscliente = $cliente->idestatuscliente;


//                otros
                $row->observacion = $cliente->observacion;
                $row->direccion_otro = $cliente->direccion_otro;
                $row->idruta = $cliente->idruta;

                $row->vigente = $cliente->vigente;

                $this->response->setContent(json_encode($row));
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    public function savepersonaAction(){
        $this->view->disable();

        if ($this->request->isPost() == true) {
            $rawBody = $this->request->getJsonRawBody();
            list($token, $strDev, $idUser) = $this->getUserdata();
            //$form = new ClientesForm();
            $idCliente = $rawBody->id;
            $resp = new \stdClass();
            $isnew = false;
            $this->db->begin();
            $dataB = new BitacoraCambios();
            $dataB->modulo = 'CLIENTE';
            $dataB->tabla = "cliente.cliente";
            $dataB->idusuario = $idUser;

            $mensualidad = $rawBody->mensualidad;
            $comercio = $rawBody->comercio;
            $oUser = Users::findFirst($idUser);
            $odlCLiente = null;


            $perfiles = $this->getProfiles($idUser);
            if($idCliente != null){
                $resp->nuevo = false;
                $dataB->accion = 'EDICION CLIENTE';
                $clientes = Clientes::findFirstById_cliente($idCliente);
                $odlCLiente = Clientes::findFirstById_cliente($idCliente);
                if($clientes === false){
                    $this->response->setStatusCode(404, "Not Found");
                    return $this->response;
                }
                $clientes->fecha_modificacion = date("c");
                $dataOrigin = json_encode($clientes);

                $peredapp = $this->acl->isAllowedUserNosesion('clientes', 'percliedapp', $perfiles);
                //$peredapp = true;
                if(!$peredapp){
                    $this->response->setStatusCode(500, "No cuenta con permisos de edicion de cliente");
                    return $this->response;
                }

            }
            else{
                $dataB->accion = 'ALTA CLIENTE';
                $dataOrigin = null;
                $isnew = true;
                $resp->nuevo = true;
                $clientes = new Clientes();
                $clientes->id_usuario = $idUser;
                $clientes->fecha_modificacion = date("c");
                $clientes->fecha_creacion = date("c");
                $idestatuscliente = $rawBody->idestatuscliente;
                
                $clientes->idestatuscliente = $idestatuscliente;
                $oEstatusCliente = Estatus_cliente::findFirstById($idestatuscliente);

                //Datosadicionales
                $monto_cobro = $mensualidad;
                $cobro_manual = false;
                $clientes->mensualidad = $mensualidad;
                $clientes->monto_cobro = $mensualidad;
                $clientes->cobro_manual = false;
            }
            $dataB->original = $dataOrigin;

            $clientes->idusuario_modifico = $idUser;

//            direccion de servicio
            $calle = $rawBody->calle;
            $calleLetra = trim($rawBody->calle_letra);
            $calleLetra = $calleLetra ? $calleLetra : null;
            $numero = $rawBody->numero;
            $numeroLetra = trim($rawBody->numero_letra);
            $numeroLetra = $numeroLetra ? $numeroLetra : null;
            $cruza1 = $rawBody->cruza1;
            $cruza1Letra = $rawBody->cruza1_letra;
            $cruza2 = $rawBody->cruza2;
            $cruza2Letra = $rawBody->cruza2_letra;
            $idcolonia = $rawBody->idcolonia;
            $maginado = $rawBody->marginado;
            $abandonado = $rawBody->abandonado;
            $idruta = $rawBody->idruta;

            if($calleLetra) {
                $auxCalleLetra = trim(preg_replace("/[a-z0-9 ]+/i", "", $calleLetra));
                if ($auxCalleLetra != "") {
                    $this->response->setStatusCode(500, "Caracteres no permitidos");
                    return $this->response;
                }
            }

            if($numeroLetra) {
                $auxNumeroLetra = trim(preg_replace("/[a-z0-9 ]+/i", "", $numeroLetra));
                if ($auxNumeroLetra != "") {
                    $this->response->setStatusCode(500, "Caracteres no permitidos");
                    return $this->response;
                }
            }
            
            $privada = $rawBody->privada;
            $nombre_privada = $rawBody->nombre_privada;
            $nombre_comercio = $rawBody->nombre_comercio;
            $ultimo_anio_pago = $rawBody->ultimo_anio_pago;
            $ultimo_mes_pago = $rawBody->ultimo_mes_pago;

            $referencia_ubicacion = $rawBody->referencia_ubicacion;
            $idmunicipio = $rawBody->idmunicipio;

//            ubicacion
            $latitud = $rawBody->latitud;
            $longitud = $rawBody->longitud;

//            requisitos


//            persona
            $apepat = $rawBody->apepat;
            $apemat = $rawBody->apemat;
            $nombres = $rawBody->nombres;
            $telefono = $rawBody->telefono;
            $correo = $rawBody->correo;

//            propietario
            $apepat_propietario = $rawBody->apepat_propietario;
            $apemat_propietario = $rawBody->apemat_propietario;
            $nombres_propietario = $rawBody->nombres_propietario;

//            $folio_catastral = $this->request->getPost('folio_catastral', 'int');
//            $folio_catastral = ($folio_catastral != null and $folio_catastral != "") ? intval($folio_catastral) : null;
//            $idestatuscliente = $this->request->getPost('idestatuscliente', 'int');
//            $idestatuscliente = ($idestatuscliente != null and $idestatuscliente != "") ? intval($idestatuscliente) : null;

//            otro
            $observacion = $rawBody->observacion;



            $validaDireccion = Clientes::validateDireccionWithMuni($idCliente, $idmunicipio, null, $calle, $calleLetra, $numero, $numeroLetra, $idcolonia);
            if($validaDireccion->valid()){
                $this->response->setStatusCode(409, "Conflict");
                return $this->response;
            }



            $ubicado = ($latitud != null and $longitud != null) ? true : false;

            $clientes->assign(array(
                'calle' => $calle,
                'calle_letra' => $calleLetra,
                'numero' => $numero,
                'numero_letra' => $numeroLetra,
                'cruza1' => $cruza1,
                'letra_cruza1' => $cruza1Letra,
                'cruza2' => $cruza2,
                'letra_cruza2' => $cruza2Letra,
                'idcolonia' => $idcolonia,
                'idruta' => $idruta,
                'apepat' => $apepat,
                'apemat' => $apemat,
                'nombres' => $nombres,
                'apepat_propietario' => $apepat_propietario,
                'apemat_propietario' => $apemat_propietario,
                'nombres_propietario' => $nombres_propietario,
//                'folio_catastral' => $folio_catastral,
                'activo' => TRUE,
                'latitud' => $latitud,
                'longitud' => $longitud,
                'ubicado' => true,
                'referencia_ubicacion' => $referencia_ubicacion,
                'fisica' => true,
                'telefono' => $telefono,
                'correo' => $correo,
                //'mensualidad' => $mensualidad,
                'ultimo_anio_pago' => $ultimo_anio_pago,
                'ultimo_mes_pago' => $ultimo_mes_pago,
                'observacion' => $observacion,
                'vigente' => true,
                'ubicado' => $ubicado,
                //'cobro_manual' => $cobro_manual,
                //'monto_cobro' => $monto_cobro,
                'comercio' => $comercio,
                'abandonado' => $abandonado,
                'marginado' => $maginado,
                'privada' => $privada,
                'nombre_privada' => $nombre_privada,
                'nombre_comercio' => $nombre_comercio,
                'telefono_contacto' => $telefono,
                'idmunicipio' => $idmunicipio
            ));

//                var_dump($clientes->toArray());
//                die;

            if($clientes->save() === false) {
                foreach ($clientes->getMessages() as $message) {
                    $this->logger->info("(save-dirper): " . $message);
                }
                $this->response->setStatusCode(500);
                $this->db->rollback();
            }
            else{
                $dataB->cambios = json_encode($clientes);
                $dataB->identificador = $clientes->id_cliente;
                if ($dataB->save()) {
                    $this->db->commit();
                    $clientes->refresh();
                    if($odlCLiente != null){
                        if($oEstatusCliente->sigla == Estatus_cliente::COMERCIO_SUPERVISOR &&
                            ($odlCLiente->calle != $clientes->calle || $odlCLiente->calle_letra != $clientes->calle_letra ||
                            $odlCLiente->numero != $clientes->numero || $odlCLiente->nuermo_letra != $clientes->numero_letra ||
                            $odlCLiente->idcolonia != $clientes->idcolonia)){
                            $mail = new Mail();
                            $emailJuridico = $this->config->application->emailJuridico;
                            $emailJuridico2 = $this->config->application->emailJuridico2;
                            $subject = 'CAMBIO DE DIRECCION DE CLIENTE';
                            $params = [
                                "fecha" => Util::formatDateTimeForView($clientes->fecha_modificacion),
                                "cobratario" => $oUser->getFullName(),
                                "iscomercio" => $clientes->comercio,
                                "nombre_comercio" => $clientes->nombre_comercio,
                                "direccion" => "De ".$odlCLiente->getFormatAddress()." a ".$clientes->getFormatAddress(),
                                "idcliente" => $clientes->id_cliente,
                                "mensualidad" => "",
                                "motivo" => "",
                            ];
                            $mail->send(
                                [$emailJuridico, $emailJuridico2],
                                $subject,
                                "modcliente",
                                $params);
                        }
                    }
                    if($isnew && $oEstatusCliente->sigla == Estatus_cliente::COMERCIO_SUPERVISOR){
                        $mail = new Mail();
                        $emailJuridico = $this->config->application->emailJuridico;
                        $subject = 'ALTA COMERCIO SUPERVISOR';
                        $params = [
                            "fecha" => Util::formatDateTimeForView($clientes->fecha_creacion),
                            "cobratario" => $oUser->getFullName(),
                            "iscomercio" => $clientes->comercio,
                            "nombre_comercio" => $clientes->nombre_comercio,
                            "direccion" => $clientes->getFormatAddress(),
                            "idcliente" => $clientes->id_cliente,
                            "mensualidad" => Util::formatToCurrency($clientes->mensualidad),
                            "motivo" => "",
                        ];
                        $mail->send($emailJuridico, $subject, "modcliente", $params);
                    }
                }
                else{
                    foreach ($dataB->getMessages() as $message) {
                        $this->logger->info("(apiv2-cancelacion-pago-bitacora-pago): " . $message);
                    }
                    $this->response->setStatusCode(500);
                    $this->db->rollback();
                }
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    private function getHistorial($idcliente){
        $txtMeses = array(
            1 => "Enero",
            2 => "Febrero",
            3 => "Marzo",
            4 => "Abril",
            5 => "Mayo",
            6 => "Junio",
            7 => "Julio",
            8 => "Agosto",
            9 => "Septiembre",
            10 => "Octubre",
            11 => "Noviembre",
            12 => "Diciembre",
        );
        $pago = Pagos::findFirst([
            "idcliente = $idcliente and activo",
            "order" => "idpago desc"
        ]);

        $resp = new \stdClass();
        if($pago){
//            $sql = "select * from cliente.historial_pago
//            where idpago = ".$pago->idpago." and activo order by anio::integer desc, mes::integer asc";
//            $hps = GenericSQL::getBySQL($sql);
            $hps = HistorialPago::find([
                "idpago = ".$pago->idpago." and activo"
            ]);
            $meses = array();
            $corte = false;
            foreach ($hps as $hp){
                $corte = $hp->idcorte ? true: false;
                $data = array(
                    "cantidad" => $hp->cantidad,
                    "cantidadf" => $resp->cantidadf = Util::formatToCurrency($hp->cantidad),
                    "mes" => $txtMeses[$hp->mes]
                );
                if(isset($meses[$hp->anio])){
                    array_push($meses[$hp->anio], $data);
                }
                else{
                    $meses[$hp->anio] = [];
                    array_push($meses[$hp->anio], $data);
                }
            }
            $resp->idpago = $pago->idpago;
            $resp->cantidad = $pago->cantidad;
            $resp->cantidadf = Util::formatToCurrency($pago->cantidad);
//            $resp->cantidad_letras = Util::number_words($pago->cantidad,"pesos","y","centavos");
            $resp->cantidad_letras = NumeroLetras::convertir($pago->cantidad,"pesos","centavos",true);
            $resp->fechaf = Util::formatDateForView($pago->fecha_pago);
            $resp->fecha = $pago->fecha_pago;
            $resp->meses = $meses;
            $resp->cancelar = false;
            $resp->folio = $pago->folio_interno;
            $resp->ispagoanual = $pago->ispagoanual;
            $resp->totpagoanual = $pago->ispagoanual ? Util::formatToCurrency($pago->totpagoanual): 0;
            $resp->isnegociacion = $pago->isnegociacion;
            $resp->total_pago = $pago->total_pago ? Util::formatToCurrency($pago->total_pago) : 0;
            $resp->descuento_negociacion = $pago->descuento_negociacion ? Util::formatToCurrency($pago->descuento_negociacion) : 0;
        }

        return $resp;
    }

    public function getFolioAction($idcliente){
        list($token, $strDev, $idUser) = $this->getUserdata();
        $foliador = Foliador::getNextFolio($idUser, $idcliente);

        $this->response->setContent(json_encode($foliador));
        return $this->response;
    }

    public function cancelarAction($idcliente, $idpago){
        $resp = new \stdClass();
        $resp->success = false;
        $resp->message = "";

        list($token, $strDev, $idUser) = $this->getUserdata();
        if($this->request->isPost() == true) {
            $rawBody = $this->request->getJsonRawBody();
            $this->db->begin();
            $oPago = Pagos::findFirst($idpago);
            $dataOrigin = json_encode($oPago);
            $ultanniopago = "0";
            $ultmespago = "0";
            if ($oPago) {
                $oPago->activo = false;
                $oPago->fecha_modificacion = date("c");
                $oPago->fecha_cancelacion = date("c");
                $oPago->idusuario_cancelo = $idUser;
                $oPago->motivo_cancelacion = "CANCELACION DESDE APP";

                if ($oPago->save()) {
                    $dataB = new BitacoraCambios();
                    $dataB->identificador = $idpago;
                    $dataB->modulo = 'PAGOS';
                    $dataB->accion = 'CANCELAR PAGO';
                    $dataB->idusuario = $idUser;
                    $dataB->tabla = "cliente.pagos";
                    $dataB->cambios = json_encode($oPago);
                    $dataB->original = $dataOrigin;

                    if ($dataB->save()) {
                        $rows = GenericSQL::getBySQL("select * from cliente.historial_pago where idpago = $idpago 
                        order by anio::integer desc, mes::integer desc");
                        foreach ($rows as $r) {
                            $dp = HistorialPago::findFirst($r->id);
                            $dp->activo = false;
                            $dp->fecha_modificacion = date("c");
                            if (!$dp->save()) {
                                foreach ($dp->getMessages() as $message) {
                                    $this->logger->info("(apiv2-cancelacion-pago-detalle): " . $message);
                                }
                                $this->response->setStatusCode(500, "Internal Servere Error");
                                $this->db->rollback();
                                return $this->response;
                            }
                            $ultmespago = intval($r->mes);
                            $ultanniopago = intval($r->anio);
                        }

                        $rowCount = $this->db->query("select * from cliente.historial_pago where idcliente = $idcliente and activo 
                        order by anio::integer desc, mes::integer desc limit 1");
                        $rowCount = $rowCount->fetch();
                        if($rowCount){
                            $ultmespago = $rowCount["mes"];
                            $ultanniopago = $rowCount["anio"];
                        }
                        else {
                            if($ultmespago === 1){
                                $ultmespago = 12;
                                $ultanniopago -= 1;
                            }
                            else{
                                $ultmespago -= 1;
                            }
                        }

                        $oCli = Clientes::findFirst($idcliente);
                        $dataOrigin = json_encode($oCli);

                        $oCli->ultimo_anio_pago = $ultanniopago;
                        $oCli->ultimo_mes_pago = $ultmespago;
                        $oCli->fecha_modificacion = date("c");
                        if ($oCli->save()) {
                            $dataB = new BitacoraCambios();
                            $dataB->identificador = $idcliente;
                            $dataB->modulo = 'CLIENTE';
                            $dataB->accion = 'ACTUALIZAR ULTIMO PAGO';
                            $dataB->idusuario = $idUser;
                            $dataB->tabla = "cliente.cliente";
                            $dataB->cambios = json_encode($oCli);
                            $dataB->original = $dataOrigin;

                            if (!$dataB->save()) {
                                foreach ($dataB->getMessages() as $message) {
                                    $this->logger->info("(apiv2-cancelacion-pago-bitacora-cliente): " . $message);
                                }
                                $this->response->setStatusCode(500);
                                $this->db->rollback();
                            }
                            else{
                                $this->db->commit();
                            }
                        } else {
                            foreach ($oCli->getMessages() as $message) {
                                $this->logger->info("(apiv2-cancelacion-pago-cliente): " . $message);
                            }
                            $this->response->setStatusCode(500);
                            $this->db->rollback();
                        }
                    } else {
                        foreach ($dataB->getMessages() as $message) {
                            $this->logger->info("(apiv2-cancelacion-pago-bitacora-pago): " . $message);
                        }
                        $this->response->setStatusCode(500);
                        $this->db->rollback();
                    }
                } else {
                    foreach ($oPago->getMessages() as $message) {
                        $this->logger->info("(apiv2-cancelacion-pago): " . $message);
                    }
                    $this->response->setStatusCode(500);
                    $this->db->rollback();
                }
            } else {
                $this->db->rollback();
                $this->response->setStatusCode(404);
            }
        }
        return $this->response;
    }

    public function actualizarmensualidadAction($idcliente){
        $oCliente = Clientes::findFirstById_cliente($idcliente);
        $dataOrigin = json_encode($oCliente);
        $rawBody = $this->request->getJsonRawBody();
        list($token, $strDev, $idUser) = $this->getUserdata();
        $this->db->begin();
        $oldMensualidad = $oCliente->mensualidad;

        $oCliente->cobro_manual = true;
        $oCliente->mensualidad = $rawBody->mensualidad;
        $oCliente->fecha_modificacion = date("c");
        if(!$oCliente->save()){
            $this->db->rollback();
            $this->response->setStatusCode(500);
            foreach ($oCliente->getMessages() as $message) {
                $this->logger->info("(save-actualizar-mensualidad): " . $message);
            }
        }
        else{
            $dataB = new BitacoraCambios();
            $dataB->identificador = $idcliente;
            $dataB->modulo = 'CLIENTE';
            $dataB->accion = 'ACTUALIZAR MENSUALIDAD';
            $dataB->idusuario = $idUser;
            $dataB->tabla = "cliente.cliente";
            $dataB->cambios = json_encode($oCliente);
            $dataB->original = $dataOrigin;
            $dataB->apartado = "APP";

            if (!$dataB->save()) {
                foreach ($dataB->getMessages() as $message) {
                    $this->logger->info("(apiv2-actualizar-mensualdiad-bitacora): " . $message);
                }
                $this->response->setStatusCode(500);
                $this->db->rollback();
            }
            else{
                $this->db->commit();
                $oCliente->refresh();
                $oEstatusCliente = Estatus_cliente::findFirstById($oCliente->idestatuscliente);
                if($oEstatusCliente->sigla == Estatus_cliente::COMERCIO_SUPERVISOR){
                    $oUser = Users::findFirst($idUser);
                    $mail = new Mail();
                    $emailJuridico = $this->config->application->emailJuridico;
                    $subject = 'NUEVA TARIFA COMERCIO SUPERVISOR';
                    $params = [
                        "fecha" => Util::formatDateTimeForView($oCliente->fecha_modificacion),
                        "cobratario" => $oUser->getFullName(),
                        "iscomercio" => $oCliente->comercio,
                        "nombre_comercio" => $oCliente->nombre_comercio,
                        "direccion" => $oCliente->getFormatAddress(),
                        "idcliente" => $oCliente->id_cliente,
                        "mensualidad" => "de ".Util::formatToCurrency($oldMensualidad)." a ".Util::formatToCurrency($oCliente->mensualidad),
                        "motivo" => "",
                    ];
                    $mail->send($emailJuridico, $subject, "modcliente", $params);
                }
            }
        }
        return $this->response;
    }

    public function municipiosAction(){
        $munis = MunicipiosYucatan::find([
            "activo = true",
            "order" => "nombre asc"
        ]);
        $this->response->setContent(json_encode($munis));
        return $this->response;
    }

    public function datosclientesAction(){
        $ids = $this->request->get("ids");
    }

    public function cambiarcontraseniaAction(){
        $this->view->disable();
        if ($this->request->isPost()) {
            list($token, $strDev, $idUser) = $this->getUserdata();
            $json = $this->request->getJsonRawBody();
            $user = Users::findFirstById($idUser);

            $newPssword = $this->security->hash($json->contrasenia);
            $user->clave = $newPssword;
            $user->fecha_modificacion = date('c');
            if (!$user->update()) {
                $this->response->setStatusCode(500, "Internal Server Error");
                $this->response->setContent("Ocurrió un error actualizar la información");
            } else {
//                $this->response->setContent("La contaseña se actualizo correctamente");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
            $this->response->setContent("Método no permitido");
        }
        return $this->response;
    }

    public function promanualAction(){
        $this->view->disable();
        $proman = Promociones::getAnualVigente();
        $resp = count($proman) > 0 ? $proman[0]: null;
        $this->response->setContent(json_encode($resp));
        return $this->response;

        /*$this->view->disable();
        $proman = Promociones::getAnualVigente();
        $resp = count($proman) > 0 ? $proman[0]: "";
        $aux = new \stdClass();
        $aux->meses = "0";
        $this->response->setContent( count($proman) ? json_encode($resp) : json_encode($aux));
        return $this->response;*/
    }

    public function corteAction(){
        $this->view->disable();
        list($token, $strDev, $idUser) = $this->getUserdata();
        $sqlp = "select count(distinct(idcliente)) clis 
        from cliente.pagos 
        where activo and idcorte is null and fecha_pago::date > '2020-01-01' and idusuario = ".$idUser;
        $rowp = GenericSQL::getBySQL($sqlp);

        $sqlm = "select coalesce (sum(cantidad), 0)::money clis 
        from cliente.pagos 
        where activo and idcorte is null and fecha_pago::date > '2020-01-01' and idusuario = ".$idUser;
        $rowm = GenericSQL::getBySQL($sqlm);

        $sqlsacp = "select count(distinct(idcliente)) clis, 
        (date_trunc('week', CURRENT_DATE)::date - '1 day' :: interval)::date ini, 
        (date_trunc('week', current_date) + '5 days') ::date fin
        from cliente.pagos 
        where activo and 
        fecha_pago::date >= (date_trunc('week', CURRENT_DATE)::date - '1 day' :: interval)::date and 
        fecha_pago::date <= (date_trunc('week', current_date) + '5 days') ::date and idusuario = ".$idUser;
        $rowsacp = GenericSQL::getBySQL($sqlsacp);

        $sqlsacm = "select COALESCE(sum(cantidad),0)::money clis, 
        (date_trunc('week', CURRENT_DATE)::date - '1 day' :: interval)::date ini, 
        (date_trunc('week', current_date) + '5 days') ::date fin
        from cliente.pagos 
        where activo and 
        fecha_pago::date >= (date_trunc('week', CURRENT_DATE)::date - '1 day' :: interval)::date and 
        fecha_pago::date <= (date_trunc('week', current_date) + '5 days') ::date and idusuario = ".$idUser;
        $rowsacm = GenericSQL::getBySQL($sqlsacm);

        $sqlsanp = "select count(distinct(idcliente)) clis, 
        (date_trunc('week', CURRENT_DATE - '6 day' :: interval)::date - '1 day' :: interval)::date ini,
        (date_trunc('week', CURRENT_DATE)::date - '2 day' :: interval)::date fin
        from cliente.pagos 
        where activo and 
        fecha_pago::date >= (date_trunc('week', CURRENT_DATE - '6 day' :: interval)::date - '1 day' :: interval)::date and 
        fecha_pago::date <= (date_trunc('week', CURRENT_DATE)::date - '2 day' :: interval)::date and idusuario = ".$idUser;
        $rowsanp = GenericSQL::getBySQL($sqlsanp);

        $sqlsanm = "select COALESCE(sum(cantidad),0)::money clis, 
        (date_trunc('week', CURRENT_DATE - '6 day' :: interval)::date - '1 day' :: interval)::date ini,
        (date_trunc('week', CURRENT_DATE)::date - '2 day' :: interval)::date fin
        from cliente.pagos 
        where activo and 
        fecha_pago::date >= (date_trunc('week', CURRENT_DATE - '6 day' :: interval)::date - '1 day' :: interval)::date and 
        fecha_pago::date <= (date_trunc('week', CURRENT_DATE)::date - '2 day' :: interval)::date and idusuario = ".$idUser;
        $rowsanm = GenericSQL::getBySQL($sqlsanm);


        $resp = array(
            "predios" => $rowp[0]->clis,
            "monto" => $rowm[0]->clis,
            "semactp" => array(
                "ini" => Util::formatDateVForView($rowsacp[0]->ini),
                "fin" => Util::formatDateVForView($rowsacp[0]->fin),
                "total" => $rowsacp[0]->clis
            ),
            "semactm" => array(
                "ini" => Util::formatDateVForView($rowsacm[0]->ini),
                "fin" => Util::formatDateVForView($rowsacm[0]->fin),
                "total" => $rowsacm[0]->clis
            ),
            "semantp" => array(
                "ini" => Util::formatDateVForView($rowsanp[0]->ini),
                "fin" => Util::formatDateVForView($rowsanp[0]->fin),
                "total" => $rowsanp[0]->clis
            ),
            "semantm" => array(
                "ini" => Util::formatDateVForView($rowsanm[0]->ini),
                "fin" => Util::formatDateVForView($rowsanm[0]->fin),
                "total" => $rowsanm[0]->clis
            )
        );
        $this->response->setContent(json_encode($resp));
        return $this->response;
    }

    public function detallescorteAction(){
        $this->view->disable();
        list($token, $strDev, $idUser) = $this->getUserdata();
        $sqlp = "select coalesce (p.cantidad)::money cantidad,
        p.idcliente,
        coalesce(upper(trim(calle)), '') || coalesce(' ' || upper(trim(calle_letra)), '') calle,
        coalesce(upper(trim(numero)), '') || coalesce(' ' || upper(trim(numero_letra)), '') || coalesce(' ' || upper(trim(tipo_numero)), '') numero,
        col.nombre colonia 
        from cliente.pagos p 
        left join cliente.cliente c on p.idcliente = c.id_cliente
        left join cliente.colonia col on c.idcolonia = col.id 
        where p.activo and idcorte is null and fecha_pago::date > '2020-01-01' and idusuario =".$idUser;
        $rowp = GenericSQL::getBySQL($sqlp);

        $this->response->setContent(json_encode($rowp));
        return $this->response;
    }

    public function cambiarestatusAction($idcliente){
        list($token, $strDev, $idUser) = $this->getUserdata();
        $json = $this->request->getJsonRawBody();
        $response = Estatus_cliente::fullSave($idcliente, $idUser, "APP", $json);
        if($response->error) {
            $this->response->setStatusCode($response->errorCode);
        }
        return $this->response;
    }

    public function estatusAltaAction(){
        $this->view->disable();
        $munis = Estatus_cliente::find([
            "activo = true and alta_app = true",
            "order" => "orden asc"
        ]);
        $this->response->setContent(json_encode($munis));
        return $this->response;
    }

    public function countImpresionesAction($idcliente, $idpago){
        $this->view->disable();
        list($token, $strDev, $idUser) = $this->getUserdata();
        $oPago = Pagos::findFirst($idpago);

        $this->db->begin();
        $oBitacoraImpresiones = new BitacoraImpresiones();
        $oBitacoraImpresiones->idcliente = $idcliente;
        $oBitacoraImpresiones->idpago = $idpago;
        $oBitacoraImpresiones->idusuario = $idUser;
        $oBitacoraImpresiones->origen = "APP-IMPRIMIR";

        if(!$oBitacoraImpresiones->save()){
            $this->db->rollback();
            $this->response->setStatusCode(500);
            foreach ($oBitacoraImpresiones->getMessages() as $message) {
                $this->logger->info("(save-bitacora-count-impresiones): " . $message);
            }
        }
        else{
            if($oPago->impresiones)
                $oPago->impresiones += 1;
            else{
                $oPago->impresiones = 1;
            }
            if($oPago->save()){
                $this->db->commit();
            }
            else{
                $this->db->rollback();
                $this->response->setStatusCode(500);
                foreach ($oPago->getMessages() as $message) {
                    $this->logger->info("(save-bitacora-count-impresiones-update-count): " . $message);
                }
            }
        }
        return $this->response;
    }

    public function sendEmailReciboAction($idCliente, $idPago){

        $this->view->disable();
        if($this->request->isPost()){
            list($token, $strDev, $idUser) = $this->getUserdata();
            $this->response->setContentType("application/json");
            $rawBody = $this->request->getJsonRawBody();
            $correo = $rawBody->correo;

            $c = Clientes::findFirstById_cliente($idCliente);
            $oMun = MunicipiosYucatan::findFirstByCve_mun($c->idmunicipio);
            if($c){
                $nombre = $c->getFUllName();


                $direccion = $c->getFormatAddress();

                $pago = Pagos::findFirst("idpago = ".$idPago);

                if($pago){
                    $fecha_pago = date("d/m/Y H:i A", strtotime(str_replace('/', '-',$pago->fecha_pago)));

                    $HistorialPago = HistorialPago::find([
                        "idpago = $idPago",
                        "order" => "id"
                    ]);

                    if($HistorialPago) {
                        $mesesString = '';

                        $sumdec = 0;
                        $sumtotal = 0;

                        $mesesPagados = array();
                        foreach ($HistorialPago as $hp) {
                            $data = Util::getMonthNumberToString($hp->mes);
                            if(isset($mesesPagados[$hp->anio])){
                                array_push($mesesPagados[$hp->anio], $data);
                            }
                            else{
                                $mesesPagados[$hp->anio] = [];
                                array_push($mesesPagados[$hp->anio], $data);
                            }
                            $sumdec += $hp->descuento;
                            $sumtotal += $hp->cantidad;
                        }


                        foreach ($mesesPagados as $aa => $auxanio){
                            $mesesString .= $aa . ': ';
                            foreach ($auxanio as $am){
                                $mesesString .= $am . ', ';
                            }
                            $mesesString = trim($mesesString, ', ').'<br>';
                        }

                        $montoPromo = 0;
                        if($pago->ispagoanual){
                            $montoPromo = $pago->totpagoanual;
                        }

                        $mailSettings = $this->config->mailrecibocobranza;
                        $transport = NULL;

                        $message = Message::newInstance();
                        $message->setSubject("Recibo de pago");
                        if ($correo) {
                            $message->setTo($correo);
                        }

                        $message->setFrom(array($mailSettings->fromEmail => $mailSettings->fromName));

                        $bodymsg = $this->view->getPartial("layouts/recibocobranza",
                            [
                                "folio" => $pago->folio_interno,
                                "nombre" => $nombre,
                                "direccion" => $direccion,
                                "idcliente" => $idCliente,
                                "fecha_pago" => $fecha_pago,
                                "meses" => $mesesString,
                                "municipio" => $oMun->nombre,
                                "ispagoanual" => $pago->ispagoanual,
                                "monto_anual" => Util::formatToCurrency($montoPromo),
                                "cantidad_letras" => NumeroLetras::convertir($pago->cantidad,"pesos","centavos",true),
                                "cantidad" => Util::formatToCurrency($pago->cantidad),
                                "descuento" => $sumdec,
                                "format_descuento" => Util::formatToCurrency($sumdec),
                                "isnegociacion" => $pago->isnegociacion,
                                "total_pago" => $pago->total_pago ? Util::formatToCurrency($pago->total_pago) : ($sumtotal > 0 ? Util::formatToCurrency($sumtotal) : 0),
                                "descuento_negociacion" => $pago->descuento_negociacion ? Util::formatToCurrency($pago->descuento_negociacion) : 0,
                            ]
                        );
                        $message->setBody($bodymsg, 'text/html');

                        $html2pdf = new html2Pdf('P','LETTER','es');
                        $html2pdf->WriteHTML($bodymsg);
                        $content_PDF = $html2pdf->Output('', true);

                        if (!$transport) {
                            $transport = Smtp::newInstance(
                                $mailSettings->smtp->server,
                                $mailSettings->smtp->port,
                                $mailSettings->smtp->security
                            );
                            $transport->setUsername($mailSettings->smtp->username);
                            $transport->setPassword($mailSettings->smtp->password);
                        }
                        // Create the Mailer using your created Transport
                        $mailer = \Swift_Mailer::newInstance($transport);
                        $correoNoEnviador = array();

                        $attachment = \Swift_Attachment::newInstance($content_PDF, 'recibo.pdf', 'application/pdf');
                        $message->attach($attachment);

                        if(!$mailer->send($message, $correoNoEnviador)){

                            $this->logger->error("No se envio el recibo");
                            $this->response->setStatusCode(500);
                            return $this->response;
                        }
                        else{
                            if($c->correo != $correo){
                                $c->correo = $correo;
                                $c->fecha_modificacion = date("c");
                                #savec = true;
                                if(!$c->save()){
                                    foreach ($c->getMessages() as $message) {
                                        $this->logger->info("(senemail-save-cliente-email): " . $message);
                                    }
                                }
                            }
                            $this->db->begin();
                            $oBitacoraImpresiones = new BitacoraImpresiones();
                            $oBitacoraImpresiones->idcliente = $idCliente;
                            $oBitacoraImpresiones->idpago = $idPago;
                            $oBitacoraImpresiones->idusuario = $idUser;
                            $oBitacoraImpresiones->origen = "APP-CORREO";

                            if(!$oBitacoraImpresiones->save()){
                                $this->db->rollback();
                                foreach ($oBitacoraImpresiones->getMessages() as $message) {
                                    $this->logger->info("(senemail-save-bitacora-count-impresiones): " . $message);
                                }
                            }
                            else{
                                if($pago->impresiones)
                                    $pago->impresiones += 1;
                                else{
                                    $pago->impresiones = 1;
                                }
                                if($pago->save()){
                                    $historial = $this->getHistorial($idCliente);
                                    $this->response->setContent(json_encode($historial));
                                    $this->db->commit();
                                }
                                else{
                                    $this->db->rollback();
                                    foreach ($pago->getMessages() as $message) {
                                        $this->logger->info("(senemail-save-bitacora-count-impresiones-update-count): " . $message);
                                    }
                                }
                            }
                            return $this->response;
                        }
                    }
                    else{
                        //$this->logger->info(' mal 1 ');
                        $this->logger->info("Ocurrio un error al obtener el historial de pagos.");
                        $this->response->setStatusCode(500);
                        return $this->response;
                    }
                }
                else{
                    //$this->logger->info(' mal 2 ');
                    $this->logger->info("Ocurrio un error al obtener los datos del pago.");
                    $this->response->setStatusCode(500);
                    return $this->response;
                }
            }
            else{
                //$this->logger->info(' mal 3 ');
                $this->logger->info("Ocurrio un error al obtener los datos del cliente.");
                $this->response->setStatusCode(500);
                return $this->response;
            }

        }
        else {
            //$this->logger->info(' mal 4 ');
            $this->logger->info("Ocurrio un error al enviar el correo.");
            $this->response->setStatusCode(500);
            return $this->response;
        }
    }

    public function rutasAction(){
        $this->view->disable();
        $rutsa = Ruta::find([
            "activo = true",
            "order" => "nombre asc",
            "columns" => "id,nombre"
        ]);
        $this->response->setContent(json_encode($rutsa));
        return $this->response;
    }

    public function permisosAction(){
        $this->view->disable();
        list($token, $strDev, $strIdUser) = $this->getUserdata();
        $perfiles = $this->getProfiles($strIdUser);

        $resp = array(
            "canAddPerson" => $this->acl->isAllowedUserNosesion('clientes', 'percliaddapp', $perfiles),
            "canEditPerson" => $this->acl->isAllowedUserNosesion('clientes', 'percliedapp', $perfiles),
            "rutaObligatoria" => $this->config->application->rutaObligatoria
        );
        $this->response->setContent(json_encode($resp));
        return $this->response;
    }
}
