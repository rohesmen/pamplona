<?php
namespace Vokuro\Controllers;
use Vokuro\DT\SSPGEO;
use Vokuro\Models\Cobratario;
use Vokuro\Models\Dispositivos;
use Vokuro\Models\Users;
use Vokuro\Models\UsuarioDispositivoSeguimiento;

/**
 * Display the default index page.
 */
class DispositivosController extends ControllerBase
{

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function indexAction()
    {
        $this->view->setTemplateBefore('public');
        $this->view->setVar('cobratarios', Users::findByActivo());
        $conacciones = 'no';
        if($this->acl->isAllowedUser('dispositivos', 'new')){
            $conacciones = 'si';
        }
        if($this->acl->isAllowedUser('dispositivos', 'edit')){
            $conacciones = 'si';
        }
        if($this->acl->isAllowedUser('dispositivos', 'delete')){
            $conacciones = 'si';
        }
        if($this->acl->isAllowedUser('dispositivos', 'info')){
            $conacciones = 'si';
        }
        if($this->acl->isAllowedUser('dispositivos', 'history')){
            $conacciones = 'si';
        }
        $this->view->setVar('coacciones', $conacciones);
    }

    public function buscarAction(){
        $request = $this->request;
        $response = $this->response;


        $columns = array(
            array( 'db' => '', 'dt' => 0,
                'formatter' => function( $d, $row ) {

                    return null;
                }
            ),
            array( 'db' => '', 'dt' => 1),
            array( 'db' => 'activo', 'datatype' => 'boolean', 'dt' => 2,
                'formatter' => function( $d, $row ) {
                    $vigente = $d ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                        : '<i class="fa fa-remove" style="color: red" title="Inactivo"></i>';
                    return $vigente;
                }
            ),
            array( 'db' => 'id', 'dt' => 3, 'datatype' => 'number'),
            array( 'db' => 'dispositivo', 'dt' => 4),
            array( 'db' => 'usuario', 'dt' => 5),
            array( 'db' => 'descripcion', 'dt' => 6),
            array( 'db' => 'idusuario', 'dt' => 7, 'datatype' => 'number'),
            array( 'db' => 'activo', 'datatype' => 'boolean', 'dt' => 8),
        );

        $data = SSPGEO::complex_geo($this->request->get(), "tracking.dispositivos", "id", $columns);

//        $where = SSPGEO::filter_join_data( $this->request->get(), $columns, $bindings );



        $auxdata = array();
        foreach($data["data"] as $ds){
            $buttons = '';
            if($this->acl->isAllowedUser('dispositivos', 'info')){
                $buttons .= '<button class="btn btn-primary btn-sm dispositivos-info" data-id="{id}" type="button" title="Consultar">
                <i class="fa fa-info"></i>
            </button> ';
            }
            if($this->acl->isAllowedUser('dispositivos', 'edit')){
                $buttons .= '<button class="btn btn-primary btn-sm dispositivos-edit" data-id="{id}" type="button" title="Editar">
                <i class="fa fa-pencil"></i>
            </button> ';
            }
            if($this->acl->isAllowedUser('dispositivos', 'history')){
                $buttons .= '<button class="btn btn-primary btn-sm dispositivos-history" data-id="{id}" type="button" title="Consultar usuarios">
                <i class="fa fa-list"></i>
            </button> ';
            }
            if($this->acl->isAllowedUser('dispositivos', 'delete') && $ds[8] === true){
                $buttons .= '<button class="btn btn-danger btn-sm dispositivos-delete" data-id="{id}" type="button" title="¿Desea eliminar?">
                <i class="fa fa-times"></i>
            </button> ';
            }
            $ds[0] = str_replace("{id}", $ds[3], $buttons);
            array_push($auxdata, $ds);
        }

        $data["data"] = $auxdata;
        $response->setContent(json_encode($data));
        return $response;
    }

    public function saveAction()
    {
        $data = $this->request->getJsonRawBody();
        $id = $data->id;
        $dispisitivo = $data->dispisitivo;
        $cobratario = $data->cobratario;
        $descripcion = $data->descripcion;

        $crearsegui = false;

        if($id){
            $md = Dispositivos::findFirstById($id);
            $amd = Dispositivos::findFirstByDispositivo($dispisitivo);
            if($amd && $md->id != $id){

                $this->response->setStatuscode(409, "Ya existe un registro ".($amd->activo ? "vigente" : "no vigente")." con el mismo dispositivo");
                return $this->response;
            }
            $md->activo = true;
            $md->fecha_modificacion = date("c");
            if($md->idusuario == null || $md->idusuario != $cobratario){
                $crearsegui = true;
            }
        }
        else{
            $md = new Dispositivos();
            $amd = Dispositivos::findFirstByDispositivo($dispisitivo);
            if($amd){
                $this->response->setStatuscode(409, "Ya existe un registro ".($amd->activo ? "vigente" : "no vigente")." con el mismo dispositivo");
                return $this->response;
            }
            $crearsegui = true;
        }

        $cob = Users::findFirstById($cobratario);

        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];

        $md->dispositivo = $dispisitivo;
        $md->idusuario = $cobratario;
        $md->descripcion = $descripcion;
        $md->usuario = $cob->usuario;
        $md->idusuario_creacion = $idUser;

        $this->db->begin();
        if($md->save()){
            if($crearsegui){
                $uds = new UsuarioDispositivoSeguimiento();
                $uds->dispositivo = $dispisitivo;
                $uds->idusuario = $cobratario;
                $uds->usuario = $cob->usuario;
                $uds->idusuario_creacion = $idUser;
                if($uds->save()){
                    $this->db->commit();
                }
                else{
                    $this->db->rollback();
                    foreach ($uds->getMessages() as $message) {
                        $this->logger->error("save-dispositivo-seguimiento: ".$message->getMessage());
                    }
                    $this->response->setStatusCode(500);
                }
            }
            else{
                $this->db->commit();
            }
        }
        else{
            $this->db->rollback();
            foreach ($md->getMessages() as $message) {
                $this->logger->error("save-dispositivo: ".$message->getMessage());
            }
            $this->response->setStatusCode(500);
        }
        return $this->response;
    }

    public function deleteAction($id){
        if(!empty($id)){
            $d = Dispositivos::findFirstById($id);
            if ($d) {
                $d->activo = false;
                $d->fecha_modificacion = date("c");
                if(!$d->save()){
                    foreach ($d->getMessages() as $message) {
                        $this->logger->info("(delete-dispositivo): " . $message);
                    }
                    $this->response->setStatusCode(500);
                }
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(400, "Bad Request");
        }
        return $this->response;
    }

    public function historyAction($dispositivo){
        if(!empty($dispositivo)){
            $ds = UsuarioDispositivoSeguimiento::findByDispositivo($dispositivo);
            $resp = array();
            foreach ($ds as $d){
                array_push($resp, array(
                    $d->usuario ? $d->usuario : null,
                    $d->fecha_creacion ? date("d/m/Y H:i:s", strtotime($d->fecha_creacion)) : ""
                ));
            }
            $this->response->setContent(json_encode($resp));
        }
        else{
            $this->response->setStatusCode(400, "Bad Request");
        }
        return $this->response;
    }
}
