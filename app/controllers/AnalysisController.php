<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 21/02/17
 * Time: 04:48 PM
 */

namespace Vokuro\Controllers;

use Phalcon\Mvc\Dispatcher;
use Vokuro\GenericSQL\GenericSQL;
use Vokuro\Models\Clientes;
use Vokuro\Models\Colonias;
use Vokuro\Util\Util;
use Vokuro\DT\SSPGEO_dos;
use Phalcon\Mvc\View;
use GuzzleHttp\Client;
use Vokuro\Models\LayersGroup;
use Vokuro\Models\Layers;
use PHPExcel;

class AnalysisController extends ControllerBase
{
    private $excludeHeaders = array('the_geom','nombrebusqueda','urlcontenido','d_idcolonia', 'ubicado', 'folio', 'col_compara', 'exacto',
        'idpredio', 'sin_letra_numero', 'sin_letra_calle', 'sin_concatenar_letras', 'sin_cosiderar_colonias', 'con_letra_numero_pegado',
        'con_letra_calle_pegado', 'col_nombre_cat', 'predio_sin_folio', 'ine_corregido', 'ine_incorrecto', 'ine_respaldo',
        'corregido_sistema', 'activo', 'fecha_creacion', 'fecha_modificacion', 'distrito_local_old');
    public function indexAction()
    {
        $this->view->setTemplateBefore('public');
        $this->view->setVar('search_layers', Util::renderJSON("analysis_layers"));
        $this->view->setVar('grupos', json_encode($this->getGrupos()));
		$this->view->setVar('colonias',json_encode($this->getColonias()));
		$this->view->setVar('zonas',json_encode($this->getZonas()));
//        $this->view->setVar('zonas', []);
//		$this->view->setVar('origenes',$this->getOrigenes());
        $this->view->setVar('origenes', []);
    }

    public function getPersonsAction()
    {
        $this->view->disable();

        $primaryKey = 'id_cliente';
        $querySearch = is_array($this->request->getPost("query")) ? $this->request->getPost("query") : json_decode($this->request->getPost("query"));
        $typeSearch = $this->request->getPost("type");

		if($typeSearch === "CLIENTES_POR_PAGAR"){
			return $this->getCustomersFromDatePay($this->response, $this->request->get());
		}else{
			$searchKey = "id_cliente";
			$layers = Util::renderJSON("analysis_layers");
			for ($i = 0; $i < sizeof($layers); $i++) {
				if ($layers[$i]->clave === $typeSearch) {
					$layer = $layers[$i];
					break;
				}
			}

			$whereResult = null;
			$whereAll = null;

			if($typeSearch == "GEOMETRIA" || $typeSearch == "POINT"){
				if($querySearch == ""){
					$condicion = "";
				}
				else{
					$condicion = str_replace('$VALUE', $querySearch, $layer->condicion);

				}
				$whereResult = $condicion;
				$whereAll = $condicion;
			}

			$columns = array(
				array( 'db' => '', 'dt' => 0 ),
				array( 'db' => 'id_cliente', 'dt' => 1, 'datatype' => 'number' ),
				array( 'db' => 'id_cliente', 'dt' => 2, 'datatype' => 'number' ),
				array(
					'db' => 'nombre',
					'dt' => 3,
					'formatter' => function( $d, $row ) {
						$marker = "";
						if($row["ubicado"]){
							$marker = "<i class='fa fa-map-marker'></i> ";
						}
						return "$marker$d";
					}
				),
				array( 'db' => 'descripcion', 'dt' => 4 ),
				array( 'db' => 'ubicado', 'dt' => 5, 'datatype' => 'boolean'),
				array( 'db' => 'zona', 'dt' => 6 ),
				array( 'db' => 'id_cliente', 'dt' => 7 ),
				array( 'db' => 'colonia', 'dt' => 8 ),
				array( 'db' => 'ultimo_pago_txt', 'dt' => 9),
				array( 'db' => 'latitud', 'dt' => 10),
				array( 'db' => 'longitud', 'dt' => 11),
				array( 'db' => 'fecha_ultimo_pago', 'dt' => 12),
				array( 'db' => 'origen', 'dt' => 13),
				array( 'db' => 'calle', 'dt' => 14),
				array( 'db' => 'numero', 'dt' => 15),
				array( 'db' => 'total', 'dt' => 16),
				array( 'db' => 'importe', 'dt' => 17),
				
				array(
					'db' => 'id_cliente',
					'datatype' => 'number',
					'dt' => 'DT_RowId',
					'formatter' => function( $d, $row ) {
						return 'row_'.$d;
					}
				),
				array(
					'db' => 'id_cliente',
					'datatype' => 'number',
					'dt' => 'DT_RowClass',
					'formatter' => function( $d, $row ) {
						return $row["ubicado"]? 'zoom-enable' : '';
					}
				)
			);

			$data = SSPGEO_dos::complex_geo($this->request->get(), $layer->tabla, $primaryKey, $columns, $whereResult, $whereAll, true);
			$this->response->setContent(json_encode($data));
			return $this->response->send();
		}
    }
	
	private function getCustomersFromDatePay($response, $request){
			$searchKey = "id_cliente";
			$whereResult = null;
			$whereAll = null;
			
			$columns = array(
				array( 'db' => '', 'dt' => 0 ),
				array( 'db' => 'id', 'dt' => 1, 'datatype' => 'number' ),
				array( 'db' => 'id_cliente', 'dt' => 2, 'datatype' => 'number' ),
				array(
					'db' => 'nombre',
					'dt' => 3,
					'formatter' => function( $d, $row ) {
						$marker = "";
						if($row["ubicado"]){
							$marker = "<i class='fa fa-map-marker'></i> ";
						}
						return "$marker$d";
					}
				),
				array( 'db' => 'descripcion', 'dt' => 4 ),
				array( 'db' => 'ubicado', 'dt' => 5, 'datatype' => 'boolean'),
				array( 'db' => 'zona', 'dt' => 6 ),
				array( 'db' => 'id_cliente', 'dt' => 7 ),
				array( 'db' => 'colonia', 'dt' => 8 ),
				array( 'db' => 'ultimo_pago', 'dt' => 9),
				array( 'db' => 'latitud', 'dt' => 10),
				array( 'db' => 'longitud', 'dt' => 11),
				array( 'db' => 'fecha_ultimo_pago', 'dt' => 12),
				array( 'db' => 'origen', 'dt' => 13),
				array( 'db' => 'calle', 'dt' => 14),
				array( 'db' => 'numero', 'dt' => 15),
				array( 'db' => 'importe', 'dt' => 16),
				array( 'db' => 'total', 'dt' => 17),
				array(
					'db' => 'id',
					'datatype' => 'number',
					'dt' => 'DT_RowId',
					'formatter' => function( $d, $row ) {
						return 'row_'.$d;
					}
				),
				array(
					'db' => 'id',
					'datatype' => 'number',
					'dt' => 'DT_RowClass',
					'formatter' => function( $d, $row ) {
						return $row["ubicado"]? 'zoom-enable' : '';
					}
				)
			);


			$data = $this->complex_geo($request, $response, $columns);
			$response->setContent(json_encode($data));
			return $response;
	}
	
	private function complex_geo( $request, $response, $columns)
    {
        $limit = SSPGEO_dos::limit( $request, $columns );
        $values = explode(",", $request['columns'][3]['search']['value']);
		
		$inicio = $values[0];
		$fin = $values[1];
		$tipo = $values[2];
		$colonias = $request['columns'][8]['search']['value'];

		$where = "WHERE to_date(c.ultimo_pago, 'DD-MM-YYYY') >= to_date('$inicio', 'DD-MM-YYYY') 
			and to_date(c.ultimo_pago, 'DD-MM-YYYY') < to_date('$fin', 'DD-MM-YYYY') 
			AND c.id_cliente in ( SELECT id_cliente FROM geodata.clientes_view c where total_meses > 0)";
		if(empty($colonias)){
			$where .= " ";
        }else{
			$where .= " and colonia LIKE ALL (array[$colonias]) ";
		}
		
        $sql = "SELECT c.id_cliente, c.id_cliente,c.nombre,c.descripcion,c.ubicado, c.zona, c.id_cliente,c.colonia, c.ultimo_pago, c.latitud, c.longitud, c.fecha_ultimo_pago, c.origen, c.importe, c.total, c.id_cliente, c.id_cliente
			FROM geodata.clientes_view c
			$where
			ORDER BY cast(c.ultimo_pago as date) desc, total_meses desc
			$limit";
		
		/*
		return array(
			"sql" => $sql 
		);
		*/
		
		GenericSQL::getBySQL("SET datestyle = mdy;");
        $data = GenericSQL::getBySQL($sql);

		// Data set length after filtering
		$resFilterLength = GenericSQL::getBySQL("SELECT COUNT(c.id_cliente) FROM  geodata.clientes_view c $where");
        $recordsFiltered = $resFilterLength[0]->count;

        // Total data set length
        $resTotalLength = GenericSQL::getBySQL("SELECT COUNT(id_cliente) FROM geodata.clientes_view");
        $recordsTotal = $resTotalLength[0]->count;

        $auxCols = array();
        foreach($columns as $col) {
            $col["db"] = str_replace("\"", "", $col["db"]);
            $auxCols[] = $col;
        }
		
		return array(
            "draw"            => isset ( $request['draw'] ) ?
                intval( $request['draw'] ) :
                0,
            "recordsTotal"    => intval( $recordsTotal ),
            "recordsFiltered" => intval( $recordsFiltered ),
            "data"            => SSPGEO_dos::data_output( $auxCols, $data )
        );
    }
	
	public function bboxAction(){
		$this->view->disable();

        $schema = $this->request->getPost("esquema");
		$table = $this->request->getPost("tabla");
		
		$sql = "SELECT array_to_json(ARRAY[ST_XMin(ST_Extent(st_transform(the_geom,4326))), ST_YMin(ST_Extent(st_transform(the_geom,4326)))]) as sw, array_to_json(ARRAY[ST_XMax(ST_Extent(st_transform(the_geom,4326))), ST_YMax(ST_Extent(st_transform(the_geom,4326)))]) as ne FROM $schema.$table";
		
		$data = GenericSQL::getBySQL($sql);
		$aux = new \stdClass();
		$aux->sw = $data[0]->sw;
		$aux->ne = $data[0]->ne;
		
		$this->response->setContent(json_encode($aux));
		return $this->response;
	}
	
	public function table2Action(){
		$this->view->disable();		
		$type = $this->request->getPost("type");
		$schema = explode(".", $type)[0];
		$table = explode(".", $type)[1];
		$query = $this->request->getPost("query");
			
		$dataForHeaders = GenericSQL::getBySQL("SELECT column_name
            FROM information_schema.columns
            WHERE table_schema = '$schema' AND table_name   = '$table'");

		$limit = SSPGEO_dos::limit( $this->request->get(), null );
		
		$sql = "SELECT * FROM $schema.$table $limit";
		
		$data = GenericSQL::getBySQL($sql);
	
		// Data set length after filtering
		$resFilterLength = GenericSQL::getBySQL("SELECT COUNT(*) FROM  $schema.$table");
        $recordsFiltered = $resFilterLength[0]->count;

        // Total data set length
        $resTotalLength = GenericSQL::getBySQL("SELECT COUNT(*) FROM  $schema.$table");
        $recordsTotal = $resTotalLength[0]->count;
	
		$out = array();

        for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
            $row = array();
            $dataArray = json_decode(json_encode($data[$i]), true);
			foreach ($dataForHeaders as $key => $column){
				$row[$key] = $dataArray[$column->column_name];
			}
            array_push($out, $row);
        }
	
		/*
         * Output
         */
        $dataout = array(
			"draw"            => isset ( $request['draw'] ) ?
                intval( $request['draw'] ) :
                0,
			"recordsTotal"    => intval( $recordsTotal ),
            "recordsFiltered" => intval( $recordsFiltered ),
            "data"            => $out
        );
	
		$this->response->setContent(json_encode($dataout));
		return $this->response;
	}
	
	public function tableAction(){
			$whereResult = null;
			$whereAll = null;
			
			$type = $this->request->getPost("type");
			$schema = explode(".", $type)[0];
			$table = explode(".", $type)[1];
			$query = $this->request->getPost("query");
			
			$dataForHeaders = GenericSQL::getBySQL("SELECT column_name
            FROM information_schema.columns
            WHERE table_schema = '$schema' AND table_name   = '$table'");
			
			$columns = array();

			$contCol = 0;
			foreach ($dataForHeaders as $key => $column){
				if($column->column_name != "the_geom"){
					array_push($columns, array( 'db' => $column->column_name, 'dt' => $contCol ));
					$contCol += 1;	
				}				
			}
			
			$data = SSPGEO_dos::complex_geo($this->request->get(), $type, "*", $columns, $whereResult, $whereAll);
			$this->response->setContent(json_encode($data));
			return $this->response;
	}
	
	public function headerstableAction(){
		$this->view->disable();

		$schema = $this->request->getPost("esquema");
		$table = $this->request->getPost("tabla");
		
		$dataForHeaders = GenericSQL::getBySQL("SELECT column_name
            FROM information_schema.columns
            WHERE table_schema = '$schema' AND table_name   = '$table'");
	
		$out = array();
		foreach ($dataForHeaders as $key => $column){
			if($column->column_name != "the_geom"){
				$out[] = $column->column_name;
			}	
		}
		$this->response->setJsonContent($out);
		return $this->response;
	}

	public function exportAction($view){
		set_time_limit(300);
		ini_set('memory_limit', '1024M');
		
		$schema = explode(".", $view)[0];
		$table =  explode(".", $view)[1];
		
		$dataForHeaders = GenericSQL::getBySQL("SELECT column_name
            FROM information_schema.columns
            WHERE table_schema = '$schema' AND table_name   = '$table'");
	
		$out = array();
		foreach ($dataForHeaders as $key => $column){
			if($column->column_name != "the_geom"){
				$out[] = $column->column_name;
			}	
		}
		
		$colname = implode(",", $out);
		
		$data = GenericSQL::getBySQL("select $colname from $view");
		$data = $this->data_output( $out, $data );
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->fromArray($out, null, 'A1');
		$objPHPExcel->getActiveSheet()->fromArray($data, null, 'A2');
		
		$fname = date('dmY')."_".$table.".xlsx";
		
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="export.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}
	
	public function exportCsvAction(){
		set_time_limit(300);
		ini_set('memory_limit', '1024M');
		
		$nombre = $this->request->get()["nombre"];
		$ubicado = $this->request->get()["ubicado"];
		$zona = $this->request->get()["zona"];
		$no_cliente = $this->request->get()["id_cliente"];
		$colonia = $this->request->get()["colonia"];
		$calle = $this->request->get()["calle"];
		$numero = $this->request->get()["numero"];
		
		$where = "";
		
		if(!empty($calle)){
			$where = " trim(lower(calle::text)) LIKE ANY (array['%$calle%'])";	
		}
		
		if(!empty($numero)){
			if(!empty($where)){
				$where.=" and trim(lower(numero::text)) LIKE ANY (array['%$numero%'])";	
			}else{
				$where.=" trim(lower(numero::text)) LIKE ANY (array['%$numero%'])";	
			}
		}
		
		if(!empty($ubicado)){
			if(!empty($where)){
				$where.=" and ubicado = $ubicado";	
			}else{
				$where.=" ubicado = $ubicado";	
			}
		}
		
		if(!empty($no_cliente)){
			if(!empty($where)){
				$where.=" and trim(lower(id_cliente::text)) LIKE ANY (array['%$no_cliente%'])";
			}else{
				$where.=" trim(lower(id_cliente::text)) LIKE ANY (array['%$no_cliente%'])";
			}
		}
		
		if(!empty($nombre)){
			if(!empty($where)){
				$where.=" and trim(lower(nombre::text)) LIKE ANY (array['%$nombre%'])";	
			}else{
				$where.=" trim(lower(nombre::text)) LIKE ANY (array['%$nombre%'])";	
			}
		}
		
		if(!empty($zona)){
			$arrayZona = explode(",", $zona);
			$qZona = "";
			for($i =0; $i < count($arrayZona); $i++){
				$zonaI = $arrayZona[$i];
				if(!empty($qZona)){
					$qZona.= ",'%".$zonaI."%'";
				}else{
					$qZona.= "'%".$zonaI."%'";
				}
			}
			if(!empty($where)){
				$where.=" and trim(lower(zona::text)) LIKE ANY (array[$qZona])";	
			}else{
				$where.=" trim(lower(zona::text)) LIKE ANY (array[$qZona])";	
			}
		}
		
		if(!empty($colonia)){
			$arrayColonia = explode(",", $colonia);
			$qColonia = "";
			for($i =0; $i < count($arrayColonia); $i++){
				$colI = $arrayColonia[$i];
				if(!empty($qColonia)){
					$qColonia.= ",'%".$colI."%'";
				}else{
					$qColonia.= "'%".$colI."%'";
				}
			}
			if(!empty($where)){
				$where.=" and trim(lower(colonia::text)) LIKE ANY (array[$qColonia])";	
			}else{
				$where.=" trim(lower(colonia::text)) LIKE ANY (array[$qColonia])";	
			}
		}
		
		//$this->response->setContent(json_encode($where));
		//return $this->response;
		
		
		$colname = "id_cliente, nombre, descripcion,zona,colonia,ultimo_pago,fecha_ultimo_pago, origen";
		$out = explode(",", $colname);
		
		$data = GenericSQL::getBySQL("select $colname from cliente.cliente where $where order by nombre");
		
		
		//"trim(lower(calle::text)) LIKE ANY (array['%15%']) AND trim(lower(numero::text)) LIKE ANY (array['%f%']) ORDER BY nombre ASC"}"
		
		//$this->response->setContent(json_encode($out));
		//return $this->response;
		
		$data = $this->data_output2( $out, $data );
		
		
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->fromArray($out, null, 'A1');
		$objPHPExcel->getActiveSheet()->fromArray($data, null, 'A2');
		
		$fname = date('dmY')."_cliente.xlsx";
		
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="export.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}
	
	private function data_output( $columns, $data )
    {
        $out = array();

        for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
            $row = array();
            $dataArray = json_decode(json_encode($data[$i]), true);
            for ( $j=0, $jen=count($columns) ; $j<$jen ; $j++ ) {
                $column = $columns[$j];
				$row[ $j ] = $dataArray[$column];
            }

            $out[] = $row;
        }

        return $out;
    }
	
	private function data_output2( $columns, $data )
    {
        $out = array();
		
        for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
            $row = array();
            $dataArray = json_decode(json_encode($data[$i]), true);
            $out[] = $dataArray;
        }

		return $out;
    }
	
    public function searchAction()
    {
        $this->view->disable();
        /*
        $data = $this->request->getJsonRawBody();

        $layer = null;
        $layers = Util::renderJSON("analysis_layers");
        for ($i = 0; $i < sizeof($layers); $i++) {
            if ($layers[$i]->clave === $data->type) {
                $layer = $layers[$i];
                break;
            }
        }

        $condicion = $this->prepareQuery($layer->condicion, $data->query);
        $tables = CensusCatalog::findByNombreAndActivo('', 'true');
        $entidades = NomalListing::getSummary($layer, $condicion, $tables, $layer->clave === "POINT");
//        $datos = NomalListing::getDataSearch($layers[$indexLayer], $data->query);
        $content = array();

        foreach ($entidades as $entidad) {
            $o = new \stdClass();
            $o->id = $entidad->id;
            $o->capa = $entidad->capa;
            $o->estilo = $entidad->estilo;
            $o->cqlfilter = $this->prepareCQL($layer, $data->query, $entidad);
            $o->etiqueta = $entidad->etiqueta;
            $o->sw = $this->parseExtent($entidad->sw);
            $o->ne = $this->parseExtent($entidad->ne);
            $o->total = $entidad->total;
            $o->ubicados = $entidad->ubicados;
            $o->noubicados = $entidad->noubicados;
            array_push($content, $o);
        }
*/
        $content = array();
        return json_encode($content);
    }

    public function predioAction(){
        $this->view->disable();
        $lat = $this->request->getQuery('lat');
        $lng = $this->request->getQuery('lng');
        if($lat != null && trim($lat) != "" && $lat != null && trim($lat) != "" &&
            $lng != null && trim($lng) != "" && $lng != null && trim($lng) != "") {
            $data = GenericSQL::getPredioInfo($lat, $lng);
            $predio = sizeof($data) > 0? $data[0] : null;

            $o = new \stdClass();
            $o->predio = $predio;
            $this->response->setJsonContent($o);
        } else {
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    private function parseExtent($coordinate){
        $coordinate = json_decode($coordinate);
        if(is_array($coordinate)){
            if($coordinate[0] != null && $coordinate[1] != null){
                return $coordinate;
            }else{
                return null;
            }
        }
    }

    private function getAreas()
    {
        $data = array();
        $entidades = NomalListing::findAllAreas();

        foreach ($entidades as $entidad) {
            $o = new \stdClass();
            $o->id = $entidad->id;
            $o->text = $entidad->nombre;
            array_push($data, $o);
        }

        return $data;
    }

    private function getClientes()
    {
        $data = array();
        $entidades = Clientes::findAllClientes();

        foreach ($entidades as $entidad) {
            $o = new \stdClass();
            $o->id = $entidad->id_cliente;
            $o->text = $entidad->nombre;
            array_push($data, $o);
        }

        return $data;
    }
	
	private function getColonias()
    {
        $data = array();
        $entidades = Colonias::findByActivo();

        foreach ($entidades as $entidad) {
            $o = new \stdClass();
            $o->id = $entidad->id;
            $o->text = $entidad->nombre;
            array_push($data, $o);
        }

        return $data;
    }
	
	private function getZonas()
    {
        $data = array();
//        $entidades = Clientes::findZonasNombre();

        foreach (range(1,15) as $numero) {
            $o = new \stdClass();
            $o->id = $numero;
            $o->text = "Ruta ".$numero;
            $sqlzonas = "select *, st_asgeojson(st_transform(the_geom,4326)) the_geom from geodata.ruta".$numero;
            $rutas = GenericSQL::getBySQL($sqlzonas);
            $o->rutas = $rutas;
            array_push($data, $o);
        }

        return $data;
    }
	
	private function getClientesId()
    {
        $data = array();
        $entidades = Clientes::findAllClientesId();

        foreach ($entidades as $entidad) {
            $o = new \stdClass();
            $o->id = $entidad->nombre;
            $o->text = $entidad->nombre;
            array_push($data, $o);
        }

        return $data;
    }
	
	private function getClientesNombre()
    {
        $data = array();
        $entidades = Clientes::findAllClientesNombre();

        foreach ($entidades as $entidad) {
            $o = new \stdClass();
            $o->id = $entidad->nombre;
            $o->text = $entidad->nombre;
            array_push($data, $o);
        }

        return $data;
    }

    private function getDistritosLocales()
    {
        $data = array();
        $entidades = NomalListing::findAllDistritosLocales();

        foreach ($entidades as $entidad) {
            $o = new \stdClass();
            $o->id = $entidad->nombre;
            $o->text = $entidad->nombre;
            array_push($data, $o);
        }

        return $data;
    }

    public function infopredioAction() {
        $this->view->disable();
        $lat = $this->request->getQuery('lat');
        $lng = $this->request->getQuery('lng');
        if($lat != null && trim($lat) != "" && $lat != null && trim($lat) != "" &&
            $lng != null && trim($lng) != "" && $lng != null && trim($lng) != "") {
            $datas = GenericSQL::getPadronInfo($lat, $lng);
            $json_data = json_encode($datas);
            return $json_data;
        } else {
            $this->response->setStatusCode(501, "Not Implemented");
            return $this->response;
        }
    }

    public function infopadronAction() {
        $this->view->disable();
        $lat = $this->request->getQuery('lat');
        $lng = $this->request->getQuery('lng');
        if($lat != null && trim($lat) != "" && $lat != null && trim($lat) != "" &&
            $lng != null && trim($lng) != "" && $lng != null && trim($lng) != "") {
            $datas = GenericSQL::getPadronInfo($lat, $lng);
            $json_data = json_encode($datas);
            return $json_data;
        } else {
            $this->response->setStatusCode(501, "Not Implemented");
            return $this->response;
        }
    }

    private function getDistritosFederales()
    {
        $data = array();
        $entidades = NomalListing::findAllDistritosFederales();

        foreach ($entidades as $entidad) {
            $o = new \stdClass();
            $o->id = $entidad->nombre;
            $o->text = $entidad->nombre;
            array_push($data, $o);
        }

        return $data;
    }
	
	private function getGrupos(){
		$data = array();
		$grupos = LayersGroup::findAll();
		$user = $this->auth->getUser();
		
		foreach ($grupos as $grupo) {
            $capas = Layers::findByGrupoCapa($grupo->id, $user->id);
			if($capas && count($capas) > 0){
				$g = new \stdClass();
				$g->id = $grupo->nombre;
				$g->nombre = $grupo->nombre;
				$dataCapas = array();
				foreach ($capas as $capa) {
					$c = new \stdClass();
					$c->titulo = $capa->nombre;
					$c->name = $capa->nombre;
					$c->base = $capa->base;
					$c->visible = false;
					$c->capa = $capa->capa;
					$c->estilo = $capa->estilo;
					$c->estilo_etiqueta = $capa->estilo_etiqueta;
					$c->labeled = false;
					$c->label = true;
					//$c->tematico = $capa->tematico;
					$c->infopadron = false;
					//$c->camposinfo = $capa->camposinfo;
					$c->tabla = $capa->tabla;
					$c->esquema = $capa->esquema;
					array_push($dataCapas, $c);
				}
				
				$g->capas = $dataCapas;
				array_push($data, $g);
			}            
        }
		return $data;
	}

    public function geoserverAction(){
        $this->view->disable();
        $url = 'http://localhost:9181'.$this->config->application->geoserverUrl;


        $client = new Client();
        $response = $client->request('GET', $url, [
            'debug' => false,
            'http_errors' => false,
            'verify' => false,
            'query' => $_GET
//            'auth' => ['4dm1n1str4d0r', 'svrm@p@g30t3cn0l0g1a']
        ]);

        $body = (string) $response->getBody();
        $contentType = $response->getHeader('content-type')[0];

        header('Content-Type:'.$contentType);

        echo $body;
    }
    public function legendAction()
    {
        if ($this->request->isGet() == true) {
            $request = $this->request->getQuery('request');
            $version = $this->request->getQuery('version');
            $format = $this->request->getQuery('format');
            $width = $this->request->getQuery('width');
            $height = $this->request->getQuery('height');
            $strict = $this->request->getQuery('strict');
            $style = $this->request->getQuery('style');
            $layer = $this->request->getQuery('layer');

            $url = $this->config->application['urlWMS'] . "/wms?request=" . $request . "&format=" . $format . "&width=" .
                $width . "&height=" . $height . "&strict=" . $strict . "&style=" . $style . "&version=" . $version . "&r=" . rand() . "&LAYER=$layer";
            $data = file_get_contents($url);

            foreach ($http_response_header as $header){
                foreach (['Content-'] as $header_allowed) {
                    if (strpos($header, $header_allowed) !== false){
                        $this->response->setRawHeader($header);
                    }
                }
            }

            $this->response->setContent($data);
        } else {
            $this->response->setStatusCode(501, "Not Implemented");
        }

        return $this->response;
    }

    public function datatableAction($id){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $querySearch = $this->request->getQuery("query");
        $typeSearch = $this->request->getQuery("type");
        $join = "";

        $schema = "persona";
        $table = "listado_nominal";
        $title = "Listado nominal";
        if($id > 0){
            $cc = CensusCatalog::findFirstById($id);
            $schema = $cc->esquema;
            $table = $cc->tabla;
            $title = $cc->nombre;
            $join = "persona.listado_nominal";
        }

        $dataForHeaders = GenericSQL::getBySQL("SELECT data_type, column_name
            FROM information_schema.columns
            WHERE table_schema = '$schema' AND table_name   = '$table'");
        $headers = array();

        $aux = GenericSQL::getBySQL("SELECT c.column_name, c.data_type
        FROM information_schema.table_constraints tc 
        JOIN information_schema.constraint_column_usage AS ccu USING (constraint_schema, constraint_name) 
        JOIN information_schema.columns AS c ON c.table_schema = tc.constraint_schema AND tc.table_name = c.table_name AND ccu.column_name = c.column_name
        where constraint_type = 'PRIMARY KEY' and tc.table_name = '$table' and tc.table_schema = '$schema';");
        $primaryKey = $aux[0]->column_name;

        $colsConfg = array();
        $auxCol = new \stdClass();
        $auxCol->nombre = "";
        $auxCol->type = "text";
        $headers[] = $auxCol;
        $iCol = 0;
        $colsConfg[] = array(
            "targets" => $iCol,
            "orderable" => false,
            "className" => 'control',
            "responsivePriority" => 0,
            "searchable" => false,
            "width" => "25px"
        );
        $iCol++;
        $auxCol = new \stdClass();
        $auxCol->nombre = "idine";
        $auxCol->type = "text";
        $headers[] = $auxCol;
        $colsConfg[] = array(
            "targets" => $iCol,
            "searchable" => false,
            "visible" => false
        );
        $iCol++;
        $auxCol = new \stdClass();
        $auxCol->nombre = "Ubicado";
        $auxCol->type = "text";
        $headers[] = $auxCol;
        $colsConfg[] = array(
            "targets" => $iCol,
            "searchable" => false,
        );
        $iCol++;
        $auxCol = new \stdClass();
        $auxCol->nombre = "Ubicado_hide";
        $auxCol->type = "text";
        $headers[] = $auxCol;
        $colsConfg[] = array(
            "targets" => $iCol,
            "searchable" => false,
            "visible" => false
        );
        $iCol++;
        $excludedHeaders = $this->excludeHeaders;
        foreach ($dataForHeaders as $key => $column){
            if(!in_array($column->column_name, $excludedHeaders)){
                $auxCol = new \stdClass();
                $colDefDT = [];
                $colDefDT["targets"] = $iCol;

                if($column->column_name == $primaryKey){
                    $colDefDT["visible"] = false;
                }
                else{

                }
                $colsConfg[] = $colDefDT;


                $auxCol->nombre = $column->column_name;
                $auxCol->type = $column->data_type;
                $headers[] = $auxCol;
                $iCol++;
            }
        }

        $this->view->setVar('id', $id);
        $this->view->setVar('typeSearch', $typeSearch);
        $this->view->setVar('querySearch', json_encode($querySearch));
        $this->view->setVar('join', $join);
        $this->view->setVar('auxid', time());
        $this->view->setVar('title', $title);
        $this->view->setVar('headers', $headers);
        $this->view->setVar('pk', $primaryKey);
        $this->view->setVar('colsdef', json_encode($colsConfg));
    }

    public function searchdatatableAction(){
        $this->view->disable();
        /*
        $schema = "persona";
        $table = "listado_nominal";
        $title = "Listado nominal";

        $idcat = $this->request->get("idcat", "int");
        $querySearch = is_array($this->request->getPost("query")) ? $this->request->getPost("query") : json_decode($this->request->getPost("query"));
        $typeSearch = $this->request->getPost("type");

        $searchKey = "clave_elector";
        $join = false;
        if($idcat > 0){
            $cc = CensusCatalog::findFirstById($idcat);
            $schema = $cc->esquema;
            $table = $cc->tabla;
            $join = true;
            $searchKey = "\"INE\"";
        }

        $layers = Util::renderJSON("analysis_layers");
        for ($i = 0; $i < sizeof($layers); $i++) {
            if ($layers[$i]->clave === $typeSearch) {
                $layer = $layers[$i];
                break;
            }
        }

        $whereResult = "";
        $whereAll = "";
        $joinPref = "";
        if($join){
            if(is_array($querySearch)){
                $querySearch = array_map('trim', $querySearch);
                $querySearch = array_map('strtoupper', $querySearch);
                $querySearch = "'".implode("','", $querySearch)."'";
            }

            if($querySearch == ""){
                $condicion = "";
            }
            else{
                if($typeSearch == "GEOMETRIA"){
                    $layer->condicion = str_replace("the_geom", "ln.the_geom", $layer->condicion);
                }
                $condicion = str_replace('$VALUE', $querySearch, $layer->condicion);
            }

            $whereResult = $condicion;
            $whereAll = $condicion;
            $joinPref = "\"";
        }
        else{
            if(is_array($querySearch)){
                $querySearch = array_map('trim', $querySearch);
                $querySearch = array_map('strtoupper', $querySearch);
                $querySearch = "'".implode("','", $querySearch)."'";
            }

            if($querySearch == ""){
                $condicion = "";
            }
            else{
                $condicion = str_replace('$VALUE', $querySearch, $layer->condicion);

            }

            $whereResult = $condicion;
            $whereAll = $condicion;
        }

        // Table's primary key
        $aux = GenericSQL::getBySQL("SELECT c.column_name, c.data_type
        FROM information_schema.table_constraints tc 
        JOIN information_schema.constraint_column_usage AS ccu USING (constraint_schema, constraint_name) 
        JOIN information_schema.columns AS c ON c.table_schema = tc.constraint_schema AND tc.table_name = c.table_name AND ccu.column_name = c.column_name
        where constraint_type = 'PRIMARY KEY' and tc.table_name = '$table' and tc.table_schema = '$schema';");
        $primaryKey = $aux[0]->column_name;

        $columns = array();
        $sqlColumns = GenericSQL::getBySQL("SELECT data_type, column_name
            FROM information_schema.columns
            WHERE table_schema = '$schema' AND table_name   = '$table'");

        $columns = array();
        $excludedHeaders = $this->excludeHeaders;
        $iCol = 0;
        $columns[] = array(
            'db' => "",
            'dt' => $iCol
        );
        $iCol++;
        $columns[] = array(
            'db' => "ln.id as idine",
            'datatype' => 'number',
            'dt' => $iCol,
            'formatter' => function( $d, $row ) {
                return $row["idine"];
            },
        );
        $iCol++;
        $columns[] = array(
            'db' => "ubicado",
            'dt' => $iCol,
            'formatter' => function( $d, $row ) {
                $marker = "";
                if($row["ubicado"]){
                    $marker = "<i class='fa fa-map-marker'></i> ";
                }
                return $marker;
            },
        );
        $iCol++;

        $columns[] = array(
            'db' => "ubicado",
            'dt' => $iCol
        );
        $iCol++;

        foreach ($sqlColumns as $column){
            if(!in_array($column->column_name, $excludedHeaders)){
                $colName = $joinPref.$column->column_name.$joinPref;
                $headers[] = $colName;
                $uaxDefCol = array(
                    'db' => $colName,
                    "dt" => $iCol
                );
                if($column->data_type == "bigint" || $column->data_type == "integer"){
                    $uaxDefCol["datatype"] = "number";
                }

                if($column->data_type == "date"){
                    $uaxDefCol["datatype"] = "date";
                }

                if($column->data_type == "boolean"){
                    $uaxDefCol["datatype"] = "boolean";
                }

                $columns[] = $uaxDefCol;
                $iCol++;
            }
        }

        $columns[] = array(
            'db' => $primaryKey,
            'dt' => 'DT_RowId',
            'datatype' => 'number',
            'formatter' => function( $d, $row ) {
                return 'row_'.$d;
            }
        );
        $columns[] = array(
            'db' => 'id',
            'datatype' => 'number',
            'dt' => 'DT_RowClass',
            'formatter' => function( $d, $row ) {
                return $row["ubicado"]? 'zoom-enable' : '';
            }
        );

        $table = "$schema.$table";
        if($join){
            $data = SSPGEO_dos::complex_join_geo($this->request->get(), $table, "persona.listado_nominal", $primaryKey, $columns, $whereResult, $whereAll);
        }
        else {
            $data = SSPGEO_dos::complex_geo($this->request->get(), $table." as ln", $primaryKey, $columns, $whereResult, $whereAll);
        }
        */
        $data = [];
        $this->response->setContent(json_encode($data));
        return $this->response;
    }

    public function pointpersonAction($id)
    {
        if($this->request->isGet()){
            if($id != null && $id != ""){
                $ln = NomalListing::findGeomById($id);
                if($ln){
                    $geojson = json_decode($ln->toArray()[0]["geojson"]);
                    $coordinates = $geojson->coordinates;
                    $this->response->setContent(json_encode($coordinates));
                }
                else{
                    $this->response->setStatusCode(404);
                }
            }
            else{
                $this->response->setStatusCode(400);
            }
        }
        else{
            $this->response->setStatusCode(405);
        }
        return $this->response;
    }
	
	private function getOrigenes()
    {
        $data = array();
        $entidades =  Clientes::findOrigenes();

        foreach ($entidades as $entidad) {
            $o = new \stdClass();
            $o->id = $entidad->origen;
            $o->text = $entidad->origen;
            array_push($data, $o);
        }

        return $data;
    }

    public function getdashAction($capa){
        set_time_limit ( 0);
        $data = $this->request->getJsonRawBody();
        $geometry = $data->geometry;

        $sqlGeometry = "
        st_transform(
                    ST_SetSRID(
                        ST_GeomFromGeoJSON('$geometry'),
                        3857
                    )
                , 32616)
        ";
        if($data->zona === true){
            $sqlGeometry = "(select st_union(st_transform(the_geom, 32616)) from geodata.ruta".$data->idzona.")";
        }
        if($capa == "clientes" || $capa == "comercios"){
            $sql = "
            select 
              count(1), sum(importe) importe
            from geodata.".$capa."_view where st_intersects(
                $sqlGeometry, 
            the_geom)
            ";
            $exec = GenericSQL::getBySQL($sql);

            $sqlatrasado = "
            select 
              count(1)
            from geodata.".$capa."_view where st_intersects(
                $sqlGeometry, 
            the_geom)
            and meses_a_pagar::integer > 1
            ";
            $execatrasado = GenericSQL::getBySQL($sqlatrasado);

            $sqlaldia = "
            select 
              count(1)
            from geodata.".$capa."_view where st_intersects(
                $sqlGeometry, 
            the_geom)
            and meses_a_pagar::integer = 1
            ";
            $execaldia = GenericSQL::getBySQL($sqlaldia);

            $sqladelantadas = "
            select 
              count(1)
            from geodata.".$capa."_view where st_intersects(
                $sqlGeometry, 
            the_geom)
            and meses_a_pagar::integer < 1
            ";
            $execadelantados = GenericSQL::getBySQL($sqladelantadas);

            $nombrecapa = "clientes";
            $sqlanualizados = "
            select 
              count(1)
            from geodata.".$nombrecapa."_pago_anual_view where st_intersects(
                $sqlGeometry, 
            the_geom)
            ";
            $execanualizado = GenericSQL::getBySQL($sqlanualizados);

            $view = clone $this->view;
            $this->view->disable();

            $view->start();
            $view->setVar('datos', $exec[0]->count);
            $view->setVar('importe', $exec[0]->importe);
            $view->setVar('atrasados', $execatrasado[0]->count);
            $view->setVar('aldia', $execaldia[0]->count);
            $view->setVar('anualizado', $execanualizado[0]->count);
            $view->setVar('adelantados', $execadelantados[0]->count - $execanualizado[0]->count);
            $view->setVar('geometry', $geometry);
            $view->setVar('tipo', $capa);
            $view->setVar('zona', $data->zona ? "si": "no");
            $view->setVar('idzona', $data->zona ? $data->idzona: "null");

            $view->setRenderLevel(View::LEVEL_ACTION_VIEW);
            $view->render('analysis', 'sanacorbase');
            $view->finish();

            $this->response->setContent($view->getContent());
        }
        elseif ($capa == "predios"){

            $sql = "
                select 
                  count(1)
                from geodata.clientes_view where st_intersects(
                    $sqlGeometry, 
                the_geom)
                ";
            $execcli = GenericSQL::getBySQL($sql);

            $sql = "
                select 
                  count(1)
                from geodata.comercios_view where st_intersects(
                    $sqlGeometry, 
                the_geom)
                ";
            $execcom = GenericSQL::getBySQL($sql);
            if($data->zona === true){
                $sql = "
                select count(1) 
                from geodata.".$capa." p
                join geodata.ruta".$data->idzona." z on st_intersects(st_transform(z.the_geom,32616), p.the_geom)
                ";


                $exec = GenericSQL::getBySQL($sql);
            }
            else{
                $sql = "
                select 
                  count(1)
                from geodata.".$capa." where st_intersects(
                    $sqlGeometry, 
                the_geom)
                ";
                $exec = GenericSQL::getBySQL($sql);
            }

            //$this->response->setJsonContent($exec);

            $view = clone $this->view;
            $this->view->disable();

            $view->start();
            $view->setVar('datos', $exec[0]->count);
            $view->setVar('clientes', $execcli[0]->count);
            $view->setVar('comercios', $execcom[0]->count);
            $view->setVar('geometry', $geometry);
            $view->setVar('tipo', $capa);
            $view->setVar('zona', $data->zona ? "si": "no");

            $view->setRenderLevel(View::LEVEL_ACTION_VIEW);
            $view->render('analysis', 'predios');
//            $view->setVar('geometry', $geometry);
            $view->finish();

            $this->response->setContent($view->getContent());
        }
        else {
            $this->response->setStatusCode(404);
        }
        return $this->response;
    }

    public function searchdatadashAction(){
        set_time_limit ( 0);
        $geometry = $this->request->getPost("geometry");
        $tipo = $this->request->getPost("type");
        $zona = $this->request->getPost("zona");
        $idzona = $this->request->getPost("idzona");

        $sqlGeometry = "
        st_transform(
                    ST_SetSRID(
                        ST_GeomFromGeoJSON('$geometry'),
                        3857
                    )
                , 32616)
        ";
        if($zona === "true"){
            $table = "clientes_view";
            if ($tipo == "predios"){
                $table = $tipo;
            }
            elseif ($tipo == "clientes"){
                $table = $tipo."_view";
            }
            $table = "geodata." . $table;

            $condicion = "id in (
                select p.id_cliente 
                from $table p
                join geodata.ruta".$idzona." z on st_intersects(st_transform(z.the_geom,32616), p.the_geom)
            )";
        }
        else{
            $condicion = "
            st_intersects(
                $sqlGeometry, 
            the_geom)
        ";
        }


        $whereResult = $condicion;
        $whereAll = $condicion;

        // Table's primary key

        $columns = ($tipo == "clientes" || $tipo == "comercios") ?
            $this->getColsDashCLients() : $this->getColsDashPredios();

        if($tipo == "clientes" || $tipo == "comercios") {
            $columns[] = array(
                'db' => "id_cliente",
                'dt' => 'DT_RowId',
                'datatype' => 'number',
                'formatter' => function ($d, $row) {
                    return 'row_' . $d;
                }
            );
        }
        else{
            $columns[] = array(
                'db' => "folio",
                'dt' => 'DT_RowId',
                'datatype' => 'number',
                'formatter' => function ($d, $row) {
                    return 'row_' . $d;
                }
            );
        }

        $table = "clientes_view";
        if ($tipo == "predios"){
            $table = $tipo;
        }
        elseif ($tipo == "clientes" || $tipo == "comercios"){
            $table = $tipo."_view";
        }
        $table = "geodata." . $table;

        $data = SSPGEO_dos::complex_geo($this->request->get(), $table, $tipo == "predios" ? "folio":"id_cliente", $columns, $whereResult, $whereAll);

        $this->response->setContent(json_encode($data));
        return $this->response;
    }

    private function getColsDashCLients(){
        set_time_limit ( 0);
        $columns = array();
        $iCol = 0;
        $columns[] = array(
            'db' => "",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "no_cliente",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "nombre",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "folio_catastral",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "calle",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "numero",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "cruza1",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "cruza2",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "colonia",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "zona",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "meses_a_pagar",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "importe",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "ultimo_pago",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "fecha_ultimo_pago",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "latitud",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "longitud",
            'dt' => $iCol
        );
        $iCol++;

        return $columns;
    }

    private function getColsDashPredios(){
        set_time_limit ( 0);
//        folio,
//              seccion,
//              manzana_1,
//              nomclatu,
//              tipouh,
//              uh,
//              poblacion,
//              superficie
        $columns = array();
        $iCol = 0;
        $columns[] = array(
            'db' => "",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "folio",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "catastral",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "calle",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "numero",
            'dt' => $iCol
        );
        $iCol++;

        $columns[] = array(
            'db' => "colonia",
            'dt' => $iCol
        );
        $iCol++;

//        $columns[] = array(
//            'db' => "uh",
//            'dt' => $iCol
//        );
//        $iCol++;
//
//        $columns[] = array(
//            'db' => "poblacion",
//            'dt' => $iCol
//        );
//        $iCol++;
//
//        $columns[] = array(
//            'db' => "superficie",
//            'dt' => $iCol
//        );
//        $iCol++;
//
//        $columns[] = array(
//            'db' => "latitud",
//            'dt' => $iCol
//        );
//        $iCol++;
//
//        $columns[] = array(
//            'db' => "longitud",
//            'dt' => $iCol
//        );
//        $iCol++;

        return $columns;
    }
}