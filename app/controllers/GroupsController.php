<?php
namespace Vokuro\Controllers;

use Phalcon\Tag;
use Vokuro\Forms\GroupsForm;
use Vokuro\Models\LayersGroup;

/**
 * Vokuro\Controllers\groupsController
 * CRUD to manage groups
 
 */
class GroupsController extends ControllerBase
{

	public function initialize(){
//		$this->view->setTemplateBefore('public');
	}

	/**
	 * Default action, shows the search form
	 */
	public function indexAction(){
		$this->view->setTemplateBefore('public');
		$groups = array();
		$grupos = LayersGroup::find();
		foreach ($grupos as $key=>$value){
			array_push($groups, $grupos[$key]);
		}
		$this->view->setVar("grupos", $groups);
	}

	public function infoAction($id){
		$id = intval($id);
		$this->view->setTemplateBefore('public');
		$identity = $this->auth->getIdentity();
		
		$this->response->setContentType("application/json");
		
		if(empty($id)){
			$this->response->setStatusCode(501);
			$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"Grupo de capas no especificado.")));
			return $this->response;
		}else{
			$bdGroup = LayersGroup::find($id);
			if(!$bdGroup){
				$this->response->setStatusCode(404);
				$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"La capa que intenta consultar no se encuentra registrado.")));
				return $this->response;
			}else{
				$aux = new \stdClass();
				foreach($bdGroup as $group){
					$aux->id 							= $group->id;
					$aux->nombre 					= $group->nombre;
					$aux->descripcion 					= $group->descripcion;
					$aux->orden 					= $group->orden;
					$aux->fecha_creacion 	= $group->fecha_creacion;
					$aux->fecha_modificacion 	= $group->fecha_modificacion;
					$aux->activo 					= $group->activo ? "Si" : "No";
				}
				$this->response->setStatusCode(200);
				$this->response->setContent(json_encode(array("lError"=>false, "cMensaje"=>"Grupo de capas consultado.", "resultado"=>$aux)));
				return $this->response;
			}
			return $this->response;
		}
	}

	public function createAction(){
		$identity = $this->auth->getIdentity();
		if($this->request->isGet() == true) {
			$this->view->setTemplateBefore('public');
			$this->view->form = new GroupsForm();
		}elseif ($this->request->isPost() == true){
			try{
				$this->view->disable();
		
				$rawBody = $this->request->getJsonRawBody();
				
				$aux = new LayersGroup();
				$nombre   					= $rawBody->nombre;
				$aux->nombre 					= $rawBody->nombre;
				$aux->descripcion 					= $rawBody->descripcion;
				$aux->orden 					= $rawBody->orden;
				$aux->activo		= $rawBody->activo;
				
				$bdGroup = LayersGroup::findFirstByNombre($nombre);
				if($bdGroup){
					$this->response->setStatusCode(409, "El grupo de capas $nombre que intenta crear ya se encuentra registrado.");
					return $this->response;
				}

				$this->db->begin();

				if($aux->save() === false){
					$this->db->rollback();
					
					$messages = $objUser->getMessages();
							$cError = "";
					foreach ($messages as $message) {
						$cError .= "[".$message."]";
					}

					$this->response->setStatusCode(501, $cError);
					return $this->response;
				}else{
					$this->db->commit();
					$this->response->setStatusCode(201, "Grupo de capas registrado satisfactoriamente.");
					$this->flash->error("Grupo de capas registrada satisfactoriamente.");
					return $this->response;
				}
			}catch(Exception $e){
				$this->response->setStatusCode(500, $e->getMessage());
				return $this->response;
			}
		}
	}

	public function editAction($id){
		$id = intval($id);
		$infogroup = null;

		if($this->request->isGet() == true) {
			$this->view->setTemplateBefore('public');

			$dbinfogroup = LayersGroup::find($id);
			foreach($dbinfogroup as $group){
				$info = new \stdClass();

				$info->id = $group->id;
				$info->activo = $group->activo;
				$info->nombre = $group->nombre;
				$info->descripcion = $group->descripcion;
				$info->orden = $group->orden;
			}
			$this->view->setVar("infogroup", $info);

			$this->view->form = new GroupsForm($info, [
									'edit' => true,
								]);
		}elseif ($this->request->isPost() == true){
			try{
				$this->view->disable();

				$rawBody = $this->request->getJsonRawBody();

				$id 			= $rawBody->id;
				$nombre 	= $rawBody->nombre;
				
				$bdGroup = LayersGroup::findFirstById($id);
				
				if(!$bdGroup){
					$this->response->setStatusCode(409, "El grupo de capas $nombre que intenta modificar no se encuentra registrado.");
					return $this->response;
				}
				
				
				$bdGroup->nombre = $rawBody->nombre;
				$bdGroup->descripcion = $rawBody->descripcion;
				$bdGroup->activo = $rawBody->activo;
				$bdGroup->orden = $rawBody->orden;
				
				$this->db->begin();

				if($bdGroup->save() === false){
					$this->db->rollback();

					$messages = $bdGroup->getMessages();
					$cError = "";
					foreach ($messages as $message) {
						$cError .= "[".$message."]";
					}

					$this->response->setStatusCode(501, $cError);
					return $this->response;
				}else{
					$this->db->commit();
					$this->response->setStatusCode(201, "Grupo de capas actualizado satisfactoriamente.");
					$this->flashSession->success("Grupo de capas \"$bdGroup->nombre\" actualizado satisfactoriamente.");
					return $this->response;
				}
			}catch(Exception $e){
				$this->response->setStatusCode(500, $e->getMessage());
				return $this->response;
			}
		}
	}


	function deactivateAction($id){
		$this->view->setTemplateBefore('public');
		$identity = $this->auth->getIdentity();
		$idUser = $identity["id"];
		$this->response->setContentType("application/json");
		if ($this->request->isPut() == true) {
			if(empty($id)){
				$this->response->setStatusCode(505);
				$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"Grupo de capas no especificado.")));
				return $this->response;
			}else{
				$bdGroup = LayersGroup::findFirstById($id);
				if(!$bdGroup){
					$this->response->setStatusCode(404);
					$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"El grupo de capas que intenta desactivar no se encuentra registrado.")));
					return $this->response;
				}else{
					$this->db->begin();
					$bdGroup->fecha_modificacion = date("c");
					$bdGroup->activo = false;
					if($bdGroup->save()){
						$this->db->commit();
						$this->response->setStatusCode(200);
						$this->response->setContent(json_encode(array("lError"=>false, "cMensaje"=>"Grupo de capas desactivado satisfactoriamente.")));
						return $this->response;
					}else{
						$this->db->rollback();
						$this->response->setStatusCode(200);
						$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"Ocurrio un error al desactivar el grupo de capas.")));
						return $this->response;
					}
				}
				return $this->response;
			}
		}else{
			$this->response->setStatusCode(405);
			return $this->response;
		}
	}

	function activateAction($id){
		$this->view->setTemplateBefore('public');
		$identity = $this->auth->getIdentity();
		$idUser = $identity["id"];
		$this->response->setContentType("application/json");
		if ($this->request->isPut() == true) {
			if(empty($id)){
				$this->response->setStatusCode(505);
				$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"Grupo de capas no especificado.")));
				return $this->response;
			}else{
				$bdGroup = LayersGroup::findFirstById($id);
				if(!$bdGroup){
					$this->response->setStatusCode(404);
					$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"El grupo de capas que intenta activar no se encuentra registrado.")));
					return $this->response;
				}else{
					$this->db->begin();
					$bdGroup->fecha_modificacion = date("c");
					$bdGroup->activo = true;
					if($bdGroup->save()){
						$this->db->commit();
						$this->response->setStatusCode(200);
						$this->response->setContent(json_encode(array("lError"=>false, "cMensaje"=>"Grupo de capas activado satisfactoriamente.")));
						return $this->response;
					}else{
						$this->db->rollback();
						$this->response->setStatusCode(200);
						$this->response->setContent(json_encode(array("lError"=>true, "cMensaje"=>"Ocurrio un error al activar el grupo de capas.")));
						return $this->response;
					}
				}
				return $this->response;
			}
		}else{
			$this->response->setStatusCode(405);
			return $this->response;
		}
	}
}
