<?php
namespace Vokuro\Controllers;
use Vokuro\DT\SSPGEO;
use Vokuro\GenericSQL\GenericSQL;
use Vokuro\Models\Cobratario;
use Vokuro\Models\Dispositivos;
use Vokuro\Models\Tracking;
use Vokuro\Models\Users;
use Vokuro\Models\UsuarioDispositivoSeguimiento;

/**
 * Display the default index page.
 */
class TrackcobController extends ControllerBase
{

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function indexAction()
    {
        $this->view->setTemplateBefore('public');
        $this->view->setVar('dispositivos', Dispositivos::findByActivo(true));
        $this->view->setVar('cobratarios', Users::findByActivo(true));

    }

    public function buscarAction(){
        $request = $this->request;
        $response = $this->response;

        $dispositivo = $request->get("disp");
        $cobratario = $request->get("cob");

        $where = "";
        if(!empty($dispositivo)){
            $where = " and d.dispositivo ilike '%".$dispositivo."%'";
        }

        if(!empty($cobratario)){
            $where = " and d.idusuario = $cobratario";
        }

        $sql = "select d.dispositivo, coalesce(d.usuario, 'SIN ASIGNACION') usuario, t.latitud, t.longitud from tracking.dispositivos d
        left join (
            select distinct on (dispositivo) * from tracking.tracking order by dispositivo desc, fecha_creacion desc
        ) t on d.dispositivo = t.dispositivo
        where d.activo = true $where";
        $q = GenericSQL::getBySQL($sql);
        $response->setContent(json_encode($q));
        return $response;
    }

    public function seguimientoAction(){
        $this->view->disable();
        $dispositivo = $this->request->get("dispositivo");
        $dtInicio = $this->request->get("fecini");
        $dtFinal = $this->request->get("fecfin");
        $dtInicio = implode("-", array_reverse(explode("-", $dtInicio)));
        $dtFinal = implode("-", array_reverse(explode("-", $dtFinal)));

        $sql = "select id, latitud, longitud, coalesce(usuario, 'SIN ASIGNACION') usuario 
        from tracking.tracking 
        where dispositivo = '$dispositivo' AND fecha_creacion::date BETWEEN '$dtInicio'::date AND '$dtFinal'::date";
        $seguimento = GenericSQL::getBySQL($sql);

        $ccobros = array();

        $disp = Dispositivos::findFirstByDispositivo($dispositivo);
        if($disp->idusuario){
//            $sql = "select count(distinct(idpago)) count
//            from cliente.historial_pago
//            where activo = true and idusuario = ".$disp->idusuario." and fecha_creacion::date BETWEEN '$dtInicio'::date AND '$dtFinal'::date";
            $sql = "select distinct on (idpago) c.calle || coalesce(' ' || calle_letra , '') calle, c.numero || coalesce(' ' || numero_letra , '') numero, col.nombre colonia, hp.fecha_creacion fecha 
            from cliente.historial_pago hp
            left join cliente.cliente c on hp.idcliente = c.id_cliente
            left join cliente.colonia col on c.idcolonia = col.id
            where hp.activo = true and hp.idusuario = ".$disp->idusuario." and hp.fecha_creacion::date BETWEEN '$dtInicio'::date AND '$dtFinal'::date";
            $ccobros = GenericSQL::getBySQL($sql);
        }

        $data = new \stdClass();
        $data->data = $seguimento;
        $data->cobros = $ccobros;

        $this->response->setContent(json_encode($data));
        return $this->response;
    }
}
