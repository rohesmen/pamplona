<?php
namespace Vokuro\Controllers;

use Vokuro\Models\Clientes;
use Phalcon\Mvc\View;
use Vokuro\Models\Colonias;
use Vokuro\Models\Users;
use Vokuro\Models\FormaPago;
use Vokuro\Models\HistorialPago;
use Vokuro\Models\Rutas;
use Vokuro\Models\TarifaColonia;
use Vokuro\Models\TipoDescuento;

/**
 * Display the default index page.
 */
class RutaController extends ControllerBase
{



    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function indexAction()
    {
        /*$this->view->setVar('logged_in', is_array($this->auth->getIdentity()));*/
        $this->view->setTemplateBefore('public');

        $this->view->setVar('doSearch', $this->acl->isAllowedUser('users', 'search'));
        $this->view->setVar('colonias', Colonias::findByActivo());
        $this->view->setVar('usuarios', Users::findByActivo());

    }

    public function searchAction(){

        $view = clone $this->view;
        $this->view->disable();




        if ($this->request->isPost() == true) {

            $rawBody = $this->request->getJsonRawBody();

            $results = $this->filterSearch($rawBody);



        }
        else{
            $view->setVar('NotImplemented', true);
        }

        $this->response->setContent(json_encode($results));
        return $this->response;



    }



    private function filterSearch($rawBody){

        $request = $this->request;
        $response = $this->response;

        $nombre = $rawBody->nombre;
        $clave = $rawBody->clave;
        $tipo = $rawBody->tipo;

        $colonia = $rawBody->colonia;

        $clave = preg_replace('/\s+/', ' ',$clave);
        $nombre = preg_replace('/\s+/', ' ',$nombre);
        $colonia = preg_replace('/\s+/', ' ',$colonia);


        $where = ' 1 = 1 ';
        if($tipo == "nom")
            $where .= " AND lower(r.nombre) LIKE lower('%$nombre%')";

        if($tipo == "col") {
            if ($colonia != "")
                $where .= " AND r.idcolonia = '" . $colonia . "'";
        }

        if($tipo == "cla")
            $where .= " AND r.id = ". $clave;

        $results= array();



        $results = Clientes::findByQuery("select r.*, 
            COALESCE(col.nombre, '')||', '||COALESCE(col.localidad, '') colonia,
            COALESCE(us.nombre, '')||' '||COALESCE(us.apellido_paterno, '')||' '||COALESCE(us.apellido_materno, '')  usuario
            FROM cliente.ruta r 
            LEFT JOIN cliente.colonia AS col ON col.id = r.idcolonia
            LEFT JOIN usuario.usuario AS us ON us.id = r.idusuario
            WHERE  ".$where." ORDER BY id ASC");


        $rows = array();
        if(count($results) > 0){

            foreach($results as $res) {

                $colonia = $res->colonia;

                $vigente = $res->activo ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                    : '<i class="fa fa-remove" style="color: red" title="Activo"></i>';

                $d = array();
                $d["0"] = null;
                $d["1"] = null;
                $d["2"] = $vigente;
                $d["3"] = $res->id;
                $d["4"] = $res->nombre;
                $d["5"] = $colonia;
                $d["6"] = $res->usuario;
                $d["DT_RowId"] = "ruta-$res->id";
                array_push($rows, $d);

            }
        }

        return $rows;
    }


    public function guardarAction()
    {


        $view = clone $this->view;
        $this->view->disable();
        $respuesta = [];

        $rawBody = $this->request->getJsonRawBody();

        if($rawBody) {
            $id = intval($rawBody->clave);
            $resp = new \stdClass();
            if ($id > 0)
            {
                $conditions = "id != :id: AND activo = :activo: AND nombre = :nombre: AND idcolonia = :colonia:";
                $parameters = array("id" => $id, "activo" => $rawBody->activo, "nombre" => $rawBody->nombre, "colonia" => $rawBody->colonia);
            }
            else {
                $conditions = "activo = :activo: AND nombre = :nombre: AND idcolonia = :colonia:";
                $parameters = array("activo" => $rawBody->activo, "nombre" => $rawBody->nombre, "colonia" => $rawBody->colonia);
            }
            $rutas     = Rutas::find(array($conditions, "bind" => $parameters));



            if(count($rutas) > 0)
            {


                $this->response->setContent(json_encode(["lOk" => false, "cMensaje" => "Ya existe un registro con los mismos datos"]));
                return $this->response;
            }



            if ($id > 0)
            {
                $resp->nuevo = false;
                $ruta = Rutas::findFirst($id);
                $ruta->assign(array(
                    'activo' => $rawBody->activo,
                    'nombre' =>  $rawBody->nombre,
                    'idcolonia' => $rawBody->colonia,
                    'idusuario' => $rawBody->usuario,
                    'fecha_modificacion' => 'NOW()'
                ));

            }
            else {
                $resp->nuevo = true;
                $ruta = new Rutas();
                $ruta->assign(array(
                    //                'id' => ,
                    'activo' => $rawBody->activo,
                    'nombre' => $rawBody->nombre,
                    'idcolonia' => $rawBody->colonia,
                    'idusuario' => $rawBody->usuario,
                    'fecha_creacion' => 'NOW()',
                    'fecha_modificacion' => 'NOW()'

                ));
            }

            if(!$ruta->save()) {
                $this->flash->error($ruta->getMessages());
                $this->response->setContent(json_encode(["lOk" => false, "cMensaje" => "No fue posible guardar el registro"]));
                return $this->response;
            }
            else
            {


                $vigente = $ruta->activo ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                    : '<i class="fa fa-remove" style="color: red" title="Activo"></i>';

                $colonia = "";
                $eColonia = Colonias::findFirstById($ruta->idcolonia);
                if($eColonia !== false){
                    $colonia = $eColonia->nombre.", ".$eColonia->localidad;
                }

                $usuario = "";
                $eUsuario = Users::findFirstById($ruta->idusuario);
                if($eUsuario !== false){
                    if($eUsuario->nombre != null and $eUsuario->nombre != ""){
                        $usuario .= $eUsuario->nombre;
                    }

                    if($eUsuario->apellido_paterno != null and $eUsuario->apellido_paterno != ""){
                        if($usuario !=  ""){
                            $usuario .= " ";
                        }
                        $usuario .= $eUsuario->apellido_paterno;
                    }
                    if($eUsuario->apellido_materno != null and $eUsuario->apellido_materno != ""){
                        if($usuario !=  ""){
                            $usuario .= " ";
                        }
                        $usuario .= $eUsuario->apellido_materno;
                    }
                }


                $resp->lOk = true;
                $resp->cMensaje = "";
                $resp->id = $ruta->id;
                $resp->nombre = $ruta->nombre;
                $resp->colonia = $colonia;
                $resp->usuario = $usuario;
                $resp->activo = $vigente;



                $this->response->setContent(json_encode($resp));
                return $this->response;
            }


            die;
        }

    }

    public function getAction($id) {
        $this->view->disable();
        if ($this->request->isGet() == true) {
            $ruta = Rutas::findFirst($id);
            if($ruta != false){

                $row = new \stdClass();
                $row->id = $ruta->id;
                $row->nombre = $ruta->nombre;
                $row->colonia = $ruta->idcolonia;
                $row->usuario = $ruta->idusuario;
                $row->activo = $ruta->activo;



                $this->response->setContent(json_encode($row));
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    public function deleteAction($id)
    {
        if($id != null and $id != ""){
            $ruta = Rutas::findFirstById($id);
            if ($ruta) {
                $ruta->activo = false;
                $ruta->fecha_modificacion = date("c");
                $ruta->save();
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(400, "Bad Request");
        }
        return $this->response;
    }
}
