<?php
namespace Vokuro\Controllers;
use Vokuro\Models\Multimedia;

/**
 * View and define permissions for the various profile levels.
 */
class MultimediaController extends ControllerBase
{
    public function indexAction()
    {
        if($this->request->isAjax()){
            $vigente = $this->request->get("vigente");
            $nombre = $this->request->get("nombre");
            $where = "";
            if($vigente != ""){
                $where .= "activo = $vigente and";
            }
            if($nombre != ""){
                $where .= "lower(nombre) like lower('%$nombre%') and ";
            }

            $where = $where != "" ? "where ".substr($where, 0, -4) : "";
            $sqlQuery = "SELECT * FROM cliente.multimedia $where ORDER BY id DESC";

            $results = Multimedia::findByQuery($sqlQuery);

            $data = array();
            if(count($results) > 0){
                foreach($results as $res){

                    $actions = '';
//                    if($this->acl->isAllowedUser("multimedia", "edit")){
                        $actions .= '<button class="btn btn-primary btn-sm datatable-edit" title="Editar" data-id="'.$res->id.'">
                            <i class="fa fa-pencil"></i>
                        </button> ';
//                    }

//                    if($this->acl->isAllowedUser("multimedia", "info")){
                        $actions .= '<button class="btn btn-primary btn-sm datatable-info" title="Consultar" data-id="'.$res->id.'">
                            <i class="fa fa-info"></i>
                        </button> ';
//                    }

//                    if($this->acl->isAllowedUser("multimedia", "delete")){
                        if ($res->activo){
                            $actions .= '<button class="btn btn-danger btn-sm datatable-delete" title="Desactrivar" data-id="'.$res->id.'">
                                <i class="fa fa-remove"></i>
                            </button> ';
                        }
                        else{
                            $actions .= '<button class="btn btn-success btn-sm datatable-active" title="Activar" data-id="'.$res->id.'">
                                <i class="fa fa-check"></i>
                            </button> ';
                        }
//                    }

                    if($res->activo){
                        $estatus = '<i class="fa fa-check-circle" style="color: green;"></i>';
                    }
                    else{
                        $estatus = '<i class="fa fa-times-circle" style="color: red;"></i>';
                    }

                    array_push($data, array(
                        "",
                        $actions,
                        $estatus,
                        $res->id,
                        $res->tipo == "video" ? "<i class='fa fa-file-video-o'></i>" : "<i class='fa fa-file-image-o'></i>",
                        $res->nombre,
                        $res->descripcion,
                        date("d/m/Y", strtotime($res->fecha)),
                        date("d/m/Y", strtotime($res->fecha_creacion)),
                        $res->activo ? "true" : "false"
                    ));
                }
                //fin:foreach($results as $res)
            }
            //fin:if(count($results) > 0)

            $this->response->setContent(json_encode($data));
            return $this->response;
        }
        else{
            $this->view->setTemplateBefore('public');
        }
    }

    public function saveAction(){
        if(!$this->request->isAjax()){
            $this->view->setTemplateBefore('error');
        }

        $clave = $this->request->getPost("clave");
        $nombre = $this->request->getPost("nombre");
        $fecha = $this->request->getPost("fecha");
        $tipo = $this->request->getPost("tipo");
        $descripcion = $this->request->getPost("descripcion");
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];
        if($this->request->isPost()){

            if($clave){
                if ($this->request->hasFiles() == true) {
                    $m = Multimedia::findFirstById($clave);
                    if($m){
                        $baseLocation = IMG_DIR.DIRECTORY_SEPARATOR."multimedia".DIRECTORY_SEPARATOR;
                        $baseLocation1 = "/var/www/html/public/img/multimedia".DIRECTORY_SEPARATOR;

                        $file = $this->request->getUploadedFiles();
                        $file = $file[0];
                        $split = explode(".",$file->getName());
                        $ext = $split[count($split) - 1];

                        $name = "multimedia_".time()."_".date("dmY").".".$ext;
                        $resp = new \stdClass();
                        if($file->moveTo($baseLocation.$name)){
                            copy($baseLocation.$name, $baseLocation1.$name);
                            $m->ruta = $name;
                            $m->descripcion = $descripcion;
                            $m->nombre = $nombre;
                            $m->fecha = implode("-", array_reverse(explode("/", $fecha)));
                            $m->idusuario = $idUser;
                            $m->fecha_modificacion = date("c");
                            if($m->save()){
                                $resp->id = $m->id;
                                $resp->ruta = $name;
                                $resp->tipo = $tipo;
                                $this->response->setContent(json_encode($resp));
                            }
                            else {
                                unlink($baseLocation.$name);
                                $this->response->setStatusCode(500, "No se pudo guardar el archivo");
                            }
                        }
                        else{
                            $this->response->setStatusCode(500, "No se pudo guardar el archivo");
                        }

                        $m->save();
                    }
                    else{
                        $this->response->setStatusCode(404, "Registro no encontrado");
                    }
                }
                else{
                    $m = Multimedia::findFirstById($clave);
                    if($m){
                        $m->descripcion = $descripcion;
                        $m->nombre = $nombre;
                        $m->fecha = implode("-", array_reverse(explode("/", $fecha)));
                        $m->idusuario = $idUser;
                        $m->fecha_modificacion = date("c");
                        $resp = new \stdClass();
                        if($m->save()){
                        }
                        else {
                            $this->response->setStatusCode(500, "No se pudo guardar el archivo");
                        }
                    }
                    else{
                        $this->response->setStatusCode(404, "Registro no encontrado");
                    }
                }
            }
            else{
                if ($this->request->hasFiles() == true) {
                    $baseLocation = IMG_DIR.DIRECTORY_SEPARATOR."multimedia".DIRECTORY_SEPARATOR;
                    $baseLocation1 = "/var/www/html/public/img/multimedia".DIRECTORY_SEPARATOR;

                    $file = $this->request->getUploadedFiles();
                    $file = $file[0];
                    $split = explode(".",$file->getName());
                    $ext = $split[count($split) - 1];

                    $name = "multimedia_".time()."_".date("dmY").".".$ext;
                    $resp = new \stdClass();
                    if($file->moveTo($baseLocation.$name)){
                        copy($baseLocation.$name, $baseLocation1.$name);
                        $m = new Multimedia();
                        $m->ruta = $name;
                        $m->tipo = $tipo;
                        $m->descripcion = $descripcion;
                        $m->nombre = $nombre;
                        $m->fecha = implode("-", array_reverse(explode("/", $fecha)));
                        $m->idusuario = $idUser;
                        if($m->save()){
                            $resp->id = $m->id;
                            $resp->ruta = $name;
                            $resp->tipo = $tipo;
                            $this->response->setContent(json_encode($resp));
                        }
                        else {
                            unlink($baseLocation.$name);
                            $this->response->setStatusCode(500, "No se pudo guardar el archivo");
                        }
                    }
                    else{
                        $this->response->setStatusCode(500, "No se pudo guardar el archivo");
                    }

                }
                else{
                    $this->response->setStatusCode(400);
                }
            }
        }
        else{
            $this->response->setStatusCode(405);
        }
        return $this->response;
    }

    public function getAction($id){
        if($id != null and $id != ""){
            $m = Multimedia::findFirstById($id);
            if($m){
                $aux = new \stdClass();
                $aux->id = $m->id;
                $aux->ruta = $m->ruta;
                $aux->nombre = $m->nombre;
                $aux->fecha = implode("/", array_reverse(explode("-", $m->fecha)));
                $aux->tipo = $m->tipo;
                $aux->descripcion = $m->descripcion;
                $this->response->setContent(json_encode($aux));
            }
            else{
                $this->response->setStatusCode(404);
            }
        }
        else{
            $this->response->setStatusCode(400);
        }
        return $this->response;
    }

    public function deactivateAction($id){
        if($id != null and $id != ""){
            $m = Multimedia::findFirstById($id);
            if($m){
                $m->fecha_modificacio = date("c");
                $m->activo = false;
                $m->save();
            }
            else{
                $this->response->setStatusCode(404);
            }
        }
        else{
            $this->response->setStatusCode(400);
        }
        return $this->response;
    }

    public function activateAction($id){
        if($id != null and $id != ""){
            $m = Multimedia::findFirstById($id);
            if($m){
                $m->fecha_modificacio = date("c");
                $m->activo = true;
                $m->save();
            }
            else{
                $this->response->setStatusCode(404);
            }
        }
        else{
            $this->response->setStatusCode(400);
        }
        return $this->response;
    }
}
