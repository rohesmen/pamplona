<?php
namespace Vokuro\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Vokuro\Models\Users\Application;
use Vokuro\Models\Users\Device;
use Vokuro\Models\Users\Permissions;
use Vokuro\Models\Users\Profiles;
use Vokuro\Models\Users\Users;
use Vokuro\Models\Users\Session;
/**
 * ControllerBase
 * This is the base controller for all controllers in the application
 */
class GeoserverControllerBase extends Controller
{
    /**
     * Execute before the router so we can determine if this is a private controller, and must be authenticated, or a
     * public controller that is open to all.
     *
     * @param Dispatcher $dispatcher
     * @return boolean
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $controllerName = $dispatcher->getControllerName();
        $actionName = $dispatcher->getActionName();

        $istoken = $this->request->has("token") || $this->request->has("TOKEN");
        if ($istoken) {
            $token = $this->request->has("token") ? $this->request->get("token") : $this->request->get("TOKEN");
            if (!empty($token)) {
                if("6607e3c028e84b422effca0e22c0b1932b8e253f" == $token ){
                    return true;
                }
                else{
                    $this->view->setVar("TOKEN", "");
                    $this->response->setStatusCode(404);
                    $dispatcher->forward([
                        'controller' => 'index',
                        'action' => 'pageNotFound'
                    ]);
                    return false;
                }
            }
            else {
                $this->view->setVar("TOKEN", "");
                $this->response->setStatusCode(404);
                $dispatcher->forward([
                    'controller' => 'index',
                    'action' => 'pageNotFound'
                ]);
                return false;
            }
        }
        else {
            $this->view->setVar("TOKEN", "");
            $this->response->setStatusCode(404);
            $dispatcher->forward([
                'controller' => 'index',
                'action' => 'pageNotFound'
            ]);
            return false;
        }

    }
}
