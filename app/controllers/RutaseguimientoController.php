<?php
namespace Vokuro\Controllers;
use Vokuro\DT\SSPGEO;
use Vokuro\GenericSQL\GenericSQL;
use Vokuro\Models\Ruta;
use Vokuro\Models\RutaRecoleccion;
use Vokuro\Models\TipoRecoleccion;
use Vokuro\Models\DiasRecoleccion;
use Vokuro\Models\Turno;
use Vokuro\Models\BitacoraCambios;

/**
 * Display the default index page.
 */
class RutaseguimientoController extends ControllerBase {

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function indexAction() {
        $this->view->setTemplateBefore('public');

        $conacciones = 'no';
        if($this->acl->isAllowedUser('rutaseguimiento', 'edit') or $this->acl->isAllowedUser('rutaseguimiento', 'deactivate') or $this->acl->isAllowedUser('rutaseguimiento', 'activate') or $this->acl->isAllowedUser('rutaseguimiento', 'info')){
            $conacciones = 'si';
        }
        $this->view->setVar('coacciones', $conacciones);

        $tipo_recoleccion = TipoRecoleccion::find([
            "activo = true",
            "order" => "tipo asc"
        ]);
        $this->view->setVar('tipo_recoleccion', $tipo_recoleccion);

        $dias_recoleccion = DiasRecoleccion::find([
            "activo = true",
            "order" => "dia_bd asc"
        ]);
        $this->view->setVar('dias_recoleccion', $dias_recoleccion);

        $turno = Turno::find([
            "activo = true",
            "order" => "nombre asc"
        ]);
        $this->view->setVar('turno', $turno);

    }

    public function buscarAction(){
        $request = $this->request;
        $response = $this->response;


        $columns = array(
            array( 'db' => '', 'dt' => 0,
                'formatter' => function( $d, $row ) {
                    $buttons = '';
                    if($this->acl->isAllowedUser('rutaseguimiento', 'info')){
                        $buttons .= '<button class="btn btn-primary btn-sm rutaseguimiento-info" data-id="'.$row["id"].'" type="button" title="Consultar">
                            <i class="fa fa-info"></i>
                        </button> ';
                    }
                    if($this->acl->isAllowedUser('rutaseguimiento', 'edit') && $row["activo"] === true){
                        $buttons .= '<button class="btn btn-primary btn-sm rutaseguimiento-edit" data-id="'.$row["id"].'" type="button" title="Editar">
                            <i class="fa fa-pencil"></i>
                        </button> ';
                    }
                    if($this->acl->isAllowedUser('rutaseguimiento', 'deactivate') && $row["activo"] === true){
                        $buttons .= '<button class="btn btn-danger btn-sm rutaseguimiento-delete" data-id="'.$row["id"].'" type="button" title="¿Desea desactivar?">
                            <i class="fa fa-times"></i>
                        </button> ';
                    }
                    if($this->acl->isAllowedUser('rutaseguimiento', 'activate') && $row["activo"] === false){
                        $buttons .= '<button class="btn btn-info btn-sm rutaseguimiento-active" data-id="'.$row["id"].'" type="button" title="¿Desea activar?">
                            <i class="fa fa-check"></i>
                        </button> ';
                    }
                    return $buttons;
                }
            ),
            array( 'db' => '', 'dt' => 1),
            array( 'db' => 'activo', 'datatype' => 'boolean', 'dt' => 2,
                'formatter' => function( $d, $row ) {
                    $vigente = $d ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                        : '<i class="fa fa-remove" style="color: red" title="Inactivo"></i>';
                    return $vigente;
                }
            ),
            array( 'db' => 'id', 'dt' => 3, 'datatype' => 'number'),
            array( 'db' => 'nombre', 'dt' => 4),
            array( 'db' => 'fecha_creacion_f', 'datatype' => 'date', 'dt' => 5),
            array( 'db' => 'fecha_modificacion_f', 'datatype' => 'date', 'dt' => 6),
            array( 'db' => 'activo', 'datatype' => 'boolean', 'dt' => 7),
            array( 'db' => 'geojson', 'dt' => 8),
            array( 'db' => 'descripcion', 'dt' => 9),
        );

        $data = SSPGEO::complex_geo($this->request->get(), "monitoreo.view_ruta", "id", $columns);

        $response->setContent(json_encode($data));
        return $response;
    }

    public function saveAction(){

        $rawBody = $this->request->getJsonRawBody();

        $id = $rawBody->id;
        $nombre = mb_strtoupper(trim($rawBody->nombre));
        $descripcion = mb_strtoupper(trim($rawBody->descripcion));
        //$rutarecoleccion = $rawBody->rutarecoleccion;

        $geoJson = $rawBody->geoJson;
        $kml = $rawBody->kml;
        $sizexml = $rawBody->sizexml;
        $namexml = $rawBody->namexml;

        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];

        if($nombre == ""){
            $this->response->setStatuscode(400, "No se ingresó nombre de la ruta");
            return $this->response;
        }

        $sql = "replace(upper(nombre), ' ', '') = replace(upper('$nombre'), ' ', '')";

        if(intval($id) > 0){
            $sql .= " AND id <> $id";
        }

        $busqueda = Ruta::findFirst($sql);

        if($busqueda){
            $this->response->setStatuscode(409, "Ya existe un registro ".($busqueda->activo ? "activo" : "inactivo")." con el mismo nombre");
            return $this->response;
        }

        $dataOrigin = null;
        $accion = "CREACION";

        if(intval($id) > 0){
            $data = Ruta::findFirstById($id);
            $dataOrigin = json_encode($data);
            $accion = "EDICION";
        }
        else{
            $data = new Ruta();
            $data->fecha_creacion = date("c");
        }

        $data->activo = true;
        $data->idusuario = $idUser;
        $data->nombre = $nombre;
        $data->descripcion = $descripcion != "" ? $descripcion : null;
        $data->fecha_modificacion = date("c");

        $data->geojson = $geoJson;
        $data->size_archivo = $sizexml;
        $data->nombre_archivo = $namexml;
        $data->kml = $kml;

        $this->db->begin();
        if($data->save()){

            $data->refresh();

            $dataB = new BitacoraCambios();
            $dataB->identificador = $data->id;
            $dataB->modulo = 'RUTSEG';
            $dataB->idusuario = $idUser;
            $dataB->tabla = "monitoreo.ruta";
            $dataB->cambios = json_encode($data);
            $dataB->original = $dataOrigin;
            $dataB->accion = $accion . " MONITOREO RUTA";

            if($dataB->save()){
                /*foreach ($rutarecoleccion as $valor) {

                    $lSave = false;
                    $detalleOrigin = null;
                    $detalleAccion = "CREACION";

                    //$this->logger->info("valor: ".json_encode($valor));

                    if($valor[8] && $valor[8] == true && intval($valor[9]) == -1) {
                        $detalle = new RutaRecoleccion();
                        $detalle->idruta = $data->id;
                        $detalle->dia = $valor[5];
                        $detalle->tipo_recoleccion = $valor[6];
                        $detalle->idturno = $valor[7];
                        $detalle->idusuario = $idUser;
                        $detalle->activo = true;
                        $detalle->fecha_creacion = 'NOW()';
                        $detalle->fecha_modificacion = 'NOW()';
                        $lSave = true;
                    }else if((!$valor[8]) && $valor[8] == false && intval($valor[9]) > 0) {
                        $detalleAccion = "ELIMINACION";
                        $detalle = RutaRecoleccion::findFirstById($valor[9]);
                        $detalleOrigin = json_encode($detalle);
                        $detalle->idusuario = $idUser;
                        $detalle->idusuario_desactivo = $idUser;
                        $detalle->activo = false;
                        $detalle->fecha_modificacion = 'NOW()';
                        $lSave = true;
                    }

                    if($lSave) {
    
                        if(!$detalle->save()){
                            $this->db->rollback();
                            foreach ($detalle->getMessages() as $message) {
                                $this->logger->info("(create-delete-detalle-rutarecoleccionseguimiento): " . $message);
                            }
                            $mensaje = "Ocurrió un error al crear/eliminar las rutas de recoleccion.";
                            $this->logger->error($mensaje);
                            $this->response->setStatusCode(500, $mensaje);
                            return $this->response;
                        }

                        $detalle->refresh();

                        $dataB = new BitacoraCambios();
                        $dataB->identificador = $detalle->id;
                        $dataB->modulo = 'RUTSEG';
                        $dataB->idusuario = $idUser;
                        $dataB->tabla = "monitoreo.recoleccion";
                        $dataB->cambios = json_encode($detalle);
                        $dataB->original = $detalleOrigin;
                        $dataB->accion = $detalleAccion . " MONITOREO RUTA RECOLECCION";
    
                        if (!$dataB->save()) {
                            $this->db->rollback();
                            foreach ($dataB->getMessages() as $message) {
                                $this->logger->info("(save-bitacora-detalle-rutarecoleccionseguimiento): " . $message);
                            }
                            $mensaje = "Ocurrió un error al guardar la bitacora de las rutas de recoleccion.";
                            $this->logger->error($mensaje);
                            $this->response->setStatusCode(500, $mensaje);
                            return $this->response;
                        }
                    }
                }*/
                $this->db->commit();
            }else{
                $this->db->rollback();
                foreach ($dataB->getMessages() as $message) {
                    $this->logger->error("save-bitacora-rutaseguimiento: ".$message->getMessage());
                }
                $mensaje = "Ocurrió un error al guardar la bitacora.";
                $this->logger->error($mensaje);
                $this->response->setStatusCode(500, $mensaje);
            }            
        }else{
            $this->db->rollback();
            foreach ($data->getMessages() as $message) {
                $this->logger->error("save-rutaseguimiento: ".$message->getMessage());
            }
            $mensaje = "Ocurrió un error al guardar el registro.";
            $this->logger->error($mensaje);
            $this->response->setStatusCode(500, $mensaje);
        }
        return $this->response;
    }

    public function deleteAction($id, $activo){
        if(!empty($id)){
            $d = Ruta::findFirstById($id);
            if ($d) {

                $identity = $this->auth->getIdentity();
                $idUser = $identity["id"];

                $this->db->begin();

                $dataOrigin = json_encode($d);
                $accion = "DESACTIVACION";
                if($activo === true || $activo == "true"){
                    $accion = "ACTIVACION";
                    $d->idusuario_desactivo = null;
                } else{
                    $d->idusuario_desactivo = $idUser;
                }
                $d->activo = $activo;
                $d->idusuario = $idUser;
                $d->fecha_modificacion = date("c");
                if(!$d->save()){
                    $this->db->rollback();
                    foreach ($d->getMessages() as $message) {
                        $this->logger->info("(delete-rutaseguimiento): " . $message);
                    }
                    $mensaje = "Ocurrió un error al guardar el registro.";
                    $this->logger->error($mensaje);
                    $this->response->setStatusCode(500, $mensaje);
                }else{
                    $d->refresh();
                    $dataB = new BitacoraCambios();
                    $dataB->identificador = $d->id;
                    $dataB->modulo = 'RUTSEG';
                    $dataB->idusuario = $idUser;
                    $dataB->tabla = "monitoreo.ruta";
                    $dataB->cambios = json_encode($d);
                    $dataB->original = $dataOrigin;
                    $dataB->accion = $accion . " MONITOREO RUTA";
                    if (!$dataB->save()) {
                        $this->db->rollback();
                        foreach ($dataB->getMessages() as $message) {
                            $this->logger->info("(save-bitacora-delete-rutaseguimiento): " . $message);
                        }
                        $mensaje = "Ocurrió un error al guardar la bitacora.";
                        $this->logger->error($mensaje);
                        $this->response->setStatusCode(500, $mensaje);
                    }
                    $this->db->commit();
                }
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(400, "Bad Request");
        }
        return $this->response;
    }

    public function getAction($id){
        if($this->request->isGet()){
            if(intval($id) > 0){
                /*$sql = "SELECT id, dia, tipo_recoleccion, turno, iddia, idtipo_recoleccion, idturno, orden
                FROM monitoreo.view_ruta_recoleccion
                WHERE activo AND idruta = $id";
                $data = GenericSQL::getBySQL($sql);
                $resp = array();
                foreach ($data as $i){
                    array_push($resp, array(
                        "",
                        '<button class="btn btn-sm btn-danger delete_ruta_recoleccion" data-id="'.$i->id.'" title="¿Desea eliminar?"><i class="fa fa-remove"></i></button>',
                        $i->dia,
                        $i->tipo_recoleccion,
                        $i->turno,
                        $i->iddia,
                        $i->idtipo_recoleccion,
                        $i->idturno,
                        true,
                        $i->id,
                        $i->orden
                    ));
                }*/
                $ruta = Ruta::findFirst($id);
                $data = array(
                    //"days" => $resp,
                    "ruta" => $ruta
                );
                $this->response->setContent(json_encode($data));
            }else{
                $this->response->setStatusCode(400);
            }
        }else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

}
