<?php
/**
 * Clase controller para el Módulo de los clientes
 * @author [russel] <[<email address>]>
 */
namespace Vokuro\Controllers;

use Phalcon\Tag;
use Vokuro\DT\SSPGEO;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Vokuro\GenericSQL\GenericSQL;
use Vokuro\Models\Cobratario;
use Phalcon\Mvc\View;
use html2pdf;

class BitmensController extends ControllerBase
{

    private function validaFecha($fecha){
        $error = true;
        if($fecha != "") {
            if (preg_match("/([0-9]{2})\-([0-9]{2})\-([0-9]{4})/", $fecha, $matches)) {
                if (!checkdate($matches[2], $matches[1], $matches[3])) {
                    $error = true;
                }else{
                    $error = $matches[3]."-".$matches[2]."-".$matches[1];
                }
            } else {
                $error = true;
            }
        }
        return $error;
    }


    /**
     * [indexAction  Default action. Set the public layout (layouts/public.volt)]
     * @return [view] [Vista para mensualidades]
     */
    public function indexAction()
    {
        $this->view->setTemplateBefore('public');
        $this->view->setVar('cobratarios', Cobratario::findByActivo());
    }

    function buscarAction(){

        $columns = array(
            array( 'db' => '', 'dt' => 0),
            array( 'db' => 'cliente', 'dt' => 1, 'datatype' => 'number'),
            array( 'db' => 'mensualidad', 'dt' => 2),
            array( 'db' => 'idusuario', 'dt' => 3, 'datatype' => 'number'),
            array( 'db' => 'nombre_completo_usuario', 'dt' => 4),
            array( 'db' => 'fecha', 'dt' => 5, 'datatype' => 'date'),
            array( 'db' => 'origen', 'dt' => 6),
            array( 'db' => 'motivo', 'dt' => 7),
            array( 'db' => '', 'dt' => 8),
            array( 'db' => '', 'dt' => 9),
            array(
                'db' => 'cliente',
                'dt' => 'DT_RowId',
                'formatter' => function( $d, $row ) {
                    // Technically a DOM id cannot start with an so we prefix
                    // a string. This can also be useful if you have multiple tables
                    // to ensure that the id is unique with a different prefix
                    return 'cliente-'.$d.'cobratario-'.$row["idusuario"].'fecha-'.$row["fecha"];
                }
            )
        );

        $whereResult = "";
        $whereAll = "";
        $request = $this->request->get();

        $FechaIni = $request['columns'][8]["search"]["value"];
        $FechaFin = $request['columns'][9]["search"]["value"];

        if($FechaIni != "" && $FechaIni != null) {
            $whereAll .= "date(fecha) >= date('$FechaIni')";
        }
        if($FechaFin != "" && $FechaFin != null) {
            $whereAll .= $whereAll != "" ? " AND " : "";
            $whereAll .= "date(fecha) <= date('$FechaFin')";
        }

        $request['columns'][8]["search"]["value"] = "";
        $request['columns'][9]["search"]["value"] = "";

        $data = SSPGEO::complex_geo($request, "comun.view_bitacora_mensualidades", "cliente", $columns, $whereResult, $whereAll);

        $this->response->setContent(json_encode($data));
        return $this->response;

    }


    function exportAction(){

        $this->view->disable();

        $where = "";
        $rawBody = $this->request->getJsonRawBody();
        $fechaIni = $rawBody->fechaInicio;
        $fechaFin = $rawBody->fechaFin;
        $cobratario = intval($rawBody->idCobratario);

        if($fechaIni != "" && $fechaIni != null) {
            $where .= $where != "" ? " AND " : " WHERE ";
            $where .= "date(fecha) >= date('$fechaIni')";
        }
        if($fechaFin != "" && $fechaFin != null) {
            $where .= $where != "" ? " AND " : " WHERE ";
            $where .= "date(fecha) <= date('$fechaFin')";
        }
        if($cobratario > 0) {
            $where .= $where != "" ? " AND " : " WHERE ";
            $where .= "idusuario  = ".$cobratario;
        }

        $sql = "SELECT cliente, mensualidad, nombre_completo_usuario, fecha, origen, motivo
        FROM comun.view_bitacora_mensualidades $where ORDER BY cliente DESC, idusuario DESC, date(fecha) DESC";

        $result = GenericSQL::getBySQL($sql);
        $this->response->setContent(json_encode($result));
        return $this->response;
    }

}