<?php
namespace Vokuro\Controllers;
use Phalcon\Mvc\View;
use Vokuro\DT\SSPGEO;
use Vokuro\GenericSQL\GenericSQL;
use Vokuro\Models\Cuadrilla;
use Vokuro\Models\CuadrillaRecolector;
use Vokuro\Models\CuadrillaAsignacion;
use Vokuro\Models\DiasRecoleccion;
use Vokuro\Models\Recoleccion;
use Vokuro\Models\Recolector;
use Vokuro\Models\BitacoraCambios;
use Vokuro\Models\Ruta;
use Vokuro\Models\TipoRecoleccion;
use Vokuro\Models\Turno;
use Vokuro\Models\Unidad;

/**
 * Display the default index page.
 */
class SeguimientoasigController extends ControllerBase {

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function indexAction() {
        $this->view->setTemplateBefore('public');

        $rutas = Ruta::find([
            "activo",
            "order" => "nombre"
        ]);

        $dias = DiasRecoleccion::find([
            "activo",
            "order" => "dia_bd"
        ]);

        $turno = Turno::find([
            "activo",
            "order" => "nombre"
        ]);

        $unidades = Unidad::find([
            "activo",
            "order" => "placa"
        ]);

        $recoleccion = Recoleccion::find([
            "activo",
            "order" => "idruta,dia, tipo_recoleccion"
        ]);
        $sqlRec = "select p.id, p.idruta, p.dia sigla_dia, dr.dia dia_nombre, dr.abreviatura dia_abrev, p.tipo_recoleccion sigla_tipo_recoleccion, tr.tipo tipor_nombre,
                p.idturno, tu.nombre turno_nombre, p.placa, c.nombre cuadrilla, p.idcuadrilla
        from monitoreo.recoleccion p 
        join monitoreo.ruta ru on p.idruta = ru.id
        join monitoreo.dias_recoleccion dr on p.dia = dr.sigla
        join monitoreo.tipo_recoleccion tr on p.tipo_recoleccion = tr.sigla
        join monitoreo.turno tu on p.idturno = tu.id
        join monitoreo.cuadrilla c on p.idcuadrilla = c.id
        join monitoreo.unidad u on p.placa = u.placa
        where p.activo and ru.activo and dr.activo and tr.activo and tu.activo and c.activo and u.activo
        order by ru.nombre, dr.dia_bd, tr.tipo";
        $recoleccion = GenericSQL::getBySQL($sqlRec);
        $dataRecoleccion = [];
        foreach ($recoleccion as $r){
            if(!isset($dataRecoleccion[$r->sigla_dia][$r->idruta])){
                $dataRecoleccion[$r->sigla_dia][$r->idruta] = $r;
            }
        }

        $dataUnidad = [];
        foreach ($unidades as $u){
            $sqlCua = "select c.* from monitoreo.unidad_cuadrilla uc 
            join monitoreo.cuadrilla c on uc.idcuadrilla = c.id
            where uc.placa = '".$u->placa."' and uc.activo and c.activo order by c.nombre";
            $cuad = GenericSQL::getBySQL($sqlCua);
            if(count($cuad)){
                $dataCuad = [];
                foreach ($cuad as $c){
                    $sqlRec = "select r.*, cr.ischofer from monitoreo.cuadrilla_recolector cr 
                    join monitoreo.recolector r on cr.idrecolector = r.id
                    where cr.idcuadrilla = ".$c->id." and r.activo and cr.activo order by cr.ischofer desc, r.apepat, r.apemat, r.nombres";
                    $recs = GenericSQL::getBySQL($sqlRec);
                    $auxCuad = array(
                        "data" => $c,
                        "recolectores" => $recs
                    );
                    array_push($dataCuad, $auxCuad);
                }
                $dataUnidad[$u->placa] = array(
                    "unidad" => $u,
                    "cuadrilla" => $dataCuad
                );
            }
        }

        $this->view->setVar('rutas', $rutas);
        $this->view->setVar('dias', $dias);
        $this->view->setVar('turnos', $turno);
        $this->view->setVar('cturnos', count($turno));
        $this->view->setVar('unidades', $dataUnidad);
        $this->view->setVar('recoleccion', $dataRecoleccion);

        //modulo de cuadrillas
        $recolectores = Recolector::find([
            "activo = true",
            "order" => "nombres, apepat, apemat"
        ]);
        $this->view->setVar('recolectores', $recolectores);

    }

    public function deactivateAction($id){
        $this->view->disable();
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];

        $rec = Recoleccion::findFirst($id);
        $dataOrigin = json_encode($rec);
        $rec->activo = false;
        $rec->fechamodificacion = date('c');
        $rec->idusuario_desactivo = $idUser;

        $this->db->begin();
        if($rec->save()){
            $rec->refresh();

            $dataB = new BitacoraCambios();
            $dataB->identificador = $rec->id;
            $dataB->modulo = 'PROGRECO';
            $dataB->idusuario = $idUser;
            $dataB->tabla = "monitoreo.recoleccion";
            $dataB->cambios = json_encode($rec);
            $dataB->original = $dataOrigin;
            $dataB->accion = "DESACTIVAR PROGRAMACION RECOLECCION";

            if($dataB->save()){

                $this->deactivatedetails($rec->id);

                $this->db->commit();
            }
            else {
                $this->db->rollback();
                foreach ($dataB->getMessages() as $message) {
                    $this->logger->error("save-bitacora-deactivave-recoleccion: ".$message->getMessage());
                }
                $mensaje = "Ocurrió un error al guardar la bitacora.";
                $this->logger->error($mensaje);
                $this->response->setStatusCode(500);
            }
        }
        else{
            $this->db->rollback();
            foreach ($rec->getMessages() as $message) {
                $this->logger->error("deactivave-recoleccion: ".$message->getMessage());
            }
            $this->response->setStatusCode(500);
        }

        return $this->response;
    }

    private function deactivatedetails($id){
        if(intval($id) > 0){
            $identity = $this->auth->getIdentity();
            $idUser = $identity["id"];
            $cuadasig = CuadrillaAsignacion::find("activo and idrecoleccion = $id");
            $mensaje = "";
            foreach($cuadasig as $detalle){
                $detalleOrigin = json_encode($detalle);
                $detalle->idusuario = $idUser;
                $detalle->idusuario_desactivo = $idUser;
                $detalle->activo = false;
                $detalle->fecha_modificacion = 'NOW()';
                if(!$detalle->save()){
                    foreach ($detalle->getMessages() as $message) {
                        $this->logger->info("(delete-detalle-cuadrillarecolectorasigseguimiento): " . $message);
                    }
                    $mensaje = "Ocurrió un error al eliminar cudrilla-recolector-asignacion.";
                    $this->logger->error($mensaje);
                    return $mensaje;
                }
                $detalle->refresh();
                $dataB = new BitacoraCambios();
                $dataB->identificador = $detalle->id;
                $dataB->modulo = 'PROGRECO';
                $dataB->idusuario = $idUser;
                $dataB->tabla = "monitoreo.cuadrilla_asignacion";
                $dataB->cambios = json_encode($detalle);
                $dataB->original = $detalleOrigin;
                $dataB->accion = "ELIMINACION MONITOREO CUADRILLA RECOLECTOR ASIGNACION D";
                if (!$dataB->save()) {
                    foreach ($dataB->getMessages() as $message) {
                        $this->logger->info("(save-bitacora-detalle-cuadrillarecolectorasigseguimiento): " . $message);
                    }
                    $mensaje = "Ocurrió un error al guardar la bitacora de cudrilla-recolector-asignacion.";
                    $this->logger->error($mensaje);
                    return $mensaje;
                }
            }
        }
        return null;
    }

    public function createAction(){
        $this->view->disable();
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];

        $data = $this->request->getJsonRawBody();
        $id = $data->id;
        $idruta = $data->idruta;
        $placa = $data->placa;
        $dia = $data->dia;
        $idturno = $data->idturno;
        $idcuadrilla = $data->idcuadrilla;
        $cuadrillarecolector = $data->cuadrillarecolectorasignacion;

        $this->logger->info(json_encode($data));

        $dataOrigin = null;
        $accion = "ALTA ";

        if($id){
            //desactivar asignacion anterior
            $this->deactivatedetails($id);
            $rec = Recoleccion::findFirst($id);
            $dataOrigin = json_encode($data);
            $rec->fechamodificacion = date('c');
            $accion = "EDICION ";
        }
        else{
            $rec = new Recoleccion();
            $rec->idusuario = $idUser;
            $rec->idruta = $idruta;
            $rec->dia = $dia;
        }
        $rec->placa = $placa;
        $rec->idturno = $idturno;
        $rec->idcuadrilla = $idcuadrilla;
        $rec->tipo_recoleccion = TipoRecoleccion::ORGANICA;

        $this->db->begin();
        if(!$rec->save()){
            $this->db->rollback();
            foreach ($rec->getMessages() as $message) {
                $this->logger->error("crear/actualiza-recoleccion: ".$message->getMessage());
            }
            $this->response->setStatusCode(500);
        }
        else{
            $rec->refresh();
            $dataB = new BitacoraCambios();
            $dataB->identificador = $rec->id;
            $dataB->modulo = 'PROGRECO';
            $dataB->idusuario = $idUser;
            $dataB->tabla = "monitoreo.recoleccion";
            $dataB->cambios = json_encode($rec);
            $dataB->original = $dataOrigin;
            $dataB->accion = $accion . " PROGRAMACION RECOLECCION";

            if($dataB->save()){
                foreach ($cuadrillarecolector as $valor) {

                    $lSave = false;
                    $detalleOrigin = null;
                    $detalleAccion = "CREACION";

                    $this->logger->info("valor: ".json_encode($valor));

                    if($valor[6] && $valor[6] == true && intval($valor[7]) == -1) {
                        $detalle = new CuadrillaAsignacion();
                        $detalle->idrecoleccion = $rec->id;
                        $detalle->idcuadrilla = $rec->idcuadrilla;
                        $detalle->idrecolector = $valor[4];
                        $detalle->ischofer = $valor[5];
                        $detalle->idusuario = $idUser;
                        $detalle->activo = true;
                        $detalle->fecha_creacion = 'NOW()';
                        $detalle->fecha_modificacion = 'NOW()';
                        $lSave = true;
                    }else if((!$valor[6]) && $valor[6] == false && intval($valor[7]) > 0) {
                        $detalleAccion = "ELIMINACION";
                        $detalle = CuadrillaAsignacion::findFirstById($valor[7]);
                        $detalleOrigin = json_encode($detalle);
                        $detalle->idusuario = $idUser;
                        $detalle->idusuario_desactivo = $idUser;
                        $detalle->activo = false;
                        $detalle->fecha_modificacion = 'NOW()';
                        $lSave = true;
                    }else if(($valor[6]) && $valor[6] == true && $valor[8] == true && intval($valor[7]) > 0) {
                        $detalleAccion = "EDICION";
                        $detalle = CuadrillaAsignacion::findFirstById($valor[7]);
                        $detalleOrigin = json_encode($detalle);
                        $detalle->idusuario = $idUser;
                        $detalle->ischofer = $valor[5];
                        $detalle->fecha_modificacion = 'NOW()';
                        $lSave = true;
                    }

                    if($lSave) {
    
                        if(!$detalle->save()){
                            $this->db->rollback();
                            foreach ($detalle->getMessages() as $message) {
                                $this->logger->info("(create-delete-update-detalle-cuadrillarecolectorasigseguimiento): " . $message);
                            }
                            $mensaje = "Ocurrió un error al crear/eliminar/editar cudrilla-recolector-asignacion.";
                            $this->logger->error($mensaje);
                            $this->response->setStatusCode(500, $mensaje);
                            return $this->response;
                        }

                        $detalle->refresh();

                        $dataB = new BitacoraCambios();
                        $dataB->identificador = $detalle->id;
                        $dataB->modulo = 'PROGRECO';
                        $dataB->idusuario = $idUser;
                        $dataB->tabla = "monitoreo.cuadrilla_asignacion";
                        $dataB->cambios = json_encode($detalle);
                        $dataB->original = $detalleOrigin;
                        $dataB->accion = $detalleAccion . " MONITOREO CUADRILLA RECOLECTOR ASIGNACION";
    
                        if (!$dataB->save()) {
                            $this->db->rollback();
                            foreach ($dataB->getMessages() as $message) {
                                $this->logger->info("(save-bitacora-detalle-cuadrillarecolectorasigseguimiento): " . $message);
                            }
                            $mensaje = "Ocurrió un error al guardar la bitacora de cudrilla-recolector-asignacion.";
                            $this->logger->error($mensaje);
                            $this->response->setStatusCode(500, $mensaje);
                            return $this->response;
                        }
                    }
                }
                $this->db->commit();
                $btnDeac = '';
                if($this->acl->isAllowedUser('seguimientoasig', 'deactivate')){
                    $btnDeac = '<button class="btn btn-primary btn-sm delete-asignacion-unidad" data-id="'.$rec->id.'">
                                        <i class="fa fa-remove"></i>
                                    </button>';
                }
                $cuad = Cuadrilla::findFirst($idcuadrilla);
                $turno = Turno::findFirst($idturno);
                $html = '<div>
                                <h5>'.$placa.'</h5>
                                <div class="cuadrilla nombrecuadrilla-'.$cuad->id.'">'.$cuad->nombre.'</div>
                                <div class="turno">'.$turno->nombre.'</div>
                                <div class="acciones-unidad">'.$btnDeac.'</div>
                            </div>';
                $this->response->setContent($html);
            }
            else{
                $this->db->rollback();
                foreach ($dataB->getMessages() as $message) {
                    $this->logger->error("bitacora-crear/actualiza-recoleccion: ".$message->getMessage());
                }
                $mensaje = "Ocurrió un error al guardar la bitacora.";
                $this->logger->error($mensaje);
                $this->response->setStatusCode(500, $mensaje);
            }
        }

        return $this->response;
    }

    public function consultaunidadAction($placa){
        $view = clone $this->view;
        $this->view->disable();
        $view->start();
        $view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $unidad = Unidad::findFirstByPlaca($placa);
        $view->setvar("unidad", $unidad);
        $view->render('seguimientoasig', 'form-unidades');
        $view->finish();
        $this->response->setContent($view->getContent());
        return $this->response;
    }

    public function getRecolectoresCuadrillaAction($id){
        if($this->request->isGet()){
            $result = CuadrillaRecolector::find("idcuadrilla = $id AND activo");
            $resp = array();
            $table = array();
            $aux = new \stdClass();
            $aux->id = "";
            $aux->text = "Seleccionar";
            array_push($resp, $aux);
            foreach ($result as $data){
                $datarec = $data->getRecolector();
                if($datarec){
                    $aux = new \stdClass();
                    $aux->id = $datarec->id;
                    $aux->text = $datarec->getFullName();
                    array_push($resp, $aux);
                    array_push($table, array(
                        "",
                        '<button class="btn btn-sm btn-danger adddelete_cuadrilla_recoleccion" data-id="-1" title="¿Desea eliminar?"><i class="fa fa-remove"></i></button>',
                        $datarec->getFullName(),
                        $data->ischofer ? "CHOFER" : "RECOLECTOR",
                        $datarec->id,
                        $data->ischofer,
                        true,
                        -1,
                        false
                    ));
                }
            }
            $this->response->setContent(json_encode(array("select" => $resp, "tabla" => $table)));
        }else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

}
