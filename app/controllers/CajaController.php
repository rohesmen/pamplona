<?php
namespace Vokuro\Controllers;

use Vokuro\GenericSQL\GenericSQL;
use Vokuro\Models\BitacoraCambios;
use Vokuro\Models\ClienteDescuento;
use Vokuro\Models\ClienteDescuentoDetalle;
use Vokuro\Models\Clientes;
use Phalcon\Mvc\View;
use Vokuro\Models\Colonias;
use Vokuro\Models\Foliador;
use Vokuro\Models\Folios;
use Vokuro\Models\HistorialPago;
use Vokuro\Models\InventarioFolios;
use Vokuro\Models\MetodoPago;
use Vokuro\Models\MotioDescuento;
use Vokuro\Models\Promociones;
use Vokuro\Models\RangoFolio;
use Vokuro\Models\SecuenciaFolios;
use Vokuro\Models\TarifaColonia;
use Vokuro\Models\TipoDescuento;
use Vokuro\Models\Tipo_servicio;
use Vokuro\Models\Forma_pago;
use Vokuro\Models\Users;
use Vokuro\Models\RazonSocial;

use Phalcon\Mvc\User\Component;
use Phalcon\Acl\Adapter\Memory as AclMemory;
use Phalcon\Acl\Role as AclRole;
use Phalcon\Acl\Resource as AclResource;
use Vokuro\Models\Profiles;
use Vokuro\Models\Permissions;
use Vokuro\Models\HistorialCorte;
use Vokuro\Models\Pagos;
use Vokuro\Models\PagosMasivo;

use Swift_Message as Message;
use Swift_SmtpTransport as Smtp;
use Swift_Attachment;

use Vokuro\Mail\Mail;
use html2pdf;
use Vokuro\Util\NumeroLetras;
use Vokuro\Util\Util;
use Vokuro\Util\UtilFile;

/**
 * Display the default index page.
 */
class CajaController extends ControllerBase{

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function indexAction()
    {
        /*$this->view->setVar('logged_in', is_array($this->auth->getIdentity()));*/
        $this->view->setTemplateBefore('public');

        $this->view->setVar('doSearch', $this->acl->isAllowedUser('users', 'search'));
        $this->view->setVar('colonias', Colonias::findByActivo());
        $this->view->setVar('metodopago', MetodoPago::findByActivo());
        $this->view->setVar('tiposervicio', Tipo_servicio::findByActivo(true));
		$this->view->setVar('razonsocial', RazonSocial::findByActivo(true));

        $this->view->setVar('colonias', Colonias::findByActivo());

        $rss = array();
        $razon_social = RazonSocial::find([
            "activo = true",
            "order" => "razon_social",
        ]);
        foreach ($razon_social as $rs){
            $std = new \stdClass();
            $std->text = $rs->razon_social;
            $std->value = $rs->id;
            $std->fisica = $rs->fisica ? "true" : "false";
            array_push($rss, $std);
        }
        $this->view->setVar('rss', $razon_social);
        $this->view->setVar('rss_json', json_encode($rss));
        $proman = Promociones::getAnualVigente();
        $this->view->setVar("proman", count($proman) > 0 ? $proman[0]: null);
    }
    //-------------------------------------------------------------------------
    public function searchAction(){

        $view = clone $this->view;
        $this->view->disable();
        if ($this->request->isPost() == true) {
            $rawBody = $this->request->getJsonRawBody();
            $results = $this->filterSearch($rawBody);
        }
        else{
            $view->setVar('NotImplemented', true);
        }

        $this->response->setContent(json_encode($results));
        return $this->response;
    }
    //-------------------------------------------------------------------------
    private function filterSearch($rawBody){

        $request  = $this->request;
        $response = $this->response;

        $nombre  = $rawBody->nombre;
        $clave   = $rawBody->clave;
        $tipo    = $rawBody->tipo;
        $calle   = $rawBody->calle;
        $numero  = $rawBody->numero;
        $colonia = $rawBody->colonia;
        $empresa = $rawBody->empresa;
        $folio = $rawBody->folio;
        $privada = $rawBody->privada;


        $clave = preg_replace('/\s+/', ' ',$clave);
        $nombre = preg_replace('/\s+/', ' ',$nombre);
        $tipo = preg_replace('/\s+/', ' ',$tipo);

        $calle   = preg_replace('/\s+/', ' ',$calle);
        $numero  = preg_replace('/\s+/', ' ',$numero);
        $colonia = preg_replace('/\s+/', ' ',$colonia);
        $empresa = preg_replace('/\s+/', ' ',$empresa);

        $where = ' 1 = 1';
        if($tipo == "nom"){
            $aSearch = explode(' ', $nombre);
            if(count($aSearch) > 1){
                $newParams = $this->eliminarPreposiciones($aSearch);
            }
            else{
                $newParams = $aSearch;
            }

            foreach ($newParams as $valor) {
                $where .= " AND COALESCE(trim(lower(c.apepat)), '')||' '||COALESCE(trim(lower(c.apemat)), '')||' '||COALESCE(trim(lower(c.nombres)), '') LIKE lower('%$valor%') ";
            }
        }
//            $where .= " AND lower(COALESCE(c.apepat, '')||' '||COALESCE(c.apemat, '')||' '||COALESCE(c.nombres, '')) LIKE lower('%$nombre%')";

        if($tipo == "dir") {
            if ($calle != ""){
                $calle = str_replace(' ', '', $calle);
                $where .= " AND lower(coalesce(c.calle::text, '')||coalesce(c.calle_letra, '')) 
                        LIKE lower('%$calle%')";
            }


            if ($numero != "") {
                $numero = str_replace(' ', '', $numero);
                $where .= " AND lower(coalesce(c.numero::text, '')||coalesce(c.numero_letra, '')) 
                        LIKE lower('%$numero%')";
            }

            if ($colonia != "")
                $where .= " AND c.idcolonia = '" . $colonia . "'";
        }//fin:if($tipo == "dir")

        //[2016-12-09] Todos los cliente Vigentes
        if($tipo == "todos"){
            ;
        }//fin:if($tipo == "todos")

        //[2016-12-08] Cliente Vigente por Clave
        if($tipo == "cla"){
            $where .= " AND c.id_cliente = ". $clave;
        }//fin:

        //[2016-12-09] Cliente por Razon Social Vigente
        if($tipo == "rsocial"){
            //$where .= "AND lower(COALESCE(c.razon_social, '')) LIKE lower('%" . $empresa . "%')";
            $where .= " AND (
				lower(COALESCE(c.razon_social, '')) LIKE lower('%" . $empresa . "%') or
				lower(COALESCE(c.nombres, ''))      LIKE lower('%" . $empresa . "%'))";
        }//fin:

        //[2016-12-16] Cliente por Razon Social Vigente
        if($tipo == "folio"){
            $where .= " AND folio_catastral = $folio";
        }//fin:

        //[2018-12-20] Cliente por nombre de privada
        if($tipo == "privada"){
            $where .= " AND nombre_privada ilike '%$privada%'";
        }//fin:

        $results= array();

        $meses = array('enero','febrero','marzo','abril','mayo','junio','julio',
            'agosto','septiembre','octubre','noviembre','diciembre');

        $sqlQuery = "select 
			c.id_cliente, 
			c.calle, 
			c.calle_letra,
			c.numero, 
			c.numero_letra, 
			c.cruza1, 
			c.letra_cruza1,
			c.cruza2,      
			c.letra_cruza2, 
			c.apepat,
			c.apemat,
			c.nombres,
			c.apepat_propietario,
			c.apemat_propietario, 
			c.nombres_propietario,
			c.latitud,
			c.longitud,
			c.razon_social,		
			COALESCE(c.ultimo_anio_pago, 0) as ultimo_anio_pago,
			COALESCE(c.ultimo_mes_pago, '') as ultimo_mes_pago,				
			COALESCE(c.apepat, '')||' '||COALESCE(c.apemat, '')||' '||COALESCE(c.nombres, '')  persona,
			COALESCE(c.apepat_propietario, '')||' '||COALESCE(c.apemat_propietario, '')||' '||COALESCE(c.nombres_propietario, '')  propietario,
			COALESCE(col.nombre, '')||', '||COALESCE(col.localidad, '') colonia,
			COALESCE(col.monto, 0.00) monto,			
			(CASE WHEN COALESCE(c.idruta, 0)  > 0 
			then (select r.nombre from cliente.ruta r where r.id = c.idruta) else ''
			END) as ruta,
			COALESCE(c.correo, '') as correo,
            c.cobro_manual,
            c.monto_cobro,
            c.nombre_privada,
            case when c.comercio = false then 'NO' else coalesce (c.nombre_comercio, '') end comercio, 
            CASE
            WHEN cp.id_cliente IS NOT NULL THEN true
            ELSE false
            END AS pensionado, 
            c.mensualidad,
            c.idestatuscliente,
            ec.nombre estatus_cliente,
            ec.sigla sigla_estatus_cliente,
            ec.permcobro,
            ec.permmen,
            ec.isfactura,
            c.activo,
            c.vigente 
		FROM cliente.cliente c 
		JOIN cliente.colonia AS col ON col.id = c.idcolonia
		LEFT JOIN cliente.cliente_descuento_pensionado cp ON c.id_cliente = cp.id_cliente 
		    and cp.fecha_inicio::date <= now()::date  and cp.fecha_fin::date >= now()::date
		left join cliente.estatus_cliente ec on c.idestatuscliente = ec.id
		WHERE  ". $where. " ORDER BY id_cliente ASC";

        $results = Clientes::findByQuery($sqlQuery);
        //*********************************************************************************************
        $data = array();
        if(count($results) > 0){
            foreach($results as $res){

                $ResultadoAdeudo = 0;

                $calle = ($res->calle != null and $res->calle != "") ? trim($res->calle) : "";
                $calleLetra = ($res->calle_letra != null and $res->calle_letra != "") ? " ".trim($res->calle_letra) : "";
                $calle = $calle.$calleLetra;

                $numero = ($res->numero != null and $res->numero != "") ? trim($res->numero) : "";
                $numeroLetra = ($res->numero_letra != null and $res->numero_letra != "") ? " ".trim($res->numero_letra) : "";
                $numero = $numero.$numeroLetra;

                $cruz1 = ($res->cruza1 != null and $res->cruza1 != "") ? trim($res->cruza1) : "";
                $cruz1Letra = ($res->letra_cruza1 != null and $res->letra_cruza1 != "") ? trim($res->letra_cruza1) : "";
                $cruz1 = $cruz1.$cruz1Letra;

                $cruz2 = ($res->cruza2 != null and $res->cruza2 != "") ? trim($res->cruza2) : "";
                $cruz2Letra = ($res->letra_cruza2 != null and $res->letra_cruza2 != "") ? trim($res->letra_cruza2) : "";
                $cruz2 = $cruz2.$cruz2Letra;

                $colonia = $res->colonia;

                $cruzjoin = "";
                if($cruz1 && $cruz2){
                    $cruzjoin .= " X ".$cruz1." Y ".$cruz2." ";
                }
                else{
                    if($cruz1){
                        $cruzjoin .= " X ".$cruz1." ";
                    }
                    if($cruz2){
                        $cruzjoin .= " X ".$cruz2." ";
                    }
                }
                $dir = $calle." ".$numero.$cruzjoin.$colonia;

                $fullname = (($res->activo && $res->vigente) ? '<i class="fa fa-check" style="color: green" title="Activo"></i> '
                    : '<i class="fa fa-remove" style="color: red" title="Inactivo"></i> ');
                $fullname .= $res->apepat;
                if($fullname !=  ""){$fullname .= " ";}
                $fullname .= $res->apemat;
                if($fullname !=  ""){$fullname .= " ";}
                $fullname .= $res->nombres;

                //Concatenar la razon social es caso que el cliente lo tenga
                $persona = $res->nombres;
                if($res->razon_social !=  ""){
                    $persona .= "(".$res->razon_social.")";
                }

                $propietario = "";
                $propietario .= $res->apepat_propietario;

                if($propietario !=  ""){
                    $propietario .= " ";
                }
                $propietario .= $res->apemat_propietario;
                if($propietario !=  ""){
                    $propietario .= " ";
                }
                $propietario .= $res->nombres_propietario;
                $mensualidad = $res->mensualidad;

                $ultimoMesPago = "";
                $ultimoMesPago = ($res->ultimo_mes_pago != null and $res->ultimo_mes_pago != "") ? trim($res->ultimo_mes_pago) : "";

                if($ultimoMesPago == "" || strlen(trim($res->ultimo_mes_pago)) > 2){
                    $ultimoMesPago = "";
                }//fin:
                else{
                    $idxMes =  intval($res->ultimo_mes_pago) - 1;
                    $ultimoMesPago = $meses[$idxMes] ;
                }//fin:else

                //Validamos el adeudo del cliente.
                //si hay ultimo año y ultimo mes se calcula el adeudo
                if ($res->ultimo_anio_pago > 0 && $res->ultimo_mes_pago > 0)  {
                    if($res->ultimo_anio_pago == date("Y")) {
                        $ResultadoAdeudo = (date("m") - $res->ultimo_mes_pago) * $mensualidad;
                    }
                    else
                    {
                        $iMeseTranscurridos = 0;
                        $aniosTranscurridos = (date("Y") - $res->ultimo_anio_pago) + 1;
                        for ($i = 1; $i <= $aniosTranscurridos; $i++) {
                            //Año inicial
                            if ($i == 1) {
                                $iMeseTranscurridos = $iMeseTranscurridos + (12 - $res->ultimo_mes_pago);
                            }
                            else {
                                //Año final
                                if ($i== $aniosTranscurridos) {
                                    $iMeseTranscurridos = $iMeseTranscurridos + (date("m"));
                                }
                            //Años Intemedios
                                else {
                                    $iMeseTranscurridos = $iMeseTranscurridos + 12;
                                }
                            }
                        }
                        $ResultadoAdeudo = $iMeseTranscurridos * $mensualidad;
                    }
                }

                $d = array();
                $d["0"] = $res->id_cliente.( $res->pensionado === true ? " <span style='background-color: green; color: white; padding: 5px;'>(PENSIONADO)</span>" : "");  	//=id_cliente
				$d["1"] = $fullname;			//fullname;
                $d["2"] = $calle;			 	//=calle y calle_letra
                $d["3"] = $numero;			 	//=numero y numero_letra
                $d["4"] = $res->ultimo_anio_pago;//=ultimo_anio_pago
                $d["5"] = $ultimoMesPago;		//=ultimo_mes_pago
                $d["6"]  = $colonia.(!empty($res->nombre_privada) ? " (Privada/Cerrada: ".$res->nombre_privada.")" : "");			//=colonia
                $d["7"]  = $ResultadoAdeudo;	//=$Adeudi
                $d["8"]  = $cruz1;		 	    //=cruz1
                $d["9"]  = $propietario;	 	//=propietario
                $d["10"] = $res->latitud;	 	//=latitud
                $d["11"] = $res->longitud;	 	//=longitud
                $d["12"] = $res->apepat;	 	//=Apellido Paterno
                $d["13"] = $res->apemat;	 	//=Apellido Materno
                $d["14"] = $res->razon_social;	//=Empresa(razon social)
				$d["15"] = $persona;			//=persona
                $d["16"] = $mensualidad;		 //=mensualidad
                $d["17"] = $res->ultimo_mes_pago;//=$res->ultimo_mes_pago
				$d["18"] = $res->ruta;			 //=ruta de Recoleccion
				$d["19"] = $res->correo;		 //=correo
                $d["20"] = $cruz2;		         //=$cruz2
                $d["21"] = $res->cobro_manual;               //=$cruz2
                $d["22"] = $mensualidad;               //=$cruz2
                $d["23"]  = $res->comercio;	//=$comercio
                $d["24"]  = $res->idestatuscliente;	//=$idestatuscliente
                $d["25"]  = $res->estatus_cliente;	//=$idestatuscliente
                $d["26"]  = $res->sigla_estatus_cliente;	//=$idestatuscliente
                $d["27"]  = $res->isfactura;	//=$idestatuscliente
                $d["28"]  = $res->permcobro;	//=$idestatuscliente
                $d["29"]  = $res->activo && $res->vigente;	//=$idestatuscliente

                array_push($data, $d);
            }//fin:foreach($results as $res)
        }//fin:if(count($results) > 0)
        return $data;
    }//fin:filterSearch
    //-------------------------------------------------------------------------
    public function buscarHistorialAction(){

        $view = clone $this->view;
        $where = "";
        $this->view->disable();
        if ($this->request->isPost() == true) {
            $rawBody = $this->request->getJsonRawBody();
            $clave = $rawBody->clave;
            $clave = preg_replace('/\s+/', ' ',$clave);
            $where .= " hp.idcliente = ". $clave;
			
            $results= array();
            $sqlQuery = "SELECT 
					hp.id,
					hp.anio,
					hp.mes, 
					hp.cantidad, 
					hp.descuento, 
					hp.idcliente, 
					hp.fecha_pago, 
					hp.activo,
					date(hp.fecha_pago) as fechapago,
					hp.idtipo_cobro,
					hp.idtipo_servicio,
					hp.concepto_servicio,
					hp.idpago,
					COALESCE(hp.folio_factura,'') folio_factura,
					COALESCE(mp.nombre, '') as metodo_pago,
					COALESCE(hp.referencia_bancaria, '') referencia_bancaria
			FROM cliente.historial_pago hp
			left JOIN cliente.metodo_pago mp on hp.idforma_pago = mp.id 
            WHERE  " .$where. " ORDER by hp.id desc ";

            $sqlQuery = "select 
            p.idpago, 
            date(p.fecha_pago) fecha_pago, 
            p.idcliente, 
            p.folio_interno,
            u.usuario, 
            p.cantidad::money total,
            coalesce (p.cantidad - p.comision, '0')::money subtotal, 
            coalesce (p.comision, '0')::money comision, 
            case when p.payment_id is not null then 'SI' else 'NO' end paypal, 
            case when idcorte is not null then 'SI' else 'NO' end encorte, 
            p.activo, cp.id iddescuento_pensionado,
            p.impresiones,
            case when p.isnegociacion = true then 'SI' else 'NO' end isnegociacion 
            from cliente.pagos p
            left join usuario.usuario u on p.idusuario = u.id
			LEFT JOIN cliente.cliente_descuento_pensionado cp ON 
				p.idcliente = cp.id_cliente and 
				cp.idpago = p.idpago
            where p.idcliente = $clave
            order by p.fecha_pago desc";
			//ORDER BY coalesce(hp.anio,'0') DESC, coalesce(hp.mes,'0') DESC";

            $results = HistorialPago::findByQuery($sqlQuery);
			
            $data = array();
			$meses = array('enero','febrero','marzo','abril','mayo','junio','julio',
               'agosto','septiembre','octubre','noviembre','diciembre');
			$mesPago = "";
			$imes = 0;
			
            if(count($results) > 0){
                foreach($results as $res){
					$mesPago = ($res->mes != null and $res->mes != "") ? trim($res->mes) : "";				
					if($mesPago == "" || strlen(trim($res->mes)) > 2){
						$mesPago = "";
						$imes = 0;
					}//fin:
					else{
						$imes = intval($res->mes);						
						$mesPago = $meses[$imes - 1] ;
					}//fin:else									
                    array_push($data, [
                        "idpago" => $res->idpago,
						"fecha_pago" => $res->fecha_pago, //$res->mes,
                        "idcliente" => $res->idcliente,
                        "folio_interno" => $res->folio_interno,
                        "usuario" => $res->usuario,
                        "total"=>$res->total,
                        "subtotal" => $res->subtotal,
                        "comision"=> $res->comision,
                        "paypal" => $res->paypal,
                        "encorte" => $res->encorte,
						"activo" => $res->activo ? "1" :"0",
						"pensiondado" => $res->iddescuento_pensionado ? 'SI' : 'NO',
						"iddescuento_pensionado" => $res->iddescuento_pensionado,
						"impresiones" => $res->impresiones,
						"isnegociacion" => $res->isnegociacion,
                    ]);
                }//fin:foreach($results as $res)
            }//fin:if(count($results) > 0)
        }
        else{
            $view->setVar('NotImplemented', true);
        }
        $this->response->setContent(json_encode($data));
        return $this->response;
    }//fin:buscarHistorialAction
    //----------------------------------------------------------------------------
    public function obtenerTipoServicioAction(){

        $view = clone $this->view;
        $this->view->disable();
        //if ($this->request->isPost() == true){
        if ($this->request->isGet() == true){
            $results= array();
            $results = Tipo_servicio::findByActivo(true);
            $data = array();
            if(count($results) > 0){
                foreach($results as $res){
                    array_push($data, [
                        "id" => $res->id,
                        "nombre" => $res->nombre,
                        "descripcion" => $res->descripcion
                    ]);
                }//fin:foreach($results as $res)
            }//fin:if(count($results) > 0)
        }//fin:if ($this->request->isPost() == true)
        else{
            $view->setVar('NotImplemented', true);
        }
        $this->response->setContent(json_encode($data));
        return $this->response;
    }//fin:obtenerAction
    //----------------------------------------------------------------------------
    public function obtenerFormaPagoAction(){

        $view = clone $this->view;
        $this->view->disable();
        if ($this->request->isGet() == true){
            $results= array();
			$results = Forma_pago::findByActivo(true);
            $data = array();
            if(count($results) > 0){
                foreach($results as $res){
                    array_push($data, [
                        "id" => $res->id,
                        "nombre" => $res->nombre,
                        "descripcion" => $res->descripcion
                    ]);
                }//fin:foreach($results as $res)
            }//fin:if(count($results) > 0)
        }//fin:if ($this->request->isPost() == true)
        else{
            $view->setVar('NotImplemented', true);
        }
        $this->response->setContent(json_encode($data));
        return $this->response;
    }//fin:obtenerAction
    //----------------------------------------------------------------------------      
    public function validardescuentoAction(){

		//[permiso: true, message:""]
        $boContinue = true;

        $this->view->disable();

        $resp = new \stdClass();
		$resp->permiso = false;
        $resp->message = "";

        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];
        if ($this->request->isPost() == true){
            if($this->request->isAjax() == true){
                $rawBody = $this->request->getJsonRawBody();

                $usuario  = $rawBody->usuario;
                $password = $rawBody->password;
//				$recurso  = $rawBody->recurso;
//				$accion   = $rawBody->accion;
                $motivo   = $rawBody->motivo;

                // Check if the user exist
                $user = Users::findFirstByUsuario($usuario);
                if($user == false || !$user->activo){
					$resp->permiso = false;
                    $resp->message = "El Usuario no fue encontrado.";
                    $boContinue = false;
                }//fin:user

                if($boContinue){
                    if(!empty($user->clave) and !$this->security->checkHash($password, $user->clave)){
						$resp->permiso = false;
                        $resp->message = "Usuario y Contraseña incorrectos";
                        $boContinue = false;
                    }//fin:
                }//fin:

                if($boContinue){
					$profiles = Profiles::findByUser($user->id);		
					$idPerfil = 0;
					if(count($profiles) <= 0){			
						$resp->permiso = false;
						$resp->message = "El usuario no tiene asociado un perfil.";					
					}//fin:count
					else{					
						//iterar para obtener datos del Perfil					
						foreach($profiles as $rowdata){
						    if($rowdata->id == 18){
                                $idPerfil = $rowdata->id;
                                break;
                            }

						}//fin:foreach($results as $res)								
										
						//Obtener el listado de Permiso asociados al perfil
						$permisos = Permissions::findByProfile($idPerfil);				
						if(count($permisos) > 0){					
							$resp->permiso = false;
							$resp->message = "El usuario no tiene asociado el permiso consultado.";	
							foreach($permisos as $rowper){		
								if($rowper->recurso == 'caja' &&
								   $rowper->accion == 'descuento' && ((bool)$rowper->activo)){
									$resp->permiso = true;							
									$resp->message = "ok";
                                    $objMotivo = new MotioDescuento();
                                    $objMotivo->idusuario_solicito = $idUser;
                                    $objMotivo->idusuario_valido = $user->id;
                                    $objMotivo->motivo = $motivo;
                                    if(!$objMotivo->save()){
                                        $resp->permiso = false;
                                        $resp->message = "Ocurrio un error al guardar la información";
                                        foreach ($objMotivo->getMessages() as $message) {
                                            $this->logger->info("(save-motivo-descuento): " . $message);
                                        }
                                    }
                                    else{
                                        $resp->permiso = true;
                                        $resp->message = "ok";
                                        $resp->idmotivo = $objMotivo->id;
                                    }
									break;
								}//fin:if					
							}//fin:foreach($results as $res)
						}//fin:if(count($results) > 0)	
						else{					
							$resp->permiso = false;
							$resp->message = "El usuario no tiene ningun permiso asignado.";
						}//fin:else

					}//fin:else							
				}//fin:if($boContinue)				
            }//fin:if($this->request->isAjax() == true)
        }//fin:if ($this->request->isPost() == true)
        $this->response->setContent(json_encode($resp));
        return $this->response;
    }//fin:validardescuentoAction
	//----------------------------------------------------------------------------		  
    function eliminarPreposiciones($datos){
        $preposiciones = array('de', 'los', 'la', 'y');
        $newData = array();
        foreach ($datos as $valor) {
            if(!in_array($valor, $preposiciones)){
                array_push($newData,$valor);
            }
        }
        return $newData;
    }
	//----------------------------------------------------------------------------
	public function validarCorteAction(){
		
		$this->view->disable();
		
		$resp = new \stdClass();
		$resp->cancelar = false;
		$resp->message = "";
        if($this->request->isPost() == true){
            if($this->request->isAjax() == true){
				$rawBody = $this->request->getJsonRawBody();   
				
				$idhistorial  = $rawBody->idhistorial;
				
				$resulset = HistorialCorte::findByidHistorial($idhistorial);
				if(count($resulset) > 0){	
					$resp->cancelar = false;
					$resp->message = "No es posible cancelar el Pago, pertenece a un corte vigente.";
				}//fin:if
				else{
					$resp->cancelar = true;
					$resp->message = "ok";					
				}//Fin:else				
			}//fin:if($this->request->isAjax() == true)
		}//fin:if($this->request->isPost() == true)
		$this->response->setContent(json_encode($resp));
        return $this->response;	
	}//fin:validarCorteAction
	//----------------------------------------------------------------------------
	public function cancelarPagoAction($idpago = null){
		
		$this->view->disable();
		
		$resp = new \stdClass();
		$resp->success = false;
		$resp->message = "";
		
		$resp->ultanniopago = "0";
		$resp->ultmespago   = "0";
        if($this->request->isPost() == true){
            if($this->request->isAjax() == true){

				$rawBody = $this->request->getJsonRawBody();
				$idpago = $rawBody->idpago;
				$idcliente   = $rawBody->idcliente;
				$motivo   = $rawBody->motivo;

				$sqlquery = "select * from cliente.pagos where idcliente = ".$idcliente." order by fecha_pago desc limit 1";
				$rows = GenericSQL::getBySQL($sqlquery);
				if(count($rows) > 0){
				    if($rows[0]->idpago != $idpago){
                        $resp->message = "Solo se puede cancelar el ultimo pago";
                        $this->response->setContent(json_encode($resp));
                        return $this->response;
                    }
                }

				$oPago = Pagos::findFirst($idpago);
				if($oPago->idcorte){
                    $resp->message = "El pago se encuentra en un corte vigente";
                    $this->response->setContent(json_encode($resp));
                    return $this->response;
                }

                $this->db->begin();
                $dataOrigin = json_encode($oPago);
                $identity = $this->auth->getIdentity();
                $idUser = $identity["id"];
                if ($oPago) {
                    $oPago->activo = false;
                    $oPago->fecha_modificacion = date("c");
                    $oPago->fecha_cancelacion = date("c");
                    $oPago->idusuario_cancelo = $idUser;
                    $oPago->motivo_cancelacion = $motivo;
                    if ($oPago->save()) {
                        $dataB = new BitacoraCambios();
                        $dataB->identificador = $idpago;
                        $dataB->modulo = 'PAGOS';
                        $dataB->accion = 'CANCELAR PAGO';
                        $dataB->idusuario = $idUser;
                        $dataB->tabla = "cliente.pagos";
                        $dataB->cambios = json_encode($oPago);
                        $dataB->original = $dataOrigin;

                        if ($dataB->save()) {
                            $rows = GenericSQL::getBySQL("select * from cliente.historial_pago where idpago = $idpago 
                            order by anio::integer desc, mes::integer desc");
                            foreach ($rows as $r) {
                                $dp = HistorialPago::findFirst($r->id);
                                $dp->activo = false;
                                $dp->fecha_modificacion = date("c");
                                if (!$dp->save()) {
                                    foreach ($dp->getMessages() as $message) {
                                        $this->logger->info("(caja-cancelacion-pago-detalle): " . $message);
                                    }
                                    $resp->message = "Ocurrio un error al cancelar el pago";
                                    $this->db->rollback();
                                    return $this->response;
                                }
                                $ultmespago = intval($r->mes);
                                $ultanniopago = intval($r->anio);
                            }

                            $rowCount = $this->db->query("select * from cliente.historial_pago where idcliente = $idcliente and activo
                            order by anio::integer desc, mes::integer desc limit 1");
                            $rowCount = $rowCount->fetch();
                            if($rowCount){
                                $ultmespago = $rowCount["mes"];
                                $ultanniopago = $rowCount["anio"];
                            }
                            else {
                                if($ultmespago === 1){
                                    $ultmespago = 12;
                                    $ultanniopago -= 1;
                                }
                                else{
                                    $ultmespago -= 1;
                                }
                            }

                            $oCli = Clientes::findFirst($idcliente);
                            $dataOrigin = json_encode($oCli);

                            $oCli->ultimo_anio_pago = $ultanniopago;
                            $oCli->ultimo_mes_pago = $ultmespago;
                            $oCli->fecha_modificacion = date("c");
                            if ($oCli->save()) {
                                $dataB = new BitacoraCambios();
                                $dataB->identificador = $idcliente;
                                $dataB->modulo = 'CLIENTE';
                                $dataB->accion = 'ACTUALIZAR ULTIMO PAGO';
                                $dataB->idusuario = $idUser;
                                $dataB->tabla = "cliente.cliente";
                                $dataB->cambios = json_encode($oCli);
                                $dataB->original = $dataOrigin;

                                if (!$dataB->save()) {
                                    foreach ($dataB->getMessages() as $message) {
                                        $this->logger->info("(apiv2-cancelacion-pago-bitacora-cliente): " . $message);
                                    }
                                    $resp->message = "Ocurrio un error al cancelar el pago";
                                    $this->db->rollback();
                                }
                                else{
                                    $resp->success = true;
                                    $this->db->commit();
                                }
                            }
                            else {
                                foreach ($oCli->getMessages() as $message) {
                                    $this->logger->info("(apiv2-cancelacion-pago-cliente): " . $message);
                                }
                                $resp->message = "Ocurrio un error al cancelar el pago";
                                $this->db->rollback();
                            }
                        }
                        else {
                            foreach ($dataB->getMessages() as $message) {
                                $this->logger->info("(apiv2-cancelacion-pago-bitacora-pago): " . $message);
                            }
                            $resp->message = "Ocurrio un error al cancelar el pago";
                            $this->db->rollback();
                        }
                    }
                    else {
                        foreach ($oPago->getMessages() as $message) {
                            $this->logger->info("(apiv2-cancelacion-pago): " . $message);
                        }
                        $resp->message = "Ocurrio un error al cancelar el pago";
                        $this->db->rollback();
                    }
                }
                else {
                    $this->db->rollback();
                    $this->response->setStatusCode(404);
                }
			}//fin:if
		}//fin:if
		$this->response->setContent(json_encode($resp));
        return $this->response;		
	}//fin:cancelarPagoAction 
	//----------------------------------------------------------------------------
    public function enviarEmailABackction(){

        // Settings
        $this->view->disable();

        $resp = new \stdClass();
        $resp->success = false;
        $resp->message = "";

        $mailSettings = $this->config->mail;
        $transport = NULL;
        //$html = "";
        $correo = "";
        $tblcliente = "";
        $tbltotales = "";
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

        if($this->request->isPost() == true){
            if($this->request->isAjax() == true){

                $rawBody = $this->request->getJsonRawBody();

                //$html = $rawBody->html;
                $tblcliente = $rawBody->tblcliente;
                $tbltotales = $rawBody->tbltotales;
                $correo = preg_replace('/\s+/', ' ', $rawBody->correo);

                // Create the message
                $message = Message::newInstance();
                $message->setSubject("Ultimo recibo de pago");
                $message->setTo($correo);
                $message->setFrom(array($mailSettings->fromEmail => $mailSettings->fromName));
                //$message->setBody($html, 'text/html');
                $bodymsg = "Estimado Cliente atendiendo a la solicitud le hacemos llegar el archivo correspondiente al recibo de pago. ";
                $message->setBody($bodymsg, 'text/html');

                if (!$transport){
                    $transport = Smtp::newInstance(
                        $mailSettings->smtp->server,
                        $mailSettings->smtp->port,
                        $mailSettings->smtp->security
                    );
                    $transport->setUsername($mailSettings->smtp->username);
                    $transport->setPassword($mailSettings->smtp->password);
                }//fin:if
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //creacion del pdf
                $content = '<page orientation="portrait" backtop="30mm" backbottom="7mm">'.
                    '<page_header>'.
                    '<table style="width:100%;">'.
                    '<tr>'.
                    '<td style="width:20%; text-align:right;">&nbsp;'.
                    '</td> '.
                    '<td style="width:60%; text-align:center;">'.
                    '<br>'.
                    '<img src="img/logo_pamplona.png" alt="Pamplona" style="width:45%;"/>'.
                    '<br>'.
                    '<span style="font-weight: bold; font-size: 12pt; color: #0000;">Recibo de Cobro</span>'.
                    '</td>'.
                    '<td style="width:20%; text-align:right; vertical-align:bottom">'.date('d')." de ".$meses[date('n')-1]. " del ".date('Y').
                    '</td> '.
                    '</tr>'.
                    '</table>'.
                    '</page_header>'.
                    '<page_footer>'.
                    '<table style="width:100%; margin-top:30px;" cellpading="0" cellspacing="0" class="page_footer">'.
                    '<tr>'.
                    '<td style="width:30%; height:2%; text-align:center;">'.
                    '&nbsp;'.
                    '</td>'.
                    '<td style="width:50%; height:2%; text-align:Left;">'.
                    '<strong></strong>'.
                    '</td>'.
                    '<td style="width:20%; height:2%; text-align:center;">'.
                    '[[page_cu]] de [[page_nb]]'.
                    '</td>'.
                    ' </tr>'.
                    '</table>'.
                    '</page_footer>';

                $content .= '<br/>'.$tblcliente;
                $content .= '<br/>'.$tbltotales;
                $content .= '</page>';
                $html2pdf = new html2Pdf('P','A4','fr');
                $html2pdf->WriteHTML($content);
                $content_PDF = $html2pdf->Output('', true);
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                // Create the Mailer using your created Transport
                $mailer = \Swift_Mailer::newInstance($transport);
                $correoNoEnviador = array();
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //attachment pdf
                $attachment = Swift_Attachment::newInstance($content_PDF, 'recibo.pdf', 'application/pdf');
                $message->attach($attachment);
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if($mailer->send($message, $correoNoEnviador)){
                    $resp->success = true;
                    $resp->message = "ok";
                }//fin:if
                else{
                    $resp->success = false;
                    $resp->message = "ocurrio un error al enviar el correo.";
                    $this->response->setStatusCode(500, "Internal Server Error");
                }//fin:if
            }//fin:if
        }//fin:if
        $this->response->setContent(json_encode($resp));
        return $this->response;
    }//enviarEmailAction

	public function enviarEmailAction(){
        
		// Settings
        $this->view->disable();
		
		$resp = new \stdClass();
		$resp->success = false;
		$resp->message = "";
        
		$mailSettings = $this->config->mail;
		$transport = NULL;
		//$html = "";
		$correo = "";
		$tblcliente = "";		
		$tbltotales = "";
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
							
		if($this->request->isPost() == true){
            if($this->request->isAjax() == true){
				
				$rawBody = $this->request->getJsonRawBody();

                $correo = preg_replace('/\s+/', ' ', $rawBody->correo);
                $idcliente = $rawBody->idcliente;
                $idpago = $rawBody->idpago;

                $message = Message::newInstance();
                $message->setSubject("Recibo de pago");
                if ($correo) {
                    $message->setTo($correo);
                }

                $message->setFrom(array($mailSettings->fromEmail => $mailSettings->fromName));

                $bodymsg = $this->view->getPartial("layouts/recibocobranza", Util::getDataRecibo($idcliente, $idpago));
                $message->setBody($bodymsg, 'text/html');

                $html2pdf = new html2Pdf('P','LETTER','es');
                $html2pdf->WriteHTML($bodymsg);
                $content_PDF = $html2pdf->Output('', true);

                if (!$transport) {
                    $transport = Smtp::newInstance(
                        $mailSettings->smtp->server,
                        $mailSettings->smtp->port,
                        $mailSettings->smtp->security
                    );
                    $transport->setUsername($mailSettings->smtp->username);
                    $transport->setPassword($mailSettings->smtp->password);
                }
                // Create the Mailer using your created Transport
                $mailer = \Swift_Mailer::newInstance($transport);
                $correoNoEnviador = array();

                $attachment = \Swift_Attachment::newInstance($content_PDF, 'recibo.pdf', 'application/pdf');
                $message->attach($attachment);
				//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@				
				if($mailer->send($message, $correoNoEnviador)){
					$resp->success = true;
					$resp->message = "ok";
				}//fin:if
				else{
					$resp->success = false;
					$resp->message = "ocurrio un error al enviar el correo.";
					$this->response->setStatusCode(500, "Internal Server Error");
				}//fin:if
			}//fin:if
		}//fin:if
		$this->response->setContent(json_encode($resp));    
        return $this->response;
	}//enviarEmailAction
	//----------------------------------------------------------------------------
	public function savePagoServicioAction(){

        $this->view->disable();
		$idPago = 0;

        if ($this->request->isPost() == true){
            if($this->request->isAjax() == true){
                $rawBody = $this->request->getJsonRawBody();
                
				$resp = new \stdClass();
				$resp->success = false;
				
				//Generar el objeto para guardar el Pago				
				$oPago = new Pagos();
				$oPago->fecha_modificacion = date("c");
                $oPago->fecha_creacion = date("c");
				$oPago->fecha_pago = date("c");
								
				//Generar el objeto para la forma de Pago
                $oHistorialPago = new HistorialPago();
                $oHistorialPago->fecha_modificacion = date("c");
                $oHistorialPago->fecha_creacion = date("c");

                $oHistorialPago->idtipo_servicio = 0;
                $oHistorialPago->concepto_servicio = "";

                $idcliente = ($rawBody->idcliente) ? intval($rawBody->idcliente) : null;
                $annio = date('Y');
                $mes = date('m');

                $descuento = ($rawBody->descuento) ? $rawBody->descuento : 0.00;
                $iva = ($rawBody->iva) ? $rawBody->iva : 0.00;
                $cantidad = ($rawBody->cantidad) ? $rawBody->cantidad : 0.00;
                $fecha = ($rawBody->fecha_pago) ? date( 'c' , strtotime($rawBody->fecha_pago)) : null;
                $formapago = $rawBody->idforma_pago ? $rawBody->idforma_pago: 0;
                $refbancaria = $rawBody->referencia_bancaria ? $rawBody->referencia_bancaria: null;
                $idtipo_cobro = $rawBody->idtipo_cobro ? $rawBody->idtipo_cobro: 0;
                $idvisita_cliente = $rawBody->idvisita_cliente ? $rawBody->idvisita_cliente: null;
                $idbanco = $rawBody->idbanco ? $rawBody->idbanco: null;
                $idcobratario = $rawBody->idcobratario ? $rawBody->idcobratario: null;
                $folio_factura = $rawBody->folio_factura ? $rawBody->folio_factura : null;
                $facturado = $rawBody->facturado ? $rawBody->facturado : false;

                $idtipo_servicio = $rawBody->idtipo_servicio ? intval($rawBody->idtipo_servicio): 10;
                $concepto_servicio = $rawBody->concepto_servicio ? $rawBody->concepto_servicio: '';
				
                $identity = $this->auth->getIdentity();
                $idUser = $identity["id"];
				
				//Asignacion de valores para realizar el guardado del Pago
				$oPago->assign(array(						
					"idcliente" => $idcliente,
					"folio_factura" => $folio_factura,
					"referencia_bancaria" => $refbancaria,
					"idforma_pago" => $formapago,
					"activo" => TRUE																		
				));
				
				//Guardado del renglon de pago
				if($oPago->save()){					
					$idPago = $oPago->idpago;
                    $resp->success = true;
               							
					//Continuar con el guardado del registro en el Historial_pago
					$oHistorialPago->assign(array(
						"idcliente" => $idcliente,
						"mes"=> $mes,
						"anio"=> $annio,
						"descuento"=> $descuento,
						"iva"=> $iva,
						"cantidad"=> $cantidad,
						"fecha_pago"=> $fecha,
						"idforma_pago"=> $formapago,
						"referencia_bancaria" => $refbancaria,
						"validado" => TRUE,
						"idusuario" => $idUser,
						"facturado" => $facturado,
						"folio_factura" => $folio_factura,
						"idcobratario" => $idcobratario,
						"caja" => FALSE,
						"idbanco" => $idbanco,
						"activo" => TRUE,
						"envio_correo" => FALSE,
						"recibo_impreso" => FALSE,
						"incluido_corte" => FALSE,
						"fecha_corte" => null,
						"idvisita_cliente" => $idvisita_cliente,
						"idtipo_cobro" => $idtipo_cobro,
						"idtipo_servicio" => $idtipo_servicio,
						"concepto_servicio" => $concepto_servicio,
						"idpago" => $idPago
					));

					if($oHistorialPago->save()){
						$resp->success = true;
						$resp->idpago = $idPago;
						$this->response->setContent(json_encode($resp));											
					}//fin:if($oHistorialPago->save())
					else{
						$this->response->setStatusCode(500, "Internal Server Error");						
					}																									
                }//fin:if($oPago->save())
                else{
                    $this->response->setStatusCode(500, "Internal Server Error");
                }//fin:else
            }//fin:if($this->request->isAjax() == true)
        }//fin:if ($this->request->isPost() == true)
        return $this->response;
    }//fin:guardarHistServicioAction	
	//----------------------------------------------------------------------------
	public function savePagoMesesAction(){

        $this->view->disable();

        $arrayId = array();
        $bSuccess = TRUE;
		$idPago = 0;
        $resp = new \stdClass();

        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];

        if ($this->request->isPost() == true){
            if($this->request->isAjax() == true){
                $rawBody = $this->request->getJsonRawBody();
                $countItems = count($rawBody);

                $this->db->begin();
				//Generar el objeto para guardar el Pago
                $objRow = $rawBody[0];
				$isFactura = $objRow->isfactura;
				$oPago = new Pagos();
				$oPago->fecha_modificacion = date("c");
                $oPago->fecha_creacion = date("c");
				$oPago->fecha_pago = date("c");
                $oPago->idusuario = $idUser;
                $oPago->idmotivo = $objRow->idmotivo;

				
				//Obtener los datos para gaurdar el pago
				if($countItems > 0){
					$objRow = $rawBody[0];
					$folfac = $objRow->folio_factura ? $objRow->folio_factura : null;
                    $folint = $objRow->folio_interno ? $objRow->folio_interno : null;
					$referencia = $objRow->referencia_bancaria ? $objRow->referencia_bancaria: null;
					$idformapago = $objRow->idforma_pago ? $objRow->idforma_pago: 0;
					$idcliente = ($objRow->idcliente) ? intval($objRow->idcliente) : 0;
					$isfolio = $folint ? true : false;
                    $isdescan = $objRow->isdescan;
                    $tottdescan = $objRow->tottdescan;


					if($isfolio) {
                        $isFolioInterno = Pagos::findFirst([
                            "folio_interno = '$folint' and activo = true"
                        ]);

                        if ($isFolioInterno) {
                            $this->response->setContent("El folio interno ya esta en uso.");
                            $this->response->setStatusCode(409);
                            return $this->response;
                        }

                        $objSecfol = SecuenciaFolios::findFirst([
                            "activo = true",
                            "order" => "id desc"
                        ]);
                        $secfol = 1;
                        if ($objSecfol) {
                            $secfol = $objSecfol->id;
                        }

                        $objFolio = Folios::findFirst([
                            "folio = " . $folint . " and secuencia = " . $secfol . " and activo = true"
                        ]);

                        if ($objFolio) {
                            if ($objFolio->idcobratario != $idUser) {
                                $this->response->setContent("El folio interno esta asignado a otro cobratario.");
                                $this->response->setStatusCode(409);
                                return $this->response;
                            }
                        } else {
                            $this->response->setContent("El folio no existe existe en el catálogo.");
                            $this->response->setStatusCode(409);
                            return $this->response;
                        }
                    }
					else{
                        $foliador = Foliador::getNextFolio($idUser, $idcliente);
                        $folint = $foliador->folio_anio;
                    }

                    if($objRow->pdfFactura){
                        $pdfFactura = UtilFile::saveFile($objRow->pdfFactura, IMG_DIR.'/facturas/', 'pago');
                        if(!$pdfFactura){
                            $this->response->setContent("No se pudo guardar el pago.");
                            $this->logger->error("no se pudo guardar la factura del pago");
                            $this->response->setStatusCode(409);
                            return $this->response;
                        }
                        $oPago->ruta_factura = $pdfFactura;
                    }
					//Asignacion de valores para realizar el guardado del Pago
					$oPago->assign(array(						
						"idcliente" => $idcliente,
						"folio_factura" => $folfac,
						"isfacturado" => $folfac ? true: false,
						"referencia_bancaria" => $referencia,
						"idforma_pago" => $idformapago,
						"activo" => TRUE,
                        "latitud" => $objRow->latitud,
                        "longitud" => $objRow->longitud,
                        "folio_interno" => $folint,
                        "ispagoanual" => $isdescan,
                        "totpagoanual" => round($tottdescan, 2)
					));
					
					//Guardado del renglon de pago
					if($oPago->save()) {

                        if ($objRow->idmotivo) {
                            $objMotivo = MotioDescuento::findFirstById($objRow->idmotivo);
                            $objMotivo->activo = true;
                            $objMotivo->fecha_modificacion = date("c");
                            $objMotivo->idpago = $oPago->idpago;
                            if (!$objMotivo->save()) {
                                $this->db->rollback();
                                $this->response->setStatusCode(500, "Internal Server Error");
                                foreach ($objMotivo->getMessages() as $message) {
                                    $this->logger->info("(save-motivo-descuento): " . $message);
                                }
                                return $this->response;
                            }
                        }

						$idPago = $oPago->idpago;
						$resp->success = true;

						$cantidadLiquidar = 0;
						for($iIdx = 0; $iIdx < $countItems; $iIdx++){
							$rowJson = $rawBody[$iIdx];

							//Generar el objeto para la forma de pago
							$oHistorialPago = new HistorialPago();
							$oHistorialPago->fecha_modificacion = date("c");
							$oHistorialPago->fecha_creacion = date("c");

							$idcliente = ($rowJson->idcliente) ? intval($rowJson->idcliente) : null;
							$mes 	   = ($rowJson->mes) ? $rowJson->mes: null;
							$annio 	   = ($rowJson->anio) ? $rowJson->anio : null;

							$descuento = ($rowJson->descuento) ? $rowJson->descuento : 0.00;
							$iva = ($rowJson->iva) ? $rowJson->iva : 0.00;
							$cantidad = ($rowJson->cantidad) ? $rowJson->cantidad : 0.00;
							$fecha = date( 'c');
							$formapago = $rowJson->idforma_pago ? $rowJson->idforma_pago: 0;
							$refbancaria = $rowJson->referencia_bancaria ? $rowJson->referencia_bancaria: null;
							$idtipo_cobro = $rowJson->idtipo_cobro ? $rowJson->idtipo_cobro: 0;
							$idvisita_cliente = $rowJson->idvisita_cliente ? $rowJson->idvisita_cliente: null;
							$idbanco = $rowJson->idbanco ? $rowJson->idbanco: null;
							$idcobratario = $rowJson->idcobratario ? $rowJson->idcobratario: null;
							$folio_factura = $rowJson->folio_factura ? $rowJson->folio_factura : null;
							$facturado = $rowJson->facturado ? $rowJson->facturado : false;

							$cantidadLiquidar += $cantidad;

							$oHistorialPago->assign(array(
								"idcliente" => $idcliente,
								"mes"=> $mes,
								"anio"=> $annio,
								"descuento"=> $descuento,
								"iva"=> $iva,
								"cantidad"=> $cantidad,
								"fecha_pago"=> $fecha,
								"idforma_pago"=> $formapago,
								"referencia_bancaria" => $refbancaria,
								"validado" => TRUE,
								"idusuario" => $idUser,
								"facturado" => $facturado,
								"folio_factura" => $folio_factura,
								"idcobratario" => $idcobratario,
								"caja" => FALSE,
								"idbanco" => $idbanco,
								"activo" => TRUE,
								"envio_correo" => FALSE,
								"recibo_impreso" => FALSE,
								"incluido_corte" => FALSE,
								"fecha_corte" => null,
								"idvisita_cliente" => $idvisita_cliente,
								"idtipo_cobro" => $idtipo_cobro,
								"idtipo_servicio" => 0,
								"concepto_servicio" => '',
								"idpago" => $idPago
							));

							if($oHistorialPago->save()){
								$bSuccess = ($bSuccess && TRUE);
								$arrayId[$iIdx] = $oHistorialPago->id;
							}
							else{
								$bSuccess = ($bSuccess && FALSE);
								break;
							}//fin:else
						}//fin:for($iIdx = 0; $iIdx < $countItems; $iIdx++)
						
						$cantidadLiquidar = floatval($cantidadLiquidar);

                        $oPago->refresh();
						$oPago->cantidad = $cantidadLiquidar;
						if($isFactura) {
							$iva_calc = (($cantidadLiquidar) * 0.16);
							$oPago->iva = $iva_calc;
							$oPago->total_iva = $cantidadLiquidar + $iva_calc;
						}
                        if($oPago->save()){
                            $bSuccess = ($bSuccess && TRUE);
                        }
                        else{
                            foreach ($oPago->getMessages() as $message) {
                                $this->logger->error("(save-update motno pago): " . $message);
                            }
                            $bSuccess = ($bSuccess && FALSE);
                        }//fin:else

                        if($isfolio){
                            $objFolio->idpago = $oPago->idpago;
                            $objFolio->monto = $cantidadLiquidar;
                            if($objFolio->save()){
                                $bSuccess = ($bSuccess && TRUE);
                            }
                            else{
                                foreach ($objFolio->getMessages() as $message) {
                                    $this->logger->error("(save-folio): " . $message);
                                }
                                $bSuccess = ($bSuccess && FALSE);
                            }//fin:else

                            $this->logger->error("(save-idrango_folio): " . $objFolio->idrango_folio);
                            $objRangoFolio = RangoFolio::findFirstById($objFolio->idrango_folio);
                            if(!$objRangoFolio){
                                $this->response->setContent("va nulo rango");
                                $bSuccess = ($bSuccess && FALSE);
                            }
                            $objRangoFolio->porliquidar += 1;
                            $objRangoFolio->disponibles-=1;
                            $objRangoFolio->monto_porliquidar += $cantidadLiquidar;
                            if($objRangoFolio->save()){
                                $bSuccess = ($bSuccess && TRUE);
                            }
                            else{
                                foreach ($objRangoFolio->getMessages() as $message) {
                                    $this->logger->error("(save-rangofolio): " . $message);
                                }
                                $bSuccess = ($bSuccess && FALSE);
                            }//fin:else

                            $this->logger->error("(save-idinventario): " . $objRangoFolio->idinventario);
                            $objInvFolio = InventarioFolios::findFirstById($objFolio->idinventario);

                            if(!$objInvFolio){
                                $this->response->setContent("va nulo inventario ".$objRangoFolio->idinventario);
                                $this->response->setStatusCode(409);
                                return $this->response;
                            }
                            ++$objInvFolio->usados;
                            --$objInvFolio->disponibles;
                            if($objInvFolio->save()){
                                $bSuccess = ($bSuccess && TRUE);
                            }
                            else{
                                foreach ($objInvFolio->getMessages() as $message) {
                                    $this->logger->error("(save-usados): " . $message);
                                }
                                $bSuccess = ($bSuccess && FALSE);
                            }//fin:else
                        }

						$resp->success = $bSuccess;						
						$resp->idpago = $idPago;
						$resp->arrayid = $arrayId;
						$resp->folio = $folint;
						$this->response->setContent(json_encode($resp));
                        if($bSuccess) {
                            $this->db->commit();
                        }
                        else {
                            $this->db->rollback();
                            $this->response->setStatusCode(500, "Internal Server Error");
                        }
					}//fin:if($oPago->save())
					else{
                        $this->db->rollback();
                        $resp->success = false;
                        $this->response->setContent(json_encode($resp));
						$this->response->setStatusCode(500, "Internal Server Error");
					}//fin:else	
				}//fin:if($countItems > 0)
            }//fin:if($this->request->isAjax() == true)
        }//fin:if ($this->request->isPost() == true)
        return $this->response;
    }//fin:guardarHistRecolectaAction
	//----------------------------------------------------------------------------		
	public function updateUbicacionAction(){
		
		$this->view->disable();
		
        $resp = new \stdClass();
		$resp->success = false;
		$resp->message = "";
		
		//[idcliente:, latitud:, longitud:, ubicado: ]

        if($this->request->isPost() == true){
            if($this->request->isAjax() == true){
                $rawBody = $this->request->getJsonRawBody();				
				
				$idcliente = $rawBody->idcliente;
				
				$latitud = $rawBody->latitud;
				$latitud = preg_replace('/\s+/', ' ', $latitud);
								
				$longitud = $rawBody->longitud;
				$longitud = preg_replace('/\s+/', ' ', $longitud);
				
				$ubicado = ((bool)$rawBody->ubicado);
				
				$sqlUpdate = "update cliente.cliente set latitud = " . $latitud . ", "; 
				$sqlUpdate = $sqlUpdate . " longitud = " . $longitud . ", ";
				if($ubicado)
					$sqlUpdate = $sqlUpdate . " ubicado = true ";
				else
					$sqlUpdate = $sqlUpdate . " ubicado = false ";
				$sqlUpdate = $sqlUpdate . " where id_cliente = " . $idcliente;
				
				$result = Clientes::updateByQuery($sqlUpdate);
				$resp->success = ((bool)$result);
				if(!$resp->success){$resp->message = "No fue posible actualizar la ubicacion del cliente.";}							
			}//fin:if($this->request->isAjax() == true)
		}//fin:if($this->request->isPost() == true)
		$this->response->setContent(json_encode($resp));
        return $this->response;				
	}//fin:updateUbicacionAction
	//----------------------------------------------------------------------------
	public function searchRecibosAction(){

        $view = clone $this->view;  
        $this->view->disable();
        
		$arrayRecibos = array();
        //[idcliente:, fechaini, fechafin]
        if($this->request->isPost()){
            $rawBody = $this->request->getJsonRawBody();
            
            $idcliente = $rawBody->idcliente;
            $fechaini = $rawBody->fechaini;
            
			//$fechafin = strtotime ($rawBody->fechafin.' + 1 days');
			$fechafin = date('Y-m-d', strtotime($rawBody->fechafin. ' + 1 days'));		         
            $results= array();
			$result_tot= array();
			
            $sqlQuery = "select 
				p.idpago,
				p.idcliente,				
				p.folio_factura,
				p.referencia_bancaria,
				p.idforma_pago,
				p.activo,
				to_char(p.fecha_creacion, 'DD/MM/YYYY') as fecha_creacion,
				to_char(p.fecha_pago, 'DD/MM/YYYY') as fecha_pago,
				p.folio_interno,
				u.usuario cobratario 
			from cliente.pagos p 
			left join usuario.usuario u on p.idusuario = u.id
			where p.idcliente = " . $idcliente . " and p.fecha_creacion between " .
			" to_date('". $fechaini ."','YYYY-MM-DD') AND to_date('" . $fechafin . "','YYYY-MM-DD') order by p.idpago desc";
					         
            $results = Pagos::findByQuery($sqlQuery);  			
            $data = array();            
            if(count($results) > 0){
                foreach($results as $res){
					$arrayRow = array(
						"idpago" => 0,
						"fecpago" => "",
						"feccreacion"=> "",
						"subtotal"=> 0.00,
						"descto"=> 0.00,
						"total"=> 0.00,
						"activo"=> "1",
						"tipocobro"=> "0",
                        "folio" => $res->folio_interno,
                        "cobratario" => $res->cobratario
					);
					//Asignacion de los valores para el renglon de pago
					$arrayRow["idpago"] = $res->idpago;
					$arrayRow["fecpago"] = $res->fecha_pago;
					$arrayRow["feccreacion"] = $res->fecha_creacion;
					$arrayRow["activo"] = $res->activo ? "1" :"0";
					
					//Obtener el datalle para cada renglon de pago(idpago)
					$sqlQuery = "select
						sum(coalesce(hp.descuento, 0)) as descuento,	
						sum(coalesce(hp.cantidad, 0)) as total,	
						max(hp.idtipo_cobro) as idtipo_cobro
					from cliente.historial_pago hp where hp.idcliente = " . $idcliente . " and hp.idpago = " . $res->idpago;
					
					$result_tot = HistorialPago::findByQuery($sqlQuery);
					if(count($result_tot) > 0){								
						$tot_descuento = 0.00;
						$tot_total = 0.00;
						$tot_subtotal = 0.00;
						$idtipocobro = "0";						
						foreach($result_tot as $totales){
							$tot_descuento = $tot_descuento + $totales->descuento;
							$tot_total = $tot_total + $totales->total;
							$tot_subtotal = $tot_subtotal + ($totales->descuento + $totales->total);
							$idtipocobro  = $totales->idtipo_cobro;
						}//fin:foreach($results as $totales)						
						$arrayRow["subtotal"] = $tot_subtotal;
						$arrayRow["descto"] = $tot_descuento;
						$arrayRow["total"] = $tot_total;
						$arrayRow["tipocobro"] = $idtipocobro;							
					}//fin:if(count($results) > 0)					
					array_push($data, $arrayRow);																		                  
                }//fin:foreach($results as $res)
            }//fin:if(count($results) > 0)			
        }//fin:if($this->request->isPost())
        else{ $view->setVar('NotImplemented', true);}
	
        $this->response->setContent(json_encode($data));
        return $this->response;    
    }//fin:searchRecibosAction
	//----------------------------------------------------------------------------
	public function getHistorialPagosAction(){
		
		$view = clone $this->view;  
        $this->view->disable();
        if($this->request->isPost()){
            $rawBody = $this->request->getJsonRawBody();
            $idpago = $rawBody->idpago;
            $idpago = preg_replace('/\s+/', ' ',$idpago);     
			
            $results= array();
            $sqlQuery = "SELECT 
				hp.id,
				hp.anio,
				hp.mes, 
				hp.cantidad, 
				hp.descuento, 
				hp.idcliente, 
				hp.fecha_pago, 
				hp.activo,
				date(hp.fecha_pago) as fechapago,
				hp.idtipo_cobro,
				hp.idtipo_servicio,
				hp.concepto_servicio,
				hp.idpago,
				COALESCE(hp.folio_factura,'') folio_factura,
				coalesce((select nombre from cliente.metodo_pago 
					where id = hp.idforma_pago), '') as metodo_pago,  
				COALESCE(hp.referencia_bancaria, '') referencia_bancaria
			FROM cliente.historial_pago hp where idpago = " . $idpago." order by hp.anio::integer, hp.mes::integer";

            $results = HistorialPago::findByQuery($sqlQuery);
			
            $data = array();
			$meses = array('enero','febrero','marzo','abril','mayo','junio','julio',
               'agosto','septiembre','octubre','noviembre','diciembre');
			$mesPago = "";
			$imes = 0;
			
            if(count($results) > 0){
                foreach($results as $res){
					$mesPago = ($res->mes != null and $res->mes != "") ? trim($res->mes) : "";				
					if($mesPago == "" || strlen(trim($res->mes)) > 2){
						$mesPago = "";
						$imes = 0;
					}//fin:
					else{
						$imes = intval($res->mes);						
						$mesPago = $meses[$imes - 1] ;
					}//fin:else									
                    array_push($data, [
                        "anio" => $res->anio,
						"mes" =>  $mesPago, 
                        "subtotal" => $res->cantidad + $res->descuento,
                        "descuento" => $res->descuento,
                        "total" => $res->cantidad,
                        "fechapago"=>$res->fechapago,
                        "idcliente" => $res->idcliente,					
                        "idtipocobro"=> $res->idtipo_cobro,
                        "metodo_pago" => $res->metodo_pago,
                        "folio_factura" => $res->folio_factura,
                        "referencia_bancaria"=> $res->referencia_bancaria,
						"activo" => $res->activo ? "1" :"0",
						"imes"=> $imes,
						"id"=>$res->id,
						"idpago" =>$res->idpago,
						"concepto" => $res->concepto_servicio
                    ]);
                }//fin:foreach($results as $res)
            }//fin:if(count($results) > 0)
        }
        else{$view->setVar('NotImplemented', true);}
        $this->response->setContent(json_encode($data));
        return $this->response;		
	}//fin:getHistorialPagos
	//----------------------------------------------------------------------------
	public function generarpdfAction(){

		$request = $this->request;
        $response = $this->response;
		$this->view->disable();
		$rawBody = $this->request->getJsonRawBody();
				
		$tblcliente = $rawBody->tblcliente;	
		$tbltotales = $rawBody->tbltotales;
							              		
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
       		
        $content = '<page orientation="portrait" backtop="30mm" backbottom="7mm">'.
            '<page_header>'.
                  '<table style="width:100%;">'.
                    '<tr>'.           
                        '<td style="width:20%; text-align:right;">&nbsp;'.
                        '</td> '.       
                        '<td style="width:60%; text-align:center;">'.
                            '<br>'.
                            '<img src="img/logo_pamplona.png" alt="Pamplona" style="width:45%;"/>'.
                            '<br>'.
                            '<span style="font-weight: bold; font-size: 12pt; color: #0000;">Recibo de Cobro</span>'.
                        '</td>'.
                        '<td style="width:20%; text-align:right; vertical-align:bottom">'.date('d')." de ".$meses[date('n')-1]. " del ".date('Y').
                        '</td> '.
                    '</tr>'.
                '</table>'.
            '</page_header>'.
            '<page_footer>'.
                        '<table style="width:100%; margin-top:30px;" cellpading="0" cellspacing="0" class="page_footer">'.
                            '<tr>'.
                                '<td style="width:30%; height:2%; text-align:center;">'.
                                    '&nbsp;'.
                                '</td>'.
                                '<td style="width:50%; height:2%; text-align:Left;">'.
                                    '<strong></strong>'.
                                '</td>'.
                                '<td style="width:20%; height:2%; text-align:center;">'.
                                    '[[page_cu]] de [[page_nb]]'.
                                '</td>'.
                            ' </tr>'.
                        '</table>'.     
            '</page_footer>';   			
		
		$content .= '<br/>'.$tblcliente;
		$content .= '<br/>'.$tbltotales;
		$content .= '</page>';
		
        $html2pdf = new html2Pdf('P','A4','fr');
        $html2pdf->WriteHTML($content);
        $html2pdf->Output('recibo.pdf');
    }//fin:generarpdfAction
	//----------------------------------------------------------------------------
	public function getclientesByrsocialAction(){
		
		$view = clone $this->view;  
        $this->view->disable();
        if($this->request->isPost()){
            $rawBody = $this->request->getJsonRawBody();
            
			$idrazon_social = $rawBody->idrazon_social;
			$fisica			= ((bool)$rawBody->fisica);
			$results= array();
            $sqlQuery = "select 
				c.id_cliente,
				COALESCE(c.nombres, '') as nombres,
				c.fisica,
				coalesce(c.idfacturacion_tamboreo, 0) as idfacturacion_tamboreo,				
				coalesce(c.tipo_facturacion, '') as tipo_facturacion,				
				(case when coalesce(c.idcolonia, 0) > 0 
					then (select monto from cliente.colonia where id = c.idcolonia) 
					else .00 end) as monto,		
				coalesce(c.monto_tasa_fija, 0.00) as monto_tasa_fija, 
				(case when coalesce(c.idfacturacion_tamboreo, 0) >  0
					then (select monto_iva from cliente.facturacion_tamboreo 
						 where id = c.idfacturacion_tamboreo) else .000 end) as monto_iva,
				COALESCE(c.ultimo_anio_pago, 0) as ultimo_anio_pago,
				COALESCE(c.ultimo_mes_pago, '') as ultimo_mes_pago
			from cliente.cliente c where c.idrazon_social = ". $idrazon_social;							
			if($fisica)
				$sqlQuery .= " and c.fisica = true and c.activo = true order by id_cliente desc";
			else
				$sqlQuery .= " and c.fisica = false and c.activo = true order by id_cliente desc";
			
			$results = Clientes::findByQuery($sqlQuery);		
            $data = array();
			
			$ultmespago = "0";
			if(count($results) > 0){
                foreach($results as $res){
					$ultmespago = "0";
					$ultmespago = ($res->ultimo_mes_pago != null and $res->ultimo_mes_pago != "") ? trim($res->ultimo_mes_pago) : "0";
					if($ultmespago == "" || strlen($ultmespago) > 2){$ultmespago = "0";}									
					//$ultmespago = ($res->ultimo_mes_pago != "") ? $res->ultimo_mes_pago : 0;
					
                    array_push($data, [
						"id_cliente" => $res->id_cliente,
						"nombres" => $res->nombres,
						"fisica" => $res->fisica ? "true": "false",
						"tipo_facturacion"=>$res->tipo_facturacion,
						"monto"=>$res->monto,
						"monto_tasa_fija"=> $res->monto_tasa_fija,
						"monto_iva" => $res->monto_iva,
						//"monto_sin_iva" => $res->monto_sin_iva,
						"idfacturacion_tamboreo" => $res->idfacturacion_tamboreo,
						"ultimo_aniopago" => $res->ultimo_anio_pago,
						"ultimo_mespago" =>  $ultmespago						
                    ]);
                }//fin:foreach($results as $res)
            }//fin:if(count($results) > 0)		
        }//fin:if($this->request->isPost())
        else{$view->setVar('NotImplemented', true);}
        $this->response->setContent(json_encode($data));
        return $this->response;			
	}//fin:getclientesByrsocialAction
	//----------------------------------------------------------------------------
	public function savePagoMasivoAction(){

        $this->view->disable();

        $arrayId = array();
        $bSuccess = TRUE;
		$idPago = 0;
		$idpagomasivo = 0;
        $resp = new \stdClass();

        if ($this->request->isPost()){
            if($this->request->isAjax()){
                $rawBody = $this->request->getJsonRawBody();
                
				$folfac = "";
				$referencia = "";
				$idformapago = 0;
				$idrazon_social = 0;
				$countItems = count($rawBody);
															
				//Obtener los datos para gaurdar el pago
				$arrayClientes = array();				
				if($countItems > 0){				
					for($iIdx = 0; $iIdx < $countItems; $iIdx++){
						$rowjson = $rawBody[$iIdx];							
						if(empty($arrayClientes)){
							array_push($arrayClientes, $rowjson->idcliente);}
						else{
							$searchval = in_array($rowjson->idcliente, $arrayClientes);							
							if($searchval == FALSE){
								array_push($arrayClientes, $rowjson->idcliente);
							}//fin:
						}//fin:else						
					}//fin:for($iIdx = 0; $iIdx < $countItems; $iIdx++)
						
					$objRow = $rawBody[0];
					$folfac = $objRow->folio_factura ? $objRow->folio_factura : null;
					$referencia = $objRow->referencia_bancaria ? $objRow->referencia_bancaria: null;
					$idformapago = $objRow->idforma_pago ? $objRow->idforma_pago: 0;
					$idrazon_social = $objRow->idrazon_social ? $objRow->idrazon_social: 0;							
				}//fin:if($countItems > 0)
											
				//Generar el objeto para guardar el Pago Masivo
				$oPagoMasivo = new PagosMasivo();
				$oPagoMasivo->fecha_modificacion = date("c");
                $oPagoMasivo->fecha_creacion = date("c");
				$oPagoMasivo->fecha_pago = date("c");						
							
				//Asignacion de valores para el guardado masivo
				$oPagoMasivo->assign(array(												
					"folio_factura" => $folfac,
					"referencia_bancaria" => $referencia,
					"idforma_pago" => $idformapago,						
					"activo" => TRUE,
					"idrazon_social" => $idrazon_social
				));
				
				if($oPagoMasivo->save()){
					
					$idpagomasivo = $oPagoMasivo->idpagomasivo;
					//iterar los clientes para realizar el guardado
					for($iIdy = 0; $iIdy < count($arrayClientes); $iIdy++){
						$oPago = new Pagos();
						$oPago->fecha_modificacion = date("c");
						$oPago->fecha_creacion = date("c");
						$oPago->fecha_pago = date("c");
						
						//Asignacion de valores para realizar el guardado del Pago
						$oPago->assign(array(						
							"idcliente" => $arrayClientes[$iIdy],
							"folio_factura" => $folfac,
							"referencia_bancaria" => $referencia,
							"idforma_pago" => $idformapago,
							"activo" => TRUE,
							"idpagomasivo" => $idpagomasivo
						));
						
						if($oPago->save()){		
							$idPago = $oPago->idpago;
							array_push($arrayId, $idPago);
							$itotalrows = count($rawBody);
							for($iIdz = 0; $iIdz < $itotalrows; $iIdz++){
								$rowpago = $rawBody[$iIdz];
								if(intval($rowpago->idcliente) == intval($arrayClientes[$iIdy])){
																		
									//Generar el objeto para la forma de pago
									$oHistorialPago = new HistorialPago();
									$oHistorialPago->fecha_modificacion = date("c");
									$oHistorialPago->fecha_creacion = date("c");

									$idcliente = ($rowpago->idcliente) ? intval($rowpago->idcliente) : null;
									$mes 	   = ($rowpago->mes) ? $rowpago->mes: null;
									$annio 	   = ($rowpago->anio) ? $rowpago->anio : null;

									$descuento = ($rowpago->descuento) ? $rowpago->descuento : 0.00;
									$iva = ($rowpago->iva) ? $rowpago->iva : 0.00;
									$cantidad = ($rowpago->cantidad) ? $rowpago->cantidad : 0.00;
									$fecha = date( 'c');
									$formapago = $rowpago->idforma_pago ? $rowpago->idforma_pago: 0;
									$refbancaria = $rowpago->referencia_bancaria ? $rowpago->referencia_bancaria: null;
									$idtipo_cobro = $rowpago->idtipo_cobro ? $rowpago->idtipo_cobro: 0;
									$idvisita_cliente = $rowpago->idvisita_cliente ? $rowpago->idvisita_cliente: null;
									$idbanco = $rowpago->idbanco ? $rowpago->idbanco: null;
									$idcobratario = $rowpago->idcobratario ? $rowpago->idcobratario: null;
									$folio_factura = $rowpago->folio_factura ? $rowpago->folio_factura : null;
									$facturado = $rowpago->facturado ? $rowpago->facturado : false;

									$identity = $this->auth->getIdentity();
									$idUser = $identity["id"];

									$oHistorialPago->assign(array(
										"idcliente" => $idcliente,
										"mes"=> $mes,
										"anio"=> $annio,
										"descuento"=> $descuento,
										"iva"=> $iva,
										"cantidad"=> $cantidad,
										"fecha_pago"=> $fecha,
										"idforma_pago"=> $formapago,
										"referencia_bancaria" => $refbancaria,
										"validado" => TRUE,
										"idusuario" => $idUser,
										"facturado" => $facturado,
										"folio_factura" => $folio_factura,
										"idcobratario" => $idcobratario,
										"caja" => FALSE,
										"idbanco" => $idbanco,
										"activo" => TRUE,
										"envio_correo" => FALSE,
										"recibo_impreso" => FALSE,
										"incluido_corte" => FALSE,
										"fecha_corte" => null,
										"idvisita_cliente" => $idvisita_cliente,
										"idtipo_cobro" => $idtipo_cobro,
										"idtipo_servicio" => 0,
										"concepto_servicio" => '',
										"idpago" => $idPago
									));
									if($oHistorialPago->save()){
										$bSuccess = ($bSuccess && TRUE);										
									}//fin:if
									else{
										$bSuccess = ($bSuccess && FALSE);
										$this->response->setStatusCode(500, "Internal Server Error");
									}//fin:else																	
								}//fin:if(intval($rowpago->idcliente) == intval($arrayClientes[$iIdy]))							
							}//fin:for($iIdz = 0; $iIdz < $itotalrows; $iIdz++)															
						}//fin:if($oPago->save())
					}//fin:for($iIdy = 0; $iIdy < count($arrayClientes); $iIdy++)					
					$resp->success = $bSuccess;						
					$resp->idpagomasivo = $idpagomasivo;
					$resp->idpagos = $arrayId;
					$this->response->setContent(json_encode($resp));				
				}//fin:if($oPagoMasivo->save())
				else{
					$this->response->setStatusCode(500, "Internal Server Error");
				}//fin else: if($oPagoMasivo->save())				
            }//fin:if($this->request->isAjax() == true)
        }//fin:if ($this->request->isPost() == true)
        return $this->response;
    }//fin:guardarHistRecolectaAction
	//----------------------------------------------------------------------------
	public function isPagoRazonSocialAction(){
		
		$this->view->disable();
		
		$resp = new \stdClass();
		$resp->cancelar = false;
		$resp->message = "";
        if($this->request->isPost() == true){
            if($this->request->isAjax() == true){
				$rawBody = $this->request->getJsonRawBody();   				
				$idPago  = $rawBody->idPago;
				
				$sqlQuery = "select p.idpago from cliente.pagos_masivo pm join cliente.pagos p
				on pm.idpagomasivo = p.idpagomasivo where p.idpago = " . $idPago;
				
				$resulset = Pagos::findByQuery($sqlQuery);  			           		
				if(count($resulset) > 0){	
					$resp->cancelar = false;
					$resp->message = "No es posible cancelar el Pago, pertenece a un cobro por Razon Social.";
				}//fin:if
				else{
					$resp->cancelar = true;
					$resp->message = "ok";					
				}//Fin:else				
			}//fin:if($this->request->isAjax() == true)
		}//fin:if($this->request->isPost() == true)
		$this->response->setContent(json_encode($resp));
        return $this->response;				
	}//fin:isPagoRazonSocialAction
	//----------------------------------------------------------------------------
	public function searchPagosMasivosAction(){

        $view = clone $this->view;  
        $this->view->disable();
        
		$arrayRecibos = array();
        //[fechaini, fechafin]
        if($this->request->isPost()){
            $rawBody = $this->request->getJsonRawBody();
                     
            $fechaini = $rawBody->fechaini;           
			$fechafin = date('Y-m-d', strtotime($rawBody->fechafin. ' + 1 days'));		         
            $results= array();
			$result_tot= array();
			           
			$sqlquery = "select 
				pm.idpagomasivo,
				pm.activo,
				pm.fecha_creacion,
				to_char(pm.fecha_pago, 'DD/MM/YYYY') as fecha_pago,
				pm.idforma_pago,
				coalesce((select nombre 
					from cliente.metodo_pago where id = pm.idforma_pago), '') as metodo_pago, 
				pm.idrazon_social,
				pm.folio_factura,
				pm.referencia_bancaria,
				rs.rfc,
				rs.razon_social
			from cliente.pagos_masivo pm join cliente.razon_social rs
				on pm.idrazon_social = rs.id
			where pm.fecha_creacion between to_date('" . $fechaini ."', 'YYYY-MM-DD') 
				AND to_date('". $fechafin. "','YYYY-MM-DD') order by pm.fecha_creacion desc";
					         
            $results = PagosMasivo::findByQuery($sqlquery);  			
            $data = array();            
            if(count($results) > 0){
                foreach($results as $res){					
					$arrayRow = array(						
						"idpagomasivo" => 0,
						"fecpago" => "",
						"feccreacion"=> "",
						"subtotal"=> 0.00,
						"descto"=> 0.00,
						"total"=> 0.00,
						"activo"=> "1",
						"rfc" => "",
						"razon_social" => "",
						"metodopago" => "",
						"referencia" => "",
						"factura" => "",
						"idrazon_social" => 0
					);					
					//Asignacion de los valores para el renglon de pago
					$arrayRow["idpagomasivo"] = $res->idpagomasivo;
					$arrayRow["fecpago"] = $res->fecha_pago;
					$arrayRow["feccreacion"] = $res->fecha_creacion;
					$arrayRow["activo"] = $res->activo ? "1" :"0";
					$arrayRow["rfc"] = $res->rfc;
					$arrayRow["razon_social"] = $res->razon_social;
					
					$arrayRow["metodopago"] = $res->metodo_pago;
					$arrayRow["referencia"] = $res->referencia_bancaria;
					$arrayRow["factura"] = $res->folio_factura;
					$arrayRow["idrazon_social"] = $res->idrazon_social;
										
					//Obtener el detalle para los totales
					$sqlquery = "select
						coalesce(sum(coalesce(hp.descuento, 0)),0) as descuento,	
						coalesce(sum(coalesce(hp.cantidad, 0)),0) as total
					from cliente.pagos p join cliente.historial_pago hp on p.idpago = hp.idpago
					where p.idpagomasivo = ". $res->idpagomasivo;
									
					$result_tot = HistorialPago::findByQuery($sqlquery);
					if(count($result_tot) > 0){								
						$tot_descuento = 0.00;
						$tot_total = 0.00;
						$tot_subtotal = 0.00;										
						foreach($result_tot as $totales){
							$tot_descuento = $tot_descuento + $totales->descuento;
							$tot_total = $tot_total + $totales->total;
							$tot_subtotal = $tot_subtotal + ($totales->descuento + $totales->total);							
						}//fin:foreach($results as $totales)						
						$arrayRow["subtotal"] = $tot_subtotal;
						$arrayRow["descto"] = $tot_descuento;
						$arrayRow["total"] = $tot_total;											
					}//fin:if(count($results) > 0)										
					array_push($data, $arrayRow);																		                  
                }//fin:foreach($results as $res)
            }//fin:if(count($results) > 0)			
        }//fin:if($this->request->isPost())
        else{ $view->setVar('NotImplemented', true);}
	
        $this->response->setContent(json_encode($data));
        return $this->response;    
    }//fin:searchRecibosAction
	//----------------------------------------------------------------------------
	public function cancelarPagoMasivoAction(){
		
		$this->view->disable();
		
		$resp = new \stdClass();
		$resp->cancelado = FALSE;
		$resp->message = "";
		$idpagomasivo = 0;
		
		$result = FALSE;
		
		$boSuccess = TRUE;
		
        if($this->request->isPost() == true){
            if($this->request->isAjax() == true){
				$rawBody = $this->request->getJsonRawBody();   				
				
				$idpagomasivo = $rawBody->idpagomasivo;
				
				$sqlPagos = "select p.idpagomasivo, p.idpago, p.idcliente	
					from cliente.pagos p where p.idpagomasivo = " . $idpagomasivo;
				$dtsPagos = Pagos::findByQuery($sqlPagos);
				
				$countPagos = count($dtsPagos);
				//$countPagoEnCorte = 0;
				$existeCorte = FALSE;
				
				if(count($dtsPagos) > 0){
					for($iIdy = 0; $iIdy < $countPagos; $iIdy++){
						$rowpago = $dtsPagos[$iIdy];
						$sqlHistPagos = "select hp.idcliente, hp.id, hp.idpago 
						from cliente.historial_pago hp where hp.idpago = " . $rowpago->idpago;
						
						$dtsHistPago = HistorialPago::findByQuery($sqlHistPagos);					
						$countHistPago = count($dtsHistPago);
						
						for($iIdz = 0; $iIdz < $countHistPago; $iIdz++){
							//validar si el pagohistorial pertenece a algun corte
							$idhistorialp = $dtsHistPago[$iIdz]->id;
							$dtsPagoEnCorte = HistorialCorte::findByidHistorial($idhistorialp);
							$existeCorte = count($dtsPagoEnCorte) > 0 ? TRUE : FALSE;
							if($existeCorte){
								$iIdz = $countHistPago;
								$iIdy = $countPagos;
							}//fin:if
																
						}//fin:for($iIdz = 0; $iIdz < count($dtsHistPago); $iIdz++)																	
					}//fin:for
					
					if($existeCorte){
						$resp->cancelado = FALSE;
						$resp->message = "No es posible cancelar el pago. Existe un pago que pertenece  a un corte.";						
					}//fin:if($existeCorte)
					else{
						
						//Realizar la cancelacion de los registros del historico de pago
						for($iterx = 0; $iterx < $countPagos; $iterx++){						
							$rowp = $dtsPagos[$iterx];
							$sqlHistPagos = "select hp.idcliente, hp.id, hp.idpago 
							from cliente.historial_pago hp where hp.idpago = " . $rowp->idpago;
											
							$dtsHistPago = HistorialPago::findByQuery($sqlHistPagos);					
							$countHistPago = count($dtsHistPago);							
							for($itery = 0; $itery < $countHistPago; $itery++){
								$idhistorialp = $dtsHistPago[$itery]->id;
								//Actualizar la tabla historial_pago
								$sqlUpdateHistPago = "update cliente.historial_pago set activo = false where id =" . $idhistorialp;
								$result = HistorialPago::updateByQuery($sqlUpdateHistPago);				
								//$result = ((bool)$result); 				
							}//fin:for($iIdz = 0; $iIdz < count($dtsHistPago); $iIdz++)
								
							$sqlqueryUlt = "select hp.id, hp.anio, hp.mes, hp.idtipo_cobro, hp.activo 
								from cliente.historial_pago hp where hp.idcliente = " . 
								$rowp->idcliente ." and idtipo_cobro = 1 and activo = true ORDER BY hp.id desc, hp.fecha_creacion desc ";
										
							$resultset = HistorialPago::findByQuery($sqlqueryUlt);
							$countItems = count($resultset);
							//$iteracion = 0;
							$ultanniopago = "0";
							$ultmespago = "0";
							if($countItems > 0){								
								$rowHist = ($resultset[0]);
								$ultanniopago = $rowHist->anio;
								$ultmespago = $rowHist->mes;							
							}//fin:if
							else{
								$ultanniopago = "0";
								$ultmespago = "0";
							}//fin:else
														
							//Actualizar las columnas Ultimo annio - mes de pago
							$sqlUpdateCte = "update cliente.cliente set ultimo_mes_pago = '". $ultmespago
							. "', ultimo_anio_pago = " . $ultanniopago . " where id_cliente = ". $rowp->idcliente;
							$result = Clientes::updateByQuery($sqlUpdateCte);
							
							if($result){
								//Cancelar el registro en cliente.pagos
								//update cliente.pagos set activo = FALSE where idpago = 0;
								$sqlUpdatePagos = "update cliente.pagos set activo = FALSE where idpago = " . $rowp->idpago;
								$result = Pagos::updateByQuery($sqlUpdatePagos);
								
								$boSuccess = ($boSuccess && TRUE);
							}//fin:if
							else{ $boSuccess = ($boSuccess && FALSE);}							
						}//fin:for($iterx = 0; $iterx < $countPagos; $iterx++)
						
						//Cancelar el folio de pago masivo						
						if($boSuccess){
							//update cliente.pagos_masivo set activo = FALSE where idpagomasivo = ;
							$sqlUpdatePagoMasivo = "update cliente.pagos_masivo set activo = FALSE where idpagomasivo =" . $idpagomasivo;
							$result = PagosMasivo::updateByQuery($sqlUpdatePagoMasivo);										
							$resp->cancelado = $result;
							$resp->message = "";
						}//fin:if
						else{
							$resp->cancelado = FALSE;
							$resp->message = "Internal Server Error";
						}//fin:										
					}//fin:else				
				}//fin:if(count($dtsPagos) > 0)				
			}//fin:if($this->request->isAjax() == true)
		}//fin:if($this->request->isPost() == true)
		$this->response->setContent(json_encode($resp));
        return $this->response;						
	}//fin:cancelarPagoMasivoAction
	//----------------------------------------------------------------------------
	public function getinfoReciboMasivoAction(){
		
		$this->view->disable();
		
		$resp = new \stdClass();
		
		//$resp->cancelado = FALSE;
		//$resp->message = "";
		$resp->idrazonsocial = 0;
		$resp->razonsocial = "";
		$resp->direccion = "";
		$resp->colonia = "";
		$resp->total = 0.00;
		$resp->fechapago = "";
		$resp->correo = "";
		$resp->clientes = array();
		
		$idpagomasivo = 0;
		$grantotal = 0.00;
		
        if($this->request->isPost() == true){
            if($this->request->isAjax() == true){
				$rawBody = $this->request->getJsonRawBody();   				
				
				$idpagomasivo = $rawBody->idpagomasivo;
				$idrazonsocial = $rawBody->idrazonsocial;
									
				//Obtener la fecha de pago del folio masivo
				//date(fecha_pago) as fechapago
				$sqlPagoMasivo = "select date(fecha_pago) as fechapago 
				from cliente.pagos_masivo idpagomasivo where idpagomasivo = " . $idpagomasivo; 
				
				$dtsPagosMasivos = PagosMasivo::findByQuery($sqlPagoMasivo);
				if(count($dtsPagosMasivos) > 0){
					$rowpm = $dtsPagosMasivos[0];
					$resp->fechapago =  $rowpm->fechapago;
				}
				else{ $resp->fechapago = date('Y-m-d');}
												
				//Obtener informacion de la razon social
				$sqlrsocial = "select
					rs.activo,
					rs.calle,
					rs.calle_letra,
					rs.calle_tipo,
					rs.codigo_contpaqi,
					rs.correo,
					rs.cp,
					rs.cruz1,
					rs.cruz1_letra,
					rs.cruz1_tipo,
					rs.cruz2,
					rs.cruz2_letra,
					rs.cruz2_tipo,
					rs.fecha_creacion,
					rs.fecha_moddificacion,
					rs.fisica,
					rs.id,
					rs.idcolonia,
					rs.idusuario,
					rs.nombre_comercial,
					rs.numero,
					rs.numero_letra,
					rs.numero_tipo,
					rs.razon_social,
					rs.rfc,
					rs.telefono,
					(case when coalesce(rs.idcolonia, 0) > 0 
						then (select nombre  from cliente.colonia where id = rs.idcolonia) 
						else '' end) as colonia
				from cliente.razon_social rs where id = " . $idrazonsocial;
				
				$dtsRazonSocial = RazonSocial::findByQuery($sqlrsocial);
				$count_rsocial = count($dtsRazonSocial);
				if($count_rsocial > 0){
					$rowrsocial = $dtsRazonSocial[0];
										
					$calle = ($rowrsocial->calle != null and $rowrsocial->calle != "") ? trim($rowrsocial->calle) : "";
					$calleLetra = ($rowrsocial->calle_letra != null and $rowrsocial->calle_letra != "") ? " ".trim($rowrsocial->calle_letra) : "";
					$calle = $calle.$calleLetra;

					$numero = ($rowrsocial->numero != null and $rowrsocial->numero != "") ? trim($rowrsocial->numero) : "";
					$numeroLetra = ($rowrsocial->numero_letra != null and $rowrsocial->numero_letra != "") ? " ".trim($rowrsocial->numero_letra) : "";
					$numero = $numero.$numeroLetra;

					$cruz1 = ($rowrsocial->cruz1 != null and $rowrsocial->cruz1 != "") ? trim($rowrsocial->cruz1) : "";
					$cruz1Letra = ($rowrsocial->cruz1_letra != null and $rowrsocial->cruz1_letra != "") ? trim($rowrsocial->cruz1_letra) : "";
					$cruz1 = $cruz1.$cruz1Letra;

					$cruz2 = ($rowrsocial->cruz2 != null and $rowrsocial->cruz2 != "") ? trim($rowrsocial->cruz2) : "";
					$cruz2Letra = ($rowrsocial->cruz2_letra != null and $rowrsocial->cruz2_letra != "") ? trim($rowrsocial->cruz2_letra) : "";
					$cruz2 = $cruz2.$cruz2Letra;
					
					$direccion = "c. " . $calle . " #". $numero . " ". $cruz1 . " ". $cruz2;
					
					$resp->idrazonsocial = $rowrsocial->id;
					$resp->razonsocial = $rowrsocial->razon_social;
					$resp->direccion = $direccion;
					$resp->colonia = $rowrsocial->colonia;
					$resp->correo = $rowrsocial->correo;
																
					//obtener todos los clientes por el idpagomasivo
					$sqlPagos = "select 
						p.idpagomasivo, 
						p.idpago, 
						p.idcliente,
						coalesce(c.nombres, '') as nombres
					from cliente.pagos p join cliente.cliente c
					on p.idcliente = c.id_cliente
					where p.idpagomasivo = " . $idpagomasivo;
					
					$dtsPagos = Pagos::findByQuery($sqlPagos);				
					$countPagos = count($dtsPagos);
											
					if($countPagos > 0){
						for($iIdy = 0; $iIdy < $countPagos; $iIdy++){
							$rowpago = $dtsPagos[$iIdy];
														
							$arrayitemCliente = array(
							"idcliente" => $rowpago->idcliente,
							"nombre" => $rowpago->nombres,
							"total" => 0,
							"meses"=> array());
																	
							$sqlHistPagos = "select hp.idcliente, hp.id, 
							hp.idpago, hp.mes, hp.anio, hp.cantidad  
							from cliente.historial_pago hp where hp.idpago = " . $rowpago->idpago;
							
							$dtsHistPago = HistorialPago::findByQuery($sqlHistPagos);					
							$countHistPago = count($dtsHistPago);															
							$totalpagos = 0.00;
							$arrayMeses = array();
							for($itery = 0; $itery < $countHistPago; $itery++){
								$rowhistpagos = $dtsHistPago[$itery];
								$totalpagos = $totalpagos + floatval($rowhistpagos->cantidad);
								array_push($arrayMeses, 
									array("annio" => $rowhistpagos->anio, 
										  "imes"  => $rowhistpagos->mes, 
										  "total" => $rowhistpagos->cantidad
									)
								);
							}//fin:for
							$grantotal = $grantotal + $totalpagos; 
							array_push($arrayitemCliente['meses'], $arrayMeses);
							$arrayitemCliente['total'] = $totalpagos;
							$resp->total = $grantotal;
							array_push($resp->clientes, $arrayitemCliente);
						}//fin:if($countPagos > 0)			
					}//fin:if($countPagos > 0)
				}//if($count_rsocial > 0)			
			}//fin:if($this->request->isAjax() == true)
		}//fin:if($this->request->isPost() == true)
		$this->response->setContent(json_encode($resp));
        return $this->response;					
	}//fin:getinfoReciboMasivoAction
	//----------------------------------------------------------------------------
	public function enviarEmailPagoMasivoAction(){
        
		// Settings
        $this->view->disable();
		
		$resp = new \stdClass();
		$resp->success = false;
		$resp->message = "";
        
		$mailSettings = $this->config->mail;
		$transport = NULL;
		$correo = "";
		$tblcliente = "";		
		$tbltotales = "";
		$idpagomasivo = 0;
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		
		$arrayClientes = array();
		
		if($this->request->isPost()){
            if($this->request->isAjax()){
				
				$rawBody = $this->request->getJsonRawBody();
				
				$tblcliente = $rawBody->tblcliente;
				//$tblempresas = $rawBody->tblempresas;
				$tbltotales = $rawBody->tbltotales;				
				$correo = preg_replace('/\s+/', ' ', $rawBody->correo);
				$idpagomasivo = $rawBody->idpagomasivo;
				
				// Create the message
				$message = Message::newInstance();
				$message->setSubject("Ultimo recibo de pago cobro");
				$message->setTo($correo);
				$message->setFrom(array($mailSettings->fromEmail => $mailSettings->fromName));			
				$bodymsg = "Estimado Cliente atendiendo a la solicitud le hacemos llegar el archivo correspondiente al recibo de pago. ";
				$message->setBody($bodymsg, 'text/html');
				
				if (!$transport){
					$transport = Smtp::newInstance(
						$mailSettings->smtp->server,
						$mailSettings->smtp->port,
						$mailSettings->smtp->security
					);
					$transport->setUsername($mailSettings->smtp->username);
					$transport->setPassword($mailSettings->smtp->password);		
				}//fin:if				
				//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@				
				//obtener todos los clientes por el idpagomasivo
				$sqlPagos = "select 
					p.idpagomasivo, 
					p.idpago, 
					p.idcliente,
					coalesce(c.nombres, '') as nombres
				from cliente.pagos p join cliente.cliente c
				on p.idcliente = c.id_cliente
				where p.idpagomasivo = " . $idpagomasivo;
							
				$dtsPagos = Pagos::findByQuery($sqlPagos);				
				$countPagos = count($dtsPagos);
													
				if($countPagos > 0){
					for($iIdy = 0; $iIdy < $countPagos; $iIdy++){
						$rowpago = $dtsPagos[$iIdy];
													
						$arrayitemCliente = array(
						"idcliente" => $rowpago->idcliente,
						"nombre" => $rowpago->nombres,
						"total" => 0,
						"meses"=> array());
																
						$sqlHistPagos = "select hp.idcliente, hp.id, 
						hp.idpago, hp.mes, hp.anio, hp.cantidad  
						from cliente.historial_pago hp where hp.idpago = " . $rowpago->idpago;
						
						$dtsHistPago = HistorialPago::findByQuery($sqlHistPagos);					
						$countHistPago = count($dtsHistPago);															
						$totalpagos = 0.00;
						$arrayMeses = array();
						for($itery = 0; $itery < $countHistPago; $itery++){
							$rowhistpagos = $dtsHistPago[$itery];
							$totalpagos = $totalpagos + floatval($rowhistpagos->cantidad);				
							array_push($arrayitemCliente['meses'], array(
								"annio" => $rowhistpagos->anio, 
								"imes"  => $rowhistpagos->mes, 
								"total" => $rowhistpagos->cantidad)
							);
						}//fin:for	
						$arrayitemCliente['total'] = $totalpagos;
						array_push($arrayClientes, $arrayitemCliente);
					}//fin:for($iIdy = 0; $iIdy < $countPagos; $iIdy++)
				}//fin:if($countPagos > 0)
				//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@							
				//creacion del pdf				
				$content = '<page orientation="portrait" backtop="30mm" backbottom="7mm">'.
				'<page_header>'.
					  '<table style="width:100%;">'.
						'<tr>'.           
							'<td style="width:20%; text-align:right;">&nbsp;'.
							'</td> '.       
							'<td style="width:60%; text-align:center;">'.
								'<br>'.
								'<img src="img/logo_pamplona.png" alt="Pamplona" style="width:45%;"/>'.
								'<br>'.
								'<span style="font-weight: bold; font-size: 12pt; color: #0000;">Recibo de Cobro por Razon Social</span>'.
							'</td>'.
							'<td style="width:20%; text-align:right; vertical-align:bottom">'.date('d')." de ".$meses[date('n')-1]. " del ".date('Y').
							'</td> '.
						'</tr>'.
					'</table>'.
				'</page_header>'.
				'<page_footer>'.
							'<table style="width:100%; margin-top:30px;" cellpading="0" cellspacing="0" class="page_footer">'.
								'<tr>'.
									'<td style="width:30%; height:2%; text-align:center;">'.
										'&nbsp;'.
									'</td>'.
									'<td style="width:50%; height:2%; text-align:Left;">'.
										'<strong></strong>'.
									'</td>'.
									'<td style="width:20%; height:2%; text-align:center;">'.
										'[[page_cu]] de [[page_nb]]'.
									'</td>'.
								' </tr>'.
							'</table>'.     
				'</page_footer>';   
				//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
				$meses_eval = "";
				$max_mes_line = 0;	
				if(count($arrayClientes)){
					//creacion de tabla con un contenido iterativo
					$table_deta = "
						<table border='1' align='center' cellspacing='0' 
								style='font-family:Arial,Helvetica;font-size: 12px;width:360pt; 
								border:1px black; padding: 0; margin: 0;' align='center'>
							<tr style='border-bottom: 1px solid;'>
								<th style='border-left: 1px solid; width: 30%;text-align:center;'>Empresa</th>
								<th style='border-left: 1px solid; width: 50%;text-align:center;'>Meses</th>
								<th style='border-left: 1px solid; width: 20%;text-align:center;'>Total</th>			
							</tr>";		
					$row_cliente = "";
					$imes = 0;
					for($idx = 0; $idx < count($arrayClientes); $idx++){
						$item_cte  = $arrayClientes[$idx];				
						$meses_eval = "";				
						for($idy = 0; $idy < count($item_cte['meses']) ; $idy++){
							$item_mes = $item_cte['meses'][$idy];						
							$imes = intval($item_mes['imes']);
							if($idy > 0){ $meses_eval .= ", ";}//fin:if
							if($max_mes_line > 5){
								$meses_eval .= "<br/>";		
								$max_mes_line = 0;
							}//fin:if
							$meses_eval .= $meses[$imes - 1];
							$max_mes_line++;
						}//fin:for		
						$row_cliente = "<tr><td style='border-left: 1px solid;'>" . $item_cte['nombre'] . "</td>";
						$row_cliente .= "<td style='border-left: 1px solid; text-align: center;'>";
						$row_cliente .= $meses_eval;
						$row_cliente .= "</td>";
						$row_cliente .= " <td style='border-left: 1px solid; text-align: right;'>$ " . $item_cte['total'] . "</td></tr>";		
						$table_deta .= $row_cliente;
					}//fin:for($idx = 0; $idx < count($arrayClientes); $idx++)
					$table_deta .= "</table>";
				}//fin:if(count($arrayClientes))
				//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@							
				$content .= '<br/>'.$tblcliente;
				//$content .= '<br/>'.$tblempresas;
				$content .= '<br/>'.$table_deta;
				$content .= '<br/>'.$tbltotales;
				$content .= '</page>';							
				$html2pdf = new html2Pdf('P','A4','fr');
				$html2pdf->WriteHTML($content);
				$content_PDF = $html2pdf->Output('', true);				
				//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
				// Create the Mailer using your created Transport
				$mailer = \Swift_Mailer::newInstance($transport);
				$correoNoEnviador = array();							
				//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
				//attachment pdf
				$attachment = Swift_Attachment::newInstance($content_PDF, 'recibo_razon_social.pdf', 'application/pdf');				
				$message->attach($attachment);
				//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@				
				if($mailer->send($message, $correoNoEnviador)){
					$resp->success = true;
					$resp->message = "ok";
				}//fin:if
				else{
					$resp->success = false;
					$resp->message = "ocurrio un error al enviar el correo.";
					$this->response->setStatusCode(500, "Internal Server Error");
				}//fin:if
			}//fin:if
		}//fin:if
		$this->response->setContent(json_encode($resp));    
        return $this->response;
	}//enviarEmailAction
	//----------------------------------------------------------------------------
	public function pdfPagoMasivoAction(){

		$request = $this->request;
        $response = $this->response;
		$this->view->disable();
		$rawBody = $this->request->getJsonRawBody();
				
		$tblcliente = $rawBody->tblcliente;
		$tbltotales = $rawBody->tbltotales;
		$idpagomasivo = $rawBody->idpagomasivo;
							              		
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");		
		$arrayClientes = array();
		//*********************************************************************
		//obtener todos los clientes por el idpagomasivo
		$sqlPagos = "select 
			p.idpagomasivo, 
			p.idpago, 
			p.idcliente,
			coalesce(c.nombres, '') as nombres
		from cliente.pagos p join cliente.cliente c
		on p.idcliente = c.id_cliente
		where p.idpagomasivo = " . $idpagomasivo;
					
		$dtsPagos = Pagos::findByQuery($sqlPagos);				
		$countPagos = count($dtsPagos);
											
		if($countPagos > 0){
			for($iIdy = 0; $iIdy < $countPagos; $iIdy++){
				$rowpago = $dtsPagos[$iIdy];
											
				$arrayitemCliente = array(
				"idcliente" => $rowpago->idcliente,
				"nombre" => $rowpago->nombres,
				"total" => 0,
				"meses"=> array());
														
				$sqlHistPagos = "select hp.idcliente, hp.id, 
				hp.idpago, hp.mes, hp.anio, hp.cantidad  
				from cliente.historial_pago hp where hp.idpago = " . $rowpago->idpago;
				
				$dtsHistPago = HistorialPago::findByQuery($sqlHistPagos);					
				$countHistPago = count($dtsHistPago);															
				$totalpagos = 0.00;
				$arrayMeses = array();
				for($itery = 0; $itery < $countHistPago; $itery++){
					$rowhistpagos = $dtsHistPago[$itery];
					$totalpagos = $totalpagos + floatval($rowhistpagos->cantidad);				
					array_push($arrayitemCliente['meses'], array("annio" => $rowhistpagos->anio, "imes"  => $rowhistpagos->mes, "total" => $rowhistpagos->cantidad));
				}//fin:for	
				$arrayitemCliente['total'] = $totalpagos;
				array_push($arrayClientes, $arrayitemCliente);
			}//fin:for($iIdy = 0; $iIdy < $countPagos; $iIdy++)
		}//fin:if($countPagos > 0)
		//*********************************************************************
        $content = '<page orientation="portrait" backtop="30mm" backbottom="7mm">'.
            '<page_header>'.
                  '<table style="width:100%;">'.
                    '<tr>'.           
                        '<td style="width:20%; text-align:right;">&nbsp;'.
                        '</td> '.       
                        '<td style="width:60%; text-align:center;">'.
                            '<br>'.
                            '<img src="img/logo_pamplona.png" alt="Pamplona" style="width:45%;"/>'.
                            '<br>'.
                            '<span style="font-weight: bold; font-size: 12pt; color: #0000;">Recibo de Cobro</span>'.
                        '</td>'.
                        '<td style="width:20%; text-align:right; vertical-align:bottom">'.date('d')." de ".$meses[date('n')-1]. " del ".date('Y').
                        '</td> '.
                    '</tr>'.
                '</table>'.
            '</page_header>'.
            '<page_footer>'.
                        '<table style="width:100%; margin-top:30px;" cellpading="0" cellspacing="0" class="page_footer">'.
                            '<tr>'.
                                '<td style="width:30%; height:2%; text-align:center;">'.
                                    '&nbsp;'.
                                '</td>'.
                                '<td style="width:50%; height:2%; text-align:Left;">'.
                                    '<strong></strong>'.
                                '</td>'.
                                '<td style="width:20%; height:2%; text-align:center;">'.
                                    '[[page_cu]] de [[page_nb]]'.
                                '</td>'.
                            ' </tr>'.
                        '</table>'.     
            '</page_footer>';
		
		$meses_eval = "";
		$max_mes_line = 0;	
		if(count($arrayClientes)){
			//creacion de tabla con un contenido iterativo
			$table_deta = "
				<table border='1' align='center' cellspacing='0' 
						style='font-family:Arial,Helvetica;font-size: 12px;width:360pt; 
						border:1px black; padding: 0; margin: 0;' align='center'>
					<tr style='border-bottom: 1px solid;'>
						<th style='border-left: 1px solid; width: 30%;text-align:center;'>Empresa</th>
						<th style='border-left: 1px solid; width: 50%;text-align:center;'>Meses</th>
						<th style='border-left: 1px solid; width: 20%;text-align:center;'>Total</th>			
					</tr>";		
			$row_cliente = "";
			$imes = 0;
			for($idx = 0; $idx < count($arrayClientes); $idx++){
				$item_cte  = $arrayClientes[$idx];				
				$meses_eval = "";				
				for($idy = 0; $idy < count($item_cte['meses']) ; $idy++){
					$item_mes = $item_cte['meses'][$idy];						
					$imes = intval($item_mes['imes']);
					if($idy > 0){ $meses_eval .= ", ";}//fin:if
					if($max_mes_line > 5){
						$meses_eval .= "<br/>";		
						$max_mes_line = 0;
					}//fin:if
					$meses_eval .= $meses[$imes - 1];
					$max_mes_line++;
				}//fin:for		
				$row_cliente = "<tr><td style='border-left: 1px solid;'>" . $item_cte['nombre'] . "</td>";
				$row_cliente .= "<td style='border-left: 1px solid; text-align: center;'>";
				$row_cliente .= $meses_eval;
				$row_cliente .= "</td>";
				$row_cliente .= " <td style='border-left: 1px solid; text-align: right;'>$ " . $item_cte['total'] . "</td></tr>";		
				$table_deta .= $row_cliente;
			}//fin:for($idx = 0; $idx < count($arrayClientes); $idx++)
			$table_deta .= "</table>";
		
			$content .= '<br/>'.$tblcliente;		
			$content .= '<br/>'.$table_deta;
			$content .= '<br/>'.$tbltotales;
			$content .= '</page>';			
						
			$html2pdf = new html2Pdf('P','A4','fr');					
			$html2pdf->WriteHTML($content);
			$html2pdf->Output('recibo_Razon_Social.pdf');
		}//fin:if(count($arrayClientes))		
    }//fin:pdfPagoMasivoAction
    //----------------------------------------------------------------------------

    public function nextfolioAction(){
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];

        $objSecfol = SecuenciaFolios::findFirst([
            "activo = true",
            "order" => "id desc"
        ]);

        //TODO considerar que sucede cuando se reinicie y queden folio de la secuencia anterior
        $secfol = 1;
        if($objSecfol){
            $secfol = $objSecfol->id;
        }
        $objNextFolio = Folios::findFirst([
            "idcobratario = ".$idUser." and activo = true and cancelado = false and idpago is null and secuencia = ".$secfol,
            "order" => "folio asc"
        ]);
        $nextFolio = 0;
        if($objNextFolio){
            $nextFolio = $objNextFolio->folio;
        }

        $this->response->setContent($nextFolio);
        return $this->response;
    }

    public function aplicardescuentoAction(){
        $this->view->disable();

        $arrayId = array();
        $bSuccess = TRUE;
        $idPago = 0;
        $resp = new \stdClass();

        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];

        if ($this->request->isPost() == true){
            if($this->request->isAjax() == true){
                $rawBody = $this->request->getJsonRawBody();
                $countItems = count($rawBody);

                $this->db->begin();
                //Generar el objeto para guardar el Pago
                $objRow = $rawBody[0];
                $oPago = new ClienteDescuento();
                $oPago->idusuario = $idUser;
                $oPago->idmotivo_descuento = $objRow->idmotivo;
                $oPago->activo = true;
                $oPago->fecha_creacion = date("c");
                $oPago->fecha_modificacion = date("c");


                //Obtener los datos para gaurdar el pago
                if($countItems > 0){
                    $objRow = $rawBody[0];
                    $idcliente = ($objRow->idcliente) ? intval($objRow->idcliente) : 0;

                    //Asignacion de valores para realizar el guardado del Pago
                    $oPago->idcliente = $idcliente;

                    //Guardado del renglon de pago
                    if($oPago->save()) {
                        $resp->success = true;

                        $cantidadLiquidar = 0;
                        for($iIdx = 0; $iIdx < $countItems; $iIdx++){
                            $rowJson = $rawBody[$iIdx];

                            //Generar el objeto para la forma de pago
                            $oHistorialPago = new ClienteDescuentoDetalle();

                            $idcliente = ($rowJson->idcliente) ? intval($rowJson->idcliente) : null;
                            $mes 	   = ($rowJson->mes) ? $rowJson->mes: null;
                            $annio 	   = ($rowJson->anio) ? $rowJson->anio : null;

//                            $cantidad = ($rowJson->cantidad) ? $rowJson->cantidad : 0.00;
                            $cantidad = ($rowJson->descuento) ? $rowJson->descuento : 0.00;

                            $cantidadLiquidar += $cantidad;

                            $oHistorialPago->assign(array(
                                "idcliente" => $idcliente,
                                "idcliente_descuento" => $oPago->id,
                                "mes"=> $mes,
                                "anio"=> $annio,
                                "cantidad"=> $cantidad,
                                "idusuario" => $idUser
                            ));

                            if($oHistorialPago->save()){
                                $bSuccess = ($bSuccess && TRUE);
                                $arrayId[$iIdx] = $oHistorialPago->id;
                            }
                            else{
                                $bSuccess = ($bSuccess && FALSE);
                                foreach ($oHistorialPago->getMessages() as $message) {
                                    $this->logger->error("(save-desceunto-detalle): " . $message);
                                }
                                break;
                            }//fin:else
                        }//fin:for($iIdx = 0; $iIdx < $countItems; $iIdx++)

                        $oPago->cantidad = $cantidadLiquidar;
                        $oPago->meses_descuento = $countItems;
                        if($oPago->save()){
                            $bSuccess = ($bSuccess && TRUE);
                        }
                        else{
                            foreach ($oPago->getMessages() as $message) {
                                $this->logger->error("(save-folio): " . $message);
                            }
                            $bSuccess = ($bSuccess && FALSE);
                        }//fin:else

                        $objCliente = Clientes::findFirstById_cliente($idcliente);
                        $objCliente->ultimo_anio_pago = $annio;
                        $objCliente->ultimo_mes_pago = $mes;
                        if($objCliente->save()){
                            $bSuccess = ($bSuccess && TRUE);
                        }
                        else{
                            foreach ($objCliente->getMessages() as $message) {
                                $this->logger->error("(save-descuento-update-mes-anio): " . $message);
                            }
                            $bSuccess = ($bSuccess && FALSE);
                        }//fin:else

                        $resp->success = $bSuccess;
                        $this->response->setContent(json_encode($resp));
                        if($bSuccess) {
                            $this->db->commit();
                        }
                        else {
                            $this->db->rollback();
                            $this->response->setStatusCode(500, "Internal Server Error");
                        }
                    }//fin:if($oPago->save())
                    else{
                        $this->db->rollback();
                        $resp->success = false;
                        $this->response->setContent(json_encode($resp));
                        $this->response->setStatusCode(500, "Internal Server Error");
                    }//fin:else
                }//fin:if($countItems > 0)
            }//fin:if($this->request->isAjax() == true)
        }//fin:if ($this->request->isPost() == true)
        return $this->response;
    }

    public function getDatosPagoAction($idpago){
        $rawBody = $this->request->getJsonRawBody();
        $clave = $rawBody->clave;
        $clave = preg_replace('/\s+/', ' ',$clave);

        $results= array();
        $sqlQuery = "SELECT 
					hp.id,
					hp.anio,
					hp.mes, 
					hp.cantidad, 
					hp.descuento, 
					hp.idcliente, 
					hp.fecha_pago, 
					hp.activo,
					date(hp.fecha_pago) as fechapago,
					hp.idtipo_cobro,
					hp.idtipo_servicio,
					hp.concepto_servicio,
					hp.idpago,
					COALESCE(hp.folio_factura,'') folio_factura,
					COALESCE(hp.referencia_bancaria, '') referencia_bancaria
			FROM cliente.historial_pago hp
            WHERE idpago = $idpago and activo ORDER by anio desc, mes::integer desc ";

        $results = HistorialPago::findByQuery($sqlQuery);

        $data = array();
        $meses = array('enero','febrero','marzo','abril','mayo','junio','julio',
            'agosto','septiembre','octubre','noviembre','diciembre');
        $mesPago = "";
        $imes = 0;

        if(count($results) > 0){
            foreach($results as $res){
                $mesPago = ($res->mes != null and $res->mes != "") ? trim($res->mes) : "";
                if($mesPago == "" || strlen(trim($res->mes)) > 2){
                    $mesPago = "";
                    $imes = 0;
                }//fin:
                else{
                    $imes = intval($res->mes);
                    $mesPago = $meses[$imes - 1] ;
                }//fin:else
                array_push($data, [
                    "anio" => $res->anio,
                    "mes" => $mesPago, //$res->mes,
                    "descto" => $res->descuento,
                    "total" => $res->cantidad,
                ]);
            }//fin:foreach($results as $res)
        }//fin:if(count($results) > 0)
        $this->response->setContent(json_encode(array(
            "data" => $data
        )));
        return $this->response;
    }
}
?>