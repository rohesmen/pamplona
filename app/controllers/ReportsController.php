<?php
namespace Vokuro\Controllers;
use Vokuro\Mail\Exception;
use Vokuro\Models\Reports;
use Phalcon\Mvc\View;
use Vokuro\Models\LocalDistricts;
use Vokuro\Models\Polygons;

/**
 * Display the default index page.
 */
class ReportsController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateBefore('public');
    }

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function indexAction()
    {
        $this->view->setVar('reports', Reports::findByActivo(true));
    }
    public function getAction($id)
    {

        $view = clone $this->view;
        $this->view->disable();
//        $d = $this->request->get("d");
//        $p = $this->request->get("p");

        $view->start();
        $report = Reports::getDatosQueryById($id);
        if($report->valid()){
            $error = ($report[0]->json == 'error');
            $emptyJson = ($report[0]->json == '');
            $view->setVar('NotFound', false);
            $view->setVar('id', $report[0]->id);
            $view->setVar('name', $report[0]->nombre);
            $view->setVar('emptyJson', $emptyJson);
            $view->setVar('error', $error);
            if($emptyJson == false && $error == false){
                $json = json_decode($report[0]->json);
                $view->setVar('json', $json);

                $view->setVar('heads', array_keys(get_object_vars($json[0])) );
            }
            $hasButtons = false;
            $buttons = array();
            if($this->acl->isAllowedUser('reports', 'exportExcel')){
                array_push($buttons,'{"text": "<i class='."\'fa fa-file-excel-o\'".'></i>", "title": "'.$report[0]->nombre.'", "extend": "excel","exportOptions": {"columns": ":visible"}}');
                $hasButtons = true;
            }
            if($this->acl->isAllowedUser('reports', 'exportPdf')){
                array_push($buttons,'{"text": "<i class='."\'fa fa-file-pdf-o\'".'></i>", "title": "'.$report[0]->nombre.'", "extend": "pdf","exportOptions": {"columns": ":visible"}}');
                $hasButtons = true;
            }
            if($this->acl->isAllowedUser('reports', 'print')){
                $jsonPrint = '{"text": "<i class='."\'fa fa-print\'".'></i>", "title": "'.$report[0]->nombre.'", "extend": "print","exportOptions": {"columns": ":visible"}}';
                array_push($buttons, $jsonPrint);

                $hasButtons = true;
            }
            $view->setVar('hasButtons', $hasButtons);
            $view->setVar('buttons', $buttons);
        }
        else{
            $view->setVar('NotFound', true);
        }
        $view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $view->render('reports', 'reports');
        $view->finish();

        $this->response->setContent($view->getContent());
        return $this->response;
    }
}
