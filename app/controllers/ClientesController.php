<?php
/**
 * Clase controller para el Módulo de los clientes
 * @author [russel] <[<email address>]>
 */
namespace Vokuro\Controllers;

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Vokuro\DT\SSPGEO;
use Vokuro\Forms\ClientesForm;
use Vokuro\Mail\Mail;
use Vokuro\Models\BitacoraCambios;
use Vokuro\Models\BitacoraEstatus;
use Vokuro\Models\FacturacionTamboreo;
use Vokuro\Models\HistorialPago;
use Vokuro\Models\MetodoPago;
use Vokuro\Models\Pagos;
use Vokuro\Models\Profiles;
use Vokuro\Models\RazonSocial;
//use Vokuro\Models\TipoDocumento;
use Vokuro\Models\Ruta;
use Vokuro\Models\UserProfiles;
use Vokuro\Models\Clientes;
use Phalcon\Mvc\View;
use Vokuro\GenericSQL\GenericSQL;
use Vokuro\Models\Colonias;
use Vokuro\Models\Cliente_Descuento_Pensionado;
use Vokuro\Models\Tipo_descuento;
use Vokuro\Models\Estatus_cliente;
use Vokuro\Models\Tarifa_colonia;
use Vokuro\Models\Rutas;
use Vokuro\Models\Tipo_contratos;
use Vokuro\Models\Users;
use Vokuro\Models\TipoDocumento;

class ClientesController extends ControllerBase
{

    /**
     * [indexAction  Default action. Set the public layout (layouts/public.volt)]
     * @return [view] [Vista para Clientes]
     */
    public function indexAction()
    {
        $this->view->setTemplateBefore('public');
        $this->view->setVar('doSearch', $this->acl->isAllowedUser('clientes', 'search'));
        $this->view->setVar('colonias', Colonias::findByActivo());
        $this->view->setVar('tipo_descuento', Tipo_descuento::findByActivo());
        $this->view->setVar('estatus_cliente', Estatus_cliente::findByActivo());
//        $this->view->setVar('tarifa_colonia', Tarifa_colonia::findByActivo());
        //$this->view->setVar('rutas', Rutas::findByActivo());
        $this->view->setVar('rutas', Ruta::findByActivo(true));
//        $this->view->setVar('tipo_contratos', Tipo_contratos::findByActivo());
        $this->view->setVar('metodos_pago', MetodoPago::findByActivo(true));
        $this->view->setVar('facturacion_tamboreo', FacturacionTamboreo::findByActivo(true));
        $this->view->setVar('tipocomprobante', TipoDocumento::findByComprobante(true));
        $this->view->setVar('tipoidentificacion', TipoDocumento::findByComprobante(false));

        $rss = array();
        $razon_social = RazonSocial::find([
            "activo = true",
            "order" => "razon_social",
        ]);
        foreach ($razon_social as $rs){
            $std = new \stdClass();
            $std->text = $rs->razon_social;
            $std->value = $rs->id;
            $std->fisica = $rs->fisica ? "true" : "false";
            array_push($rss, $std);
        }
        $this->view->setVar('rss', $razon_social);
        $this->view->setVar('rss_json', json_encode($rss));
    }

    /**
     * [addAction Crea un nuevo Cliente]
     * @author [russel] <[<email address>]>
     */
    public function saveAction()
    {
        $this->view->disable();

        if ($this->request->isPost() == true) {
            if($this->request->isAjax() == true) {
                //$form = new ClientesForm();
                $identity = $this->auth->getIdentity();
                $idUser = $identity["id"];
                $idCliente = $this->request->getPost('id', 'int');
                $resp = new \stdClass();
                $comercio = $this->request->getPost('comercio');
                $idestatuscliente = $this->request->getPost('idestatuscliente', 'int');

                $dataB = new BitacoraCambios();
                $dataB->modulo = 'CLIENTE WEB';
                $dataB->tabla = "cliente.cliente";
                $dataB->idusuario = $idUser;

                $odlCLiente = null;

                $this->db->begin();

                if($idCliente != null){
                    $dataB->accion = 'EDICION CLIENTE';
                    $odlCLiente = Clientes::findFirstById_cliente($idCliente);
                    $resp->nuevo = false;
                    $clientes = Clientes::findFirstById_cliente($idCliente);
                    if($clientes === false){
                        $this->response->setStatusCode(404, "Not Found");
                        return $this->response;
                    }
                    $clientes->fecha_modificacion = date("c");

                    $dataOrigin = json_encode($clientes);
                }
                else{
                    $dataB->accion = 'ALTA CLIENTE';
                    $dataOrigin = null;
                    $resp->nuevo = true;
                    $clientes = new Clientes();
                    $clientes->id_usuario = $idUser;
                    $clientes->fecha_modificacion = date("c");
                    $clientes->fecha_creacion = date("c");
                    $clientes->idestatuscliente = $idestatuscliente;
                }

                $dataB->original = $dataOrigin;

                $clientes->idusuario_modifico = $idUser;

                $calle = $this->request->getPost('calle', 'int');
                $calle = ($calle != null and $calle != "") ? intval($calle) : null;
                $calleLetra = $this->request->getPost('calle_letra', 'striptags');
                $calleLetra = ($calleLetra != null and $calleLetra != "") ? $calleLetra : null;
                $tipoCalle = $this->request->getPost('tipo_calle', 'striptags');
                $tipoCalle = ($tipoCalle != null and $tipoCalle != "") ? $tipoCalle : null;
                $numero = $this->request->getPost('numero', 'int');
                $numero = ($numero != null and $numero != "") ? intval($numero) : null;
                $numeroLetra = $this->request->getPost('numero_letra', 'striptags');
                $numeroLetra = ($numeroLetra != null and $numeroLetra != "") ? $numeroLetra : null;
                $tipoNumero = $this->request->getPost('tipo_numero', 'striptags');
                $tipoNumero = ($tipoNumero != null and $tipoNumero != "") ? $tipoNumero : null;
                $cruza1 = $this->request->getPost('cruza1', 'striptags');
                $cruza1 = ($cruza1 != null and $cruza1 != "") ? $this->request->getPost('cruza1', 'striptags') : null;
                $cruza1Letra = $this->request->getPost('letra_cruza1', 'striptags');
                $cruza1Letra = ($cruza1Letra != null and $cruza1Letra != "") ? $cruza1Letra : null;
                $tipoCruza1 = $this->request->getPost('tipo_cruza1', 'striptags');
                $tipoCruza1 = ($tipoCruza1 != null and $tipoCruza1 != "") ? $tipoCruza1 : null;
                $cruza2 = $this->request->getPost('cruza2', 'striptags');
                $cruza2 = ($cruza2 != null and $cruza2 != "") ? $cruza2 : null;
                $cruza2Letra = $this->request->getPost('letra_cruza2', 'striptags');
                $cruza2Letra = ($cruza2Letra != null and $cruza2Letra != "") ? $cruza2Letra : null;
                $tipoCruza2 = $this->request->getPost('tipo_cruza2', 'striptags');
                $tipoCruza2 = ($tipoCruza2 != null and $tipoCruza2 != "") ? $tipoCruza2 : null;
                $idcolonia = $this->request->getPost('idcolonia', 'int');
                $idcolonia = ($idcolonia != null and $idcolonia != "") ? intval($idcolonia) : null;
                $localidad = $this->request->getPost('localidad', 'striptags');
                $localidad = ($localidad != null and $localidad != "") ? $localidad : null;
                $apepat = $this->request->getPost('apepat', 'striptags');
                $apepat = ($apepat != null and $apepat != "") ? $apepat : null;
                $apemat = $this->request->getPost('apemat', 'striptags');
                $apemat = ($apemat != null and $apemat != "") ? $apemat : null;
                $nombres = $this->request->getPost('nombres', 'striptags');
                $nombres = ($nombres != null and $nombres != "") ? $nombres : null;
                $apepat_propietario = $this->request->getPost('apepat_propietario', 'striptags');
                $apepat_propietario = ($apepat_propietario != null and $apepat_propietario != "") ? $apepat_propietario : null;
                $apemat_propietario = $this->request->getPost('apemat_propietario', 'striptags');
                $apemat_propietario = ($apemat_propietario != null and $apemat_propietario != "") ? $apemat_propietario : null;
                $nombres_propietario = $this->request->getPost('nombres_propietario', 'striptags');
                $nombres_propietario = ($nombres_propietario != null and $nombres_propietario != "") ? $nombres_propietario : null;
                $folio_catastral = $this->request->getPost('folio_catastral', 'int');
                $folio_catastral = ($folio_catastral != null and $folio_catastral != "") ? intval($folio_catastral) : null;
                //$idestatuscliente = $this->request->getPost('idestatuscliente', 'int');
                //$idestatuscliente = ($idestatuscliente != null and $idestatuscliente != "") ? intval($idestatuscliente) : null;
                $latitud = $this->request->getPost('latitud');
                $latitud = ($latitud != null and $latitud != "") ? floatval($latitud) : null;
                $longitud = $this->request->getPost('longitud');
                $longitud = ($longitud != null and $longitud != "") ? floatval($longitud) : null;
                $idtarifa_colonia = $this->request->getPost('idtarifa_colonia', 'int');
                $idtarifa_colonia = ($idtarifa_colonia != null and $idtarifa_colonia != "") ? intval($idtarifa_colonia) : null;
                $idruta = $this->request->getPost('idruta', 'int');
                $idruta = ($idruta != null and $idruta != "") ? intval($idruta) : null;
                $idtipo_contrato = $this->request->getPost('idtipo_contrato', 'int');
                $idtipo_contrato = ($idtipo_contrato != null and $idtipo_contrato != "") ? intval($idtipo_contrato) : null;
                $referencia_ubicacion = $this->request->getPost('referencia_ubicacion', 'striptags');
                $referencia_ubicacion = ($referencia_ubicacion != null and $referencia_ubicacion != "") ? $referencia_ubicacion : null;
                $telefono = $this->request->getPost('telefono', 'int');
                $telefono = str_replace(array("(", ")", " ", "-"), array("", "", "", ""), $telefono);
                $telefono = ($telefono != null and $telefono != "") ? $telefono : null;
                $correo = $this->request->getPost('correo', 'striptags');
                $correo = ($correo != null and $correo != "") ? $correo : null;
                $idtipo_descuento = $this->request->getPost('idtipo_descuento', 'int');
                $idtipo_descuento = ($idtipo_descuento != null and $idtipo_descuento != "") ? intval($idtipo_descuento) : null;
                $ultimo_anio_pago = $this->request->getPost('ultimo_anio_pago', 'int');
                $ultimo_anio_pago = ($ultimo_anio_pago != null and $ultimo_anio_pago != "") ? intval($ultimo_anio_pago) : null;
                $ultimo_mes_pago = $this->request->getPost('ultimo_mes_pago', 'int');
                $ultimo_mes_pago = ($ultimo_mes_pago != null and $ultimo_mes_pago != "") ? intval($ultimo_mes_pago) : null;
                $observacion = $this->request->getPost('observacion', 'striptags');
                $observacion = ($observacion != null and $observacion != "") ? $observacion : null;
                $direccion_otro = $this->request->getPost('direccion_otro', 'striptags');
                $direccion_otro = ($direccion_otro != null and $direccion_otro != "") ? $direccion_otro : null;
                $mensualidad = $this->request->getPost('mensualidad', 'float');
                $mensualidad = ($mensualidad != null and $mensualidad != "") ? floatval($mensualidad) : null;

                //Datosadicionales
                $monto_cobro = $this->request->getPost('monto_cobro', 'float');
                $cobro_manual = $this->request->getPost('cobro_manual');

                //moral
                $nombre_sucursal = $this->request->getPost('nombre_sucursal', 'striptags');
                $nombre_sucursal = ($nombre_sucursal != null and $nombre_sucursal != "") ? $nombre_sucursal : null;
                $contacto_servicio = $this->request->getPost('contacto_servicio', 'striptags');
                $contacto_servicio = ($contacto_servicio != null and $contacto_servicio != "") ? $contacto_servicio : null;
                $contacto_servicio_tel = $this->request->getPost('contacto_servicio_tel', 'int');
                $contacto_servicio_tel = str_replace(array("(", ")", " ", "-"), array("", "", "", ""), $contacto_servicio_tel);
                $contacto_servicio_tel = ($contacto_servicio_tel != null and $contacto_servicio_tel != "") ? $contacto_servicio_tel : null;
                $contacto_servicio_ext = $this->request->getPost('contacto_servicio_ext', 'int');
                $contacto_servicio_ext = ($contacto_servicio_ext != null and $contacto_servicio_ext != "") ? $contacto_servicio_ext : null;
                $contacto_pago1 = $this->request->getPost('contacto_pago1', 'striptags');
                $contacto_pago1 = ($contacto_pago1 != null and $contacto_pago1 != "") ? $contacto_pago1 : null;
                $contacto_pago1_tel = $this->request->getPost('contacto_pago1_tel', 'int');
                $contacto_pago1_tel = str_replace(array("(", ")", " ", "-"), array("", "", "", ""), $contacto_pago1_tel);
                $contacto_pago1_tel = ($contacto_pago1_tel != null and $contacto_pago1_tel != "") ? $contacto_pago1_tel : null;
                $contacto_pago1_ext = $this->request->getPost('contacto_pago1_ext', 'int');
                $contacto_pago1_ext = ($contacto_pago1_ext != null and $contacto_pago1_ext != "") ? $contacto_pago1_ext : null;
                $contacto_pago2 = $this->request->getPost('contacto_pago2', 'striptags');
                $contacto_pago2 = ($contacto_pago2 != null and $contacto_pago2 != "") ? $contacto_pago2 : null;
                $contacto_pago2_tel = $this->request->getPost('contacto_pago2_tel', 'int');
                $contacto_pago2_tel = str_replace(array("(", ")", " ", "-"), array("", "", "", ""), $contacto_pago2_tel);
                $contacto_pago2_tel = ($contacto_pago2_tel != null and $contacto_pago2_tel != "") ? $contacto_pago2_tel : null;
                $contacto_pago2_ext = $this->request->getPost('contacto_pago2_ext', 'int');
                $contacto_pago2_ext = ($contacto_pago2_ext != null and $contacto_pago2_ext != "") ? $contacto_pago2_ext : null;
                $metodo_pago = $this->request->getPost('metodo_pago', 'int');
                $metodo_pago = ($metodo_pago != null and $metodo_pago != "") ? intval($metodo_pago) : null;
                $numero_contrato = $this->request->getPost('numero_contrato', 'striptags');
                $numero_contrato = ($numero_contrato != null and $numero_contrato != "") ? $numero_contrato : null;
                $periodicidad_facturacion = $this->request->getPost('periodicidad_facturacion', 'striptags');
                $periodicidad_facturacion = ($periodicidad_facturacion != null and $periodicidad_facturacion != "") ? $periodicidad_facturacion : "";
                $tipo_facturacion = $this->request->getPost('tipo_facturacion', 'striptags');
                $tipo_facturacion = ($tipo_facturacion != null and $tipo_facturacion != "") ? $tipo_facturacion : "";
                $monto_tasa_fija = $this->request->getPost('monto_tasa_fija', 'float');
                $monto_tasa_fija = ($monto_tasa_fija != null and $monto_tasa_fija != "") ? floatval($monto_tasa_fija) : null;
                $idfacturacion_tamboreo = $this->request->getPost('idfacturacion_tamboreo', 'int');
                $idfacturacion_tamboreo = ($idfacturacion_tamboreo != null and $idfacturacion_tamboreo != "") ? intval($idfacturacion_tamboreo) : null;
                $cantidad_tamboreo = $this->request->getPost('cantidad_tamboreo', 'int');
                $cantidad_tamboreo = ($cantidad_tamboreo != null and $cantidad_tamboreo != "") ? intval($cantidad_tamboreo) : null;

                $idrazon_social = $this->request->getPost('idrazon_social', 'int');
                $idrazon_social = ($idrazon_social != null and $idrazon_social != "") ? intval($idrazon_social) : null;

                $maginado = $this->request->getPost('marginado');
                $abandonado = $this->request->getPost('abandonado');
                $privada = $this->request->getPost('privada');
                $nombre_privada = $this->request->getPost('nombre_privada');
                $nombre_comercio = $this->request->getPost('nombre_comercio');

                $idtipocomp = $this->request->getPost('id_tipo_comprobante', 'int');
                $idtipoiden = $this->request->getPost('id_tipo_identificacion', 'int');

                $telefono = $this->request->getPost('telefono_contacto', 'int');
                $telefono = str_replace(array("(", ")", " ", "-"), array("", "", "", ""), $telefono);
                $telefono = ($telefono != null and $telefono != "") ? $telefono : null;

                $validaDireccion = Clientes::validateDireccion($idCliente, $folio_catastral, $calle, $calleLetra, $numero, $numeroLetra, $idcolonia);
                if($validaDireccion->valid()){
                    $this->response->setStatusCode(409, "Conflict");
                    return $this->response;
                }

                $muni = Colonias::findFirst($idcolonia);
                $ubicado = ($latitud != null and $longitud != null) ? true : false;

                $clientes->assign(array(
                    'calle' => $calle,
                    'calle_letra' => $calleLetra,
                    'tipo_calle' => $tipoCalle,
                    'numero' => $numero,
                    'numero_letra' => $numeroLetra,
                    'tipo_numero' => $tipoNumero,
                    'cruza1' => $cruza1,
                    'letra_cruza1' => $cruza1Letra,
                    'tipo_cruza1' => $tipoCruza1,
                    'cruza2' => $cruza2,
                    'letra_cruza2' => $cruza2Letra,
                    'tipo_cruza2' => $tipoCruza2,
                    'idcolonia' => $idcolonia,
                    'localidad' => $localidad,
                    'apepat' => $apepat,
                    'apemat' => $apemat,
                    'nombres' => $nombres,
                    'apepat_propietario' => $apepat_propietario,
                    'apemat_propietario' => $apemat_propietario,
                    'nombres_propietario' => $nombres_propietario,
                    'folio_catastral' => $folio_catastral,
                    //'idestatuscliente' => $idestatuscliente,
                    'activo' => TRUE,
                    'latitud' => $latitud,
                    'longitud' => $longitud,
                    'ubicado' => true,
                    'idtarifa_colonia' => $idtarifa_colonia,
                    'idruta' => $idruta,
                    'idtipo_contrato' => $idtipo_contrato,
                    'referencia_ubicacion' => $referencia_ubicacion,
                    'fisica' => $this->request->getPost('fisica'),
                    'telefono' => $telefono,
                    'correo' => $correo,
                    'idtipo_descuento' => $idtipo_descuento,
//                    'descuento' => $this->request->getPost('descuento'),
                    'mensualidad' => $mensualidad,
                    'ultimo_anio_pago' => $ultimo_anio_pago,
                    'ultimo_mes_pago' => $ultimo_mes_pago,
                    'observacion' => $observacion,
//                    'id_usuario' => $idUser,
                    'direccion_otro' => $direccion_otro,
                    'vigente' => true,
                    'monto_tasa_fija' => $monto_tasa_fija,
                    'cantidad_tamboreo' => $cantidad_tamboreo,
                    'nombre_sucursal' => $nombre_sucursal,
                    'contacto_servicio' => $contacto_servicio,
                    'contacto_servicio_tel' => $contacto_servicio_tel,
                    'contacto_servicio_ext' => $contacto_servicio_ext,
                    'contacto_pago1' => $contacto_pago1,
                    'contacto_pago1_tel' => $contacto_pago1_tel,
                    'contacto_pago1_ext' => $contacto_pago1_ext,
                    'contacto_pago2' => $contacto_pago2,
                    'contacto_pago2_tel' => $contacto_pago2_tel,
                    'contacto_pago2_ext' => $contacto_pago2_ext,
                    'idmetodo_pago' => $metodo_pago,
                    'contrato' => $numero_contrato != null ? true : false,
                    'numero_contrato' => $numero_contrato,
                    'periodicidad_facturacion' => $periodicidad_facturacion,
                    'idfacturacion_tamboreo' => $idfacturacion_tamboreo,
                    'tipo_facturacion' => $tipo_facturacion,
                    'ubicado' => $ubicado,
                    'idrazon_social' => $idrazon_social,
                    'cobro_manual' => $cobro_manual,
                    'monto_cobro' => $monto_cobro,

                    'comercio' => $comercio,
                    'abandonado' => $abandonado,
                    'marginado' => $maginado,
                    'privada' => $privada,
                    'nombre_privada' => $nombre_privada,
                    'nombre_comercio' => $nombre_comercio,
                    'telefono_contacto' => $telefono,
                    'id_tipo_comprobante' => $idtipocomp ? $idtipocomp : null,
                    'id_tipo_identificacion' => $idtipoiden ? $idtipoiden : null,
                    'idmunicipio' => $muni->cve_mun
                ));

//                var_dump($clientes->toArray());
//                die;

                if($clientes->save() === false) {
                    foreach ($clientes->getMessages() as $message) {
                        $this->logger->info("(save-dirper): " . $message);
                    }
                    $this->response->setStatusCode(500, "Internal Servere Error");
                    $this->db->rollback();
                }
                else{
                    $dataB->cambios = json_encode($clientes);
                    $dataB->identificador = $clientes->id_cliente;

                    if ($dataB->save()) {
                        $this->db->commit();
                        $clientes->refresh();

                        $persona = "";
                        if($apepat != null and $apepat != ""){
                            $persona .= $apepat;
                        }
                        if($apemat != null and $apemat != ""){
                            if($persona !=  ""){
                                $persona .= " ";
                            }
                            $persona .= $apemat;
                        }
                        if($nombres != null and $nombres != ""){
                            if($persona !=  ""){
                                $persona .= " ";
                            }
                            $persona .= $nombres;
                        }

                        $propietario = "";
                        if($apepat_propietario != null and $apepat_propietario != ""){
                            $propietario .= $apepat_propietario;
                        }
                        if($apemat_propietario != null and $apemat_propietario != ""){
                            if($propietario !=  ""){
                                $propietario .= " ";
                            }
                            $propietario .= $apemat_propietario;
                        }
                        if($nombres_propietario != null and $nombres_propietario != ""){
                            if($propietario !=  ""){
                                $propietario .= " ";
                            }
                            $propietario .= $nombres_propietario;
                        }

                        $vigente = '<i class="fa fa-check" style="color: green" title="Activo"></i>';
                        $calle = ($calle != null and $calle != "") ? trim($calle) : "";
                        $calleLetra = ($calleLetra != null and $calleLetra != "") ? " ".trim($calleLetra) : "";
                        $calle = $calle.$calleLetra;

                        $numero = ($numero != null and $numero != "") ? trim($numero) : "";
                        $numeroLetra = ($numeroLetra != null and $numeroLetra != "") ? " ".trim($numeroLetra) : "";
                        $numero = $numero.$numeroLetra;

                        $cruz1 = ($cruza1 != null and $cruza1 != "") ? trim($cruza1) : "";
                        $cruz1Letra = ($cruza1Letra != null and $cruza1Letra != "") ? trim($cruza1Letra) : "";
                        $cruz1 = $cruz1.$cruz1Letra;

                        $cruz2 = ($cruza2 != null and $cruza2 != "") ? trim($cruza2) : "";
                        $cruz2Letra = ($cruza2Letra != null and $cruza2Letra != "") ? trim($cruza2Letra) : "";
                        $cruz2 = $cruz2.$cruz2Letra;

                        $colonia = "";
                        if($idcolonia != null and $idcolonia != ""){
                            $eColonia = Colonias::findFirstById($idcolonia);
                            if($eColonia !== false){
                                $colonia = $eColonia->nombre.", ".$eColonia->localidad;
                            }
                        }

                        $resp->vigente = $vigente;
                        $resp->id = $clientes->id_cliente;
                        $resp->calle = $calle;
                        $resp->numero = $numero;
                        $resp->cruz1 = $cruz1;
                        $resp->cruz2 = $cruz2;
                        $resp->colonia = $colonia;
                        $resp->persona = $persona;
                        $resp->propietario = $propietario;
                        $this->response->setContent(json_encode($resp));
                    }
                    else{
                        foreach ($dataB->getMessages() as $message) {
                            $this->logger->info("(apiv2-cancelacion-pago-bitacora-pago): " . $message);
                        }
                        $this->response->setStatusCode(500);
                        $this->db->rollback();
                    }

                }
            }
            else{
                $this->response->setStatusCode(400, "Bad Request");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    /**
     * [deleteAction Dar de baja al cliente seteando activo=0]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function deleteAction($id)
    {
        if($id != null and $id != ""){
            $identity = $this->auth->getIdentity();
            $idUser = $identity["id"];
            $dataB = new BitacoraCambios();
            $dataB->modulo = 'CLIENTE WEB';
            $dataB->tabla = "cliente.cliente";
            $dataB->idusuario = $idUser;

            $cliente = Clientes::findFirstById_cliente($id);
            $dataOrigin = json_encode($cliente);
            if ($cliente) {
                $this->db->begin();
                $dataB->accion = 'DESACTIVAR CLIENTE';
                $dataB->original = $dataOrigin;
                $cliente->vigente = false;
                $cliente->fecha_modificacion = date("c");
                if(!$cliente->save()){

                    foreach ($cliente->getMessages() as $message) {
                        $this->logger->info("(delete-persona): " . $message);
                    }
                    $this->response->setStatusCode(500);
                }
                else{
                    $dataB->cambios = json_encode($cliente);
                    $dataB->identificador = $id;

                    if ($dataB->save()) {
                        $this->db->commit();
                    }
                    else{
                        foreach ($dataB->getMessages() as $message) {
                            $this->logger->info("(delete-persona-bitacora): " . $message);
                        }
                        $this->response->setStatusCode(500);
                        $this->db->rollback();
                    }
                }
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(400, "Bad Request");
        }
        return $this->response;
    }

    function buscarAction(){
        $request = $this->request;
        $response = $this->response;

        $columns = array(
            array( 'db' => '', 'dt' => 0,
                'formatter' => function( $d, $row ) {
                    return null;
                }
            ),
            array( 'db' => '', 'dt' => 1),
            array( 'db' => 'vigente', 'datatype' => 'boolean', 'dt' => 2,
                'formatter' => function( $d, $row ) {
                    $vigente = $d ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                        : '<i class="fa fa-remove" style="color: red" title="Activo"></i>';

                    return $vigente;
                }
            ),
            array( 'db' => 'id_cliente', 'dt' => 3, 'datatype' => 'number',
                'formatter' => function( $d, $row ) {
                    return $d.( $row["pensionado"] === true ? " <span style='background-color: green; color: white; padding: 5px;'>(PENSIONADO)</span>" : "");

                }
            ),
            array( 'db' => 'calle_completa', 'dt' => 4, 'separator' => ','),
            array( 'db' => 'numero_completa', 'dt' => 5, 'separator' => ','),
            array( 'db' => 'cruza1_completa', 'dt' => 6, 'separator' => ','),
            array( 'db' => 'cruza2_completa', 'dt' => 7, 'separator' => ','),
            array( 'db' => 'colonia', 'dt' => 8, 'separator' => ','),
            array( 'db' => 'persona', 'dt' => 9, 'separator' => ','),
            array( 'db' => 'propietario', 'dt' => 10, 'separator' => ','),
            array( 'db' => 'ultimo_mes_pago', 'dt' => 11, 'datatype' => 'number',
                'formatter' => function( $d, $row ) {
                    $meses = array('enero','febrero','marzo','abril','mayo','junio','julio',
                        'agosto','septiembre','octubre','noviembre','diciembre');

                    $ultmespago = ($d != null) ? ucfirst($meses[intval($d) - 1]): "";
                    return $ultmespago;

                }
            ),
            array( 'db' => 'ultimo_anio_pago', 'dt' => 12, 'datatype' => 'number'),
            array( 'db' => 'comercio', 'dt' => 13, 'formatter' => function( $d, $row ) {
                return $d === true ? $row["nombre_comercio"] : "NO";

            }),
            array( 'db' => 'abandonado', 'dt' => 14, 'formatter' => function( $d, $row ) {
                return $d === true ? "SI" : "NO";

            }),
            array( 'db' => 'marginado', 'dt' => 15, 'formatter' => function( $d, $row ) {
                return $d === true ? "SI" : "NO";

            }),
            array( 'db' => 'privada', 'dt' => 16, 'formatter' => function( $d, $row ) {
                return $d === true ? $row["nombre_privada"] : "NO";

            }),
            array( 'db' => 'latitud', 'dt' => 17, 'datatype' => 'number'),
            array( 'db' => 'longitud', 'dt' => 18, 'datatype' => 'number'),
            array( 'db' => 'idcolonia', 'dt' => 19, 'datatype' => 'number'),
            array( 'db' => 'nombre_comercio', 'dt' => 20),
            array( 'db' => 'nombre_privada', 'dt' => 21),
            array( 'db' => 'pensionado', 'dt' => 22),
            array( 'db' => 'estatus', 'dt' => 23, 'formatter' => function( $d, $row ) {
                if ($this->acl->isAllowedUser("clientes", "editsta") || $this->acl->isAllowedUser("clientes", "infosegsta")){
                    $li = '';
                    if($this->acl->isAllowedUser("clientes", "editsta")){
                        $ec = Estatus_cliente::find([
                            "order" => "orden asc"
                        ]);
                    
                        foreach($ec as $o){
                            if($o->id == $row["idestatuscliente"]) continue;
                            $li .= '<li data-id="'.$o->id.'" data-sigla="'.$o->sigla.'" class="cliestat-change-status">
                                <a href="#">'.$o->nombre.'</a>
                            </li>';
                        }
                    }
                    if($this->acl->isAllowedUser("clientes", "infosegsta") &&
                        $this->acl->isAllowedUser("clientes", "editsta")){
                        $li .= '<li role="separator" class="divider"></li>';
                    }
                    if($this->acl->isAllowedUser("clientes", "infosegsta")){
                        $li .= '<li data-id="'.$o->id.'" class="cliestat-change-bitacora">
                            <a href="#">Seguimiento</a>
                        </li>';
                    }
                    $showactions = ($this->acl->isAllowedUser("clientes", "infosegsta") or $this->acl->isAllowedUser("clientes", "editsta")) ?
                        '' : 'disabled';
                    $html = '<div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" '.$showactions.' data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        '.$row["estatus"].' <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">'.$li.'</ul>
                    </div>';
                }
                else {
                    $html = $d;
                }
                
                return $html;
            }),
            array( 'db' => 'idestatuscliente', 'dt' => 24, 'datatype' => 'number'),
            array( 'db' => 'comercio', 'dt' => 25, 'datatype' => 'number'),
            array( 'db' => 'vigente', 'dt' => 26, 'datatype' => 'boolean'),
            array( 'db' => 'activo', 'dt' => 27, 'datatype' => 'boolean'),
            array( 'db' => 'estatus_sigla', 'dt' => 28),
            array( 'db' => 'mensualidad', 'dt' => 29, 'datatype' => 'number'),
            array( 'db' => 'idruta', 'dt' => 30, 'datatype' => 'number'),
            array( 'db' => 'ruta', 'dt' => 31),
            array( 'db' => 'ruta_validada', 'dt' => 32, 'datatype' => 'boolean', 'formatter' => function( $d, $row ) {
                $html = 'No';
                if($row["idruta"]) {
                    if ($d) {
                        $html = 'Si <button data-estatus="validado" class="btn btn-primary btn-sm btnInfoRutaValidar"><i class="fa fa-info"></i> </button>';
                    }
                    else {
                        if ($this->acl->isAllowedUser("clientes", "valru")) {
                            $html = 'No <button data-estatus="novalidado" class="btn btn-primary btn-sm btnInfoRutaValidar" title="Validar ruta"><i class="fa fa-check"></i> </button>';
                        }
                    }
                }
                return $html;
            }),
            array( 'db' => 'usuario_valida_ruta', 'dt' => 33),
            array( 'db' => 'fecha_ruta_validada', 'dt' => 34),
            array( 'db' => 'ruta_validada', 'dt' => 35, 'datatype' => 'boolean'),
            array( 'db' => 'observaciones_valida_ruta', 'dt' => 36, 'datatype' => 'boolean'),
            array(
                'db' => 'id_cliente',
                'dt' => 'DT_RowId',
                'formatter' => function( $d, $row ) {
                    // Technically a DOM id cannot start with an so we prefix
                    // a string. This can also be useful if you have multiple tables
                    // to ensure that the id is unique with a different prefix
                    return 'cliente-'.$d;
                }
            )
        );

        $request = $this->request->get();
        $whereAll = "activo = true";
        
        if($request['columns'][30]["search"]["value"] == "-1"){
            $whereAll = " idruta is null";
            $request['columns'][30]["search"]["value"] = "";
        }

        if($request['columns'][32]["search"]["value"] != ""){
            $whereAll = " ruta_validada = ".$request['columns'][32]["search"]["value"];
            $request['columns'][32]["search"]["value"] = "";
        }

        $data = SSPGEO::complex_geo($request, "cliente.view_clientes", "id_cliente", $columns, "activo = true", $whereAll);

//        $where = SSPGEO::filter_join_data( $this->request->get(), $columns, $bindings );

        $this->logger->info(json_encode($data));

        $response->setContent(json_encode($data));
        return $response;
    }

    function eliminarPreposiciones($datos){
        $preposiciones = array('de', 'los', 'la', 'y');
        $newData = array();
        foreach ($datos as $valor) {
            if(!in_array($valor, $preposiciones)){
                array_push($newData,$valor);
            }
        }
        return $newData;
    }

    public function montocoloniaAction($idcolonia){
        $this->view->disable();
        if ($this->request->isGet() == true) {
            $colonia = Colonias::findFirstById($idcolonia);
            if($colonia != false){
                $row = new \stdClass();
                $row->monto = $colonia->monto;
                $rutas = array();
                $r = Rutas::findByIdcolonia($idcolonia);
                if($r->valid()){
                    foreach ($r as $aux){
                        $auxstd = new \stdClass();
                        $auxstd->id = $aux->id;
                        $auxstd->nombre = $aux->nombre;
                        array_push($rutas, $auxstd);
                    }
                }
                $row->rutas = $rutas;
                $this->response->setContent(json_encode($row));
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    public function rutacobratarioAction($idruta){
        $this->view->disable();
        if ($this->request->isGet() == true) {
            $ruta = Rutas::findFirstById($idruta);
            if($ruta != false){
                $row = new \stdClass();
                $r = Users::findFirstById($ruta->idusuario);
                $row->nombre = "";
                if($r != false){
                    $row->nombre = $r->nombre." ".$r->apellido_paterno." ".$r->apellido_materno;
                }
                $this->response->setContent(json_encode($row));
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    public function getAction($id) {
        $this->view->disable();
        if ($this->request->isGet() == true) {
            $cliente = Clientes::findFirstById_cliente($id);
            if($cliente != false){

                $row = new \stdClass();
                $row->id_cliente = $cliente->id_cliente;
                $row->calle = $cliente->calle;
                $row->calle_letra = $cliente->calle_letra;
                $row->tipo_calle = $cliente->tipo_calle;
                $row->numero = $cliente->numero;
                $row->numero_letra = $cliente->numero_letra;
                $row->tipo_numero = $cliente->tipo_numero;
                $row->cruza1 = $cliente->cruza1;
                $row->letra_cruza1 = $cliente->letra_cruza1;
                $row->tipo_cruza1 = $cliente->tipo_cruza1;
                $row->cruza2 = $cliente->cruza2;
                $row->letra_cruza2 = $cliente->letra_cruza2;
                $row->tipo_cruza2 = $cliente->tipo_cruza2;
                $row->localidad = $cliente->localidad;
                $row->apepat = $cliente->apepat;
                $row->apemat = $cliente->apemat;
                $row->nombres = $cliente->nombres;
                $row->apepat_propietario = $cliente->apepat_propietario;
                $row->apemat_propietario = $cliente->apemat_propietario;
                $row->nombres_propietario = $cliente->nombres_propietario;
                $row->folio_catastral = $cliente->folio_catastral;
                $row->activo = $cliente->activo;
                $row->fecha_creacion = $cliente->fecha_creacion;
                $row->fecha_modificacion = $cliente->fecha_modificacion;
                $row->latitud = $cliente->latitud;
                $row->longitud = $cliente->longitud;
//                $row->ubicado = $cliente->ubicado;
                $row->referencia_ubicacion = $cliente->referencia_ubicacion;
                $row->fisica = $cliente->fisica;
                $row->nombre_moral = $cliente->nombre_moral;
                $row->rfc = $cliente->rfc;
                $row->cp = $cliente->cp;
                $row->direccion_moral = $cliente->direccion_moral;
                $row->telefono = $cliente->telefono;
                $row->correo = $cliente->correo;
                $row->descuento = $cliente->descuento;
                $row->mensualidad = $cliente->mensualidad;
                $row->ultimo_anio_pago = $cliente->ultimo_anio_pago;
                $row->ultimo_mes_pago = $cliente->ultimo_mes_pago;
                $row->observacion = $cliente->observacion;
                $row->direccion_otro = $cliente->direccion_otro;
                $row->vigente = $cliente->vigente;
                $row->numero_contrato = $cliente->numero_contrato;
                $row->idestatuscliente = $cliente->idestatuscliente;
                $row->idtarifa_colonia = $cliente->idtarifa_colonia;
                $row->idruta = $cliente->idruta;
                $row->idtipo_contrato = $cliente->idtipo_contrato;
                $row->idtipo_descuento = $cliente->idtipo_descuento;
                $row->idcolonia = $cliente->idcolonia;

                $row->razon_social = $cliente->razon_social;
                $row->monto_tasa_fija = $cliente->monto_tasa_fija;
                $row->cantidad_tamboreo = $cliente->cantidad_tamboreo;
                $row->codigo_contpaqi = $cliente->codigo_contpaqi;
                $row->nombre_comercial = $cliente->nombre_comercial;
                $row->nombre_sucursal = $cliente->nombre_sucursal;
                $row->contacto_servicio = $cliente->contacto_servicio;
                $row->contacto_servicio_tel = $cliente->contacto_servicio_tel;
                $row->contacto_servicio_ext = $cliente->contacto_servicio_ext;
                $row->contacto_pago1 = $cliente->contacto_pago1;
                $row->contacto_pago1_tel = $cliente->contacto_pago1_tel;
                $row->contacto_pago1_ext = $cliente->contacto_pago1_ext;
                $row->contacto_pago2 = $cliente->contacto_pago2;
                $row->contacto_pago2_tel = $cliente->contacto_pago2_tel;
                $row->contacto_pago2_ext = $cliente->contacto_pago2_ext;
                $row->fiscal_calle = $cliente->fiscal_calle;
                $row->fiscal_calle_letra = $cliente->fiscal_calle_letra;
                $row->fiscal_calle_tipo = $cliente->fiscal_calle_tipo;
                $row->fiscal_numero = $cliente->fiscal_numero;
                $row->fiscal_numero_letra = $cliente->fiscal_numero_letra;
                $row->fiscal_numero_tipo = $cliente->fiscal_numero_tipo;
                $row->fiscal_cruz1 = $cliente->fiscal_cruz1;
                $row->fiscal_cruz1_letra = $cliente->fiscal_cruz1_letra;
                $row->fiscal_cruz1_tipo = $cliente->fiscal_cruz1_tipo;
                $row->fiscal_cruz2 = $cliente->fiscal_cruz2;
                $row->fiscal_cruz2_letra = $cliente->fiscal_cruz2_letra;
                $row->fiscal_cruz2_tipo = $cliente->fiscal_cruz2_tipo;
                $row->fiscal_idcolonia = $cliente->fiscal_idcolonia;
                $row->idmetodo_pago = $cliente->idmetodo_pago;
                $row->numero_contrato = $cliente->numero_contrato;
                $row->periodicidad_facturacion = $cliente->periodicidad_facturacion;
                $row->idfacturacion_tamboreo = $cliente->idfacturacion_tamboreo;
                $row->tipo_facturacion = $cliente->tipo_facturacion;
                $row->idrazon_social = $cliente->idrazon_social;

                $row->cobro_manual = $cliente->cobro_manual;
                $row->monto_cobro = $cliente->mensualidad;

                $row->marginado = $cliente->marginado;
                $row->comercio = $cliente->comercio;
                $row->abandonado = $cliente->abandonado;
                $row->privada = $cliente->privada;
                $row->nombre_privada = $cliente->nombre_privada;
                $row->nombre_comercio = $cliente->nombre_comercio;

                $row->id_tipo_comprobante = $cliente->id_tipo_comprobante;
                $row->ruta_comprobante = $cliente->ruta_comprobante;
                $row->id_tipo_identificacion = $cliente->id_tipo_identificacion;
                $row->ruta_identificacion = $cliente->ruta_identificacion;
                $row->ruta_foto_predio = $cliente->ruta_foto_predio;
                $row->telefono_contacto = $cliente->telefono;
                $conPago = Pagos::findFirst([
                    "idcliente = ".$id." and activo and cast(fecha_creacion as date) >= '2020-07-14'"
                ]);
                $row->conpago = (bool)$conPago;

                $this->response->setContent(json_encode($row));
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    /**
     * [validaDireccionAction Valida dirección del cliente para no repetir ]
     * @param  [type] $id_cliente   [description]
     * @param  [type] $calle        [description]
     * @param  [type] $calle_letra  [description]
     * @param  [type] $numero       [description]
     * @param  [type] $numero_letra [description]
     * @param  [type] $idcolonia    [description]
     * @return [json]               [retorna los datos en json]
     */
    public function validateByDireccionAction() {
        $this->view->disable();
        if ($this->request->isPost() == true)
        {
            if($this->request->isAjax() == true)
            {
                $id_cliente = $this->request->getPost('id_cliente', 'int');
                $calle = $this->request->getPost('calle', 'int');
                $calle_letra = $this->request->getPost('calle_letra', array('striptags','upper','trim'));
                $numero = $this->request->getPost('numero', 'int');
                $numero_letra = $this->request->getPost('numero_letra', array('striptags','upper','trim'));
                $idcolonia = $this->request->getPost('idcolonia', 'int');

                $clientes = Clientes::validateDireccion($id_cliente, $calle, $calle_letra, $numero, $numero_letra, $idcolonia);
                if($clientes === false)
                {
                    $this->response->setStatusCode(404, "Not Found Client");
                    return $this->response;
                }
                else
                {
                    $row = new \stdClass();
                    $row->status = 200;
                    $cli_aux = array();
                    if(count($clientes) > 0)
                    {
                        $row->ok = true;
                        foreach($clientes as $cli)
                        {
                            $rowp = new \stdClass();
                            $rowp->id_cliente = $cli->id_cliente;
                            $rowp->calle = $cli->calle;
                            $rowp->calle_letra = $cli->calle_letra;
                            $rowp->numero = $cli->numero;
                            $rowp->numero_letra = $cli->numero_letra;
                            $rowp->idcolonia = $cli->idcolonia;
                            $rowp->nombre = $cli->nombre;
                            $rowp->alias = $cli->alias;
                            $rowp->localidad = $cli->localidad;
                            array_push($cli_aux, $rowp);
                        }
                    }
                    else
                    {
                        $row->ok = false;
                    }
                    $row->result = $cli_aux;
                    $this->response->setContent(json_encode($row));
                }
            }
        }
        else
        {
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    public function guardardescuentoAction() {


        $view = clone $this->view;
        $this->view->disable();
        $respuesta = [];

        if(!$this->request->isPost()){
            $this->response->setStatusCode(405);
            return $this->response;
        }

        if ($this->request->hasFiles() != true) {
            $this->response->setContent(json_encode([
                "lOk" => false, 
                "cMensaje" => "La imagen es obligatoria"
            ]));
            $this->response->setStatusCode(400);
            return $this->response;
        }

        
        $clave = $this->request->getPost("clave");
        $idCliente = $this->request->getPost("idCliente");
        $fInicio = $this->request->getPost("fInicio");
        $fFin = $this->request->getPost("fFin");
        $oficio = $this->request->getPost("oficio");
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];


        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];

        $porciones = explode("/", $fInicio);
        $fInicio = $porciones[1]."-".$porciones[0]."-01";
        $fechaInicio = strtotime($porciones[1]."-".$porciones[0]."-01");

        $porciones = explode("/", $fFin);
        $fFin = $porciones[1]."-".$porciones[0]."-01";
        $fechaFin = strtotime($porciones[1]."-".$porciones[0]."-01");

        $cliente = Clientes::findFirstById_cliente($idCliente);

        //guardado de imagen
        $baseLocation = IMG_DIR.DIRECTORY_SEPARATOR."despen".DIRECTORY_SEPARATOR;

        $file = $this->request->getUploadedFiles();
        $file = $file[0];
        $split = explode(".",$file->getName());
        $ext = $split[count($split) - 1];

        $filename = "descpen_".$clave."_".time()."_".date("dmY").".".$ext;
        $fullfilename = $baseLocation.$filename;
        if(!$file->moveTo($fullfilename)){
            $this->response->setContent(json_encode([
                "lOk" => false, 
                "cMensaje" => "La imagen no sepudo guardar"
            ]));
            $this->response->setStatusCode(400);
            return $this->response;
        }

        //fin guardado de imagen

        if(!$cliente) {
            unlink($fullfilename);
            $this->response->setContent(json_encode(["lOk" => false, "cMensaje" => "No existe la información del cliente"]));
            return $this->response;
        }

        //en caso de no contar con ultimo anio o ultimo mes
        $ultimo_anio_pago_aux = $cliente->ultimo_anio_pago ? $cliente->ultimo_anio_pago : "2009";
        $ultimo_mes_pago_aux = $cliente->ultimo_mes_pago ? $cliente->ultimo_mes_pago : "12";

        //$this->logger->info($ultimo_anio_pago_aux);
        //$this->logger->info($ultimo_mes_pago_aux);

        $lastPay = strtotime($ultimo_anio_pago_aux."-".$ultimo_mes_pago_aux."-01");
        $nextPay = strtotime("+1 month", $lastPay);
        $finiPay = strtotime($fInicio);
        // $this->logger->info(date("Y-m-d", $lastPay)." ----------- ".date("Y-m-d", $nextPay)." --------- ".date("Y-m-d", $fechaInicio));
        // $this->logger->info($nextPay." --------- ".$finiPay);
        if($finiPay != $nextPay) {
            unlink($fullfilename);
            $this->response->setContent(json_encode(["lOk" => false, "cMensaje" => "El último mes(".$cliente->ultimo_mes_pago.") y/o el último año (".$cliente->ultimo_anio_pago.") de pago no coincide con la fecha de inicio"]));
            return $this->response;
        }

        $precioTarifa = $cliente->mensualidad;

        $arrayHistorial = array();

        $idcliente = $cliente->id_cliente;
        $this->db->begin();
        $descuento = new Cliente_Descuento_Pensionado();
        $descuento->assign(array(
            'activo' => true,
            'oficio' => $oficio,
            'fecha_inicio' => $fInicio,
            'fecha_fin' => $fFin,
            'id_cliente' => $idCliente,
            'idusuario' => $idUser,
            'fecha_creacion' => 'NOW()',
            'fecha_modificacion' => 'NOW()',
            'archivo' => $filename,
            'tipo_archivo' => $ext 
        ));


        if(!$descuento->save()) {
            unlink($fullfilename);
            $this->db->rollback();
            $this->flash->error($descuento->getMessages());
            foreach ($descuento->getMessages() as $message) {
                $this->logger->error("(save-descuento-pensionado): " . $message);
            }
            $this->response->setContent(json_encode([
                "lOk" => false, 
                "cMensaje" => "No fue posible guardar el registro", 
                "data" => $descuento->getMessages()
            ]));
            $this->response->setStatusCode(500);
            return $this->response;
        }
        else {
            $fpago = date("c");
            $oPago = new Pagos();
            $oPago->fecha_modificacion = date("c");
            $oPago->fecha_creacion = date("c");
            $oPago->fecha_pago = date("c");
            $oPago->idusuario = $idUser;
            $oPago->assign(array(
                "idcliente" => $idcliente,
                "idforma_pago" => 1,
                "activo" => TRUE,
                "ispagoanual" => false,
                "cantidad" => 0,
                "iddescuento_pensionado" => $descuento->id
            ));


            if(!$oPago->save()) {
                unlink($fullfilename);
                $this->db->rollback();
                $this->flash->error($oPago->getMessages());
                foreach ($oPago->getMessages() as $message) {
                    $this->logger->error("(save-descuento-pensionado-pago): " . $message);
                }
                $this->response->setStatusCode(500);
                $this->response->setContent(json_encode([
                    "lOk" => false, 
                    "cMensaje" => "No fue posible guardar el registro", 
                    "data" => $oPago->getMessages()
                ]));
                return $this->response;
            }
            while ($fechaInicio <= $fechaFin) {

                $historial = new HistorialPago();

                $historial->fecha_modificacion = date("c");
                $historial->fecha_creacion = date("c");

                $historial->idtipo_servicio = 0;
                $historial->concepto_servicio = "";


                $historial->assign(array(
                    "idcliente" => $idCliente,
                    "mes"=> date("n", $fechaInicio),
                    "anio"=>  date("Y", $fechaInicio),
                    "descuento"=> $cliente->mensualidad,
                    "iva"=> 0,
                    "cantidad"=> 0,
                    "fecha_pago"=> $fpago,
                    "idforma_pago"=> 3,
                    "referencia_bancaria" => '',
                    "validado" => TRUE,
                    "idusuario" => $idUser,
                    "facturado" => FALSE,
                    "folio_factura" => '',
                    "idcobratario" => null,
                    "caja" => FALSE,
                    "idbanco" => null,
                    "activo" => TRUE,
                    "envio_correo" => FALSE,
                    "recibo_impreso" => FALSE,
                    "incluido_corte" => FALSE,
                    "fecha_corte" => null,
                    "idvisita_cliente" => null,
                    "idtipo_cobro" => 1,
                    "idtipo_servicio" => null,
                    "concepto_servicio" => '',
                    "idpago" => $oPago->idpago
                ));


                if(!$historial->save()) {
                    foreach ($historial->getMessages() as $message) {
                        $this->logger->error("(save-descuento-pensionado-historial): " . $message);
                    }
                    $this->db->rollback();
                    $this->response->setStatusCode(500);
                    $this->flash->error($historial->getMessages());
                    $this->response->setContent(json_encode([
                        "lOk" => false, 
                        "cMensaje" => "No fue posible guardar los registros de historial de pago", 
                        "data" => $historial->getMessages()
                    ]));
                    return $this->response;
                }


                $fechaInicio = strtotime("+1 month", $fechaInicio);

            }

            $rowCount = HistorialPago::findFirst([
                "idcliente = ".$idcliente,
                "order" => "anio desc, mes desc"
            ]);
            // $rowCount = $this->db->query("select * from cliente.historial_pago where idcliente = $idcliente and activo 
                // order by anio::integer desc, mes::integer desc limit 1");
            // $rowCount = $rowCount->fetch();
            if($rowCount){
                $oCli = Clientes::findFirst($idcliente);
                $oCli->ultimo_mes_pago = $rowCount->mes;
                $oCli->ultimo_anio_pago = $rowCount->anio;
                // $oCli->ultimo_mes_pago = $rowCount["mes"];
                // $oCli->ultimo_anio_pago = $rowCount["anio"];
                if(!$oCli->save()){
                    foreach ($historial->getMessages() as $message) {
                        $this->logger->error("(save-descuento-pensionado-update-mes-anio): " . $message);
                    }
                    $this->db->rollback();
                    $this->response->setStatusCode(500);
                    $this->flash->error($historial->getMessages());
                    $this->response->setContent(json_encode([
                        "lOk" => false, 
                        "cMensaje" => "No fue posible guardar los registros del oficio", 
                        "data" => $historial->getMessages()
                    ]));
                    return $this->response;
                }
            }
            $descuento->refresh();
            $descuento->idpago = $oPago->idpago;
            if(!$descuento->save()) {
                unlink($fullfilename);
                $this->db->rollback();
                $this->response->setStatusCode(500);
                $this->flash->error($descuento->getMessages());
                foreach ($descuento->getMessages() as $message) {
                    $this->logger->error("(save-descuento-pensionado-update-idpago): " . $message);
                }
                $this->response->setContent(json_encode([
                    "lOk" => false, 
                    "cMensaje" => "No fue posible guardar el registro", 
                    "data" => $descuento->getMessages()
                ]));
                return $this->response;
            }

            $this->db->commit();

            $this->response->setContent(json_encode(["lOk" => true, "cMensaje" => ""]));
            return $this->response;
        }
    }

    public function getdescuentoAction($idCliente) {
        $this->view->disable();
        if ($this->request->isGet() == true) {

            $conditions = "id_cliente = :idCliente: AND activo = :activo: AND '".date("Y-m-d")."' BETWEEN fecha_inicio AND fecha_fin";
            $parameters = array("idCliente" => $idCliente, "activo" => true);

            $descuento     = Cliente_Descuento_Pensionado::findFirst(array($conditions, "bind" => $parameters));


            if($descuento){

                $this->response->setContent(json_encode(["lOk" => false, "cMensaje" => "Ya existe el oficio ".$descuento->oficio." vigente"]));
                return $this->response;
            }
            else{

                $this->response->setContent(json_encode(["lOk" => true, "cMensaje" => ""]));
                return $this->response;
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    public function saversAction()
    {
        $this->view->disable();

        if ($this->request->isPost() == true) {
            if($this->request->isAjax() == true) {
                $id = $this->request->getPost('id', 'int');
                $resp = new \stdClass();
                if($id != null){
                    $resp->nuevo = false;
                    $rs = RazonSocial::findFirstById($id);
                    if($rs === false){
                        $this->response->setStatusCode(404, "Not Found");
                        return $this->response;
                    }
                    $rs->fecha_modificacion = date("c");
                }
                else{
                    $resp->nuevo = true;
                    $rs = new RazonSocial();
                    $rs->fecha_modificacion = date("c");
                    $rs->fecha_creacion = date("c");
                }

                $rfc = $this->request->getPost('rfc', 'striptags');
                $rfc = ($rfc != null and $rfc != "") ? $rfc : null;
                $telefono = $this->request->getPost('telefono', 'int');
                $telefono = str_replace(array("(", ")", " ", "-"), array("", "", "", ""), $telefono);
                $telefono = ($telefono != null and $telefono != "") ? $telefono : null;
                $correo = $this->request->getPost('correo', 'striptags');
                $correo = ($correo != null and $correo != "") ? $correo : null;
                $razon_social = $this->request->getPost('razon_social', 'striptags');
                $razon_social = ($razon_social != null and $razon_social != "") ? $razon_social : null;
                $codigo_contpaq = $this->request->getPost('codigo_contpaq', 'striptags');
                $codigo_contpaq = ($codigo_contpaq != null and $codigo_contpaq != "") ? $codigo_contpaq : null;
                $nombre_comercial = $this->request->getPost('nombre_comercial', 'striptags');
                $nombre_comercial = ($nombre_comercial != null and $nombre_comercial != "") ? $nombre_comercial : null;
                $calle = $this->request->getPost('calle', 'int');
                $calle = ($calle != null and $calle != "") ? intval($calle) : null;
                $calleLetra = $this->request->getPost('calle_letra', 'striptags');
                $calleLetra = ($calleLetra != null and $calleLetra != "") ? $calleLetra : null;
                $calleTipo = $this->request->getPost('calle_tipo', 'striptags');
                $calleTipo = ($calleTipo != null and $calleTipo != "") ? $calleTipo : null;
                $numero = $this->request->getPost('numero', 'int');
                $numero = ($numero != null and $numero != "") ? intval($numero) : null;
                $numeroLetra = $this->request->getPost('numero_letra', 'striptags');
                $numeroLetra = ($numeroLetra != null and $numeroLetra != "") ? $numeroLetra : null;
                $numeroTipo = $this->request->getPost('numero_tipo', 'striptags');
                $numeroTipo = ($numeroTipo != null and $numeroTipo != "") ? $numeroTipo : null;
                $cruza1 = $this->request->getPost('cruza1', 'striptags');
                $cruza1 = ($cruza1 != null and $cruza1 != "") ? $this->request->getPost('cruza1', 'striptags') : null;
                $cruza1Letra = $this->request->getPost('cruza1_letra', 'striptags');
                $cruza1Letra = ($cruza1Letra != null and $cruza1Letra != "") ? $cruza1Letra : null;
                $cruza1Tipo = $this->request->getPost('cruza1_tipo', 'striptags');
                $cruza1Tipo = ($cruza1Tipo != null and $cruza1Tipo != "") ? $cruza1Tipo : null;
                $cruza2 = $this->request->getPost('cruza2', 'striptags');
                $cruza2 = ($cruza2 != null and $cruza2 != "") ? $cruza2 : null;
                $cruza2Letra = $this->request->getPost('cruza2_letra', 'striptags');
                $cruza2Letra = ($cruza2Letra != null and $cruza2Letra != "") ? $cruza2Letra : null;
                $cruza2Tipo = $this->request->getPost('cruza2_letra', 'striptags');
                $cruza2Tipo = ($cruza2Tipo != null and $cruza2Tipo != "") ? $cruza2Tipo : null;
                $cp = $this->request->getPost('cp', 'int');
                $cp = ($cp != null and $cp != "") ? intval($cp) : null;
                $idcolonia = $this->request->getPost('idcolonia', 'int');
                $idcolonia = ($idcolonia != null and $idcolonia != "") ? intval($idcolonia) : null;
                $fisica = $this->request->getPost('fisica');
                $identity = $this->auth->getIdentity();
                $idusuario = $identity["id"];


                $existRFC = RazonSocial::find([
                    "rfc = :rfc: AND activo = true",
                    "bind" => [
                        "rfc" => $rfc
                    ],
                ]);
                if($existRFC->valid()){
                    $this->response->setStatusCode(409, "Conflict");
                    return $this->response;
                }

                $rs->assign(array(
                    'rfc' => $rfc,
                    'cp' => $cp,
                    'telefono' => $telefono,
                    'correo' => $correo,
                    'razon_social' => $razon_social,
                    'codigo_contpaqi' => $codigo_contpaq,
                    'nombre_comercial' => $nombre_comercial,
                    'calle' => $calle,
                    'calle_letra' => $calleLetra,
                    'calle_tipo' => $calleTipo,
                    'numero' => $numero,
                    'numero_letra' => $numeroLetra,
                    'numero_tipo' => $numeroTipo,
                    'cruza1' => $cruza1,
                    'letra_cruza1' => $cruza1Letra,
                    'cruza1_tipo' => $cruza1Tipo,
                    'cruza2' => $cruza2,
                    'letra_cruza2' => $cruza2Letra,
                    'cruza2_tipo' => $cruza2Tipo,
                    'cp' => $cp,
                    'idcolonia' => $idcolonia,
                    'fisica' => $fisica,
                    'idusuario' => $idusuario
                ));

//                var_dump($clientes->toArray());
//                die;

                if($rs->save() === false) {
                    $this->response->setStatusCode(500, "Internal Servere Error");
                }
                else{
                    $resp->text = $razon_social;
                    $resp->value = $rs->id;
                    $resp->fisica = $fisica ? "true": "false";
                    $this->response->setContent(json_encode($resp));
                }
            }
            else{
                $this->response->setStatusCode(400, "Bad Request");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    public function getrsAction() {
        $this->view->disable();
        if ($this->request->isGet() == true) {
            $razon_social = $this->request->get("rs");
            $fisica = $this->request->get("rs");
            $rs = RazonSocial::findByRazon_socialAndFisicaAndActivo($razon_social, $fisica);
            if($rs->valid()){
                $resp = array();
                foreach ($rs as $auxrs){
                    $aux = new \stdClass();
                    $aux->text = $auxrs->razon_social;
                    $aux->value = $auxrs->id;
                    array_push($resp, $aux);
                }
                $this->response->setContent(json_encode($resp));
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }


    public function savecobroAction()
    {
        $this->view->disable();

        if ($this->request->isPost() == true) {
            if($this->request->isAjax() == true) {
                //$form = new ClientesForm();
                $identity = $this->auth->getIdentity();
                $idUser = $identity["id"];
                $idCliente = $this->request->getPost('id', 'int');
                $resp = new \stdClass();
                if($idCliente != null){
                    $resp->nuevo = false;
                    $clientes = Clientes::findFirstById_cliente($idCliente);
                    if($clientes === false){
                        $this->response->setStatusCode(404, "Not Found");
                        return $this->response;
                    }
                    $clientes->fecha_modificacion = date("c");
                    $clientes->idusuario_modifico = $idUser;
                }
                else{
                    $resp->nuevo = true;
                    $clientes = new Clientes();
                    $clientes->id_usuario = $idUser;
                    $clientes->fecha_modificacion = date("c");
                    $clientes->fecha_creacion = date("c");
                }

                $calle = $this->request->getPost('calle', 'int');
                $calle = ($calle != null and $calle != "") ? intval($calle) : null;
                $calleLetra = $this->request->getPost('calle_letra', 'striptags');
                $calleLetra = ($calleLetra != null and $calleLetra != "") ? $calleLetra : null;
                $tipoCalle = $this->request->getPost('tipo_calle', 'striptags');
                $tipoCalle = ($tipoCalle != null and $tipoCalle != "") ? $tipoCalle : null;
                $numero = $this->request->getPost('numero', 'int');
                $numero = ($numero != null and $numero != "") ? intval($numero) : null;
                $numeroLetra = $this->request->getPost('numero_letra', 'striptags');
                $numeroLetra = ($numeroLetra != null and $numeroLetra != "") ? $numeroLetra : null;
                $tipoNumero = $this->request->getPost('tipo_numero', 'striptags');
                $tipoNumero = ($tipoNumero != null and $tipoNumero != "") ? $tipoNumero : null;
                $cruza1 = $this->request->getPost('cruza1', 'striptags');
                $cruza1 = ($cruza1 != null and $cruza1 != "") ? $this->request->getPost('cruza1', 'striptags') : null;
                $cruza1Letra = $this->request->getPost('letra_cruza1', 'striptags');
                $cruza1Letra = ($cruza1Letra != null and $cruza1Letra != "") ? $cruza1Letra : null;
                $tipoCruza1 = $this->request->getPost('tipo_cruza1', 'striptags');
                $tipoCruza1 = ($tipoCruza1 != null and $tipoCruza1 != "") ? $tipoCruza1 : null;
                $cruza2 = $this->request->getPost('cruza2', 'striptags');
                $cruza2 = ($cruza2 != null and $cruza2 != "") ? $cruza2 : null;
                $cruza2Letra = $this->request->getPost('letra_cruza2', 'striptags');
                $cruza2Letra = ($cruza2Letra != null and $cruza2Letra != "") ? $cruza2Letra : null;
                $tipoCruza2 = $this->request->getPost('tipo_cruza2', 'striptags');
                $tipoCruza2 = ($tipoCruza2 != null and $tipoCruza2 != "") ? $tipoCruza2 : null;
                $idcolonia = $this->request->getPost('idcolonia', 'int');
                $idcolonia = ($idcolonia != null and $idcolonia != "") ? intval($idcolonia) : null;
                $localidad = $this->request->getPost('localidad', 'striptags');
                $localidad = ($localidad != null and $localidad != "") ? $localidad : null;
                $apepat = $this->request->getPost('apepat', 'striptags');
                $apepat = ($apepat != null and $apepat != "") ? $apepat : null;
                $apemat = $this->request->getPost('apemat', 'striptags');
                $apemat = ($apemat != null and $apemat != "") ? $apemat : null;
                $nombres = $this->request->getPost('nombres', 'striptags');
                $nombres = ($nombres != null and $nombres != "") ? $nombres : null;
                $apepat_propietario = $this->request->getPost('apepat_propietario', 'striptags');
                $apepat_propietario = ($apepat_propietario != null and $apepat_propietario != "") ? $apepat_propietario : null;
                $apemat_propietario = $this->request->getPost('apemat_propietario', 'striptags');
                $apemat_propietario = ($apemat_propietario != null and $apemat_propietario != "") ? $apemat_propietario : null;
                $nombres_propietario = $this->request->getPost('nombres_propietario', 'striptags');
                $nombres_propietario = ($nombres_propietario != null and $nombres_propietario != "") ? $nombres_propietario : null;
                $folio_catastral = $this->request->getPost('folio_catastral', 'int');
                $folio_catastral = ($folio_catastral != null and $folio_catastral != "") ? intval($folio_catastral) : null;
                $idestatuscliente = $this->request->getPost('idestatuscliente', 'int');
                $idestatuscliente = ($idestatuscliente != null and $idestatuscliente != "") ? intval($idestatuscliente) : null;
                $latitud = $this->request->getPost('latitud');
                $latitud = ($latitud != null and $latitud != "") ? floatval($latitud) : null;
                $longitud = $this->request->getPost('longitud');
                $longitud = ($longitud != null and $longitud != "") ? floatval($longitud) : null;
                $idtarifa_colonia = $this->request->getPost('idtarifa_colonia', 'int');
                $idtarifa_colonia = ($idtarifa_colonia != null and $idtarifa_colonia != "") ? intval($idtarifa_colonia) : null;
                $idruta = $this->request->getPost('idruta', 'int');
                $idruta = ($idruta != null and $idruta != "") ? intval($idruta) : null;
                $idtipo_contrato = $this->request->getPost('idtipo_contrato', 'int');
                $idtipo_contrato = ($idtipo_contrato != null and $idtipo_contrato != "") ? intval($idtipo_contrato) : null;
                $referencia_ubicacion = $this->request->getPost('referencia_ubicacion', 'striptags');
                $referencia_ubicacion = ($referencia_ubicacion != null and $referencia_ubicacion != "") ? $referencia_ubicacion : null;
                $telefono = $this->request->getPost('telefono', 'int');
                $telefono = str_replace(array("(", ")", " ", "-"), array("", "", "", ""), $telefono);
                $telefono = ($telefono != null and $telefono != "") ? $telefono : null;
                $correo = $this->request->getPost('correo', 'striptags');
                $correo = ($correo != null and $correo != "") ? $correo : null;
                $idtipo_descuento = $this->request->getPost('idtipo_descuento', 'int');
                $idtipo_descuento = ($idtipo_descuento != null and $idtipo_descuento != "") ? intval($idtipo_descuento) : null;
                $ultimo_anio_pago = $this->request->getPost('ultimo_anio_pago', 'int');
                $ultimo_anio_pago = ($ultimo_anio_pago != null and $ultimo_anio_pago != "") ? intval($ultimo_anio_pago) : null;
                $ultimo_mes_pago = $this->request->getPost('ultimo_mes_pago', 'int');
                $ultimo_mes_pago = ($ultimo_mes_pago != null and $ultimo_mes_pago != "") ? intval($ultimo_mes_pago) : null;
                $observacion = $this->request->getPost('observacion', 'striptags');
                $observacion = ($observacion != null and $observacion != "") ? $observacion : null;
                $direccion_otro = $this->request->getPost('direccion_otro', 'striptags');
                $direccion_otro = ($direccion_otro != null and $direccion_otro != "") ? $direccion_otro : null;
                $mensualidad = $this->request->getPost('mensualidad', 'float');
                $mensualidad = ($mensualidad != null and $mensualidad != "") ? floatval($mensualidad) : null;

                //Datosadicionales
                $monto_cobro = $this->request->getPost('monto_cobro', 'float');
                $cobro_manual = $this->request->getPost('cobro_manual');

                //moral
                $nombre_sucursal = $this->request->getPost('nombre_sucursal', 'striptags');
                $nombre_sucursal = ($nombre_sucursal != null and $nombre_sucursal != "") ? $nombre_sucursal : null;
                $contacto_servicio = $this->request->getPost('contacto_servicio', 'striptags');
                $contacto_servicio = ($contacto_servicio != null and $contacto_servicio != "") ? $contacto_servicio : null;
                $contacto_servicio_tel = $this->request->getPost('contacto_servicio_tel', 'int');
                $contacto_servicio_tel = str_replace(array("(", ")", " ", "-"), array("", "", "", ""), $contacto_servicio_tel);
                $contacto_servicio_tel = ($contacto_servicio_tel != null and $contacto_servicio_tel != "") ? $contacto_servicio_tel : null;
                $contacto_servicio_ext = $this->request->getPost('contacto_servicio_ext', 'int');
                $contacto_servicio_ext = ($contacto_servicio_ext != null and $contacto_servicio_ext != "") ? $contacto_servicio_ext : null;
                $contacto_pago1 = $this->request->getPost('contacto_pago1', 'striptags');
                $contacto_pago1 = ($contacto_pago1 != null and $contacto_pago1 != "") ? $contacto_pago1 : null;
                $contacto_pago1_tel = $this->request->getPost('contacto_pago1_tel', 'int');
                $contacto_pago1_tel = str_replace(array("(", ")", " ", "-"), array("", "", "", ""), $contacto_pago1_tel);
                $contacto_pago1_tel = ($contacto_pago1_tel != null and $contacto_pago1_tel != "") ? $contacto_pago1_tel : null;
                $contacto_pago1_ext = $this->request->getPost('contacto_pago1_ext', 'int');
                $contacto_pago1_ext = ($contacto_pago1_ext != null and $contacto_pago1_ext != "") ? $contacto_pago1_ext : null;
                $contacto_pago2 = $this->request->getPost('contacto_pago2', 'striptags');
                $contacto_pago2 = ($contacto_pago2 != null and $contacto_pago2 != "") ? $contacto_pago2 : null;
                $contacto_pago2_tel = $this->request->getPost('contacto_pago2_tel', 'int');
                $contacto_pago2_tel = str_replace(array("(", ")", " ", "-"), array("", "", "", ""), $contacto_pago2_tel);
                $contacto_pago2_tel = ($contacto_pago2_tel != null and $contacto_pago2_tel != "") ? $contacto_pago2_tel : null;
                $contacto_pago2_ext = $this->request->getPost('contacto_pago2_ext', 'int');
                $contacto_pago2_ext = ($contacto_pago2_ext != null and $contacto_pago2_ext != "") ? $contacto_pago2_ext : null;
                $metodo_pago = $this->request->getPost('metodo_pago', 'int');
                $metodo_pago = ($metodo_pago != null and $metodo_pago != "") ? intval($metodo_pago) : null;
                $numero_contrato = $this->request->getPost('numero_contrato', 'striptags');
                $numero_contrato = ($numero_contrato != null and $numero_contrato != "") ? $numero_contrato : null;
                $periodicidad_facturacion = $this->request->getPost('periodicidad_facturacion', 'striptags');
                $periodicidad_facturacion = ($periodicidad_facturacion != null and $periodicidad_facturacion != "") ? $periodicidad_facturacion : null;
                $tipo_facturacion = $this->request->getPost('tipo_facturacion', 'striptags');
                $tipo_facturacion = ($tipo_facturacion != null and $tipo_facturacion != "") ? $tipo_facturacion : null;
                $monto_tasa_fija = $this->request->getPost('monto_tasa_fija', 'float');
                $monto_tasa_fija = ($monto_tasa_fija != null and $monto_tasa_fija != "") ? floatval($monto_tasa_fija) : null;
                $idfacturacion_tamboreo = $this->request->getPost('idfacturacion_tamboreo', 'int');
                $idfacturacion_tamboreo = ($idfacturacion_tamboreo != null and $idfacturacion_tamboreo != "") ? intval($idfacturacion_tamboreo) : null;
                $cantidad_tamboreo = $this->request->getPost('cantidad_tamboreo', 'int');
                $cantidad_tamboreo = ($cantidad_tamboreo != null and $cantidad_tamboreo != "") ? intval($cantidad_tamboreo) : null;

                $idrazon_social = $this->request->getPost('idrazon_social', 'int');
                $idrazon_social = ($idrazon_social != null and $idrazon_social != "") ? intval($idrazon_social) : null;

                $maginado = $this->request->getPost('marginado');
                $abandonado = $this->request->getPost('abandonado');
                $comercio = $this->request->getPost('comercio');
                $privada = $this->request->getPost('privada');
                $nombre_privada = $this->request->getPost('nombre_privada');
                $nombre_comercio = $this->request->getPost('nombre_comercio');


                $validaDireccion = Clientes::validateDireccion($idCliente, $folio_catastral, $calle, $calleLetra, $numero, $numeroLetra, $idcolonia);
                if($validaDireccion->valid()){
                    $this->response->setStatusCode(409, "Conflict");
                    return $this->response;
                }



                $ubicado = ($latitud != null and $longitud != null) ? true : false;

                $clientes->assign(array(
                    'calle' => $calle,
                    'calle_letra' => $calleLetra,
                    'tipo_calle' => $tipoCalle,
                    'numero' => $numero,
                    'numero_letra' => $numeroLetra,
                    'tipo_numero' => $tipoNumero,
                    'cruza1' => $cruza1,
                    'letra_cruza1' => $cruza1Letra,
                    'tipo_cruza1' => $tipoCruza1,
                    'cruza2' => $cruza2,
                    'letra_cruza2' => $cruza2Letra,
                    'tipo_cruza2' => $tipoCruza2,
                    'idcolonia' => $idcolonia,
                    //'localidad' => $localidad,
                    'apepat' => $apepat,
                    'apemat' => $apemat,
                    'nombres' => $nombres,
                    //'apepat_propietario' => $apepat_propietario,
                    //'apemat_propietario' => $apemat_propietario,
                    //'nombres_propietario' => $nombres_propietario,
                    'folio_catastral' => $folio_catastral,
                    //'idestatuscliente' => $idestatuscliente,
                    'activo' => TRUE,
                    'latitud' => $latitud,
                    'longitud' => $longitud,
                    'ubicado' => true,
                    //'idtarifa_colonia' => $idtarifa_colonia,
                    //'idruta' => $idruta,
                    //'idtipo_contrato' => $idtipo_contrato,
                    'referencia_ubicacion' => $referencia_ubicacion,
                    //'fisica' => $this->request->getPost('fisica'),
                    //'telefono' => $telefono,
                    //'correo' => $correo,
                    //'idtipo_descuento' => $idtipo_descuento,
//                    'descuento' => $this->request->getPost('descuento'),
                    //'mensualidad' => $mensualidad,
                    //'ultimo_anio_pago' => $ultimo_anio_pago,
                    //'ultimo_mes_pago' => $ultimo_mes_pago,
                    //'observacion' => $observacion,
//                    'id_usuario' => $idUser,
                    'direccion_otro' => $direccion_otro,
                    'vigente' => true,
                    //'monto_tasa_fija' => $monto_tasa_fija,
                    //'cantidad_tamboreo' => $cantidad_tamboreo,
                    //'nombre_sucursal' => $nombre_sucursal,
                    //'contacto_servicio' => $contacto_servicio,
                    //'contacto_servicio_tel' => $contacto_servicio_tel,
                    //'contacto_servicio_ext' => $contacto_servicio_ext,
                    //'contacto_pago1' => $contacto_pago1,
                    //'contacto_pago1_tel' => $contacto_pago1_tel,
                    //'contacto_pago1_ext' => $contacto_pago1_ext,
                    ///'contacto_pago2' => $contacto_pago2,
                    //'contacto_pago2_tel' => $contacto_pago2_tel,
                    //'contacto_pago2_ext' => $contacto_pago2_ext,
                    //'idmetodo_pago' => $metodo_pago,
                    //'contrato' => $numero_contrato != null ? true : false,
                    //'numero_contrato' => $numero_contrato,
                    //'periodicidad_facturacion' => $periodicidad_facturacion,
                    //'idfacturacion_tamboreo' => $idfacturacion_tamboreo,
                    //'tipo_facturacion' => $tipo_facturacion,
                    'ubicado' => $ubicado,
                    //'idrazon_social' => $idrazon_social,
                    //'cobro_manual' => $cobro_manual,
                    //'monto_cobro' => $monto_cobro,

                    'comercio' => $comercio,
                    'abandonado' => $abandonado,
                    'marginado' => $maginado,
                    'privada' => $privada,
                    'nombre_privada' => $nombre_privada,
                    'nombre_comercio' => $nombre_comercio,
                ));

//                var_dump($clientes->toArray());
//                die;

                if($clientes->save() === false) {
                    foreach ($clientes->getMessages() as $message) {
                        $this->logger->info("(save-dirper): " . $message);
                    }
                    $this->response->setStatusCode(500, "Internal Servere Error");
                }
                else{
                    $persona = "";
                    if($apepat != null and $apepat != ""){
                        $persona .= $apepat;
                    }
                    if($apemat != null and $apemat != ""){
                        if($persona !=  ""){
                            $persona .= " ";
                        }
                        $persona .= $apemat;
                    }
                    if($nombres != null and $nombres != ""){
                        if($persona !=  ""){
                            $persona .= " ";
                        }
                        $persona .= $nombres;
                    }

                    $propietario = "";
                    if($apepat_propietario != null and $apepat_propietario != ""){
                        $propietario .= $apepat_propietario;
                    }
                    if($apemat_propietario != null and $apemat_propietario != ""){
                        if($propietario !=  ""){
                            $propietario .= " ";
                        }
                        $propietario .= $apemat_propietario;
                    }
                    if($nombres_propietario != null and $nombres_propietario != ""){
                        if($propietario !=  ""){
                            $propietario .= " ";
                        }
                        $propietario .= $nombres_propietario;
                    }

                    $vigente = '<i class="fa fa-check" style="color: green" title="Activo"></i>';
                    $calle = ($calle != null and $calle != "") ? trim($calle) : "";
                    $calleLetra = ($calleLetra != null and $calleLetra != "") ? " ".trim($calleLetra) : "";
                    $calle = $calle.$calleLetra;

                    $numero = ($numero != null and $numero != "") ? trim($numero) : "";
                    $numeroLetra = ($numeroLetra != null and $numeroLetra != "") ? " ".trim($numeroLetra) : "";
                    $numero = $numero.$numeroLetra;

                    $cruz1 = ($cruza1 != null and $cruza1 != "") ? trim($cruza1) : "";
                    $cruz1Letra = ($cruza1Letra != null and $cruza1Letra != "") ? trim($cruza1Letra) : "";
                    $cruz1 = $cruz1.$cruz1Letra;

                    $cruz2 = ($cruza2 != null and $cruza2 != "") ? trim($cruza2) : "";
                    $cruz2Letra = ($cruza2Letra != null and $cruza2Letra != "") ? trim($cruza2Letra) : "";
                    $cruz2 = $cruz2.$cruz2Letra;

                    $colonia = "";
                    if($idcolonia != null and $idcolonia != ""){
                        $eColonia = Colonias::findFirstById($idcolonia);
                        if($eColonia !== false){
                            $colonia = $eColonia->nombre.", ".$eColonia->localidad;
                        }
                    }

                    $resp->vigente = $vigente;
                    $resp->id = $clientes->id_cliente;
                    $resp->calle = $calle;
                    $resp->numero = $numero;
                    $resp->cruz1 = $cruz1;
                    $resp->cruz2 = $cruz2;
                    $resp->colonia = $colonia;
                    $resp->persona = $persona;
                    $resp->propietario = $propietario;
                    $this->response->setContent(json_encode($resp));
                }
            }
            else{
                $this->response->setStatusCode(400, "Bad Request");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    public function uploadAction(){
        ini_set("post_max_size", "10M");
        ini_set("upload_max_filesize", "10M");
        ini_set("memory_limit", "20000M");

        $this->view->disable();

        if ($this->request->isPost() == true) {
            $idg = $this->request->getPost("idcliente");
            $idtipoiden = $this->request->getPost("idtipoiden");
            $idtipocomp = $this->request->getPost("idtipocomp");

            if ($this->request->hasFiles() == true) {
                $baseLocation = $this->config->application['imgRequisitos'];

                $file = $this->request->getUploadedFiles();
                $file = $file[0];
                $split = explode(".",$file->getName());
                $ext = $split[count($split) - 1];

                $name = $idg."_".time()."_".rand(1,1000).".".$ext;
                $resp = new \stdClass();

                if($file->moveTo(BASE_DIR."/public".$baseLocation.$name)){
                    $cliente = Clientes::findFirstById_cliente($idg);

                    //$cctmp = new ManagementImages();
                    if($cliente){
                        $nombre = "/public".$baseLocation.$name;
                        if($idtipoiden != null){
                            if($cliente->ruta_identificacion != ""){
                                unlink(BASE_DIR.$cliente->ruta_identificacion);
                            }

                            $cliente->id_tipo_identificacion = $idtipoiden;
                            $cliente->ruta_identificacion = $nombre;

                        }
                        else {
                            if ($idtipocomp != null) {
                                if($cliente->ruta_comprobante != ""){
                                    unlink(BASE_DIR.$cliente->ruta_comprobante);
                                }

                                $cliente->id_tipo_comprobante = $idtipocomp;
                                $cliente->ruta_comprobante = $nombre;
                            }
                            else {
                                if($cliente->ruta_foto_predio != ""){
                                    unlink(BASE_DIR.$cliente->ruta_foto_predio);
                                }

                                $cliente->ruta_foto_predio = $nombre;
                            }
                        }
                    }


                    if($cliente->save() === true){
                        $datos = array(
                            "id" => $cliente->id_cliente,
                            "url" => $baseLocation,
                            "archivo" => $name
                        );
                        $this->response->setContent(json_encode($datos));
                    }
                    else{
                        unlink($baseLocation.$name);
                        $this->response->setStatusCode(500, "No se pudo guardar el archivo");
                    }
                }
                else{
                    $this->response->setStatusCode(500, "No se pudo guardar el archivo");
                }
            }
            else{
                $this->response->setStatusCode(400);
            }
        }
        else{
            $this->response->setStatusCode(405);
        }
        return $this->response;
    }

    public function antiguosAction($id = null){
        if($this->request->isGet()){
            if(!empty($id)){
                $objC = Clientes::findFirstById_cliente($id);
                if($objC){
                    $this->response->setContent(json_encode(
                        array(
                            'anio' => $objC->ultimo_anio_pago,
                            'mes' => $objC->ultimo_mes_pago,
                        )
                    ));
                    return $this->response;
                }
            }
            else{
                $this->view->setTemplateBefore('public');
            }
        }
        else{
            $objC = Clientes::findFirstById_cliente($id);
            if($objC){
                $json = $this->request->getJsonRawBody();
                $objC->ultimo_anio_pago = $json->anio;
                $objC->ultimo_mes_pago = $json->mes;
                if(!$objC->save()){
                    foreach ($objC->getMessages() as $message) {
                        $this->logger->error("save-update-mes-anio: ".$message->getMessage());
                    }
                    $this->response->setStatusCode(500);
                    return $this->response;
                }
            }
        }
    }

    public function bitmensAction($id){
        $view = clone $this->view;
        $this->view->disable();
        $view->start();

        $view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $sql = "select * from comun.view_bitacora_mensualidades where cliente = ".$id;
        $bit = GenericSQL::getBySQL($sql);
        $view->setVar("bitacora", $bit);
        $view->render('clientes', 'mensualidad/bitacora');
        $view->finish();
        $this->response->setContent($view->getContent());
        return $this->response;
    }

    public function segdescpenAction($id){
        $view = clone $this->view;
        $this->view->disable();
        $view->start();

        $view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $sql = "select cp.id, u.usuario, cp.activo, meses.meses, cp.archivo, cp.tipo_archivo, 
        to_char(cp.fecha_creacion, 'DD/MM/YYYY') as fecha, to_char(cp.fecha_cancelacion, 'DD/MM/YYYY') as fecha_cancelacion,
        uc.usuario usuario_cancelacion, cp.motivo_cancelacion, cp.idpago, meses.mes, meses.anio min_anio, LEFT(meses.mes, 1) min_mes
        from cliente.cliente_descuento_pensionado cp
        left join usuario.usuario u on cp.idusuario = u.id
        left join usuario.usuario uc on cp.idusuario_cancelacion = uc.id
        left join (
            select idpago, string_agg(strmes|| '-' ||anio, ',') meses, string_agg(mes::text, ',') mes, min(anio) as anio from (
                select idpago,
                case 
                    when mes = '1' then 'ENERO'
                    when mes = '2' then 'FEBRERO'
                    when mes = '3' then 'MARZO'
                    when mes = '4' then 'ABRIL'
                    when mes = '5' then 'MAYO'
                    when mes = '6' then 'JUNIO'
                    when mes = '7' then 'JULIO'
                    when mes = '8' then 'AGOSTO'
                    when mes = '9' then 'SEPTIEMBRE'
                    when mes = '10' then 'OCTUBRE'
                    when mes = '11' then 'NOVIEMBRE'
                    when mes = '12' then 'DICIEMBRE'
                end strmes, mes, anio 
                from cliente.historial_pago
				where idcliente = $id
                order by idpago, anio, mes) t1
            group by idpago ) meses on cp.idpago = meses.idpago
        where cp.id_cliente = $id
		order by min_anio desc, min_mes desc";
        $bit = GenericSQL::getBySQL($sql);
        $view->setVar("bitdescpen", $bit);
        $view->render('clientes', 'segdescpen');
        $view->finish();
        $this->response->setContent($view->getContent());
        return $this->response;
    }

    public function infosegstaAction($id){
        $view = clone $this->view;
        $this->view->disable();
        $view->start();

        $view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $sql = "select u.usuario, ec.nombre estatus, to_char(b.fecha_creacion, 'DD/MM/YYYY HH24:MI') as fecha, b.origen, b.motivo 
        from comun.bitacora_estatus b
        left join usuario.usuario u on b.idusuario = u.id
        left join cliente.estatus_cliente ec on b.idestatus = ec.id
        where b.idcliente = ".$id."
        order by b.fecha_creacion desc";
        $bit = GenericSQL::getBySQL($sql);
        $view->setVar("bitacora", $bit);
        $view->render('clientes', 'estatus/bitacora');
        $view->finish();
        $this->response->setContent($view->getContent());
        return $this->response;
    }

    public function editstaAction(){
        $json = $this->request->getJsonRawBody();
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];

        $response = Estatus_cliente::fullSave($json->idcliente, $idUser, "WEB", $json);
        if($response->error) {
            $this->response->setStatusCode($response->errorCode);
        }

        return $this->response;
    }

    public function clichgmensAction($idcliente){
        $this->view->disable();
        $oCliente = Clientes::findFirstById_cliente($idcliente);
        $dataOrigin = json_encode($oCliente);
        $rawBody = $this->request->getJsonRawBody();
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];
        $this->db->begin();

        $oCliente->cobro_manual = true;
        $oCliente->mensualidad = $rawBody->monto;
        $oCliente->fecha_modificacion = date("c");
        if(!$oCliente->save()){
            $this->db->rollback();
            $this->response->setStatusCode(500);
            foreach ($oCliente->getMessages() as $message) {
                $this->logger->info("(save-actualizar-mensualidad): " . $message);
            }
        }
        else{
            $dataB = new BitacoraCambios();
            $dataB->identificador = $idcliente;
            $dataB->modulo = 'CLIENTE';
            $dataB->accion = 'ACTUALIZAR MENSUALIDAD';
            $dataB->idusuario = $idUser;
            $dataB->tabla = "cliente.cliente";
            $dataB->cambios = json_encode($oCliente);
            $dataB->original = $dataOrigin;
            $dataB->apartado = "WEB";
            $dataB->motivo = $rawBody->motivo;

            if (!$dataB->save()) {
                foreach ($dataB->getMessages() as $message) {
                    $this->logger->info("(apiv2-actualizar-mensualdiad-bitacora): " . $message);
                }
                $this->response->setStatusCode(500);
                $this->db->rollback();
            }
            else{
                $this->db->commit();
            }
        }
        return $this->response;
    }

    public function validarutaAction($idcliente){
        $this->view->disable();
        $oCliente = Clientes::findFirstById_cliente($idcliente);
        $dataOrigin = json_encode($oCliente);
        $rawBody = $this->request->getJsonRawBody();
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];
        $this->db->begin();

        $oCliente->idruta = $rawBody->idruta;
        $oCliente->ruta_validada = true;
        $oCliente->observaciones_valida_ruta = $rawBody->observaciones;
        $oCliente->fecha_ruta_validada = date("c");
        $oCliente->idusaurio_valida_ruta = $idUser;
        $oCliente->fecha_modificacion = date("c");
        if(!$oCliente->save()){
            $this->db->rollback();
            $this->response->setStatusCode(500);
            foreach ($oCliente->getMessages() as $message) {
                $this->logger->info("(save-validar-ruta): " . $message);
            }
        }
        else{
            $dataB = new BitacoraCambios();
            $dataB->identificador = $idcliente;
            $dataB->modulo = 'CLIENTE';
            $dataB->accion = 'VALIDAR RUTA';
            $dataB->idusuario = $idUser;
            $dataB->tabla = "cliente.cliente";
            $dataB->cambios = json_encode($oCliente);
            $dataB->original = $dataOrigin;
            $dataB->apartado = "WEB";
            $dataB->motivo = $rawBody->motivo;

            if (!$dataB->save()) {
                foreach ($dataB->getMessages() as $message) {
                    $this->logger->info("(clientes-validar-ruta-bitacora): " . $message);
                }
                $this->response->setStatusCode(500);
                $this->db->rollback();
            }
            else{
                $this->db->commit();
            }
        }
        return $this->response;
    }
}