<?php
namespace Vokuro\Controllers;

use GuzzleHttp\Client;

class MapsController extends GeoserverControllerBase
{

    /**
     * Default action.
     * Se deshabilita las vistas
     */
    public function initialize(){
        $this->view->disable();
    }

    public function indexAction(){
        $url = $this->config->application->urlWMS;
        $response = $this->response;

        $geoserverUser = $this->config->geoserver->user;
        $geoserverPass = $this->config->geoserver->password;


        $client = new Client();
        $respcli = $client->request('GET', $url, [
            'debug' => false,
            'http_errors' => false,
            'verify' => false,
            'query' => $_GET,
            'headers' => [
                'Authorization'     => 'Basic '.base64_encode($geoserverUser.":".$geoserverPass)
            ]
        ]);

        if($respcli->getStatusCode() != '200'){
            $response->setStatusCode($respcli->getStatusCode());
        }
        else{
            $body = (string) $respcli->getBody();
            $contentType = $respcli->getHeader('content-type')[0];

            $response->setHeader('Content-Type', $contentType);
            $response->setContent($body);
        }

        $response->send();
    }
}