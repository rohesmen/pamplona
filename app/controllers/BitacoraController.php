<?php
namespace Vokuro\Controllers;

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Mvc\View;
use Vokuro\Models\Unidad;
use Vokuro\Models\Chofer;
use Vokuro\Models\Recolector;
use Vokuro\Models\Turno;
use Vokuro\Models\Bitacora;
use Vokuro\Models\Users;
use Vokuro\Models\Supervisor;
use Vokuro\Models\Coordinador;

/**
 * Vokuro\Controllers\BitacoraController
 * CRUD to manage bitacora
 */
class BitacoraController extends ControllerBase
{

    public function initialize()
    {
        $this->view->setTemplateBefore('public');
    }

    /**
     * Default action, shows the search form
     */
    public function indexAction()
    {
    }
	
	public function bitacoraAction()
    {
		if ($this->request->isPost() == true) {
            $rawBody = $this->request->getJsonRawBody();
			$resp = new \stdClass();
			$bitacora = array();
			$fecha = date('Y/m/d', date_create_from_format("d/m/Y",$rawBody->fecha)->getTimestamp());
			
			//$this->response->setContent(json_encode($fecha));
			//return $this->response;
			
			$unidades = Unidad::findByActivo();
			$choferes = Chofer::findByActivo();
			$recolectores = Recolector::findByActivo();
			$turnos = Turno::findByActivo();
            $supervisores = Supervisor::findByActivo();
            $coordinadores = Coordinador::findByActivo();
			
			$bitacorasToday = Bitacora::findByDate($fecha);
			$yesterday = date('Y/m/d', strtotime($fecha . ' -1 day'));
			
			$bitacorasYesterday = Bitacora::findByDate($yesterday);
			
			//$this->response->setContent(json_encode($bitacorasYesterday));
			//return $this->response;
			
			$bitacora = $this->MergetBitacoraUnidades($bitacorasToday, $bitacorasYesterday, $unidades, $fecha);
			
			$recoResult = array();
			foreach ($recolectores as $key => $r){
				$rTemp = new \stdClass();
				$rTemp->id = $r->id;
				$rTemp->nombre = $r->nombres." ".$r->apepat." ".$r->apemat;
				array_push($recoResult, $rTemp);
			}

            $turnosResult = array();
            foreach ($turnos as $key => $r){
                $rTemp = new \stdClass();
                $rTemp->id = $r->id;
                $rTemp->nombre = $r->nombre;
                array_push($turnosResult, $rTemp);
            }
			
			$choferResult = array();
			foreach ($choferes as $key => $c){
				$rTemp = new \stdClass();
				$rTemp->id = $c->id;
				$rTemp->nombre = $c->nombres." ".$c->apepat." ".$c->apemat;
				array_push($choferResult, $rTemp);
			}

            $supervisoresResult = array();
            foreach ($supervisores as $key => $c){
                $rTemp = new \stdClass();
                $rTemp->id = $c->id;
                $rTemp->nombre = $c->nombres." ".$c->apepat." ".$c->apemat;
                array_push($supervisoresResult, $rTemp);
            }

            $coordinadoresResult = array();
            foreach ($coordinadores as $key => $c){
                $rTemp = new \stdClass();
                $rTemp->id = $c->id;
                $rTemp->nombre = $c->nombres." ".$c->apepat." ".$c->apemat;
                array_push($coordinadoresResult, $rTemp);
            }
			
			
			
			$resp->bitacora = $bitacora;
			$resp->choferes = $choferResult;
			$resp->turnos = $turnosResult;
			$resp->recolectores = $recoResult;
            $resp->supervisores = $supervisoresResult;
            $resp->coordinadores = $coordinadoresResult;
			
			$this->response->setContent(json_encode($resp));
			return $this->response;
        }
    }
	
	private function MergetBitacoraUnidades($bitacoraToday, $bitacoraYesterday, $unidades, $fecha){
		date_default_timezone_set('UTC');
		date_default_timezone_set("America/Mexico_City");
		$dias = array('Lunes','Martes','Miércoles','Jueves','Viernes','Sábado', 'Domingo');

		$fechaDb = date('d/m/Y', date_create_from_format("Y/m/d",$fecha)->getTimestamp());
		
		//$user = $this->auth->getUser();
		$bitacoraFinal = array();
		foreach ($bitacoraYesterday as $key => $b){
			
			$bToday = null;
			foreach ($bitacoraToday as $key2 => $b2){
				if($b->idunidad == $b2->idunidad){
					$bToday = $b2;
					break;
				}
			}
			
			//$fechaDb = date('d/m/Y', date_create_from_format("Y-m-d",$b->fecha)->getTimestamp());
			
			$aux = new \stdClass();
			$aux->fecha = $fechaDb;
			$aux->dia =  $dias[date('N', strtotime($fecha)) - 1];
			$aux->idunidad = $b->idunidad;
			
			if(isset($bToday) && $bToday != null){
				$aux->id = $bToday->id;
				$aux->idturno = $bToday->idturno;
				$aux->horasalida = $bToday->horasalida;
				$aux->horaregreso = $bToday->horaregreso;
				$aux->totalhoras = $bToday->totalhoras;
				$aux->tiempomuerto = $bToday->tiempomuerto;
				$aux->kilometraje = $bToday->kilometraje;
				$aux->vueltasrelleno = $bToday->vueltasrelleno;
                $aux->idsupervisor = $bToday->idsupervisor;
                $aux->idcoordinador = $bToday->idcoordinador;
				$aux->idchofer = $bToday->idchofer;
				$aux->idrecolector1 = $bToday->idrecolector1;
				$aux->idrecolector2 = $bToday->idrecolector2;
				$aux->idrecolector3 = $bToday->idrecolector3;
				$aux->observaciones = $bToday->observaciones;
				$aux->idusuario = $bToday->idusuario;
				$userBd = Users::findFirstById($bToday->idusuario);
				if($userBd != null){
					$aux->usuario = $userBd->usuario;
				}
			}else{
				$aux->id = 0;
				$aux->idturno = $b->idturno;
				$aux->horasalida = $b->horasalida;
				$aux->horaregreso = $b->horaregreso;
				$aux->totalhoras = $b->totalhoras;
				$aux->tiempomuerto = $b->tiempomuerto;
				$aux->kilometraje = $b->kilometraje;
				$aux->vueltasrelleno = $b->vueltasrelleno;
				$aux->idchofer = $b->idchofer;
                $aux->idsupervisor = $b->idsupervisor;
                $aux->idcoordinador = $b->idcoordinador;
				$aux->idrecolector1 = $b->idrecolector1;
				$aux->idrecolector2 = $b->idrecolector2;
				$aux->idrecolector3 = $b->idrecolector3;
				$aux->observaciones = $b->observaciones;
				//$aux->idusuario = $user->id;
				//$aux->usuario = $user->usuario;				
			}
			
			foreach ($unidades as $key2 => $unidad){
				if($unidad->id == $b->idunidad){
					$aux->unidad = $unidad->nombre;
					break;
				}
			}
			array_push($bitacoraFinal, $aux);
		}
		
		foreach ($bitacoraToday as $key => $b){
			
			$finded = false;
			foreach ($bitacoraFinal as $key2 => $b2){
				if($b->idunidad == $b2->idunidad){
					$finded = true;
					break;
				}
			}
			
			if(!$finded){
				$aux = new \stdClass();
				$aux->id = $b->id;
				$aux->fecha = $fechaDb;
				$aux->dia =  $dias[date('N', strtotime($fecha)) - 1];
				$aux->idunidad = $b->idunidad;
				$aux->idturno = $b->idturno;
				$aux->horasalida = $b->horasalida;
				$aux->horaregreso = $b->horaregreso;
				$aux->totalhoras = $b->totalhoras;
				$aux->tiempomuerto = $b->tiempomuerto;
				$aux->kilometraje = $b->kilometraje;
				$aux->vueltasrelleno = $b->vueltasrelleno;
				$aux->idchofer = $b->idchofer;
                $aux->idsupervisor = $b->idsupervisor;
                $aux->idcoordinador = $b->idcoordinador;
				$aux->idrecolector1 = $b->idrecolector1;
				$aux->idrecolector2 = $b->idrecolector2;
				$aux->idrecolector3 = $b->idrecolector3;
				$aux->observaciones = $b->observaciones;
				$aux->idusuario = $b->idusuario;
				
				$userBd = Users::findFirstById($b->idusuario);
				if($userBd != null){
					$aux->usuario = $userBd->usuario;
				}
				
				foreach ($unidades as $key2 => $unidad){
					if($unidad->id == $b->idunidad){
						$aux->unidad = $unidad->nombre;
						break;
					}
				}
				array_push($bitacoraFinal, $aux);
			}
		}
		
		foreach ($unidades as $key => $unidad){
			$finded = false;
			foreach ($bitacoraFinal as $key2 => $b){
				if($unidad->id == $b->idunidad){
					$finded = true;
				}
			}
			
			if(!$finded){
				$aux = new \stdClass();
				$aux->id = 0;
				$aux->fecha = $fechaDb;
				$aux->dia =  $dias[date('N', strtotime($fecha)) - 1];
				$aux->unidad = $unidad->nombre;
				$aux->idunidad = $unidad->id;
				$aux->idturno = 0;
				$aux->horasalida = "";
				$aux->horaregreso = "";
				$aux->totalhoras = "";
				$aux->kilometraje = "";
				$aux->vueltasrelleno = "";
				$aux->tiempomuerto = 0;
				$aux->idchofer = 0;
                $aux->idsupervisor = 0;
                $aux->idcoordinador = 0;
				$aux->idrecolector1 = 0;
				$aux->idrecolector2 = 0;
				$aux->idrecolector3 = 0;
				$aux->observaciones = "";
				//$aux->idusuario = $user->id;
				//$aux->usuario = $user->usuario;
				array_push($bitacoraFinal, $aux);
			}
		}
		
		return $bitacoraFinal;
	}
	
	public function saveAction(){
		$this->view->disable();
		
		if ($this->request->isPost() == true) {
			$user = $this->auth->getUser();
			$rawBody = $this->request->getJsonRawBody();
			if($rawBody->id == 0){
				$fecha = date('Y/m/d', date_create_from_format("d/m/Y",$rawBody->fecha)->getTimestamp());
				
				
				$bitacora = new Bitacora();
				$bitacora->fecha = $fecha;
				$bitacora->dia = $rawBody->dia;
				$bitacora->idunidad = $rawBody->idunidad;
				$bitacora->idturno = $rawBody->idturno;
				$bitacora->horasalida = $rawBody->horasalida == "" ? "00:00" : $rawBody->horasalida;
				$bitacora->horaregreso = $rawBody->horaregreso == "" ? "00:00" : $rawBody->horaregreso;
				$bitacora->totalhoras = $rawBody->totalhoras == "" ? "00:00" : $rawBody->totalhoras;
				$bitacora->tiempomuerto = $rawBody->tiempomuerto;
				$bitacora->kilometraje = $rawBody->kilometraje;
				$bitacora->vueltasrelleno = $rawBody->vueltasrelleno;
				$bitacora->idchofer = $rawBody->idchofer;
                $bitacora->idsupervisor = $rawBody->idsupervisor;
                $bitacora->idcoordinador = $rawBody->idcoordinador;
				$bitacora->idrecolector1 = $rawBody->idrecolector1;
				$bitacora->idrecolector2 = $rawBody->idrecolector2;
				$bitacora->idrecolector3 = $rawBody->idrecolector3;
				$bitacora->observaciones = $rawBody->observaciones;
				$bitacora->idusuario = $user->id;
				$bitacora->activo = true;
				$bitacora->fechacreacion = date("c");
				$bitacora->fechamodificacion = $bitacora->fechacreacion;
			}else{
				$bitacora = Bitacora::findFirstById($rawBody->id);
				$bitacora->idturno = $rawBody->idturno;
				$bitacora->horasalida = $rawBody->horasalida == "" ? "00:00" : $rawBody->horasalida;
				$bitacora->horaregreso = $rawBody->horaregreso == "" ? "00:00" : $rawBody->horaregreso;
				$bitacora->totalhoras = $rawBody->totalhoras == "" ? "00:00" : $rawBody->totalhoras;
				$bitacora->tiempomuerto = $rawBody->tiempomuerto;
				$bitacora->kilometraje = $rawBody->kilometraje;
				$bitacora->vueltasrelleno = $rawBody->vueltasrelleno;
				$bitacora->idchofer = $rawBody->idchofer;
                $bitacora->idsupervisor = $rawBody->idsupervisor;
                $bitacora->idcoordinador = $rawBody->idcoordinador;
				$bitacora->idrecolector1 = $rawBody->idrecolector1;
				$bitacora->idrecolector2 = $rawBody->idrecolector2;
				$bitacora->idrecolector3 = $rawBody->idrecolector3;
				$bitacora->observaciones = $rawBody->observaciones;
			}
			
			if($bitacora->save()){
				$saveResult->ok = true;
				$saveResult->status = 200;
				$saveResult->id = $bitacora->id;
				$saveResult->data = $bitacora;
				$this->response->setStatusCode(200, "");
				$this->response->setContent(json_encode($saveResult));
			} else {
				$this->response->setStatusCode(500, "Internal Server Error");
			}
		} else {
            $this->response->setStatusCode(501, "Not Implemented");
        }
		
		return $this->response;
	}
}
