<?php
namespace Vokuro\Controllers;

//use Phalcon\Di;
use Exception;
use GuzzleHttp\Client;
use Vokuro\GenericSQL\GenericSQL;

use Phalcon\Mvc\View;


use Vokuro\Models\Clientes;
use Vokuro\Models\ColoniaHorarios;
use Vokuro\Models\Colonias;
use Vokuro\Models\EstadoOrdenServicio;
use Vokuro\Models\Etapa;
use Vokuro\Models\Foliador;
use Vokuro\Models\Folios;
use Vokuro\Models\GrupoServicios;
use Vokuro\Models\HistorialPago;
use Vokuro\Models\HistorialPagoAux;
use Vokuro\Models\MunicipiosYucatan;
use Vokuro\Models\OrdenServicio;
use Vokuro\Models\OrdenServicioDetalle;
use Vokuro\Models\Pagos;
use Vokuro\Models\PagosAux;
use Vokuro\Models\Parameters;
use Vokuro\Models\Promociones;
use Vokuro\Models\Queja;
use Vokuro\Models\Seguimiento;
use Vokuro\Models\SeguimientoGStrackMe;
use Vokuro\Models\Servicios;
use Vokuro\Models\Users\Application;
use Vokuro\Models\Users\Device;
use Vokuro\Models\Users\Session;
use Vokuro\Models\Users\Users;
use Vokuro\Models\Users\Permissions;
use Phalcon\Mvc\Model\Query;

use Vokuro\Forms\LoginForm;
use Vokuro\Forms\SignUpForm;
use Vokuro\Forms\ForgotPasswordForm;
use Vokuro\Auth\Exception as AuthException;
//use Vokuro\Models\Users;
use Vokuro\Models\ResetPasswords;




use Swift_Message as Message;
use Swift_SmtpTransport as Smtp;

use html2pdf;
use Vokuro\Util\NumeroLetras;
use Vokuro\Util\Util;

/**
 * Display the default index page.
 */
class ApiController extends ControllerBase{
    private $IDUSUARIO = 88;

    public function initialize()
    {
        $this->view->disable();
    }

    public function syncembsoftAction(){
        $this->view->disable();

        try {
            //http://vindenweb.com/api/dgty/units_location
            $token = "5c37f160fbb311eb9154b386164d67be";

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://gstrackme.mx/api/v1/devicesManagement/Vehicles/pamplona-svc',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_SSL_VERIFYHOST => 2,  // Verificar el host SSL
                CURLOPT_SSL_VERIFYPEER => 1,  // Verificar el certificado SSL
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => [
                    'x-api-key: Johs1iVHaZE=0f1ddbbed4644ee49f176f578450b4e7'
                ],
            ));

            $result = curl_exec($curl);
            if($result){
//        "status": "En geocerca"
                $body = json_decode($result);
                $this->logger->info($result);
                foreach ($body as $b){
                    $seg = new SeguimientoGStrackMe();
                    $seg->vehiculo = $b->number;
                    $seg->latitud = $b->lat;
                    $seg->longitud = $b->lng;
                    $seg->fecha = $b->lastUpdate;
                    $seg->nombre_conductor = $b->driver;
                    $seg->placas = $b->plate;
                    $seg->foto_conductor = $b->photo;
                    $seg->estado = $b->status;

                    if(!$seg->save()){
                        foreach ($seg->getMessages() as $message) {
                            $this->logger->error("save-diagrama-persona_encuesta: ".$message->getMessage());
                        }
                    }
                }

            }
            else{
                $this->response->setStatusCode(500);
                $this->response->setContent("Ocurrió un error al procesar la información, favor de contactar al administrador");
            }

            curl_close($curl);

        } catch (Exception $e) {
            $this->logger->error($e->getMessage() . "\n " . $e->getTraceAsString());
            $this->response->setContent("Ocurrió un error al procesar la información, favor de contactar al administrador");
            $this->response->setStatusCode(500);
            return $this->response;
        }

        return $this->response;
    }

    public function dispositivosAction($id = '', $aplicacion = '')
    {
        $single = !empty($id);
        $device = null;
        if(empty($id) || empty($aplicacion)){
            $this->response->setStatusCode(400);
            return $this->response;
        }
        if($single){
            $device = Device::get($id,$aplicacion);

            if($this->request->isGet()){
                $this->response->setJsonContent($device);
            }else{
                $this->response->setStatusCode(405);
            }
        }else{
            $this->response->setStatusCode(405);
        }

        return $this->response;
    }

    public function usuariosAction($id = '')
    {
        $single = !empty($id);
        $device = null;
        if($single){
            $identity = $this->auth->getIdentity();
            if($id == $identity['id'] || $id == 'actual'){
                $entity = $id === 'actual'? Users::findFirstById($identity['id']) : Users::findFirstById($id);

                if($this->request->isGet()){
                    if($entity != false){
                        $this->response->setJsonContent($entity);
                    }else{
                        $this->response->setStatusCode(404);
                    }
                }else{
                    $this->response->setStatusCode(405);
                }
            }else{
                $this->response->setStatusCode(403);
            }

        }else{
            $this->response->setStatusCode(405);
        }

        return $this->response;
    }

    public function sesionesAction($id = '')
    {
        $single = !empty($id);
        $device = null;
        if($single){

            $this->logger->info($id.' <=5=> '.$this->session->getId());
                if($this->request->isHead()){
                    $identity = $this->auth->getIdentity();

                    if($id == $this->session->getId() && is_array($identity)){
                        $this->logger->info($id.' bien session head');
                        $this->response->setStatusCode(204);
                    }else{
                        $this->logger->info($id.' mal session head');
                        $this->response->setStatusCode(404);
                    }
                }elseif($this->request->isGet()){
                    if($id == $this->session->getId()){
                        $this->logger->info($id.' bien session get');
                        $this->response->setJsonContent($_SESSION);
                    }else{
                        $this->logger->info($id.' mal session get');
                        $this->response->setStatusCode(404);
                    }
                }elseif($this->request->isDelete()){
                    if($id == $this->session->getId()){
                        $this->logger->info($id.' <=6=> '.$this->session->getId());
                        $this->session->destroy();
                        $this->response->setStatusCode(204);
                    }else{
                        $this->response->setStatusCode(404);
                    }
                }
        }else{
            if($this->request->isPost()) {//login
                $credentials = $this->request->getJsonRawBody();
                $this->logger->info($id.' <=7=> '.$this->session->getId());
                try {
                    $this->auth->check([
                        'user' => $credentials->user,
                        'password' => $credentials->password,
                        'application' => $credentials->application,
                        'device' => $credentials->device
                    ]);
                    $this->logger->info($id.' <=8=> '.$this->session->getId());
                } catch (AuthException $e) {
                    $this->response->setStatusCode(401);
                }

                $this->response->setHeader("Token", session_id());
                $this->logger->info($id.' <=9=> '.$this->session->getId());
            }else{
                $this->response->setStatusCode(405);
            }
        }

        return $this->response;
    }

    public function appAction($aplicacion, $subdominio, $sSubdominio){
        if(isset($aplicacion) && !empty($aplicacion)) {
            $app = Application::findFirstByLlave($aplicacion);
            if($app) {
                $idaplicacion = $app->id;
                $idEn = null;
                if ($this->request->hasQuery("idenc") && !empty($this->request->getQuery("idenc"))) {
                    $idEn = $this->request->getQuery("idenc");
                }
                if (empty($subdominio)) {
                    $this->response->setStatusCode(404);
                } elseif ($subdominio == "vigente") {
                    $vig = Apks::getVigente($idaplicacion, $idEn);
                    if ($vig->count() > 0) {
                        $vig = $vig->getFirst();
                        $this->response->setContent(json_encode($vig));
                    } else {
                        $this->response->setStatusCode(404);
                    }
                } elseif ($subdominio == "descargar") {
                }
                else {
                    $this->response->setStatusCode(400);
                }
            }
            else{
                $this->response->setStatusCode(404);
            }
        }
        else{
            $this->response->setStatusCode(400);
        }

        return $this->response;
    }

    public function coloniasAction($id = null){
        if($this->request->isGet()){
            if(isset($id)){
                if(!empty($id) && is_numeric($id)){
                    $this->response->setContent(json_encode(Colonias::findFirstById($id)));
                }
                else{
                    $this->response->setStatusCode(404);
                }
            }
            else{
                $this->response->setContent(json_encode(Colonias::findByActivo()));
            }
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    public function clientesAction(){
        if($this->request->isGet()){
            if($this->request->has("calle")
                && $this->request->has("numero")
                && $this->request->has("colonia")
            ){
                $calle = $this->request->get("calle");
                $numero = $this->request->get("numero");
                $calle_letra = $this->request->get("calle_letra");
                $numero_letra = $this->request->get("numero_letra");
                $colonia = $this->request->get("colonia");

//                $this->logger->info(json_encode($this->request));

                if(empty($calle) || empty($numero)){
                    $this->response->setStatusCode(400, "Calle, número y colonia son requeridos");
                }
                else{
                    $this->logger->info($calle_letra);
                    $wcl = "";
                    $wnl = "";
                    $wcol = "";
                    if($this->request->has("calle_letra") && !empty(trim($calle_letra))){
                        $wcl = " and regexp_replace(trim(lower(calle_letra)), ' ', '', 'g') = regexp_replace('".strtolower( $calle_letra)."', '\s', '', 'g')";
                    }
                    else{
                        $wcl = ' and calle_letra is null';
                    }
                    if($this->request->has("numero_letra") && !empty($numero_letra)){
                        $wnl = " and regexp_replace(trim(lower(numero_letra)), ' ', '', 'g') = regexp_replace('".strtolower ($numero_letra)."', '\s', '', 'g')";
                    }
                    else{
                        $wnl = ' and numero_letra is null';
                    }
                    if($this->request->has("colonia") && !empty($colonia)){
                        $wcol = " and idcolonia = $colonia";
                    }
                    $sql = "select 
                        id_cliente id, 
                        coalesce(upper(trim(calle)), '') || coalesce(' ' || upper(trim(calle_letra)), '') calle,
                        coalesce(upper(trim(numero)), '') || coalesce(' ' || upper(trim(numero_letra)), '') numero,
                        coalesce(upper(trim(cruza1)), '') || coalesce(' ' || upper(trim(letra_cruza1)), '') cruz1,
                        coalesce(upper(trim(cruza2)), '') || coalesce(' ' || upper(trim(letra_cruza2)), '') cruz2,
                        col.nombre colonia,
                        c.ultimo_mes_pago::integer ultimomes,
                        c.ultimo_anio_pago ultimoanio,
                        case 
                    when ultimo_mes_pago = '1' then 'Enero'
                    when ultimo_mes_pago = '2' then 'Febrero'
                    when ultimo_mes_pago = '3' then 'Marzo'
                    when ultimo_mes_pago = '4' then 'Abril'
                    when ultimo_mes_pago = '5' then 'Mayo'
                    when ultimo_mes_pago = '6' then 'Junio'
                    when ultimo_mes_pago = '7' then 'Julio'
                    when ultimo_mes_pago = '8' then 'Agosto'
                    when ultimo_mes_pago = '9' then 'Septiembre'
                    when ultimo_mes_pago = '10' then 'Octubre'
                    when ultimo_mes_pago = '11' then 'Noviembre'
                    when ultimo_mes_pago = '12' then 'Diciembre' end txtultimomes, 
                        c.idcolonia
                    from cliente.cliente c
                    left join cliente.colonia col on c.idcolonia = col.id 
                    where c.activo = true and c.vigente = true and c.comercio = false 
                    and regexp_replace(trim(lower(calle)), ' ', '', 'g') = regexp_replace('".strtolower ($calle)."', '\s', '', 'g') 
                    and regexp_replace(trim(lower(numero)), ' ', '', 'g') = regexp_replace('".strtolower  ($numero)."', '\s', '', 'g') 
                    $wcl $wnl $wcol";
                    $this->logger->info($sql);
                    $qs = GenericSQL::getBySQL($sql);

                    foreach ($qs as $q){

                        $ResultadoAdeudo = 0;
                        //si hay ultimo año y ultimo mes se calcula el adeudo
                        if ($q->ultimoanio > 0 && $q->ultimomes > 0) {
                            if($q->ultimoanio == date("Y")) {
                                $ResultadoAdeudo = (date("m") - $q->ultimomes) * $q->mensualidad;
                            }
                            else {
                                $iMeseTranscurridos = 0;
                                $aniosTranscurridos = (date("Y") - $q->ultimoanio) + 1;
                                for ($i = 1; $i <= $aniosTranscurridos; $i++) {
                                    //Año inicial
                                    if ($i == 1) {
                                        $iMeseTranscurridos = $iMeseTranscurridos + (12 - $q->ultimomes);
                                    }
                                    else {
                                        //Año final
                                        if ($i== $aniosTranscurridos) {
                                            $iMeseTranscurridos = $iMeseTranscurridos + (date("m"));
                                        }
                                        //Años Intemedios
                                        else {
                                            $iMeseTranscurridos = $iMeseTranscurridos + 12;
                                        }
                                    }
                                }
                                $ResultadoAdeudo = $iMeseTranscurridos * $q->mensualidad;
                            }
                        }
                        $q->adeudo = $ResultadoAdeudo;
                        $q->adeudof = Util::formatToCurrency($ResultadoAdeudo);

                        $colh = ColoniaHorarios::find(
                            "idcolonia = ".$q->idcolonia." and activo = true and tipo = 'ORGÁNICA'"
                        );
                        $org = array();
                        foreach ($colh as $c){
                            $aux = new \stdClass();
                            $aux->dia = $c->dia;
                            $aux->horario = $c->horario;
                            array_push($org, $aux);
                        }
                        $q->organica = $org;

                        $colh = ColoniaHorarios::find(
                            "idcolonia = ".$q->idcolonia." and activo = true and tipo = 'INORGÁNICA'"
                        );
                        $inorg = array();
                        foreach ($colh as $c){
                            $aux = new \stdClass();
                            $aux->dia = $c->dia;
                            $aux->horario = $c->horario;
                            array_push($inorg, $aux);
                        }
                        $q->inorganica = $inorg;
                    }

                    $this->response->setContent(json_encode(array("data" => $qs)));
                }
            }
            else{
                $this->response->setStatusCode(400, "Calle, número y colonia son requeridos");
            }
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    public function clientesadeudoAction($id){
        if($this->request->isGet()){
            if(!empty($id) && is_numeric($id)){
                $sql = "select 
                COALESCE(c.ultimo_anio_pago, 0) as ultimo_anio_pago,
                COALESCE(c.ultimo_mes_pago, '') as ultimo_mes_pago,
                COALESCE(col.monto, 0.00) monto,
                c.mensualidad 	
                from cliente.cliente c 
                left join cliente.colonia col on c.idcolonia = col.id where id_cliente = $id";
                $c = GenericSQL::getBySQL($sql);
                if(count($c) > 0){
                    $c = $c[0];
                    if ($c->ultimo_anio_pago > 0 && $c->ultimo_mes_pago > 0){ //si hay ultimo año y ultimo mes se calcula el adeudo
                        if($c->ultimo_anio_pago == date("Y")){
                            $ResultadoAdeudo = (date("m") - $c->ultimo_mes_pago) * $c->mensualidad;
                        }
                        else{
                            $iMeseTranscurridos = 0;
                            $aniosTranscurridos = (date("Y") - $c->ultimo_anio_pago) + 1;
                            for ($i = 1; $i <= $aniosTranscurridos; $i++) {
                                if ($i == 1) //Año inicial
                                {
                                    $iMeseTranscurridos = $iMeseTranscurridos + (12 - $c->ultimo_mes_pago);
                                }
                                else
                                {
                                    if ($i== $aniosTranscurridos) //Año final
                                    {
                                        $iMeseTranscurridos = $iMeseTranscurridos + (date("m"));
                                    }
                                    else //Años Intemedios
                                    {
                                        $iMeseTranscurridos = $iMeseTranscurridos + 12;
                                    }
                                }
                            }
                            $ResultadoAdeudo = $iMeseTranscurridos * $c->mensualidad;
                        }
                        $this->response->setContent(json_encode(
                            array(
                                "err" => false,
                                "msj" => $ResultadoAdeudo
                            )
                        ));
                    }
                    else{
                        $this->response->setContent(json_encode(
                            array(
                                "err" => true,
                                "msj" => "El cliente no esta vigente"
                            )
                        ));
                    }
                }
                else {
                    $this->response->setStatusCode(404, "No se encontro al cliente");
                }
            }
            else{
                $this->response->setStatusCode(400);
            }
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    public function clientehistorialAction($id){
        $view = clone $this->view;
        $where = "";
        $this->view->disable();
        if ($this->request->isGet() == true) {
            $where .= " hp.idcliente = ". $id;

            $results= array();
            $sqlQuery = "SELECT 
					hp.id,
					hp.anio,
					hp.mes, 
					hp.cantidad, 
					hp.descuento, 
					hp.idcliente, 
					hp.fecha_pago, 
					hp.activo,
					date(hp.fecha_pago) as fechapago,
					hp.idtipo_cobro,
					hp.idtipo_servicio,
					hp.concepto_servicio,
					hp.idpago,
					COALESCE(hp.folio_factura,'') folio_factura,
					COALESCE(mp.nombre, '') as metodo_pago,
					COALESCE(hp.referencia_bancaria, '') referencia_bancaria
			FROM cliente.historial_pago hp
			left JOIN cliente.metodo_pago mp on hp.idforma_pago = mp.id 
            WHERE  " .$where. " ORDER by hp.id desc ";
            //ORDER BY coalesce(hp.anio,'0') DESC, coalesce(hp.mes,'0') DESC";

            $results = HistorialPago::findByQuery($sqlQuery);

            $data = array();
            $meses = array('enero','febrero','marzo','abril','mayo','junio','julio',
                'agosto','septiembre','octubre','noviembre','diciembre');
            $mesPago = "";
            $imes = 0;

            if(count($results) > 0){
                foreach($results as $res){
                    $mesPago = ($res->mes != null and $res->mes != "") ? trim($res->mes) : "";
                    if($mesPago == "" || strlen(trim($res->mes)) > 2){
                        $mesPago = "";
                        $imes = 0;
                    }//fin:
                    else{
                        $imes = intval($res->mes);
                        $mesPago = $meses[$imes - 1] ;
                    }//fin:else
                    array_push($data, [
                        "anio" => $res->anio,
                        "mes" => $mesPago, //$res->mes,
                        "total" => $res->cantidad,
                        "activo" => $res->activo,
                        "id"=>$res->id,
                        "idpago" =>$res->idpago
                    ]);
                }//fin:foreach($results as $res)
            }//fin:if(count($results) > 0)
            $this->response->setContent(json_encode($data));
        }
        else{
            $view->setVar('NotImplemented', true);
        }
        return $this->response;
    }

    public function clientehistorialv2Action($id){
        $view = clone $this->view;
        $where = "";
        $this->view->disable();
        if ($this->request->isGet() == true) {
            $where .= " hp.idcliente = ". $id;

            $results= array();
            $sqlQuery = "select idpago, cantidad::money cantidadf, cantidad, date(fecha_pago) fecha_pago, 
            comision, comision::money comisionf, folio_interno, payment_id
            from cliente.pagos where idcliente = $id and activo and folio_interno is not null 
            order by fecha_creacion desc";

            $results = Pagos::findByQuery($sqlQuery);

            $this->response->setContent(json_encode($results));
        }
        else{
            $view->setVar('NotImplemented', true);
        }
        return $this->response;
    }

    public function clientereportarAction($id){
        $this->view->disable();
        if($this->request->isPost()){
            $rawBody = $this->request->getJsonRawBody();
            $c = Clientes::findFirstByid_cliente($id);
            if($c){
                $correo = $rawBody->correo;
                $celular = $rawBody->celular;
                $reporte = $rawBody->reporte;
                $origen = $rawBody->origen;

                $resp = new \stdClass();
                $nuevo = true;
                $queja = new Queja();
                $etapa = Etapa::findFirstByInicial(true);
                if(!$etapa) {
                    $this->response->setStatusCode(500);
                    return $this->response;
                }



                $fechaRep = date("c");
                $queja->assign(array(
                    'activo' => true,
                    'id_cliente' => $id,
                    'id_etapa' => $etapa->id,
                    'titulo' =>  'Reporte en línea',
                    'descripcion' => $reporte,
                    'apepat_reporta' => $c->apepat,
                    'apemat_reporta' => $c->apemat,
                    'nombres_reporta' => $c->nombres,
                    'origen' => $origen ? $origen :'APP',
                    'fecha_reporte' => $fechaRep,
                    'correo' => $correo,
                    'celular' => $celular

                ));

                $this->db->begin();
                if(!$queja->save()) {
                    $this->db->rollback();
                    foreach ($queja->getMessages() as $message) {
                        $this->logger->error("save-diagrama-persona_encuesta: ".$message->getMessage());
                    }
                    $this->response->setStatusCode(500);
                    return $this->response;
                }

                $seguimiento = new Seguimiento();
                $seguimiento->assign(array(
                    'activo' => TRUE,
                    'id_origen' => $queja->id,
                    'id_etapa' => $queja->id_etapa,
                    'id_usuario' =>  1,
                    'fecha_creacion' => $fechaRep,
                    'fecha_modificacion' => $fechaRep

                ));

                if(!$seguimiento->save()) {
                    $this->db->rollback();
                    $this->response->setStatusCode(500);
                    foreach ($seguimiento->getMessages() as $message) {
                        $this->logger->error("save-diagrama-persona_encuesta: ".$message->getMessage());
                    }
                    return $this->response;
                }

                $nombre = $c->nombres." ".$c->apepat;
                $this->db->commit();

                $mailSettings = $this->config->mailreporte;
                $publicURL = $this->config->application->publicUrl;
                $transport = NULL;

                $message = Message::newInstance();
                $message->setSubject("Reporte en línea");
                if($correo){
                    $message->setTo($correo);
                    $message->addBcc($this->config->application->notireport);
                    $message->addBcc("rohesmen@gmail.com");
                }
                else{
                    $message->setTo($this->config->application->notireport);
                    $message->addBcc("rohesmen@gmail.com");
                }

                $message->setFrom(array($mailSettings->fromEmail => $mailSettings->fromName));
                //$message->setBody($html, 'text/html');
                $bodymsg = '
                    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;font-family:helvetica,arial,sans-serif;font-size:14px;border:1px solid;color:#666666">
                        <tbody>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" style="width:100%;color:#ffffff;background-color:#ffffff;font-size:24px">
                                        <tbody>
                                            <tr>
                                                <td width="100%" valign="top" style="text-align:center" align="center">
                                                    <img src="https://pamplona.com.mx/img/logo_pamplona.png?r='.time().'" style="max-width:700px;margin:20px 0px">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; white-space: pre-wrap; word-wrap: break-word; padding: 0px 40px;">
                                        <tbody>
                                            <tr>
                                                <td width="100%" valign="top" style="width:100%;color:#8d8d8d" colspan="2">
                                                    <h2 style="color:#000;margin:00px 0px">Ha realizado un reporte</h2>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" style="padding:0px 0px 40px 0px;%" colspan="2">
                                                    <pre style="font-family:helvetica,arial,sans-serif;font-size:14px">Estimada Sr(a) '.$nombre.',<br>Muchas gracias por contactarnos. Es este momento su reporte esta siendo canalizado a nuestros asesores para su pronta atención. Agradecemos su reporte, de esta forma mejoraremos nuestros servicios.</pre>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:0px 20px 20px 20px;">Folio de su reporte:</td>
                                                <td valign="top" style="padding:0px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$queja->id.'</b></td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:0px 20px 20px 20px;">Descripción:</td>
                                                <td valign="top" style="padding:0px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$reporte.'</b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    ';
                $message->setBody($bodymsg, 'text/html');
                $mailSettings = $this->config->mail->smtp;
                $server = $mailSettings->server;
            $port = $mailSettings->port;

            $security = $mailSettings->security;
            $username = $mailSettings->username;
            $password = $mailSettings->password;

                if (!$transport) {
                    $transport = Smtp::newInstance(
                        $server,
                        $port,
                        $security
                    );
                    $transport->setUsername($username);
                    $transport->setPassword($password);
                }
                // Create the Mailer using your created Transport
                $mailer = \Swift_Mailer::newInstance($transport);
                $correoNoEnviador = array();
                if($mailer->send($message, $correoNoEnviador)){
//                        $resp->success = true;
//                        $resp->message = "ok";
                }//fin:if
                else{
                    $this->logger->error("No se envio el correo a ".$correo);
                }//fin:if
                $this->response->setContent($queja->id);
//                $this->flashSession->success("Tu reporte ha sido enviado, se atendera a la brevedad posible");
            }
            else{
                $this->response->setStatusCode(404);
            }
        }
        else {
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    public function clientesdescargarreciboAction($id){
        $this->view->disable();

        $idHp = $this->request->get("idp");
        $hp = HistorialPago::findFirstById($idHp);
        $cli = Clientes::findFirstById_cliente($id);
        $date = new \DateTime($hp->fecha_pago);
        $fecpago = $date->format('d/m/Y');
        $colonia = "";
        $direccion = "C. ".$cli->calle.$cli->calle_letra;
        if($cli->numero){
            $direccion .= " #".$cli->numero.$cli->numero_letra;
        }

        if($cli->cruza1){
            $direccion .= " x ".$cli->cruza1.$cli->letra_cruza1;
        }

        if($cli->cruza2){
            $direccion .= " y ".$cli->cruza2.$cli->letra_cruza2;
        }

        if($cli->idcolonia) {
            $col = Colonias::findFirstById($cli->idcolonia);
            $colonia = $col->nombre.($col->localidad ? (", ".$col->localidad) : "");
        }
        $numLetras = NumeroLetras::convertir($hp->cantidad, 'PESOS');
        $resp = new \stdClass();
        $resp->success = false;
        $resp->message = "";

        //$html = "";
        $correo = "";
        $tblcliente = "";
        $tbltotales = "";
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

//        if($this->request->isPost() == true){
//            if($this->request->isAjax() == true){

                $rawBody = $this->request->getJsonRawBody();

                //$html = $rawBody->html;
//                $tblcliente = $rawBody->tblcliente;
//                $tbltotales = $rawBody->tbltotales;
//                $correo = preg_replace('/\s+/', ' ', $rawBody->correo);

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //creacion del pdf
                $content = '<page orientation="portrait" backtop="30mm" backbottom="7mm">'.
                    '<page_header>'.
                    '<table style="width:100%;">'.
                    '<tr>'.
                    '<td style="width:20%; text-align:right;">&nbsp;'.
                    '</td> '.
                    '<td style="width:60%; text-align:center;">'.
                    '<br>'.
                    '<img src="img/logo_pamplona.png" alt="Pamplona" style="width:45%;"/>'.
                    '<br>'.
                    '<span style="font-weight: bold; font-size: 12pt; color: #0000;">Recibo de Cobro</span>'.
                    '</td>'.
                    '<td style="width:20%; text-align:right; vertical-align:bottom">'.date('d')." de ".$meses[date('n')-1]. " del ".date('Y').
                    '</td> '.
                    '</tr>'.
                    '</table>'.
                    '</page_header>'.
                    '<page_footer>'.
                    '<table style="width:100%; margin-top:30px;" cellpading="0" cellspacing="0" class="page_footer">'.
                    '<tr>'.
                    '<td style="width:30%; height:2%; text-align:center;">'.
                    '&nbsp;'.
                    '</td>'.
                    '<td style="width:50%; height:2%; text-align:Left;">'.
                    '<strong></strong>'.
                    '</td>'.
                    '<td style="width:20%; height:2%; text-align:center;">'.
                    '[[page_cu]] de [[page_nb]]'.
                    '</td>'.
                    ' </tr>'.
                    '</table>'.
                    '</page_footer>';

                $content .= '<br/>
                    <table id="tblinfocliente" class="table no-border table-condensed" border="0" cellpadding="0" cellspacing="0" align="center">
						<tbody>									
							<tr>
							  <td class="" colspan="2" align="center">
								<b>'.$this->config->application->nom_com .'</b></td>
							  <td class=""></td>
							</tr>
							<tr>
								<td colspan="2" align="center">'. $this->config->application->dir_pam .'</td>
								<td class=""></td>
							</tr>
							<tr>
								<td colspan="2" align="center">Mérida, Yucatán, México. R.F.C.: '. $this->config->application->rfcpam .'</td>
								<td></td>
							</tr>
							<tr>
								<td align="right">Tel. 9844799</td>
								<td align="center">No.:  <label id="recibo-folio">'.$hp->idpago.'-'.$hp->id.'</label></td>
							</tr>													
							<tr>
								<td align="left" colspan="2">Nombre:  <label id="recibo-nombre"></label></td>								
							</tr>
							
							<tr>
								<td align="left" colspan="2">Dirección:  <label id="recibo-direccion">'.$direccion.'</label></td>								
							</tr>
							
							<tr>
								<td align="left" colspan="2">Colonia:  <label id="recibo-colonia">'.$colonia.'</label></td>
							</tr>
							<tr>
								<td align="left">Ruta:  <label id="recibo-ruta"></label></td>
								<td align="left">No. Cliente:  <label id="recibo-idcliente">'.$id.'</label></td>
							</tr>
							<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
							
							<tr>							
								<td colspan="2" align="left"><label id="recibo-concepto">RECIBO DE RECOLECCION EN EL DIA: '.$fecpago.'</label></td>							
							</tr>	
							<tr>														
								<td colspan="2" align="left"><label id="recibo-meses">DEL MES DE: '.$meses[$hp->mes].'</label></td>									
							</tr>	
														
						</tbody>
					</table>																														
					<table class="table no-border table-condensed" id="tblReciboTotales" border="0" cellpadding="0" cellspacing="0" align="center">
						<tbody>																
							<tr id="row-total1">
								<td align="left" width="60%">Cantidad en letras</td>
								<td align="right" width="40%">Subsidio: $3.00</td>
							</tr>																				
							<tr id="row-total2">
								<td align="left" width="60%">Son:  <label id="recibo-numletras">'.$numLetras.'</label></td>
								<td align="right" width="40%">Total pagado:  <label id="recibo-total">$ '.$hp->cantidad.'</label></td>
							</tr>																																			
							<tr>
								<td align="left" width="60%">
									Todos los Residuos sólidos (Basura)<br> 
									serán depositados en el Relleno Sanitario<br>
									Subsidio del 10% sobre Tarifa anterior aplicada.						
								</td>
								<td align="left" width="40%">
									Nota: Para cualquier aclaración,<br> 
									favor de conservar su último recibo.
								</td>
							</tr>																																			
						</tbody>
					</table>
                ';
                $content .= '</page>';
                $html2pdf = new html2Pdf('P','A4','fr');
                $html2pdf->WriteHTML($content);
                $filename = 'recibo'.time().'.pdf';
                $content_PDF = $html2pdf->Output($filename);
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="recibo.pdf"');
                readfile($filename);
//                    $this->logger->info('salida readfile ' . $rfile);
//                exit();
//                    return $this->response;
                $this->response->send();
                unlink($filename);
//            }//fin:if
//        }//fin:if
//        $this->response->setContent(json_encode($resp));
//        return $this->response;
    }

    public function clientesdescargarrecibov2Action($idcliente){
        $this->view->disable();

        $idpago = $this->request->get("idp");
        $meses = array(
            "1" => "Enero",
            "2" => "Febrero",
            "3" => "Marzo",
            "4" => "Abril",
            "5" => "Mayo",
            "6" => "Junio",
            "7" => "Julio",
            "8" => "Agosto",
            "9" => "Septiembre",
            "10" => "Octubre",
            "11" => "Noviembre",
            "12" => "Diciembre",
        );


        $c = Clientes::findFirstByid_cliente($idcliente);
        $oMun = MunicipiosYucatan::findFirstByCve_mun($c->idmunicipio);
            $nombre = $c->nombres." ".$c->apepat." ".$c->apemat;

            $direccion = "Calle ".$c->calle." ";

            if($c->calle_letra != null && $c->calle_letra != ""){
                $direccion .= "- ".$c->calle_letra." ";
            }

            $direccion .= "# ".$c->numero." ";

            if($c->numero_letra != null && $c->numero_letra != ""){
                $direccion .= "- ".$c->numero_letra." ";
            }

            $colonia = "";
            if($c->idcolonia != null && $c->idcolonia != ""){
                $colonias = Colonias::findFirstById($c->idcolonia);
                if($colonias){
                    $colonia = $colonias->nombre;
                }
            }

            $pago = Pagos::findFirst([
                "idpago = $idpago"
            ]);

            if($pago){
                $fecha_pago = date("d/m/Y", strtotime(str_replace('/', '-',$pago->fecha_pago)));

                $HistorialPago = HistorialPago::find([
                    "idpago = $idpago",
                    "order" => "id"
                ]);

                $mesAux = '';
                $anioAux = '';
                $anioAnt = '';
                $mesAnioAux ='';
                $contMes = 0;
                $mesesString = '';

                $CountMes = count($HistorialPago);

                foreach($HistorialPago as $hp){
                    $mesAux = $meses[$hp->mes];
                    $anioAux = $hp->anio;

                    if($anioAnt != $anioAux){
                        $mesesString .= $CountMes == 0 ? '' : '<br>';
                        $mesesString .= $anioAux . ': ';
                    }

                    $mesesString .= $mesAux;

                    if($CountMes > 1){
                        if($contMes == $CountMes -1){
                            $mesesString .= '.';
                        } else{
                            $mesesString .= ', ';
                        }
                    }else{
                        $mesesString .= '.';
                    }

                    $mesAnioAux = $mesAux . ' ' . $anioAux;
                    array_push($mesPago, $mesAnioAux);

                    $contMes += 1;
                    $anioAnt = $anioAux;

                }

                $cantidad = $pago->comision ? ($pago->cantidad - $pago->comision) : $pago->cantidad;
                $publicURL = $this->config->application->publicUrl;
                $bodymsg = $this->view->getPartial("layouts/recibo",
                    [
                        "folio" => $pago->folio_interno,
                        "nombre" => $nombre,
                        "direccion" => $direccion,
                        "colonia" => $colonia,
                        "idcliente" => $idcliente,
                        "municipio" => $oMun->nombre,
                        "fecha_pago" => $fecha_pago,
                        "meses" => $mesesString,
                        "cantidad_letras" => NumeroLetras::convertir($pago->cantidad,"pesos","centavos",true),
                        "cantidad" => Util::formatToCurrency($pago->cantidad),
                        "comision" => $pago->comision ? Util::formatToCurrency($pago->comision) : null,
                        "subtotal" =>  Util::formatToCurrency($cantidad),
                    ]
                );
                $this->logger->info($bodymsg);
                $html2pdf = new html2pdf('P','A4','es');
                $html2pdf->WriteHTML($bodymsg);
                $ruta = BASE_DIR.'/public/img/recibo'.$idpago.'.pdf';
                $content_PDF = $html2pdf->Output($ruta,'F');
                if(file_exists($ruta)) {
                    header('Content-type: application/pdf');
                    header('Content-Disposition: attachment; filename="'.basename($ruta).'"');
                    header('Content-Transfer-Encoding: binary');

                    header('Accept-Ranges: bytes');
                    header('Content-Length: ' . filesize($ruta));
                    flush(); // Flush system output buffer
                    readfile($ruta);
                    unlink($ruta);
                    die();
                } else {
                    http_response_code(404);
                    die();
                }
                    //unlink($filename);

            }
            else{
                http_response_code(404);
                die();
            }
    }

    public function serviciosAction($id = null){
        if($this->request->isGet()){
            if(isset($id)){
                if(!empty($id) && is_numeric($id)){
                    $this->response->setContent(json_encode(Servicios::findFirstById($id)));
                }
                else{
                    $this->response->setStatusCode(404);
                }
            }
            else{
                $this->response->setContent(json_encode(
                    Servicios::find([
                        "activo = true",
                        "orden" => "idgrupo asc, orden asc"
                    ])
                ));
            }
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    public function gruposAction($id = null){
        if($this->request->isGet()){
            if(isset($id)){
                if(!empty($id) && is_numeric($id)){
                    $this->response->setContent(json_encode(GrupoServicios::findFirstById($id)));
                }
                else{
                    $this->response->setStatusCode(404);
                }
            }
            else{
                $this->response->setContent(json_encode(GrupoServicios::find([
                    "activo = true",
                    "order" => "orden asc"
                ])));
            }
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    public function gruposserviciosAction($id){
        if($this->request->isGet()){
            if(isset($id)){
                if(!empty($id) && is_numeric($id)){
                    $this->response->setContent(json_encode(Servicios::find(
                        [
                            "idgrupo = $id",
                            "order" => "orden asc"
                        ]
                    )));
                }
                else{
                    $this->response->setStatusCode(404);
                }
            }
            else{
                $this->response->setStatusCode(400);
            }
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    public function parametrosAction($clave = null){
        if($this->request->isGet()){
            if(isset($clave)){
                if(!empty($clave)){
                    $this->response->setContent(json_encode(array(strtoupper($clave) => Parameters::findFirstByClave($clave))));
                }
                else{
                    $this->response->setStatusCode(404);
                }
            }
            else{
                $params = Parameters::findByActivo(true);
                $resp = array();
                foreach ($params as $par){
                    $resp[strtoupper($par->clave)] = $par;
                }
                $this->response->setContent(json_encode($resp));
            }
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    /**
     * Agenda 1 servicio para 1 cliente
     * $id integer - identificador del cliente
     */
    public function clientesagendarcitaAction($id){
        if($this->request->isPost()){
            if(isset($id)){
                $rawBody = $this->request->getJsonRawBody();
                $fecha = $rawBody->fecha;
                $hora = $rawBody->hora;
                $cantidad = $rawBody->cantidad;
                $idservicio = $rawBody->servicio;
                $correo = $rawBody->correo;
                $telefono = $rawBody->telefono;
                $fecha = implode("-", array_reverse(explode("/", $fecha)));

                $eos = EstadoOrdenServicio::findFirstByClave("SOL");
                $ser = Servicios::findFirstById($rawBody->servicio);
                $horas = $ser->horas_estimadas + $ser->horas_tolereancia;
                $fini = $fecha."T".$hora;
                $os = new OrdenServicio();
//                $os->idservicio = $idservicio;
                $os->idcliente = $id;
                $os->fecha = $fecha;
                $os->hora = $hora;
                $os->correo = $correo;
                $os->telefono = $telefono;
//                $os->cantidad = $cantidad;
                $os->idestado = $eos->id;
//                $os->horafin = date('H:i', strtotime($fini)+ 60*60*$horas);

                $sum_costos = $ser->costo;
                $iva = 0.16;
                $array_osd = array();

                $osd = new OrdenServicioDetalle();
                $osd->idservicio = $ser->id;
                $osd->costo = $ser->costo;
                $osd->cantidad = 1;
                $osd->costo_final = (1 * $ser->costo);

                array_push($array_osd, $osd);

                $os->horafin = date('H:i', strtotime($fini)+ 60*60*$horas);
                $os->total = $sum_costos;
                $os->iva = ($iva * $sum_costos);

                $this->db->begin();
                if($os->save()){
                    $os = OrdenServicio::findFirst($os->id);

                    $bodymsg = '
                    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;font-family:helvetica,arial,sans-serif;font-size:14px;border:1px solid;color:#666666">
                        <tbody>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" style="width:100%;color:#ffffff;background-color:#ffffff;font-size:24px">
                                        <tbody>
                                            <tr>
                                                <td width="100%" valign="top" style="text-align:center" align="center">
                                                    <img src="https://pamplona.com.mx/img/logo_pamplona.png?r='.time().'" style="max-width:700px;margin:20px 0px">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; white-space: pre-wrap; word-wrap: break-word; padding: 0px 40px;">
                                        <tbody>
                                            <tr>
                                                <td width="100%" valign="top" style="width:100%;color:#8d8d8d" colspan="2">
                                                    <h2 style="color:#000;margin:00px 0px">Han solicitado el(los) servicio(s): </h2>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="" style="padding:20px 20px 20px 20px;">Teléfono:</td>
                                                <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$telefono.'</b></td>
                                            </tr>
                                            <tr>
                                                <td valign="" style="padding:20px 20px 20px 20px;">Correo:</td>
                                                <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$correo.'</b></td>
                                            </tr>
                                            <tr>
                                                <td valign="" style="padding:20px 20px 20px 20px;">Fecha:</td>
                                                <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$fecha.'</b></td>
                                            </tr>
                                            <tr>
                                                <td valign="" style="padding:20px 20px 20px 20px;">Hora:</td>
                                                <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$hora.'</b></td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" style="width:100%;color:#8d8d8d" colspan="2">
                                                    <div style="color:#000;margin:00px 0px">  </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; white-space: pre-wrap; word-wrap: break-word; padding: 0px 40px;">
                                        <tbody>
                                            <tr>
                                                <td valign="top" style="padding:0px 20px 20px 20px;">Servicio</td>
                                                <td valign="top" style="padding:0px 20px 20px 20px;">Cantidad</td>
                                                <td valign="top" style="padding:0px 20px 20px 20px;">Costo Unitario</td>
                                                <td valign="top" style="padding:0px 20px 20px 20px;">Costo Total</td>
                                            </tr>
                    ';

                    foreach ($array_osd as $osd) {

                        $osd->idorden_servicio = $os->id;
                        if(!$osd->save()) {
                            $this->db->rollback();
                            $this->response->setStatusCode(500);
                            foreach ($os->getMessages() as $message) {
                                $this->logger->error("save-orden-servicio-detalle: ".$message->getMessage());
                            }
                            return $this->response;
                        }

                        $servicio = Servicios::findFirstById($osd->idservicio);

                        $bodymsg .= '   
                                    <tr>
                                        <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$servicio->nombre.'</b></td>
                                        <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$osd->cantidad.'</b></td>
                                        <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>$ '.$osd->costo.'</b></td>
                                        <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>$ '.($osd->cantidad*$osd->costo).'</b></td>
                                    </tr>
                        ';
                    }
                    $this->db->commit();

                    $bodymsg .= '   
                                    <tr>
                                        <td valign="" style="padding:20px 20px 20px 20px;">Subtotal:</td>
                                        <td valign="top" style="padding:20px 20px 20px 20px;"></td>
                                        <td valign="top" style="padding:20px 20px 20px 20px;"></td>
                                        <td valign="" style="padding:20px 20px 20px 20px;"><b>$ '.($os->total - $os->iva).'</b></td>
                                    </tr>
                                    <tr>
                                        <td valign="" style="padding:20px 20px 20px 20px;">IVA:</td>
                                        <td valign="top" style="padding:20px 20px 20px 20px;"></td>
                                        <td valign="top" style="padding:20px 20px 20px 20px;"></td>
                                        <td valign="" style="padding:20px 20px 20px 20px;"><b>$ '.$os->iva.'</b></td>
                                        
                                    </tr>
                                    <tr>
                                        <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d">Total:</td>
                                        <td valign="top" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"></td>
                                        <td valign="top" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"></td>
                                        <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>$ '.$os->total.'</b></td>
                                    </tr>
                        ';

                    $bodymsg .= '        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    ';

                    $this->logger->info($bodymsg);

                    //Util::notifyServicio($bodymsg);

                    $this->response->setContent($os->id);
                }
                else{
                    $this->db->rollback();
                    $this->response->setStatusCode(500);
                    foreach ($os->getMessages() as $message) {
                        $this->logger->error("save-orden-servicio: ".$message->getMessage());
                    }
                }
                $this->logger->error("llego" .$this->request->getRawBody());
            }
            else{
                $this->response->setStatusCode(400, "El cliente es necesario");
            }
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    /**
     * Agenda N servicios para 1 cliente
     * $idcliente integer - identificador del cliente
     */
    public function clientesagendarcitasAction($idcliente){
        $this->logger->info("clientesagendarcitaAction/$idcliente");
        if($this->request->isPost()){
            $this->logger->info("POST");
            if(isset($idcliente)){
                $rawBody = $this->request->getJsonRawBody();
                $fecha = $rawBody->fecha;
                $hora = $rawBody->hora;
                $servicios = $rawBody->servicios;
                $correo = $rawBody->correo;
                $telefono = $rawBody->telefono;
                $fecha = implode("-", array_reverse(explode("/", $fecha)));

                $eos = EstadoOrdenServicio::findFirstByClave("SOL");

                $fini = $fecha."T".$hora;
                $os = new OrdenServicio();
                $os->idcliente = $idcliente;
                $os->fecha = $fecha;
                $os->hora = $hora;
                $os->correo = $correo;
                $os->telefono = $telefono;
                $os->idestado = $eos->id;

                $sum_horas = 0;
                $sum_costos = 0;
                $iva = 0.16;
                $array_osd = array();
                if(!empty($servicios) && is_array($servicios) && count($servicios) > 0) {
                    foreach ($servicios as $ser_data) {

                        $idservicio = $ser_data->idservicio;
                        $cantidad = $ser_data->cantidad;

                        $ser = Servicios::findFirstById($idservicio);
                        if(!empty($ser)) {
                            $sum_horas += ($ser->horas_estimadas + $ser->horas_tolereancia);
                            $sum_costos += ($cantidad * $ser->costo);

                            $osd = new OrdenServicioDetalle();
                            $osd->idservicio = $idservicio;
                            $osd->costo = $ser->costo;
                            $osd->cantidad = $cantidad;
                            $osd->costo_final = ($cantidad * $ser->costo);

                            array_push($array_osd, $osd);
                        }
                    }
                }

                $os->horafin = date('H:i', strtotime($fini)+ 60*60*$sum_horas);
                $os->total = $sum_costos;
                $os->iva = ($iva * $sum_costos);

                $this->db->begin();

                if($os->save()) {

                    $os = OrdenServicio::findFirst($os->id);

                    $bodymsg = '
                    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;font-family:helvetica,arial,sans-serif;font-size:14px;border:1px solid;color:#666666">
                        <tbody>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" style="width:100%;color:#ffffff;background-color:#ffffff;font-size:24px">
                                        <tbody>
                                            <tr>
                                                <td width="100%" valign="top" style="text-align:center" align="center">
                                                    <img src="https://pamplona.com.mx/img/logo_pamplona.png?r='.time().'" style="max-width:700px;margin:20px 0px">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; white-space: pre-wrap; word-wrap: break-word; padding: 0px 40px;">
                                        <tbody>
                                            <tr>
                                                <td width="100%" valign="top" style="width:100%;color:#8d8d8d" colspan="2">
                                                    <h2 style="color:#000;margin:00px 0px">Han solicitado el(los) servicio(s): </h2>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="" style="padding:20px 20px 20px 20px;">Teléfono:</td>
                                                <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$telefono.'</b></td>
                                            </tr>
                                            <tr>
                                                <td valign="" style="padding:20px 20px 20px 20px;">Correo:</td>
                                                <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$correo.'</b></td>
                                            </tr>
                                            <tr>
                                                <td valign="" style="padding:20px 20px 20px 20px;">Fecha:</td>
                                                <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$fecha.'</b></td>
                                            </tr>
                                            <tr>
                                                <td valign="" style="padding:20px 20px 20px 20px;">Hora:</td>
                                                <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$hora.'</b></td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" style="width:100%;color:#8d8d8d" colspan="2">
                                                    <div style="color:#000;margin:00px 0px">  </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; white-space: pre-wrap; word-wrap: break-word; padding: 0px 40px;">
                                        <tbody>
                                            <tr>
                                                <td valign="top" style="padding:0px 20px 20px 20px;">Servicio</td>
                                                <td valign="top" style="padding:0px 20px 20px 20px;">Cantidad</td>
                                                <td valign="top" style="padding:0px 20px 20px 20px;">Costo Unitario</td>
                                                <td valign="top" style="padding:0px 20px 20px 20px;">Costo Total</td>
                                            </tr>
                    ';

                    foreach ($array_osd as $osd) {

                        $osd->idorden_servicio = $os->id;
                        if(!$osd->save()) {
                            $this->db->rollback();
                            $this->response->setStatusCode(500);
                            foreach ($os->getMessages() as $message) {
                                $this->logger->error("save-orden-servicio-detalle: ".$message->getMessage());
                            }
                            return $this->response;
                        }

                        $servicio = Servicios::findFirstById($osd->idservicio);

                        $bodymsg .= '   
                                    <tr>
                                        <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$servicio->nombre.'</b></td>
                                        <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$osd->cantidad.'</b></td>
                                        <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>$ '.$osd->costo.'</b></td>
                                        <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>$ '.($osd->cantidad*$osd->costo).'</b></td>
                                    </tr>
                        ';
                    }
                    $this->db->commit();

                    $bodymsg .= '   
                                    <tr>
                                        <td valign="" style="padding:20px 20px 20px 20px;">Subtotal:</td>
                                        <td valign="top" style="padding:20px 20px 20px 20px;"></td>
                                        <td valign="top" style="padding:20px 20px 20px 20px;"></td>
                                        <td valign="" style="padding:20px 20px 20px 20px;"><b>$ '.($os->total - $os->iva).'</b></td>
                                    </tr>
                                    <tr>
                                        <td valign="" style="padding:20px 20px 20px 20px;">IVA:</td>
                                        <td valign="top" style="padding:20px 20px 20px 20px;"></td>
                                        <td valign="top" style="padding:20px 20px 20px 20px;"></td>
                                        <td valign="" style="padding:20px 20px 20px 20px;"><b>$ '.$os->iva.'</b></td>
                                        
                                    </tr>
                                    <tr>
                                        <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d">Total:</td>
                                        <td valign="top" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"></td>
                                        <td valign="top" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"></td>
                                        <td valign="" style="padding:20px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>$ '.$os->total.'</b></td>
                                    </tr>
                        ';

                    $bodymsg .= '        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    ';

                    $this->logger->info($bodymsg);

                    //Util::notifyServicio($bodymsg);

                    $this->response->setContent($os->id);
                }
                else{
                    $this->db->rollback();
                    $this->response->setStatusCode(500);
                    foreach ($os->getMessages() as $message) {
                        $this->logger->error("save-orden-servicio: ".$message->getMessage());
                    }
                }
                //$this->logger->error("llego" .$this->request->getRawBody());
            }
            else{
                $this->response->setStatusCode(400, "El cliente es necesario");
            }
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    /**
     * Recupera las ordenes de servicio a partir del $id del cliente recibido.
     *
     */
    public function misserviciosAction($id = null){
        if($this->request->isGet()){
            if(isset($id)){
                if(!empty($id) && is_numeric($id)){
                    $this->response->setContent(json_encode(array(
                        "data" => OrdenServicio::find([
                            "idcliente = ".$id,
                            "order" => "fecha desc"
                        ])
                    )));
                }
                else{
                    $this->response->setStatusCode(400);
                }
            }
            else{
                $this->response->setStatusCode(400);
            }
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    /**
     * Recupera las ordenes de servicio y sus detalles a partir del $idcliente del cliente recibido.
     *
     */
    public function misservicioscondetalleAction($idcliente = null){
        if($this->request->isGet()){
            if(isset($idcliente)){
                if(!empty($idcliente) && is_numeric($idcliente)){
                    $ordenes = OrdenServicio::find([
                            "idcliente = ".$idcliente,
                            "order" => "fecha desc"
                        ]);
                    $array_ordenes = array();
                    foreach ($ordenes as $orden) {
                        $o = json_decode(json_encode($orden));

                        $orden_detalles = OrdenServicioDetalle::find("idorden_servicio = $o->id and activo = true");

                        $o->detalles = $orden_detalles;

                        array_push($array_ordenes, $o);
                    }
                    $this->response->setContent(json_encode(array("data" => $array_ordenes)));
                }
                else{
                    $this->response->setStatusCode(400);
                }
            }
            else{
                $this->response->setStatusCode(400);
            }
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    public function promocionesAction($id = null){
        if($this->request->isGet()){
            if(isset($id)){
                if(!empty($id) && is_numeric($id)){
                    $this->response->setContent(json_encode(Promociones::findFirstById($id)));
                }
                else{
                    $this->response->setStatusCode(404);
                }
            }
            else{
                $this->response->setContent(json_encode(Promociones::getVigentes()));
            }
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    public function datoscontactoAction(){
        if($this->request->isPost()){
            $rawBody = $this->request->getJsonRawBody();
            $nombre = $rawBody->nombre;
            $telefono = $rawBody->tel;
            $bodymsg = '
                    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;font-family:helvetica,arial,sans-serif;font-size:14px;border:1px solid;color:#666666">
                        <tbody>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" style="width:100%;color:#ffffff;background-color:#ffffff;font-size:24px">
                                        <tbody>
                                            <tr>
                                                <td width="100%" valign="top" style="text-align:center" align="center">
                                                    <img src="https://pamplona.com.mx/img/logo_pamplona.png?r='.time().'" style="max-width:700px;margin:20px 0px">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; white-space: pre-wrap; word-wrap: break-word; padding: 0px 40px;">
                                        <tbody>
                                            <tr>
                                                <td width="100%" valign="top" style="width:100%;color:#8d8d8d" colspan="2">
                                                    <h2 style="color:#000;margin:00px 0px">Contactar cliente </h2>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:0px 20px 20px 20px;">Nombre:</td>
                                                <td valign="top" style="padding:0px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$nombre.'</b></td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:0px 20px 20px 20px;">Teléfono:</td>
                                                <td valign="top" style="padding:0px 20px 20px 20px;background-color:#f0f0f0;color:#8d8d8d"><b>'.$telefono.'</b></td>
                                            </tr>
                                           
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    ';

                    Util::notifyDatosContacto($bodymsg);
        }
        else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

    public function padeudosAction(){
        $datos = $this->request->getJsonRawBody();
        $resp = array();
        $proman = Promociones::getAnualVigente();
        foreach ($datos as $d){
            $sql = "select 
                        id_cliente id, 
                        coalesce(upper(trim(calle)), '') || coalesce(' ' || upper(trim(calle_letra)), '') calle,
                        coalesce(upper(trim(numero)), '') || coalesce(' ' || upper(trim(numero_letra)), '') numero,
                        coalesce(upper(trim(cruza1)), '') || coalesce(' ' || upper(trim(letra_cruza1)), '') cruz1,
                        coalesce(upper(trim(cruza2)), '') || coalesce(' ' || upper(trim(letra_cruza2)), '') cruz2,
                        col.nombre colonia,
                        c.ultimo_mes_pago::integer ultimomes,
                        c.ultimo_anio_pago ultimoanio,
                        case 
                    when ultimo_mes_pago = '1' then 'Enero'
                    when ultimo_mes_pago = '2' then 'Febrero'
                    when ultimo_mes_pago = '3' then 'Marzo'
                    when ultimo_mes_pago = '4' then 'Abril'
                    when ultimo_mes_pago = '5' then 'Mayo'
                    when ultimo_mes_pago = '6' then 'Junio'
                    when ultimo_mes_pago = '7' then 'Julio'
                    when ultimo_mes_pago = '8' then 'Agosto'
                    when ultimo_mes_pago = '9' then 'Septiembre'
                    when ultimo_mes_pago = '10' then 'Octubre'
                    when ultimo_mes_pago = '11' then 'Noviembre'
                    when ultimo_mes_pago = '12' then 'Diciembre' end txtultimomes, 
                        c.idcolonia,
                        c.mensualidad
                    from cliente.cliente c
                    left join cliente.colonia col on c.idcolonia = col.id 
                    where c.activo = true and c.vigente = true and c.id_cliente = ".$d->id;
            $qs = GenericSQL::getBySQL($sql);

            foreach ($qs as $q){
                $q->alias = $d->alias;
                $ResultadoAdeudo = 0;
                //si hay ultimo año y ultimo mes se calcula el adeudo
                if ($q->ultimoanio > 0 && $q->ultimomes > 0) {
                    if($q->ultimoanio == date("Y")) {
                        $ResultadoAdeudo = (date("m") - $q->ultimomes) * $q->mensualidad;
                    }
                    else {
                        $iMeseTranscurridos = 0;
                        $aniosTranscurridos = (date("Y") - $q->ultimoanio) + 1;
                        for ($i = 1; $i <= $aniosTranscurridos; $i++) {
                            //Año inicial
                            if ($i == 1) {
                                $iMeseTranscurridos = $iMeseTranscurridos + (12 - $q->ultimomes);
                            }
                            else {
                                //Año final
                                if ($i== $aniosTranscurridos) {
                                    $iMeseTranscurridos = $iMeseTranscurridos + (date("m"));
                                }
                                //Años Intemedios
                                else {
                                    $iMeseTranscurridos = $iMeseTranscurridos + 12;
                                }
                            }
                        }
                        $ResultadoAdeudo = $iMeseTranscurridos * $q->mensualidad;
                    }
                }
                $q->adeudo = $ResultadoAdeudo;
                $q->adeudof = Util::formatToCurrency($ResultadoAdeudo);
                $q->proman = count($proman) > 0 ? $proman[0]: null;
            }
            array_push($resp, $qs[0]);
        }

        $this->response->setContent(json_encode($resp));
        return $this->response;
    }

    public function cobrarAction($idcliente){
        $this->view->disable();
        $idpago = $this->request->get("idp");

        $bSuccess = TRUE;
        $resp = new \stdClass();

//        $this->logger->info('10');
        if ($this->request->isPost() == true){
//            $this->logger->info('11');
            $oCli = Clientes::findFirst($idcliente);
            $rawBody = $this->request->getJsonRawBody();
            $paymentid = $rawBody->id;
            $strIdUser = $this->IDUSUARIO;

            $paymentId = $rawBody->payment_id;
            $idCaptures = $rawBody->id_captures;
            $facilitatorAT = $rawBody->facilitatorAT;
            $payerId = $rawBody->payer_id;
            $payerEmail = $rawBody->payer_email;
            $payerName = $rawBody->payer_name;

            $this->db->begin();
            //Generar el objeto para guardar el Pago
            $oPagoAux = PagosAux::findFirstByIdpago($idpago);
            $oPagoAux->payment_id = $paymentid;
            $oPagoAux->fecha_modificacion = date("c");
            $oPagoAux->payment_id = $paymentId;
            $oPagoAux->payer_id = $payerId;
            $oPagoAux->facilitator_access_token = $facilitatorAT;
            $oPagoAux->id_captures = $idCaptures;
            $oPagoAux->payer_email = $payerEmail;
            $oPagoAux->payer_name = $payerName;

            if($oPagoAux->save()){
//                $this->logger->info('12.'.$idpago);
                $oPago = new Pagos();
                $oPago->fecha_modificacion = date("c");
                $oPago->fecha_creacion = date("c");
                $oPago->fecha_pago = date("c");
                $oPago->idusuario = $strIdUser;
                $oPago->cantidad = $oPagoAux->cantidad;
                $oPago->comision = $oPagoAux->comision;
                $oPago->idpago_aux = $oPagoAux->idpago;
                $oPago->ispagoanual = $oPagoAux->ispagoanual;
                $oPago->totpagoanual = $oPagoAux->totpagoanual;

                $oPago->payment_id = $oPagoAux->payment_id;
                $oPago->payer_id = $oPagoAux->payer_id;
                $oPago->facilitator_access_token = $oPagoAux->facilitator_access_token;
                $oPago->id_captures = $oPagoAux->id_captures;
                $oPago->payer_email = $oPagoAux->payer_email;
                $oPago->payer_name = $oPagoAux->payer_name;

                //Obtener los datos para gaurdar el pago
                //Asignacion de valores para realizar el guardado del Pago
                $oPago->assign(array(
                    "idcliente" => $idcliente,
                    "folio_factura" => $oPagoAux->folio_factura,
                    "referencia_bancaria" => $oPagoAux->referencia_bancaria,
                    "idforma_pago" => $oPagoAux->idforma_pago,
                    "activo" => TRUE,
                    "latitud" => null,
                    "longitud" => null,
                    "folio_interno" => $oPagoAux->folio_interno
                ));
//                $this->logger->info('3');

                //Guardado del renglon de pago
                if($oPago->save()) {
//                    $this->logger->info('13');
//                    $idPago = $oPago->idpago;
                    $resp->success = true;

                    $cantidadLiquidar = 0;
                    $mesesxpagar = HistorialPagoAux::find([
                        "idpago = ".$idpago
                    ]);
                    foreach ($mesesxpagar as $m){
                        $oHistorialPago = new HistorialPago();
                        $oHistorialPago->fecha_modificacion = date("c");
                        $oHistorialPago->fecha_creacion = date("c");

                        $oHistorialPago->assign(array(
                            "idcliente" => $idcliente,
                            "mes"=> $m->mes,
                            "anio"=> $m->anio,
                            "descuento"=> $m->descuento,
                            "iva"=> $m->iva,
                            "cantidad"=> $m->cantidad,
                            "fecha_pago"=> $m->fecha_pago,
                            "idforma_pago"=> $m->idforma_pago,
                            "referencia_bancaria" => $m->referencia_bancaria,
                            "validado" => TRUE,
                            "idusuario" => $m->idusuario,
                            "facturado" => $m->facturado,
                            "folio_factura" => $m->folio_factura,
                            "idcobratario" => $m->idcobratario,
                            "caja" => FALSE,
                            "idbanco" => $m->idbanco,
                            "activo" => TRUE,
                            "envio_correo" => FALSE,
                            "recibo_impreso" => FALSE,
                            "incluido_corte" => FALSE,
                            "fecha_corte" => null,
                            "idvisita_cliente" => $m->idvisita_cliente,
                            "idtipo_cobro" => $m->idtipo_cobro,
                            "idtipo_servicio" => 0,
                            "concepto_servicio" => '',
                            "idpago" => $oPago->idpago,
                            "idpago_aux" => $oPagoAux->idpago,
                            "idhistorial_aux" => $m->id
                        ));

                        if($oHistorialPago->save()){
//                            $this->logger->info('14');
                            $bSuccess = ($bSuccess && TRUE);
                        }
                        else{
//                            $this->logger->info('15');
                            $bSuccess = ($bSuccess && FALSE);
                            break;
                        }//fin:else
                    }
                    $objFolio = Folios::findFirstBySerie_folio($oPago->folio_interno);
//                    $this->logger->info('17');
//                    $this->logger->info('6')0;
                    $objFolio->idpago = $oPago->idpago;
                    $objFolio->monto = $oPago->cantidad;
                    if($objFolio->save()){
//                        $this->logger->info('16');
                        $bSuccess = ($bSuccess && TRUE);
                    }
                    else{
                        foreach ($objFolio->getMessages() as $message) {
                            $this->logger->error("(save-folio): " . $message);
                        }
                        $bSuccess = ($bSuccess && FALSE);
                    }//fin:else

//                    $this->logger->info('18');
                    $rowCount = $this->db->query("select * from cliente.historial_pago where idcliente = $idcliente and activo 
                    order by anio::integer desc, mes::integer desc limit 1");
                    $rowCount = $rowCount->fetch();
                    if($rowCount){
//                        $this->logger->info('19');
                        $oCli = Clientes::findFirst($idcliente);
                        $oCli->ultimo_mes_pago = $rowCount["mes"];
                        $oCli->ultimo_anio_pago = $rowCount["anio"];
                        if(!$oCli->save()){
                            foreach ($oCli->getMessages() as $message) {
                                $this->logger->error("(save-folio-ultimo-pago-cliente): " . $message);
                            }
                            $bSuccess = ($bSuccess && FALSE);
                        }
                    }
//                    $this->logger->info('20');
//                    $this->logger->info('7');
                    if($bSuccess) {
                        $this->db->commit();
                    }
                    else {
//                        $this->logger->info('3');
                        $this->db->rollback();
                        $this->response->setStatusCode(500, "Internal Server Error");
                    }
                }//fin:if($oPago->save())
                else{
//                    $this->logger->info('2');
                    $this->db->rollback();
                    $this->response->setStatusCode(500, "Internal Server Error");
                }//fin:else
            }
            else{
//                $this->logger->info('1');
                $this->db->rollback();
                $this->response->setStatusCode(500, "Internal Server Error");
            }
        }//fin:if ($this->request->isPost() == true)
        else{
//            $this->logger->info('options <=3=> post');
            $this->response->setStatusCode(400);
        }
        return $this->response;
    }

    public function cobrartemporalAction($idcliente){
        $this->view->disable();

        $arrayId = array();
        $bSuccess = TRUE;
        $idPago = 0;
        $resp = new \stdClass();

        if ($this->request->isPost() == true){
            $oCli = Clientes::findFirst($idcliente);
            $rawBody = $this->request->getJsonRawBody();
            $meses = $rawBody->mesesxpagar;
            $pago = $rawBody->pago;
            $comision = $rawBody->comision;
            $strIdUser = $this->IDUSUARIO;
            $jsonResp = $rawBody->respuesta;
            $isproman = isset($rawBody->ispromanual) ?  $rawBody->ispromanual : false;
            $totoproman = isset($rawBody->totpromanual) ?  $rawBody->totpromanual : false;

            $this->db->begin();
            //Generar el objeto para guardar el Pago
            $oPago = new PagosAux();
            $oPago->fecha_modificacion = date("c");
            $oPago->fecha_creacion = date("c");
            $oPago->fecha_pago = date("c");
            $oPago->idusuario = $strIdUser;
            $oPago->idmotivo = null;
            $oPago->cantidad = $pago;
            $oPago->comision = $comision;
            $oPago->ispagoanual = $isproman;
            $oPago->totpagoanual = $totoproman;

            //Obtener los datos para gaurdar el pago
            if(count($meses) > 0){
                $foliador = Foliador::getNextFolio($strIdUser, $idcliente);
                $folioInterno = $foliador->folio_anio;

                $folfac = null;
                $referencia = null;
                $idformapago = 1;
                $idcliente = intval($idcliente);

                $objFolio = new Folios();
                $objFolio->folio = $folioInterno;
                $objFolio->serie_folio = $folioInterno;
                $objFolio->idusuario = $strIdUser;
                $objFolio->idusuario_asigno = $strIdUser;
                $objFolio->idcobratario = $strIdUser;
                $objFolio->origen = 'INTERNETWEB';
//                $this->logger->info('1');

                if(!$objFolio->save()){
                    $this->db->rollback();
                    $this->response->setStatusCode(500, "Internal Server Error");
                    foreach ($objFolio->getMessages() as $message) {
                        $this->logger->info("(save-folio): " . $message);
                    }
                    return $this->response;
                }
//                $this->logger->info('2');

                //Asignacion de valores para realizar el guardado del Pago
                $oPago->assign(array(
                    "idcliente" => $idcliente,
                    "folio_factura" => $folfac,
                    "referencia_bancaria" => $referencia,
                    "idforma_pago" => $idformapago,
                    "activo" => TRUE,
                    "latitud" => null,
                    "longitud" => null,
                    "folio_interno" => $folioInterno
                ));
//                $this->logger->info('3');

                //Guardado del renglon de pago
                if($oPago->save()) {

                    $idPago = $oPago->idpago;
                    $resp->success = true;

                    $cantidadLiquidar = 0;
                    for($iIdx = 0; $iIdx < count($meses); $iIdx++){
//                        $this->logger->info('4');
                        $rowJson = $meses[$iIdx];

                        //Generar el objeto para la forma de pago
                        $oHistorialPago = new HistorialPagoAux();
                        $oHistorialPago->fecha_modificacion = date("c");
                        $oHistorialPago->fecha_creacion = date("c");

                        $mes 	   = ($rowJson->mes) ? $rowJson->mes: null;
                        $annio 	   = ($rowJson->anio) ? $rowJson->anio : null;

                        $descuento = ($rowJson->descuento) ? $rowJson->descuento : 0.00;
                        $iva = ($rowJson->iva) ? $rowJson->iva : 0.00;
                        $cantidad = ($rowJson->mensualidad) ? $rowJson->mensualidad : 0.00;
                        $fecha = date( 'c');
                        $formapago = $rowJson->idforma_pago ? $rowJson->idforma_pago: 0;
                        $refbancaria = $rowJson->referencia_bancaria ? $rowJson->referencia_bancaria: null;
                        $idtipo_cobro = 1;
                        $idvisita_cliente = $rowJson->idvisita_cliente ? $rowJson->idvisita_cliente: null;
                        $idbanco = $rowJson->idbanco ? $rowJson->idbanco: null;
                        $idcobratario = $strIdUser;
                        $folio_factura = $rowJson->folio_factura ? $rowJson->folio_factura : null;
                        $facturado = $rowJson->facturado ? $rowJson->facturado : false;

                        if($oCli->comercio){
                            if($iIdx == (count($meses) - 1)){
                                $cantidad = $pago;
                            }
                            else{
                                $cantidad = 0;
                            }
                        }
                        $cantidadLiquidar += $cantidad;

                        $oHistorialPago->assign(array(
                            "idcliente" => $idcliente,
                            "mes"=> $mes,
                            "anio"=> $annio,
                            "descuento"=> $descuento,
                            "iva"=> $iva,
                            "cantidad"=> $cantidad,
                            "fecha_pago"=> $fecha,
                            "idforma_pago"=> $formapago,
                            "referencia_bancaria" => $refbancaria,
                            "validado" => TRUE,
                            "idusuario" => $strIdUser,
                            "facturado" => $facturado,
                            "folio_factura" => $folio_factura,
                            "idcobratario" => $idcobratario,
                            "caja" => FALSE,
                            "idbanco" => $idbanco,
                            "activo" => TRUE,
                            "envio_correo" => FALSE,
                            "recibo_impreso" => FALSE,
                            "incluido_corte" => FALSE,
                            "fecha_corte" => null,
                            "idvisita_cliente" => $idvisita_cliente,
                            "idtipo_cobro" => $idtipo_cobro,
                            "idtipo_servicio" => 0,
                            "concepto_servicio" => '',
                            "idpago" => $idPago
                        ));

                        if($oHistorialPago->save()){
                            $bSuccess = ($bSuccess && TRUE);
                            $arrayId[$iIdx] = $oHistorialPago->id;
                        }
                        else{
                            $bSuccess = ($bSuccess && FALSE);
                            break;
                        }//fin:else
                    }//fin:for($iIdx = 0; $iIdx < $countItems; $iIdx++)

                    $oPago->refresh();

                    if($jsonResp){
                        $this->response->setContent(json_encode(
                            array(
                                "folio" => $folioInterno,
                                "idpago" => $oPago->idpago
                            )
                        ));
                    }
                    else{
                        $this->response->setContent($oPago->idpago);
                    }

                    if($bSuccess) {
                        $this->db->commit();
                    }
                    else {
                        $this->db->rollback();
                        $this->response->setStatusCode(500, "Internal Server Error");
                    }
                }//fin:if($oPago->save())
                else{
                    $this->db->rollback();
                    $this->response->setStatusCode(500, "Internal Server Error");
                }//fin:else
            }//fin:if($countItems > 0)
            else{
//                $this->logger->info('options <=1=> count');
                $this->db->rollback();
                $this->response->setStatusCode(400);
            }
        }//fin:if ($this->request->isPost() == true)
        else{
//            $this->logger->info('options <=3=> post');
            $this->response->setStatusCode(400);
        }
        return $this->response;
    }
}
