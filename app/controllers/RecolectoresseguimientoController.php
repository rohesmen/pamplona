<?php
namespace Vokuro\Controllers;
use Vokuro\DT\SSPGEO;
use Vokuro\GenericSQL\GenericSQL;
use Vokuro\Models\Recolector;
use Vokuro\Models\BitacoraCambios;

/**
 * Display the default index page.
 */
class RecolectoresseguimientoController extends ControllerBase
{

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function indexAction()
    {
        $this->view->setTemplateBefore('public');

        $conacciones = 'no';
        if($this->acl->isAllowedUser('recolectoresseguimiento', 'edit') or $this->acl->isAllowedUser('recolectoresseguimiento', 'deactivate') or $this->acl->isAllowedUser('recolectoresseguimiento', 'activate') or $this->acl->isAllowedUser('recolectoresseguimiento', 'info')){
            $conacciones = 'si';
        }
        $this->view->setVar('coacciones', $conacciones);

    }

    public function buscarAction(){
        $request = $this->request;
        $response = $this->response;


        $columns = array(
            array( 'db' => '', 'dt' => 0,
                'formatter' => function( $d, $row ) {
                    $buttons = '';
                    if($this->acl->isAllowedUser('recolectoresseguimiento', 'info')){
                        $buttons .= '<button class="btn btn-primary btn-sm recolectoresseguimiento-info" data-id="'.$row["id"].'" type="button" title="Consultar">
                            <i class="fa fa-info"></i>
                        </button> ';
                    }
                    if($this->acl->isAllowedUser('recolectoresseguimiento', 'edit') && $row["activo"] === true){
                        $buttons .= '<button class="btn btn-primary btn-sm recolectoresseguimiento-edit" data-id="'.$row["id"].'" type="button" title="Editar">
                            <i class="fa fa-pencil"></i>
                        </button> ';
                    }
                    if($this->acl->isAllowedUser('recolectoresseguimiento', 'deactivate') && $row["activo"] === true){
                        $buttons .= '<button class="btn btn-danger btn-sm recolectoresseguimiento-delete" data-id="'.$row["id"].'" type="button" title="¿Desea desactivar?">
                            <i class="fa fa-times"></i>
                        </button> ';
                    }
                    if($this->acl->isAllowedUser('recolectoresseguimiento', 'activate') && $row["activo"] === false){
                        $buttons .= '<button class="btn btn-info btn-sm recolectoresseguimiento-active" data-id="'.$row["id"].'" type="button" title="¿Desea activar?">
                            <i class="fa fa-check"></i>
                        </button> ';
                    }
                    return $buttons;
                }
            ),
            array( 'db' => '', 'dt' => 1),
            array( 'db' => 'activo', 'datatype' => 'boolean', 'dt' => 2,
                'formatter' => function( $d, $row ) {
                    $vigente = $d ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                        : '<i class="fa fa-remove" style="color: red" title="Inactivo"></i>';
                    return $vigente;
                }
            ),
            array( 'db' => 'id', 'dt' => 3, 'datatype' => 'number'),
            array( 'db' => 'nombres', 'dt' => 4),
            array( 'db' => 'apepat', 'dt' => 5),
            array( 'db' => 'apemat', 'dt' => 6),
            array( 'db' => 'fecha_creacion_f', 'datatype' => 'date', 'dt' => 7),
            array( 'db' => 'fecha_modificacion_f', 'datatype' => 'date', 'dt' => 8),
            array( 'db' => 'activo', 'datatype' => 'boolean', 'dt' => 9),
            array( 'db' => 'foto', 'dt' => 10),
            array( 'db' => 'ruta_foto', 'dt' => 11)
        );

        $data = SSPGEO::complex_geo($this->request->get(), "monitoreo.view_recolector", "id", $columns);

        $response->setContent(json_encode($data));
        return $response;
    }

    public function saveAction(){

        $valor = $this->request->getPost("data");

        $rawBody = json_decode($valor);

        $id = $rawBody->id;
        $nombre = mb_strtoupper(trim($rawBody->nombre));
        $apepat = mb_strtoupper(trim($rawBody->apepat));
        $apemat = mb_strtoupper(trim($rawBody->apemat));

        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];

        if($nombre == ""){
            $this->response->setStatuscode(400, "No se ingresó nombre del recolector");
            return $this->response;
        }

        if($apepat == ""){
            $this->response->setStatuscode(400, "No se ingresó apellido paterno del recolector");
            return $this->response;
        }

        $sql = "replace(upper(nombres), ' ', '') = replace(upper('$nombre'), ' ', '')";

        if($apepat != "" && $apepat != null){
            $sql .= " AND replace(upper(apepat), ' ', '') = replace(upper('$apepat'), ' ', '')";
        }

        if($apemat != "" && $apemat != null){
            $sql .= " AND replace(upper(apemat), ' ', '') = replace(upper('$apemat'), ' ', '')";
        }

        if(intval($id) > 0){
            $sql .= " AND id <> $id";
        }

        $busqueda = Recolector::findFirst($sql);

        if($busqueda){
            $this->response->setStatuscode(409, "Ya existe un registro ".($busqueda->activo ? "activo" : "inactivo")." con los mismos datos");
            return $this->response;
        }

        $dataOrigin = null;
        $accion = "CREACION";

        if(intval($id) > 0){
            $data = Recolector::findFirstById($id);
            $dataOrigin = json_encode($data);
            $accion = "EDICION";
        }
        else{
            $data = new Recolector();
            $data->fechacreacion = date("c");
        }

        $data->activo = true;
        $data->idusuario = $idUser;
        $data->nombres = $nombre;
        $data->apepat = $apepat != "" ? $apepat : null;
        $data->apemat = $apemat != "" ? $apemat : null;
        $data->fechamodificacion = date("c");

        $this->db->begin();

        if($data->save()){

            $data->refresh();

            $dataB = new BitacoraCambios();
            $dataB->identificador = $data->id;
            $dataB->modulo = 'RECOLECTSEG';
            $dataB->idusuario = $idUser;
            $dataB->tabla = "monitoreo.recolector";
            $dataB->cambios = json_encode($data);
            $dataB->original = $dataOrigin;
            $dataB->accion = $accion . " MONITOREO RECOLECTOR";

            if($dataB->save()){
                if ($this->request->hasFiles()){
                    $dataOrigin = json_encode($data);
                    $baseLocation = BASE_DIR.DIRECTORY_SEPARATOR."public";
                    $location = DIRECTORY_SEPARATOR."img".DIRECTORY_SEPARATOR."recolectores".DIRECTORY_SEPARATOR;
                    $this->logger->info("con archivo");
                    $files = $this->request->getUploadedFiles();
                    $file = $files[0];
                    $file_name = $file->getName();
                    $this->logger->info("nombre archivo: ".$file_name);
                    $split = explode(".", $file_name);
                    $ext = $split[count($split) - 1];
                    $filename = $data->id."_".time()."_".date("dmY").".".$ext;
                    $fullfilename = $baseLocation.$location.$filename;
                    if(!$file->moveTo($fullfilename)){
                        $this->logger->info("!move to: ".$fullfilename);
                        $this->db->rollback();
                        $mensaje = "Ocurrió un error al guardar la imagen.";
                        $this->logger->error($mensaje);
                        $this->response->setStatusCode(500, $mensaje);
                        return $this->response;
                    } else {
                        $this->logger->info("move to: ".$fullfilename);
                        $data->foto = $filename;
                        $data->ruta_foto = $location;
                        if($data->save()){
                            $data->refresh();
                            $dataB = new BitacoraCambios();
                            $dataB->identificador = $data->id;
                            $dataB->modulo = 'RECOLECTSEG';
                            $dataB->idusuario = $idUser;
                            $dataB->tabla = "monitoreo.recolector";
                            $dataB->cambios = json_encode($data);
                            $dataB->original = $dataOrigin;
                            $dataB->accion = $accion . " MONITOREO RECOLECTOR IMAGEN";
                            if(!$dataB->save()){
                                $this->db->rollback();
                                foreach ($dataB->getMessages() as $message) {
                                    $this->logger->error("save-bitacora-recolectoresseguimiento-imagen: ".$message->getMessage());
                                }
                                $mensaje = "Ocurrió un error al guardar la bitacora imagen.";
                                $this->logger->error($mensaje);
                                $this->response->setStatusCode(500, $mensaje);
                                return $this->response;
                            }
                        } else {
                            $this->db->rollback();
                            foreach ($data->getMessages() as $message) {
                                $this->logger->error("save-recolectoresseguimiento-imagen: ".$message->getMessage());
                            }
                            $mensaje = "Ocurrió un error al guardar el registro imagen.";
                            $this->response->setStatusCode(500, $mensaje);
                            $this->logger->error($mensaje);
                            return $this->response;
                        }
                    }
                }
                $this->db->commit();
            }else{
                $this->db->rollback();
                foreach ($dataB->getMessages() as $message) {
                    $this->logger->error("save-bitacora-recolectoresseguimiento: ".$message->getMessage());
                }
                $mensaje = "Ocurrió un error al guardar la bitacora.";
                $this->logger->error($mensaje);
                $this->response->setStatusCode(500, $mensaje);
            }            
        }else{
            $this->db->rollback();
            foreach ($data->getMessages() as $message) {
                $this->logger->error("save-recolectoresseguimiento: ".$message->getMessage());
            }
            $mensaje = "Ocurrió un error al guardar el registro.";
            $this->logger->error($mensaje);
            $this->response->setStatusCode(500, $mensaje);
        }
        return $this->response;
    }

    public function deleteAction($id, $activo){
        if(!empty($id)){
            $d = Recolector::findFirstById($id);
            if ($d) {

                $identity = $this->auth->getIdentity();
                $idUser = $identity["id"];

                $this->db->begin();

                $dataOrigin = json_encode($d);
                $accion = "DESACTIVACION";
                if($activo === true || $activo == "true"){
                    $accion = "ACTIVACION";
                    $d->idusuario_desactivo = null;
                } else{
                    $d->idusuario_desactivo = $idUser;
                }
                $d->activo = $activo;
                $d->idusuario = $idUser;
                $d->fechamodificacion = date("c");
                if(!$d->save()){
                    $this->db->rollback();
                    foreach ($d->getMessages() as $message) {
                        $this->logger->info("(delete-recolectoresseguimiento): " . $message);
                    }
                    $mensaje = "Ocurrió un error al guardar el registro.";
                    $this->logger->error($mensaje);
                    $this->response->setStatusCode(500, $mensaje);
                }else{
                    $d->refresh();
                    $dataB = new BitacoraCambios();
                    $dataB->identificador = $d->id;
                    $dataB->modulo = 'RECOLECTSEG';
                    $dataB->idusuario = $idUser;
                    $dataB->tabla = "monitoreo.recolector";
                    $dataB->cambios = json_encode($d);
                    $dataB->original = $dataOrigin;
                    $dataB->accion = $accion . " MONITOREO RECOLECTOR";
                    if (!$dataB->save()) {
                        $this->db->rollback();
                        foreach ($dataB->getMessages() as $message) {
                            $this->logger->info("(save-bitacora-delete-recolectoresseguimiento): " . $message);
                        }
                        $mensaje = "Ocurrió un error al guardar la bitacora.";
                        $this->logger->error($mensaje);
                        $this->response->setStatusCode(500, $mensaje);
                    }
                    $this->db->commit();
                }
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(400, "Bad Request");
        }
        return $this->response;
    }

}
