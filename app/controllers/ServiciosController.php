<?php
namespace Vokuro\Controllers;

use Vokuro\DT\DataTableGeo;
use Vokuro\GenericSQL\GenericSQL;
use Vokuro\Models\Brigada;
use Vokuro\Models\Clientes;
use Phalcon\Mvc\View;
use Vokuro\Models\Colonias;
use Vokuro\Models\EstadoOrdenServicio;
use Vokuro\Models\GrupoServicios;
use Vokuro\Models\OrdenServicio;
use Vokuro\Models\OrdenServicioDetalle;
use Vokuro\Models\Parameters;
use Vokuro\Models\SeguimientoOrdenServicio;
use Vokuro\Models\Servicios;
use Vokuro\Models\Users;
use Vokuro\Models\FormaPago;
use Vokuro\Models\HistorialPago;
use Vokuro\Models\Rutas;
use Vokuro\Models\TarifaColonia;
use Vokuro\Models\TipoDescuento;
use Vokuro\Util\Util;
use PHPExcel;
use PHPExcel_Style_Fill;
use PHPExcel_Settings;
use PHPExcel_Shared_Font;

/**
 * Display the default index page.
 */
class ServiciosController extends ControllerBase
{



    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function indexAction()
    {
        /*$this->view->setVar('logged_in', is_array($this->auth->getIdentity()));*/
        $this->view->setTemplateBefore('public');
        $this->view->setVar("estados", EstadoOrdenServicio::find([
            "activo = true",
            "order" => "orden asc"
        ]));

        $paramsini = Parameters::findFirstByClave("MINHORASERVICIO");
        $horaini = $paramsini->valor;
        $paramsfin = Parameters::findFirstByClave("MAXHORASERVICIO");
        $horafin = $paramsfin->valor;
        $horas = array();
        for($i = (int)$horaini; $i <= (int)$horafin; $i++){
            array_push($horas, $i<10 ? "0".$i : $i);
        }
        $this->view->setVar("horas", $horas);
        $grupos = GrupoServicios::findByActivo(true);
        $servicios = [];
        $aux = [];
        foreach ($grupos as $g){
            array_push($servicios, array(
                "grupo" => $g->nombre,
                "data" => Servicios::find([
                    "activo = true and idgrupo = ".$g->id,
                    "order" => "orden asc"
                ])
            ));
        }
//        $this->view->setVar("servicios", Servicios::findByActivo(true));
        $this->view->setVar("servicios", $servicios);
        $this->view->setVar('colonias', Colonias::find([
            "order" => "nombre asc"
        ]));

        $brigadas = Brigada::findByActivo(true);
        $this->view->setVar("brigadas", $brigadas);
    }

    public function getallAction(){
        $this->view->setTemplateBefore('public');
        $fecini = $this->request->get("fecini");
        $fecfin = $this->request->get("fecfin");

        $servs = OrdenServicio::find(
            [
                "fecha >= '".$fecini."' and fecha < '".$fecfin."' and activo = true"
            ]
        );

        return $this->response->setContent(json_encode($servs));
    }

    public function getbrigadasAction($idservicio)
    {
        $this->view->disable();
        $resp = array();
        if($idservicio === "-1"){
            $horaestimada = "08:00";
            $ids = $this->request->get("ids");
            $where = "";
        }
        else{
            $os = OrdenServicio::findFirstById($idservicio);
            $horaestimada = $os->hora;
            $ids = $os->idservicio;
            $where = "and os.id != ".$os->id;
        }


        $ser = Servicios::findFirstById($ids);
        $fecha = $this->request->get("fechaini");
        $duracion_trabajo = $ser->horas_estimadas + $ser->horas_tolereancia;
        $fini = $fecha." ".$horaestimada;
        $ffin = date('Y-m-d h:i', strtotime($fini)+ 60*60*$duracion_trabajo);

        $sqlb = "select * from servicio.brigadas where id not in (
            select idbrigada/*, to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') fecini,
            to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') + ((horas_estimadas + horas_tolereancia) || ' hour')::interval fecfin*/
            from servicio.orden_servicio os
            left join servicio.servicios s on os.idservicio = s.id
            where os.activo = true ".$where." and idbrigada is not null and 
            (select id from servicio.estado_orden_servicio  where clave = 'AG') = idestado
             and ( 
                (
                    to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') = to_timestamp('$fini', 'YYYY-MM-DD HH24:MI:SS') and 
                    to_timestamp(fecha || ' ' || horafin, 'YYYY-MM-DD HH24:MI:SS') = to_timestamp('$ffin', 'YYYY-MM-DD HH24:MI:SS')
                ) or
                (
                    to_timestamp('$fini', 'YYYY-MM-DD HH24:MI:SS') between 
                        to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') and 
                        to_timestamp(fecha || ' ' || horafin, 'YYYY-MM-DD HH24:MI:SS')
                ) or
                (
                    to_timestamp('$ffin', 'YYYY-MM-DD HH24:MI:SS') between 
                        to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') and 
                        to_timestamp(fecha || ' ' || horafin, 'YYYY-MM-DD HH24:MI:SS')
                )
            )
        )";
        $q = GenericSQL::getBySQL($sqlb);
        $resp = array();
        $brigadas = Brigada::findByActivo(true);
        $paramsini = Parameters::findFirstByClave("MINHORASERVICIO");
        $horaini = $paramsini->valor;
        $paramsfin = Parameters::findFirstByClave("MAXHORASERVICIO");
        $horafin = $paramsfin->valor;
        $horas = array();
        $disp = false;
        if(count($q) > 0){
            $disp = true;

        }

        $data = array();
        foreach ($brigadas as $b){
            $horas = array();
            for($i = (int)$horaini; $i <= (int)$horafin; $i++){
                $auxh = ($i<10 ? "0".$i : $i).":00";
                $fini = $fecha." ".$auxh;
                $ffin = date('Y-m-d h:i', strtotime($fini)+ 60*60*$duracion_trabajo);
                if($this->getDisponibilidad($idservicio, $b->id, $fini, $ffin)){
                    array_push($horas, $auxh);
                }
            }
            if($horas > 0){
                array_push($data, array(
                    "id" => $b->id,
                    "responsable" => $b->responsable,
                    "horas" => $horas
                ));
            }
        }
        $resp = array(
            "disp" => $disp,
            "data" => $data
        );

        /*
         *
         * fecini no este en los rangos, fecfin no esten en los angos, fecini y fecfin no sea igual al rango ni que los rangos esten dentroe de fecini - fec fin
         */

        return $this->response->setContent(json_encode($resp));
    }

    public function gethorariobrigadaAction($idbrigada)
    {
        $this->view->disable();
        $resp = array();
        if($idservicio === "-1"){
            $horaestimada = "08:00";
            $ids = $this->request->get("ids");
            $where = "";
        }
        else{
            $os = OrdenServicio::findFirstById($idservicio);
            $horaestimada = $os->hora;
            $ids = $os->idservicio;
            $where = "and os.id != ".$os->id;
        }


        $ser = Servicios::findFirstById($ids);
        $fecha = $this->request->get("fechaini");
        $duracion_trabajo = $ser->horas_estimadas + $ser->horas_tolereancia;
        $fini = $fecha." ".$horaestimada;
        $ffin = date('Y-m-d h:i', strtotime($fini)+ 60*60*$duracion_trabajo);

        $sqlb = "select * from servicio.brigadas where id not in (
            select idbrigada/*, to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') fecini,
            to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') + ((horas_estimadas + horas_tolereancia) || ' hour')::interval fecfin*/
            from servicio.orden_servicio os
            left join servicio.servicios s on os.idservicio = s.id
            where os.activo = true ".$where." and idbrigada is not null and 
            (select id from servicio.estado_orden_servicio  where clave = 'AG') = idestado
             and ( 
                (
                    to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') = to_timestamp('$fini', 'YYYY-MM-DD HH24:MI:SS') and 
                    to_timestamp(fecha || ' ' || horafin, 'YYYY-MM-DD HH24:MI:SS') = to_timestamp('$ffin', 'YYYY-MM-DD HH24:MI:SS')
                ) or
                (
                    to_timestamp('$fini', 'YYYY-MM-DD HH24:MI:SS') between 
                        to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') and 
                        to_timestamp(fecha || ' ' || horafin, 'YYYY-MM-DD HH24:MI:SS')
                ) or
                (
                    to_timestamp('$ffin', 'YYYY-MM-DD HH24:MI:SS') between 
                        to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') and 
                        to_timestamp(fecha || ' ' || horafin, 'YYYY-MM-DD HH24:MI:SS')
                )
            )
        )";
        $q = GenericSQL::getBySQL($sqlb);
        $resp = array();
        $brigadas = Brigada::findByActivo(true);
        $paramsini = Parameters::findFirstByClave("MINHORASERVICIO");
        $horaini = $paramsini->valor;
        $paramsfin = Parameters::findFirstByClave("MAXHORASERVICIO");
        $horafin = $paramsfin->valor;
        $horas = array();
        $disp = false;
        if(count($q) > 0){
            $disp = true;

        }

        $data = array();
        foreach ($brigadas as $b){
            $horas = array();
            for($i = (int)$horaini; $i <= (int)$horafin; $i++){
                $auxh = ($i<10 ? "0".$i : $i).":00";
                $fini = $fecha." ".$auxh;
                $ffin = date('Y-m-d h:i', strtotime($fini)+ 60*60*$duracion_trabajo);
                if($this->getDisponibilidad($idservicio, $b->id, $fini, $ffin)){
                    array_push($horas, $auxh);
                }
            }
            if($horas > 0){
                array_push($data, array(
                    "id" => $b->id,
                    "responsable" => $b->responsable,
                    "horas" => $horas
                ));
            }
        }
        $resp = array(
            "disp" => $disp,
            "data" => $data
        );

        /*
         *
         * fecini no este en los rangos, fecfin no esten en los angos, fecini y fecfin no sea igual al rango ni que los rangos esten dentroe de fecini - fec fin
         */

        return $this->response->setContent(json_encode($resp));
    }

    public function buscarClienteAction()
    {
        $request = $this->request;
        $response = $this->response;

        $sLimit = "";
        if ($request->hasQuery('start') && $request->get('length') != '-1') {
            $sLimit = " LIMIT " . $request->get('length') . " OFFSET " . $request->get('start');
        }

        $sWhere = "WHERE c.activo = true and c.vigente = true ";
        $columnsName = array();
        $columnsName[1] = "id_cliente";
        $columnsName[2] = "calle";
        $columnsName[3] = "numero";
        $columnsName[4] = "cruza1";
        $columnsName[5] = "cruza2";
        $columnsName[6] = "colonia";
        $columnsName[7] = "persona";
        $columnsName[8] = "propietario";
        $filterColumns = $request->get('columns');
        /* Individual column filtering */
        foreach($filterColumns as $i=>$filterColumn){
            if($i < 1){
                continue;
            }
            $search = $filterColumn["search"]["value"];
            if($filterColumn["searchable"] and $search != null and $search != "" ){
                if ( $sWhere == "" )
                {
                    $sWhere = "WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                if($i == 1){
                    $sWhere .= "id_cliente = $search ";
                }
                else if($i == 2){
                    $search = str_replace(' ', '', $search);
                    $sWhere .= "lower(coalesce(calle::text, '')||coalesce(calle_letra, '')) 
                    LIKE lower('%$search%') ";
                }
                else if($i == 3){
                    $search = str_replace(' ', '', $search);
                    $sWhere .= "lower(coalesce(numero::text, '')||coalesce(numero_letra, '')) 
                    LIKE lower('%$search%') ";
                }
                else if($i == 6) {
                    $sWhere .= "idcolonia = $search ";
                }
                else if($i == 7){
                    $aSearch = explode(' ', $search);
                    if(count($aSearch) > 1){
                        $newParams = $this->eliminarPreposiciones($aSearch);
                    }
                    else{
                        $newParams = $aSearch;
                    }

                    foreach ($newParams as $valor) {
                        $sWhere .= "COALESCE(trim(lower(c.apepat)), '')||' '||COALESCE(trim(lower(c.apemat)), '')||' '||COALESCE(trim(lower(c.nombres)), '') LIKE lower('%$valor%') AND ";
                    }
                    $sWhere = substr($sWhere, 0, -4);
                }
                else{
                    $sWhere .= "lower($columnsName[$i]::text) LIKE lower('%$search%') ";
                }
            }
        }

        $columns = $request->get('columns');

        /*
         * Ordering
         */
        $sOrder = "ORDER BY ";
        $orderColumns = $request->get('order');
        foreach($orderColumns as $orderColumn){
            $sOrder .= $columnsName[$orderColumn["column"]]." ".$orderColumn["dir"].", ";
        }
        $sOrder = substr_replace( $sOrder, "", -2 );
        if ( $sOrder == "ORDER BY" ) {
            $sOrder = "";
        }

        $sql = "select c.*, 
        COALESCE(c.apepat, '')||' '||COALESCE(c.apemat, '')||' '||COALESCE(c.nombres, '')  persona,
        COALESCE(c.apepat_propietario, '')||' '||COALESCE(c.apemat_propietario, '')||' '||COALESCE(c.nombres_propietario, '')  propietario,
        COALESCE(col.nombre, '')||', '||COALESCE(col.localidad, '') colonia 
        from cliente.cliente c
        left join cliente.colonia as col on col.id = c.idcolonia
        ".$sWhere.$sOrder.$sLimit;
//        echo $sql;
//        die;
        $results = Clientes::findByQuery($sql);

        $sqlfiltered = "select count(*) count from cliente.cliente c ".$sWhere;
        $filtered = Clientes::findByQuery($sqlfiltered);

        $totalRecords = Clientes::count();

        $resp = new \stdClass();
        $resp->draw = $request->get('draw');
        $resp->recordsTotal = $totalRecords;
        $resp->recordsFiltered = $filtered[0]->count;
        $data = array();

        $ids = "";
        $count = 0;
        foreach($results as $res){
            $vigente = $res->vigente ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                : '<i class="fa fa-remove" style="color: red" title="Activo"></i>';
            $calle = ($res->calle != null and $res->calle != "") ? trim($res->calle) : "";
            $calleLetra = ($res->calle_letra != null and $res->calle_letra != "") ? " ".trim($res->calle_letra) : "";
            $calle = $calle.$calleLetra;

            $numero = ($res->numero != null and $res->numero != "") ? trim($res->numero) : "";
            $numeroLetra = ($res->numero_letra != null and $res->numero_letra != "") ? " ".trim($res->numero_letra) : "";
            $numero = $numero.$numeroLetra;

            $cruz1 = ($res->cruza1 != null and $res->cruza1 != "") ? trim($res->cruza1) : "";
            $cruz1Letra = ($res->letra_cruza1 != null and $res->letra_cruza1 != "") ? trim($res->letra_cruza1) : "";
            $cruz1 = $cruz1.$cruz1Letra;

            $cruz2 = ($res->cruza2 != null and $res->cruza2 != "") ? trim($res->cruza2) : "";
            $cruz2Letra = ($res->letra_cruza2 != null and $res->letra_cruza2 != "") ? trim($res->letra_cruza2) : "";
            $cruz2 = $cruz2.$cruz2Letra;

            $colonia = $res->colonia;
//            if($res->idcolonia != null){
//                $aux = Colonias::findFirstById($res->idcolonia);
//                if($aux !== false){
//                    $colonia = "$aux->nombre ($aux->alias)";
//                }
//            }

            $persona = "";
            if($res->apepat != null and $res->apepat != ""){
                $persona .= $res->apepat;
            }
            if($res->apemat != null and $res->apemat != ""){
                if($persona !=  ""){
                    $persona .= " ";
                }
                $persona .= $res->apemat;
            }
            if($res->nombres != null and $res->nombres != ""){
                if($persona !=  ""){
                    $persona .= " ";
                }
                $persona .= $res->nombres;
            }

            $propietario = "";
            if($res->apepat_propietario != null and $res->apepat_propietario != ""){
                $propietario .= $res->apepat_propietario;
            }
            if($res->apemat_propietario != null and $res->apemat_propietario != ""){
                if($propietario !=  ""){
                    $propietario .= " ";
                }
                $propietario .= $res->apemat_propietario;
            }
            if($res->nombres_propietario != null and $res->nombres_propietario != ""){
                if($propietario !=  ""){
                    $propietario .= " ";
                }
                $propietario .= $res->nombres_propietario;
            }

            $meses = array('enero','febrero','marzo','abril','mayo','junio','julio',
                'agosto','septiembre','octubre','noviembre','diciembre');
            $ultmespago = ($res->ultimo_mes_pago != null) ? ucfirst($meses[intval($res->ultimo_mes_pago) - 1]): "";
            $ultaniopago = ($res->ultimo_anio_pago != null) ? $res->ultimo_anio_pago : "";

            $d = array();
            $d["0"] = null;
            $d["1"] = $res->id_cliente;
            $d["2"] = $calle;
            $d["3"] = $numero;
            $d["4"] = $cruz1;
            $d["5"] = $cruz2;
            $d["6"] = $colonia;
            $d["7"] = $persona;
            $d["8"] = $propietario;
            $d["DT_RowId"] = "cliente-$res->id_cliente";
            array_push($data, $d);
        }
        $resp->data = $data;
        $response->setContent(json_encode($resp));
        return $response;
    }

    function eliminarPreposiciones($datos){
        $preposiciones = array('de', 'los', 'la', 'y');
        $newData = array();
        foreach ($datos as $valor) {
            if(!in_array($valor, $preposiciones)){
                array_push($newData,$valor);
            }
        }
        return $newData;
    }

    public function saveAction($idos){
        $this->view->disable();
        $os = OrdenServicio::findFirstById($idos);
        $rawBody = $this->request->getJsonRawBody();
        $clave = $rawBody->clave;
        $observaciones = $rawBody->observaciones;

        $identity = $this->session->get('auth-identity');
        $iduser = $identity['id'];
        $this->db->begin();

        $es = EstadoOrdenServicio::findFirstByClave($clave);
        $sor = new SeguimientoOrdenServicio();
        $sor->idestado_anterior = $os->idestado;
        $sor->idestado_nuevo = $es->id;
        $sor->idusuario_creacion = $iduser;
        $sor->observaciones = $observaciones;

        $os->fecha_modificacion = date("c");
        $os->observaciones = $observaciones;
        $os->idestado = $es->id;
        switch ($clave){
            case "AG":
                $os->idbrigada = $rawBody->responsable;
                $os->fecha = $rawBody->fecha;
                $os->hora = $rawBody->hora;
                $os->horafin = $rawBody->horafin;
                $os->idusuario_asignacion = $iduser;
                $os->fecha_asignacion = date("c");
                break;
            case "CA":
                break;
            case "ATE":
                break;
            case "FIN":
                $os->factura = $rawBody->factura;
                $os->duracion = $rawBody->duracion;
                $os->total = $rawBody->costo;
                $os->costo_extra = $rawBody->materialextra;
                break;
        }

        if($sor->save()){
            if($os->save()){
                $this->db->commit();
                $this->response->setContent(
                    json_encode(OrdenServicio::findFirstById($idos))
                );
            }
            else{
                $this->response->setStatusCode(500);
                $this->db->rollback();
            }
        }
        else{
            $this->response->setStatusCode(500);
            $this->db->rollback();
        }
        return $this->response;
    }

    private function getDisponibilidad($idos, $idbrigada, $fini, $ffin){
        $sql = "
            select idbrigada
            from servicio.orden_servicio os
            left join servicio.servicios s on os.idservicio = s.id
            where os.activo = true and os.id != ".$idos." and idbrigada = $idbrigada and 
            (select id from servicio.estado_orden_servicio  where clave = 'AG') = idestado
             and ( 
                (
                    to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') = to_timestamp('$fini', 'YYYY-MM-DD HH24:MI:SS') and 
                    to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') + ((horas_estimadas + horas_tolereancia) || ' hour')::interval = to_timestamp('$ffin', 'YYYY-MM-DD HH24:MI:SS')
                ) or
                (
                    to_timestamp('$fini', 'YYYY-MM-DD HH24:MI:SS') between 
                        to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') and 
                        to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') + ((horas_estimadas + horas_tolereancia) || ' hour')::interval
                ) or
                (
                    to_timestamp('$ffin', 'YYYY-MM-DD HH24:MI:SS') between 
                        to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') and 
                        to_timestamp(fecha || ' ' || hora, 'YYYY-MM-DD HH24:MI:SS') + ((horas_estimadas + horas_tolereancia) || ' hour')::interval
                )
            )
        ";
        $q = GenericSQL::getBySQL($sql);
        return count($q) === 0;
    }

    public function clientesagendarcitaAction($id){
        $bCommit = false; //datos guardados
        $cError = ""; //Errores
        $os = null;
        $orden = null;

        if($this->request->isPost()){
            if(isset($id)){
                $this->db->begin();

                $rawBody = $this->request->getJsonRawBody();
                $lista_servicios = $rawBody->servicios;
                $fecha = empty($rawBody->fecha) ? date("d/m/Y") : $rawBody->fecha;
                $hora = $rawBody->hora;
                $horafin = $rawBody->horafin;
                $correo = $rawBody->correo;
                $telefono = $rawBody->telefono;
                $responsable = $rawBody->responsable;
                $referencia = $rawBody->referencia;
                $descripcion = $rawBody->descripcion;

                $identity = $this->session->get('auth-identity');
                $iduser = $identity['id'];

                $cotizar = $rawBody->cotizar;
                $claveEstado = $cotizar;
                $eos = EstadoOrdenServicio::findFirstByClave($claveEstado);

                $os = new OrdenServicio();
                $os->idservicio = null;
                $os->idcliente = $id;
                $os->fecha = implode("-", array_reverse(explode("/", $fecha)));
                $os->hora = $hora;
                $os->horafin = $horafin;
                $os->correo = $correo;
                $os->telefono = $telefono;
                $os->idestado = $eos->id;
                $os->idusuario_creacion = $iduser;
                $os->idbrigada = $responsable;
                $os->fecha_asignacion = date("c");
                $os->idusuario_asignacion = $iduser;
                $os->referencia = $referencia ? $referencia : null;
                $os->descripcion = $descripcion ? $descripcion : null;

                if($os->save()){
                    $os->refresh();
                    $orden = $os;

                    $costoTotal=0;
                    foreach ($lista_servicios as $key => $valor){
                        $id = $valor[1];
                        $servicio = $valor[2];
                        $costoTotal = $costoTotal + $valor[5];

                        $ordenServicioDetalle = new OrdenServicioDetalle();
                        $ordenServicioDetalle->idorden_servicio = $os->id;
                        $ordenServicioDetalle->costo = $valor[5];
                        $ordenServicioDetalle->activo = true;
                        $ordenServicioDetalle->fecha_creacion = date("c");
                        $ordenServicioDetalle->fecha_modificacion = date("c");
                        $ordenServicioDetalle->idservicio = $valor[1];

                        if(!$ordenServicioDetalle->save()){
                            $bCommit = false;
                            $this->db->rollback();
                            foreach ($ordenServicioDetalle->getMessages() as $message) {
                                $this->logger->error("save-orden-servicio-detalle: ".$message->getMessage());
                            }
                            $this->response->setStatusCode(500, "No se pudo guardar el servicio detalle");
                            return $this->response;
                        }else{
                            $bCommit = true;
                            $ordenServicioDetalle->refresh();
                            $this->logger->info("Guardar orden servicio y servicio detalle");
                        }
                    }

                    $ordenNew = OrdenServicio::findFirst($os->id);
                    if($ordenNew !== false){
                        $ordenNew->total = $costoTotal;
                        $ordenNew->iva = (0.16 * $costoTotal);
                        $ordenNew->fecha_modificacion = date("c");

                        if(!$ordenNew->save()){
                            foreach ($ordenNew->getMessages() as $message) {
                                $this->logger->error("save-orden-servicio total: ".$message->getMessage());
                                $this->logger->info("save-orden-servicio total: ".$message->getMessage());
                            }
                        }
                    }

                }else{
                    $bCommit = false;
                    $this->db->rollback();
                    foreach ($os->getMessages() as $message) {
                        $this->logger->error("save-orden-servicio: ".$message->getMessage());
                    }
                    $this->response->setStatusCode(500, "No se pudo guardar la orden del servicio");
                    return $this->response;
                }

            }else{
                $this->response->setStatusCode(400, "El cliente es necesario");
            }
        }else{
            $this->response->setStatusCode(501);
        }

        if($bCommit){
            $this->db->commit();
            $this->response->setStatusCode(200, "OK");
            $this->response->setContent(json_encode(array("lError" => false, "cMensaje" => "la orden del servicio y servicio detalle se guardaron Correctamente", "orden" => $orden)));
        }else{
            $this->db->rollback();
            $this->response->setStatusCode(501, $cError);
            $this->response->setContent(json_encode(array("lError" => true, "cMensaje" => "Error al Guardar la Orden del Servicio.")));
        }

        return $this->response;
    }

    public function getOrdenesAction($idcliente){
        $request = $this->request;
        $response = $this->response;
        $sql = "SELECT os.id, os.telefono, os.fecha, os.total, s.nombre as servicio, calle, calle_letra, numero, numero_letra, cruza1, letra_cruza1, cruza2, letra_cruza2,
            trim(COALESCE(c.apepat, '')||' '||COALESCE(c.apemat, '')||' '||COALESCE(c.nombres, ''))  persona,
                COALESCE(col.nombre, '')||', '||COALESCE(col.localidad, '') colonia, c.idcolonia, es.nombre estado, es.color, os.observaciones, os.costo_extra  
        FROM servicio.orden_servicio os
        left join servicio.servicios s on os.idservicio = s.id
        left join cliente.cliente c on os.idcliente = c.id_cliente
        left join cliente.colonia as col on col.id = c.idcolonia 
        left join servicio.estado_orden_servicio es on os.idestado = es.id
        where os.idcliente = $idcliente and os.activo = true";

        $res = GenericSQL::getBySQL($sql);
        $data = [];
        foreach ($res as $r){
            array_push($data, array(
                "",
                $r->id,
                ' <span style="display: inline-block;background-color: #'.$r->color.'; border-radius: 15px; padding: 10px;">'.$r->estado.'</span>',
                "C. ".$r->calle.$r->calle_letra." #".$r->numero.$r->numero_letra." ".$r->colonia.(!empty($r->persona) ? " (".$r->persona.")" : ""),
                $r->telefono,
                $r->servicio,
                !empty($r->fecha) ? date('d/m/Y', strtotime($r->fecha)) : "",
                $r->total,
                $r->costo_extra,
                $r->observaciones
            ));
        }

        $response->setContent(json_encode($data));
        return $response;
    }

    public function getClientesOrdenesAction(){
        $request = $this->request;
        $response = $this->response;

        $calle = $request->get("calle");
        $numero = $request->get("numero");
        $colonia = $request->get("colonia");
        $telefono = $request->get("telefono");
        $nombre = $request->get("nombre");
        $tipo = $request->get("tipo");

        $where = "where c.activo = true";
        if($tipo == "tel"){
            $where .= " and os.telefono = '$telefono'";
        }
        elseif($tipo == "nombre"){
            $aSearch = explode(' ', $nombre);
            if(count($aSearch) > 1){
                $newParams = $this->eliminarPreposiciones($aSearch);
            }
            else{
                $newParams = $aSearch;
            }

            $where .= " and ";
            foreach ($newParams as $valor) {
                $where .= " trim(COALESCE(c.apepat, '')||' '||COALESCE(c.apemat, '')||' '||COALESCE(c.nombres, '')) ILIKE lower('%$valor%') AND ";
            }
            $where = substr($where, 0, -4);
        }
        else{
            if(!empty($calle)){
                $calle = str_replace(' ', '', $calle);
                $where .= " and lower(coalesce(calle::text, '')||coalesce(calle_letra, '')) 
                    LIKE lower('%$calle%') ";
            }
            if(!empty($numero)){
                $numero = str_replace(' ', '', $numero);
                $where .= " and lower(coalesce(numero::text, '')||coalesce(numero_letra, '')) 
                    LIKE lower('%$numero%') ";
            }
            if(!empty($colonia)) {
                $where .= " and idcolonia = $colonia ";
            }
        }

        $sql = "SELECT distinct on (c.id_cliente)  c.id_cliente, calle, calle_letra, numero, numero_letra, cruza1, letra_cruza1, cruza2, letra_cruza2,
            trim(COALESCE(c.apepat, '')||' '||COALESCE(c.apemat, '')||' '||COALESCE(c.nombres, ''))  persona,
                COALESCE(col.nombre, '')||', '||COALESCE(col.localidad, '') colonia, c.idcolonia, c.vigente ,
                case when os.correo is null then c.correo else os.correo end correo,
                 case when os.telefono is null then c.telefono else os.telefono end telefono, c.isservicio
        from cliente.cliente c
        left join cliente.colonia as col on col.id = c.idcolonia 
        left join servicio.orden_servicio os on os.idcliente = c.id_cliente
        $where order by c.id_cliente, os.id desc ";

        $this->logger->info($sql);
        $res = GenericSQL::getBySQL($sql);
        $data = [];
        foreach ($res as $r){
            array_push($data, array(
                "",
                $r->id_cliente,
                $r->persona,
                "C. ".$r->calle.$r->calle_letra." #".$r->numero.$r->numero_letra." ".$r->colonia,
                $r->vigente,
                $r->correo,
                $r->telefono,
                $r->isservicio
            ));
        }

        $response->setContent(json_encode($data));
        return $response;
    }


    public function saveclienteAction()
    {
        $this->view->disable();

        if ($this->request->isPost() == true) {
            if($this->request->isAjax() == true) {
                //$form = new ClientesForm();
                $identity = $this->auth->getIdentity();
                $idUser = $identity["id"];
                $idCliente = $this->request->getPost('id', 'int');
                $resp = new \stdClass();
                $resp->nuevo = true;
                $clientes = new Clientes();
                $clientes->id_usuario = $idUser;
                $clientes->fecha_modificacion = date("c");
                $clientes->fecha_creacion = date("c");

                $clientes->idusuario_modifico = $idUser;

                $calle = $this->request->getPost('calle', 'int');
                $calle = ($calle != null and $calle != "") ? intval($calle) : null;
                $calleLetra = $this->request->getPost('calle_letra', 'striptags');
                $calleLetra = ($calleLetra != null and $calleLetra != "") ? $calleLetra : null;
                $tipoCalle = $this->request->getPost('tipo_calle', 'striptags');
                $tipoCalle = ($tipoCalle != null and $tipoCalle != "") ? $tipoCalle : null;
                $numero = $this->request->getPost('numero', 'int');
                $numero = ($numero != null and $numero != "") ? intval($numero) : null;
                $numeroLetra = $this->request->getPost('numero_letra', 'striptags');
                $numeroLetra = ($numeroLetra != null and $numeroLetra != "") ? $numeroLetra : null;
                $tipoNumero = $this->request->getPost('tipo_numero', 'striptags');
                $tipoNumero = ($tipoNumero != null and $tipoNumero != "") ? $tipoNumero : null;
                $cruza1 = $this->request->getPost('cruza1', 'striptags');
                $cruza1 = ($cruza1 != null and $cruza1 != "") ? $this->request->getPost('cruza1', 'striptags') : null;
                $cruza1Letra = $this->request->getPost('letra_cruza1', 'striptags');
                $cruza1Letra = ($cruza1Letra != null and $cruza1Letra != "") ? $cruza1Letra : null;
                $tipoCruza1 = $this->request->getPost('tipo_cruza1', 'striptags');
                $tipoCruza1 = ($tipoCruza1 != null and $tipoCruza1 != "") ? $tipoCruza1 : null;
                $cruza2 = $this->request->getPost('cruza2', 'striptags');
                $cruza2 = ($cruza2 != null and $cruza2 != "") ? $cruza2 : null;
                $cruza2Letra = $this->request->getPost('letra_cruza2', 'striptags');
                $cruza2Letra = ($cruza2Letra != null and $cruza2Letra != "") ? $cruza2Letra : null;
                $tipoCruza2 = $this->request->getPost('tipo_cruza2', 'striptags');
                $tipoCruza2 = ($tipoCruza2 != null and $tipoCruza2 != "") ? $tipoCruza2 : null;
                $idcolonia = $this->request->getPost('idcolonia', 'int');
                $idcolonia = ($idcolonia != null and $idcolonia != "") ? intval($idcolonia) : null;
                $apepat = $this->request->getPost('apepat', 'striptags');
                $apepat = ($apepat != null and $apepat != "") ? $apepat : null;
                $apemat = $this->request->getPost('apemat', 'striptags');
                $apemat = ($apemat != null and $apemat != "") ? $apemat : null;
                $nombres = $this->request->getPost('nombres', 'striptags');
                $nombres = ($nombres != null and $nombres != "") ? $nombres : null;
                $referencia_ubicacion = $this->request->getPost('referencia_ubicacion', 'striptags');
                $referencia_ubicacion = ($referencia_ubicacion != null and $referencia_ubicacion != "") ? $referencia_ubicacion : null;
                $telefono = $this->request->getPost('telefono', 'int');
                $telefono = str_replace(array("(", ")", " ", "-"), array("", "", "", ""), $telefono);
                $telefono = ($telefono != null and $telefono != "") ? $telefono : null;
                $correo = $this->request->getPost('correo', 'striptags');
                $correo = ($correo != null and $correo != "") ? $correo : null;

                $validaDireccion = Clientes::validateDireccion(null, null, $calle, $calleLetra, $numero, $numeroLetra, $idcolonia);
                if($validaDireccion->valid()){
                    $this->response->setStatusCode(409, "Conflict");
                    return $this->response;
                }

                $clientes->assign(array(
                    'calle' => $calle,
                    'calle_letra' => $calleLetra,
                    'tipo_calle' => $tipoCalle,
                    'numero' => $numero,
                    'numero_letra' => $numeroLetra,
                    'tipo_numero' => $tipoNumero,
                    'cruza1' => $cruza1,
                    'letra_cruza1' => $cruza1Letra,
                    'tipo_cruza1' => $tipoCruza1,
                    'cruza2' => $cruza2,
                    'letra_cruza2' => $cruza2Letra,
                    'tipo_cruza2' => $tipoCruza2,
                    'idcolonia' => $idcolonia,
                    'apepat' => $apepat,
                    'apemat' => $apemat,
                    'nombres' => $nombres,
                    'activo' => TRUE,
                    'referencia_ubicacion' => $referencia_ubicacion,
                    'telefono' => $telefono,
                    'correo' => $correo,
                    'vigente' => false,
                    'isservicio' => true
                ));

//                var_dump($clientes->toArray());
//                die;

                if($clientes->save() === false) {
                    foreach ($clientes->getMessages() as $message) {
                        $this->logger->info("(save-dirper): " . $message);
                    }
                    $this->response->setStatusCode(500, "Internal Servere Error");
                }
                else{
                    $persona = "";
                    if($apepat != null and $apepat != ""){
                        $persona .= $apepat;
                    }
                    if($apemat != null and $apemat != ""){
                        if($persona !=  ""){
                            $persona .= " ";
                        }
                        $persona .= $apemat;
                    }
                    if($nombres != null and $nombres != ""){
                        if($persona !=  ""){
                            $persona .= " ";
                        }
                        $persona .= $nombres;
                    }

                    $calle = ($calle != null and $calle != "") ? trim($calle) : "";
                    $calleLetra = ($calleLetra != null and $calleLetra != "") ? " ".trim($calleLetra) : "";
                    $calle = $calle.$calleLetra;

                    $numero = ($numero != null and $numero != "") ? trim($numero) : "";
                    $numeroLetra = ($numeroLetra != null and $numeroLetra != "") ? " ".trim($numeroLetra) : "";
                    $numero = $numero.$numeroLetra;

                    $cruz1 = ($cruza1 != null and $cruza1 != "") ? trim($cruza1) : "";
                    $cruz1Letra = ($cruza1Letra != null and $cruza1Letra != "") ? trim($cruza1Letra) : "";
                    $cruz1 = $cruz1.$cruz1Letra;

                    $cruz2 = ($cruza2 != null and $cruza2 != "") ? trim($cruza2) : "";
                    $cruz2Letra = ($cruza2Letra != null and $cruza2Letra != "") ? trim($cruza2Letra) : "";
                    $cruz2 = $cruz2.$cruz2Letra;

                    $colonia = "";
                    if($idcolonia != null and $idcolonia != ""){
                        $eColonia = Colonias::findFirstById($idcolonia);
                        if($eColonia !== false){
                            $colonia = $eColonia->nombre.", ".$eColonia->localidad;
                        }
                    }

                    $direccion = "C. ".$calle." #".$numero;
                    if($cruz1 && $cruz2){
                        $direccion .= " X ".$cruz1." Y ".$cruz2;
                    }
                    elseif ($cruz1){
                        $direccion .= " X ".$cruz1;
                    }
                    elseif($cruz2){
                        $direccion .= " X ".$cruz2;
                    }
                    $direccion .= " ".$colonia;

                    $resp->id = $clientes->id_cliente;
                    $resp->calle = $calle;
                    $resp->numero = $numero;
                    $resp->cruz1 = $cruz1;
                    $resp->cruz2 = $cruz2;
                    $resp->colonia = $colonia;
                    $resp->persona = $persona;
                    $resp->direccion = $direccion;
                    $this->response->setContent(json_encode($resp));
                }
            }
            else{
                $this->response->setStatusCode(400, "Bad Request");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    public function getServicioByiDAction(){
        if ($this->request->isGet() == true){
            $view = clone $this->view;
            $this->view->disable();
            $view->start();

            $id = $this->request->get("id");
            $id = intval($id);

            $servicio = Servicios::findFirst($id);
            if($servicio !== false){
                $this->response->setStatusCode(200, "OK");
                $this->response->setContent(json_encode(array("lError" => false, "cMensaje" => "Servicio obtenido Correctamente", "servicio" => $servicio)));
            }else{
                $this->response->setStatusCode(201, "No content");
                $this->response->setContent(json_encode(array("lError" => true, "cMensaje" => "Servicio no encontrado")));
            }
        }else{
            $this->response->setStatusCode(404);
        }
        return $this->response;
    }

    public function getServiciosByIdOrdenAction(){
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];

        if($this->request->isAjax()== true && $this->request->isPost() == true) {
            $rawBody = $this->request->getJsonRawBody();
            $idorden_servicio = $rawBody->byidOrdenServicio;
            $where=" where activo=true and idorden_servicio=$idorden_servicio";
            $sql="SELECT * FROM servicio.view_serviciosdetalles $where";

            $services = GenericSQL::getBySQL($sql);

            $os = OrdenServicio::findFirst($idorden_servicio);

            $this->response->setContent(json_encode(array("orden" => $os, "servicios" => $this->getDataTable($services))));
            return $this->response;

            /*$resultset  = $this->modelsManager->createQuery("SELECT * FROM \Vokuro\Models\ServiciosDetalles $where")
                ->execute();
            $dataTables = new DataTableGeo();
            $dataTables->fromResultSet($resultset)->sendResponse();*/
        }
    }

    private function getDataTable($services){
        $resp = array();
        foreach ($services as $service){
            $actions='';

            $costo_final = $service->costo_final != null ? $service->costo_final : $service->costo;
            if($this->acl->isAllowedUser("servicios", "edit") and $service->clave_estado == 'AG'){
                $actions.='<input type="text" id="cost" minlength="0" class="form-control EditCosto" value="'. $costo_final .'">';
            }else{
                $actions.= $costo_final;
            }

            array_push($resp, array(
                "",
                $service->id,
                $service->nombre,
                $service->cantidad,
                $costo_final,
                $costo_final,
            ));
        }
        return $resp;
    }

    public function updateCostoServicioAction()
    {
        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];
        $cError = "";

        if($this->request->isPost()){
            $view = clone $this->view;
            $this->view->disable();
            $view->start();

            $rawBody = $this->request->getJsonRawBody();

            $servicioDetalle = OrdenServicioDetalle::findFirst($rawBody->id);
            if($servicioDetalle !== false){
                $this->db->begin();

                $servicioDetalle->costo_final = $rawBody->costo;
                $servicioDetalle->fecha_modificacion = date("c");

                if(!$servicioDetalle->save()){
                    $this->db->rollback();
                    $bCommit = false;
                    foreach ($servicioDetalle->getMessages() as $message) {
                        $cError .= "[" . $message . "]";
                        $this->logger->info("save OrdenServicioDetalle error: " . $message);
                    }
                }else{
                    $this->db->commit();

                    $where=" where activo=true and idorden_servicio=$servicioDetalle->idorden_servicio";
                    $sql="SELECT * FROM servicio.view_serviciosdetalles $where";
                    $services = GenericSQL::getBySQL($sql);
                    if(count($services)){
                        $costoTotal = 0;
                        foreach ($services as $key => $item){
                            $total = $item->costo_final != null ? $item->costo_final : $item->costo;
                            $costoTotal = $costoTotal + $total;
                        }

                        $ordenService = OrdenServicio::findFirst($servicioDetalle->idorden_servicio);
                        if($ordenService !== false){
                            $ordenService->total = $costoTotal;
                            $ordenService->fecha_modificacion = date("c");

                            if(!$ordenService->save()){
                                foreach ($ordenService->getMessages() as $message) {
                                    $this->logger->error("save-orden-servicio total: ".$message->getMessage());
                                    $this->logger->info("save-orden-servicio total: ".$message->getMessage());
                                }
                            }
                        }

                    }

                    $this->response->setStatusCode(201, "Cambio de costo del servicio Guardado satisfactoriamente.");
                    $this->response->setContent(json_encode(array("lError" => false, "cMensaje" => "Cambio de costo del servicio Guardado satisfactoriamente", "servicio" => $servicioDetalle)));
                }

            }else{
                $this->logger->info("No se encontro la orden servicio detalle con la clave: ".$rawBody->id);
                $this->response->setStatusCode(201);
                $this->response->setContent(json_encode(array("lError" => true, "cMensaje" => "No se encontro la orden servicio detalle con la clave: ".$rawBody->id)));
            }

        }else{
            $this->response->setStatusCode(404);
        }
        return $this->response;
    }

    public function exportarAction(){
        set_time_limit(300);
        ini_set('memory_limit', '1024M');

        $idcliente = $this->request->get('cliente');
        $finicio = $this->request->get('finicio');
        $ffin = $this->request->get('ffin');
        $estado = $this->request->get('estado');

        if(!empty($finicio)) {
            $finicio = date("Y-m-d", strtotime(str_replace('/', '-', $finicio )));
        }
        if(!empty($ffin)) {
            $ffin = date("Y-m-d", strtotime(str_replace('/', '-', $ffin )));
        }

        $where = " where os.activo = true ";
        
        if(!empty($idcliente)) {
            $where .= " and os.idcliente = $idcliente ";
        }
        if(!empty($finicio)) {
            $where .= " and os.fecha >= '$finicio' ";
        }
        if(!empty($ffin)) {
            $where .= " and os.fecha <= '$ffin' ";
        }
        if(!empty($estado)) {
            $where .= " and os.idestado = $estado ";
        }

        $sql="select os.*, count(distinct osd.id) num_servicios, eos.nombre estado, c.nombres, c.apepat, c.apemat,
                c.calle, c.calle_letra, c.numero, c.numero_letra, c.cruza1, c.letra_cruza1, c.cruza2, c.letra_cruza2, c.idcolonia, col.nombre, c.localidad, c.folio_catastral    
                from servicio.orden_servicio os
                join servicio.orden_servicio_detalle osd on (os.id = osd.idorden_servicio and osd.activo = true)
                join servicio.estado_orden_servicio eos on (os.idestado = eos.id and eos.activo = true)
                join cliente.cliente c on (c.id_cliente = os.idcliente)
                join cliente.colonia col on (c.idcolonia = col.id)
            $where 
            group by os.id, eos.nombre, c.nombres, c.apepat, c.apemat, c.calle, c.calle_letra, c.numero, c.numero_letra, c.cruza1, c.letra_cruza1, c.cruza2, c.letra_cruza2, c.idcolonia, col.nombre, c.localidad, c.folio_catastral";

        $ordenes = GenericSQL::getBySQL($sql);
        
        // ORDENES DE SERVICIO - HOJA 1

        PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
        //PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);
        $activeSheet = $objPHPExcel->getActiveSheet();
        $activeSheet->setTitle("Ordenes de Servicio");

        $counter = 2;
        $activeSheet->setCellValue('B'.$counter, "ORDENES DE SERVICIO");
        $activeSheet->getStyle("B".$counter.":B".$counter)->getFont()->setBold(true);
        
        $counter = 4;
        $activeSheet->setCellValue('A'.$counter, "CLIENTE");
        $activeSheet->setCellValue('B'.$counter, "NOMBRE");
        $activeSheet->setCellValue('C'.$counter, "APE. PATERNO");
        $activeSheet->setCellValue('D'.$counter, "APE. MATERNO");
        $activeSheet->setCellValue('E'.$counter, "TELEFONO");
        $activeSheet->setCellValue('F'.$counter, "CORREO");
        $activeSheet->setCellValue('G'.$counter, "FECHA SOLICITUD");
        $activeSheet->setCellValue('H'.$counter, "FECHA DEL SERVICIO");
        $activeSheet->setCellValue('I'.$counter, "HORA DEL SERVICIO");
        $activeSheet->setCellValue('J'.$counter, "ESTADO");
        $activeSheet->setCellValue('K'.$counter, "ORDEN");
        $activeSheet->setCellValue('L'.$counter, "NÚM. SERVICIOS");
        $activeSheet->setCellValue('M'.$counter, "COSTO TOTAL");
        $activeSheet->setCellValue('N'.$counter, "MATERIAL EXTRA");
        $activeSheet->setCellValue('O'.$counter, "FOLIO CATASTRAL");
        $activeSheet->setCellValue('P'.$counter, "CALLE");
        $activeSheet->setCellValue('Q'.$counter, "LETRA CALLE");
        $activeSheet->setCellValue('R'.$counter, "NÚMERO");
        $activeSheet->setCellValue('S'.$counter, "LETRA NÚMERO");
        $activeSheet->setCellValue('T'.$counter, "CRUZ 1");
        $activeSheet->setCellValue('U'.$counter, "LETRA CRUZ 1");
        $activeSheet->setCellValue('V'.$counter, "CRUZ 2");
        $activeSheet->setCellValue('W'.$counter, "LETRA CRUZ 2");
        $activeSheet->setCellValue('X'.$counter, "COLONIA");
        $activeSheet->getStyle("A".$counter.":X".$counter)->getFont()->setBold(true);
        $activeSheet->getStyle("A".$counter.":X".$counter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("95C01E");

        if($ordenes){
            foreach ($ordenes as $orden){   
                $counter++;
                $color = "F5F5F5";

                $activeSheet->setCellValue('A'.$counter, $orden->idcliente);
                $activeSheet->setCellValue('B'.$counter, $orden->nombres);
                $activeSheet->setCellValue('C'.$counter, $orden->apepat);
                $activeSheet->setCellValue('D'.$counter, $orden->apemat);
                $activeSheet->setCellValue('E'.$counter, $orden->telefono);
                $activeSheet->setCellValue('F'.$counter, $orden->correo);
                $activeSheet->setCellValue('G'.$counter, date("d/m/Y H:m", strtotime($orden->fecha_creacion)));
                $activeSheet->setCellValue('H'.$counter, date("d/m/Y", strtotime($orden->fecha)));
                $activeSheet->setCellValue('I'.$counter, $orden->hora);
                $activeSheet->setCellValue('J'.$counter, $orden->estado);
                $activeSheet->setCellValue('K'.$counter, $orden->id);
                $activeSheet->setCellValue('L'.$counter, $orden->num_servicios);
                $activeSheet->setCellValue('M'.$counter, $orden->total);
                $activeSheet->setCellValue('N'.$counter, $orden->costo_extra);
                $activeSheet->setCellValue('O'.$counter, $orden->folio_catastral);
                $activeSheet->setCellValue('P'.$counter, $orden->calle);
                $activeSheet->setCellValue('Q'.$counter, $orden->calle_letra);
                $activeSheet->setCellValue('R'.$counter, $orden->numero);
                $activeSheet->setCellValue('S'.$counter, $orden->numero_letra);
                $activeSheet->setCellValue('T'.$counter, $orden->cruza1);
                $activeSheet->setCellValue('U'.$counter, $orden->letra_cruza1);
                $activeSheet->setCellValue('V'.$counter, $orden->cruza2);
                $activeSheet->setCellValue('W'.$counter, $orden->letra_cruza2);
                $activeSheet->setCellValue('X'.$counter, $orden->nombre);
                
                //$activeSheet->getStyle("A".$counter.":Q".$counter)->getFont()->setBold(true);
                $activeSheet->getStyle("A".$counter.":X".$counter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB(str_replace("#", "", $color));
                             
            }
            $counter++;
            $activeSheet->setCellValue('A'.$counter, "TOTALES");
            $activeSheet->setCellValue('L'.$counter,  '=SUM(L5:L'.($counter -1).')' ); // suma de num_servicios
            $activeSheet->setCellValue('M'.$counter,  '=SUM(M5:M'.($counter -1).')' );
            $activeSheet->setCellValue('N'.$counter,  '=SUM(N5:N'.($counter -1).')' );
            $activeSheet->getStyle("A".$counter.":X".$counter)->getFont()->setBold(true);
            $activeSheet->getStyle("A".$counter.":X".$counter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("99A6C4");
        }        

        $activeSheet->setAutoFilter('A4:X4');
        foreach(range('A','X') as $columnID) {
            $activeSheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        // DETALLE DE LAS ORDENES - HOJA 2

        $activeSheet = $objPHPExcel->createSheet();
        $activeSheet->setTitle("Detalle de las Ordenes");
        
        $counter = 2;
        $activeSheet->setCellValue('B'.$counter, "DETALLE DE LAS ORDENES DE SERVICIO");
        $activeSheet->getStyle("B".$counter.":B".$counter)->getFont()->setBold(true);
        
        $counter = 4;
        $activeSheet->setCellValue('A'.$counter, "ORDEN");
        $activeSheet->setCellValue('B'.$counter, "ID SERVICIO");
        $activeSheet->setCellValue('C'.$counter, "SERVICIO");
        $activeSheet->setCellValue('D'.$counter, "CANTIDAD");
        $activeSheet->setCellValue('E'.$counter, "COSTO UNITARIO");
        $activeSheet->setCellValue('F'.$counter, "COSTO FINAL");
        $activeSheet->setCellValue('G'.$counter, "SOLICITADO");
        $activeSheet->getStyle("A".$counter.":G".$counter)->getFont()->setBold(true);
        $activeSheet->getStyle("A".$counter.":G".$counter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("EAA46B");

        if($ordenes){
            foreach ($ordenes as $orden){   
                $color = "F5F5F5";

                $sql = "select osd.*, s.nombre nombre_servicio
                    from servicio.orden_servicio_detalle osd 
                    join servicio.servicios s on (osd.idservicio = s.id)
                    where osd.idorden_servicio = $orden->id and osd.activo = true";

                $detalles = GenericSQL::getBySQL($sql);

                if($detalles) {
                    foreach ($detalles as $detalle){   
                        $counter++;

                        $activeSheet->setCellValue('A'.$counter, $orden->id);
                        $activeSheet->setCellValue('B'.$counter, $detalle->idservicio);
                        $activeSheet->setCellValue('C'.$counter, $detalle->nombre_servicio);
                        $activeSheet->setCellValue('D'.$counter, $detalle->cantidad);
                        $activeSheet->setCellValue('E'.$counter, $detalle->costo);
                        $activeSheet->setCellValue('F'.$counter, $detalle->costo_final);
                        $activeSheet->setCellValue('G'.$counter, date("d/m/Y H:m", strtotime($detalle->fecha_creacion)));
                
                        $activeSheet->getStyle("A".$counter.":G".$counter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB(str_replace("#", "", $color));

                    }
                }
                             
            }
            $counter++;
            $activeSheet->setCellValue('A'.$counter, "TOTALES");
            $activeSheet->setCellValue('D'.$counter,  '=SUM(D5:D'.($counter -1).')' ); // suma de num_servicios
            $activeSheet->setCellValue('E'.$counter,  '=SUM(E5:E'.($counter -1).')' );
            $activeSheet->setCellValue('F'.$counter,  '=SUM(F5:F'.($counter -1).')' );
            $activeSheet->getStyle("A".$counter.":G".$counter)->getFont()->setBold(true);
            $activeSheet->getStyle("A".$counter.":G".$counter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("99A6C4");
        }        

        $activeSheet->setAutoFilter('A4:G4');
        foreach(range('A','G') as $columnID) {
            $activeSheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }


        $objPHPExcel->setActiveSheetIndex(0);
        
        $fname = date('dmYHm')."_ordenesservicio.xlsx";

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fname.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;

    }

}
