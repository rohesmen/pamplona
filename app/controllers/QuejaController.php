<?php
namespace Vokuro\Controllers;

use Vokuro\Models\Clientes;
use Phalcon\Mvc\View;
use Vokuro\Models\Colonias;
use Vokuro\Models\Users;
use Vokuro\Models\FormaPago;
use Vokuro\Models\Queja;
use Vokuro\Models\Etapa;
use Vokuro\Models\Seguimiento;
use Vokuro\Models\TipoDescuento;

/**
 * Display the default index page.
 */
class QuejaController extends ControllerBase
{



    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function indexAction()
    {
        /*$this->view->setVar('logged_in', is_array($this->auth->getIdentity()));*/
        $this->view->setTemplateBefore('public');

        $this->view->setVar('doSearch', $this->acl->isAllowedUser('users', 'search'));
        $this->view->setVar('colonias', Colonias::findByActivo());
        $this->view->setVar('usuarios', Users::findByActivo());
        $this->view->setVar('etapas', Etapa::findByActivo(true));
    }

    public function searchAction(){

        $view = clone $this->view;
        $this->view->disable();




        if ($this->request->isPost() == true) {

            $rawBody = $this->request->getJsonRawBody();

            $results = $this->filterSearch($rawBody);



        }
        else{
            $view->setVar('NotImplemented', true);
        }

        $this->response->setContent(json_encode($results));
        return $this->response;



    }



    private function filterSearch($rawBody){

        $request = $this->request;
        $response = $this->response;

        $nombre = $rawBody->titulo;
        $clave = $rawBody->clave;
        $tipo = $rawBody->tipo;
        $etapa = $rawBody->etapa;

        $clave = preg_replace('/\s+/', ' ',$clave);
        $nombre = preg_replace('/\s+/', ' ',$nombre);



        $where = ' 1 = 1 ';
        if($tipo == "nom")
            $where .= " AND lower(q.titulo) LIKE lower('%$nombre%')";

        if($tipo == "cla")
            $where .= " AND q.id = ". $clave;

        if($tipo == "etapa"){
            $where .= " AND e.id = ".$etapa;
        }

        $results= array();



        $results = Clientes::findByQuery("select q.*, 
            e.nombre etapa,
            COALESCE(c.apepat, '')||' '||COALESCE(c.apemat, '')||' '||COALESCE(c.nombres, '')  persona,
            c.razon_social,
            e.final
            FROM queja.queja q 
            LEFT JOIN cliente.cliente AS c ON c.id_cliente = q.id_cliente
            LEFT JOIN queja.etapa AS e ON e.id = q.id_etapa
            WHERE  ".$where." ORDER BY id ASC");


        $rows = array();
        if(count($results) > 0){

            foreach($results as $res) {

                $vigente = $res->activo ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                    : '<i class="fa fa-remove" style="color: red" title="Activo"></i>';

                $persona = $res->persona;
                if($res->razon_social !=  ""){
                    $persona = $res->razon_social;
                }


                $d = array();
                $d["0"] = null;
                $d["1"] = null;
                $d["2"] = $vigente;
                $d["3"] = $res->id;
                $d["4"] = $res->titulo;
                $d["5"] = $persona;
                $d["6"] = $res->etapa;
                $d["7"] = $res->final;
                if(!$res->final) {
                    $d["8"] = "<button type=\"button\" class=\"btn btn-default\" onclick='cargaMenu($res->id);'>Enviar a</button>";
                }
                else
                    $d["8"] = "";
                $d["DT_RowId"] = "queja-$res->id";
                $d["9"] = date("d-m-Y h:i:s", strtotime($res->fecha_creacion));
                array_push($rows, $d);

            }
        }

        return $rows;
    }

    public function searchClienteAction(){

        $view = clone $this->view;
        $this->view->disable();




        if ($this->request->isPost() == true) {

            $rawBody = $this->request->getJsonRawBody();

            $results = $this->filterSearchCliente($rawBody);



        }
        else{
            $view->setVar('NotImplemented', true);
        }

        $this->response->setContent(json_encode($results));
        return $this->response;



    }

    function eliminarPreposiciones($datos){
        $preposiciones = array('de', 'los', 'la', 'y');
        $newData = array();
        foreach ($datos as $valor) {
            if(!in_array($valor, $preposiciones)){
                array_push($newData,$valor);
            }
        }
        return $newData;
    }

    private function filterSearchCliente($rawBody){

        $request  = $this->request;
        $response = $this->response;

        $nombre  = $rawBody->nombre;
        $clave   = $rawBody->clave;
        $tipo    = $rawBody->tipo;
        $calle   = $rawBody->calle;
        $numero  = $rawBody->numero;
        $colonia = $rawBody->colonia;


        $clave = preg_replace('/\s+/', ' ',$clave);
        $nombre = preg_replace('/\s+/', ' ',$nombre);
        $tipo = preg_replace('/\s+/', ' ',$tipo);

        $calle   = preg_replace('/\s+/', ' ',$calle);
        $numero  = preg_replace('/\s+/', ' ',$numero);
        $colonia = preg_replace('/\s+/', ' ',$colonia);


        $where = ' c.vigente = true ';
        if($tipo == "nom"){
            $aSearch = explode(' ', $nombre);
            if(count($aSearch) > 1){
                $newParams = $this->eliminarPreposiciones($aSearch);
            }
            else{
                $newParams = $aSearch;
            }

            foreach ($newParams as $valor) {
                $where .= " AND COALESCE(trim(lower(c.apepat)), '')||' '||COALESCE(trim(lower(c.apemat)), '')||' '||COALESCE(trim(lower(c.nombres)), '') LIKE lower('%$valor%') ";
            }
//            $where = substr($where, 0, -4);
//            $where .= " AND lower(COALESCE(c.apepat, '')||' '||COALESCE(c.apemat, '')||' '||COALESCE(c.nombres, '')) LIKE lower('%$nombre%')";
        }

        if($tipo == "dir") {
            if ($calle != "")
                $where .= " AND lower(coalesce(c.calle::text, '')||coalesce(c.calle_letra, '')) LIKE lower('%".str_replace(' ', '', $calle)."%')";

            if ($numero != "")
                $where .= " AND lower(coalesce(c.numero::text, '')||coalesce(c.numero_letra, '')) LIKE lower('%".str_replace(' ', '', $numero)."%')";

            if ($colonia != "")
                $where .= " AND c.idcolonia = '" . $colonia . "'";
        }//fin:if($tipo == "dir")

        //[2016-12-09] Todos los cliente Vigentes
        if($tipo == "todos"){
            ;
        }//fin:if($tipo == "todos")

        //[2016-12-08] Cliente Vigente por Clave
        if($tipo == "cla"){
            $where .= " AND c.id_cliente = ". $clave;
        }//fin:


        $results= array();

        $meses = array('enero','febrero','marzo','abril','mayo','junio','julio',
            'agosto','septiembre','octubre','noviembre','diciembre');

        $sqlQuery = "select 
			c.id_cliente, 
			c.calle, 
			c.calle_letra,
			c.numero, 
			c.numero_letra, 
			c.cruza1, 
			c.letra_cruza1,
			c.cruza2,      
			c.letra_cruza2, 
			c.apepat,
			c.apemat,
			c.nombres,
			c.apepat_propietario,
			c.apemat_propietario, 
			c.nombres_propietario,
			c.latitud,
			c.longitud,
			c.razon_social,		
			COALESCE(c.ultimo_anio_pago, 0) as ultimo_anio_pago,
			COALESCE(c.ultimo_mes_pago, '') as ultimo_mes_pago,			
			COALESCE(c.apepat, '')||' '||COALESCE(c.apemat, '')||' '||COALESCE(c.nombres, '')  persona,
			COALESCE(c.apepat_propietario, '')||' '||COALESCE(c.apemat_propietario, '')||' '||COALESCE(c.nombres_propietario, '')  propietario,
			COALESCE(col.nombre, '')||', '||COALESCE(col.localidad, '') colonia,
			COALESCE(col.monto, 0.00) monto,
			c.correo,
			c.telefono
		FROM cliente.cliente c 
		JOIN cliente.colonia AS col ON col.id = c.idcolonia
		WHERE  ". $where. " ORDER BY id_cliente ASC";

        $results = Clientes::findByQuery($sqlQuery);
        //*********************************************************************************************
        $data = array();
        if(count($results) > 0){
            foreach($results as $res){

                $calle = ($res->calle != null and $res->calle != "") ? trim($res->calle) : "";
                $calleLetra = ($res->calle_letra != null and $res->calle_letra != "") ? " ".trim($res->calle_letra) : "";
                $calle = $calle.$calleLetra;

                $numero = ($res->numero != null and $res->numero != "") ? trim($res->numero) : "";
                $numeroLetra = ($res->numero_letra != null and $res->numero_letra != "") ? " ".trim($res->numero_letra) : "";
                $numero = $numero.$numeroLetra;

                $cruz1 = ($res->cruza1 != null and $res->cruza1 != "") ? trim($res->cruza1) : "";
                $cruz1Letra = ($res->letra_cruza1 != null and $res->letra_cruza1 != "") ? trim($res->letra_cruza1) : "";
                $cruz1 = $cruz1.$cruz1Letra;

                $cruz2 = ($res->cruza2 != null and $res->cruza2 != "") ? trim($res->cruza2) : "";
                $cruz2Letra = ($res->letra_cruza2 != null and $res->letra_cruza2 != "") ? trim($res->letra_cruza2) : "";
                $cruz2 = $cruz2.$cruz2Letra;

                $colonia = $res->colonia;

                $fullname = "";
                $fullname .= $res->apepat;
                if($fullname !=  ""){$fullname .= " ";}
                $fullname .= $res->apemat;
                if($fullname !=  ""){$fullname .= " ";}
                $fullname .= $res->nombres;

                //Concatenar la razon social es caso que el cliente lo tenga
                $persona = $res->persona;
                if($res->razon_social !=  ""){
                    $persona = $res->razon_social;
                }

                $propietario = "";
                $propietario .= $res->apepat_propietario;

                if($propietario !=  ""){
                    $propietario .= " ";
                }
                $propietario .= $res->apemat_propietario;
                if($propietario !=  ""){
                    $propietario .= " ";
                }
                $propietario .= $res->nombres_propietario;
                $mensualidad = $res->monto;

                $ultimoMesPago = "";
                $ultimoMesPago = ($res->ultimo_mes_pago != null and $res->ultimo_mes_pago != "") ? trim($res->ultimo_mes_pago) : "";

                if($ultimoMesPago == "" || strlen(trim($res->ultimo_mes_pago)) > 2){
                    $ultimoMesPago = "";
                }//fin:
                else{
                    $idxMes =  intval($res->ultimo_mes_pago) - 1;
                    $ultimoMesPago = $meses[$idxMes] ;
                }//fin:else

                $d = array();
                $d["0"] = $res->id_cliente;  //=id_cliente
                $d["1"] = $persona;			 //$res->nombres;
                $d["2"] = $calle;			 //=calle y calle_letra
                $d["3"] = $numero;			 //=numero y numero_letra
                $d["4"] = $res->ultimo_anio_pago;//=ultimo_anio_pago
                //$d["5"] = $res->ultimo_mes_pago;//=ultimo_mes_pago
                $d["5"] = $ultimoMesPago;//=ultimo_mes_pago

                $d["6"]  = $cruz1;			 	//=cruz1
                $d["7"]  = $cruz2;			 	//=cruz2
                $d["8"]  = $colonia;		 	//=colonia
                $d["9"]  = $propietario;	 	//=propietario
                $d["10"] = $res->latitud;	 	//=latitud
                $d["11"] = $res->longitud;	 	//=longitud
                $d["12"] = $res->apepat;	 	//=Apellido Paterno
                $d["13"] = $res->apemat;	 	//=Apellido Materno
                $d["14"] = $res->razon_social;	//=Empresa(razon social)
                $d["15"] = $fullname;			//=fullName
                $d["16"] = $mensualidad;		 //=mensualidad
                $d["17"] = $res->ultimo_mes_pago;//=$res->ultimo_mes_pago
				$d["18"] = $res->correo;
				$d["19"] = $res->telefono;
                array_push($data, $d);
            }//fin:foreach($results as $res)
        }//fin:if(count($results) > 0)
        return $data;
    }//fin:filterSearch

    public function guardarAction()
    {


        $view = clone $this->view;
        $this->view->disable();
        $respuesta = [];

        $rawBody = $this->request->getJsonRawBody();

        if($rawBody) {
            $id = intval($rawBody->clave);
            $resp = new \stdClass();


            $this->db->begin();

            if($rawBody->fReporte != "") {

                $porciones = explode("/", $rawBody->fReporte);
                $fReporte = $porciones[1] . "/" . $porciones[0] . "/" . $porciones[2];

            }
            else $fReporte = null;

            if($rawBody->fAtencion != "") {

                $porciones = explode("/", $rawBody->fAtencion);
                $fAtencion = $porciones[1] . "/" . $porciones[0] . "/" . $porciones[2];

            }
            else $fAtencion = null;

            if ($id > 0)
            {
                $nuevo = false;
                $queja = Queja::findFirst($id);
                $queja->assign(array(
                    'activo' => $rawBody->activo,
                    'id_cliente' => $rawBody->idCliente,
                    'titulo' =>  $rawBody->titulo,
                    'descripcion' => $rawBody->descripcion,
                    'apepat_reporta' => $rawBody->aPaternoReporta,
                    'apemat_reporta' => $rawBody->aMaternoReporta,
                    'nombres_reporta' => $rawBody->nomReporta,
                    'origen' => $rawBody->origen,
                    'apepat_ayuntamiento' => $rawBody->aPaternoAyunta,
                    'apemat_ayuntamiento' => $rawBody->aMaternoAyunta,
                    'nombres_ayuntamiento' => $rawBody->nomAyunta,
                    'folio' => $rawBody->folio,
                    'fecha_reporte' => $fReporte,
                    'fecha_atencion' => $fAtencion,
                    'folio' => $rawBody->folio,
                    'fecha_modificacion' => 'NOW()',
					'correo' => $rawBody->correo,
					'celular' => $rawBody->telefono
                ));

            }
            else {
                $nuevo = true;
                $queja = new Queja();
                $etapa = Etapa::findFirstByInicial(true);
                if(!$etapa)
                {
                    $this->db->rollback();
                    $this->response->setContent(json_encode(["lOk" => false, "cMensaje" => "No se encuentra la etapa inicial"]));
                    return $this->response;
                }

                $queja->assign(array(
                    //                'id' => ,
                    'activo' => $rawBody->activo,
                    'id_cliente' => $rawBody->idCliente,
                    'id_etapa' => $etapa->id,
                    'titulo' =>  $rawBody->titulo,
                    'descripcion' => $rawBody->descripcion,
                    'apepat_reporta' => $rawBody->aPaternoReporta,
                    'apemat_reporta' => $rawBody->aMaternoReporta,
                    'nombres_reporta' => $rawBody->nomReporta,
                    'origen' => $rawBody->origen,
                    'apepat_ayuntamiento' => $rawBody->aPaternoAyunta,
                    'apemat_ayuntamiento' => $rawBody->aMaternoAyunta,
                    'nombres_ayuntamiento' => $rawBody->nomAyunta,
                    'folio' => $rawBody->folio,
                    'fecha_reporte' => $fReporte,
                    'fecha_atencion' => $fAtencion,
                    'fecha_creacion' => 'NOW()',
                    'fecha_modificacion' => 'NOW()',
					'correo' => $rawBody->correo,
					'celular' => $rawBody->telefono

                ));
            }

            if(!$queja->save()) {
                $this->db->rollback();
                $this->flash->error($queja->getMessages());
                $this->response->setContent(json_encode(["lOk" => false, "cMensaje" => "No fue posible guardar el registro", "data" => $this->flash->error($queja->getMessages())]));
                return $this->response;
            }
            else
            {


                $resp = $this->buacaQueja($queja->id);
                $resp->lOk = true;
                $resp->cMensaje = "";
                $resp->nuevo = $nuevo;
                $d = array();

                if($nuevo)
                {
                    if($this->acl->isAllowedUser("queja","info")){
                        $btninfo = "<button class=\"btn btn-primary btn-sm queja-info\" type=\"button\" title=\"Consultar\">
                                <i class='fa fa-info'></i>
                            </button> ";
                    }
                    $btnEdit = "";
                    if($this->acl->isAllowedUser("queja","edit")){
                        $btnEdit = "<button class=\"btn btn-primary btn-sm queja-edit\" type=\"button\" title=\"Modificar\">
                                <i class='fa fa-pencil'></i>
                            </button> ";
                    }
                    $btnDelete = "";
                    if($this->acl->isAllowedUser("queja","delete")){
                        $btnDelete = "<button class=\"btn btn-danger btn-sm queja-delete\" type=\"button\" title=\"¿Desea eliminar?\">
                                <i class='fa fa-remove'></i>
                            </button>";
                    }
                    $tr = "<tr data-index='".$resp->id."' id='queja-".$resp->id."''>
                            <td>".
                        $btninfo.
                        $btnEdit.
                        $btnDelete.
                        "</td>
                            <td>".$resp->vigente."</td>
                            <td>".$resp->id."</td>
                            <td>".$resp->titulo."</td>
                            <td>".$resp->cliente."</td>
                            <td>".$resp->etapa."</td>
                            <td>false</td>
                        </tr>";
                    //$resp->tr = $tr;
                }
                $resp->DT_RowId = "queja-$resp->id";

                $identity = $this->auth->getIdentity();
                $idUser = $identity["id"];



                $seguimiento = new Seguimiento();
                $seguimiento->assign(array(
                    'activo' => TRUE,
                    'id_origen' => $resp->id,
                    'id_etapa' => $resp->id_etapa,
                    'id_usuario' =>  $idUser,
                    'fecha_creacion' => 'NOW()',
                    'fecha_modificacion' => 'NOW()'

                ));

                if(!$seguimiento->save()) {
                    $this->db->rollback();
                    die;
                    $this->response->setContent(json_encode(["lOk" => false, "cMensaje" => "No fue posible guardar el seguimiento", "data" => $this->flash->error($seguimiento->getMessages())]));
                    return $this->response;
                }

                $this->db->commit();
                $this->response->setContent(json_encode($resp));
                return $this->response;
            }


            die;
        }

    }

    private function buacaQueja($id) {

            $queja = Queja::findFirstById($id);
            if($queja != false){


                $sqlQuery = "select 
                    c.razon_social,		
                    COALESCE(c.apepat, '')||' '||COALESCE(c.apemat, '')||' '||COALESCE(c.nombres, '')  persona,
					c.correo,
					c.telefono
                FROM cliente.cliente c 
                WHERE  c.id_cliente = ".$queja->id_cliente." ORDER BY id_cliente ASC";

                $results = Clientes::findByQuery($sqlQuery);
                //*********************************************************************************************
                $data = array();
				$correo = "";
				$telefono = "";
                if(count($results) > 0){
                    $persona = $results[0]->persona;
                    if($results[0]->razon_social !=  ""){
                        $persona = $results[0]->razon_social;
                    }
					$correo = $results[0]->correo;
					$telefono = $results[0]->telefono;
                }


                $vigente = $queja->activo ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                    : '<i class="fa fa-remove" style="color: red" title="Activo"></i>';

                $row = new \stdClass();


                $etapa = Etapa::findFirstById($queja->id_etapa);

                if($etapa)
                    $row->etapa = $etapa->nombre;
                else
                    $row->etapa = "";



                $row->id = $queja->id;
                $row->titulo = $queja->titulo;
                $row->id_cliente = $queja->id_cliente;
                $row->cliente = $persona;
				$row->correo = $correo;
				$row->telefono = $telefono;
                $row->id_etapa = $queja->id_etapa;
                $row->descripcion = $queja->descripcion;
                $row->aPaternoReporta = is_null($queja->apepat_reporta) ? "" : $queja->apepat_reporta;
                $row->aMaternoReporta = is_null($queja->apemat_reporta) ? "" : $queja->apemat_reporta;
                $row->nomReporta = is_null($queja->nombres_reporta) ? "" : $queja->nombres_reporta;
                $row->origen = $queja->origen;
                $row->aPaternoAyunta = is_null($queja->apepat_ayuntamiento) ? "" : $queja->apepat_ayuntamiento;
                $row->aMaternoAyunta = is_null($queja->apemat_ayuntamiento) ? "" : $queja->apemat_ayuntamiento;
                $row->nomAyunta = is_null($queja->nombres_ayuntamiento) ? "" : $queja->nombres_ayuntamiento;
                $row->folio = is_null($queja->folio) ? "" : $queja->folio;
                $row->activo = $queja->activo;
                $row->vigente = $vigente;
                $row->fecha_reporte = is_null($queja->fecha_reporte) ? "" : date('d/m/Y', strtotime($queja->fecha_reporte));
                $row->fecha_atencion = is_null($queja->fecha_atencion) ? "" : date('d/m/Y', strtotime($queja->fecha_atencion));
                $row->final = $etapa->final;
				$row->correo_q = $queja->correo;
				$row->telefono_q = $queja->celular;
                if(!$etapa->final) {
                    $row->btnEtapa = "<button type=\"button\" class=\"btn btn-default\" onclick='cargaMenu($row->id);'>Enviar a</button>";
                }
                else
                    $row->btnEtapa = "";
                return $row;


        }

        return null;
    }

    public function getAction($id) {
        $this->view->disable();
        if ($this->request->isGet() == true) {




            $row = $this->buacaQueja($id);
            if($row != null)

                $this->response->setContent(json_encode($row));

            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    public function deleteAction($id)
    {
        if($id != null and $id != ""){
            $this->db->begin();

            $queja = Queja::findFirstById($id);
            if ($queja) {
                $queja->activo = false;
                $queja->fecha_modificacion = date("c");
                if(!$queja->save())
                    $this->db->rollback();
                else
                    $this->db->commit();

            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(400, "Bad Request");
        }
        return $this->response;
    }

    public function etapasAction($id)
    {
        if($id != null and $id != ""){

            $queja = Queja::findFirstById($id);
            if ($queja) {

                $results = Etapa::getEtapasSiguientes($queja->id_etapa);
                $data = array();
                $resp = new \stdClass();
                if(count($results) > 0){
                    foreach($results as $res){

                        $d = array();
                        $d = ["name" => $res->nombre];
                        $data[$res->id] = $d;



                        //array_push($data, $resp);
                    }//fin:foreach($results as $res)
                }//fin:if(count($results) > 0)
                $this->response->setContent(json_encode($data));
                return $this->response;
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(400, "Bad Request");
        }
        return $this->response;
    }


    public function cambiaretapaAction()
    {



        $view = clone $this->view;
        $this->view->disable();
        $respuesta = [];

        $rawBody = $this->request->getJsonRawBody();

        if($rawBody) {
            $id = intval($rawBody->clave);
            $resp = new \stdClass();
            $id_etapa_anterior = 0;


            // Start a transaction
            $this->db->begin();
            if ($id > 0)
            {
                $nuevo = false;
                $queja = Queja::findFirst($id);
                $id_etapa_anterior = $queja->id_etapa;
                $queja->assign(array(
                    'id_etapa' => $rawBody->id_etapa,
                    'fecha_modificacion' => 'NOW()'
                ));

            }


            if(!$queja->save()) {
                $this->db->rollback();
                $this->flash->error($queja->getMessages());
                $this->response->setContent(json_encode(["lOk" => false, "cMensaje" => "No fue posible guardar el registro", "data" => $this->flash->error($queja->getMessages())]));
                return $this->response;
            }
            else
            {


                $resp = $this->buacaQueja($id);

                $etapa = Etapa::findFirstById($resp->id_etapa);

                $resp->lOk = true;
                $resp->cMensaje = "";
                $resp->nuevo = $nuevo;
                if($etapa)
                    $resp->etapa = $etapa->nombre;

                $identity = $this->auth->getIdentity();
                $idUser = $identity["id"];

                $seguimiento = new Seguimiento();
                $seguimiento->assign(array(
                    'activo' => TRUE,
                    'id_origen' => $resp->id,
                    'id_etapa' => $resp->id_etapa,
                    'id_etapa_anterior' =>  $id_etapa_anterior,
                    'id_usuario' =>  $idUser,
                    'fecha_creacion' => 'NOW()',
                    'fecha_modificacion' => 'NOW()'

                ));


                if(!$seguimiento->save()) {
                    $this->db->rollback();
                    $this->flash->error($seguimiento->getMessages());
                    $this->response->setContent(json_encode(["lOk" => false, "cMensaje" => "No fue posible guardar el seguimiento", "data" => $this->flash->error($seguimiento->getMessages())]));
                    return $this->response;
                }


                $this->db->commit();

                $this->response->setContent(json_encode($resp));
                return $this->response;
            }


            die;
        }

    }
}
