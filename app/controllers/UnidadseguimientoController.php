<?php
namespace Vokuro\Controllers;
use Vokuro\DT\SSPGEO;
use Vokuro\GenericSQL\GenericSQL;
use Vokuro\Models\Unidad;
use Vokuro\Models\BitacoraCambios;
use Vokuro\Models\Cuadrilla;
use Vokuro\Models\UnidadCuadrilla;

/**
 * Display the default index page.
 */
class UnidadseguimientoController extends ControllerBase
{

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function indexAction()
    {
        $this->view->setTemplateBefore('public');

        $conacciones = 'no';
        if($this->acl->isAllowedUser('unidadseguimiento', 'edit') or $this->acl->isAllowedUser('unidadseguimiento', 'deactivate') or $this->acl->isAllowedUser('unidadseguimiento', 'activate') or $this->acl->isAllowedUser('unidadseguimiento', 'info')){
            $conacciones = 'si';
        }
        $this->view->setVar('coacciones', $conacciones);

        $cuadrillas = Cuadrilla::find([
            "activo = true",
            "order" => "nombre"
        ]);
        $this->view->setVar('cuadrillas', $cuadrillas);

    }

    public function buscarAction(){
        $request = $this->request;
        $response = $this->response;


        $columns = array(
            array( 'db' => '', 'dt' => 0,
                'formatter' => function( $d, $row ) {
                    $buttons = '';
                    if($this->acl->isAllowedUser('unidadseguimiento', 'info')){
                        $buttons .= '<button class="btn btn-primary btn-sm unidadseguimiento-info" data-id="'.$row["id"].'" type="button" title="Consultar">
                            <i class="fa fa-info"></i>
                        </button> ';
                    }
                    if($this->acl->isAllowedUser('unidadseguimiento', 'edit') && $row["activo"] === true){
                        $buttons .= '<button class="btn btn-primary btn-sm unidadseguimiento-edit" data-id="'.$row["id"].'" type="button" title="Editar">
                            <i class="fa fa-pencil"></i>
                        </button> ';
                    }
                    if($this->acl->isAllowedUser('unidadseguimiento', 'deactivate') && $row["activo"] === true){
                        $buttons .= '<button class="btn btn-danger btn-sm unidadseguimiento-delete" data-id="'.$row["id"].'" type="button" title="¿Desea desactivar?">
                            <i class="fa fa-times"></i>
                        </button> ';
                    }
                    if($this->acl->isAllowedUser('unidadseguimiento', 'activate') && $row["activo"] === false){
                        $buttons .= '<button class="btn btn-info btn-sm unidadseguimiento-active" data-id="'.$row["id"].'" type="button" title="¿Desea activar?">
                            <i class="fa fa-check"></i>
                        </button> ';
                    }
                    return $buttons;
                }
            ),
            array( 'db' => '', 'dt' => 1),
            array( 'db' => 'activo', 'datatype' => 'boolean', 'dt' => 2,
                'formatter' => function( $d, $row ) {
                    $vigente = $d ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                        : '<i class="fa fa-remove" style="color: red" title="Inactivo"></i>';
                    return $vigente;
                }
            ),
            array( 'db' => 'nombre', 'dt' => 3),
            array( 'db' => 'id', 'dt' => 4),
            array( 'db' => 'numero_economico', 'dt' => 5),
            array( 'db' => 'fecha_creacion_f', 'datatype' => 'date', 'dt' => 6),
            array( 'db' => 'fecha_modificacion_f', 'datatype' => 'date', 'dt' => 7),
            array( 'db' => 'activo', 'datatype' => 'boolean', 'dt' => 8),
            array( 'db' => 'descripcion', 'dt' => 9)
        );

        $data = SSPGEO::complex_geo($this->request->get(), "monitoreo.view_unidad", "id", $columns);

        $response->setContent(json_encode($data));
        return $response;
    }

    public function saveAction(){

        $rawBody = $this->request->getJsonRawBody();

        $id = $rawBody->id;
        $nombre = mb_strtoupper(trim($rawBody->nombre));
        $placa = mb_strtoupper(trim($rawBody->placa));
        $numero_economico = mb_strtoupper(trim($rawBody->numero_economico));
        $descripcion = mb_strtoupper(trim($rawBody->descripcion));
        $unidadcuadrilla = $rawBody->unidadcuadrilla;

        $identity = $this->auth->getIdentity();
        $idUser = $identity["id"];

        if($nombre == ""){
            $this->response->setStatuscode(400, "No se ingresó nombre de la unidad");
            return $this->response;
        }

        if($placa == ""){
            $this->response->setStatuscode(400, "No se ingresó placa de la unidad");
            return $this->response;
        }

        $sql = "replace(upper(nombre), ' ', '') = replace(upper('$nombre'), ' ', '')";
        if($id != "" && $id != null){
            $sql .= " AND replace(upper(placa), ' ', '') != replace(upper('$id'), ' ', '')";
        }
        $busqueda = Unidad::findFirst($sql);
        if($busqueda){
            $this->response->setStatuscode(409, "Ya existe un registro ".($busqueda->activo ? "activo" : "inactivo")." con mismo nombre");
            return $this->response;
        }

        $sql = "replace(upper(placa), ' ', '') = replace(upper('$placa'), ' ', '')";
        if($id != "" && $id != null){
            $sql .= " AND replace(upper(placa), ' ', '') != replace(upper('$id'), ' ', '')";
        }
        $busqueda = Unidad::findFirst($sql);
        if($busqueda){
            $this->response->setStatuscode(409, "Ya existe un registro ".($busqueda->activo ? "activo" : "inactivo")." con misma placa");
            return $this->response;
        }

        if($numero_economico != "" && $numero_economico != null){
            $sql = "replace(upper(numero_economico), ' ', '') = replace(upper('$numero_economico'), ' ', '')";
            if($id != "" && $id != null){
                $sql .= " AND replace(upper(placa), ' ', '') != replace(upper('$id'), ' ', '')";
            }
            $busqueda = Unidad::findFirst($sql);
            if($busqueda){
                $this->response->setStatuscode(409, "Ya existe un registro ".($busqueda->activo ? "activo" : "inactivo")." con mismo no. economico");
                return $this->response;
            }
        }

        $dataOrigin = null;
        $accion = "CREACION";

        if($id != "" && $id != null){
            $data = Unidad::findFirstByPlaca($id);
            $dataOrigin = json_encode($data);
            $accion = "EDICION";
        }else{
            $data = new Unidad();
            $data->fechacreacion = date("c");
        }

        $data->activo = true;
        $data->idusuario = $idUser;
        $data->nombre = $nombre;
        $data->placa = $placa;
        $data->numero_economico = $numero_economico != "" ? $numero_economico : null;
        $data->descripcion = $descripcion != "" ? $descripcion : null;
        $data->fechamodificacion = date("c");

        $this->db->begin();

        if($data->save()){

            $data->refresh();

            $dataB = new BitacoraCambios();
            $dataB->identificador_txt = $data->placa;
            $dataB->modulo = 'UNIDSEG';
            $dataB->idusuario = $idUser;
            $dataB->tabla = "monitoreo.unidad";
            $dataB->cambios = json_encode($data);
            $dataB->original = $dataOrigin;
            $dataB->accion = $accion . " MONITOREO UNIDAD";

            if($dataB->save()){
                foreach ($unidadcuadrilla as $valor) {

                    $lSave = false;
                    $detalleOrigin = null;
                    $detalleAccion = "CREACION";

                    //$this->logger->info("valor: ".json_encode($valor));

                    if($valor[4] && $valor[4] == true && intval($valor[5]) == -1) {
                        $detalle = new UnidadCuadrilla();
                        $detalle->placa = $data->placa;
                        $detalle->idcuadrilla = $valor[3];
                        $detalle->idusuario = $idUser;
                        $detalle->activo = true;
                        $detalle->fecha_creacion = 'NOW()';
                        $detalle->fecha_modificacion = 'NOW()';
                        $lSave = true;
                    }else if((!$valor[4]) && $valor[4] == false && intval($valor[5]) > 0) {
                        $detalleAccion = "ELIMINACION";
                        $detalle = UnidadCuadrilla::findFirstById($valor[5]);
                        $detalleOrigin = json_encode($detalle);
                        $detalle->idusuario = $idUser;
                        $detalle->idusuario_desactivo = $idUser;
                        $detalle->activo = false;
                        $detalle->fecha_modificacion = 'NOW()';
                        $lSave = true;
                    } else if($valor[4] && $valor[4] == true && intval($valor[5]) > 0 && $valor[6] != $data->placa){
                        $detalleAccion = "EDICION";
                        $detalle = UnidadCuadrilla::findFirstById($valor[5]);
                        $detalleOrigin = json_encode($detalle);
                        $detalle->idusuario = $idUser;
                        $detalle->placa = $data->placa;
                        $detalle->fecha_modificacion = 'NOW()';
                        $lSave = true;
                    }

                    if($lSave) {
    
                        if(!$detalle->save()){
                            $this->db->rollback();
                            foreach ($detalle->getMessages() as $message) {
                                $this->logger->info("(create-delete-detalle-unidadcuadrillaseguimiento): " . $message);
                            }
                            $mensaje = "Ocurrió un error al crear/eliminar las rutas de recoleccion.";
                            $this->logger->error($mensaje);
                            $this->response->setStatusCode(500, $mensaje);
                            return $this->response;
                        }

                        $detalle->refresh();

                        $dataB = new BitacoraCambios();
                        $dataB->identificador = $detalle->id;
                        $dataB->modulo = 'UNIDSEG';
                        $dataB->idusuario = $idUser;
                        $dataB->tabla = "monitoreo.unidad_cuadrilla";
                        $dataB->cambios = json_encode($detalle);
                        $dataB->original = $detalleOrigin;
                        $dataB->accion = $detalleAccion . " MONITOREO UNIDAD CUADRILLA";
    
                        if (!$dataB->save()) {
                            $this->db->rollback();
                            foreach ($dataB->getMessages() as $message) {
                                $this->logger->info("(save-bitacora-detalle-unidadcuadrillaseguimiento): " . $message);
                            }
                            $mensaje = "Ocurrió un error al guardar la bitacora de las cuadrillas.";
                            $this->logger->error($mensaje);
                            $this->response->setStatusCode(500, $mensaje);
                            return $this->response;
                        }
                    }
                }
                $this->db->commit();
            }else{
                $this->db->rollback();
                foreach ($dataB->getMessages() as $message) {
                    $this->logger->error("save-bitacora-unidadseguimiento: ".$message->getMessage());
                }
                $mensaje = "Ocurrió un error al guardar la bitacora.";
                $this->logger->error($mensaje);
                $this->response->setStatusCode(500, $mensaje);
            }            
        }else{
            $this->db->rollback();
            foreach ($data->getMessages() as $message) {
                $this->logger->error("save-unidadseguimiento: ".$message->getMessage());
            }
            $mensaje = "Ocurrió un error al guardar el registro.";
            $this->logger->error($mensaje);
            $this->response->setStatusCode(500, $mensaje);
        }
        return $this->response;
    }

    public function deleteAction($id, $activo){
        if(!empty($id)){
            $d = Unidad::findFirstByPlaca($id);
            if ($d) {

                $identity = $this->auth->getIdentity();
                $idUser = $identity["id"];

                $this->db->begin();

                $dataOrigin = json_encode($d);
                $accion = "DESACTIVACION";
                if($activo === true || $activo == "true"){
                    $accion = "ACTIVACION";
                    $d->idusuario_desactivo = null;
                } else{
                    $d->idusuario_desactivo = $idUser;
                }
                $d->activo = $activo;
                $d->idusuario = $idUser;
                $d->fechamodificacion = date("c");
                if(!$d->save()){
                    $this->db->rollback();
                    foreach ($d->getMessages() as $message) {
                        $this->logger->info("(delete-unidadseguimiento): " . $message);
                    }
                    $mensaje = "Ocurrió un error al guardar el registro.";
                    $this->logger->error($mensaje);
                    $this->response->setStatusCode(500, $mensaje);
                }else{
                    $d->refresh();
                    $dataB = new BitacoraCambios();
                    $dataB->identificador_txt = $d->placa;
                    $dataB->modulo = 'UNIDSEG';
                    $dataB->idusuario = $idUser;
                    $dataB->tabla = "monitoreo.unidad";
                    $dataB->cambios = json_encode($d);
                    $dataB->original = $dataOrigin;
                    $dataB->accion = $accion . " MONITOREO UNIDAD";
                    if (!$dataB->save()) {
                        $this->db->rollback();
                        foreach ($dataB->getMessages() as $message) {
                            $this->logger->info("(save-bitacora-delete-unidadseguimiento): " . $message);
                        }
                        $mensaje = "Ocurrió un error al guardar la bitacora.";
                        $this->logger->error($mensaje);
                        $this->response->setStatusCode(500, $mensaje);
                    }
                    $this->db->commit();
                }
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(400, "Bad Request");
        }
        return $this->response;
    }

    public function getAction($id){
        if($this->request->isGet()){
            if($id != ""){
                $sql = "SELECT id, idcuadrilla, cuadrilla, placa
                FROM monitoreo.view_unidad_cuadrilla
                WHERE activo AND placa = '$id'";
                $data = GenericSQL::getBySQL($sql);
                $resp = array();
                foreach ($data as $i){
                    array_push($resp, array(
                        "",
                        '<button class="btn btn-sm btn-danger delete_cuadrilla_unidad" data-id="'.$i->id.'" title="¿Desea eliminar?"><i class="fa fa-remove"></i></button>',
                        $i->cuadrilla,
                        $i->idcuadrilla,
                        true,
                        $i->id,
                        $i->placa
                    ));
                }
                $this->response->setContent(json_encode($resp));
            }else{
                $this->response->setStatusCode(400);
            }
        }else{
            $this->response->setStatusCode(501);
        }
        return $this->response;
    }

}
