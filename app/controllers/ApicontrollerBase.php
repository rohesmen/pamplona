<?php
namespace Vokuro\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Vokuro\Models\Messages;
use Vokuro\Models\Device;
use Vokuro\Models\Profiles;

/**
 * ControllerBase
 * This is the base controller for all controllers in the application
 */
class ApicontrollerBase extends Controller
{

    /**
     * Execute before the router so we can determine if this is a provate controller, and must be authenticated, or a
     * public controller that is open to all.
     *
     * @param Dispatcher $dispatcher
     * @return boolean
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher){
//        $enc = $this->request->getHeader("Enc");
//        if($enc != null){
//            $this->logger->info("ENC");
//        }
//
//        $token = '$2a$08$n1U1buD35u6JVD1zgXkxPOdm.0rx0NoF2mSFi4GTtJpa4zD3l4MaG';
        $identity = $this->auth->getIdentity();
        $controllerName = $dispatcher->getControllerName();
        $actionName = $dispatcher->getActionName();
        $this->view->disable();
        if ($this->request->isOptions()) {
            $this->response
                ->setHeader('Access-Control-Allow-Origin', '*')
                ->setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
                ->setHeader('Access-Control-Allow-Headers', 'Content-Length, Accept, Accept-Encoding, Accept-Language, Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type, Authorization, token, tdata, Tdata, Token')
                ->setHeader('Access-Control-Allow-Credentials', 'true');

            $this->response->setStatusCode(200, 'OK')->send();
            exit;
        }

        $this->response
            ->setHeader('Access-Control-Allow-Origin', '*')
            ->setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
            ->setHeader('Access-Control-Allow-Headers', 'Content-Length, Accept, Accept-Encoding, Accept-Language, Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type, Authorization, token, tdata, Tdata, Token')
            ->setHeader('Access-Control-Allow-Credentials', 'true');
//                ->setHeader('Access-Control-Allow-Credentials', 'true');

        $headers = $this->request->getHeaders();
//            $this->logger->info(' <=1=> '.$this->session->getId());
        if(isset($headers["Token"]) || isset($headers["token"])){
//                $this->logger->info(' <=2=> '.$this->session->getId());
            $logger = $this->getDI()->getLogger();

//                $session = Session::findFirstById($token);
//            $token = $headers["Token"] ? $headers["Token"] : $headers["token"];
//            $b64UserData = $headers["Tdata"] ? $headers["Tdata"] : $headers["tdata"];
//            $userData = base64_decode($b64UserData);
//            list($strDev, $strIdUser) = explode("|", $userData);

            list($token, $strDev, $strIdUser) = $this->getUserdata();

//                $session = Session::findFirstById($token);
            if($token == '$2a$08$n1U1buD35u6JVD1zgXkxPOdm.0rx0NoF2mSFi4GTtJpa4zD3l4MaG'){

                $device = Device::findFirstById($strDev);
                if(!$device || !$device->permitido){
                    $this->response->setStatusCode(403);
                    return false;
                }

                /*if(isset($headers["versionApp"])){
                    $token = $headers["versionApp"];
                    if($token != $this->config->application->applicationVersion)
                    {
                        $this->response->setStatusCode(406);
                        return false;
                    }
                }
                else{
                    $this->response->setStatusCode(406);
                    return false;
                }*/

                return true;
            }
            else{
                $this->response->setStatusCode(403);
                return false;
            }

        }
        elseif(!$this->acl->isPrivateAction($controllerName, $actionName)){
            return true;
        }
        else{
            $this->response->setStatusCode(403);
            return false;
        }
    }
    public function convertDateToDefaultTZ($strDate){
        $dt = new \DateTime($strDate);
        $dt->setTimeZone(new \DateTimeZone(date_default_timezone_get()));
        return $dt->format('c');
    }

    public function getUserdata(){
        $headers = $this->request->getHeaders();
        $token = $headers["Token"] ? $headers["Token"] : $headers["token"];
        $b64UserData = $headers["Tdata"] ? $headers["Tdata"] : $headers["tdata"];
        $userData = base64_decode($b64UserData);
        list($strDev, $strIdUser) = explode("|", $userData);
        return array(
            $token,
            $strDev,
            $strIdUser
        );
    }

    public function getProfiles($idUser){
        $profiles = Profiles::findByUser($idUser);
        return $profiles;
    }
}
