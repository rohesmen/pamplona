<?php
namespace Vokuro\Controllers;

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Vokuro\Forms\ChangePasswordForm;
use Vokuro\Forms\UsersForm;
use Vokuro\Models\Profiles;
use Vokuro\Models\UserProfiles;
use Vokuro\Models\Users;
use Vokuro\Models\PasswordChanges;
use Phalcon\Mvc\View;

/**
 * Vokuro\Controllers\UsersController
 * CRUD to manage users
 */
class UsersController extends ControllerBase
{

    public function initialize()
    {
        $this->view->setTemplateBefore('public');
    }

    /**
     * Default action, shows the search form
     */
    public function indexAction()
    {
        //$this->persistent->conditions = null;
        //$this->view->form = new UsersForm();
        $this->view->setVar('doSearch', $this->acl->isAllowedUser('users', 'search'));
        $this->view->setVar('resetPass', $this->acl->isAllowedUser("users","resetPassword"));
    }

    public function searchAction(){
        $view = clone $this->view;
        $this->view->disable();

        $view->start();
        if ($this->request->isPost() == true) {
            $rawBody = $this->request->getJsonRawBody();
            $value = strtolower(trim($rawBody->valor));
            $filter = strtolower(trim($rawBody->filtro));
            $results = $this->filterSearch($value, $filter);

            $view->setVar('users', $results);
        }
        else{
            $view->setVar('NotImplemented', true);
        }

        $datosUsuario = $this->auth->getIdentity();
        $idTerritory = $datosUsuario['idTerritory'];
        $territory = $datosUsuario['territory'];
        if($idTerritory != null and $territory != null){
            $view->setVar('pageLength', -1);
        }
        else{
            $view->setVar('pageLength', 15);
        }

        $hasButtons = true;
        $buttons = array();

        if($this->acl->isAllowedUser('users', 'exportExcel')){
            array_push($buttons,'{"text": "<i class='."\'fa fa-file-excel-o\'".'></i>", "title": "Usuarios", "extend": "excel","exportOptions": {"columns": ":visible"}}');
            $hasButtons = true;
        }
        if($this->acl->isAllowedUser('users', 'exportPdf')){
            array_push($buttons,'{"text": "<i class='."\'fa fa-file-pdf-o\'".'></i>", "title": "Usuarios", "extend": "pdf","exportOptions": {"columns": ":visible"}}');
            $hasButtons = true;
        }
        if($this->acl->isAllowedUser('users', 'print')){
            $jsonPrint = '{"text": "<i class='."\'fa fa-print\'".'></i>", "title": "Usuarios", "extend": "print","exportOptions": {"columns": ":visible"}}';
            array_push($buttons, $jsonPrint);

            $hasButtons = true;
        }
        array_push($buttons,'{"text": "<i class='."\'fa fa-gear\'".'></i>", "extend": "colvis"}');

        $view->setVar('hasButtons', $hasButtons);
        $view->setVar('buttons', $buttons);
        $view->setVar('edit', $this->acl->isAllowedUser("users","edit"));
        $view->setVar('info', $this->acl->isAllowedUser("users","info"));
        $view->setVar('editProfiles', $this->acl->isAllowedUser("users","editProfiles"));

        $view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $view->render('users', 'search');
        $view->finish();

        $this->response->setContent($view->getContent());
        return $this->response;
    }

    private function filterSearch($value, $filter){
        $valor = preg_replace('/\s+/', ' ',$value);

        $datosUsuario = $this->auth->getIdentity();
        $general = $datosUsuario['general'];
        $idTerritory = $datosUsuario['idTerritory'];
        $territory = $datosUsuario['territory'];
        $idUser = $datosUsuario['id'];
        $results = array();

        if($filter == -1) {
            if ($general == true) {
                $results = Users::findAll();
            } elseif ($territory != null and $idTerritory != null) {
                $results = Users::findByTypeAndIdTerritory($this->getNameColumnId($territory), $idTerritory);
            }
        }elseif($filter == 'cla' and $value != ''){
            try{
                $busqueda = intval($valor);
                if ($general == true) {
                    $results = Users::findFromColumnAndValueNumber("id", $valor);
                } elseif ($territory != null and $idTerritory != null) {
                    $results = Users::findFromColumnAndValueNumber("id", $valor,$this->getNameColumnId($territory), $idTerritory);
                }
            }
            catch(Exception $e){}
        }elseif($filter == 'nom' and $valor != ''){
                $results = $this->findByNombre($valor, true);
        }
        elseif($filter == 'usr' and $valor != ''){
                $results = Users::findLikeByUsuario($valor);
        }

        $rows = array();
        if(count($results) > 0){
            foreach($results as $t) {

                if($idUser != 1 && $t->id == 1)
                    continue;
                $row = new \stdClass();

                $strnombrecompleto = "";
                if($t->nombre != null && $t->nombre != "") {
                    $strnombrecompleto = $t->nombre;
                }
                if($t->apellido_paterno != null && $t->apellido_paterno != "") {
                    $strnombrecompleto .= " ".$t->apellido_paterno;
                }
                if($t->apellido_materno != null && $t->apellido_materno != "") {
                    $strnombrecompleto .= " ".$t->apellido_materno;
                }

                $profiles = Profiles::findByUser($t->id);
                $nombreperfil = "";
                if(count($profiles) > 0) {
                    foreach($profiles as $profile) {
                        $nombreperfil .= strtoupper($profile->nombre) . ", ";
                    }
                    $nombreperfil = substr($nombreperfil, 0, -2);
                }

                $row->id = $t->id;
                $row->usuario = $t->usuario;
                $row->nombre_completo = strtoupper(trim($strnombrecompleto));
                $row->telefono = $t->telefono;
                $row->activo = $t->activo;
                $row->perfil = $nombreperfil;
                $row->general = $t->general;
                $row->creacion = date("d/m/Y H:i A", strtotime(str_replace('/', '-',$t->fecha_creacion)));
                $row->modificacion = date("d/m/Y H:i A", strtotime(str_replace('/', '-',$t->fecha_modificacion)));
                array_push($rows,$row);
            }
        }
        return $rows;
    }

    private function findByNombre($valor, $general){
        $results = array();
        $params = explode(' ', $valor);
        if(count($params) > 1){
            $newParams = $this->eliminarPreposiciones($params);
        }
        else{
            $newParams = $params;
        }
        if(count($newParams) > 0){
            $results = Users::findByNombre($newParams);
        }
        return $results;
    }

    function eliminarPreposiciones($datos){
        $preposiciones = array('de', 'los', 'la', 'y');
        $newData = array();
        foreach ($datos as $valor) {
            if(!in_array($valor, $preposiciones)){
                array_push($newData,$valor);
            }
        }
        return $newData;
    }

    /**
     * Creates a User
     */
    public function createAction()
    {
        if ($this->request->isPost()) {

            $user = new Users();

            $user->assign(array(
                'name' => $this->request->getPost('name', 'striptags'),
                'profilesId' => $this->request->getPost('profilesId', 'int'),
                'email' => $this->request->getPost('email', 'email')
            ));

            if (!$user->save()) {
                $this->flash->error($user->getMessages());
            } else {

                $this->flash->success("User was created successfully");

                Tag::resetInput();
            }
        }

        $this->view->form = new UsersForm(null);
    }

    /**
     * Saves the user from the 'edit' action
     */
    public function editAction($id)
    {
        $user = Users::findFirstById($id);
        if (!$user) {
            $this->flash->error("User was not found");
            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        }

        if ($this->request->isPost()) {

            $user->assign(array(
                'name' => $this->request->getPost('name', 'striptags'),
                'profilesId' => $this->request->getPost('profilesId', 'int'),
                'email' => $this->request->getPost('email', 'email'),
                'banned' => $this->request->getPost('banned'),
                'suspended' => $this->request->getPost('suspended'),
                'active' => $this->request->getPost('active')
            ));

            if (!$user->save()) {
                $this->flash->error($user->getMessages());
            } else {

                $this->flash->success("User was updated successfully");

                Tag::resetInput();
            }
        }

        $this->view->user = $user;

        $this->view->form = new UsersForm($user, array(
            'edit' => true
        ));
    }

    /**
     * Deletes a User
     *
     * @param int $id
     */
    public function deleteAction($id)
    {
        $user = Users::findFirstById($id);
        if (!$user) {
            $this->flash->error("User was not found");
            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        }

        if (!$user->delete()) {
            $this->flash->error($user->getMessages());
        } else {
            $this->flash->success("User was deleted");
        }

        return $this->dispatcher->forward(array(
            'action' => 'index'
        ));
    }

    /**
     * Users must use this action to change its password
     */
    /*public function changePasswordAction()
    {
        $form = new ChangePasswordForm();

        if ($this->request->isPost()) {

            if (!$form->isValid($this->request->getPost())) {

                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            } else {

                $user = $this->auth->getUser();

                $user->password = $this->security->hash($this->request->getPost('password'));
                $user->mustChangePassword = 'N';

                $passwordChange = new PasswordChanges();
                $passwordChange->user = $user;
                $passwordChange->ipAddress = $this->request->getClientAddress();
                $passwordChange->userAgent = $this->request->getUserAgent();

                if (!$passwordChange->save()) {
                    $this->flash->error($passwordChange->getMessages());
                } else {

                    $this->flash->success('Your password was successfully changed');

                    Tag::resetInput();
                }
            }
        }

        $this->view->form = $form;
    }*/

    public function changePasswordAction()
    {
        $this->view->disable();
        if ($this->request->isPost()) {
            $json = $this->request->getJsonRawBody();
            $identity = $this->auth->getIdentity();
            $idUser = $identity["id"];
            $user = Users::findFirstById($idUser);
            $actualPassword = $json->actual;

            if($this->security->checkHash($actualPassword, $user->clave) || $user->clave == null || $user->clave == ""){
                $newPssword = $this->security->hash($json->new);
                $user->clave = $newPssword;
                $user->fecha_modificacion = date('c');
                if (!$user->update()) {
                    $this->response->setStatusCode(500, "Internal Server Error");
                    $this->response->setContent("Ocurrió un error actualizar la información");
                } else {
                    $this->response->setContent("La contaseña se actualizo correctamente");
                }
            }
            else{
                $this->response->setStatusCode(400, "Bad Request");
                $this->response->setContent("La contraseña actual no coincide");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
            $this->response->setContent("Método no permitido");
        }
        return $this->response;
    }

    public function getAction($id){
        $this->view->disable();
        if ($this->request->isGet() == true) {
            $user = Users::findFirst($id);
            if($user != false){
                $strnombrecompleto = "";
                if($user->nombre != null && $user->nombre != "") {
                    $strnombrecompleto = $user->nombre;
                }
                if($user->apellido_paterno != null && $user->apellido_paterno != "") {
                    $strnombrecompleto .= " ".$user->apellido_paterno;
                }
                if($user->apellido_materno != null && $user->apellido_materno != "") {
                    $strnombrecompleto .= " ".$user->apellido_materno;
                }


                $row = new \stdClass();
                $row->id = $user->id;
                $row->usuario = $user->usuario;
                $row->apepat = $user->apellido_paterno;
                $row->apemat = $user->apellido_materno;
                $row->nombre = $user->nombre;
                $row->nombre_completo = strtoupper(trim($strnombrecompleto));
                $row->telefono = $user->telefono;
                $row->activo = $user->activo;
                $row->general = $user->general;
                $row->correo = $user->correo;

                $profiles = Profiles::findByUser($user->id);
                $perfiles = array();
                if(count($profiles) > 0) {
                    foreach($profiles as $profile) {
                        $rowp = new \stdClass();
                        $rowp->id = $profile->id;
                        $rowp->nombre = $profile->nombre;
                        array_push($perfiles, $rowp);
                    }
                }
                $row->perfiles = $perfiles;
                $this->response->setContent(json_encode($row));
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    public function saveAction()
    {
        $this->view->disable();
        if ($this->request->isPost() == true) {
            $rawBody = $this->request->getJsonRawBody();
            $id = ($rawBody->id != null) ? ((int)$rawBody->id) : (null);
            $nombre = $rawBody->nombre;
            $paterno = $rawBody->paterno;
            $materno = $rawBody->materno;
            $correo = $rawBody->correo;
            $usuario = $rawBody->usuario;
            $tel = $rawBody->tel;
            $activo = $rawBody->activo;

            $saveResult = new \stdClass();
            $now = date("c");
            if($id != null){
                $user = Users::findFirst($id);
                if($user != false){
                    $user->nombre = $nombre;
                    $user->apellido_paterno = $paterno;
                    $user->apellido_materno = $materno;
                    $user->correo = $correo;
                    $user->telefono = $tel;
                    $user->activo = $activo;
                    $user->fecha_modificacion = $now;

                    $strnombrecompleto = "";
                    if($user->nombre != null && $user->nombre != "") {
                        $strnombrecompleto = $user->nombre;
                    }
                    if($user->apellido_paterno != null && $user->apellido_paterno != "") {
                        $strnombrecompleto .= " ".$user->apellido_paterno;
                    }
                    if($user->apellido_materno != null && $user->apellido_materno != "") {
                        $strnombrecompleto .= " ".$user->apellido_materno;
                    }

                    $user->save();
                    $saveResult->id = $user->id;
                    $saveResult->edit = true;
                    $saveResult->nombre = strtoupper(trim($strnombrecompleto));
                    $saveResult->actualizacion = $now;
                    $saveResult->activo = $user->activo;
                    $this->response->setContent(json_encode($saveResult));
                }
                else{
                    $this->response->setStatusCode(404, "Not Found");
                }
            }
            else{
                $userExist = Users::findByUsuario($usuario);
                if($userExist->valid()){
                    $this->response->setStatusCode(409, "Conflict");
                }
                else{
                    $user = new Users();
                    $user->nombre = $nombre;
                    $user->apellido_paterno = $paterno;
                    $user->apellido_materno = $materno;
                    $user->correo = $correo;
                    $user->telefono = $tel;
                    $user->activo = $activo;
                    $user->usuario = $usuario;
                    $user->clave = $this->security->hash($usuario);
                    $user->fecha_modificacion = $now;
                    $user->fecha_creacion = $now;
                    $user->general = true;

                    $strnombrecompleto = "";
                    if($user->nombre != null && $user->nombre != "") {
                        $strnombrecompleto = $user->nombre;
                    }
                    if($user->apellido_paterno != null && $user->apellido_paterno != "") {
                        $strnombrecompleto .= " ".$user->apellido_paterno;
                    }
                    if($user->apellido_materno != null && $user->apellido_materno != "") {
                        $strnombrecompleto .= " ".$user->apellido_materno;
                    }

                    if($user->save()){
                        $saveResult->id = $user->id;
                        $saveResult->edit = false;

                        $btninfo = "";
                        if($this->acl->isAllowedUser("users","info")){
                            $btninfo = "<button class='btn btn-sm btn-info read' title='Consultar datos del usuario' data-index='".$user->id."'>
                                <i class='fa fa-info'></i>
                            </button> ";
                        }
                        $btnEdit = "";
                        if($this->acl->isAllowedUser("users","edit")){
                            $btnEdit = "<button class='btn btn-sm btn-primary edit' title='Editar datos del usuario' data-index='".$user->id."'>
                                <i class='fa fa-pencil'></i>
                            </button> ";
                        }
                        $btnEditProfiles = "";
                        if($this->acl->isAllowedUser("users","editProfiles")){
                            $btnEditProfiles = "<button class='btn btn-sm btn-primary edit-profiles' title='Editar perfiles' data-index='".$user->id."'>
                                <i class='fa fa-list'></i>
                            </button>";
                        }
                        $strNow = date("d/m/Y H:i A", strtotime(str_replace('/', '-',$now)));
                        $tr = "<tr data-index='".$user->id."' id='tr-".$user->id."''>
                            <td>".
                                $btninfo.
                                $btnEdit.
                                $btnEditProfiles.
                            "</td>
                            <td><i class='fa fa-check' title='Activo'></i></td>
                            <td>".$user->id."</td>
                            <td><div id='name-user-".$user->id."' data-index='".$user->id."'>".$user->usuario."</div></td>
                            <td>".strtoupper(trim($strnombrecompleto))."</td>
                            <td></td>
                            <td>".$strNow."</td>
                            <td>".$strNow."</td>
                        </tr>";
                        $saveResult->tr = $tr;
                        $this->response->setContent(json_encode($saveResult));
                    }
                    else{
                        $this->response->setStatusCode(500, "Internal Server Error");
                    }
                }
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    public function getProfilesAction(){
        if ($this->request->isGet() == true) {
            $view = clone $this->view;
            $this->view->disable();

            $view->start();
            $idUser = $this->request->getQuery("id", "int");
            $userProfiles = Profiles::findByUser($idUser);
            $profiles = Profiles::findActivos();

            $resp = array();
            foreach($profiles as $profile){
                $row = new \stdClass();
                $row->id = $profile->id;
                $row->nombre = $profile->nombre;
                $userProfileCheck = false;
                foreach($userProfiles as $up){
                    if($profile->id == $up->id){
                        $userProfileCheck = true;
                        break;
                    }
                }
                $row->checked = $userProfileCheck;
                array_push($resp, $row);
            }
            $view->setVar('profiles', $resp);
            $view->setVar('iduser', $idUser);

            $view->setRenderLevel(View::LEVEL_ACTION_VIEW);
            $view->render('users', 'profiles');
            $view->finish();

            $this->response->setContent($view->getContent());
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }

        return $this->response;
    }

    public function saveProfilesAction(){
        $this->view->disable();

        if ($this->request->isPost() == true) {
            $rawBody = $this->request->getJsonRawBody();
            $ids = $rawBody->ids;
            $idUser = $rawBody->idusuario;
            $profiles = Profiles::findByUser($idUser);

            $activos = array();
            foreach($profiles as $p){
                $isInArray = false;
                foreach($ids as $id){
                    if($p->id == $id and $p->activo){
                        $isInArray = true;
                        break;
                    }
                }
                if($isInArray){
                    array_push($activos, $p->id);
                }
                else{
                    $aux = UserProfiles::findByIdUsuarioAndIdPerfilAndActivo($idUser, $p->id);
                    if(count($aux) > 0){
//                        $aux[0]->fecha_modificacion = date("c");
//                        $aux[0]->activo = false;
//                        $aux[0]->save();
                        $aux[0]->delete();
                    }
                }
            }
            foreach($ids as $id){
                if(in_array($id, $activos)){
                    continue;
                }
                $now = date("c");
                $nuevo = new UserProfiles();
                $nuevo->idusuario = $idUser;
                $nuevo->idperfil = $id;
                $nuevo->activo = true;
                $nuevo->fecha_creacion = $now;
                $nuevo->fecha_modificacion = $now;
                $nuevo->save();
            }

            $row = new \stdClass();
            $profiles = Profiles::findByUser($idUser);
            $nombreperfil = "";
            if(count($profiles) > 0) {
                foreach($profiles as $profile) {
                    $nombreperfil .= strtoupper($profile->nombre) . ", ";
                }
                $nombreperfil = substr($nombreperfil, 0, -2);
            }
            $row->perfiles = $nombreperfil;
            $this->response->setContent(json_encode($row));
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    public function resetPasswordAction($id){
        $this->view->disable();

        if ($this->request->isPut() == true) {
//            $rawBody = $this->request->getJsonRawBody();
//            $id = $rawBody->id;
            if($id != null){
                $user = Users::findFirst($id);
                if($user != false){
                    $user->clave = $this->security->hash($user->usuario);
                    $user->save();
                    $this->response->setStatusCode(204);
                }
                else{
                    $this->response->setStatusCode(404, "Not Found");
                }
            }
            else{
                $this->response->setStatusCode(400, "Bad Request");
            }

        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }
}
