<?php
namespace Vokuro\Controllers;

use Phalcon\Mvc\View;
use Vokuro\Models\SeguimientoGStrackMe;

class SeguimientoController extends ControllerBase {
    public function indexAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->setVar("marcadores", json_encode(SeguimientoGStrackMe::findLastLocation()->toArray()));
    }

    public function lastlocationAction(){
        $this->view->disable();
        if ($this->request->isGet() == true) {
            $resp = array();
            $ll = SeguimientoGStrackMe::findLastLocation();
            if($ll->valid()){
                $resp = $ll->toArray();
                $this->response->setContent(json_encode($resp));
            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }
}
