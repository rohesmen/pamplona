<?php
namespace Vokuro\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Vokuro\Models\Indicators;
use Vokuro\Models\Messages;
use Vokuro\Models\Users;

/**
 * ControllerBase
 * This is the base controller for all controllers in the application
 */
class ControllerBase extends Controller
{

    /**
     * Execute before the router so we can determine if this is a provate controller, and must be authenticated, or a
     * public controller that is open to all.
     *
     * @param Dispatcher $dispatcher
     * @return boolean
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
//        $enc = $this->request->getHeader("Enc");
//        if($enc != null){
//            $this->logger->info("ENC");
//        }
//
//        $token = '$2a$08$n1U1buD35u6JVD1zgXkxPOdm.0rx0NoF2mSFi4GTtJpa4zD3l4MaG';
            $identity = $this->auth->getIdentity();
            $controllerName = $dispatcher->getControllerName();
        $actionName = $dispatcher->getActionName();
        if($controllerName == 'api' || $controllerName == 'apicob'){
            $this->view->disable();
            if ($this->request->isOptions()) {
                $this->response
                    ->setHeader('Access-Control-Allow-Origin', '*')
                    ->setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
                    ->setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type, Authorization, token')
                    ->setHeader('Access-Control-Allow-Credentials', 'true');

                $this->response->setStatusCode(200, 'OK')->send();

                exit;
            }

            $this->response
                ->setHeader('Access-Control-Allow-Origin', '*')
                ->setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
                ->setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type, Authorization, token')
                ->setHeader('Access-Control-Allow-Credentials', 'true');
//                ->setHeader('Access-Control-Allow-Credentials', 'true');

            $headers = $this->request->getHeaders();
//            $this->logger->info(' <=1=> '.$this->session->getId());
            if(isset($headers["Token"])){
//                $this->logger->info(' <=2=> '.$this->session->getId());
                $token = $headers["Token"];
                $logger = $this->getDI()->getLogger();

//                $session = Session::findFirstById($token);
                if($token == md5("app-pamplona")){
                    return true;
                }
                else{
                    $this->response->setStatusCode(401);
                    return false;
                }

            }
            elseif(!$this->acl->isPrivateAction($controllerName, $actionName)){
                return true;
            }
            else{
                $this->response->setStatusCode(401);
                return false;
            }
        }

//            if (!is_array($identity) && $controllerName != 'session' ) {
//                $this->response->redirect('session/login');
//                return false;
//            }

            $this->view->setVar('identity',$identity);


//        $this->logger->info("Route [".(is_array($this->auth->getIdentity())? http_build_query($this->auth->getIdentity(),'',',') : '')."] => [".$controllerName." -> ".$dispatcher->getActionName()."]");

            // Only check permissions on private controllers
            $this->view->setVar("TOKEN", "6607e3c028e84b422effca0e22c0b1932b8e253f");
            if ($this->acl->isPrivate($controllerName)) {

                // Get the current identity
                //$identity = $this->auth->getIdentity();

                if($this->acl->isSession($controllerName, $actionName)){
                    if (is_array($identity)) {
                        return true;
                    }
                }

                $user = Users::findFirst($identity["id"]);
                if($user and !$user->activo){
                    $this->response->redirect('session/login');
                    //$this->response->setStatusCode(403);
                    return true;
                }
                
                // If there is no identity available the user is redirected to index/index
                if (!is_array($identity)) {

                    //$this->flash->notice('You don\'t have access to this module: private');

                    /*$dispatcher->forward(array(
                        'controller' => 'session',
                        'action' => 'login'
                    ));*/

                    if ($this->request->isAjax() == true) {
                        $this->view->disable();
                        $this->response->setStatusCode(401);
                        return false;
                    }

                    $this->response->redirect('session/login');
                    return false;
                }

                // Check if the user have permission to the current option

                $controllerAllowed = false;
                $actionAllowed = false;

                foreach ($identity['profiles'] as $profile) {
//                $this->logger->info("Profile [".$profile->nombre."] => [".$controllerName." -> ".$actionName."]");
                    if ($this->acl->isAllowed($profile->nombre, $controllerName, $actionName)) {
                        $controllerAllowed = true;
                        $actionAllowed = true;
                        break;
                    }
                    if ($this->acl->isAllowed($profile->nombre, $controllerName, 'index')) {
                        $controllerAllowed = true;
                        $actionAllowed = false;
                        break;
                    }
                }
                if ($controllerName == "index" and $actionName == "index") {
                    $controllerAllowed = true;
                    $actionAllowed = false;
                }
                if(!$controllerAllowed){
                    //$this->flash->notice('You don\'t have access to this module: ' . $controllerName . ':' . $actionName);
                    if ($this->request->isAjax() == true) {
                        $this->view->disable();
                        $this->response->setStatusCode(403);
                        return false;
                    }
                        $dispatcher->forward(array(
                            'controller' => 'error',
                            'action' => 'index'
                        ));
                    return false;
                }

                $this->view->setVar("TOKEN", "6607e3c028e84b422effca0e22c0b1932b8e253f");
            }
    }
    public function convertDateToDefaultTZ($strDate){
        $dt = new \DateTime($strDate);
        $dt->setTimeZone(new \DateTimeZone(date_default_timezone_get()));
        return $dt->format('c');
    }
}
