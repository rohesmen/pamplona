<?php
namespace Vokuro\Controllers;

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Mvc\View;
use Vokuro\Models\Coordinador;
use Vokuro\Models\Supervisor;
use Vokuro\Models\Chofer;
use Vokuro\Models\Recolector;
use Vokuro\Models\Bitacora;
use Vokuro\Models\Unidad;
use Vokuro\Models\Turno;
use Vokuro\Models\Users;
use PHPExcel;
use PHPExcel_Style_Fill;
use PHPExcel_Settings;

/**
 * Vokuro\Controllers\BitReportesController
 * CRUD to manage bitacora
 */
class BitreportesController extends ControllerBase
{

    public function initialize()
    {
        $this->view->setTemplateBefore('public');
    }

    /**
     * Default action, shows the search form
     */
    public function indexAction()
    {
    }
	
	public function getdataAction()
    {
		if ($this->request->isPost() == true) {
            $rawBody = $this->request->getJsonRawBody();
			$resp = new \stdClass();

			$data = array();

			$dbData = null;

			switch ($rawBody->type){
                case "chofer":
                    $dbData = Chofer::findByActivo();
                    break;
                case "recolector":
                    $dbData = Recolector::findByActivo();
                    break;
                case "coordinador":
                    $dbData = Coordinador::findByActivo();
                    break;
                case "supervisor":
                    $dbData = Supervisor::findByActivo();
                    break;
            }

			foreach ($dbData as $key => $r){
				$rTemp = new \stdClass();
				$rTemp->id = $r->id;
				$rTemp->nombre = $r->nombres." ".$r->apepat." ".$r->apemat;
				array_push($data, $rTemp);
			}

            $resp->ok = true;
            $resp->status = 200;
			$resp->data = $data;
			
			$this->response->setContent(json_encode($resp));
			return $this->response;
        }
    }

    public function bitacoraAction()
    {
        if ($this->request->isPost() == true) {
            $rawBody = $this->request->getJsonRawBody();
            $resp = new \stdClass();
            $dias = array('Lunes','Martes','Miércoles','Jueves','Viernes','Sábado', 'Domingo');

            $unidades = Unidad::findByActivo();
            $choferes = Chofer::findByActivo();
            $recolectores = Recolector::findByActivo();
            $turnos = Turno::findByActivo();
            $supervisores = Supervisor::findByActivo();
            $coordinadores = Coordinador::findByActivo();
            $usuarios = Users::findByActivo();

            switch ($rawBody->tipo){
                case "chofer":
                    $type = "idchofer";
                    break;
                case "recolector":
                    $type = "idrecolector1";
                    break;
                case "supervisor":
                    $type = "idsupervisor";
                    break;
                case "coordinador":
                    $type = "idcoordinador";
                    break;
                case "todos":
                    $type = "todos";
                    break;
            }

            $idType = $rawBody->idTipo;

            $startDate = date('Y/m/d', date_create_from_format("d/m/Y",$rawBody->fechaI)->getTimestamp());

            $interval = intval(date_diff(date_create_from_format("d/m/Y",$rawBody->fechaI), date_create_from_format("d/m/Y",$rawBody->fechaF))->format("%a"));

			//$this->response->setContent(json_encode($interval));
			//return $this->response;
			
            $bitacora = array();
            $current = null;
			if($interval > 0){
				for($i=0; $i < $interval; $i++){
					if($i == 0){
						$current  =  $startDate;
					}else{
						$current = date('Y/m/d', strtotime($current . ' +1 day'));
					}
					$result = Bitacora::findByDateFilter($current, $type, $idType);
					if(count($result) > 0){
						$b = new \stdClass();
						$b->dia = $dias[date('N', strtotime($current)) - 1];

						$bitResult = array();
						foreach ($result as $key => $r){
							$bit = new \stdClass();

							$sup = $this->filterRecord($supervisores, $r->idsupervisor);
							$coord = $this->filterRecord($coordinadores, $r->idcoordinador);
							$chof = $this->filterRecord($choferes, $r->idchofer);
							$r1 = $this->filterRecord($recolectores, $r->idrecolector1);
							$r2 = $this->filterRecord($recolectores, $r->idrecolector2);
							$r3 = $this->filterRecord($recolectores, $r->idrecolector3);
							$turno = $this->filterRecord($turnos, $r->idturno);

							$bit->fecha = $r->fecha;
							$bit->supervisor = $sup->nombres." ".$sup->apepat." ".$sup->apemat;
							$bit->color = $sup->color;
							$bit->unidad = $this->filterRecord($unidades, $r->idunidad)->nombre;

							$bit->horasalida = $r->horasalida == "" ? "00:00" : $r->horasalida;
							$bit->horaregreso = $r->horaregreso == "" ? "00:00" : $r->horaregreso;
							$bit->totalhoras = $r->totalhoras == "" ? "00:00" : $r->totalhoras;
							$bit->tiempomuerto = $r->tiempomuerto == "" ? "00:00" : $r->tiempomuerto;

							if($turno){
								$bit->turno =  $turno->nombre;
							}else{
								$bit->turno = "";
							}

							if($chof){
								$bit->chofer = $chof->nombres." ".$chof->apepat." ".$chof->apemat;
							}else{
								$bit->chofer = "";
							}

							if($sup){
								$bit->supervisor = $sup->nombres." ".$sup->apepat." ".$sup->apemat;
							}else{
								$bit->supervisor = "";
							}

							if($coord){
								$bit->coordinador = $coord->nombres." ".$coord->apepat." ".$coord->apemat;
							}else{
								$bit->coordinador = "";
							}

							if($r1){
								$bit->recolector1 = $r1->nombres." ".$r1->apepat." ".$r1->apemat;
							}else{
								$bit->recolector1 = "";
							}

							if($r2){
								$bit->recolector2 = $r2->nombres." ".$r2->apepat." ".$r2->apemat;
							}else{
								$bit->recolector2 = "";
							}

							if($r3){
								$bit->recolector3 = $r3->nombres." ".$r3->apepat." ".$r3->apemat;
							}else{
								$bit->recolector3 = "";
							}

							$bit->kilometraje = $r->kilometraje;
							$bit->vueltasrelleno = $r->vueltasrelleno;
							$bit->usuario = $this->filterRecord($usuarios, $r->idusuario)->usuario;
							$bit->observaciones = $r->observaciones;
							$b->bitacora = $result;

							array_push($bitResult, $bit);
						}
						$b->bitacora = $bitResult;

						array_push($bitacora, $b);
					}
				}
			}else{
				$result = Bitacora::findByDateFilter($startDate, $type, $idType);
				if(count($result) > 0){
					$b = new \stdClass();
					$b->dia = $dias[date('N', strtotime($current)) - 1];

					$bitResult = array();
					foreach ($result as $key => $r){
						$bit = new \stdClass();

						$sup = $this->filterRecord($supervisores, $r->idsupervisor);
						$coord = $this->filterRecord($coordinadores, $r->idcoordinador);
						$chof = $this->filterRecord($choferes, $r->idchofer);
						$r1 = $this->filterRecord($recolectores, $r->idrecolector1);
						$r2 = $this->filterRecord($recolectores, $r->idrecolector2);
						$r3 = $this->filterRecord($recolectores, $r->idrecolector3);
						$turno = $this->filterRecord($turnos, $r->idturno);

						$bit->fecha = $r->fecha;
						$bit->supervisor = $sup->nombres." ".$sup->apepat." ".$sup->apemat;
						$bit->color = $sup->color;
						$bit->unidad = $this->filterRecord($unidades, $r->idunidad)->nombre;

						$bit->horasalida = $r->horasalida == "" ? "00:00" : $r->horasalida;
						$bit->horaregreso = $r->horaregreso == "" ? "00:00" : $r->horaregreso;
						$bit->totalhoras = $r->totalhoras == "" ? "00:00" : $r->totalhoras;
						$bit->tiempomuerto = $r->tiempomuerto == "" ? "00:00" : $r->tiempomuerto;

						if($turno){
							$bit->turno =  $turno->nombre;
						}else{
							$bit->turno = "";
						}

						if($chof){
							$bit->chofer = $chof->nombres." ".$chof->apepat." ".$chof->apemat;
						}else{
							$bit->chofer = "";
						}

						if($sup){
							$bit->supervisor = $sup->nombres." ".$sup->apepat." ".$sup->apemat;
						}else{
							$bit->supervisor = "";
						}

						if($coord){
							$bit->coordinador = $coord->nombres." ".$coord->apepat." ".$coord->apemat;
						}else{
							$bit->coordinador = "";
						}

						if($r1){
							$bit->recolector1 = $r1->nombres." ".$r1->apepat." ".$r1->apemat;
						}else{
							$bit->recolector1 = "";
						}

						if($r2){
							$bit->recolector2 = $r2->nombres." ".$r2->apepat." ".$r2->apemat;
						}else{
							$bit->recolector2 = "";
						}

						if($r3){
							$bit->recolector3 = $r3->nombres." ".$r3->apepat." ".$r3->apemat;
						}else{
							$bit->recolector3 = "";
						}

						$bit->kilometraje = $r->kilometraje;
						$bit->vueltasrelleno = $r->vueltasrelleno;
						$bit->usuario = $this->filterRecord($usuarios, $r->idusuario)->usuario;
						$bit->observaciones = $r->observaciones;
						$b->bitacora = $result;

						array_push($bitResult, $bit);
					}
					$b->bitacora = $bitResult;

					array_push($bitacora, $b);
				}
			}
            
            //$bitacora = Bitacora::findByDates($startDate, $endDate);

            $resp->bitacora = $bitacora;
			$this->session->set('bitreportes-data', $bitacora);

            $this->response->setContent(json_encode($resp));
            return $this->response;
        }
    }

	public function listadoAction(){
        set_time_limit(300);
        ini_set('memory_limit', '1024M');

        $bitacora = $this->session->get('bitreportes-data');
		
		if($bitacora){
			PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setTitle("Bitácora");
			
			
			$counter = 1;
			foreach ($bitacora as $key => $b){
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$counter, "SUPERVISOR");
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$counter, "COORDINADOR");
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$counter, "UNIDAD");
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$counter, "TURNO");
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$counter, "FECHA");
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$counter, "HORA DE SALIDA");
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$counter, "HORA DE REGRESO");
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$counter, "KILOMETRAJE");
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$counter, "VUELTAS RELLENO");
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$counter, "TIEMPO MUERTO");
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$counter, "CHOFER");
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$counter, "RECOLECTOR 1");
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$counter, "RECOLECTOR 2");
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$counter, "RECOLECTOR 3");
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$counter, "OBSERVACIONES");
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$counter, "TOTAL DE HORAS");
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$counter, "USUARIO");
				$objPHPExcel->getActiveSheet()->getStyle("A".$counter.":Q".$counter)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("A".$counter.":Q".$counter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("A2A2A2");
			
				$color = "000000";
				foreach ($b->bitacora as $key2 => $b2){
					$counter++;
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$counter, $b2->supervisor);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$counter, $b2->coordinador);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$counter, $b2->unidad);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$counter, $b2->turno);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$counter, $b2->fecha);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$counter, $b2->horasalida);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$counter, $b2->horaregreso);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$counter, $b2->kilometraje);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$counter, $b2->vueltasrelleno);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$counter, $b2->tiempomuerto);
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$counter, $b2->chofer);
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$counter, $b2->recolector1);
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$counter, $b2->recolector2);
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$counter, $b2->recolector3);
					$objPHPExcel->getActiveSheet()->setCellValue('O'.$counter, $b2->observaciones);
					
					$objPHPExcel->getActiveSheet()->setCellValue('P'.$counter, $b2->totalhoras);
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$counter, $b2->usuario);
					
					//$objPHPExcel->getActiveSheet()->getStyle("A".$counter.":Q".$counter)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("A".$counter.":Q".$counter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB(str_replace("#", "", $b2->color));
				}
				$counter++;				
			}
			
			foreach($objPHPExcel->getActiveSheet()->getColumnDimension() as $col) {
				$col->setAutoSize(true);
			}
			
			$objPHPExcel->getActiveSheet()->calculateColumnWidths();
			
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('B')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('C')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('D')->setAutoSize(true);
			
			

			//$objPHPExcel->getActiveSheet()->fromArray($bitacora, null, 'A1');
			
			$fname = date('dmY')."_reporte.xlsx";

			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="export.xlsx"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');
			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0
			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('php://output');
			exit;
		}
    }
	
    public function tabsAction(){
        set_time_limit(300);
        ini_set('memory_limit', '1024M');

        $bitacora = $this->session->get('bitreportes-data');
		
		if($bitacora){
			PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			$objPHPExcel = new PHPExcel();
			
			$index = 0;
			foreach ($bitacora as $key => $b){
				if($index > 0){
					$objPHPExcel->createSheet();
				}
				$objPHPExcel->setActiveSheetIndex($index);
				$objPHPExcel->getActiveSheet()->setTitle($b->dia);
				$objPHPExcel->getActiveSheet()->setCellValue('A1', "SUPERVISOR");
				$objPHPExcel->getActiveSheet()->setCellValue('B1', "COORDINADOR");
				$objPHPExcel->getActiveSheet()->setCellValue('C1', "UNIDAD");
				$objPHPExcel->getActiveSheet()->setCellValue('D1', "TURNO");
				$objPHPExcel->getActiveSheet()->setCellValue('E1', "FECHA");
				$objPHPExcel->getActiveSheet()->setCellValue('F1', "HORA DE SALIDA");
				$objPHPExcel->getActiveSheet()->setCellValue('G1', "HORA DE REGRESO");
				$objPHPExcel->getActiveSheet()->setCellValue('H1', "KILOMETRAJE");
				$objPHPExcel->getActiveSheet()->setCellValue('I1', "VUELTAS RELLENO");
				$objPHPExcel->getActiveSheet()->setCellValue('J1', "TIEMPO MUERTO");
				$objPHPExcel->getActiveSheet()->setCellValue('K1', "CHOFER");
				$objPHPExcel->getActiveSheet()->setCellValue('L1', "RECOLECTOR 1");
				$objPHPExcel->getActiveSheet()->setCellValue('M1', "RECOLECTOR 2");
				$objPHPExcel->getActiveSheet()->setCellValue('N1', "RECOLECTOR 3");
				$objPHPExcel->getActiveSheet()->setCellValue('O1', "OBSERVACIONES");
				
				$objPHPExcel->getActiveSheet()->setCellValue('P1', "TOTAL DE HORAS");
				$objPHPExcel->getActiveSheet()->setCellValue('Q1', "USUARIO");
				
				$objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("A2A2A2");
				
				$counter = 1;
				$color = "000000";
				foreach ($b->bitacora as $key2 => $b2){
					$counter++;
					
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$counter, $b2->supervisor);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$counter, $b2->coordinador);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$counter, $b2->unidad);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$counter, $b2->turno);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$counter, $b2->fecha);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$counter, $b2->horasalida);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$counter, $b2->horaregreso);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$counter, $b2->kilometraje);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$counter, $b2->vueltasrelleno);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$counter, $b2->tiempomuerto);
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$counter, $b2->chofer);
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$counter, $b2->recolector1);
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$counter, $b2->recolector2);
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$counter, $b2->recolector3);
					$objPHPExcel->getActiveSheet()->setCellValue('O'.$counter, $b2->observaciones);
					
					$objPHPExcel->getActiveSheet()->setCellValue('P'.$counter, $b2->totalhoras);
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$counter, $b2->usuario);
					$color = str_replace("#", "", $b2->color);
					//$objPHPExcel->getActiveSheet()->getStyle("A".$counter.":Q".$counter)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("A".$counter.":Q".$counter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($color);
				}
				
				$index++;
			}
			
			
			//$objPHPExcel->getActiveSheet()->fromArray($bitacora, null, 'A1');
			
			$fname = date('dmY')."_reporte.xlsx";

			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="export.xlsx"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');
			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0
			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('php://output');
			exit;
		}
    }

    private function filterRecord($records, $id){
        foreach ($records as $key => $r){
            if($r->id == $id){
                return $r;
            }
        }

        return null;
    }
}
