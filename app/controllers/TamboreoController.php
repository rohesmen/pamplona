<?php
namespace Vokuro\Controllers;

use Vokuro\Models\Clientes;
use Phalcon\Mvc\View;
use Vokuro\Models\FacturacionTamboreo;


/**
 * Display the default index page.
 */
class TamboreoController extends ControllerBase
{



    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function indexAction()
    {
        /*$this->view->setVar('logged_in', is_array($this->auth->getIdentity()));*/
        $this->view->setTemplateBefore('public');

        $this->view->setVar('doSearch', $this->acl->isAllowedUser('users', 'search'));
        //$this->view->setVar('colonias', Colonias::findByActivo());
        //$this->view->setVar('usuarios', Users::findByActivo());

    }

    public function searchAction(){

        $view = clone $this->view;
        $this->view->disable();




        if ($this->request->isPost() == true) {

            $rawBody = $this->request->getJsonRawBody();

            $results = $this->filterSearch($rawBody);



        }
        else{
            $view->setVar('NotImplemented', true);
        }

        $this->response->setContent(json_encode($results));
        return $this->response;



    }



    private function filterSearch($rawBody){

        $request = $this->request;
        $response = $this->response;

        $nombre = $rawBody->titulo;
        $clave = $rawBody->clave;
        $tipo = $rawBody->tipo;

        $clave = preg_replace('/\s+/', ' ',$clave);
        $nombre = preg_replace('/\s+/', ' ',$nombre);



        $where = ' 1 = 1 ';
        if($tipo == "nom")
            $where .= " AND lower(nombre) LIKE lower('%$nombre%')";

        if($tipo == "cla")
            $where .= " AND id = ". $clave;

        $results= array();



        $results = Clientes::findByQuery("select *
            FROM cliente.facturacion_tamboreo 
            WHERE  ".$where." ORDER BY id ASC");


        $rows = array();
        if(count($results) > 0){

            foreach($results as $res) {

                $vigente = $res->activo ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                    : '<i class="fa fa-remove" style="color: red" title="Activo"></i>';


                $d = array();
                $d["0"] = null;
                $d["1"] = null;
                $d["2"] = $vigente;
                $d["3"] = $res->id;
                $d["4"] = $res->nombre;
                $d["5"] = $res->descripcion;
                $d["6"] = date('d/m/Y', strtotime($res->fecha_creacion));
                $d["DT_RowId"] = "tamboreo-$res->id";
                array_push($rows, $d);

            }
        }

        return $rows;
    }



    public function guardarAction()
    {


        $view = clone $this->view;
        $this->view->disable();
        $respuesta = [];

        $rawBody = $this->request->getJsonRawBody();

        if($rawBody) {
            $id = intval($rawBody->clave);
            $resp = new \stdClass();


            $this->db->begin();



            /*$phql = "UPDATE INTO Cars (name, brand_id, year, style) "
                . "VALUES ('Lamborghini Espada', 7, 1969, 'Grand Tourer')";
            $manager->executeQuery($phql);*/


            $results = Clientes::findByQuery("select *
            FROM cliente.facturacion_tamboreo
            WHERE activo = true AND nombre = '$rawBody->nombre' ORDER BY id ASC");



            if(count($results) > 0){

                foreach($results as $res) {





                    $tamboreo = FacturacionTamboreo::findFirstById($res->id);


                    $tamboreo->assign(array(
                        'activo' => false,
                        'fecha_modificacion' => 'NOW()'
                    ));

                    if(!$tamboreo->save()) {
                        $this->db->rollback();
                        $this->flash->error($tamboreo->getMessages());
                        $this->response->setContent(json_encode(["lOk" => false, "cMensaje" => "No fue posible guardar el registro(1)", "data" => $this->flash->error($tamboreo->getMessages())]));
                        return $this->response;
                    }
                }
            }

            $nuevo = true;
            $registro = new FacturacionTamboreo();

            $registro->assign(array(
                //                'id' => ,
                'activo' => $rawBody->activo,
                'nombre' =>  $rawBody->nombre,
                'descripcion' => $rawBody->descripcion,
                'monto_iva' => $rawBody->monto,
                'monto_sin_iva' => $rawBody->monto,
                'fecha_creacion' => 'NOW()',
                'fecha_modificacion' => 'NOW()'

            ));



            if(!$registro->save()) {
                $this->db->rollback();
                $this->flash->error($registro->getMessages());
                $this->response->setContent(json_encode(["lOk" => false, "cMensaje" => "No fue posible guardar el registro", "data" => $this->flash->error($registro->getMessages())]));
                return $this->response;
            }
            else
            {


                $resp = $this->buscaRegistro($registro->id);
                $resp->lOk = true;
                $resp->cMensaje = "";
                $resp->nuevo = $nuevo;
                $d = array();


                $resp->DT_RowId = "tamboreo-$resp->id";

                $this->db->commit();
                $this->response->setContent(json_encode($resp));
                return $this->response;
            }


            die;
        }

    }

    private function buscaRegistro($id) {

            $registro = FacturacionTamboreo::findFirstById($id);
            if($registro != false){


                $vigente = $registro->activo ? '<i class="fa fa-check" style="color: green" title="Activo"></i>'
                    : '<i class="fa fa-remove" style="color: red" title="Activo"></i>';

                $row = new \stdClass();


                $row->id = $registro->id;
                $row->nombre = $registro->nombre;
                $row->descripcion = $registro->descripcion;
                $row->fechaCreacion = date('d/m/Y', strtotime($registro->fecha_creacion));
                $row->vigente = $vigente;
                $row->monto = $registro->monto_iva;
                return $row;


        }

        return null;
    }

    public function getAction($id) {
        $this->view->disable();
        if ($this->request->isGet() == true) {




            $row = $this->buscaRegistro($id);
            if($row != null)

                $this->response->setContent(json_encode($row));

            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(501, "Not Implemented");
        }
        return $this->response;
    }

    public function deleteAction($id)
    {
        if($id != null and $id != ""){
            $this->db->begin();

            $queja = Queja::findFirstById($id);
            if ($queja) {
                $queja->activo = false;
                $queja->fecha_modificacion = date("c");
                if(!$queja->save())
                    $this->db->rollback();
                else
                    $this->db->commit();

            }
            else{
                $this->response->setStatusCode(404, "Not Found");
            }
        }
        else{
            $this->response->setStatusCode(400, "Bad Request");
        }
        return $this->response;
    }


}
