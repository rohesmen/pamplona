<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Chofer extends Model
{
    public function initialize()
    {
        $this->setSchema("monitoreo");
    }

    public function getSource()
    {
        return "chofer";
    }

	public static function findByActivo() {
        $sql = "select id, nombres, apepat, apemat from monitoreo.chofer where activo=true order by nombres asc";
        $choferes = new Chofer();

        return new Resultset(null, $choferes, $choferes->getReadConnection()->query($sql));
    }
}