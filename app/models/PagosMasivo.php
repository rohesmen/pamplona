<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class PagosMasivo extends Model{
	
    public function initialize(){
        $this->setSchema("cliente");
    }

    public function getSource(){
        return "pagos_masivo";
    }   
	//--------------------------------------------------------------------------------------------
	public static function findByQuery($sql){
        $result = new PagosMasivo();
        return new Resultset(null, $result, $result->getReadConnection()->query($sql));
    }//fin:findByQuery
	//--------------------------------------------------------------------------------------------
	 public static function findByActivo($activo = true){
        
		$sql = "";
        $orazonsocial = new PagosMasivo();
        return new Resultset(null, $orazonsocial, $orazonsocial->getReadConnection()->query($sql));
    }//fin:findByActivo
	//--------------------------------------------------------------------------------------------
	public static function updateByQuery($sqlquery){		
		$oPagosMasivo = new PagosMasivo();
		$result = $oPagosMasivo->getReadConnection()->execute($sqlquery);
		return $result;
	}//fin:updateByQuery
	//--------------------------------------------------------------------------------------------
}