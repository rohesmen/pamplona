<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Pagos extends  Model{

    public function initialize(){
        $this->setSchema("cliente");
    }//fin:initialize
	//-------------------------------------------------------------------------
    public function getSource(){
        return "pagos";
    }//fin:getSource
	//-------------------------------------------------------------------------
    public static function findByQuery($sql){
        $result = new Pagos();
        return new Resultset(null, $result, $result->getReadConnection()->query($sql));
    }//fin:findByQuery
	//----------------------------------------------------------------------
	public static function updateByQuery($sqlquery){		
			$oPagos = new Pagos();
			$result = $oPagos->getReadConnection()->execute($sqlquery);
			return $result;
	}//fin:updateByQuery	
	//----------------------------------------------------------------------
}
?>