<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Application extends Model
{

    public function initialize()
    {
        $this->setSchema("usuario");
    }

    public function getSource()
    {
        return "aplicacion";
    }

}