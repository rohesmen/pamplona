<?php

	namespace Vokuro\Models;

	use Phalcon\Mvc\Model;
	use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
	use Phalcon\Mvc\Model\Validator\Uniqueness;

	class Forma_pago extends  Model{
		
		public function initialize(){
			$this->setSchema("cliente");
		}//fin:
		//-------------------------------------------------------------------------
		public function getSource(){
			return "forma_pago";
		}//fin:
		//-------------------------------------------------------------------------
		/**
		 * [findByActivo obtiene los forma_pago que se encuentren activos]
		 * @param  boolean $activo [description]
		 * @return [type]          [description]
		 */
		public static function findByActivo($activo = true){
			$sql = "select id, nombre, descripcion from cliente.forma_pago 
			where activo = ".($activo? "true" : "false")." order by nombre asc";
			$oformapago = new Forma_pago();

			return new Resultset(null, $oformapago, $oformapago->getReadConnection()->query($sql));
		}//fin:findByActivo

	}//fin:class
?>