<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 * Vokuro\Models\Users
 * All the users registered in the application
 */

class TarifaColonia extends  Model
{

    public function initialize()
    {
        $this->setSchema("cliente");

    }

    public function getSource()
    {
        return "tarifa_colonia";
    }



    public static function findByQuery($sql){
        $tarifas = new TarifaColonia();
        return new Resultset(null, $tarifas, $tarifas->getReadConnection()->query($sql));
    }

}