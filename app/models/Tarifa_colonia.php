<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Tarifa_colonia extends  Model
{
    public function initialize()
    {
        $this->setSchema("cliente");
    }

    public function getSource()
    {
        return "tarifa_colonia";
    }

    /**
     * [findByActivo obtiene los tarifa_colonia que se encuentren activos]
     * @param  boolean $activo [description]
     * @return [type]          [description]
     */
    public static function findByActivo($activo = true) {
        $sql = "select id, idcolonia, tarifa from cliente.tarifa_colonia 
        where activo = ".($activo?"true":"false")." order by id asc";
        $tarifa_colonia = new Tarifa_colonia();

        return new Resultset(null, $tarifa_colonia, $tarifa_colonia->getReadConnection()->query($sql));
    }

}