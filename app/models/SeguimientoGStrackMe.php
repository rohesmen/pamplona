<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class SeguimientoGStrackMe extends  Model
{

    public function initialize()
    {
        $this->setSchema("comun");

    }

    public function getSource()
    {
        return "gstrackme_camiones_ubicacion";
    }

    public static function findLastLocation() {
        $sql = "select distinct on (vehiculo) * from comun.gstrackme_camiones_ubicacion 
        order by vehiculo asc, fecha_creacion desc";
        $seguimiento = new SeguimientoGStrackMe();

        return new Resultset(null, $seguimiento, $seguimiento->getReadConnection()->query($sql));
    }
}