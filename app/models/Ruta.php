<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Ruta extends  Model
{
    public function initialize()
    {
        $this->setSchema("monitoreo");
    }

    public function getSource()
    {
        return "ruta";
    }

    public function jsonSerialize() {
        return [ "id" => $this->id,
            "nombre" => $this->nombre,
            "nombre_archivo" => $this->nombre_archivo,
            "size_archivo" => $this->size_archivo,
            "geojson" => $this->geojson,
            "kml" => $this->kml,
        ];
    }
}