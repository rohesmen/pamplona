<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Rutas extends  Model
{
    public function initialize()
    {
        $this->setSchema("cliente");
    }

    public function getSource()
    {
        return "ruta";
    }

    /**
     * [findByActivo obtiene las rutas que se encuentren activos]
     * @param  boolean $activo [description]
     * @return [type]          [description]
     */
    public static function findByActivo($activo = true) {
        $sql = "select id, nombre, idcolonia from cliente.ruta 
        where activo = ".($activo?"true":"false")." order by nombre asc";
        $rutas = new Rutas();

        return new Resultset(null, $rutas, $rutas->getReadConnection()->query($sql));
    }

}