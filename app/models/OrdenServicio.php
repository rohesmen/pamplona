<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class OrdenServicio extends Model
{

    public function initialize()
    {
        $this->setSchema("servicio");
    }

    public function getSource()
    {
        return "orden_servicio";
    }

    public function jsonSerialize() {
        $est = EstadoOrdenServicio::findFirstById($this->idestado);
        $ser = Servicios::findFirstById($this->idservicio);
        $res = Brigada::findFirstById($this->idbrigada);
        $responsable = "Por asignar";
        if($res){
            $responsable = $res->responsable;
        }
        $horas = $ser->horas_estimadas + $ser->horas_tolereancia;
        $fini = $this->fecha."T".$this->hora;
        $fhfin = $this->fecha."T".$this->horafin;
        $horafin = $this->horafin;
        $cli = Clientes::findFirstById_cliente($this->idcliente);
        $direccion = "C. ".$cli->calle.($cli->calle_letra ? " ".$cli->calle_letra : "");
        $direccion .= " #".$cli->numero.($cli->numero_letra ? " ".$cli->numero_letra : "");
        if(!empty($cli->cruza1) && !empty($cli->cruza2)){
            $direccion .= "  X ".$cli->cruza1.($cli->letra_cruza1 ? " ".$cli->letra_cruza1 : "");
            $direccion .= "  Y ".$cli->cruza2.($cli->letra_cruza2 ? " ".$cli->letra_cruza2 : "");
        }
        elseif(!empty($cli->cruza1)){
            $direccion .= "  X ".$cli->cruza1.($cli->letra_cruza1 ? " ".$cli->letra_cruza1 : "");
        }
        elseif (!empty($cli->cruza2)){
            $direccion .= "  X ".$cli->cruza2.($cli->letra_cruza2 ? " ".$cli->letra_cruza2 : "");
        }

        if($cli->idcolonia){
            $eColonia = Colonias::findFirstById($cli->idcolonia);
            if($eColonia !== false){
                $direccion .= " ".$eColonia->nombre.", ".$eColonia->localidad;
            }
        }
        return [ "id" => $this->id,
            "idcliente" => $this->idcliente,
            "idservicio" => $this->idservicio,
            "servicio" => $ser->nombre,
            "estado" => $est->nombre,
            "idestado" => $est->id,
            "fecha"  => $this->fecha,
            "hora" => $this->hora,
            "idbrigada" => $this->idbrigada,
            "responsable" => $responsable,
            "colorestado" => $est->color,
            "fh_ini" => $fini,
            "fh_fin" =>$fhfin,
            "horafin" => $horafin,
            "clavestado" =>$est->clave,
            "correo" => $this->correo,
            "telefono" =>$this->telefono,
            "nomcli" => trim($cli->apepat." ".$cli->apemat." ".$cli->nombres),
            "costo" => $this->total,
            "factura" => $this->factura,
            "duracion" => $this->duracion,
            "observaciones" => $this->observaciones,
            "direccion" => $direccion,
            "referencia" => $this->referencia,
            "horasservicio" => $horas,
            "costoser" => $ser->costo,
            "iva" => $this->iva,
            "costo_extra" => $this->costo_extra
        ];
    }
}
