<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Tipo_descuento extends  Model
{
    public function initialize()
    {
        $this->setSchema("cliente");
    }

    public function getSource()
    {
        return "tipo_descuento";
    }

    /**
     * [findByActivo obtiene los tipos descuento que se encuentren activos]
     * @param  boolean $activo [description]
     * @return [type]          [description]
     */
    public static function findByActivo($activo = true) {
        $sql = "select id, nombre from cliente.tipo_descuento 
        where activo = ".($activo?"true":"false")." order by nombre asc";
        $tipo_descuento = new Tipo_descuento();

        return new Resultset(null, $tipo_descuento, $tipo_descuento->getReadConnection()->query($sql));
    }

}