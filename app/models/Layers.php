<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Layers extends Model
{

    public function initialize()
    {
        $this->setSchema("comun");
    }

    public function getSource()
    {
        return "capas";
    }

    public static function findByModuloAndActivo($modulo, $activo = true){
        $sql = "select * from comun.capas where modulo = '$modulo' AND activo = ".($activo ? "true" : "false")." order by orden";
        $layers = new Layers();

        return new Resultset(null, $layers, $layers->getReadConnection()->query($sql));
    }

    public static function findAll(){
        $sql = "select * from comun.capas c where activo = true order by orden";
        $layers = new Layers();
        return new Resultset(null, $layers, $layers->getReadConnection()->query($sql));
    }
	
	public static function findFirstByNombre($nombre)
    {
        $sql = "select * from comun.capas c where c.activo = true and c.nombre = '$nombre'";
        $layers = new Layers();

        $rs = new Resultset(null, $layers, $layers->getReadConnection()->query($sql));
        return $rs->getFirst();
    }
	
	public static function findByGrupoCapa($grupoCapa, $usuario){
		$sql  = "select * from comun.capas c  
        where c.idgrupo_capa = '$grupoCapa' AND c.activo = true order by c.orden";
		$layers = new Layers();
        return new Resultset(null, $layers, $layers->getReadConnection()->query($sql));
	}
}
