<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 * Vokuro\Models\Users
 * All the users registered in the application
 */

class TipoDescuento extends  Model
{

    public function initialize()
    {
        $this->setSchema("cliente");

    }

    public function getSource()
    {
        return "tipo_descuento";
    }



    public static function findByQuery($sql){
        $result = new TipoDescuento();
        return new Resultset(null, $result, $result->getReadConnection()->query($sql));
    }

}