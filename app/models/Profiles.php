<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
/**
 * Vokuro\Models\Profiles
 * All the profile levels in the application. Used in conjenction with ACL lists
 */
class Profiles extends Model
{

    /**
     * Define relationships to Users and Permissions
     */
    /*public function initialize()
    {
        $this->hasMany('id', __NAMESPACE__ . '\Users', 'profilesId', array(
            'alias' => 'users',
            'foreignKey' => array(
                'message' => 'Profile cannot be deleted because it\'s used on Users'
            )
        ));

        $this->hasMany('id', __NAMESPACE__ . '\Permissions', 'profilesId', array(
            'alias' => 'permissions'
        ));
    }*/
    public function initialize()
    {
        $this->setSchema("usuario");
        //$this->hasMany("id", "ProfilesPermissions", "idperfil");
        //$this->hasMany("id", "UsersProfiles", "idperfil");
    }

    public function getSource()
    {
        return "perfil";
    }

    public static function findByUser($id)
    {
        $sql   = "select p.* from usuario.perfil_usuario pp left join usuario.perfil p on (pp.idperfil = p.id) where pp.activo = true and pp.idusuario = ".$id." and p.activo = true";
        $profiles = new Profiles();

        return new Resultset(null, $profiles, $profiles->getReadConnection()->query($sql));
    }

    public static function findActivos()
    {
        $sql   = "select * from usuario.perfil where activo = true order by nombre asc";
        $profiles = new Profiles();

        return new Resultset(null, $profiles, $profiles->getReadConnection()->query($sql));
    }
}
