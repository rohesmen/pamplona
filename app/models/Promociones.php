<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Vokuro\Util\Util;

class Promociones extends Model
{

    public function initialize()
    {
        $this->setSchema("comun");
    }

    public function getSource()
    {
        return "promociones";
    }

    public static function getVigentes() {
        $sql = "select *, to_char(fecfin_vigencia, 'DD/MM/YYYY') finvigenciaf from comun.promociones 
        where fecini_vigencia <= current_date and current_date <= fecfin_vigencia and activo";
        $proms = new Promociones();

        return new Resultset(null, $proms, $proms->getReadConnection()->query($sql));
    }

    public static function getAnualVigente() {
        $sql = "select *, to_char(fecfin_vigencia, 'DD/MM/YYYY') finvigenciaf from comun.promociones 
        where fecini_vigencia <= current_date and current_date <= fecfin_vigencia and activo and tipo = 'anual' 
        order by fecini_vigencia asc limit 1";
        $proms = new Promociones();

        return new Resultset(null, $proms, $proms->getReadConnection()->query($sql));
    }
    public function jsonSerialize() {
        $fecvigen =Util::formatDateVForView($this->fecini_vigencia)." al ".Util::formatDateVForView($this->fecfin_vigencia);
        return [
            "id"=> $this->id,
            "nombre"=> $this->nombre,
            "alias"=> $this->alias,
            "descripcion"=> $this->descripcion,
            "imagen"=> $this->imagen,
            "fecini_vigencia"=> $this->fecini_vigencia,
            "fecfin_vigencia"=> $this->fecfin_vigencia,
            "anio"=> $this->anio,
            "meses"=> $this->meses,
            "tipo"=> $this->tipo,
            "vigenciaf" => $fecvigen
        ];
    }
}
