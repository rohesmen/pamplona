<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class RazonSocial extends Model{
	
    public function initialize()
    {
        $this->setSchema("cliente");
    }

    public function getSource()
    {
        return "razon_social";
    }

    public static function findByRazon_socialAndFisicaAndActivo($razon_social, $fisica, $activo = true) {
        $sql = "select * from cliente.razon_social 
        where razon_social like '%$razon_social%' and fisica = ".($activo?"true":"false")."  
        and activo = ".($activo?"true":"false")." order by razon_social asc";
        $razonSocial = new RazonSocial();

        return new Resultset(null, $razonSocial, $razonSocial->getReadConnection()->query($sql));
    }
	//--------------------------------------------------------------------------------------------
	public static function findByQuery($sql){
        $result = new RazonSocial();
        return new Resultset(null, $result, $result->getReadConnection()->query($sql));
    }//fin:findByQuery
	//--------------------------------------------------------------------------------------------
	 public static function findByActivo($activo = true){
        
		$sql = "select id, rfc, razon_social, (case when fisica then 1 else 0 end) as  fisica from cliente.razon_social where activo = "
			.($activo? "true" : "false")." order by razon_social asc";
        $orazonsocial = new RazonSocial();

        return new Resultset(null, $orazonsocial, $orazonsocial->getReadConnection()->query($sql));
    }//fin:findByActivo
	//--------------------------------------------------------------------------------------------
}