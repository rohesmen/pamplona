<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 * Vokuro\Models\Users
 * All the users registered in the application
 */

class Historial_Corte_Historial_Pago extends  Model
{

    public function initialize()
    {
        $this->setSchema("corte");
    }

    public function getSource()
    {
        return "historial_corte_historial_pago";
    }

        public static function findByQuery($sql){
        $hc = new Historial_Corte_Historial_Pago();
        return new Resultset(null, $hc, $hc->getReadConnection()->query($sql));
    }
}