<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Modules extends Model
{
    public function initialize()
    {
        $this->setSchema("usuario");
    }

    public function getSource()
    {
        return "modulo";
    }

    public static function findByIdGrupoModuloAndIdUsuario($idgrupomodulo, $idusuario){
        $sql   = "select * from usuario.modulo
        where id in (
            select distinct(p.idmodulo) from usuario.permiso p
            join usuario.perfil_permiso pp on pp.idpermiso = p.id
            join usuario.perfil_usuario pu on pu.idperfil = pp.idperfil
            where pu.idusuario = ".$idusuario." and p.accion = 'index'
            and pp.activo = true
            and pu.activo = true
            and p.activo = true
            and idmodulo is not null
        ) and activo = true and idgrupomodulo = ".$idgrupomodulo." order by orden asc";
        $module = new Modules();

        return new Resultset(null, $module, $module->getReadConnection()->query($sql));
    }
}