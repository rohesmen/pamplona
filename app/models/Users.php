<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 * Vokuro\Models\Users
 * All the users registered in the application
 */
class Users extends Model
{

    public function initialize()
    {
        $this->setSchema("usuario");
        //$this->hasMany("id", "UsersProfiles", "idusuario");
        /*$this->belongsTo('profilesId', __NAMESPACE__ . '\Profiles', 'id', array(
            'alias' => 'profile',
            'reusable' => true
        ));

        $this->hasMany('id', __NAMESPACE__ . '\SuccessLogins', 'usersId', array(
            'alias' => 'successLogins',
            'foreignKey' => array(
                'message' => 'User cannot be deleted because he/she has activity in the system'
            )
        ));

        $this->hasMany('id', __NAMESPACE__ . '\PasswordChanges', 'usersId', array(
            'alias' => 'passwordChanges',
            'foreignKey' => array(
                'message' => 'User cannot be deleted because he/she has activity in the system'
            )
        ));

        $this->hasMany('id', __NAMESPACE__ . '\ResetPasswords', 'usersId', array(
            'alias' => 'resetPasswords',
            'foreignKey' => array(
                'message' => 'User cannot be deleted because he/she has activity in the system'
            )
        ));*/
    }

    public function getSource()
    {
        return "usuario";
    }

    public static function getUserTodos(){
        $user = new \stdClass();
        $user->nombre = "Todos";
        $user->id = 0;
        return $user;
    }

    public static function findByActivo($activo = true){
        $sql = "select * from usuario.usuario where activo = ".($activo == true ? 'true': 'false')." order by usuario asc";
        $usuarios = new Users();

        return new Resultset(null, $usuarios, $usuarios->getReadConnection()->query($sql));
    }

    public static function findAll(){
        $sql = "select * from usuario.usuario order by usuario asc";
        $usuarios = new Users();

        return new Resultset(null, $usuarios, $usuarios->getReadConnection()->query($sql));
    }

    public static function findByTypeAndIdTerritory($type, $IdTerritory, $userTerritory = null, $userIdTerritory = null)
    {
        $filter = new \Phalcon\Filter();
        $strTerritoryWhere = "";
        if($userTerritory != null and $userIdTerritory != null){
            $strTerritoryWhere = " and ".$userTerritory."=".$userIdTerritory;
        }
        $where = " where ".$type." = ".$filter->sanitize(preg_replace('/\D/', '', $IdTerritory), "int").$strTerritoryWhere." ";
        $sql = "select * from usuario.usuario ".$where." order by usuario asc";
        $usuarios = new Users();

        return new Resultset(null, $usuarios, $usuarios->getReadConnection()->query($sql));
    }

    public static function findFromColumnAndValueNumber($column, $value, $userTerritory = null, $userIdTerritory = null){
        $strTerritoryWhere = "";
        if($userTerritory != null and $userIdTerritory != null){
            $strTerritoryWhere = " and ".$userTerritory."=".$userIdTerritory;
        }
        $where = " where ".$column." = ".$value." ".$strTerritoryWhere;
        $sql = "select * from usuario.usuario ".$where." order by usuario asc";
        $usuarios = new Users();

        return new Resultset(null, $usuarios, $usuarios->getReadConnection()->query($sql));
    }

    public static function findByNombre($datos)
    {
        $filter = new \Phalcon\Filter();
        $sqlWhere = "where ";
        foreach ($datos as $valor) {
            $sqlWhere .= "lower(coalesce(nombre,'') || ' ' || coalesce(apellido_paterno,'') || ' ' || coalesce(apellido_materno,'')) like '%".$filter->sanitize($valor, "string")."%' and ";
        }
        $sqlWhere = substr($sqlWhere, 0, -4);
        $sql = "select * from usuario.usuario ".$sqlWhere." order by usuario asc";
        $usuarios = new Users();

        return new Resultset(null, $usuarios, $usuarios->getReadConnection()->query($sql));
    }

    public static function findLikeByUsuario($usuario){
        $sql = "select * from usuario.usuario where usuario like '%".$usuario."%' order by usuario asc";
        $usuarios = new Users();

        return new Resultset(null, $usuarios, $usuarios->getReadConnection()->query($sql));
    }

    public static function findByUsuario($usuario){
        $sql = "select * from usuario.usuario where usuario = '".$usuario."'";
        $usuarios = new Users();

        return new Resultset(null, $usuarios, $usuarios->getReadConnection()->query($sql));
    }

    public function getFullName(){
        return trim($this->nombre." ".$this->apellido_paterno." ".$this->apellido_materno);
    }
}
