<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Servicios extends Model
{

    public function initialize()
    {
        $this->setSchema("servicio");
    }

    public function getSource()
    {
        return "servicios";
    }

    public function jsonSerialize() {
        $nombre = "";
        $precio = "";
        $tu = TipoUnidad::findFirstById($this->idtipo_unidad);
        if($tu){
            $nombre = $tu->nombre;
            $precion = $tu->precio;
        }
        return [ "id" => $this->id,
            "nombre" => $this->nombre,
            "descripcion" => $this->descripcion,
            "imagen" => $this->imagen,
            "horas_estimadas"  => $this->horas_estimadas,
            "horas_tolereancia" => $this->horas_tolereancia,
            "unidad" => $nombre,
            "precioxunidad" => $precio,
            "tipo_cobro" => $this->tipo_cobro,
            "costo" => $this->costo,
            "nota" => $this->nota,
            "idgrupo" => $this->idgrupo
        ];
    }
}
