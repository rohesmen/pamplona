<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class SeguimientoOrdenServicio extends Model
{

    public function initialize()
    {
        $this->setSchema("servicio");
    }

    public function getSource()
    {
        return "seguimiento_orden_servicio";
    }
}
