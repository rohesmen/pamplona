<?php
/**
 * Created by PhpStorm.
 * User: Pauli
 * Date: 18/10/2016
 * Time: 10:03 PM
 */

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Colonias extends  Model
{

    public function initialize()
    {
        $this->setSchema("cliente");
    }

    public function getSource()
    {
        return "colonia";
    }

    public static function findByActivo($activo = true) {
        $sql = "select id, nombre||', '||COALESCE(localidad, '') as nombre, monto, ismarginado from cliente.colonia 
        where activo = ".($activo?"true":"false")." order by nombre asc";
        $colonias = new Colonias();

        return new Resultset(null, $colonias, $colonias->getReadConnection()->query($sql));
    }

    public function findByColonia($id) {
        $sql = "select * from cliente.colonia where id = ".$id;
        $colonias = new Colonias();
        return new Resultset(null, $colonias, $colonias->getReadConnection()->query($sql));
    }

    public static function validaByNombre($nombre, $localidad){
        $sql = "select * from cliente.colonia where upper(nombre) = upper('".$nombre."') and upper(localidad) = upper('".$localidad."') and activo=true";
        $colonias = new Colonias();
        // echo $sql;
        // die;

        return new Resultset(null, $colonias, $colonias->getReadConnection()->query($sql));
    }

    public static function findAll() {
        $sql = "select * from cliente.colonia order by nombre asc";
        $colonias = new colonias();

        return new Resultset(null, $colonias, $colonias->getReadConnection()->query($sql));
    }

    public static function findFromColumnAndValueNumber($column, $value, $userTerritory = null, $userIdTerritory = null){
        $strTerritoryWhere = "";
        /*if($userTerritory != null and $userIdTerritory != null){
            $strTerritoryWhere = " and ".$userTerritory."=".$userIdTerritory;
        }*/
        $where = " where ".$column." = ".$value." ".$strTerritoryWhere;
        $sql = "select * from cliente.colonia ".$where." order by nombre asc";
        $colonias = new Colonias();

        return new Resultset(null, $colonias, $colonias->getReadConnection()->query($sql));
    }

    public static function findByAliasAndNombre($alias, $nombre, $coloniaAlias = null, $coloniaNombre = null)
    {
        $filter = new \Phalcon\Filter();
        $strTerritoryWhere = "";
        // if($coloniaAlias != null and $coloniaNombre != null){
        //     $strTerritoryWhere = " and ".$coloniaAlias."=".$coloniaNombre;
        // }
        $where = " where ".$alias." = ".$filter->sanitize(preg_replace('/\D/', '', $nombre), "string").$strTerritoryWhere." ";
        $sql = "select * from cliente.colonia ".$where." order by nombre asc";
        $colonias = new Colonias();

        return new Resultset(null, $colonias, $colonias->getReadConnection()->query($sql));
    }

    public static function findByNombre($datos, $userTerritory = null, $userIdTerritory = null)
    {
        $filter = new \Phalcon\Filter();
        $sqlWhere = " where ";
        foreach ($datos as $clave => $valor) {
            if($clave > 0)
                $sqlWhere .= " OR ";
            $sqlWhere .= " upper(coalesce(nombre,'')) like upper('%".$filter->sanitize($valor, "string")."%') ";
        }
        /*if($userTerritory != null and $userIdTerritory != null) {
            $sqlWhere .= $userTerritory."=".$userIdTerritory. " ";
        }*/
        $sqlWhere .= " ";
        $sql = "select * from cliente.colonia ".$sqlWhere." order by nombre asc";
        $colonias = new Colonias();

        return new Resultset(null, $colonias, $colonias->getReadConnection()->query($sql));
    }

    public static function findFromColumnAndValueString($column, $value, $userTerritory = null, $userIdTerritory = null) {
        $strTerritoryWhere = "";
        /*if($userTerritory != null and $userIdTerritory != null){
            $strTerritoryWhere = " and ".$userTerritory."=".$userIdTerritory;
        }*/
        $where = " where upper(".$column.") like upper('%".$value."%') ".$strTerritoryWhere;
        $sql = "select * from cliente.colonia ".$where." order by nombre asc";
        $colonias = new Colonias();
        // echo $sql;
        // die;

        return new Resultset(null, $colonias, $colonias->getReadConnection()->query($sql));
    }

    public function jsonSerialize() {
        $nommun = "";
        $mun = MunicipiosYucatan::findFirstById($this->cve_mun);
        if($mun){
            $nommun = $mun->nombre;
        }
        return [ "id" => $this->id,
            "nombre" => $this->nombre.', '.$nommun,
            "alias" => $this->alias,
            "monto" => $this->monto
        ];
    }
}