<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Cuadrilla extends Model
{
    public function initialize()
    {
        $this->setSchema("monitoreo");
    }

    public function getSource()
    {
        return "cuadrilla";
    }

}