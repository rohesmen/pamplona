<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Coordinador extends Model
{
    public function initialize()
    {
        $this->setSchema("monitoreo");
    }

    public function getSource()
    {
        return "coordinador";
    }

    public static function findByActivo() {
        $sql = "select id, nombres, apepat, apemat from monitoreo.coordinador where activo=true order by nombres asc";
        $supervisor = new Coordinador();

        return new Resultset(null, $supervisor, $supervisor->getReadConnection()->query($sql));
    }

}