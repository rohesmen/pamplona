<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Tipo_contratos extends  Model
{
    public function initialize()
    {
        $this->setSchema("cliente");
    }

    public function getSource()
    {
        return "tipo_contrato";
    }

    /**
     * [findByActivo obtiene los estatus_cliente que se encuentren activos]
     * @param  boolean $activo [description]
     * @return [type]          [description]
     */
    public static function findByActivo($activo = true) {
        $sql = "select id, nombre, descripcion from cliente.tipo_contrato 
        where activo = ".($activo?"true":"false")." order by nombre asc";
        $tipo_contratos = new Tipo_contratos();

        return new Resultset(null, $tipo_contratos, $tipo_contratos->getReadConnection()->query($sql));
    }

}