<?php

namespace Vokuro\Models;

use Phalcon\Di;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Vokuro\Mail\Mail;
use Vokuro\Util\Util;

class Estatus_cliente extends  Model {
    const CASA_HABITACION = "CH";
    const COMERCIO_SUPERVISOR = "CS";

    public function initialize()
    {
        $this->setSchema("cliente");
    }

    public function getSource()
    {
        return "estatus_cliente";
    }

    /**
     * [findByActivo obtiene los estatus_cliente que se encuentren activos]
     * @param  boolean $activo [description]
     * @return [type]          [description]
     */
    public static function findByActivo($activo = true) {
        $sql = "select id, nombre, iscomercio from cliente.estatus_cliente 
        where activo = ".($activo?"true":"false")." order by orden asc";
        $estatus_cliente = new Estatus_cliente();

        return new Resultset(null, $estatus_cliente, $estatus_cliente->getReadConnection()->query($sql));
    }

    public static function fullSave($idcliente, $idusuario, $origen, $data){
        $idEstatus = $data->idestatus;
        $motivo = $data->motivo;
        $nomcom = $data->nomcom;
        $response = new \stdClass();
        $response->error = false;

        $di = Di::getDefault();
        $config = $di->getConfig();
        $logger = $di->getLogger();
        $db = $di->getDb();

        $oCliente = Clientes::findFirstById_cliente($idcliente);
        $oldEstatus = Estatus_cliente::findFirst($oCliente->idestatuscliente);
        $oEstatus = Estatus_cliente::findFirst($idEstatus);
        $dataOrigin = json_encode($oCliente);
        $oUser = Users::findFirst($idusuario);

        $db->begin();
        $bit = new BitacoraEstatus();
        $bit->idcliente = $idcliente;
        $bit->idestatus = $idEstatus;
        $bit->idestatus_anterior = $oCliente->idestatuscliente;
        $bit->idusuario = $idusuario;
        $bit->nomcom = $nomcom;
        $bit->nomcom_anterior = $oCliente->nombre_comercio;
        $bit->motivo = $motivo;
        $bit->origen = $origen;

        if(!$bit->save()){
            $response->error = true;
            $response->errorCode = 500;
            $db->rollback();
            foreach ($bit->getMessages() as $message) {
                $logger->error("(save-bitacora-status): " . $message);
            }
            return $response;
        }

        $cNomCOm = $nomcom;
        $isComercio = true;
        if($oEstatus->sigla == Estatus_cliente::CASA_HABITACION){
            $cNomCOm = null;
            $isComercio = false;
        }

        $dtModificacion= date("c");
        $oCliente->comercio = $isComercio;
        $oCliente->nombre_comercio = $cNomCOm;
        $oCliente->idestatuscliente = $idEstatus;
        $oCliente->fecha_modificacion = $dtModificacion;

        if(!$oCliente->save()){
            $response->error = true;
            $response->errorCode = 500;
            $db->rollback();
            foreach ($oCliente->getMessages() as $message) {
                $logger->error("(save-bitacora-status-update-cliente): " . $message);
            }
            return $response;
        }

        $dataB = new BitacoraCambios();
        $dataB->identificador = $idcliente;
        $dataB->modulo = 'CLIENTE';
        $dataB->accion = 'CAMBIAR ESTATUS';
        $dataB->idusuario = $idusuario;
        $dataB->tabla = "cliente.cliente";
        $dataB->cambios = json_encode($oCliente);
        $dataB->original = $dataOrigin;
        $dataB->motivo = $motivo;
        $dataB->apartado = $origen;

        if (!$dataB->save()) {
            $response->error = true;
            $response->errorCode = 500;
            foreach ($dataB->getMessages() as $message) {
                $logger->info("(apiv2-cancelacion-pago-bitacora-pago): " . $message);
            }
            $db->rollback();
            return $response;
        }
        $oCliente->refresh();
        if($oldEstatus->sigla === Estatus_cliente::CASA_HABITACION && $oEstatus->sigla === Estatus_cliente::COMERCIO_SUPERVISOR){
            $mail = new Mail();
            $emailJuridico = $config->application->emailJuridico;
            $subject = 'CAMBIO CASA HABITACION A COMERCIO SUPERVISOR';
            $params = [
                "fecha" => Util::formatDateTimeForView($oCliente->fecha_modificacion),
                "cobratario" => $oUser->getFullName(),
                "iscomercio" => $oCliente->comercio,
                "nombre_comercio" => $oCliente->nombre_comercio,
                "direccion" => $oCliente->getFormatAddress(),
                "idcliente" => $idcliente,
                "mensualidad" => Util::formatToCurrency($oCliente->mensualidad),
                "motivo" => $motivo,
            ];
            $mail->send($emailJuridico, $subject, "modcliente", $params);
        }
        $db->commit();
        return $response;
    }
}