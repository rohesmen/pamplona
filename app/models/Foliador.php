<?php
/**
 * Created by PhpStorm.
 * User: Pauli
 * Date: 18/10/2016
 * Time: 10:03 PM
 */

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Foliador extends  Model
{

    public function initialize()
    {
        $this->setSchema("folios");

    }

    public function getSource()
    {
        return "foliador";
    }

    public static function getNextFolio($idusuario, $idcliente, $intent_number = 1) {
        $anio = date("Y");
        $maxFolio = Foliador::maximum(
            array(
                "column" => "folio",
                "conditions" => "anio = :anio: and activo = true", // It should be :id:, not :id in phalcon orm
                "bind" => array(
                    "anio" => $anio
                ),
            )
        );
        $nextFolio = empty($maxFolio) ? 1 : ($maxFolio + 1);

        try {
            $foliador = new Foliador();
            $foliador->folio = $nextFolio;
            $foliador->anio = $anio;
            $foliador->idusuario = $idusuario;
            $foliador->idcliente = $idcliente;
            $foliador->folio_anio = str_pad($nextFolio, 6, '0', STR_PAD_LEFT).$anio;
            if(!$foliador->save()){
                //if($intent_number < NUMBER_MAX_INTENTS_FOLIO){
                return Foliador::getNextFolio($idusuario, $idcliente, $intent_number + 1);
                /*} else {
                    return null;
                }*/
            }
        } catch(\Exception $e ) {
            return Foliador::getNextFolio($idusuario, $idcliente, $intent_number + 1);
        } catch(\PDOException $e) {
            return Foliador::getNextFolio($idusuario, $idcliente, $intent_number + 1);
        }

        return $foliador;
    }
}