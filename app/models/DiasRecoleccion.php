<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class DiasRecoleccion extends Model
{

    const LUNES = 'LUNES';
    const MARTES = 'MARTES';
    const MIERCOLES = 'MIERCOLES';
    const JUEVES = 'JUEVES';
    const VIERNES = 'VIERNES';
    const SABADO = 'SABADO';
    const DOMINGO = 'DOMINGO';
    
    public function initialize()
    {
        $this->setSchema("monitoreo");
    }

    public function getSource()
    {
        return "dias_recoleccion";
    }

}