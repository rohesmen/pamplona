<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class UserProfiles extends Model
{

    public function initialize()
    {
        $this->setSchema("usuario");
    }

    public function getSource()
    {
        return "perfil_usuario";
    }

    public static function findByIdUsuarioAndIdPerfilAndActivo($idusuario, $idperfil, $activo = true)
    {
        $sql = "SELECT * FROM usuario.perfil_usuario WHERE idusuario = ".$idusuario."
        and idperfil = ".$idperfil." and activo = ".($activo ? "true": "false");
        $userProfiles = new UserProfiles();

        return new Resultset(null, $userProfiles, $userProfiles->getReadConnection()->query($sql));
    }
}