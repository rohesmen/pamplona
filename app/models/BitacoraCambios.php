<?php


namespace Vokuro\Models;


use Phalcon\Mvc\Model;

class BitacoraCambios extends Model
{
    public function getSource()
    {
        return "bitacora";
    }

    public function initialize()
    {
        $this->setSchema("comun");
    }
}