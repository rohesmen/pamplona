<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class Tracking extends Model
{

    public function initialize()
    {
        $this->setSchema("tracking");
    }

    public function getSource()
    {
        return "tracking";
    }
}
