<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class CuadrillaRecolector extends Model
{
    public function initialize()
    {
        $this->setSchema("monitoreo");
        $this->hasOne(
            'idrecolector',
            Recolector::class,
            'id',
            [
                'reusable' => true,
                'alias'    => 'recolector'
            ]
        );
    }

    public function getSource()
    {
        return "cuadrilla_recolector";
    }

}