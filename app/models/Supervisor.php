<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Supervisor extends Model
{
    public function initialize()
    {
        $this->setSchema("monitoreo");
    }

    public function getSource()
    {
        return "supervisor";
    }

    public static function findByActivo() {
        $sql = "select id, nombres, apepat, apemat, color from monitoreo.supervisor where activo=true order by nombres asc";
        $supervisor = new Supervisor();

        return new Resultset(null, $supervisor, $supervisor->getReadConnection()->query($sql));
    }

}