<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Recolector extends Model
{
    public function initialize()
    {
        $this->setSchema("monitoreo");
    }

    public function getSource()
    {
        return "recolector";
    }

	public static function findByActivo() {
        $sql = "select id, nombres, apepat, apemat from monitoreo.recolector where activo=true order by nombres asc";
        $recolectores = new Recolector();

        return new Resultset(null, $recolectores, $recolectores->getReadConnection()->query($sql));
    }

    public function getFullName(){
        $nombre_completo = $this->nombres;

        if($this->apepat != null && trim($this->apepat) != ""){
            $nombre_completo .= ' '.$this->apepat;
        }

        if($this->apemat != null && trim($this->apemat) != ""){
            $nombre_completo .= ' '.$this->apemat;
        }

        return mb_strtoupper($nombre_completo);
    }

}