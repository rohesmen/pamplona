<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Multimedia extends  Model
{
    public function initialize()
    {
        $this->setSchema("cliente");
    }

    public function getSource()
    {
        return "multimedia";
    }

    public static function findByQuery($sql){
        $multimedia = new Multimedia();
        return new Resultset(null, $multimedia, $multimedia->getReadConnection()->query($sql));
    }
}