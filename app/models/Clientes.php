<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 * Vokuro\Models\Users
 * All the users registered in the application
 */

class Clientes extends  Model
{

    public function initialize()
    {
        $this->setSchema("cliente");
    }

    public function getSource()
    {
        return "cliente";
    }

    public static function findAll() {
        $sql = "select * from cliente.cliente order by id_cliente asc";
        $clientes = new Clientes();

        return new Resultset(null, $clientes, $clientes->getReadConnection()->query($sql));
    }

    /**
     * [validaDireccion Valida dirección del cliente para no repetir ]
     * @param  [type] $id_cliente   [se discrimina el identificador]
     * @param  [type] $calle        [description]
     * @param  [type] $calle_letra  [description]
     * @param  [type] $numero       [description]
     * @param  [type] $numero_letra [description]
     * @param  [type] $idcolonia    [description]
     */
    public static function validateDireccion($id_cliente, $folio_catastral, $calle, $calle_letra, $numero, $numero_letra, $idcolonia){

        $where = "";
        if($id_cliente != null){
            $where .= " AND id_cliente <> $id_cliente";
        }
        if($calle_letra != null){
            $where .= " AND upper(trim(regexp_replace(calle_letra, '\s', '', 'g'))) = upper(trim(regexp_replace('$calle_letra', '\s', '', 'g')))";
        }
        else{
            $where .= " AND calle_letra is null";
        }
        if($numero_letra != null){
            $where .= " AND upper(trim(regexp_replace(numero_letra, '\s', '', 'g'))) = upper(trim(regexp_replace('$numero_letra', '\s', '', 'g')))";
        }
        else{
            $where .= " AND numero_letra is null";
        }
        if($folio_catastral != null){
            $where .= " AND folio_catastral = $folio_catastral";
        }
        else{
            $where .= " AND folio_catastral is null";
        }

//        $sql="select c.id_cliente, c.calle, c.calle_letra, c.numero, c.numero_letra, c.idcolonia, co.nombre, co.alias, co.localidad ";
//        $sql.="from cliente.cliente c ";
//        $sql.="left join cliente.colonia co on co.id = c.idcolonia ";
//        $sql.="where c.id_cliente<>'".$id_cliente."' and c.calle='".$calle."' and upper(c.calle_letra)='".$calle_letra."' ";
//        $sql.="and c.numero='".$numero."' and upper(c.numero_letra)='".$numero_letra."' and c.idcolonia='".$idcolonia."' ";
//        $sql.="limit 1";

        $sql = "SELECT * FROM cliente.cliente 
          WHERE calle = '$calle' AND numero = '$numero' AND idcolonia = $idcolonia and activo = true $where limit 1";

//        echo $sql;
//        die;
        $clientes = new Clientes();
        return new Resultset(null, $clientes, $clientes->getReadConnection()->query($sql));
    }

    public static function findByDireccion($direccion, $calle, $letra, $numero, $letran, $colonia)
    {

//        $filter = new \Phalcon\Filter();
        $sqlWhere = "where 1=1 ";


        if($direccion != "")
            $sqlWhere .= " AND lower(coalesce(direccion,'')) = '" .$direccion."'";

        if($calle != "")
            $sqlWhere .= " AND calle = " .$calle;

        if($letra != "")
            $sqlWhere .= " AND lower(coalesce(calle_letra,'')) = '" .$letra."'";

        if($numero != "")
            $sqlWhere .= " AND numero = " .$numero;

        if($letran != "")
            $sqlWhere .= " AND lower(coalesce(numero_letra,'')) = '" .$letran."'";

        if($colonia != "")
            $sqlWhere .= " AND lower(coalesce(colonia,'')) = '" .$colonia."'";

        $sqlWhere .= " ";

        $sql = "select * from cliente.cliente ".$sqlWhere." order by nombres asc";
        $clientes = new Clientes();

        return new Resultset(null, $clientes, $clientes->getReadConnection()->query($sql));
    }

    public static function findByQuery($sql){
        $clientes = new Clientes();
        return new Resultset(null, $clientes, $clientes->getReadConnection()->query($sql));
    }

    /**
     * [validaDireccion Valida dirección del cliente para no repetir ]
     * @param  [type] $id_cliente   [se discrimina el identificador]
     * @param  [type] $calle        [description]
     * @param  [type] $calle_letra  [description]
     * @param  [type] $numero       [description]
     * @param  [type] $numero_letra [description]
     * @param  [type] $idcolonia    [description]
     */
    public static function validateDireccionWithMuni($id_cliente, $idmunicipio, $folio_catastral, $calle, $calle_letra,
    $numero, $numero_letra, $idcolonia){

        $where = "";
        if($id_cliente != null){
            $where .= " AND id_cliente <> $id_cliente";
        }

        if($calle_letra != null){
            $where .= " AND upper(trim(regexp_replace(calle_letra, '\s', '', 'g'))) = upper(trim(regexp_replace('$calle_letra', '\s', '', 'g')))";
        }
        else{
            $where .= " AND calle_letra is null";
        }
        if($numero_letra != null){
            $where .= " AND upper(trim(regexp_replace(numero_letra, '\s', '', 'g'))) = upper(trim(regexp_replace('$numero_letra', '\s', '', 'g')))";
        }
        else{
            $where .= " AND numero_letra is null";
        }
        /*if($folio_catastral != null){
            $where .= " AND folio_catastral = $folio_catastral";
        }
        else{
            $where .= " AND folio_catastral is null";
        }*/

//        $sql="select c.id_cliente, c.calle, c.calle_letra, c.numero, c.numero_letra, c.idcolonia, co.nombre, co.alias, co.localidad ";
//        $sql.="from cliente.cliente c ";
//        $sql.="left join cliente.colonia co on co.id = c.idcolonia ";
//        $sql.="where c.id_cliente<>'".$id_cliente."' and c.calle='".$calle."' and upper(c.calle_letra)='".$calle_letra."' ";
//        $sql.="and c.numero='".$numero."' and upper(c.numero_letra)='".$numero_letra."' and c.idcolonia='".$idcolonia."' ";
//        $sql.="limit 1";

        $sql = "SELECT * FROM cliente.cliente 
          WHERE idmunicipio = $idmunicipio and calle = '$calle' AND numero = '$numero' AND idcolonia = $idcolonia and activo = true $where limit 1";

//        echo $sql;
//        die;
        $clientes = new Clientes();
        return new Resultset(null, $clientes, $clientes->getReadConnection()->query($sql));
    }


    //-------------------------------------------------------------------------------------
	public static function updateByQuery($sqlquery){		
		$oCliente = new Clientes();
		$result = $oCliente->getReadConnection()->execute($sqlquery);
		return $result;
	}//fin:updateByQuery
	//-------------------------------------------------------------------------------------

    public function getFormatAddress(){
        $direccion = "Calle ".$this->calle." ";

        if($this->calle_letra != null && $this->calle_letra != ""){
            $direccion .= "- ".$this->calle_letra." ";
        }

        $direccion .= "# ".$this->numero." ";

        if($this->numero_letra != null && $this->numero_letra != ""){
            $direccion .= "- ".$this->numero_letra." ";
        }

        if($this->idcolonia != null && $this->idcolonia != ""){
            $colonias = Colonias::findFirstById($this->idcolonia);
            if($colonias){
                $direccion .= "Col. ".$colonias->nombre;
            }
        }
        return $direccion;
    }

    public function getFUllName(){
        $nombre = $this->nombres." ".$this->apepat." ".$this->apemat;
        if(trim($nombre)){
            $nombre = $nombre;
        } else{
            $nombre = 'Sin Nombre Registrado';
        }
        return $nombre;
    }
}