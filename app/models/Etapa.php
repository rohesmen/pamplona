<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Etapa extends  Model
{
    public function initialize()
    {
        $this->setSchema("queja");
    }

    public function getSource()
    {
        return "etapa";
    }

    public static function getEtapasSiguientes($idEtapaPadre)
    {

//        $filter = new \Phalcon\Filter();
        $sqlWhere = "where 1=1 ";


        if($idEtapaPadre != "")
            $sqlWhere .= " AND id_etapa_padre = " .$idEtapaPadre;


        $sql = "select * from queja.etapa ".$sqlWhere." order by orden asc";
        $result = new Etapa();

        return new Resultset(null, $result, $result->getReadConnection()->query($sql));
    }

}