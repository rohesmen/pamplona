<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Device extends Model
{

    public function initialize()
    {
        $this->setSchema("usuario");
    }

    public function getSource()
    {
        return "dispositivo";
    }

    public static function get($id, $aplicacion){
        $app = Application::findFirst("llave = '$aplicacion' and activo = true");
        if($app !== false){
            if(!$app->valida_dispositivo){
                $o = new \stdClass();
                $o->permitido = true;
                $o->fecha_modificacion = date('c');
                return $o;
            }

            $sql = "select d.* from usuario.dispositivo d left join usuario.aplicacion a on(d.aplicacion=a.llave) where d.id = '$id' and a.llave = '$aplicacion'";
            $device = new Device();

            $a = $device->getReadConnection()->query($sql);

            if($a->numRows() == 0){
                $d = new Device();
                $d->id = $id;
                $d->aplicacion = $aplicacion;
                $d->save();
                $d->refresh();

                return $d;
            }else{
                $rs = new Resultset(null,$device, $a);
                return $rs->getFirst();
            }
        }else{
            $o = new \stdClass();
            $o->permitido = false;
            $o->fecha_modificacion = date('c');
            return $o;
        }
    }

    public function jsonSerialize() {
        return [ "permitido" => $this->permitido,
            "fechaModificacion" => $this->fecha_modificacion];
    }
}
