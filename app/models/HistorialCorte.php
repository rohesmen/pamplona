<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 * Vokuro\Models\Users
 * All the users registered in the application
 */

class HistorialCorte extends  Model{

    public function initialize(){
        $this->setSchema("corte");

    }
	//-----------------------------------------------------
    public function getSource()
    {
        return "historial_corte";
    }
	//-----------------------------------------------------
    public static function findByQuery($sql){
        $result = new HistorialCorte();
        return new Resultset(null, $result, $result->getReadConnection()->query($sql));
    }
	//-----------------------------------------------------
	 public static function findByidHistorial($idhistoral){
        
		$sqlQuery = " select hc.* from corte.historial_corte hc 
			join corte.historial_corte_historial_pago hchp
			on  hc.id = hchp.idcorte
			where hchp.activo = true and hchp.activo = true
			and hchp.idhistorial_pago = " . $idhistoral . " order by hc.id desc";
		$result = new HistorialCorte();
        return new Resultset(null, $result, $result->getReadConnection()->query($sqlQuery));		
    }//fin:findByidHistorial
	//-----------------------------------------------------
}
?>