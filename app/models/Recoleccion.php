<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Recoleccion extends  Model
{
    public function initialize()
    {
        $this->setSchema("monitoreo");
    }

    public function getSource()
    {
        return "recoleccion";
    }

    public static function findLastRecolection($placa = null, $idruta = null) {
        $wPlaca = $placa ? " AND r.placa = '$placa'" : '';
        $wRuta = $idruta ? " AND r.idruta = $idruta" : '';
        $sql = "SELECT r.id, r.placa, r.idruta, r.dia, r.idcuadrilla, r.idturno
        FROM monitoreo.recoleccion r
        LEFT JOIN monitoreo.dias_recoleccion dr ON dr.sigla = r.dia
        WHERE r.activo ".$wPlaca . $wRuta." AND (EXTRACT(DOW FROM NOW()) = dr.dia_bd)
        ORDER BY r.fechacreacion DESC
        LIMIT 1";
        $recoleccion = new Recoleccion();
        return new Resultset(null, $recoleccion, $recoleccion->getReadConnection()->query($sql));
    }

    public function jsonSerialize() {
        return [ "id" => $this->id,
            "idruta" => $this->idruta,
            "dia" => $this->dia,
            "idturno" => $this->idturno,
            "idcuadrilla" => $this->idcuadrilla,
            "placa" => $this->placa,
        ];
    }
}