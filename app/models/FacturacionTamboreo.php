<?php
/**
 * Created by PhpStorm.
 * User: Pauli
 * Date: 18/10/2016
 * Time: 10:03 PM
 */

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;


class FacturacionTamboreo extends  Model
{

    public function initialize()
    {
        $this->setSchema("cliente");

    }

    public function getSource()
    {
        return "facturacion_tamboreo";
    }

    public function disableByNombre($nombre)
    {
        $di = \Phalcon\DI::getDefault();
        $query = "UPDATE cliente.facturacion_tamboreo SET activo = false WHERE nombre = '.$nombre.'";
        $query = new \Phalcon\Mvc\Model\Query($query, $di);
        return $query->execute();
    }
}