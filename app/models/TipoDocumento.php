<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class TipoDocumento extends Model
{

    public function initialize()
    {
        $this->setSchema("comun");
    }

    public function getSource()
    {
        return "tipo_documento";
    }

    public static function findByActivo($activo = true) {
        $sql = "select * from comun.tipo_documento 
        where activo = ".($activo?"true":"false")." order by tipo_documento asc";
        $documentos = new TipoDocumento();

        return new Resultset(null, $documentos, $documentos->getReadConnection()->query($sql));
    }

    public static function findByComprobante($comprobante = true) {
        $sql = "select * from comun.tipo_documento 
        where lcomprobante = ".($comprobante?"true":"false")." 
        and activo = true order by tipo_documento asc";
        $documentos = new TipoDocumento();

        return new Resultset(null, $documentos, $documentos->getReadConnection()->query($sql));
    }
}
