<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class GroupModule extends Model
{
    public function initialize()
    {
        $this->setSchema("usuario");
    }

    public function getSource()
    {
        return "grupomodulo";
    }

    public static function findByIdUsuario($idusuario){
        $sql   = "select * from usuario.grupomodulo as gm
        where gm.id in(
            select distinct(idgrupomodulo) from usuario.modulo
            where id in (
                select distinct(p.idmodulo) from usuario.permiso p
                join usuario.perfil_permiso pp on pp.idpermiso = p.id
                join usuario.perfil_usuario pu on pu.idperfil = pp.idperfil
                where pu.idusuario = ".$idusuario." and p.accion = 'index'
                and pp.activo = true
                and pu.activo = true
                and p.activo = true
                and idmodulo is not null
                ) and activo = true
            )
        and activo = true order by gm.orden asc";
        $groupModule = new GroupModule();

        return new Resultset(null, $groupModule, $groupModule->getReadConnection()->query($sql));
    }
}