<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Turno extends Model
{
    public function initialize()
    {
        $this->setSchema("monitoreo");
    }

    public function getSource()
    {
        return "turno";
    }

	public static function findByActivo() {
        $sql = "select id, nombre from monitoreo.turno where activo=true order by nombre asc";
        $turnos = new Turno();

        return new Resultset(null, $turnos, $turnos->getReadConnection()->query($sql));
    }
}