<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
/**
 * Permissions
 * Stores the permissions by profile
 */
class Permissions extends Model
{


    public function initialize()
    {
        $this->setSchema("usuario");
        $this->hasMany("id", "ProfilesPermissions", "idpermiso");
    }

    public function getSource()
    {
        return "permiso";
    }

    public static function findByProfile($id)
    {
        $sql   = "select p.* from usuario.perfil_permiso pp left join usuario.permiso p on (pp.idpermiso = p.id) where pp.activo = true and pp.idperfil = ".$id." and p.activo";
        $permissions = new Permissions();

        return new Resultset(null, $permissions, $permissions->getReadConnection()->query($sql));
    }

    public static function findDistinctByRecursoAndAccion()
    {
        $sql   = "select DISTINCT ON(recurso,accion) * from usuario.permiso where activo = true";
        $permissions = new Permissions();

        return new Resultset(null, $permissions, $permissions->getReadConnection()->query($sql));
    }
}
