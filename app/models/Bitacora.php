<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Bitacora extends Model
{
    public function initialize()
    {
        $this->setSchema("monitoreo");
    }

    public function getSource()
    {
        return "bitacora";
    }
	
	public static function findByDate($date) {
        $sql = "select * from monitoreo.bitacora where fecha='$date' and activo=true order by id asc";
        $bitacora = new Bitacora();

        return new Resultset(null, $bitacora, $bitacora->getReadConnection()->query($sql));
    }

    public static function findByDateFilter($date, $type, $idType) {
        if($type != "todos"){
            $sql = "select * from monitoreo.bitacora where fecha='$date' and $type=$idType and activo=true order by idsupervisor, idunidad asc";
        }else{
            $sql = "select * from monitoreo.bitacora where fecha='$date' and activo=true order by idsupervisor asc";
        }

        $bitacora = new Bitacora();

        return new Resultset(null, $bitacora, $bitacora->getReadConnection()->query($sql));
    }

    public static function findByDates($date, $date2) {
        $sql = "select * from monitoreo.bitacora where fecha BETWEEN '$date' and '$date2' and activo=true order by idsupervisor, idunidad asc";
        $bitacora = new Bitacora();

        return new Resultset(null, $bitacora, $bitacora->getReadConnection()->query($sql));
    }


}