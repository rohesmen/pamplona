<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class TipoRecoleccion extends Model
{

    const ORGANICA = 'ORGANICA';
    const INORGANICA = 'INORGANICA';
    
    public function initialize()
    {
        $this->setSchema("monitoreo");
    }

    public function getSource()
    {
        return "tipo_recoleccion";
    }

}