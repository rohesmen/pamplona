<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Unidad extends Model
{
    public function initialize()
    {
        $this->setSchema("monitoreo");
    }

    public function getSource()
    {
        return "unidad";
    }

	public static function findByActivo() {
        $sql = "select id, nombre from monitoreo.unidad where activo=true order by id asc";
        $unidades = new Unidad();

        return new Resultset(null, $unidades, $unidades->getReadConnection()->query($sql));
    }

}