<?php
/**
 * Created by PhpStorm.
 * User: Pauli
 * Date: 18/10/2016
 * Time: 10:03 PM
 */

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Cliente_Descuento_Pensionado extends  Model
{

    public function initialize()
    {
        $this->setSchema("cliente");

    }

    public function getSource()
    {
        return "cliente_descuento_pensionado";
    }
}