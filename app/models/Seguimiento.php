<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Seguimiento extends  Model
{

    public function initialize()
    {
        $this->setSchema("queja");

    }

    public function getSource()
    {
        return "seguimiento";
    }
}