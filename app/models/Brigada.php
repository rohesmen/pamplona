<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class Brigada extends Model
{

    public function initialize()
    {
        $this->setSchema("servicio");
    }

    public function getSource()
    {
        return "brigadas";
    }

//    public function jsonSerialize() {
//        $tu = TipoUnidad::findFirstById($this->idtipo_unidad);
//        return [ "id" => $this->id,
//            "nombre" => $this->nombre,
//            "descripcion" => $this->alias,
//            "imagen" => $this->imagen,
//            "horas_estimadas"  => $this->horas_estimadas,
//            "horas_tolereancia" => $this->horas_tolereancia,
//            "unidad" => $tu->nombre,
//            "precioxunidad" => $tu->precio
//        ];
//    }
}
