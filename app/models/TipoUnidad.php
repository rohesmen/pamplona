<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class TipoUnidad extends Model
{

    public function initialize()
    {
        $this->setSchema("servicio");
    }

    public function getSource()
    {
        return "tipo_unidad";
    }

    public function jsonSerialize() {
        return [ "id" => $this->id,
            "nombre" => $this->nombre,
            "descripcion" => $this->alias,
            "imagen" => $this->monto
        ];
    }
}
