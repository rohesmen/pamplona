<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 * Vokuro\Models\Users
 * All the users registered in the application
 */

class Corte extends  Model
{

    public function initialize()
    {
        $this->setSchema("corte");
    }

    public function getSource()
    {
        return "historial_corte";
    }

    public function getMontoCorte($fI,$fF,$cP,$cC)
    {   
        $corte = new Corte();
        $results= array();

        $listPagos = '';
        $cantidad = 0;
//        if($cP != 3){
            $cP = $cP==2?TRUE:FALSE;
            $sql = "select id, cantidad  from 
                    cliente.cliente c LEFT JOIN cliente.historial_pago p ON c.id_cliente = p.idcliente 
                    WHERE 
                    --c.fisica = ".($cP==true?'TRUE':'FALSE')." AND 
                    p.fecha_Pago::Date BETWEEN '".$fI."' AND '".$fF."' AND idusuario = $cC 
                    --and p.id not in (select idhistorial_pago from corte.historial_corte_historial_pago where activo = true)
                    and p.idcorte is null and p.idpago > 0
                    AND p.activo = TRUE 
                    --AND c.activo = TRUE 
                    ORDER BY id";
            
            $results = new Resultset(null, $corte, $corte->getReadConnection()->query($sql));
            if(count($results) > 0){
                foreach($results as $res) {
                    $listPagos .= ($listPagos!=''?',':'').$res->id;
                    $cantidad += $res->cantidad; 
                }
            }
//        }else{
//            $sql = "select id, cantidad, idcorte from
//                    cliente.cliente c LEFT JOIN cliente.historial_pago p ON c.id_cliente = p.idcliente  LEFT JOIN corte.historial_corte_historial_pago chp ON chp.idhistorial_pago = p.id AND chp.activo = TRUE
//                    WHERE p.fecha_Pago BETWEEN '".$fI." 00:00:00' AND '".$fF." 23:59:59' AND idusuario = $cC
//                    and p.id not in (select idhistorial_pago from corte.historial_corte_historial_pago where activo = true)
//                    AND p.activo = TRUE AND c.activo = TRUE ORDER BY id";
//            $results = new Resultset(null, $corte, $corte->getReadConnection()->query($sql));
//            if(count($results) > 0){
//                foreach($results as $res) {
//                    if($res->idcorte == ""){
//                        $listPagos .= ($listPagos!=''?',':'').$res->id;
//                        $cantidad += $res->cantidad;
//                    }
//                }
//            }
//        }
   
        return(array("cantidad"=>$cantidad, "pagos"=>$listPagos));
        //return new Resultset(null, $corte, $corte->getReadConnection()->query($sql));
    }

    public static function findByQuery($sql){
        $corte = new Corte();
        return new Resultset(null, $corte, $corte->getReadConnection()->query($sql));
    }
}