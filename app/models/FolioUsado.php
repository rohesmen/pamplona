<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class FolioUsado extends  Model
{
    public function initialize()
    {
        $this->setSchema("folios");
    }

    public function getSource()
    {
        return "folios_usados";
    }
}