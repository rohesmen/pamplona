<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;

class Parameters extends Model
{

    public function initialize()
    {
        $this->setSchema("comun");
    }

    public function getSource()
    {
        return "parametro";
    }

}
