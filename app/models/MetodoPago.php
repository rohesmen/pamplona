<?php
/**
 * Created by PhpStorm.
 * User: Pauli
 * Date: 18/10/2016
 * Time: 10:03 PM
 */

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class MetodoPago extends  Model
{

    public function initialize()
    {
        $this->setSchema("cliente");

    }

    public function getSource()
    {
        return "metodo_pago";
    }

    public static function findByActivo($activo = true){
        $sql   = "select * from cliente.metodo_pago where activo = ".($activo ? "true" : "false")." order by orden asc";
        $metodoPago = new MetodoPago();

        return new Resultset(null, $metodoPago, $metodoPago->getReadConnection()->query($sql));
    }
}