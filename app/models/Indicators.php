<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 18/11/15
 * Time: 10:33 AM
 */

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Indicators extends Model
{
    public function initialize()
    {
        $this->setSchema("comun");
    }

    public function getSource()
    {
        return "indicadores_calculados";
    }

    public static function findByUser($id)
    {
        $sql   = "select * from comun.calcular_indicadores($id) as (id integer, nombre text, descripcion text, query text, activo boolean, fecha_creacion timestamp without time zone, fecha_modificacion timestamp without time zone, avance numeric)";
        $indicators = new Indicators();

        return new Resultset(null, $indicators, $indicators->getReadConnection()->query($sql));
    }
}