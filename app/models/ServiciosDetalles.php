<?php


namespace Vokuro\Models;

use Phalcon\Mvc\Model;

class ServiciosDetalles extends Model
{
    public function initialize()
    {
        $this->setSchema("servicio");
    }

    public function getSource()
    {
        return "view_serviciosdetalles";
    }

}