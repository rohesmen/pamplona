<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class OrdenServicioDetalle extends Model
{

    public function initialize()
    {
        $this->setSchema("servicio");
    }

    public function getSource()
    {
        return "orden_servicio_detalle";
    }

    public function jsonSerialize() {

        $ser = Servicios::findFirstById($this->idservicio);
        
        return [ "id" => $this->id,
        	"idorden_servicio" => $this->idorden_servicio,
            "idservicio" => $this->idservicio,
            "servicio" => $ser->nombre,
            "cantidad"  => $this->cantidad,
            "costo" => $this->costo_final
        ];
    }

}
