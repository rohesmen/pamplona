<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Tipo_servicio extends  Model{
	
    public function initialize(){
        $this->setSchema("cliente");
    }//fin:
	//-------------------------------------------------------------------------
    public function getSource(){
        return "tipo_servicio";
    }//fin:
	//-------------------------------------------------------------------------
    /**
     * [findByActivo obtiene los tipo_servicio que se encuentren activos]
     * @param  boolean $activo [description]
     * @return [type]          [description]
     */
    public static function findByActivo($activo = true){
        $sql = "select id, nombre, descripcion from cliente.Tipo_servicio 
        where activo = ".($activo? "true" : "false")." order by nombre asc";
        $otiposervicio = new Tipo_servicio();

        return new Resultset(null, $otiposervicio, $otiposervicio->getReadConnection()->query($sql));
    }//fin:findByActivo

}//fin:class
?>