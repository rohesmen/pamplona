<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Vokuro\Models\Layers;

class LayersGroup extends Model
{

    public function initialize()
    {
        $this->setSchema("comun");
    }

    public function getSource()
    {
        return "grupo_capas";
    }
	
	public static function findAll(){
        $sql = "select * from comun.grupo_capas gc where activo = true order by orden";
        $layersGroup = new LayersGroup();
        return new Resultset(null, $layersGroup, $layersGroup->getReadConnection()->query($sql));
    }
}
