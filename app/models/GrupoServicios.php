<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class GrupoServicios extends Model
{

    public function initialize()
    {
        $this->setSchema("servicio");
    }

    public function getSource()
    {
        return "grupo_servicio";
    }

    public function jsonSerialize() {
        return [ "id" => $this->id,
            "nombre" => $this->nombre,
            "imagen" => $this->imagen
        ];
    }
}
