<?php
namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class Reports extends Model
{

    public function initialize()
    {
        $this->setSchema("comun");
    }

    public function getSource()
    {
        return "reporte";
    }

    public static function getDatosQueryById($id){
        $sql = "select *, (select comun.query_reporte(query)) json from comun.reporte where  id = ".$id." and activo = true";
        $reports = new Reports();

        return new Resultset(null, $reports, $reports->getReadConnection()->query($sql));
    }

    public static function getDatosQueryByIdAndParams($id, $d, $p){

        $sql = "select *, (select comun.query_reporte(query) json from comun.reporte where  id = ".$id." and activo = true";
        $reports = new Reports();

        return new Resultset(null, $reports, $reports->getReadConnection()->query($sql));
    }
}
