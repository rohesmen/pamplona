<?php
/**
 * Created by PhpStorm.
 * User: Pauli
 * Date: 18/10/2016
 * Time: 10:03 PM
 */

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Cobratario extends  Model
{

    public function initialize()
    {
        $this->setSchema("perfil_permiso");

    }

    public function getSource()
    {
        return "colonia";
    }

    public static function findByActivo($activo = true) {
        //$sql = "SELECT u.id, u.apellido_materno, u.apellido_paterno, u.nombre FROM usuario.perfil p LEFT JOIN usuario.perfil_usuario up ON (p.id = up.idperfil) LEFT JOIN 
         //usuario.usuario u ON (up.idusuario = u.id) where u.activo = true AND p.activo = true AND p.nombre = 'administrador' order by usuario asc";
        $sql = "SELECT u.id, u.apellido_materno, u.apellido_paterno, u.nombre FROM usuario.usuario u where u.activo = true order by usuario asc";

        $cobratario = new Cobratario();

        return new Resultset(null, $cobratario, $cobratario->getReadConnection()->query($sql));
    }

    public function findByColonia($id) {
        $sql = "select * from cliente.colonia where id = ".$id;
        $colonias = new Colonias();
        return new Resultset(null, $colonias, $colonias->getReadConnection()->query($sql));
    }
}