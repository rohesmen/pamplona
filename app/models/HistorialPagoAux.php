<?php

namespace Vokuro\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class HistorialPagoAux extends  Model
{

    public function initialize()
    {
        $this->setSchema("transaccion");

    }

    public function getSource()
    {
        return "historial_pago";
    }



    public static function findByQuery($sql){
        $result = new HistorialPago();
        return new Resultset(null, $result, $result->getReadConnection()->query($sql));
    }
	//----------------------------------------------------------------------
	public static function updateByQuery($sqlquery){
		$oHistorial = new HistorialPago();
		$result = $oHistorial->getReadConnection()->execute($sqlquery);
		return $result;
	}//fin:updateByQuery
	//----------------------------------------------------------------------
}