<?php
namespace Vokuro\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Vokuro\Models\LayersGroup;

class LayersForm extends Form{

	public function initialize($entity = null, $options = null){
		// In edition the id is hidden
		if (isset($options['edit']) && $options['edit']) {
			$id = new Hidden('id');
		} else {
			$id = new Text('id');
		}
		$this->add($id);

		//CLAVE
		$clave = new Text('txtClave', [
			'class' =>'form-control',
			'disabled' => 'disabled'
		]);
		$this->add($clave);

		//NOMBRE
		$nombre = new Text('txtNombre', [
			'class' =>'form-control save-required',
			'placeholder' => 'Nombre'
		]);
		$nombre->addValidators([
			new PresenceOf([
				'message' => 'El campo nombre es obligatorio.'
			])
		]);
		$this->add($nombre);
		
		//Descripcion
		$descripcion = new Text('txtDescripcion', [
			'class' =>'form-control',
			'placeholder' => 'Descripcion'
		]);
		$this->add($descripcion);
		
		//Capa
		$capa = new Text('txtCapa', [
			'class' =>'form-control',
			'placeholder' => 'Capa'
		]);
		$this->add($capa);
		
		$estilo = new Text('txtEstilo', [
			'class' =>'form-control',
			'placeholder' => 'Estilo'
		]);
		$this->add($estilo);
		
		$estilo_etiqueta = new Text('txtEstiloEtiqueta', [
			'class' =>'form-control',
			'placeholder' => 'Estilo Etiqueta'
		]);
		$this->add($estilo_etiqueta);
		
		$orden = new Text('txtOrden', [
			'class' =>'form-control save-required',
			'placeholder' => 'Orden'
		]);
		
		$orden->addValidators([
			new PresenceOf([
				'message' => 'El campo orden es obligatorio'
			])
		]);
		
		$this->add($orden);
		
		$esquema = new Text('txtEsquema', [
			'class' =>'form-control',
			'placeholder' => 'Esquema'
		]);
		
		$this->add($esquema);
		
		$tabla = new Text('txtTabla', [
			'class' =>'form-control',
			'placeholder' => 'Tabla'
		]);
		
		$this->add($tabla);
		
		$grupos_capa = LayersGroup::find([
			'activo = TRUE'
		]);
		
		$idcapa_value = "";
		if (isset($options['edit']) && $options['edit']) {
			$idcapa_value = $entity->idgrupo_capa;
		}
		
		$grupo = new Select('slcGrupos', $grupos_capa, [
			'using' => [
				'id',
				'nombre'
			],
			'useEmpty' => true,
			'emptyText' => 'Selecciona una opción',
			'emptyValue' => '',
			'value' => $idcapa_value,
			'class' =>'slcSelect form-control save-required'
		]);
		
		$grupo->addValidators([
			new PresenceOf([
				'message' => 'La opción Grupo es obligatoria.'
			])
		]);
		
		$this->add($grupo);
	}
}
