<?php
namespace Vokuro\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Vokuro\Models\LayersGroup;

class GroupsForm extends Form{

	public function initialize($entity = null, $options = null){
		if (isset($options['edit']) && $options['edit']) {
			$id = new Hidden('id');
		} else {
			$id = new Text('id');
		}
		$this->add($id);

		//CLAVE
		$clave = new Text('txtClave', [
			'class' =>'form-control',
			'disabled' => 'disabled'
		]);
		$this->add($clave);

		//NOMBRE
		$nombre = new Text('txtNombre', [
			'class' =>'form-control save-required',
			'placeholder' => 'Nombre'
		]);
		$nombre->addValidators([
			new PresenceOf([
				'message' => 'El campo nombre es obligatorio.'
			])
		]);
		$this->add($nombre);
		
		//Descripcion
		$descripcion = new Text('txtDescripcion', [
			'class' =>'form-control',
			'placeholder' => 'Descripcion'
		]);
		$this->add($descripcion);
		
		$orden = new Text('txtOrden', [
			'class' =>'form-control save-required',
			'placeholder' => 'Orden'
		]);
		
		$orden->addValidators([
			new PresenceOf([
				'message' => 'El campo orden es obligatorio'
			])
		]);
		
		$this->add($orden);
	}
}
