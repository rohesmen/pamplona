<?php
namespace Vokuro\Acl;

use Phalcon\Mvc\User\Component;
use Phalcon\Acl\Adapter\Memory as AclMemory;
use Phalcon\Acl\Role as AclRole;
use Phalcon\Acl\Resource as AclResource;
use Vokuro\Models\Profiles;
use Vokuro\Models\Permissions;
use Vokuro\Models\ProfilesPermissions;

/**
 * Vokuro\Acl\Acl
 */
class Acl extends Component
{

    /**
     * The ACL Object
     *
     * @var \Phalcon\Acl\Adapter\Memory
     */
    private $acl;

    /**
     * The filepath of the ACL cache file from APP_DIR
     *
     * @var string
     */
    private $filePath = '/cache/acl/data.txt';

    /**
     * Define the resources that are considered "private". These controller => actions require authentication.
     *
     * @var array
     */
    private $publicResources = array(
        'session' => array(
            'index',
            'login',
            'logout'
        ),
        'error' => array(
            'index'
        ),
        'seguimiento' => array(
            'index',
            'lastlocation'
        ),
        'api' =>array(
            'clientesdescargarrecibo',
            'syncembsoft',
            'clientesdescargarrecibov2'
        ),
        'apicob' =>array(
            'login',
            'ubicacion'
        ),
        'clientes' => array(
            'antiguos'
        ),
        'apiv2' =>array(
            'sesiones',
            'municipios'
        ),
    );

    private $sesionResources = array(
        'clientes' => array(
            'bitmens',
            'segdescpen',
            'clichgmens'
        ),
        'cuadrillaseguimiento' => array(
            'get',
            'save'
        )
    );

    /*private $privateResources = array(
        'users' => array(
            'index',
            'search',
            'edit',
            'create',
            'delete',
            'changePassword'
        ),
        'profiles' => array(
            'index',
            'search',
            'edit',
            'create',
            'delete'
        ),
        'permissions' => array(
            'index'
        ),
        'index' => array(
            'index'
        )
    );*/

    /**
     * Human-readable descriptions of the actions used in {@see $privateResources}
     *
     * @var array
     */
    /*private $actionDescriptions = array(
        'index' => 'Access',
        'search' => 'Search',
        'create' => 'Create',
        'edit' => 'Edit',
        'delete' => 'Delete',
        'changePassword' => 'Change password'
    );*/

    /**
     * Checks if a controller is private or not
     *
     * @param string $controllerName
     * @return boolean
     */
    public function isPrivate($controllerName)
    {
        $controllerName = strtolower($controllerName);
        return !isset($this->publicResources[$controllerName]);
    }

    /**
     * Checks if the current profile is allowed to access a resource
     *
     * @param string $profile
     * @param string $controller
     * @param string $action
     * @return boolean
     */
    public function isAllowedUser($controller, $action)
    {
        $identity = $this->auth->getIdentity();
        if(!$identity){
            return false;
        }
        foreach ($identity['profiles'] as $profile) {
            if($this->isAllowed($profile->nombre,$controller, $action)){
                return true;
            }
        }
        return false;
    }

    public function isAllowedUserNosesion($controller, $action, $profiles) {
        foreach ($profiles as $profile) {
            if($this->isAllowed($profile->nombre,$controller, $action)){
                return true;
            }
        }
        return false;
    }

    public function isAllowed($profile, $controller, $action)
    {
        return $this->getAcl()->isAllowed($profile, $controller, $action);
    }

    /**
     * Returns the ACL list
     *
     * @return Phalcon\Acl\Adapter\Memory
     */
    public function getAcl()
    {
        // Check if the ACL is already created
        if (is_object($this->acl)) {
            return $this->acl;
        }

        // Check if the ACL is in APC
        /*if (function_exists('apc_fetch')) {
            $acl = apc_fetch('vokuro-acl');
            if (is_object($acl)) {
                $this->acl = $acl;
                return $acl;
            }
        }*/

        // Check if the ACL is already generated
        //if (!file_exists(APP_DIR . $this->filePath)) {
            $this->acl = $this->rebuild();
            return $this->acl;
        //}

        // Get the ACL from the data file
        //$data = file_get_contents(APP_DIR . $this->filePath);
        //$this->acl = unserialize($data);

        // Store the ACL in APC
        /*if (function_exists('apc_store')) {
            apc_store('vokuro-acl', $this->acl);
        }*/

        return $this->acl;
    }

    /**
     * Returns the permissions assigned to a profile
     *
     * @param Profiles $profile
     * @return array
     */
    public function getPermissions(Profiles $profile)
    {
        $permissions = array();
        foreach ($profile->getProfilesPermissions() as $permission) {
            $permissions[$permission->resource . '.' . $permission->action] = true;
        }
        return $permissions;
    }

    /**
     * Returns all the resoruces and their actions available in the application
     *
     * @return array
     */
    public function getResources()
    {
        return $this->privateResources;
    }

    /**
     * Returns the action description according to its simplified name
     *
     * @param string $action
     * @return $action
     */
    public function getActionDescription($action)
    {
        if (isset($this->actionDescriptions[$action])) {
            return $this->actionDescriptions[$action];
        } else {
            return $action;
        }
    }

    /**
     * Rebuilds the access list into a file
     *
     * @return \Phalcon\Acl\Adapter\Memory
     */
    public function rebuild()
    {
        $acl = new AclMemory();

        $acl->setDefaultAction(\Phalcon\Acl::DENY);

        // Register roles
        $profiles = Profiles::find('activo = "Y"');
        foreach ($profiles as $profile) {
            $acl->addRole(new AclRole($profile->nombre));
        }

        $permissions = Permissions::findDistinctByRecursoAndAccion();

        foreach ($permissions as $permission) {
            $acl->addResource(new AclResource($permission->recurso), $permission->accion);
        }

        // Grant acess to private area to role Users
        foreach ($profiles as $profile) {

            /*$permission = Permissions::findById(1);
            print_r($permission);*/
            // Grant permissions in "permissions" model

            foreach (Permissions::findByProfile($profile->id) as $permission) {
                $acl->allow($profile->nombre, $permission->recurso, $permission->accion);
            }

            // Always grant these permissions
//            $acl->allow($profile->id, 'users', 'changePassword');
            //$acl->allow($profile->nombre, 'index', 'index');
        }

        if (touch(APP_DIR . $this->filePath) && is_writable(APP_DIR . $this->filePath)) {

            file_put_contents(APP_DIR . $this->filePath, serialize($acl));

            // Store the ACL in APC
            /*if (function_exists('apc_store')) {
                apc_store('vokuro-acl', $acl);
            }*/
        } else {
            $this->flash->error(
                'The user does not have write permissions to create the ACL list at ' . APP_DIR . $this->filePath
            );
        }

        return $acl;
    }

    public function isPrivateAction($controllerName, $actionName){
        $controllerName = strtolower($controllerName);
        $private = false;
        if($this->isPrivate($controllerName)){
            $private = true;
        }
        else{
            if(!in_array($actionName, $this->publicResources[$controllerName])){
                $private = true;
            }
        }
        return $private;
//        return (!$this->isPrivate($controllerName) and !in_array($this->publicResources[$controllerName], $actionName));
//            $private = true;
//        return $private;
//        return (!isset($this->publicResources[$controllerName]) and !in_array($this->publicResources[$controllerName], $actionName));

    }

    /**
     * Checks if a controller is session or not
     *
     * @param string $controllerName
     * @return boolean
     */
    public function isSession($controllerName, $actionName){
        return isset($this->sesionResources[$controllerName]) && in_array($actionName, $this->sesionResources[$controllerName]);
    }
}
