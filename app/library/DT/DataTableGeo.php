<?php

namespace Vokuro\DT;


use DataTables\Adapters\QueryBuilder;
use DataTables\DataTable;

class DataTableGeo extends DataTable
{
    public function fromBuilder($builder, $columns = []) {
        if (empty($columns)) {
            $columns = $builder->getColumns();
            $columns = (is_array($columns)) ? $columns : array_map('trim', explode(',', $columns));
        }

        $adapter = new QueryBuilderGeo($this->options['length']);

        $adapter->setBuilder($builder);
        $adapter->setParser($this->parser);
        $adapter->setColumns($columns);
        $this->response = $adapter->getResponse();

        return $this;
    }
}