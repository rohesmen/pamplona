<?php

namespace Vokuro\DT;

use Phalcon\Db\Column;
use Vokuro\DT\SSP;
use Vokuro\GenericSQL\GenericSQL;
use Vokuro\Models\Clientes\Clientes;

/*
 * Helper functions for building a DataTables server-side processing SQL query
 *
 * The static functions in this class are just helper functions to help build
 * the SQL used in the DataTables demo server-side processing scripts. These
 * functions obviously do not represent all that can be done with server-side
 * processing, they are intentionally simple to show how it works. More complex
 * server-side processing operations will likely require a custom script.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

class SSPGEO_dos extends SSP {
    /**
     * Perform the SQL queries needed for an server-side processing requested,
     * utilising the helper functions of this class, limit(), order() and
     * filter() among others. The returned array is ready to be encoded as JSON
     * in response to an SSP request, or can be modified if needed before
     * sending back to the client.
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  string $table SQL table to query
     *  @param  string $primaryKey Primary key of the table
     *  @param  array $columns Column information array
     *  @return array          Server-side processing response array
     */
    static function simple_geo ( $request, $table, $primaryKey, $columns )
    {
        $bindings = array();

        // Build the SQL query string from the request
        $limit = self::limit( $request, $columns );
        $order = self::order_join( $request, $columns );
        $where = self::filter_join( $request, $columns, $bindings );

        // Main query to actually get the data
        $colsSelect = implode(", ", self::pluck($columns, 'db'));
        $data = self::sql_exec_geo( $bindings,
            "SELECT ".$colsSelect."
			 FROM $table
			 $where 
			 $order 
			 $limit"
        );

        // Data set length after filtering
        $resFilterLength = self::sql_exec_geo( $bindings,
            "SELECT COUNT({$primaryKey})
			 FROM $table
			 $where"
        );
        $recordsFiltered = $resFilterLength[0]->count;

        // Total data set length
        $resTotalLength = self::sql_exec_geo( "SELECT COUNT({$primaryKey})
			 FROM   $table"
        );
        $recordsTotal = $resTotalLength[0]->count;

        $auxCols = array();
        foreach($columns as $col) {
            $col["db"] = str_replace("\"", "", $col["db"]);
            $auxCols[] = $col;
        }
        /*
         * Output
         */
        return array(
            "draw"            => isset ( $request['draw'] ) ?
                intval( $request['draw'] ) :
                0,
            "recordsTotal"    => intval( $recordsTotal ),
            "recordsFiltered" => intval( $recordsFiltered ),
            "data"            => self::data_output( $auxCols, $data )
        );
    }

    /**
     * The difference between this method and the `simple` one, is that you can
     * apply additional `where` conditions to the SQL queries. These can be in
     * one of two forms:
     *
     * * 'Result condition' - This is applied to the result set, but not the
     *   overall paging information query - i.e. it will not effect the number
     *   of records that a user sees they can have access to. This should be
     *   used when you want apply a filtering condition that the user has sent.
     * * 'All condition' - This is applied to all queries that are made and
     *   reduces the number of records that the user can access. This should be
     *   used in conditions where you don't want the user to ever have access to
     *   particular records (for example, restricting by a login id).
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array|PDO $conn PDO connection resource or connection parameters array
     *  @param  string $table SQL table to query
     *  @param  string $primaryKey Primary key of the table
     *  @param  array $columns Column information array
     *  @param  string $whereResult WHERE condition to apply to the result set
     *  @param  string $whereAll WHERE condition to apply to all queries
     *  @return array          Server-side processing response array
     */
    static function complex_geo ( $request, $table, $primaryKey, $columns, $whereResult=null, $whereAll=null , $datatable = false)
    {
        $bindings = array();

        // Build the SQL query string from the request
        $limit = self::limit( $request, $columns );
        $order = self::order_join( $request, $columns );
        $where = self::filter_join( $request, $columns, $bindings );

        $whereResult = self::_flatten( $whereResult );
        $whereAll = self::_flatten( $whereAll );

        if ( $whereResult ) {
            $where = $where ?
                $where .' AND '.$whereResult :
                'WHERE '.$whereResult;
        }

        if ( $whereAll ) {
            $where = $where ?
                $where .' AND '.$whereAll :
                'WHERE '.$whereAll;

            $whereAllSql = 'WHERE '.$whereAll;
        }

        // Main query to actually get the data
        $colsSelect = implode(", ", self::pluck($columns, 'db'));
        $sq = "SELECT ".$colsSelect."
			 FROM $table
			 $where 
			 $order 
			 $limit";

        //return array("data"            => $sq);

        $data = self::sql_exec_geo( $bindings,
            $sq
        );

        //return array(
        //"data"            => $data
        //);

        // Data set length after filtering
        $resFilterLength = self::sql_exec_geo( $bindings,
            "SELECT COUNT({$primaryKey})
			 FROM $table
			 $where"
        );
        $recordsFiltered = $resFilterLength[0]->count;

        // Total data set length
        $resTotalLength = self::sql_exec_geo( "SELECT COUNT({$primaryKey})
			 FROM   $table $whereAllSql"
        );
        $recordsTotal = $resTotalLength[0]->count;

        $auxCols = array();
        foreach($columns as $col) {
            $col["db"] = str_replace("\"", "", $col["db"]);
            $auxCols[] = $col;
        }

        if($datatable){
            return array(
                "draw"            => isset ( $request['draw'] ) ?
                    intval( $request['draw'] ) :
                    0,
                "recordsTotal"    => intval( $recordsTotal ),
                "recordsFiltered" => intval( $recordsFiltered ),
                "data"            => self::data_output( $auxCols, $data )
            );
        }else{
            return array(
                "draw"            => isset ( $request['draw'] ) ?
                    intval( $request['draw'] ) :
                    0,
                "recordsTotal"    => intval( $recordsTotal ),
                "recordsFiltered" => intval( $recordsFiltered ),
                "data"            => self::data_output( $auxCols, $data )
            );
        }
    }

    /**
     * The difference between this method and the `simple` one, is that you can
     * apply additional `where` conditions to the SQL queries. These can be in
     * one of two forms:
     *
     * * 'Result condition' - This is applied to the result set, but not the
     *   overall paging information query - i.e. it will not effect the number
     *   of records that a user sees they can have access to. This should be
     *   used when you want apply a filtering condition that the user has sent.
     * * 'All condition' - This is applied to all queries that are made and
     *   reduces the number of records that the user can access. This should be
     *   used in conditions where you don't want the user to ever have access to
     *   particular records (for example, restricting by a login id).
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array|PDO $conn PDO connection resource or connection parameters array
     *  @param  string $table SQL table to query
     *  @param  string $primaryKey Primary key of the table
     *  @param  array $columns Column information array
     *  @param  string $whereResult WHERE condition to apply to the result set
     *  @param  string $whereAll WHERE condition to apply to all queries
     *  @return array          Server-side processing response array
     */
    static function complex_join_geo ( $request, $table, $join, $primaryKey, $columns, $whereResult=null, $whereAll=null )
    {
        $bindings = array();

        // Build the SQL query string from the request
        $limit = self::limit( $request, $columns );
        $order = self::order_join( $request, $columns );
        $where = self::filter_join( $request, $columns, $bindings );

        $whereResult = self::_flatten( $whereResult );
        $whereAll = self::_flatten( $whereAll );

        if ( $whereResult ) {
            $where = $where ?
                $where .' AND '.$whereResult :
                'WHERE '.$whereResult;
        }

        if ( $whereAll ) {
            $where = $where ?
                $where .' AND '.$whereAll :
                'WHERE '.$whereAll;

            $whereAllSql = 'WHERE '.$whereAll;
        }

        // Main query to actually get the data
        $colsSelect = implode(", ", self::pluck($columns, 'db'));
        $data = self::sql_exec_geo( $bindings,
            "SELECT $colsSelect 
			 FROM $table c
			 LEFT JOIN $join ln on c.\"INE\"::varchar = ln.clave_elector 
			 $where 
			 $order 
			 $limit"
        );

        // Data set length after filtering
        $resFilterLength = self::sql_exec_geo( $bindings,
            "SELECT COUNT({$primaryKey})
			 FROM $table c
			 LEFT JOIN $join ln on c.\"INE\"::varchar = ln.clave_elector 
			 $where"
        );
        $recordsFiltered = $resFilterLength[0]->count;

        // Total data set length
        $resTotalLength = self::sql_exec_geo(
            "SELECT COUNT({$primaryKey})
             FROM $table  c
			 LEFT JOIN $join ln on c.\"INE\"::varchar = ln.clave_elector 
             $whereAllSql"
        );
        $recordsTotal = $resTotalLength[0]->count;
        $auxCols = array();
        foreach($columns as $col) {
            $col["db"] = str_replace("\"", "", $col["db"]);
            $auxCols[] = $col;
        }
        /*
         * Output
         */
        return array(
            "draw"            => isset ( $request['draw'] ) ?
                intval( $request['draw'] ) :
                0,
            "recordsTotal"    => intval( $recordsTotal ),
            "recordsFiltered" => intval( $recordsFiltered ),
            "data"            => self::data_output( $auxCols, $data )
        );
    }

    /**
     * Execute an SQL query on the database
     *
     * @param  resource $db  Database handler
     * @param  array    $bindings Array of PDO binding values from bind() to be
     *   used for safely escaping strings. Note that this can be given as the
     *   SQL query string if no bindings are required.
     * @param  string   $sql SQL query to execute.
     * @return array         Result from the query (all rows)
     */
    static function sql_exec_geo ($bindings, $sql=null )
    {
        // Argument shifting
        if ( $sql === null ) {
            $sql = $bindings;
        }
        //echo $sql;

        // Bind parameters
        if ( is_array( $bindings ) ) {
            for ( $i=0, $ien=count($bindings) ; $i<$ien ; $i++ ) {
                $binding = $bindings[$i];
                $sql = str_replace($binding['key'],$binding['val'], $sql);
            }
        }

        return GenericSQL::getBySQL($sql);
    }

    static function sql_exec_geo_return ($bindings, $sql=null )
    {
        // Argument shifting
        if ( $sql === null ) {
            $sql = $bindings;
        }
        //echo $sql;

        // Bind parameters
        if ( is_array( $bindings ) ) {
            for ( $i=0, $ien=count($bindings) ; $i<$ien ; $i++ ) {
                $binding = $bindings[$i];
                $sql = str_replace($binding['key'],$binding['val'], $sql);
            }
        }

        return $sql;
    }

    /**
     * Paging
     *
     * Construct the LIMIT clause for server-side processing SQL query
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     *  @return string SQL limit clause
     */
    static function limit ( $request, $columns )
    {
        $limit = '';

        if ( isset($request['start']) && $request['length'] != -1 ) {
            $limit = "LIMIT ".intval($request['length'])." OFFSET ".intval($request['start']);
        }

        return $limit;
    }


    /**
     * Ordering
     *
     * Construct the ORDER BY clause for server-side processing SQL query
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     *  @return string SQL order by clause
     */
    static function order ( $request, $columns )
    {
        $order = '';

        if ( isset($request['order']) && count($request['order']) ) {
            $orderBy = array();
            $dtColumns = self::pluck( $columns, 'dt' );

            for ( $i=0, $ien=count($request['order']) ; $i<$ien ; $i++ ) {
                // Convert the column index into the column data property
                $columnIdx = intval($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];

                $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                $column = $columns[ $columnIdx ];

                if ( $requestColumn['orderable'] == 'true' ) {
                    $dir = $request['order'][$i]['dir'] === 'asc' ?
                        'ASC' :
                        'DESC';

                    $orderBy[] = '"'.$column['db'].'" '.$dir;
                }
            }

            $order = 'ORDER BY '.implode(', ', $orderBy);
        }

        return $order;
    }

    /**
     * Ordering
     *
     * Construct the ORDER BY clause for server-side processing SQL query
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     *  @return string SQL order by clause
     */
    static function order_join ( $request, $columns )
    {
        $order = '';

        if ( isset($request['order']) && count($request['order']) ) {
            $orderBy = array();
            $dtColumns = self::pluck( $columns, 'dt' );

            for ( $i=0, $ien=count($request['order']) ; $i<$ien ; $i++ ) {
                // Convert the column index into the column data property
                $columnIdx = intval($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];

                $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                $column = $columns[ $columnIdx ];

                if ( $requestColumn['orderable'] == 'true' ) {
                    $dir = $request['order'][$i]['dir'] === 'asc' ?
                        'ASC' :
                        'DESC';

                    $orderBy[] = $column['db'].' '.$dir;
                }
            }

            $order = 'ORDER BY '.implode(', ', $orderBy);
        }

        return $order;
    }


    /**
     * Searching / Filtering
     *
     * Construct the WHERE clause for server-side processing SQL query.
     *
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here performance on large
     * databases would be very poor
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     *  @param  array $bindings Array of values for PDO bindings, used in the
     *    sql_exec() function
     *  @return string SQL where clause
     */
    static function filter ( $request, $columns, &$bindings )
    {
        $globalSearch = array();
        $columnSearch = array();
        $dtColumns = self::pluck( $columns, 'dt' );

        if ( isset($request['search']) && $request['search']['value'] != '' ) {
            for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
                $str = $request['search']['value'];
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                $column = $columns[ $columnIdx ];

                if ( $requestColumn['searchable'] == 'true' ) {
                    if(is_array($str) && !empty($str)){
                        if(isset($column['datatype']) && $column['datatype'] && $column['datatype'] == "number"){
                            $str = array_map('trim', $str);
                            $str = implode(", ", $str);
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $globalSearch[] = "\"".$column['db']."\"::text in ('$binding')";
                        }
                        else{
                            $str = array_map('trim', $str);
                            $str = array_map('strtolower', $str);
                            $str = implode("%')),trim(lower('%", $str);
                            $str = "trim(lower('%$str%'))";
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $globalSearch[] = "trim(lower(\"".$column['db']."\"::text)) LIKE ALL (array[$binding])";
                        }
                    }
                    else if(!is_array($str)){
                        if(isset($column['datatype']) && $column['datatype'] && $column['datatype'] == "number"){
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $globalSearch[] = "\"".$column['db']."\"::text in ('$binding')";
                        }
                        else{
                            if(isset($column['separator']) && $column['separator'] && $column['separator'] != ""){
                                $straux = explode($column['separator'], $str);
                                $str = implode("%')),trim(lower('%", $straux);
                            }
                            $str = "trim(lower('%$str%'))";
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $globalSearch[] = "trim(lower(\"".$column['db']."\"::text)) LIKE ALL (array[$binding])";
                        }
                    }
                }
            }
        }

        // Individual column filtering
        if ( isset( $request['columns'] ) ) {
            for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                $column = $columns[ $columnIdx ];

                $str = $requestColumn['search']['value'];

                if ( $requestColumn['searchable'] == 'true' &&
                    $str != '' ) {
                    if(is_array($str) && !empty($str)){
                        if(isset($column['datatype']) && $column['datatype'] && $column['datatype'] == "number"){
                            $str = array_map('trim', $str);
                            $str = implode(", ", $str);
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $columnSearch[] = "\"".$column['db']."\"::text in ($binding)";
                        }
                        else{
                            $str = array_map('trim', $str);
                            $str = array_map('strtolower', $str);
                            $str = implode("%')),trim(lower('%", $str);
                            $str = "trim(lower('%$str%'))";
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $columnSearch[] = "trim(lower(\"".$column['db']."\"::text)) LIKE ALL (array[$binding])";
                        }
                    }
                    else if(!is_array($str)){
                        if(isset($column['datatype']) && $column['datatype'] && $column['datatype'] == "number"){
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $columnSearch[] = "\"".$column['db']."\"::text in ('$binding')";
                        }
                        else if(isset($column['datatype']) && $column['datatype'] && $column['datatype'] == "boolean"){
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $columnSearch[] = "\"".$column['db']."\" = $binding";
                        }
                        else{
                            if(isset($column['separator']) && $column['separator'] && $column['separator'] != ""){
                                $straux = explode($column['separator'], $str);
                                $str = implode("%')),trim(lower('%", $straux);
                            }
                            $str = "trim(lower('%$str%'))";
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $columnSearch[] = "trim(lower(\"".$column['db']."\"::text)) LIKE ALL (array[$binding])";
                        }
                    }

//                    $binding = self::bind( $bindings, "'%".$str."%'", Column::PARAM_STR );
//                    $columnSearch[] = "trim(lower(\"".$column['db']."\")) LIKE trim(lower(".$binding."))";
                }
            }
        }

        // Combine the filters into a single string
        $where = '';

        if ( count( $globalSearch ) ) {
            $where = '('.implode(' OR ', $globalSearch).')';
        }

        if ( count( $columnSearch ) ) {
            $where = $where === '' ?
                implode(' AND ', $columnSearch) :
                $where .' AND '. implode(' AND ', $columnSearch);
        }

        if ( $where !== '' ) {
            $where = 'WHERE '.$where;
        }

        return $where;
    }

    /**
     * Searching / Filtering
     *
     * Construct the WHERE clause for server-side processing SQL query.
     *
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here performance on large
     * databases would be very poor
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     *  @param  array $bindings Array of values for PDO bindings, used in the
     *    sql_exec() function
     *  @return string SQL where clause
     */
    static function filter_join ( $request, $columns, &$bindings )
    {
        $globalSearch = array();
        $columnSearch = array();
        $dtColumns = self::pluck( $columns, 'dt' );

        if ( isset($request['search']) && $request['search']['value'] != '' ) {
            for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
                $str = $request['search']['value'];
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                $column = $columns[ $columnIdx ];

                if(isset($column['ignoreall']) && $column['ignoreall'])continue;
                if ( $requestColumn['searchable'] == 'true' ) {
                    if(is_array($str) && !empty($str)){
                        if(isset($column['datatype']) && $column['datatype'] && ($column['datatype'] == "number" || $column['datatype'] == "date")){
                            continue;
                            $str = array_map('trim', $str);
                            $str = array_map('strtolower', $str);
                            $str = implode("%')),trim(lower('%", $str);
                            $str = "trim(lower('%$str%'))";
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $globalSearch[] = "trim(lower(".$column['db']."::text)) LIKE ALL (array[$binding])";
                        }
                        elseif (isset($column['datatype']) && $column['datatype'] && $column['datatype'] == "boolean"){
                            continue;
                            $str = array_map('trim', $str);
                            $str = array_map('strtolower', $str);
                            $str = implode("%')),trim(lower('%", $str);
                            $str = "trim(lower('%$str%'))";
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $globalSearch[] = "trim(lower(".$column['db']."::text)) LIKE ALL (array[$binding])";
                        }
                        else{
                            $str = array_map('trim', $str);
                            $str = array_map('strtolower', $str);
                            $str = implode("%')),trim(lower('%", $str);
                            $str = "trim(lower('%$str%'))";
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $globalSearch[] = "trim(lower(".$column['db']."::text)) LIKE ALL (array[$binding])";
                        }
                    }
                    else if(!is_array($str)){
                        if(isset($column['datatype']) && $column['datatype'] && ($column['datatype'] == "number" || $column['datatype'] == "date")){
//                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
//                            $globalSearch[] = $column['db']." in ($binding)";
                            continue;
                            $str = "trim(lower('%$str%'))";
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $globalSearch[] = "trim(lower(".$column['db']."::text)) LIKE ALL (array[$binding])";
                        }
                        elseif (isset($column['datatype']) && $column['datatype'] && $column['datatype'] == "boolean"){
//                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
//                            $globalSearch[] = $column['db']." in ($binding)";
                            continue;
                            $str = "trim(lower('%$str%'))";
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $globalSearch[] = "trim(lower(".$column['db']."::text)) LIKE ALL (array[$binding])";
                        }
                        else{
//                            if(isset($column['separator']) && $column['separator'] && $column['separator'] != ""){
//                                $straux = explode($column['separator'], $str);
//                                $str = implode("%')),trim(lower('%", $straux);
//                            }
                            $straux = explode((isset($column['separator']) && $column['separator']) ? $column['separator'] : " ", $str);
                            $str = implode("%')),trim(lower('%", $straux);
                            $str = "trim(lower('%$str%'))";
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $globalSearch[] = "trim(lower(".$column['db']."::text)) LIKE ALL (array[$binding])";
                        }
                    }
                }
            }
        }

        // Individual column filtering
        if ( isset( $request['columns'] ) ) {
            for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                $column = $columns[ $columnIdx ];

                $str = $requestColumn['search']['value'];

                if ( $requestColumn['searchable'] == 'true' &&
                    $str != '' ) {
                    if(is_array($str) && !empty($str)){
                        if(isset($column['datatype']) && $column['datatype'] && $column['datatype'] == "number"){
                            $str = array_map('trim', $str);
                            $str = implode(", ", $str);
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $columnSearch[] = $column['db']." in ($binding)";
                        }
                        elseif (isset($column['datatype']) && $column['datatype'] && $column['datatype'] == "boolean"){
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $columnSearch[] = $column['db']."::text LIKE ALL (array[$binding])";
                        }
                        else{
                            $str = array_map('trim', $str);
                            $str = array_map('strtolower', $str);
                            $str = implode("%')),trim(lower('%", $str);
                            $str = "trim(lower('%$str%'))";
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $columnSearch[] = "trim(lower(".$column['db']."::text)) LIKE ALL (array[$binding])";
                        }
                    }
                    else if(!is_array($str)){
                        if(isset($column['datatype']) && $column['datatype'] && $column['datatype'] == "number"){
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            if(isset($column['widthNull']) && $column['widthNull'] && $str == "-1"){
                                $columnSearch[] = $column['db']." is null";
                            }
                            else{
                                $columnSearch[] = $column['db']." in ($binding)";
                            }
                        }
                        elseif (isset($column['datatype']) && $column['datatype'] && $column['datatype'] == "boolean"){
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $columnSearch[] = $column['db']." = $binding";
                        }
                        elseif (isset($column['datatype']) && $column['datatype'] && $column['datatype'] == "literaltext"){
                            if(isset($column['separator']) && $column['separator'] && $column['separator'] != ""){
                                $straux = explode($column['separator'], $str);
                                $str = implode("')),trim(lower('", $straux);
                            }
                            $str = "trim(lower('$str'))";
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $columnSearch[] = "trim(lower(".$column['db'].")) LIKE ANY (array[$binding])";
                        }
                        elseif (isset($column['existence']) && $column['existence']){
                            $binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $columnSearch[] = $column['db']." $binding";
                        }
                        else{
                            //$str = "trim(lower('%$str%'))";
                            //$binding = self::bind( $bindings, $str, Column::BIND_PARAM_STR);
                            $columnSearch[] = "trim(lower(".$column['db']."::text)) LIKE ANY (array[$str])";
                        }
                    }

//                    $binding = self::bind( $bindings, "'%".$str."%'", Column::PARAM_STR );
//                    $columnSearch[] = "trim(lower(\"".$column['db']."\")) LIKE trim(lower(".$binding."))";
                }
            }
        }

        // Combine the filters into a single string
        $where = '';

        if ( count( $globalSearch ) ) {
            $where = '('.implode(' OR ', $globalSearch).')';
        }

        if ( count( $columnSearch ) ) {
            $where = $where === '' ?
                implode(' AND ', $columnSearch) :
                $where .' AND '. implode(' AND ', $columnSearch);
        }

        if ( $where !== '' ) {
            $where = 'WHERE '.$where;
        }

        return $where;
    }

    /**
     * Create the data output array for the DataTables rows
     *
     *  @param  array $columns Column information array
     *  @param  array $data    Data from the SQL get
     *  @return array          Formatted data in a row based format
     */
    static function data_output ( $columns, $data )
    {
        $out = array();

        for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
            $row = array();
            $dataArray = json_decode(json_encode($data[$i]), true);
            for ( $j=0, $jen=count($columns) ; $j<$jen ; $j++ ) {
                $column = $columns[$j];

                // Is there a formatter?
                if ( isset( $column['formatter'] ) ) {
                    $row[ $column['dt'] ] = $column['formatter']( $dataArray[ $column['db'] ], $dataArray );
                }
                else {
                    $row[ $column['dt'] ] = ($dataArray[ $columns[$j]['db'] ] != null && $dataArray[ $columns[$j]['db'] ] != "") ? $dataArray[ $columns[$j]['db'] ] : "";
                }
            }

            $out[] = $row;
        }

        return $out;
    }

    /**
     * Pull a particular property from each assoc. array in a numeric array,
     * returning and array of the property values from each item.
     *
     *  @param  array  $a    Array to get data from
     *  @param  string $prop Property to read
     *  @return array        Array of property values
     */
    static function pluck ( $a, $prop)
    {
        $out = array();

        for ( $i=0, $len=count($a) ; $i<$len ; $i++ ) {
            if($a[$i][$prop] != null and $a[$i][$prop] != "")
                $out[$i] = $a[$i][$prop];
        }

        return $out;
    }

    /**
     * Create a PDO binding key which can be used for escaping variables safely
     * when executing a query with sql_exec()
     *
     * @param  array &$a    Array of bindings
     * @param  *      $val  Value to bind
     * @param  int    $type PDO field type
     * @return string       Bound key to be used in the SQL where this parameter
     *   would be used.
     */
    static function bind ( &$a, $val, $type )
    {
        $key = '{binding_'.count( $a ).'}';

        $a[] = array(
            'key' => $key,
            'val' => $val,
            'type' => $type
        );

        return $key;
    }

    /**
     * Return a string from an array or a string
     *
     * @param  array|string $a Array to join
     * @param  string $join Glue for the concatenation
     * @return string Joined string
     */
    static function _flatten ( $a, $join = ' AND ' )
    {
        if ( ! $a ) {
            return '';
        }
        else if ( $a && is_array($a) ) {
            return implode( $join, $a );
        }
        return $a;
    }
}

