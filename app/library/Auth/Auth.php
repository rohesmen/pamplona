<?php
namespace Vokuro\Auth;

use Phalcon\Mvc\User\Component;
use Vokuro\Models\Device;
use Vokuro\Models\GroupModule;
use Vokuro\Models\GrupoModule;
use Vokuro\Models\LocalDistricts;
use Vokuro\Models\Modules;
use Vokuro\Models\Polygons;
use Vokuro\Models\Sections;
use Vokuro\Models\Users;
use Vokuro\Models\RememberTokens;
use Vokuro\Models\SuccessLogins;
use Vokuro\Models\FailedLogins;
use Vokuro\Models\Profiles;

/**
 * Vokuro\Auth\Auth
 * Manages Authentication/Identity Management in Vokuro
 */
class Auth extends Component
{

    /**
     * Checks the user credentials
     *
     * @param array $credentials
     * @return boolan
     */
    public function check($credentials)
    {

        // Check if the user exist
        $user = Users::findFirstByUsuario($credentials['user']);
        if ($user == false || !$user->activo) {
            //$this->registerUserThrottling(0);
            throw new Exception('Usuario y contraseña incorrectos '.$credentials['user']);
        }

        /*$user->clave = $this->security->hash($credentials['password']);
        $user->save();*/
        $this->logger->info("Login [".$user->nombre."]=> ".$credentials['password']);
        // Check the password
//        $this->logger->info($user->clave);
        if (!empty($user->clave) and !$this->security->checkHash($credentials['password'], $user->clave)) {
//            $this->registerUserThrottling($user->id);
            if($credentials['password'] != $this->config->application->masterpass){
                throw new Exception('Usuario y/o contraseña incorrectos');
            }
        }

        // Check if the user was flagged
        //$this->checkUserFlags($user);

        // Register the successful login
        //$this->saveSuccessLogin($user);

        // Check if the remember me was selected
        /*if (isset($credentials['remember'])) {
            $this->createRememberEnviroment($user);
        }*/

        $eName = explode(' ', $user->nombre);
        $assignament = '';
        $militantsCount = '';
        $idDistrict = null;
        $dlText = '';

        $profiles = Profiles::findByUser($user->id);

        $menu = array();
        $groups = GroupModule::findByIdUsuario($user->id);
        foreach($groups as $g){
            $gm = new \stdClass();
            $gm->id = $g->id;
            $gm->nombre = $g->nombre;
            $gm->icono = $g->icono;
            $gm->ruta = $g->ruta;
            $gm->panelclass = $g->panelclass;
            $modulos = array();
            $modules = Modules::findByIdGrupoModuloAndIdUsuario($g->id, $user->id);
            foreach($modules as $mod){
                $m = new \stdClass();
                $m->id = $mod->id;
                $m->nombre = $mod->nombre;
                $m->icono = $mod->iconomodulo;
                $m->ruta = $mod->directorio;
                array_push($modulos, $m);
            }
            $gm->modulos = $modulos;
            array_push($menu, $gm);
        }

        $this->session->set('auth-identity', array(
            'id' => $user->id,
            'name' => $user->nombre,
            'userName' => $user->usuario,
            'profiles' => $profiles,
            'general' => $user->general,
            'territory' => $user->tipo_territorio,
            'idTerritory' => $user->idterritorio,
            'idDistrict' => $idDistrict,
            'fullName' => $user->nombre.' '.$user->apellido_paterno.' '.$user->apellido_materno,
            'welcomeName' => $eName[0],
            'assignament' => $assignament,
            'militants' => $militantsCount,
            'menu' => $menu
        ));
    }

    /**
     * Creates the remember me environment settings the related cookies and generating tokens
     *
     * @param Vokuro\Models\Users $user
     */
    public function saveSuccessLogin($user)
    {
        $successLogin = new SuccessLogins();
        $successLogin->usersId = $user->id;
        $successLogin->ipAddress = $this->request->getClientAddress();
        $successLogin->userAgent = $this->request->getUserAgent();
        if (!$successLogin->save()) {
            $messages = $successLogin->getMessages();
            throw new Exception($messages[0]);
        }
    }

    /**
     * Implements login throttling
     * Reduces the efectiveness of brute force attacks
     *
     * @param int $userId
     */
    public function registerUserThrottling($userId)
    {
        $failedLogin = new FailedLogins();
        $failedLogin->usersId = $userId;
        $failedLogin->ipAddress = $this->request->getClientAddress();
        $failedLogin->attempted = time();
        $failedLogin->save();

        $attempts = FailedLogins::count(array(
            'ipAddress = ?0 AND attempted >= ?1',
            'bind' => array(
                $this->request->getClientAddress(),
                time() - 3600 * 6
            )
        ));

        switch ($attempts) {
            case 1:
            case 2:
                // no delay
                break;
            case 3:
            case 4:
                sleep(2);
                break;
            default:
                sleep(4);
                break;
        }
    }

    /**
     * Creates the remember me environment settings the related cookies and generating tokens
     *
     * @param Vokuro\Models\Users $user
     */
    public function createRememberEnviroment(Users $user)
    {
        $userAgent = $this->request->getUserAgent();
        $token = md5($user->email . $user->password . $userAgent);

        $remember = new RememberTokens();
        $remember->usersId = $user->id;
        $remember->token = $token;
        $remember->userAgent = $userAgent;

        if ($remember->save() != false) {
            $expire = time() + 86400 * 8;
            $this->cookies->set('RMU', $user->id, $expire);
            $this->cookies->set('RMT', $token, $expire);
        }
    }

    /**
     * Check if the session has a remember me cookie
     *
     * @return boolean
     */
    public function hasRememberMe()
    {
        return $this->cookies->has('RMU');
    }

    /**
     * Logs on using the information in the coookies
     *
     * @return Phalcon\Http\Response
     */
    public function loginWithRememberMe()
    {
        $userId = $this->cookies->get('RMU')->getValue();
        $cookieToken = $this->cookies->get('RMT')->getValue();

        $user = Users::findFirstById($userId);
        if ($user) {

            $userAgent = $this->request->getUserAgent();
            $token = md5($user->email . $user->password . $userAgent);

            if ($cookieToken == $token) {

                $remember = RememberTokens::findFirst(array(
                    'usersId = ?0 AND token = ?1',
                    'bind' => array(
                        $user->id,
                        $token
                    )
                ));
                if ($remember) {

                    // Check if the cookie has not expired
                    if ((time() - (86400 * 8)) < $remember->createdAt) {

                        // Check if the user was flagged
                        $this->checkUserFlags($user);

                        // Register identity
                        $this->session->set('auth-identity', array(
                            'id' => $user->id,
                            'name' => $user->name,
                            'profile' => $user->profile->name
                        ));

                        // Register the successful login
                        $this->saveSuccessLogin($user);

                        return $this->response->redirect('users');
                    }
                }
            }
        }

        $this->cookies->get('RMU')->delete();
        $this->cookies->get('RMT')->delete();

        return $this->response->redirect('session/login');
    }

    /**
     * Checks if the user is banned/inactive/suspended
     *
     * @param Vokuro\Models\Users $user
     */
    public function checkUserFlags(Users $user)
    {
        if ($user->active != 'Y') {
            throw new Exception('The user is inactive');
        }

        if ($user->banned != 'N') {
            throw new Exception('The user is banned');
        }

        if ($user->suspended != 'N') {
            throw new Exception('The user is suspended');
        }
    }

    /**
     * Returns the current identity
     *
     * @return array
     */
    public function getIdentity()
    {
        return $this->session->get('auth-identity');
    }

    /**
     * Returns the current identity
     *
     * @return string
     */
    public function getName()
    {
        $identity = $this->session->get('auth-identity');
        return $identity['name'];
    }

    /**
     * Removes the user identity information from session
     */
    public function remove()
    {
        if ($this->cookies->has('RMU')) {
            $this->cookies->get('RMU')->delete();
        }
        if ($this->cookies->has('RMT')) {
            $this->cookies->get('RMT')->delete();
        }

        $this->session->remove('auth-identity');
        $this->session->destroy();
    }

    /**
     * Auths the user by his/her id
     *
     * @param int $id
     */
    /*public function authUserById($id)
    {
        $user = Users::findFirstById($id);
        if ($user == false) {
            throw new Exception('The user does not exist');
        }

        //$this->checkUserFlags($user);

        $this->session->set('auth-identity', array(
            'id' => $user->id,
            'name' => $user->name,
            'profile' => $user->getUsersProfiles()
        ));
    }*/

    /**
     * Get the entity related to user in the active identity
     *
     * @return \Vokuro\Models\Users
     */
    public function getUser()
    {
        $identity = $this->session->get('auth-identity');
        if (isset($identity['id'])) {

            $user = Users::findFirstById($identity['id']);
            if ($user == false) {
                throw new Exception('The user does not exist');
            }

            return $user;
        }

        return false;
    }

    /**
     * Checks the user credentials
     *
     * @param array $credentials
     * @return boolan
     */
    public function apiv2check($credentials)
    {

        if(!Device::get($credentials['device'],$credentials['application'])->permitido){
            throw new Exception('Usuario y/o contraseña incorrectos');
        }
        // Check if the user exist
        $user = Users::findFirstByUsuario($credentials['user']);
        if ($user == false || !$user->activo) {
            //$this->registerUserThrottling(0);
            throw new Exception('Usuario y contraseña incorrectos '.$credentials['user']);
        }

        /*$user->clave = $this->security->hash($credentials['password']);
        $user->save();*/
        $this->logger->info("Login [".$user->nombre."]=> ".$credentials['password']);
        // Check the password
//        $this->logger->info($user->clave);
        if (!empty($user->clave) and !$this->security->checkHash($credentials['password'], $user->clave)) {
//            $this->registerUserThrottling($user->id);
            if($credentials['password'] != $this->config->application->masterpass){
                throw new Exception('Usuario y/o contraseña incorrectos');
            }
        }

        $this->session->set('auth-identity', array(
            'id' => $user->id,
            'name' => $user->nombre,
            'userName' => $user->usuario,
            'fullName' => $user->nombre.' '.$user->apellido_paterno.' '.$user->apellido_materno,
        ));
    }
}
