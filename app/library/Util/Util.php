<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 5/10/16
 * Time: 02:56 PM
 */
namespace Vokuro\Util;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\User\Component;
use Phalcon\DiInterface;
use Phalcon\Di;
use Swift_Message as Message;
use Swift_SmtpTransport as Smtp;
use Vokuro\Models\Clientes;
use Vokuro\Models\HistorialPago;
use Vokuro\Models\MunicipiosYucatan;
use Vokuro\Models\Pagos;

class Util extends Component
{

    public static function notifyServicio($contenido){
        $di = Di::getDefault();
        $db = $di->getDb();
        $auth = $di->getAuth();
        $identity = $auth->getIdentity();
        $logger = $di->getLogger();
        try{
            $server = 'mail.pamplona.com.mx';
            $port = 465;
            $security = 'ssl';
            $username = "reportes@pamplona.com.mx";
            $password = "reportes2019";

//            $server = 'smtp.gmail.com';
//            $port = 465;
//            $security = 'ssl';
//            $username = "evandersoft@gmail.com";
//            $password = "evander2018";


            $message = Message::newInstance()
                ->setSubject("Solicitud de servicio")
                ->setTo([
//                    "juan.caamal@pamplona.com.mx",
//                    "sergio.zapata@gmail.com",
                    'rohesmen@gmail.com' => 'Gaspar'
                ])
                ->setFrom(array(
                    "reportes@pamplona.com.mx" => "Servicios"
                ))
                ->setBcc(['rohesmen@gmail.com' => 'Gaspar'])
                ->setBody($contenido, 'text/html');

            $transport = Smtp::newInstance(
                $server,
                $port,
                $security
            )
                ->setUsername($username)
                ->setPassword($password);
//            }

            $mailer = \Swift_Mailer::newInstance($transport);

            if($mailer->send($message)){
//            $response->setStatusCode(200);
            }
            else{
//            $response->setStatusCode(500);
            }
        }
        catch (Exception $e){
            $logger->error($e->getMessage()."\n".$e->getTraceAsString());
        }
    }

    public static function notifyDatosContacto($contenido){
        $di = Di::getDefault();
        $db = $di->getDb();
        $auth = $di->getAuth();
        $identity = $auth->getIdentity();
        $logger = $di->getLogger();
        try{
//            'server' => 'smtp.gmail.com',
//            'port' => 465,
//            'security' => 'ssl',
//            'username' => 'plataformamid@gmail.com',
//            'password' => 's1st3mas4tp'
            $server = 'smtp.gmail.com';
            $port = 465;
            $security = 'ssl';
            $username = "contacto.cliente.pamplona@gmail.com";
            $password = "r3p0rt3$";

            $message = Message::newInstance()
                ->setSubject("Contactar cliente")
                ->setTo([
//                    "juan.caamal@pamplona.com.mx",
//                    "sergio.zapata@gmail.com",
//                    "david.mex@evander.com.mx",
//                    "dmextzab@gmail.com"
                    'reportes@pamplona.com.mx'
                ])
                ->setFrom(array(
//                    'reportes@pamplona.com.mx'
                    "contacto.cliente.pamplona@gmail.com" => "Contacto clientes"
                ))
                ->setBcc(['rohesmen@gmail.com' => 'Gaspar'])
                ->setBody($contenido, 'text/html');

            $transport = Smtp::newInstance(
                $server,
                $port,
                $security
            )
                ->setUsername($username)
                ->setPassword($password);
//            }

            $mailer = \Swift_Mailer::newInstance($transport);

            if($mailer->send($message)){
//            $response->setStatusCode(200);
            }
            else{
//            $response->setStatusCode(500);
            }
        }
        catch (Exception $e){
            $logger->error($e->getMessage()."\n".$e->getTraceAsString());
        }
    }

    public static function renderJSON($file, $convert = true, ...$params){
        $cfg = Di::getDefault()->getConfig();

        $template = file_get_contents($cfg->application->jsonDir.$file.'.json');

        $length = sizeof($params);
        for($i =0; $i < $length; $i++){
            $template = str_replace('$'.($i+1).'$', $params[$i], $template);
        }

        return $convert? json_decode($template) : $template;
    }

    public static function formatDateForView($date) {
        if(empty($date)) {
            return "";
        }
        $fecha = date_create_from_format('Y-m-d H:i:s',$date);
        return date_format($fecha,'d/m/Y');
    }

    public static function formatDateTimeForView($date) {
        if(empty($date)) {
            return "";
        }
        $fecha = date_create_from_format('Y-m-d H:i:s',$date);
        return date_format($fecha,'d/m/Y H:i');
    }

    public static function formatDateVForView($date) {
        if(empty($date)) {
            return "";
        }
        $fecha = date_create_from_format('Y-m-d',$date);
        return date_format($fecha,'d/m/Y');
    }

    public static function formatToCurrency($d){
        $cantidad = '$ 0.00';
        if($d > 0){
            $cantidad = '$ '.number_format($d, 2, '.', ',');
        }
        return $cantidad;
    }

    public static function number_words($valor,$desc_moneda, $sep, $desc_decimal) {
        $arr = explode(".", $valor);
        $entero = $arr[0];
        if (isset($arr[1])) {
            $decimos = strlen($arr[1]) == 1 ? $arr[1] . '0' : $arr[1];
        }

        $fmt = new \NumberFormatter('es', \NumberFormatter::SPELLOUT);
        if (is_array($arr)) {
            $num_word = ($arr[0]>=1000000) ? "{$fmt->format($entero)} de $desc_moneda" : "{$fmt->format($entero)} $desc_moneda";
            if (isset($decimos) && $decimos > 0) {
                $num_word .= " $sep  {$fmt->format($decimos)} $desc_decimal";
            }
        }
        return $num_word;
    }

    public static function getMonthNumberToString($val){
        $meses = array(
            "1" => "Enero",
            "2" => "Febrero",
            "3" => "Marzo",
            "4" => "Abril",
            "5" => "Mayo",
            "6" => "Junio",
            "7" => "Julio",
            "8" => "Agosto",
            "9" => "Septiembre",
            "10" => "Octubre",
            "11" => "Noviembre",
            "12" => "Diciembre",
        );
        return $meses[$val];
    }

    public static function getDataRecibo($idCliente, $idPago){
        $c = Clientes::findFirstById_cliente($idCliente);
        $oMun = MunicipiosYucatan::findFirstByCve_mun($c->idmunicipio);

        $nombre = $c->getFUllName();

        $direccion = $c->getFormatAddress();

        $pago = Pagos::findFirst("idpago = ".$idPago);

        $fecha_pago = date("d/m/Y H:i A", strtotime(str_replace('/', '-',$pago->fecha_pago)));

        $HistorialPago = HistorialPago::find([
            "idpago = $idPago",
            "order" => "id"
        ]);

        if($HistorialPago) {
            $mesesString = '';

            $sumdec = 0;
            $sumtotal = 0;
            $mesesPagados = array();
            foreach ($HistorialPago as $hp) {
                $data = Util::getMonthNumberToString($hp->mes);
                if(isset($mesesPagados[$hp->anio])){
                    array_push($mesesPagados[$hp->anio], $data);
                }
                else{
                    $mesesPagados[$hp->anio] = [];
                    array_push($mesesPagados[$hp->anio], $data);
                }
                $sumdec += $hp->descuento;
                $sumtotal += $hp->cantidad;
            }


            foreach ($mesesPagados as $aa => $auxanio){
                $mesesString .= $aa . ': ';
                foreach ($auxanio as $am){
                    $mesesString .= $am . ', ';
                }
                $mesesString = trim($mesesString, ', ').'<br>';
            }

            $montoPromo = 0;
            if($pago->ispagoanual){
                $montoPromo = $pago->totpagoanual;
            }

            return [
                "folio" => $pago->folio_interno,
                "nombre" => $nombre,
                "direccion" => $direccion,
                "idcliente" => $idCliente,
                "fecha_pago" => $fecha_pago,
                "meses" => $mesesString,
                "municipio" => $oMun->nombre,
                "ispagoanual" => $pago->ispagoanual,
                "monto_anual" => Util::formatToCurrency($montoPromo),
                "cantidad_letras" => NumeroLetras::convertir($pago->cantidad,"pesos","centavos",true),
                "cantidad" => Util::formatToCurrency($pago->cantidad),
                "descuento" => $sumdec,
                "format_descuento" => Util::formatToCurrency($sumdec),
                "isnegociacion" => $pago->isnegociacion,
                "total_pago" => $pago->total_pago ? Util::formatToCurrency($pago->total_pago) : ($sumtotal > 0 ? Util::formatToCurrency($sumtotal) : 0),
                "descuento_negociacion" => $pago->descuento_negociacion ? Util::formatToCurrency($pago->descuento_negociacion) : 0,
            ];
        }
        return null;
    }
}