<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 5/10/16
 * Time: 02:56 PM
 */
namespace Vokuro\Util;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\User\Component;
use Phalcon\DiInterface;
use Phalcon\Di;
use Swift_Message as Message;
use Swift_SmtpTransport as Smtp;

class UtilFile extends Component {
    public static function saveFile($base64, $ruta, $prefix = 'file'){
//        sleep(1);
        $di = Di::getDefault();
        $config = $di->getConfig();
        $logger = $di->getLogger();
        $rand6 = substr(uniqid('', true), -6);


        try {
            //$logger->error("ruta: ".$ruta." ".$output_file);
            $data = explode(',', $base64);


            if (count($data) === 1) {
                $filename = $prefix.time()."_".rand(1,10000).$rand6.".jpeg";
                $output_file = $ruta.$filename;
                $ifp = fopen($output_file, 'wb');

                fwrite($ifp, base64_decode($data[0]));

                fclose($ifp);
//                file_put_contents($output_file, $data[1]);
                if(!file_exists($output_file)){
                    $logger->error("No se pudo guardar la imagen");
                    $filename = null;
                }
            }
            else{
                $mimetype = str_replace(array('data:', ';base64'), '', $data[0]);
                $ext = UtilFile::getFileExtensionFromMimeType($mimetype);
                $filename = $prefix.time()."_".rand(1,10000).$rand6.".".$ext;
                $output_file = $ruta.$filename;
                $ifp = fopen($output_file, 'wb');

                fwrite($ifp, base64_decode($data[1]));

                fclose($ifp);
//                file_put_contents($output_file, $data[1]);
                if(!file_exists($output_file)){
                    $logger->error("No se pudo guardar la imagen");
                    $filename = null;
                }
            }
        }
        catch (Exception $e){
            $filename = null;
            $logger->error($e->getMessage(). "\n".$e->getTraceAsString());
        }
        return $filename;
    }

    public static function getFileExtensionFromMimeType($mimetype){
        $mimetypes = array(
            'application/pdf' => 'pdf'
        );

        if($mimetypes[$mimetype]){
            return $mimetypes[$mimetype];
        }
        else {
            return 'jpeg';
        }
    }
}