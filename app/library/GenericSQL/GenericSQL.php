<?php
namespace  Vokuro\GenericSQL;

use Phalcon\Di;
use Phalcon\Db;
use Phalcon\Mvc\User\Component;
use Vokuro\Util\Util;

class GenericSQL extends Component
{

    public static function getColumnsCsv($ruta, $archivo, $cabecera, $separador){
        $connection = Di::getDefault()->getDb();

        $sql = <<<EOT
SELECT * FROM persona.obtener_columnas_csv('$ruta', '$archivo', $cabecera, '$separador') as json;
EOT;
        $resp = $connection->fetchAll(
            $sql,
            Db::FETCH_OBJ
        );
        return $resp[0]->json;
    }

    public static function saveCsv($nombre, $descripcion, $ruta, $archivo, $cabecera, $ine, $separador, $idcatmp, $idusuario, $columnas, $permisos){
        $connection = Di::getDefault()->getDb();
        $contitulo = $cabecera ? "true" : "false";
        $conine = $ine ? "true" : "false";
        $archivo = $archivo != null ? "'$archivo'" : "null";
        $ruta = $ruta != null ? "'$ruta'" : "null";
        $idcatmp = $idcatmp != null ? $idcatmp : "null";
        $sql = <<<EOT
SELECT * FROM persona.import_archivo_csv('$nombre', '$descripcion', $ruta, $archivo, $contitulo, $conine, '$separador', $idcatmp, $idusuario, '$columnas', '$permisos') as json;
EOT;
        $resp = $connection->fetchAll(
            $sql,
            Db::FETCH_OBJ
        );
        return $resp[0]->json;
    }

    public static function countTable($schema, $table){
        $connection = Di::getDefault()->getDb();

        $sql = <<<EOT
SELECT count(*) FROM $schema.$table;
EOT;
        $resp = $connection->fetchAll(
            $sql,
            Db::FETCH_OBJ
        );
        return $resp[0]->count;
    }

    public static function countTableIne($schema, $table, $coline){
        $connection = Di::getDefault()->getDb();

        $sql = <<<EOT
SELECT count(*) FROM $schema.$table as p1
join persona.listado_nominal as p2 on p1."INE" = clave_elector
where "$coline" is not null;
EOT;
        $resp = $connection->fetchAll(
            $sql,
            Db::FETCH_OBJ
        );
        return $resp[0]->count;
    }

    public static function countUbicadosIne($schema, $table, $coline){
        $connection = Di::getDefault()->getDb();

        $sql = <<<EOT
SELECT count(*) FROM $schema.$table as p1
join persona.listado_nominal as p2 on p1."INE" = clave_elector
where ubicado = true;
EOT;
        $resp = $connection->fetchAll(
            $sql,
            Db::FETCH_OBJ
        );
        return $resp[0]->count;
    }

    public static function createTable($nombre, $descripcion, $columnas){
        $connection = Di::getDefault()->getDb();

        $sql = <<<EOT
SELECT * FROM persona.crear_tabla('$nombre', '$descripcion', '$columnas') as json;
EOT;
        $resp = $connection->fetchAll(
            $sql,
            Db::FETCH_OBJ
        );
        return $resp[0]->json;
    }

    public static function vincularProgramado($id){
        $connection = Di::getDefault()->getDb();

        $sql = <<<EOT
SELECT * FROM persona.vincular_ln($id);
EOT;
        $resp = $connection->fetchAll(
            $sql,
            Db::FETCH_OBJ
        );
    }



    public static function getPadronInfo($lat, $lng)
    {
        $connection = Di::getDefault()->getDb();
        $sql = "select clave_elector, p_apellido_paterno, p_apellido_materno, p_nombre, 
       nombre, fecha_nacimiento, lugar_nacimiento, sexo, calles, calle, 
       cruzamiento1, cruzamiento2, numero, colonia, nombre_colonia, 
       codigo_postal, asentamiento, tipo_asentamiento, tiempo_residencia, 
       distrito_federal, distrito_local, municipio, nombre_municipio, 
       seccion, ubicado from persona.listado_nominal where st_contains(
          (select st_setsrid(geom,32616) from territorio.predios where st_contains(st_setsrid(geom,32616), st_transform(st_setsrid(st_makepoint($lng,$lat),3857),32616) ) ),
	        the_geom)";

        $resp = $connection->fetchAll(
            $sql,
            Db::FETCH_OBJ
        );

        return $resp;
    }

    public static function getPredioInfo($lat, $lng)
    {
        $connection = Di::getDefault()->getDb();
        $sql = "SELECT p.folio, p.catastral, p.calle||COALESCE('-'||p.letracalle,'') AS calle, p.numnomencl AS \"número\", c.unidadhabi AS colonia, c.localidad FROM territorio.predios p LEFT JOIN territorio.colonias c ON(p.idunidadha::int=c.idunidadha) WHERE st_contains(p.geom,st_transform(st_setsrid(st_point($lng, $lat),4326),32616))";

        $resp = $connection->fetchAll(
            $sql,
            Db::FETCH_OBJ
        );

        return $resp;
    }

    public static function getPredioPropietario($folio)
    {
        $connection = Di::getDefault()->getDb();
        $sql = "select apellido_paterno||' '||apellido_materno||' '||nombre AS nombre from persona.propietarios WHERE folio = $folio";

        $resp = $connection->fetchAll(
            $sql,
            Db::FETCH_OBJ
        );

        return $resp;
    }

    public static function getPredioHabitantes($folio)
    {
        $connection = Di::getDefault()->getDb();
        $sql = "select id, clave_elector, nombre from persona.listado_nominal WHERE folio::int = $folio";

        $resp = $connection->fetchAll(
            $sql,
            Db::FETCH_OBJ
        );

        return $resp;
    }

    public static function getPredioApoyos($folio, $tables)
    {
        $sqls = array();
        $connection = Di::getDefault()->getDb();
        
        $sql = "select string_agg(clave_elector,''',''') ines from persona.listado_nominal WHERE folio::int = $folio";
        $resp = $connection->fetchAll(
            $sql,
            Db::FETCH_OBJ
        );

        $ines = sizeof($resp) > 0? $resp[0]->ines : null;
        if(!is_null($ines)){
            $data = array();

            foreach($tables as $index=>$table){
                $sql = "SELECT * FROM ".$table->esquema.".".$table->tabla." t WHERE t.\"INE\" IN ('$ines')";

                $o = new \stdClass();
                $o->nombre = $table->nombre;

                $resp = $connection->fetchAll(
                    $sql,
                    Db::FETCH_OBJ
                );

                $o->registros = $resp;
                if(sizeof($resp) > 0){
                    array_push($data, $o);    
                }
            }
        }

        return $data;
    }

    public static function getBySQL($sql)
    {
        $connection = Di::getDefault()->getDb();
        $resp = $connection->fetchAll(
            $sql,
            Db::FETCH_OBJ
        );

        return $resp;
    }
}