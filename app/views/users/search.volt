{% if NotImplemented is defined %}
    <p> Método no implementado</p>
{% else %}
    <table class="table table-striped table-hover" id="users-datatable" style="width: 100%;">
        <thead>
        <tr>
            <th style="width: 35px;">&nbsp;</th>
            <th style="width: 35px;">Estatus</th>
            <th>Clave</th>
            <th>Usuario</th>
            <th>Nombre</th>
            <th>Perfil</th>
            <th>Creación</th>
            <th>Modificación</th>
        </tr>
        </thead>
        <tbody>
        {% for user in users %}
            <tr data-index="{{ user.id }}" id="tr-{{ user.id }}">
                <td>
                    {% if info %}
                        <button class="btn btn-sm btn-info read" title="Consultar datos del usuario" data-index="{{ user.id }}">
                            <i class="fa fa-info"></i>
                        </button>
                    {% endif %}
                    {% if edit %}
                        <button class="btn btn-sm btn-primary edit" title="Editar datos del usuario" data-index="{{ user.id }}">
                            <i class="fa fa-pencil"></i>
                        </button>
                    {% endif %}
                    {% if editProfiles %}
                        <button class="btn btn-sm btn-primary edit-profiles" title="Editar perfiles" data-index="{{ user.id }}">
                            <i class="fa fa-list"></i>
                        </button>
                    {% endif %}
                    {% if acl.isAllowedUser("users","resetPassword") %}
                        <button class="btn btn-sm btn-primary user-reset-password-do" title="Reiniciar contraseña" data-index="{{ user.id }}">
                            <i class="fa fa-key"></i>
                        </button>
                    {% endif %}
                </td>
                <td>
                    {% if user.activo == true %}
                        <i class="fa fa-check" title="Activo"></i>
                    {% else %}
                        <i class="fa fa-remove" title="Inactivo"></i>
                    {% endif %}
                </td>
                <td>{{ user.id }}</td>
                <td><div id="name-user-{{ user.id }}" data-index="{{ user.id }}">{{ user.usuario }}</div></td>
                <td>{{ user.nombre_completo }}</td>
                <td>{{ user.perfil }}</td>
                <td>{{ user.creacion }}</td>
                <td>{{ user.modificacion }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
    <script type="application/javascript">
        $(document).ready(function() {
            var hasButtons = {% if hasButtons == true %} 'B' {% else %} '' {% endif %};
            var buttons = new Array();
            {% for button in buttons %}
            var jsonButton = JSON.parse('{{ button }}');
            {% if loop.last %}
            jsonButton.className =  "vertical-divider";
            {% endif %}
            buttons.push(jsonButton);
            {% endfor %}
            usersDT = $("#users-datatable").DataTable({
                "dom": '<"clear"><"top container-float"'+hasButtons+'f><"users-scroll"rt><"clear"l><"clear"ip>',
                //            "responsive": true,
                "paging":   true,
                "info":     true,
                "processing": true,
                "pagingType": "full",
                "language": {
                    "paginate": {
                        "next": ">",
                        "first": "<<",
                        "last": ">>",
                        "previous": "<"
                    },
                    "search": "",
                    "searchPlaceholder": "Buscar en los resultados encontrados",
                    "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
                    "infoEmpty": "",
                    "infoFiltered": " - filtrado de _MAX_",
                    "emptyTable": "Sin resultados",
                    "sZeroRecords": "Sin resultados",
                    "lengthMenu": "Mostrar _MENU_ registros"
                },
                "lengthMenu": [[15, 30, 40, -1], [15, 30,40, "Todos"]],
                "lengthChange": true,
                "buttons": buttons,
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "className": 'dt-center',
                        "orderable": false,
                        "searchable": false
                    },
                    {
                        "targets": [ 1 ],
                        "className": 'dt-center',
                    },
                    {
                        "targets": [ 6, 7],
                        "searchable": false
                    },
                ]
            });
            $(".buttons-collection").attr('title', 'Mostrar/ocultar datos de tabla');
            $(".buttons-print").attr('title', 'Imprimir');
            $(".buttons-excel").attr('title', 'Exportar a excel');
            $(".pagination .first").attr('title', 'Primera página');
            $(".pagination .previous").attr('title', 'Página anterior');
            $(".pagination .next").attr('title', 'Página siguiente');
            $(".pagination .last").attr('title', 'Última página');

            $('#users-datatable tbody').off('click', '.read');
            $('#users-datatable tbody').on('click', '.read', function (e) {
                $indexuser = this.getAttribute("data-index");
                openReadUser($indexuser);
            });

            $('#users-datatable tbody').off('click', '.edit');
            $('#users-datatable tbody').on('click', '.edit', function (e) {
                $indexuser = this.getAttribute("data-index");
                openEditUser($indexuser);
            });

            $('#users-datatable tbody').off('click', '.edit-profiles');
            $('#users-datatable tbody').on('click', '.edit-profiles', function (e) {
                $indexperson = this.getAttribute("data-index");
                openEditProfilesUser($indexperson);
            });

            $('#users-datatable tbody').off('click', '.user-reset-password-do');
            $('#users-datatable tbody').on('click', '.user-reset-password-do', function (e) {
                var id = this.getAttribute("data-index");
                resetPassword(id);
            });
        });
    </script>
{% endif %}