{{ content() }}
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Usuarios</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    {% if doSearch %}
                        <div class="container-fluid" style="border: 1px solid #ddd;">
                            <div class="row">
                                <div style="font-weight: bold; ;padding: 15px 5px 5px 10px;">Selecciona tipo de búsqueda por:</div>
                                <div class="col-sm-2">
                                    <div class="form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" id="users-filter-column-select">
                                            <option value="cla">Clave</option>
                                            <option value="nom">Nombre</option>
                                            <option value="usr">Usuario</option>
                                            <!--option value="per">Perfil</option-->
                                            <option value="-1" selected="selected">Todos</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4" >
                                    <div class="form-group">
                                        <div class="input-group custom-search-form" >
                                            <div id="users-filter-clear-do" title="Limpiar">
                                                <i class="fa fa-close" style="margin-top: 10px;"></i>
                                            </div>
                                            <input type="text" class="form-control" id="users-search-val">
                                            <div class="input-group-btn">
                                                <button class="btn btn-default" type="button" id="users-search-do" style="color: #337ab7;" title="Buscar">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div><!-- /input-group -->

                                    </div>
                                </div>
                                <div class="col-sm-6" style="text-align: right;">
                                    {% if acl.isAllowedUser('users', 'add') %}
                                        <button class="btn btn-primary" type="button" title="Nuevo" style="display: inline-block; margin-bottom: 10px;" onclick="openNewUser();">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    {% endif %}
                                    {#</div>#}
                                    {#</div>#}
                                </div>

                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;" id="users-datatable-container"></div>
                        <!-- Modal -->
                        <div class="modal fade" id="user-info-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close btn-user-modal-close">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Información</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="user-toolbar">
                                            {% if acl.isAllowedUser('users', 'add') %}
                                                <button type="button" title="Nuevo" class="btn btn-primary btn-circle user-toolbar-read user-tool-new"><i class="fa fa-plus"></i>
                                                </button>
                                            {% endif %}
                                            {% if acl.isAllowedUser('users', 'edit') %}
                                                <button type="button" title="Editar" class="btn btn-primary btn-circle user-toolbar-read user-tool-edit"><i class="fa fa-pencil"></i>
                                                </button>
                                            {% endif %}
                                            <button type="button" title="Guardar" class="btn btn-primary btn-circle user-toolbar-edit user-tool-save"><i class="fa fa-floppy-o"></i>
                                            </button>
                                            <button type="button" title="Deshacer" class="btn btn-error btn-circle user-toolbar-edit user-tool-cancel"><i class="fa fa-undo"></i>
                                            </button>

                                            <button type="button" title="Siguiente" class="btn btn-primary btn-circle user-toolbar-read btn-next-user" style="float: right; margin-left: 3px;"><i class="fa fa-angle-right"></i></button>
                                            <button type="button" title="Anterior" class="btn btn-primary btn-circle user-toolbar-read btn-before-user" style="float: right;"><i class="fa fa-angle-left"></i></button>

                                            <span class="user-info-aviso"></span>
                                            <span class="user-info-error"></span>
                                        </div>
                                        <div id="user-data">
                                            <input type="hidden" id="users-modal-userid">
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <label class="control-label user-required" for="user-info-usuario">Usuario:</label>
                                                    <input type="text" class="form-control input-sm" id="user-info-usuario"/>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label class="control-label" for="user-info-clave">Clave:</label>
                                                    <input type="text" class="form-control input-sm" id="user-info-clave"/>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group-sm" >
                                                        <label class="control-label user-required" for="user-info-paterno">Ape. Paterno:</label>
                                                        <input type="text" class="form-control user-info-label text-uppercase user-field-required" id="user-info-paterno"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group-sm" >
                                                        <label class="control-label user-required" for="user-info-materno">Ape. Materno:</label>
                                                        <input type="text" class="form-control user-info-label text-uppercase user-field-required" id="user-info-materno"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" >
                                                    <div class="form-group-sm">
                                                        <label class="control-label user-required" for="user-info-nombre">Nombres:</label>
                                                        <input type="text" class="form-control user-info-label text-uppercase user-field-required" id="user-info-nombre"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label" for="user-info-correo">Correo:</label>
                                                    <input class="form-control input-sm" id="user-info-correo"/>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label class="control-label" for="user-info-tel">Teléfono:</label>
                                                    <input class="form-control input-sm" id="user-info-tel"/>
                                                </div>
                                                <div class="col-sm-2">
                                                </div>
                                            </div>

                                            {#<div class="row">#}
                                            {#<div class="col-sm-3">#}
                                            {#<label class="control-label" for="user-info-territorio">Territorio:</label>#}
                                            {#<select class="form-control input-sm" id="user-info-territorio"></select>#}
                                            {#</div>#}
                                            {#<div class="col-sm-3">#}
                                            {#<label class="control-label" for="user-info-dl">DL:</label>#}
                                            {#<input class="form-control input-sm" id="user-info-dl"/>#}
                                            {#</div>#}
                                            {#<div class="col-sm-3">#}
                                            {#<label class="control-label" for="user-info-poligono">Polígono:</label>#}
                                            {#<input class="form-control input-sm" id="user-info-poligono"/>#}
                                            {#</div>#}
                                            {#</div>#}

                                            {#<div class="row">#}
                                            {#<div class="col-sm-5">#}
                                            {#<label class="control-label" for="user-info-perfil">Perfil:</label>#}
                                            {#<select class="form-control input-sm" id="user-info-perfil"></select>#}
                                            {#</div>#}
                                            {#</div>#}

                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-sm-3">
                                                    Activo
                                                    <div class="onoffswitch">
                                                        <input type="checkbox" checked class="onoffswitch-checkbox" id="user-info-activo">
                                                        <label class="onoffswitch-label" for="user-info-activo">
                                                            <span class="onoffswitch-inner"></span>
                                                            <span class="onoffswitch-switch"></span>
                                                        </label>
                                                    </div>

                                                    {#<label class="control-label" for="user-info-activo">#}
                                                        {#<input type="checkbox" id="user-info-activo"> activo#}
                                                    {#</label>#}
                                                </div>
                                                <div class="col-sm-3">
                                                    {#{% if resetPass %}#}
                                                        {#<button class="btn btn-sm btn-primary" id="user-reset-password-do" title="Reiniciar contraseña">#}
                                                            {#<i class="fa fa-refresh"></i> Reiniciar password#}
                                                        {#</button>#}
                                                    {#{% endif %}#}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-user-modal-close">Cerrar</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                        <div class="modal fade" id="modal-user-profile-confirm" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">¿Desea agregar un perfil?</h4>
                                    </div>
                                    <div class="modal-body" style="text-align: right;">
                                        <input type="hidden" id="user-profile-id">
                                        <button type="button" title="No aplicar encuesta" id="user-profile-confirm-no" class="btn btn-error"> No
                                        </button>
                                        <button type="button" title="Aplicar encuesta" id="user-profile-confirm-yes" class="btn btn-primary"> Sí
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="user-edit-proflies-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel2" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel2">Perfiles</h4>
                                    </div>
                                    <div class="modal-body" id="user-edit-proflies-body" style="max-height: 400px; overflow-y: auto;"></div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" id="user-edit-profiles-save">Guardar</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <link href="css/users.css" rel="stylesheet">
                        <script type="application/javascript" src="../js/users-events.js"></script>
                    {% else %}
                        <p>No cuenta con el permiso para realizar búsquedas, contacte al administrador.</p>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</div>