<input type="hidden" id="user-edit-profiles-id" value="{{ iduser }}">
{% for profile in profiles %}
    <div class="checkbox" style="font-weight: 400; cursor: pointer; margin-top: 0px;">
        <label>
            <input type="checkbox" value="{{ profile.id }}" {% if profile.checked %} checked {% endif %}> {{ profile.nombre }}
        </label>
    </div>
{% endfor %}
<div id="user-edit-profiles-msj"></div>
<script type="application/javascript">
    $(document).ready(function() {
        $("#user-edit-profiles-save").off("click");
        $("#user-edit-profiles-save").on("click", function(){
            $("#user-edit-profiles-msj").html("");
            var inputs = $("#user-edit-proflies-body").find("input[type=checkbox]:checked");
            if(inputs.length > 0){
                var ids = new Array();
                var idusuario = $("#user-edit-profiles-id").val();
                inputs.each(function(){
                    ids.push($(this).val());
                });
                $.ajax({
                    url: "/users/saveProfiles",
                    type: "POST",
                    data: JSON.stringify({ids: ids, idusuario: idusuario}),
                    success: function(resp){
                        var jsonResp = JSON.parse(resp);
                        if(usersDT){
                            var $tr = $("tr#tr-"+idusuario);
                            var data = usersDT.row($tr).data();
                            data[5] = jsonResp.perfiles;
                            usersDT.row($tr).data(data).draw(false);
                        }
                        $("#user-edit-profiles-msj").html("<div class='has-success'>" +
                            "<label class='control-label'>La información se guardo correctamente</label>" +
                        "</div>");
                        setTimeout(function(){
                            $("#user-edit-profiles-msj").html("");
                        }, 2000);
                    },
                    error: function(){
                        $("#user-edit-profiles-msj").html("<div class='has-error'>" +
                            "<label class='control-label'>Ocurrió un error al guardar la información</label>" +
                        "</div>");
                    }
                });
            }
            else{
                $("#user-edit-profiles-msj").html("<div class='has-error'>" +
                        "<label class='control-label'>Debe seleccionar almenos uno</label>" +
                        "</div>");
            }
        });
    });
</script>