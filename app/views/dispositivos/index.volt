<style>
    .filtro-clave-clear-do,
    .filtro-dispositivo-clear-do {
        position: absolute;
        right: 15px;
        z-index: 10;
        top: 25px;
        height: 33px;
        width: 30px;
        text-align: center;
        cursor: pointer;
    }

    .cliente-modal-header {
        padding: 15px !important;
    }
</style>
<!-- View Clientes -->
{{ content() }}

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>DISPOSITIVOS</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">



                    <!-- filtros -->
                    <div>
                        <nav class="navbar navbar-default" >
                            <div>
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header" style="float: none; display: none;">
                                    <button type="button" id="colapse-busqueda" class="navbar-toggle" data-toggle="collapse"
                                            data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
                                </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="navbar-collapse collapse in clientes-only" id="contenedor-filtros" aria-expanded="true" style="border-color: #FFFFFF;">
                                    {#<form id="frmPersonas" name="frmPersonas" class="navbar-form navbar-right" role="search" style="border: none;">#}
                                    <div class="nav-bar">
                                        {#<div style="font-weight: bold; ;padding: 15px 5px 5px 10px;">Selecciona tipo de búsqueda por:</div>#}
                                        <!-- filtros -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Buscar por</label>
                                                <select class="form-control" id="ftipobusqueda">
                                                    <option value="dispositivo" selected>Dispositivo</option>
                                                    <option value="cobratario">Cobratario</option>
                                                    <option value="clave">ID</option>
                                                    {#<option value="">Todos</option>#}
                                                </select>
                                            </div>
                                        </div>
                                        <!-- vigente -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Activo</label>
                                                <select class="form-control" id="fvigente" style="width: 100%;">
                                                    <option value="">Todos</option>
                                                    <option value="true">Si</option>
                                                    <option value="false">No</option>
                                                </select>
                                            </div>
                                        </div>


                                        <!-- Dispositivo -->
                                        <div class="col-md-6" id="filtro-dispositivo">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <div class="filtro-dispositivo-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="text" class="form-control" id="fdispositivo" placeholder="Escriba el dispositivo">
                                            </div>
                                        </div>
                                        <!-- clave -->
                                        <div class="col-md-6" style="display: none" id="filtro-clave">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <div class="filtro-clave-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Números" id="fclave" data-inputmask="'mask': '9', 'repeat': 10, 'greedy' : false">
                                            </div>
                                        </div>
                                        <!-- usuario -->
                                        <div class="col-sm-6" style="display: none" id="filtro-cobratario">
                                            <label for="fcobratario">Cobratario</label>
                                            <select class="form-control" id="fcobratario" style="width: 100%;">
                                                <option value="">Todos</option>
                                                {% for cobratario in cobratarios %}
                                                    <option value="{{ cobratario.id }}">{{ cobratario.usuario }}</option>
                                                {% endfor %}
                                            </select>
                                        </div>
                                        <!-- boton search -->
                                        <div class="col-md-1">
                                            <label>&nbsp;</label>
                                            <button class="btn btn-primary btn-block" id="dispositivo-search-do" style="margin-bottom: 10px;" type="button" title="Buscar">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                        <!-- boton nuevo -->
                                        <div class="col-md-1">
                                            <div class="" style="text-align: right;">
                                                <label>&nbsp;</label>
                                                {% if acl.isAllowedUser('dispositivos', 'new') %}
                                                    <button class="btn btn-primary btn-block" title="Nuevo dispositivo" style="display: inline-block; margin-bottom: 10px;" id="addDispositivoDo">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                {% endif %}
                                            </div>
                                        </div>
                                    </div>
                                    {#</form>#}

                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->

                        </nav>
                    </div>




                    <div style="margin-top: 15px;">
                        <table class="display table-striped table-hover" id="dispositivos-datatable" style="width: 100%;">
                            <thead>
                            <tr>
                                <th data-priority="1"></th>
                                <th data-priority="2"></th>
                                <th data-priority="5">Activo</th>
                                <th data-priority="6">ID</th>
                                <th data-priority="3">Dispositivo</th>
                                <th data-priority="4">Usuario</th>
                                <th data-priority="7">Descripción</th>
                                <th>idusuario</th>
                                <th>activo</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal dispositivo -->
<div class="modal inmodal fade" id="dispositivo-info-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header cliente-modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Nuevo dispositivo</h4>
            </div>

            <div class="modal-body">
                <input type="hidden" id="id">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Dispositivo *</label>
                            <input id="dispositivo" type="text" placeholder="Dispositivo" class="form-control rs-required">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Cobratario *</label>
                            <select class="form-control" id="cobratario" style="width: 100%;">
                                <option value="">Seleccionar</option>
                                {% for cobratario in cobratarios %}
                                    <option value="{{ cobratario.id }}">{{ cobratario.nombre }} {{ cobratario.apellido_paterno }} {{ cobratario.apellido_materno }}</option>
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Descripción</label>
                            <textarea class="form-control" style="resize: none;" rows="5" id="descripcion"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnGuardar" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal dispositivo seguimiento -->
<div class="modal inmodal fade" id="dispositivo-usuarios-info-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header cliente-modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Nuevo dispositivo</h4>
            </div>

            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <table class="display table-striped table-hover" id="dispositivos-seguimiento-datatable" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th data-priority="1">Usuario</th>
                                    <th data-priority="2">Fecha creación</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- view content -->
<script>
    coacciones = '{{ coacciones }}';
</script>
{{ javascript_include("js/dispositivos/index-events.js") }}