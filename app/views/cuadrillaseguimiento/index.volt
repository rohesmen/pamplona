<style>
    .filtro-cuadrillaseguimiento-clear-do {
        position: absolute;
        right: 15px;
        z-index: 10;
        top: 25px;
        height: 33px;
        width: 30px;
        text-align: center;
        cursor: pointer;
    }
</style>
<!-- View cuadrilla -->
{{ content() }}

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>CUADRILLAS</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- filtros -->
                    <div>
                        <nav class="navbar navbar-default" >
                            <div>
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header" style="float: none; display: none;">
                                    <button type="button" id="colapse-busqueda" class="navbar-toggle" data-toggle="collapse"
                                            data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
                                </div>
                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="navbar-collapse collapse in clientes-only" id="contenedor-filtros" aria-expanded="true" style="border-color: #FFFFFF;">
                                    <div class="nav-bar">
                                        <!-- activo -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Activo</label>
                                                <select class="form-control" id="fvigente" style="width: 100%;">
                                                    <option value="">Todos</option>
                                                    <option value="true">Si</option>
                                                    <option value="false">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- nombre -->
                                        <div class="{{ acl.isAllowedUser('cuadrillaseguimiento', 'create') ?  'col-md-8' : 'col-md-9' }}">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <div class="filtro-cuadrillaseguimiento-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="text" class="form-control Filt" id="fcuadrillaseguimiento" placeholder="Escriba el nombre de la cuadrilla">
                                            </div>
                                        </div>
                                        <!-- boton search -->
                                        <div class="col-md-1">
                                            <label>&nbsp;</label>
                                            <button class="btn btn-primary btn-block" id="cuadrillaseguimiento-search-do" style="margin-bottom: 10px;" type="button" title="Buscar">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                        {% if acl.isAllowedUser('cuadrillaseguimiento', 'create') %}
                                            <!-- boton nuevo -->
                                            <div class="col-md-1">
                                                <div class="" style="text-align: right;">
                                                    <label>&nbsp;</label>
                                                    <button class="btn btn-primary btn-block" title="Crear" style="display: inline-block; margin-bottom: 10px;" id="addCuadrillaSeguimientoDo">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        {% endif %}
                                    </div>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->
                        </nav>
                    </div>
                    <div style="margin-top: 15px;">
                        <table class="display table-striped table-hover" id="cuadrillaseguimiento-datatable" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th data-priority="2"></th>
                                    <th data-priority="1"></th>
                                    <th data-priority="4">Activo</th>
                                    <th data-priority="5">ID</th>
                                    <th data-priority="3">Nombre</th>
                                    <th data-priority="6">Fecha Creación</th>
                                    <th data-priority="7">Fecha Modificacion</th>
                                    <th>activo</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- view content -->
<script>
    coacciones = '{{ coacciones }}';
</script>

{{ javascript_include("js/cuadrillaseguimiento/index-events.js?time="~time()) }}

{{ partial("cuadrillaseguimiento/form", ["mode": "create"]) }}
