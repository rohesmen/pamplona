<style>
    .cuadrillaseguimiento-modal-header {
        padding: 15px !important;
    }
    #cuadrillaseguimiento-info-modal .popover {
        z-index: 10000;
    }
    .item-divider{
        height: 1px;
        border: 1px solid #BDBDBD !important;
        margin-bottom: 10px !important;
    }
    .disnone{
        display: none;
    }
    .onoffswitch {
        width: 90px;
    }
    .onoffswitch-inner:before {
        content: "Chofer" !important;
    }
    .onoffswitch-inner:after {
        content: "Recolector" !important;
    }
    .onoffswitch-switch {
        right: 72px;
    }
</style>
<!-- Modal cuadrilla seguimiento -->
<div class="modal inmodal fade" id="cuadrillaseguimiento-info-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header cuadrillaseguimiento-modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Nueva Cuadrilla</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="idCUADSEG">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Nombre *</label>
                            <input id="txtNombreCUADSEG" type="text" placeholder="Nombre" class="form-control rs-required">
                        </div>
                    </div>
                    <div class="item-divider {{ acl.isAllowedUser('cuadrillaseguimiento', 'addcuadrec') ?  '' : 'disnone' }}"></div>
                    <div class="row {{ acl.isAllowedUser('cuadrillaseguimiento', 'addcuadrec') ?  '' : 'disnone' }}">
                        <div class="{{ acl.isAllowedUser('cuadrillaseguimiento', 'addcuadrec') ?  'col-md-9' : 'col-md-10' }} form-group">
                            <label>Recolector *</label>
                            <select class="form-control" id="cmbRecolectoCUADSEG" style="width: 100%;">
                                <option value="">Seleccionar</option>
                                {% for data in recolectores %}
                                    <option value="{{ data.id }}">{{ data.nombres|upper }} {{ data.apepat|upper }} {{ data.apemat|upper }}</option>
                                {% endfor %}
                            </select>
                        </div>
                        <div class="col-md-2">
                            ¿Chofer? *
                            <div class="onoffswitch">
                                <input type="checkbox" class="onoffswitch-checkbox" id="chkIsChoferCUADSEG">
                                <label class="onoffswitch-label" for="chkIsChoferCUADSEG">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                        {% if acl.isAllowedUser('cuadrillaseguimiento', 'addcuadrec') %}
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-block btn-info" id="create_cuadrilla_recoleccion" title="Agregar">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        {% endif %}
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <table class="display table-striped table-hover" id="cuadrilla_recoleccion-datatable" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th data-priority="1"></th>
                                    <th data-priority="2"></th>
                                    <th data-priority="3">Recolector</th>
                                    <th data-priority="4">Tipo</th>
                                    <th>idrecolector</th>
                                    <th>ischofer</th>
                                    <th>activo</th>
                                    <th>id</th>
                                    <th>edicion</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnGuardarCUADSEG" type="button" class="btn btn-primary" title="Guardar">Guardar</button>
            </div>
        </div>
    </div>
</div>
<script>
    deletecuadrec = false;
</script>

{% if acl.isAllowedUser('cuadrillaseguimiento', 'deletecuadrec') %}
    <script>
        deletecuadrec = true;
    </script>
{% endif %}

{{ javascript_include("plugins/bootbox/bootbox.min.js") }}
{{ javascript_include("js/cuadrillaseguimiento/save-events.js?time="~time()) }}
