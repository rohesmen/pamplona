<style>
    .filtro-unidadseguimiento-clear-do {
        position: absolute;
        right: 15px;
        z-index: 10;
        top: 25px;
        height: 33px;
        width: 30px;
        text-align: center;
        cursor: pointer;
    }
    .unidadseguimiento-modal-header {
        padding: 15px !important;
    }
    #unidadseguimiento-info-modal .popover {
        z-index: 10000;
    }
    .item-divider{
        height: 1px;
        border: 1px solid #BDBDBD !important;
        margin-bottom: 10px !important;
    }
    .disnone{
        display: none;
    }
</style>
<!-- View Clientes -->
{{ content() }}

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>UNIDADES</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- filtros -->
                    <div>
                        <nav class="navbar navbar-default" >
                            <div>
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header" style="float: none; display: none;">
                                    <button type="button" id="colapse-busqueda" class="navbar-toggle" data-toggle="collapse"
                                            data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
                                </div>
                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="navbar-collapse collapse in clientes-only" id="contenedor-filtros" aria-expanded="true" style="border-color: #FFFFFF;">
                                    <div class="nav-bar">
                                        <!-- activo -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Activo</label>
                                                <select class="form-control" id="fvigente" style="width: 100%;">
                                                    <option value="">Todos</option>
                                                    <option value="true">Si</option>
                                                    <option value="false">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- nombre -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <div class="filtro-unidadseguimiento-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="text" class="form-control Filt" id="fnombre" placeholder="Nombre de la unidad">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <div class="filtro-unidadseguimiento-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="text" class="form-control Filt" id="fplaca" placeholder="Placa de la unidad">
                                            </div>
                                        </div>
                                        <div class="{{ acl.isAllowedUser('unidadseguimiento', 'create') ?  'col-md-2' : 'col-md-3' }}">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <div class="filtro-unidadseguimiento-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="text" class="form-control Filt" id="fnoeconomico" placeholder="No. Economico de la unidad">
                                            </div>
                                        </div>
                                        <!-- boton search -->
                                        <div class="col-md-1">
                                            <label>&nbsp;</label>
                                            <button class="btn btn-primary btn-block" id="unidadseguimiento-search-do" style="margin-bottom: 10px;" type="button" title="Buscar">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                        {% if acl.isAllowedUser('unidadseguimiento', 'create') %}
                                            <!-- boton nuevo -->
                                            <div class="col-md-1">
                                                <div class="" style="text-align: right;">
                                                    <label>&nbsp;</label>
                                                    <button class="btn btn-primary btn-block" title="Crear" style="display: inline-block; margin-bottom: 10px;" id="addunidadseguimientoDo">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        {% endif %}
                                    </div>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->
                        </nav>
                    </div>
                    <div style="margin-top: 15px;">
                        <table class="display table-striped table-hover" id="unidadseguimiento-datatable" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th data-priority="2"></th>
                                    <th data-priority="1"></th>
                                    <th data-priority="6">Activo</th>
                                    <th data-priority="3">Nombre</th>
                                    <th data-priority="4">Placa</th>
                                    <th data-priority="5">No. Economico</th>
                                    <th data-priority="7">Fecha Creación</th>
                                    <th data-priority="8">Fecha Modificacion</th>
                                    <th>activo</th>
                                    <th>descripcion</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal unidad seguimiento -->
<div class="modal inmodal fade" id="unidadseguimiento-info-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header unidadseguimiento-modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Nueva Unidad</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label>Nombre *</label>
                            <input id="txtNombre" type="text" placeholder="Nombre *" class="form-control rs-required txtDis">
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Placa *</label>
                            <input id="txtPlaca" type="text" placeholder="Placa *" class="form-control rs-required txtDis">
                        </div>
                        <div class="col-md-4 form-group">
                            <label>No. Economico</label>
                            <input id="txtNoEconomico" type="text" placeholder="No. Economico" class="form-control rs-required txtDis">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Descripcion</label>
                            <textarea id="txtDescripcion" name="txtDescripcion" rows="8" placeholder="Descripcion" class="form-control rs-required txtDis" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <div class="item-divider  {{ acl.isAllowedUser('unidadseguimiento', 'addcuadunid') ?  '' : 'disnone' }}"></div>
                    <div class="row {{ acl.isAllowedUser('unidadseguimiento', 'addcuadunid') ?  '' : 'disnone' }}">
                        <div class="{{ acl.isAllowedUser('unidadseguimiento', 'addcuadunid') ?  'col-md-11' : 'col-md-12' }} form-group">
                            <label>Cuadrilla *</label>
                            <select class="form-control" id="cmbCuadrilla" style="width: 100%;">
                                <option value="">Seleccionar</option>
                                {% for data in cuadrillas %}
                                    <option value="{{ data.id }}">{{ data.nombre|upper }}</option>
                                {% endfor %}
                            </select>
                        </div>
                        {% if acl.isAllowedUser('unidadseguimiento', 'addcuadunid') %}
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-block btn-info" id="create_cuadrilla_unidad" title="Agregar">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        {% endif %}
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <table class="display table-striped table-hover" id="cuadrilla_unidad-datatable" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th data-priority="1"></th>
                                    <th data-priority="2"></th>
                                    <th data-priority="3">Cuadrilla</th>
                                    <th>idcuadrilla</th>
                                    <th>activo</th>
                                    <th>id</th>
                                    <th>placa</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnGuardar" type="button" class="btn btn-primary" title="Guardar">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- view content -->
<script>
    coacciones = '{{ coacciones }}';
    deletecuadunid = false;
</script>
{% if acl.isAllowedUser('unidadseguimiento', 'deletecuadunid') %}
    <script>
        deletecuadunid = true;
    </script>
{% endif %}
{{ javascript_include("js/unidadseguimiento/index-events.js") }}