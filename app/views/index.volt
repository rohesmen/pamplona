<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
	<title>SISTEMA PAMPLONA</title>

	{{ stylesheet_link('css/bootstrap/bootstrap.min.css') }}
	{{ stylesheet_link('css/font-awesome/font-awesome.min.css') }}
	{#<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.min.css" type="text/css" >#}

	<!-- Toastr style -->
	{{ stylesheet_link('plugins/toastr/toastr.min.css') }}

	<!-- Input Mask-->
	{{ stylesheet_link('plugins/jasny-bootstrap/jasny-bootstrap.min.css') }}

	<!-- DataTables -->
	{#<link rel="stylesheet" href="plugins/datatables/jquery.dataTables.min.css">#}
	{#<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">#}
	{{ stylesheet_link('plugins/datatables_new/Buttons-1.2.2/css/buttons.dataTables.css') }}
	{#<link rel="stylesheet" href="plugins/datatables/extensions/Responsive/css/dataTables.responsive.css">#}
	{#<link rel="stylesheet" href="plugins/datatables/extensions/ColVis/css/dataTables.colVis.min.css">#}

	{{ stylesheet_link('plugins/datatables_new/DataTables-1.10.12/css/jquery.dataTables.min.css') }}
	{{ stylesheet_link('plugins/datatables_new/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}
	{{ stylesheet_link('plugins/datatables_new/Responsive-2.1.0/css/responsive.dataTables.min.css') }}


	<!-- Selectize -->
	{{ stylesheet_link('plugins/selectize/selectize.bootstrap3.css') }}
	{{ stylesheet_link('plugins/selectize/selectize.clearbutton.css') }}

	<!-- Datetimepicker -->
	{{ stylesheet_link('plugins/datetime-picker/bootstrap-datetimepicker.min.css') }}

	{{ stylesheet_link('css/animate.css') }}
	{{ stylesheet_link('css/inspinia.css') }}
	{{ stylesheet_link('css/style.css') }}




	<!-- Mainly scripts -->
	{{ javascript_include("plugins/jQuery/jQuery-2.1.4.min.js") }}
	{{ javascript_include("js/bootstrap/bootstrap.min.js") }}
	{{ javascript_include("plugins/metisMenu/metisMenu.min.js") }}
	{{ javascript_include("plugins/slimScroll/jquery.slimscroll.min.js") }}

	<!-- Bootstrap confirmation -->
	{{ javascript_include("plugins/bootstrap-confirmation/bootstrap-confirmation.min.js") }}

	<!-- jQuery UI -->
	{{ javascript_include("plugins/slimScroll/jquery.slimscroll.min.js") }}

	<!--jQuery shotcuts-->
	{{ javascript_include("plugins/jQuery/shortcut.js") }}

	<!-- Toastr -->
	{{ javascript_include("plugins/toastr/toastr.min.js") }}

	{#<script src="plugins/jasny-bootstrap/jasny-bootstrap.min.js"></script>#}

	<!-- DataTables -->
	{#<script src="plugins/datatables/jquery.dataTables.min.js"></script>#}
	{#<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>#}

	{#<script src="plugins/datatables_new/extensions/Buttons/js/buttons.print.min.js"></script>#}
	{#<script src="plugins/datatables_new/extensions/Buttons/js/buttons.html5.min.js"></script>#}
	{#<script src="plugins/datatables_new/extensions/jszip.min.js"></script>#}
	{#<script src="plugins/datatables_new/extensions/pdfmake.min.js"></script>#}
	{#<script src="plugins/datatables_new/extensions/vfs_fonts.js"></script>#}
	{#<script src="plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>#}

	<!-- DataTables new version -->
	{{ javascript_include("plugins/datatables_new/DataTables-1.10.12/js/jquery.dataTables.min.js") }}
	{{ javascript_include("plugins/datatables_new/DataTables-1.10.12/js/dataTables.bootstrap.min.js") }}
	{{ javascript_include("plugins/datatables_new/Responsive-2.1.0/js/dataTables.responsive.min.js") }}

	{#<script src="plugins/datatables_new/Buttons-1.2.2/js/buttons.bootstrap.js"></script>#}
	{#<script src="plugins/datatables_new/Buttons-1.2.2/js/buttons.html5.js"></script>#}
	{#<script src="plugins/datatables_new/Buttons-1.2.2/js/buttons.colVis.js"></script>#}
    {{ javascript_include("plugins/datatables_new/date-eu.js") }}
    {{ javascript_include("plugins/datatables_new/date-euro.js") }}
    {{ javascript_include("plugins/datatables_new/date-de.js") }}



	<!-- Selectize -->
	{{ javascript_include("plugins/selectize/standalone/selectize.min.js") }}
	{{ javascript_include("plugins/selectize/selectize.clearbutton.js") }}

	<!-- Datetimepicker -->
	{{ javascript_include("plugins/datetime-picker/moment-with-locales.min.js") }}
	{{ javascript_include("plugins/datetime-picker/bootstrap-datetimepicker.min.js") }}

	<!-- Custom and plugin javascript -->
	{{ javascript_include("js/inspinia.js") }}
	{{ javascript_include("plugins/pace/pace.js") }}
	{{ javascript_include("js/common.js") }}

	{#<script src="plugins/Inputmask-2.x/dist/jquery.inputmask.bundle.js"></script>#}
	{{ javascript_include("plugins/Inputmask-2.x/jquery.inputmask.bundle.js") }}
	{#<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>#}
	{#<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>#}


	{{ javascript_include("plugins/xlsx/xlsx.full.min.js") }}
	{{ javascript_include("plugins/xlsx/FileSaver.min.js") }}

	<script>
        TOKEN = '{{ TOKEN }}';
		$(document).ready(function() {
			/*setTimeout(function () {
			 toastr.options = {
			 closeButton: true,
			 progressBar: true,
			 showMethod: 'slideDown',
			 timeOut: 4000
			 };
			 toastr.success('{{ identity['welcomeName'] }}', 'Bienvenido a EVANDER');

			 }, 1300);*/

            // if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
            //     setInterval(function () {
            //         if (navigator.geolocation) {
            //             navigator.geolocation.getCurrentPosition(enviarPosicionActual,
				// 			function(PositionError){
            //                 console.log("no location 1");
            //             });
            //         }
            //         else{
            //             console.log("no location");
            //         }
            //     }, 3000);
            // }
            // else {
            //
            // }

		});


        function enviarPosicionActual(position){
            var latitud = null;
            var longitud = null;
            if(position){
                latitud = position.coords.latitude;
                longitud = position.coords.longitude;
            }
            var data = {
                lat: latitud,
				lng: longitud
			}

            //Invocar ajax para realizar el guardado
            $.ajax({
                //url: '/caja/guardarHistRecolecta',
                url: '/apicob/prueba',
                type: 'POST',
                dataType: "json",
                data: $.param(data),
                success: function(json){
                    console.log("todo bien");
                },
                complete: function () {$('#load-save-recolecta').html("");}
            });
            return boSuccess;
        }
	</script>
</head>
<body>
<input style="display:none">
<input type="password" style="display:none">
{{ content() }}

<!-- Input Mask-->

</body>
</html>