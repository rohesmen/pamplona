<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PAMPLONA | Seguimiento en vivo</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
    <!-- Bootstrap 3.3.5 -->
    <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome/font-awesome.min.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/inspinia.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/pamplona/seguimiento.css" rel="stylesheet">
    <link href="plugins/ol3/ol.css" rel="stylesheet">
    <style type="text/css">
        body {
            height: 100%;
        }
        div.fill {
            width: 100%;
            height: 100%;
        }
        .ol-scale-line {
            bottom: 30px !important;
        }
    </style>


    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="js/bootstrap/bootstrap.min.js"></script>

    <script src="plugins/pace/pace.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.js"></script>
    <![endif]-->
</head>
<body class="gray-bg pace-done">
    <div id="wrapper">
        <div style="background-color: #EE6B03;">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <div class="collapsed navbar-toggle-pamplona">
                            <p class="navbar-text">Seguimiento</p>
                        </div>
                        <a class="navbar-brand" href="#">
                            <img alt="Logo" src="img/logo_pamplona.png">
                        </a>
                    </div>
                    {#<p class="navbar-text">Seguimiento en vivo</p>#}
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-4">
                        <p class="navbar-text">Seguimiento en vivo</p>
                    </div>
                </div>
            </nav>
        </div>
        <div class="container-fluid" style="margin-top: 15px;">
            <div class="row">
                <div class="col-xs-12">
                    <div id="map" class="map">
                        <div id="gmap" class="fill"></div>
                        <div id="olmap" class="fill"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZJImFXCY1MX5n3_OVhXg7Jgb9zBbLlLg"></script>
    <script src="plugins/ol3/ol.js"></script>
    <script type="application/javascript">
        $(window).resize(function(){
            setHeightMap();
        });
        $(document).ready(function(){
            setHeightMap();
            gmap = new google.maps.Map(document.getElementById('gmap'), {
                disableDefaultUI: true,
                keyboardShortcuts: false,
                draggable: false,
                disableDoubleClickZoom: true,
                scrollwheel: true,
                streetViewControl: true,
//                mapTypeControl: true,
//                scaleControl: true,
//                zoomControl: true,
            });

            var view = new ol.View({
                // make sure the view doesn't go beyond the 22 zoom levels of Google Maps
                maxZoom: 21
            });
            view.on('change:center', function() {
                var center = ol.proj.transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');
                gmap.setCenter(new google.maps.LatLng(center[1], center[0]));
            });
            view.on('change:resolution', function() {
                gmap.setZoom(view.getZoom());
            });

            var iconFeature = new ol.Feature({
                geometry: new ol.geom.Point([0, 0]),
                name: 'Null Island',
                population: 4000,
                rainfall: 500
            });

            var iconStyle = new ol.style.Style({
                image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
                    anchor: [0.5, 46],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'pixels',
                    opacity: 0.75,
                    src: 'http://openlayers.org/en/v3.7.0/examples/data/icon.png'
                }))
            });

            iconFeature.setStyle(iconStyle);

            var vectorSource = new ol.source.Vector({
                features: [iconFeature]
            });

            var vector = new ol.layer.Vector({
                source: vectorSource
            });

            var olMapDiv = document.getElementById('olmap');
            map = new ol.Map({
                layers: [vector],
                controls: ol.control.defaults().extend([
                    new ol.control.ScaleLine()
                ]),
                interactions: ol.interaction.defaults({
                    altShiftDragRotate: false,
                    dragPan: false,
                    rotate: false
                }).extend([new ol.interaction.DragPan({kinetic: null})]),
                target: olMapDiv,
                view: view
            });
            view.setCenter([0, 0]);
            view.setZoom(1);

            olMapDiv.parentNode.removeChild(olMapDiv);
            gmap.controls[google.maps.ControlPosition.TOP_LEFT].push(olMapDiv);
        });
        function setHeightMap(){
            var heightWindow = window.innerHeight;
            $("#map").height(heightWindow - 87);
            google.maps.event.trigger(gmap, 'resize');
//            map.updateSize();
        }
    </script>
</body>
</html>
