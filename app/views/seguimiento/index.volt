<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PAMPLONA | Seguimiento en vivo</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
    <!-- Bootstrap 3.3.5 -->
    <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome/font-awesome.min.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/inspinia.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/pamplona/seguimiento.css" rel="stylesheet">
    <style type="text/css">
        body {
            height: 100%;
        }
        div.fill {
            width: 100%;
            height: 100%;
        }
        .ol-scale-line {
            bottom: 30px !important;
        }
    </style>


    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="js/bootstrap/bootstrap.min.js"></script>

    <script src="plugins/pace/pace.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.js"></script>
    <![endif]-->
</head>
<body class="gray-bg pace-done">
    <div id="wrapper">
        <div style="background-color: #EE6B03;">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <div class="collapsed navbar-toggle-pamplona">
                            <p class="navbar-text">Seguimiento</p>
                        </div>
                        <a class="navbar-brand" href="#">
                            <img alt="Logo" src="img/logo_pamplona.png">
                        </a>
                    </div>
                    {#<p class="navbar-text">Seguimiento en vivo</p>#}
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-4">
                        <p class="navbar-text">Seguimiento en vivo</p>
                    </div>
                </div>
            </nav>
        </div>
        <div class="container-fluid" style="margin-top: 15px;">
            <div class="row">
                <div class="col-xs-12">
                    <div id="map" class="map"></div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZJImFXCY1MX5n3_OVhXg7Jgb9zBbLlLg"></script>
    <script type="application/javascript">
        CENTER_MAP_LAT = 21.02394395, CENTER_MAP_LON = -89.64514160,initZoom = 13;
//        -89.64514160,21.02394395
        {#marcadores = {{ marcadores }};#}
        locations = [];
        var rutaPamplona =
        $(window).resize(function(){
            setHeightMap();
        });
        $(document).ready(function(){
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: CENTER_MAP_LAT, lng: CENTER_MAP_LON},
                zoom: initZoom,
                disableDefaultUI: true,
                keyboardShortcuts: false,
//                draggable: false,
//                disableDoubleClickZoom: true,
                scrollwheel: true,
                streetViewControl: true,
                mapTypeControl: true,
                scaleControl: true,
                zoomControl: true,
            });
            map.data.loadGeoJson('/js/rutapamplona.json');
            map.data.setStyle({
                strokeColor: '#EE6B03',
                strokeWeight: 2
            });
//            -89.80181885992522,20.694881965925894,-89.44696059406462,21.196658059527174

            var strictBounds = new google.maps.LatLngBounds(
                    new google.maps.LatLng(20.694881965925894, -89.80181885992522),
                    new google.maps.LatLng(21.196658059527174, -89.44696059406462));

            // Listen for the dragend event
            google.maps.event.addListener(map, 'dragend', function () {
                if (strictBounds.contains(map.getCenter())) return;

                // We're out of bounds - Move the map back within the bounds

                var c = map.getCenter(),
                        x = c.lng(),
                        y = c.lat(),
                        maxX = strictBounds.getNorthEast().lng(),
                        maxY = strictBounds.getNorthEast().lat(),
                        minX = strictBounds.getSouthWest().lng(),
                        minY = strictBounds.getSouthWest().lat();

                if (x < minX) x = minX;
                if (x > maxX) x = maxX;
                if (y < minY) y = minY;
                if (y > maxY) y = maxY;

                map.setCenter(new google.maps.LatLng(y, x));
            });
            var centerControlDiv = document.createElement('div');
            var centerControl = new CenterControl(centerControlDiv, map);

            centerControlDiv.index = 1;
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);

            setHeightMap();
            var marcadores = {{ marcadores }};
            addMarcadores(marcadores);
            setInterval(function(){
                $.ajax({
                    url: "/seguimiento/lastlocation",
                    success: function(resp){
                        var marcadores = JSON.parse(resp);
                        addMarcadores(marcadores);
                    }
                });
            }, 5000);
        });

        function CenterControl(controlDiv, map) {

            // Set CSS for the control border.
            var controlUI = document.createElement('div');
            controlUI.style.backgroundColor = '#fff';
            controlUI.style.border = '2px solid #fff';
            controlUI.style.borderRadius = '3px';
            controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
            controlUI.style.cursor = 'pointer';
            controlUI.style.marginBottom = '22px';
            controlUI.style.textAlign = 'center';
            controlUI.title = 'Click to recenter the map';
            controlDiv.appendChild(controlUI);

            // Set CSS for the control interior.
            var controlText = document.createElement('div');
            controlText.style.color = 'rgb(25,25,25)';
            controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
            controlText.style.fontSize = '16px';
            controlText.style.lineHeight = '38px';
            controlText.style.paddingLeft = '5px';
            controlText.style.paddingRight = '5px';
            controlText.innerHTML = 'Centrar mapa';
            controlUI.appendChild(controlText);

            // Setup the click event listeners: simply set the map to Chicago.
            controlUI.addEventListener('click', function() {
                map.setCenter({lat: CENTER_MAP_LAT, lng: CENTER_MAP_LON});
                map.setZoom(initZoom);
            });

        }

        function addMarcadores(marcadores){
            for(var i in marcadores){
                var marcador = marcadores[i];
                if(locations[marcador.vehiculo] && locations[marcador.vehiculo].marker){
                    var actualLat = locations[marcador.vehiculo].latitud;
                    var actualLng = locations[marcador.vehiculo].longitud;
                    if(actualLat == marcador.latitud  && actualLng == marcador.longitud){
                        continue;
                    }
                    locations[marcador.vehiculo].marker.setMap(null);
                    locations[marcador.vehiculo].marker = null;
                }
                locations[marcador.vehiculo] = marcador;
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(marcador.latitud, marcador.longitud),
                    icon: "img/seguimiento.png",
                    map: map
                });
                marcador.marker = marker;
            }
        }
        function setHeightMap(){
            var heightWindow = window.innerHeight;
            $("#map").height(heightWindow - 87);
            google.maps.event.trigger(map, 'resize');
            map.setCenter({
                lat: CENTER_MAP_LAT,
                lng: CENTER_MAP_LON
            });
        }
    </script>
</body>
</html>
