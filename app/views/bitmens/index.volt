<!-- View Clientes -->
{{ content() }}

<link href="../plugins/datepicker/bootstrap-datepicker.css" rel="stylesheet">

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Bitacora Mensualidades</h2>
    </div>
</div>
<div class="wrapper wrapper-content" id="corteSearch">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div style="border: 1px solid #ddd;">
                        <nav class="navbar navbar-default" style="border-color: #000000; margin-bottom: 0px;">
                            <div>
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header" style="float: none; display: block;">
                                    <button type="button" id="colapse-busqueda" class="navbar-toggle" data-toggle="collapse"
                                            data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
                                </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="navbar-collapse collapse in clientes-only" id="contenedor-filtros" aria-expanded="true">
                                    <div class="nav-bar">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <label>Filtros de búsqueda:</label>
                                                <input type="hidden" id="idBusquedaPagos" value=""/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-9 col-sm-8">
                                                <div class="row">
                                                    <div class="col-xs-6 col-md-3 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="corte_busqueda_fIni">Fecha de Ini.</label>
                                                            <input id="corte_busqueda_fIni" name="corte_busqueda_fIni" type="text" class="form-control" placeholder="Fecha Inicial" value="{{ date("d/m/Y") }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-md-3 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="corte_busqueda_fFin">Fecha de Fin.</label>
                                                            <input id="corte_busqueda_fFin" name="corte_busqueda_fFin" type="text" class="form-control" placeholder="Fecha Final" value="{{ date("d/m/Y") }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-6 col-sm-12 form-group">
                                                        <label for="corte_busqueda_cCobratario">Cobratario</label>
                                                        <select class="form-control" id="corte_busqueda_cCobratario" style="width: 100%;">
                                                            <option value=""> -- Todos -- </option>
                                                            {% for cobratario in cobratarios %}
                                                                <option value="{{ cobratario.id }}">{{ cobratario.nombre | upper }} {{ cobratario.apellido_paterno | upper }} {{ cobratario.apellido_materno | upper }}</option>
                                                            {% endfor %}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-3 col-sm-4" style="margin-top: 5px;">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-2 col-lg-6">
                                                        <div class="form-group">
                                                            <label></label>
                                                            <button class="btn btn-primary btn-block" id="btnSearch" title="Buscar">
                                                                <i class="fa fa-search"></i> <span class="hidden-xs hidden-sm">Buscar</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    {% if acl.isAllowedUser('bitmens', 'exportexcel') %}
                                                        <div class="col-xs-12 col-sm-6 col-md-2 col-lg-6">
                                                            <div class="form-group">
                                                                <label></label>
                                                                <button class="btn btn-primary btn-block" id="btnExportCorte" title="Exportar reporte excel">
                                                                    <i class="fa fa-file-excel-o"></i> <span class="hidden-xs hidden-sm">Reporte</span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    {% endif %}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>

                    <div style="margin-top: 15px;">
                        <table class="display table-striped table-hover" id="dt-mensualidades" style="width: 100%;">
                            <thead>
                            <tr>
                                <th data-priority="0"></th>
                                <th data-priority="1">Cliente</th>
                                <th>Monto mensualidad</th>
                                <th>Clave cobratario</th>
                                <th>Cobratario</th>
                                <th>Fecha cambio</th>
                                <th>Origen</th>
                                <th>Motivo</th>
                                <th>fecha_ini_filtro</th>
                                <th>fecha_fin_filtro</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- view content -->


<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>
{{ javascript_include("plugins/validate/jquery.validate.min.js") }}

{{ javascript_include("plugins/datatables_new/Buttons-1.2.2/js/dataTables.buttons.js") }}
{{ javascript_include("plugins/datatables_new/JSZip-2.5.0/jszip.js") }}
{{ javascript_include("plugins/datatables_new/pdfmake-0.1.18/build/pdfmake.js") }}
{{ javascript_include("plugins/datatables_new/pdfmake-0.1.18/build/vfs_fonts.js") }}
{{ javascript_include("plugins/datatables_new/Buttons-1.2.2/js/buttons.bootstrap.js") }}
{{ javascript_include("plugins/datatables_new/Buttons-1.2.2/js/buttons.html5.js") }}
{{ javascript_include("plugins/datatables_new/Buttons-1.2.2/js/buttons.colVis.js") }}
{{ javascript_include("plugins/datatables_new/sum.js") }}

{{ javascript_include("js/bitmens/bitmens-events.js") }}
