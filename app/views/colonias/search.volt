{% if NotImplemented is defined %}
    <p> Método no implementado</p>
{% else %}
    <table class="table table-striped table-hover" id="colonias-datatable" style="width: 100%;">
        <thead>
        <tr>
            <th style="width: 35px;">&nbsp;</th>
            <th style="width: 35px;">Estatus</th>
            <th>Clave</th>
            <th>Nombre</th>
            <th>Alias</th>
            <th>Localidad</th>
            <th>Monto</th>
            <th>Creación</th>
            <th>Modificación</th>
        </tr>
        </thead>
        <tbody>
        {% for colonia in colonias %}
            <tr data-index="{{ colonia.id }}" id="tr-{{ colonia.id }}">
                <td>
                    {% if info %}
                        <button class="btn btn-sm btn-info read" title="Consultar datos de colonia" data-index="{{ colonia.id }}">
                            <i class="fa fa-info"></i>
                        </button>
                    {% endif %}
                    {% if edit %}
                        <button class="btn btn-sm btn-primary edit" title="Editar datos de colonia" data-index="{{ colonia.id }}">
                            <i class="fa fa-pencil"></i>
                        </button>
                    {% endif %}
                    {% if delete %}
                        <button class="btn btn-sm btn-danger delete" title="¿Desea eliminar colonia?" data-index="{{ colonia.id }}">
                            <i class="fa fa-remove"></i>
                        </button>
                    {% endif %}
                </td>
                <td>
                    {% if colonia.activo == true %}
                        <i class="fa fa-check" style="color: green" title="Activo"></i>
                    {% else %}
                        <i class="fa fa-remove" style="color: red" title="Inactivo"></i>
                    {% endif %}
                </td>
                <td>{{ colonia.id }}</td>
                <td><div id="name-colonia-{{ colonia.id }}" data-index="{{ colonia.id }}">{{ colonia.nombre }}</div></td>
                <td>{{ colonia.alias }}</td>
                <td>{{ colonia.localidad }}</td>
                <td>{{ colonia.monto }}</td>
                <td>{{ colonia.creacion }}</td>
                <td>{{ colonia.modificacion }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
    <script type="application/javascript">
        $(document).ready(function() {
            var hasButtons = {% if hasButtons == true %} 'B' {% else %} '' {% endif %};
            var buttons = new Array();
            {% for button in buttons %}
            var jsonButton = JSON.parse('{{ button }}');
            {% if loop.last %}
            jsonButton.className =  "vertical-divider";
            {% endif %}
            buttons.push(jsonButton);
            {% endfor %}
            coloniasDT = $("#colonias-datatable").DataTable({
                "dom": '<"clear"><"top container-float"'+hasButtons+'f><"users-scroll"rt><"clear"l><"clear"ip>',
                //            "responsive": true,
                "paging":   true,
                "info":     true,
                "processing": true,
                "pagingType": "full",
                "language": {
                    "paginate": {
                        "next": ">",
                        "first": "<<",
                        "last": ">>",
                        "previous": "<"
                    },
                    "search": "",
                    "searchPlaceholder": "Buscar en los resultados encontrados",
                    "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
                    "infoEmpty": "",
                    "infoFiltered": " - filtrado de _MAX_",
                    "emptyTable": "Sin resultados",
                    "sZeroRecords": "Sin resultados",
                    "lengthMenu": "Mostrar _MENU_ registros"
                },
                "lengthMenu": [[15, 30, 40, -1], [15, 30,40, "Todos"]],
                "lengthChange": true,
                "buttons": buttons,
                responsive: true,
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "className": 'dt-center',
                        "orderable": false,
                        "searchable": false
                    },
                    {
                        "targets": [ 1 ],
                        "className": 'dt-center',
                    },
                    {
                        "targets": [ 6, 7],
                        "searchable": false
                    },
                ]
            });
            $(".buttons-collection").attr('title', 'Mostrar/ocultar datos de tabla');
            $(".buttons-print").attr('title', 'Imprimir');
            $(".buttons-excel").attr('title', 'Exportar a excel');
            $(".pagination .first").attr('title', 'Primera página');
            $(".pagination .previous").attr('title', 'Página anterior');
            $(".pagination .next").attr('title', 'Página siguiente');
            $(".pagination .last").attr('title', 'Última página');

            $('#colonias-datatable tbody').off('click', '.read');
            $('#colonias-datatable tbody').on('click', '.read', function (e) {
                $indexcolonia = this.getAttribute("data-index");
                openReadColonia($indexcolonia);
            });

            $('#colonias-datatable tbody').off('click', '.edit');
            $('#colonias-datatable tbody').on('click', '.edit', function (e) {
                $indexcolonia = this.getAttribute("data-index");
                openEditColonia($indexcolonia);
            });

            $('#colonias-datatable tbody').off('click', '.delete');
            $('#colonias-datatable tbody').on('click', '.delete', function (e) {
                $(".popover.confirmation").confirmation("destroy");
                var $tr = $(this).closest("tr");
                $indexcolonia = this.getAttribute("data-index");
                // var idAttr = $tr.attr("id").split("-");
                // var id = idAttr[1];
                $(this).confirmation({
                    rootSelector: "body",
                    container: "body",
                    singleton: true,
                    popout: true,
                    btnOkLabel: "Si",
                    onConfirm: function() {

                        if ( $tr.hasClass('selected') ) {
                            //$tr.removeClass('selected');
                        }
                        else {
                            coloniasDT.$('tr.selected').removeClass('selected');
                            $tr.addClass('selected');
                        }
                        deleteColonia($tr, $indexcolonia);
                    },
                    onCancel: function() {
                        $(this).confirmation('destroy');
                    },
                });

                $(this).confirmation("show");
            });

        });
    </script>
{% endif %}