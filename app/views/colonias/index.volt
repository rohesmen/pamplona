<!-- View Colonias -russel -->
{{ content() }}

<link href="css/pamplona/colonias.css" rel="stylesheet">

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Colonias</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    {% if doSearch %}
                    <div class="container-fluid" style="border: 1px solid #ddd;">
                        <div class="row">
                            <div style="font-weight: bold; ;padding: 15px 5px 5px 10px;">Selecciona tipo de búsqueda por:</div>
                            <div class="col-sm-2">
                                <div class="form-group" style="margin-bottom: 5px;">
                                    <select class="form-control" id="colonias-filter-column-select">
                                        <option value="cla">Clave</option>
                                        <option value="nom">Nombre</option>
                                        <option value="loc">Localidad</option>
                                        <option value="mun">Municipio</option>
                                        <option value="-1" selected="selected">Todos</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4" id="filt1">
                                <div class="form-group">
                                    <div class="input-group custom-search-form" >
                                        <div id="colonias-filter-clear-do" title="Limpiar">
                                            <i class="fa fa-close" style="margin-top: 10px;"></i>
                                        </div>
                                        <input type="text" class="form-control" id="colonias-search-val">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="button" id="colonias-search-do" style="color: #337ab7;" title="Buscar">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div><!-- /input-group -->
                                </div>
                            </div>
                            <div class="col-md-4" id="filt2" style="display: none;">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <select class="form-control" id="municipio-select-filt">
                                                {% for i in municipios %}
                                                    <option value="{{ i.id }}">{{ i.nombre }}</option>
                                                {% endfor %}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 custom-search-form">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="button" id="colonias-search-do2" style="color: #337ab7;" title="Buscar">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6" style="text-align: right;">
                                {% if acl.isAllowedUser('colonias', 'add') %}
                                    <button id="openNewColonia" class="btn btn-primary" type="button" title="Nuevo" style="display: inline-block; margin-bottom: 10px;">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                {% endif %}
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;" id="colonias-datatable-container"></div>
                    {% else %}
                        <p>No cuenta con el permiso para realizar búsquedas, contacte al administrador.</p>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./view content -->

<!-- Modal -->
<div class="modal fade" id="colonia-info-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn-colonia-modal-close">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
                <!-- toolbar -->
                <div class="colonia-toolbar">
                    {% if acl.isAllowedUser('colonias', 'add') %}
                        <button type="button" title="Nuevo" class="btn btn-primary btn-circle colonia-toolbar-read colonia-tool-new"><i class="fa fa-plus"></i>
                        </button>
                    {% endif %}
                    {% if acl.isAllowedUser('colonias', 'edit') %}
                        <button type="button" title="Editar" class="btn btn-primary btn-circle colonia-toolbar-read colonia-tool-edit"><i class="fa fa-pencil"></i>
                        </button>
                    {% endif %}

                    {% if acl.isAllowedUser('colonias', 'add') %}
                    <button type="button" title="Guardar" class="btn btn-primary btn-circle colonia-toolbar-edit colonia-tool-save"><i class="fa fa-floppy-o"></i>
                    </button>
                    {% endif %}
                    {% if acl.isAllowedUser('colonias', 'cancelar') %}
                    <button type="button" title="Deshacer" class="btn btn-error btn-circle colonia-toolbar-edit colonia-tool-cancel"><i class="fa fa-undo"></i>
                    </button>
                    {% endif %}

                    <button type="button" title="Siguiente" class="btn btn-primary btn-circle colonia-toolbar-read btn-next-colonia" style="float: right; margin-left: 3px;"><i class="fa fa-angle-right"></i></button>
                    <button type="button" title="Anterior" class="btn btn-primary btn-circle colonia-toolbar-read btn-before-colonia" style="float: right;"><i class="fa fa-angle-left"></i></button>

                    <span class="colonia-info-aviso"></span>
                    <span class="colonia-info-error"></span>
                </div>
                <!-- ./toolbar -->
                <div id="colonia-data">
                    <input type="hidden" id="colonias-modal-coloniaid">
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="control-label colonia-required" for="colonia-info-nombre">Nombre:</label>
                            <input type="text" class="form-control input-sm" id="colonia-info-nombre"/>
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label" for="colonia-info-clave">Clave:</label>
                            <input type="text" class="form-control input-sm" id="colonia-info-clave"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group-sm" >
                                <label class="control-label colonia-required" for="colonia-info-alias">Alias:</label>
                                <input type="text" class="form-control colonia-info-label text-uppercase colonia-field-required" id="colonia-info-alias"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group-sm" >
                                <label class="control-label colonia-required" for="colonia-info-localidad">Localidad:</label>
                                <input type="text" class="form-control colonia-info-label text-uppercase colonia-field-required" id="colonia-info-localidad"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 hidden" >
                            <div class="form-group-sm">
                                <label class="control-label colonia-required" for="colonia-info-idunidadhabi">Unidad habi:</label>
                                <input type="text" class="form-control colonia-info-label text-uppercase colonia-field-required" id="colonia-info-idunidadhabi"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group-sm">
                                <label class="control-label colonia-required" for="municipio-select">Municipio:</label>
                                <select class="form-control" id="municipio-select">
                                    {% for i in municipios %}
                                        <option value="{{ i.id }}">{{ i.nombre }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6" >
                            <div class="form-group-sm">
                                <label class="control-label colonia-required" for="colonia-info-monto">Monto:</label>
                                <input type="text" class="form-control colonia-info-label colonia-field-required" placeholder="00.00" id="colonia-info-monto"/>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-3">
                            Activo
                            <div class="onoffswitch">
                                <input type="checkbox" checked class="onoffswitch-checkbox" id="colonia-info-activo">
                                <label class="onoffswitch-label" for="colonia-info-activo">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-colonia-modal-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->

<!-- script colonias -->
{{ javascript_include("js/colonias-events.js") }}