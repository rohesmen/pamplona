{{ content() }}

{{ stylesheet_link('plugins/fullcalendar/fullcalendar.min.css') }}
{{ stylesheet_link('plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}
{{ stylesheet_link('plugins/datepicker/bootstrap-datepicker.css') }}

<style>
    .fc-day-grid-event .fc-content {
        white-space: normal;
    }

    fieldset
    {
        border: 1px solid #ddd !important;
        margin: 0;
        xmin-width: 0;
        padding: 10px;
        position: relative;
        border-radius:4px;
        background-color:#f5f5f5;
        padding-left:10px!important;
    }

    legend
    {
        font-size:14px;
        font-weight:bold;
        margin-bottom: 0px;
        width: 35%;
        border: none;
        /*border-radius: 4px;*/
        /*padding: 5px 5px 5px 10px;*/
        /*background-color: #ffffff;*/
    }

</style>
{#{{ stylesheet_link('plugins/fullcalendar/fullcalendar.print.min.css') }}#}
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Servicios</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    <button class="btn btn-primary pull-right os-exportar-do" style="margin-bottom: 10px;" type="button" title="Exportar">
                        <i class="fa fa-file-excel-o"></i>&nbsp;Exportar
                    </button>
                    {#<button class="btn btn-primary pull-right os-cotizar-do" style="margin-bottom: 10px; margin-right: 10px;" type="button" title="Cotizar">#}
                        {#<i class="fa fa-money"></i>&nbsp;Cotizar#}
                    {#</button>#}
                    <div style="margin-top: 15px;">

                        {% for e in estados %}
                            <span style="display: inline-block;background-color: #{{ e.color }}; border-radius: 15px; padding: 10px;">
                                {{ e.nombre }}
                            </span>
                        {% endfor %}

                    </div>
                    <div id="container-filtros1">
                        <fieldset>
                            <legend>Búsqueda</legend>
                            <input type="hidden" id="idclienteforservice">
                            <div class="row">
                                <!-- filtros -->
                                <div class="col-xs-6 col-md-2">
                                    <div class="form-group">
                                        <label>Buscar por</label>
                                        <select class="form-control" id="os-filter-select">
                                            <option value="dir" selected="selected">Dirección</option>
                                            <option value="nombre">Nombre</option>
                                            <option value="tel">Teléfono</option>
                                            {#<option value="">Todos</option>#}
                                        </select>
                                    </div>
                                </div>
                                <!-- direccion -->
                                <div class="col-xs-12 col-sm-8" id="os-filtro-direccion">
                                    <div class="row">
                                        <div class="col-xs-6 col-md-3">
                                            <div class="form-group">
                                                <label class="hidden-xs">&nbsp;</label>
                                                <input id="os-calle-val" name="calle" type="text" class="form-control" placeholder="Calle">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-md-3">
                                            <div class="form-group">
                                                <label class="hidden-xs">&nbsp;</label>
                                                <input id="os-numero-val" name="numero" type="text" class="form-control" placeholder="Número">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-6 form-group">
                                            <label class="hidden-xs">&nbsp;</label>
                                            <select class="form-control" id="os-colonia-val" style="width: 100%;">
                                                <option value="">Sel. Colonia</option>
                                                {% for colonia in colonias %}
                                                    <option value="{{ colonia.id }}">{{ colonia.nombre ~ ", " ~ colonia.localidad }} {{ colonia.activo ? "" : "(SIN SERVICIO)" }}</option>
                                                {% endfor %}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- clave -->
                                <div class="col-xs-12 col-sm-8" style="display: none" id="os-filtro-clave">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <input type="text" class="form-control" placeholder="Números" id="os-telefono-val" data-inputmask="'mask': '(999) 999-99-99', 'greedy' : false">
                                    </div>
                                </div>
                                <!-- nombre -->
                                <div class="col-xs-12 col-sm-8" style="display: none" id="os-filtro-nombre">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <input type="text" class="form-control" placeholder="Nombre" id="os-nombre-val">
                                    </div>
                                </div>
                                <!-- boton search -->
                                <div class="col-xs-6 col-md-2">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-primary btn-block os-search-do" style="margin-bottom: 10px;" type="button" title="Buscar">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>

                            </div>
                        </fieldset>
                    </div>
                    <fieldset style="margin-top: 10px;">
                        <legend>Cliente seleccionado</legend>
                        <div class="row">
                            <div class="col-sm-2">
                                <strong>Folio: </strong><span id="datosBusquedaClaveCliente"></span>
                            </div>
                            <div class="col-sm-3">
                                <strong>Cliente: </strong><span id="datosBusquedaNombreCliente"></span>
                            </div>
                            <div class="col-sm-3">
                                <strong>Dirección: </strong><span id="datosBusquedaDireccionCliente"></span>
                            </div>
                            <div class="col-sm-2">
                                <strong>Teléfono: </strong><span id="datosBusquedaTelefonoCliente"></span>
                            </div>
                            <div class="col-sm-2">
                                <strong>Correo: </strong><span id="datosBusquedaCorreoCliente"></span>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset style="margin-top: 10px;">
                        <legend>Historial de servicios</legend>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="display table-striped table-hover" id="os-dt" style="width: 100%;">
                                    <thead>
                                    <tr>
                                        <th data-priority="1"></th>
                                        <th>Orden</th>
                                        <th>Estado</th>
                                        <th>Dirección</th>
                                        <th>Teléfono</th>
                                        <th>Servicio</th>
                                        <th>Fecha</th>
                                        <th>Costo</th>
                                        <th>Material Extra</th>
                                        <th>Observaciones</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </fieldset>

                    <div style="margin-top: 15px;">
                        <fieldset style="margin-top: 10px;">
                            <legend>Agenda</legend>
                            <div id="calendar"></div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="infoservicio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="nombreservicio">Información servicio</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="ordenservicio">
                <input type="hidden" id="diaorden">
                <input type="hidden" id="horaorden">
                <input type="hidden" id="duracionhorasservicios">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12" id="mensaje-disponibilidad"></div>
                    </div>
                    <div class="row hidden">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Orden servicio: </label>
                                <span id="ordenserviciolabel"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Cliente: </label>
                                <span id="nombrecliente"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Dirección: </label>
                                <span id="direccioncliente"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Referencia: </label>
                                <span id="referenciacliente"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label>Fecha: </label>
                                <span id="fechacliente"></span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label>Hora ini: </label>
                                <span id="horacliente"></span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label>Hora fin: </label>
                                <span id="horafincliente"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label>Teléfono: </label>
                                <span id="telcliente"></span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label>Correo: </label>
                                <span id="correocliente"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <table id="getTableAllServices" class="table table-striped" style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Clave</th>
                                    <th>Servicio</th>
                                    <th>Cantidad</th>
                                    <th>Costo</th>
                                    <th>Costo Final</th>
                                </tr>
                                </thead>
                                <tbody id="tblBodyServicesAdd"></tbody>
                            </table>
                        </div>
                        <div class="col-md-4 col-md-offset-8">
                            <label for="" class="control-label">Total: </label>
                            <input type="text" id="fin_sumaTotal_allServicios" class="form-control" value="" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Estado: </label>
                                <span >
                                    <select class="form-control" id="estadoservicio">
                                        <option value="">Seleccionar</option>
                                        {% for e in estados %}
                                            <option value="{{ e.clave }}">{{ e.nombre }}</option>
                                        {% endfor %}
                                    </select>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row container-cancelado">
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label>Responsable: </label>
                                <span id="nombreresponsable"></span>
                                <select class="form-control" id="listbrigadas">
                                    <option value="">Seleccionar</option>
                                    {% for b in brigadas %}
                                        <option value="{{ b.id }}">{{ b.responsable }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label>Hora ini: </label>
                                <span id="container-hora-agendado"></span>
                                <div class="row" id="container-hora-agendar">
                                    <div class="col-xs-12">
                                        <select id="horas" class="form-control">
                                            <option value="">Seleccionar</option>
                                            {% for h in horas %}
                                                <option value="{{ h }}:00">{{ h }}:00</option>
                                            {% endfor %}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label>Hora fin: </label>
                                <span id="container-horafin-agendado"></span>
                                <div class="row" id="container-horafin-agendar">
                                    <div class="col-xs-12">
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <input id="horasfin" type="text" class="form-control input-small">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row container-finalizado">
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label>Duración: </label>
                                <input type="text" class="form-control" id="duracion" placeholder="02:30">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label>Costo: </label>
                                {#{% if acl.isAllowedUser('servicios', 'edit')  %}#}
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" class="form-control" id="costo">
                                </div>
                                {#{% endif %}#}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label>Material Extra: </label>
                                {#{% if acl.isAllowedUser('servicios', 'edit')  %}#}
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" class="form-control" id="materialextra" value="0">
                                </div>
                                {#{% endif %}#}
                            </div>
                        </div>
                    </div>
                    <div class="row container-finalizado" id="container-factura">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label>¿Factura?: </label>
                                <input type="checkbox" id="confactura">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label>Factura: </label>
                                <input type="text" class="form-control" id="factura" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="container-observacion">
                        <div class="col-xs-12 ">
                            <div class="form-group">
                                <label>Observaciones: </label>
                                <textarea rows="5" style="resize: none;" class="form-control" id="observaaciones"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnGuardarModal" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalExportar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Exportar</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="exp_cliente">
                <div class="container-fluid">

                    <div class="row exp-data-cliente">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Cliente: </label>
                                <span id="exp_clientelabel"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label>Fecha inicio: </label>
                                <input type="text" class="form-control" id="exp_finicio" name="exp_finicio" placeholder="dd/MM/yyyy">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label>Fecha fin: </label>
                                <input type="text" class="form-control" id="exp_ffin" name="exp_ffin" placeholder="dd/MM/yyyy">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Estado: </label>
                                <span >
                                    <select class="form-control" id="exp_estadoservicio">
                                        <option value="">TODOS</option>
                                        {% for e in estados %}
                                            <option value="{{ e.id }}">{{ e.nombre }}</option>
                                        {% endfor %}
                                    </select>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnExportarModal" type="button" class="btn btn-primary">Exportar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addClientModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Agendar servicio</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="hddIdClienteAgendar">
                <input type="hidden" id="hddstartAgendar">
                <input type="hidden" id="hddCotizar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label>Teléfono: </label>
                                <input type="text" class="form-control" id="telefono-cliente-agendar">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label>Correo: </label>
                                <input type="text" class="form-control" id="correo-cliente-agendar">
                            </div>
                        </div>
                    </div>
                    {#<div class="row">
                        <div class="col-xs-12 col-md-10">
                            <div class="form-group">
                                <label>Servicio: </label>
                                <select class="form-control" id="agendarservicios">
                                    <option value="">Seleccionar</option>
                                    {% for ss in servicios %}
                                        <optgroup label="{{ ss["grupo"] }}">
                                            {% for s in ss["data"] %}
                                                <option value="{{ s.id }}" data-costo="{{ s.costo }}" data-horadura="{{ s.horas_estimadas + s.horas_tolereancia }}">{{ s.nombre }}</option>
                                            {% endfor %}
                                        </optgroup>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <button class="btn btn-primary btn-block" id="btnAddServicioList" style="margin-top: 22px;"><i class="fa fa-plus" ></i></button>
                        </div>
                        <div class="col-xs-12">
                            <table class="display table-striped table-hover" id="listaservicios-dt" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Servicio</th>
                                    <th>Costo</th>
                                    <th>costo_numero</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <th></th>
                                <th>Total</th>
                                <th id="total-servicios">0.00</th>
                                <th>0.00</th>
                                </tfoot>
                            </table>
                        </div>
                    </div>#}
                    <div class="row bottom-xs" style="margin-bottom: 15px;">
                        <div class="col-xs-12 col-md-10 col-md-10">
                            <div class="box">
                                <div class="form-group" style="margin-bottom: 0;">
                                    <label>Servicio: </label>
                                    <select class="form-control" id="agendarservicios">
                                        <option value="">Seleccionar</option>
                                        {% for ss in servicios %}
                                            <optgroup label="{{ ss["grupo"] }}">
                                                {% for s in ss["data"] %}
                                                    <option value="{{ s.id }}" data-horadura="{{ s.horas_estimadas + s.horas_tolereancia }}">{{ s.nombre }}</option>
                                                {% endfor %}
                                            </optgroup>
                                        {% endfor %}
                                        {#{% for s in servicios %}#}
                                        {#<option value="{{ s.id }}" data-horadura="{{ s.horas_estimadas + s.horas_tolereancia }}">{{ s.nombre }}</option>#}
                                        {#{% endfor %}#}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2">
                            <div class="box" style="margin-top: 23px;">
                                <a href="javascript:void(0)" id="addMasServicios" class="btn btn-primary btn-block"><i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <table id="tableToSaveAllServices" class="table table-striped" style="width:100%">
                                <thead>
                                <tr>
                                    <th width="75px">Remover</th>
                                    <th>Clave</th>
                                    <th>Servicio</th>
                                    <th>Costo</th>
                                    <th>Cantidad</th>
                                    <th>Total</th>
                                    <th width="75px">T. Estimado</th>
                                </tr>
                                </thead>
                                <tbody id="tblBody-servicios"></tbody>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <label for="" class="control-label">Subtotal: </label>
                            <input type="text" id="sumaSubtotalservicios" class="form-control" value="" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="" class="control-label">IVA: </label>
                            <input type="text" id="sumaIVAservicios" class="form-control" value="" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="" class="control-label">Total: </label>
                            <input type="text" id="sumaTotalservicios" class="form-control" value="" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="" class="control-label">T. Estimado (Horas): </label>
                            <input type="text" id="sumaTiemposervicios" class="form-control" value="" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>Estado: </label>
                                <select class="form-control" id="clavestaagcot">
                                    {% for e in estados %}
                                        <option value="{{ e.clave }}" style="display: {{ e.clave != 'COT' and e.clave != 'AG' ? 'none' : 'block' }}">{{ e.nombre }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-3 hiddencotizar">
                            <div class="form-group ">
                                <label>Responsable: </label>
                                <select class="form-control" id="agendarlistbrigadas">
                                    <option value="">Seleccionar</option>
                                    {% for b in brigadas %}
                                        <option value="{{ b.id }}">{{ b.responsable }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>Hora ini: </label>
                                <div class="row" id="container-hora-agendar">
                                    <div class="col-xs-12">
                                        <select id="horasagendar" class="form-control">
                                            <option value="">Seleccionar</option>
                                            {% for h in horas %}
                                                <option value="{{ h }}:00">{{ h }}:00</option>
                                            {% endfor %}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>Hora fin: </label>
                                <div class="input-group bootstrap-timepicker timepicker">
                                    <input id="horasfinagendar" type="text" class="form-control input-small">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label>Referencia: </label>
                                <input type="text" class="form-control" id="nuevo-referencia">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label>Descripción: </label>
                                <textarea type="text" class="form-control" id="nuevo-descripcion" rows="5" style="resize: none;"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="datos-costo" style="display: none;">
                        <div class="col-xs-12">
                            <div><b>Costo:</b><span id="costo-servicio-aprox"></span></div>
                            <div><b>Nota:</b><span id="nora-costo-servicio"></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnGuardarServicioModal" type="button" class="btn btn-primary">Guardar</button>
                {#<button id="btnGuardarCotizacionServicioModal" type="button" class="btn btn-primary">Cotizar</button>#}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="buscarClientetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Seleccionar cliente</h4>
            </div>
            <div class="modal-body">
                <table class="display table-striped table-hover" id="oscliente-dt" style="width: 100%;">
                    <thead>
                    <tr>
                        <th data-priority="1"></th>
                        <th>Folio</th>
                        <th>Cliente</th>
                        <th>Dirección</th>
                        <th>Vigente</th>
                        <th>correo</th>
                        <th>telefono</th>
                        <th>isservicio</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalConfirmacionCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Crear cliente</h4>
            </div>
            <div class="modal-body">
                No se encontraron resultados con estos datos
                ¿Desea crearlo?
            </div>
            <div class="modal-footer">
                <button id="btnCrearClienteCancel" type="button" class="btn btn-danger"><i class="fa fa-times"></i> No</button>
                <button id="btnCrearClienteConfirm" type="button" class="btn btn-primary"><i class="fa fa-check"></i> Sí</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalCrearCLienteExterno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Seleccionar cliente</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 border-right">
                        <div class="row">
                            <div class="col-sm-4 form-group">
                                <label>Nombre </label>
                                <input id="nombre_cliente" name="calle" type="text" placeholder="Nombre" class="form-control">
                            </div>
                            <div class="col-sm-4 form-group">
                                <label>Ape. pat.</label>
                                <input id="apepat_cliente" name="calle_letra" type="text" placeholder="Ape. pat." class="form-control">
                            </div>
                            <div class="col-sm-4 form-group">
                                <label>Ape. mat.</label>
                                <input id="apemat_cliente" name="tipo_calle" type="text" placeholder="Ape. mat." class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 border-right">
                        <div class="row">
                            <div class="col-sm-4 form-group">
                                <label>Calle *</label>
                                <input id="calle_cliente" name="calle" type="text" placeholder="Calle" class="form-control" required>
                            </div>
                            <div class="col-sm-4 form-group">
                                <label>Letra</label>
                                <input id="calle_letra_cliente" name="calle_letra" type="text" placeholder="Calle letra" class="form-control">
                            </div>
                            <div class="col-sm-4 form-group">
                                <label>Tipo</label>
                                <input id="tipo_calle_cliente" name="tipo_calle" type="text" placeholder="Tipo calle" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 form-group">
                                <label>Número *</label>
                                <input id="numero_cliente" name="numero" type="text" placeholder="Número" class="form-control" required>
                            </div>
                            <div class="col-sm-4 form-group">
                                <label>Letra</label>
                                <input id="numero_letra_cliente" name="numero_letra" type="text" placeholder="Número letra" class="form-control">
                            </div>
                            <div class="col-sm-4 form-group">
                                <label>Tipo</label>
                                <input id="tipo_numero_cliente" name="tipo_numero" type="text" placeholder="Tipo número" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 form-group">
                                <label>Cruza1</label>
                                <input id="cruza1_cliente" name="cruza1" type="text" placeholder="Cruzamiento 1" class="form-control">
                            </div>
                            <div class="col-sm-4 form-group">
                                <label>Letra</label>
                                <input id="letra_cruza1_cliente" name="letra_cruza1" type="text" placeholder="Letra cruzamiento 1" class="form-control">
                            </div>
                            <div class="col-sm-4 form-group">
                                <label>Tipo</label>
                                <input id="tipo_cruza1_cliente" name="tipo_cruza1" type="text" placeholder="Tipo cruzamiento 1" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 form-group">
                                <label>Cruza2</label>
                                <input id="cruza2_cliente" name="cruza2" type="text" placeholder="Cruzamiento 2" class="form-control">
                            </div>
                            <div class="col-sm-4 form-group">
                                <label>Letra</label>
                                <input id="letra_cruza2_cliente" name="letra_cruza2" type="text" placeholder="Letra cruzamiento 2" class="form-control">
                            </div>
                            <div class="col-sm-4 form-group">
                                <label>Tipo</label>
                                <input id="tipo_cruza2_cliente" name="tipo_cruza2" type="text" placeholder="Tipo cruzamiento 2" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label>Colonia *</label>
                                <select class="form-control" id="idcolonia_cliente" name="idcolonia" required>
                                    <option value="">Sel. Colonia</option>
                                    {% for colonia in colonias %}
                                        <option value="{{ colonia.id }}">{{ colonia.nombre }} {{ colonia.activo ? "" : "(SIN SERVICIO)" }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label>Referencia ubicación</label>
                                <input id="referencia_ubicacion_cliente" type="text" placeholder="Referencia ubicación" class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label>Teléfono*</label>
                                <input id="telefono_cliente" type="text" placeholder="Teléfono" class="form-control" required>
                            </div>

                            <div class="col-sm-6 form-group">
                                <label>Correo</label>
                                <input id="correo_cliente" type="text" placeholder="Correo ubicación" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnCrearNuevoCliente" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

{{ javascript_include("plugins/datepicker/bootstrap-datepicker.js") }}
{{ javascript_include("plugins/datepicker/locales/bootstrap-datepicker.es.js") }}
{{ javascript_include("plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js") }}
{{ javascript_include("plugins/fullcalendar/lib/moment.min.js") }}
{{ javascript_include("plugins/fullcalendar/fullcalendar.min.js") }}
{{ javascript_include("plugins/fullcalendar/locale/es.js") }}
{{ javascript_include("plugins/bootbox/bootbox.min.js") }}

<script>
    datoscosto = {{ servicios|json_encode }};
</script>

{{ javascript_include("js/servicios-events.js") }}

<script>
    var get_table_ajax_services = function(idOrdenServicio){
        $("#getTableAllServices").DataTable().destroy();
        $('#tblBodyServicesAdd').html('');
        getServicesDT = $('#getTableAllServices').DataTable( {
            processing: true,
            searching: false,
            bInfo: false,
            paging: false,
            scrollCollapse: true,
            fixedColumns: true,
            scrollX: false,
            responsive: {
                details: {
                    type: 'column',
                    target: 0
                }
            },
            "columnDefs": [
                { targets: 0, className: 'control', width: 20, orderable: false, searchable: false },
                { targets: 4, visible: false, orderable: false, searchable: true },
                { targets: '_all',  visible: true, orderable: true, searchable: true }
            ],
            colReorder: true,
            select: false,
            language: {
                url: "../../plugins/DataTables/language.MX.json"
            },
            order: [[ 1, "desc" ]],
        });


        $.ajax({
            url: "/servicios/getServiciosByIdOrden",
            type: "POST",
            data: JSON.stringify({ byidOrdenServicio: idOrdenServicio }),
            success: function(resp){
                var json = JSON.parse(resp);
                getServicesDT.clear().draw();
                getServicesDT.rows.add(json.servicios); // Add new data
                getServicesDT.columns.adjust().draw(); // Redraw the DataTable
                getServicesDT.responsive.recalc();

                var totalFinal = 0;
                getServicesDT.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                    totalFinal = totalFinal + parseFloat( this.data()[4] );
                });
                $('#fin_sumaTotal_allServicios').val(totalFinal);
                if($("#materialextra").length > 0) {
                    $("#materialextra").val(json.orden.costo_extra);
                }
            },
            error: function(xhr, status, error){
                //code
            }
        });
    };



    $('#addMasServicios').on('click', function(){
        var agendarservicios = $('#agendarservicios').val();

        if(agendarservicios != "" && agendarservicios != null){
            $.get( "/servicios/getServicioByiD", { id: agendarservicios } )
                .done(function( data ) {
                    var _json = JSON.parse(data);
                    if(!_json.lError){
                        var service = _json.servicio;
                        var cantidad = 1;

                        var data=[];
                        var btnedit='';
                        {% if acl.isAllowedUser('servicios', 'edit') %}
                            btnedit='<input type="text" id="cost" minlength="0" class="form-control EditCosto" value="'+service.costo+'">';
                        {% else %}
                        btnedit=service.costo;
                        {% endif %}
                        var btnremove='<a class="btn btn-danger btn-sm editor_remove" onclick="confirmarRemoverServicio(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>';
                        var tiempo_estimado = (service.horas_estimadas + service.horas_tolereancia);
                        var row=[btnremove, service.id, service.nombre, btnedit, cantidad, service.costo, tiempo_estimado];
                        data.push(row);
                        allServicesDT.rows.add(data).draw();//se agrega el servicio seleccionado

                        actualizaCostoTotalServicios();

                    }else{
                        console.log('error');
                        showGeneralMessage('Servicio no encontrado', 'warning', true);
                    }
                });
        }
    });
</script>