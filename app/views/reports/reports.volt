{% if NotFound == false %}

    <div class="panel panel-bordered panel-dark">
        <div class="panel-heading" id="reports-name">
            <h3 class="panel-title">{{ name }}</h3>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body" id="reports-datatable-container" >
            {% if emptyJson == true %}
                <p>Al parecer el reporte no contiene datos</p>
            {% else %}
                {% if error == true %}
                    <p>Al parecer el reporte contiene errores</p>
                {% else %}
                    <div id="reports-container-filter-column-select">
                        <br />
                        <div class="form-inline" style="margin-bottom: 10px;">
                            <div class="form-group">
                                <select class="form-control" id="reports-filter-column-select-1">
                                    <option value="-1">Filtro 1</option>
                                    {% for index,head in heads %}
                                            <option value="{{ index }}">{{ head }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="reports-filter-clear-do" id="reports-filter-clear-do-1" title="Limpiar">
                                        <i class="fa fa-close" style="margin-top: 10px;"></i>
                                    </div>
                                    <input type="text" class="form-control" id="reports-filter-text-1" data-column="">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default reports-filter-text-do" title="Buscar" type="button" id="reports-filter-text-do-1">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                    {#<div class="input-group-btn" id="reports-addColumn-Filter">#}
                                        {#<button class="btn btn-default" type="button" id="reports-add-column-filter-do">#}
                                            {#<i class="fa fa-plus"></i>#}
                                        {#</button>#}
                                    {#</div>#}
                                </div><!-- /input-group -->
                            </div>
                        </div>
                        <div class="form-inline" style="margin-bottom: 10px;">
                            <div class="form-group">
                                <select class="form-control reports-filter-column-select-2" id="reports-filter-column-select-2">
                                    <option value="-1">Filtro 2</option>
                                    {%- for index,head in heads %}
                                        <option value="{{ index }}">{{ head }}</option>
                                    {%- endfor -%}
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="reports-filter-clear-do" id="reports-filter-clear-do-2" title="Limpiar">
                                        <i class="fa fa-close" style="margin-top: 10px;"></i>
                                    </div>
                                    <input type="text" class="form-control" id="reports-filter-text-2" data-column="">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default reports-filter-text-do" title="Buscar" type="button" id="reports-filter-text-do-2" style="border-radius: 0px;">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                    {#<div class="input-group-btn" id="reports-addColumn-Filter">#}
                                        {#<button class="btn btn-default" type="button" id="reports-add-column-filter-do">#}
                                            {#<i class="fa fa-plus"></i>#}
                                        {#</button>#}
                                    {#</div>#}
                                </div><!-- /input-group -->
                            </div>
                        </div>
                    </div>
                    <table class="display dataTable" style="width: 100%;" id="reports-datatable">
                        <thead>
                            <tr>
                                {% for head in heads %}
                                    <th >{{ head }}</th>
                                {% endfor %}
                            </tr>
                        </thead>
                        <tbody>
                            {% for value in json %}
                                <tr>
                                {% for value2 in value %}
                                    <td>{{ value2 }}</td>
                                {% endfor %}
                                </tr>
                            {% endfor %}
                        </tbody>
                    </table>
                {% endif %}
            {% endif %}
        </div>
        <!-- /.panel-body -->
    </div>
<!-- /.panel -->
{% else %}
    <p>El reporte no existe</p>
{% endif %}

<script type="text/javascript">
    //<![CDATA[
    var $reportsDT = null;
    var $containerFilter = null;
    var $btnAddColumnFilter = null;
    $(document).ready(function(){
        {% if json is defined %}
            /*{{  json }}*/
            //var hasButtons = {% if hasButtons == true %} 'B' {% else %} '' {% endif %};
            var hasButtons = 'B';
            var buttons = new Array();
            var colvis = {"text": "<i class='fa fa-gear'></i>", "extend": "colvis"};
            {% if hasButtons != true %}
                colvis.className =  "vertical-divider";
            {% endif %}
            {% for button in buttons %}
                var jsonButton = JSON.parse('{{ button }}');
                {#{% if loop.last %}#}
                    {#jsonButton.className =  "vertical-divider";#}
                {#{% endif %}#}
                buttons.push(jsonButton);
            {% endfor %}
            buttons.push(colvis);
            $reportsDT = $("#reports-datatable").DataTable({
                "dom": '<"clear"><"top container-float"'+hasButtons+'f><"reports-scroll"rt><"bottom"lip><"clear">',
    //            "responsive": true,
                "paging":   true,
                "info":     true,
                "processing": true,
                "pagingType": "full",
                "language": {
                    "paginate": {
                        "next": ">",
                        "first": "<<",
                        "last": ">>",
                        "previous": "<"
                    },
                    "search": "",
                    "searchPlaceholder": "Filtra los resultados",
                    "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
                    "infoEmpty": "",
                    "infoFiltered": " - filtrado de _MAX_",
                    "emptyTable": "Sin resultados",
                    "sZeroRecords": "Sin resultados",
                    "lengthMenu": "Mostrar _MENU_ registros"
                },
                "lengthMenu": [[15, 30, 40, -1], [15, 30,40, "Todos"]],
                "lengthChange": true,
                "order": [],
                "buttons": buttons,
//                "columnDefs": [
//                    {
//                        "targets": [ 4,5,6 ],
//                        "visible": false,
//                        "searchable" : false
//                    }
//                ]
//                "columnDefs": [
//                    {
//                        "targets": [ 1 ],
//                        "searchable" : true
//                    },
//                    {
//                        "targets": "_all",
//                        "searchable": false
//                    }
//                ]
            });
        $(".buttons-collection").attr('title', 'Mostrar/ocultar datos de tabla');
        $(".buttons-print").attr('title', 'Imprimir');
        $(".buttons-excel").attr('title', 'Exportar a excel');
        $(".pagination .first").attr('title', 'Primera página');
        $(".pagination .previous").attr('title', 'Página anterior');
        $(".pagination .next").attr('title', 'Página siguiente');
        $(".pagination .last").attr('title', 'Última página');

                $containerFilter = $("#reports-container-filter-column-select").clone();
                $("#reports-container-filter-column-select").remove();

            $("#reports-container-filter").html($containerFilter);

        {% endif %}
    });
    //]]>
</script>