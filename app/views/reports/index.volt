{{ content() }}
<link href="css/pamplona/reports.css" rel="stylesheet">
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Reportes</h2>
    </div>
</div>
<!-- /.row -->

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-10 col-sm-4 col-md-3">
            <div class="form-group">
                <label for="reports-select"><b>Seleccione</b></label>
                <select class="form-control" id="reports-select">
                    <option value="">Elija una opción</option>
                    {%- for key, value in reports %}
                        <option value="{{ value.id }}">{{ value.nombre }}</option>
                    {%- endfor -%}
                </select>
            </div>
        </div>
        <div class="col-xs-2 col-sm-1" style="padding-top: 25px;">
            <button class="btn btn-default" id="update-report"><i class="fa fa-refresh" title="Actualizar"></i></button>
        </div>
        <div class="col-sm-4" id="reports-container-filter"></div>
    </div>
    <div class="row">
        <div class="col-lg-12" id="reports-container"></div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
{{ javascript_include("plugins/datatables_new/Buttons-1.2.2/js/dataTables.buttons.min.js") }}
{{ javascript_include("plugins/datatables_new/JSZip-2.5.0/jszip.min.js") }}
{{ javascript_include("plugins/datatables_new/pdfmake-0.1.18/build/pdfmake.min.js") }}
{{ javascript_include("plugins/datatables_new/pdfmake-0.1.18/build/vfs_fonts.js") }}
{{ javascript_include("plugins/datatables_new/Buttons-1.2.2/js/buttons.html5.min.js") }}
{{ javascript_include("plugins/datatables_new/Buttons-1.2.2/js/buttons.colVis.js") }}
<script type="application/javascript" src="../js/reports-events.js"></script>

