<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Iniciar sesión</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css">

    <link rel="stylesheet" href="../css/animate.css">

    <link rel="stylesheet" href="../css/inspinia.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.js"></script>
    <![endif]-->
    <style>
        .bg-image{
            background: url({{ config.application.imgFondoSession }}) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
</head>
<body class="bg-image">

<div class="middle-box text-center animated fadeInDown">
    <div class="ibox-content" style="background-color: transparent; margin-top: 40%; border:none;">
        <div>
            <img src="/img/logo_pamplona.png">
            <h2 style="color: white; font-weight: bold;">{{ config.application.sede }}</h2>
            <!--<p style="margin: 15px;">Servidor de pruebas</p>-->
        </div>
        {{ form() }}
        <div class="form-group">
            {{ form.render('user') }}
        </div>
        <div class="form-group">
            {{ form.render('password', ["class": "form-control"]) }}
        </div>
        <div class="row">
            <div class="col-md-12">{{ content() }}</div>
        </div>
		<div class="row">
			<div class="col-xs-4">

			</div><!-- /.col -->
			<div class="col-xs-4 text-center">
                {{ form.render('Entrar') }}
			</div><!-- /.col -->
			<div class="col-xs-4">
            </div>
		</div>
        </form>
    </div>
</div>

<!-- jQuery 2.1.4 -->
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="../js/bootstrap/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
</body>
</html>
