<!-- View Bitacora -->
{{ content() }}

<link href="../plugins/datepicker/bootstrap-datepicker.css" rel="stylesheet">
<link href="css/pamplona/bitreportes.css" rel="stylesheet">

<div id="page-title">
	<div id="titulo-modulo"><h1 class="page-header text-overflow">Bit&aacute;cora Reportes</h1></div>
	<div id="titulo-acciones">
		
	</div>
</div>

<ol class="breadcrumb">
	<li><a href="/">Inicio</a></li>
	<li><a href="/bitreportes">Bit&aacute;cora Reportes</a></li>
</ol>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    {% include "bitreportes/filter.volt" %}
                    <div style="margin-top: 15px;">
                        <div class="row" style="margin-top: 10px;" id="bitacora-datatable-container"></div>
						<ul class="nav nav-tabs" id="menu-dias"></ul>
						<div class="tab-content" id="tab-contenido" style="width:100% !important;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- view content -->


{{ javascript_include("plugins/validate/jquery.validate.min.js") }}
{{ javascript_include("js/bitreportes-events.js") }}
{{ javascript_include("plugins/datepicker/bootstrap-datepicker.js") }}
{{ javascript_include("plugins/datepicker/locales/bootstrap-datepicker.es.js") }}

<script type="application/javascript">

</script>