<div style="border: 1px solid #ddd;">
    <nav class="navbar navbar-default" style="border-color: #000000; margin-bottom: 0px;">
        <div>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header" style="float: none; display: block;">
                <button type="button" id="colapse-busqueda" class="navbar-toggle" data-toggle="collapse"
                        data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse in clientes-only" id="contenedor-filtros" aria-expanded="true">
                <div class="nav-bar">
                    <!-- filtros -->
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-6" id="clientes-filtro-direccion">
                         <div class="row">
							<div class="col-md-6 col-sm-6 form-group">
									<label for="fInicio">Inicio</label>
									<div class="input-group date fInicio">
                                        <input id="fInicio" name="fInicio" type="text" class="form-control" required>
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>
							</div>
							<div class="col-md-6 col-sm-6 form-group">
                                    <label for="fFin">Fin</label>
									<div class="input-group date fFin">
                                        <input id="fFin" name="fFin" type="text" class="form-control" required>
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>

							</div>
						</div>
                    </div>
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-2" id="">
						<label for="sTipo">Tipo de b&uacute;squeda</label>
						<select id="sTipo" class="form-control">
							<option value="todos" selected>Todos</option>
							<option value="chofer">Chofer</option>
							<option value="recolector">Recolector</option>
							<option value="coordinador">Coordinador</option>
							<option value="supervisor">Supervisor</option>
						</select>
                    </div>
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-2" id="option-select" style="display:none;">
						<label for="sTipoData">&nbsp;</label>
						<select id="sTipoData" class="form-control">
						</select>
                    </div>
                    <!-- boton nuevo -->
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" id="clientes-filtro-direccion">
                         <div class="row">
                            <div class="col-md-6 col-sm-6 form-group">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-primary btn-block" type="button" title="Generar" style="display: inline-block; margin-bottom: 10px;" id="consultarBitacora">
                                        <i class="fa fa-search"></i>
                                    </button>
                            </div>
                            <div class="col-md-6 col-sm-6 form-group">
                                    <label>&nbsp;</label>
                                    <button type="button" class="btn btn-primary btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: inline-block; margin-bottom: 10px;">
                                        Reportes
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="/bitreportes/listado" >Exportar listado</a></li>
                                        <li><a href="/bitreportes/tabs" >Exportar por d&iacute;a</a></li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->

    </nav>
</div>