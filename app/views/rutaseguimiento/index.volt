<style>
    .filtro-rutaseguimiento-clear-do {
        position: absolute;
        right: 15px;
        z-index: 10;
        top: 25px;
        height: 33px;
        width: 30px;
        text-align: center;
        cursor: pointer;
    }
    .rutaseguimiento-modal-header {
        padding: 15px !important;
    }
    #rutaseguimiento-info-modal .popover {
        z-index: 10000;
    }
    .item-divider{
        height: 1px;
        border: 1px solid #BDBDBD !important;
        margin-bottom: 10px !important;
    }
    .disnone{
        display: none;
    }
</style>
<!-- View Clientes -->
{{ content() }}

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>RUTAS</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- filtros -->
                    <div>
                        <nav class="navbar navbar-default" >
                            <div>
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header" style="float: none; display: none;">
                                    <button type="button" id="colapse-busqueda" class="navbar-toggle" data-toggle="collapse"
                                            data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
                                </div>
                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="navbar-collapse collapse in clientes-only" id="contenedor-filtros" aria-expanded="true" style="border-color: #FFFFFF;">
                                    <div class="nav-bar">
                                        <!-- activo -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Activo</label>
                                                <select class="form-control" id="fvigente" style="width: 100%;">
                                                    <option value="">Todos</option>
                                                    <option value="true">Si</option>
                                                    <option value="false">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- nombre -->
                                        <div class="{{ acl.isAllowedUser('rutaseguimiento', 'create') ?  'col-md-8' : 'col-md-9' }}">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <div class="filtro-rutaseguimiento-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="text" class="form-control Filt" id="frutaseguimiento" placeholder="Escriba el nombre de la ruta">
                                            </div>
                                        </div>
                                        <!-- boton search -->
                                        <div class="col-md-1">
                                            <label>&nbsp;</label>
                                            <button class="btn btn-primary btn-block" id="rutaseguimiento-search-do" style="margin-bottom: 10px;" type="button" title="Buscar">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                        {% if acl.isAllowedUser('rutaseguimiento', 'create') %}
                                            <!-- boton nuevo -->
                                            <div class="col-md-1">
                                                <div class="" style="text-align: right;">
                                                    <label>&nbsp;</label>
                                                    <button class="btn btn-primary btn-block" title="Crear" style="display: inline-block; margin-bottom: 10px;" id="addRutaSeguimientoDo">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        {% endif %}
                                    </div>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->
                        </nav>
                    </div>

                    <div>
                        <div id="allmap" class="map" style="height: 300px;"></div>
                    </div>
                    <div style="margin-top: 15px;">
                        <table class="display table-striped table-hover" id="rutaseguimiento-datatable" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th data-priority="2"></th>
                                    <th data-priority="1"></th>
                                    <th data-priority="4">Activo</th>
                                    <th data-priority="5">ID</th>
                                    <th data-priority="3">Nombre</th>
                                    <th data-priority="6">Fecha Creación</th>
                                    <th data-priority="7">Fecha Modificacion</th>
                                    <th>activo</th>
                                    <th>geojson</th>
                                    <th>descripcion</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal ruta seguimiento -->
<div class="modal inmodal fade" id="rutaseguimiento-info-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header rutaseguimiento-modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Nueva Ruta</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Nombre *</label>
                            <input id="txtNombre" type="text" placeholder="Nombre" class="form-control rs-required txtDis">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" id="txtKML">
                            <div class="form-group input_container">
                                <label for="fileUploadKML" id="lblfileUploadKML">KML</label>
                                <input type="file" class="form-control" accept=".kml" id="fileUploadKML">
                                <input type="hidden" class="form-control" id="textKML">
                                <input type="hidden" class="form-control" id="textGeojson">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="map" class="map" style="height: 300px;"></div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Descripcion</label>
                            <textarea id="txtDescripcion" name="txtDescripcion" rows="8" placeholder="Descripcion" class="form-control rs-required txtDis" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <!--<div class="item-divider {{ acl.isAllowedUser('rutaseguimiento', 'addrutrec') ?  '' : 'disnone' }}"></div>
                    <div class="row {{ acl.isAllowedUser('rutaseguimiento', 'addrutrec') ?  '' : 'disnone' }}">
                        <div class="col-md-4 form-group">
                            <label>Día Recolección *</label>
                            <select class="form-control" id="cmbDia" style="width: 100%;">
                                <option value="">Seleccionar</option>
                                {% for data in dias_recoleccion %}
                                    <option value="{{ data.sigla }}" data-data='{"orden": "{{ data.dia_bd }}"}'>{{ data.dia|upper }}</option>
                                {% endfor %}
                            </select>
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Tipo Recolección *</label>
                            <select class="form-control" id="cmbTipo" style="width: 100%;">
                                <option value="">Seleccionar</option>
                                {% for data in tipo_recoleccion %}
                                    <option value="{{ data.sigla }}">{{ data.tipo|upper }}</option>
                                {% endfor %}
                            </select>
                        </div>
                        <div class="{{ acl.isAllowedUser('rutaseguimiento', 'addrutrec') ?  'col-md-3' : 'col-md-4' }} form-group">
                            <label>Turno *</label>
                            <select class="form-control" id="cmbTurno" style="width: 100%;">
                                <option value="">Seleccionar</option>
                                {% for data in turno %}
                                    <option value="{{ data.id }}">{{ data.nombre|upper }}</option>
                                {% endfor %}
                            </select>
                        </div>
                        {% if acl.isAllowedUser('rutaseguimiento', 'addrutrec') %}
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-block btn-info" id="create_ruta_recoleccion" title="Agregar">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        {% endif %}
                    </div>-->
                </div>
                <!--<div class="container-fluid">
                    <div class="row">
                        <table class="display table-striped table-hover" id="ruta_recoleccion-datatable" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th data-priority="1"></th>
                                    <th data-priority="2"></th>
                                    <th data-priority="3">Día</th>
                                    <th data-priority="4">Tipo</th>
                                    <th data-priority="5">Turno</th>
                                    <th>sigla_dia_recoleccion</th>
                                    <th>sigla_tipo_recoleccion</th>
                                    <th>idturno</th>
                                    <th>activo</th>
                                    <th>id</th>
                                    <th>diadb</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>-->
            </div>
            <div class="modal-footer">
                <button id="btnGuardar" type="button" class="btn btn-primary" title="Guardar">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- view content -->
<script>
    coacciones = '{{ coacciones }}';
    deleterutrec = false;
</script>

{% if acl.isAllowedUser('rutaseguimiento', 'deleterutrec') %}
    <script>
        deleterutrec = true;
    </script>
{% endif %}

{{ javascript_include("plugins/mapbox/togeojson.js") }}
{{ javascript_include("js/rutaseguimiento/index-events.js") }}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZJImFXCY1MX5n3_OVhXg7Jgb9zBbLlLg"></script>