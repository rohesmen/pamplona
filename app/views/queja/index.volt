{{ content() }}

<link href="css/pamplona/clientes.css" rel="stylesheet">
<link href="css/pamplona/queja.css" rel="stylesheet">
<link href="plugins/contextMenu/jquery.contextMenu.css" rel="stylesheet">
<link href="plugins/datepicker/bootstrap-datepicker.css" rel="stylesheet">

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>QUEJAS</h2>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="container-fluid" style="border: 1px solid #ddd;">
                        <nav class="navbar navbar-default" style="border-color: #000000; margin-bottom: 0px;">

                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header" style="float: none; display: block;">
                                <button type="button" class="navbar-toggle" data-toggle="collapse"
                                        data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
                            </div>
                            <div class="navbar-collapse collapse in" id="contenedor-filtros" aria-expanded="true">

                                <div class="row">
                                    <div style="font-weight: bold; ;padding: 15px 5px 5px 10px;">Selecciona tipo de búsqueda por:</div>
                                    <div class="col-xs-9 col-sm-4 col-md-2">
                                        <div class="form-group" style="margin-bottom: 5px;">
                                            <select class="form-control" id="quejas-filter-select">
                                                <option value="">Todos</option>
                                                <option value="cla">Clave</option>
                                                <option value="nom">Título</option>
                                                <option value="etapa">Etapa</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xs-3 col-sm-6 col-md-9 "  id="quejas-filtro-todos">
                                        <div class="form-group">
                                            <div class="input-group custom-search-form" >
                                                <div class="input-group-btn">
                                                    <button class="btn btn-primary  quejas-search-do" type="button" title="Buscar">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div><!-- /input-group -->
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-9 oculto"  id="quejas-filtro-clave">
                                        <div class="form-group">
                                            <div class="input-group custom-search-form" >
                                                <div class="queja-filter-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="number" min="1" class="form-control quejas-search-val" placeholder="Números" id="quejas-clave-val" data-inputmask="'mask': '9', 'repeat': 10, 'greedy' : false">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-primary quejas-search-do" type="button" title="Buscar">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div><!-- /input-group -->
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-9 oculto"  id="quejas-filtro-nombre">
                                        <div class="form-group">
                                            <div class="input-group custom-search-form" >
                                                <div class="queja-filter-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="text" class="form-control quejas-search-val" placeholder="Escriba el valor a buscar" id="quejas-nombre-val">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-primary quejas-search-do" type="button" title="Buscar">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div><!-- /input-group -->
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-9 oculto"  id="quejas-filtro-etapa">
                                        <div class="row">
                                            <div class="col-xs-10 col-md-11">
                                                <div class="form-group">
                                                    <select class="form-control" id="queja-etapa-val">
                                                        {% for e in etapas %}
                                                        <option value="{{ e.id }}">{{ e.nombre }}</option>
                                                        {% endfor %}
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-2 col-md-1">
                                                <button class="btn btn-primary  quejas-search-do" type="button" title="Buscar">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-2 col-md-1 text-right" >
                                        {% if acl.isAllowedUser('queja', 'add') %}
                                        <button class="btn btn-primary btn-block" type="button" title="Nueva Queja" style="display: inline-block; margin-bottom: 10px;"
                                                id="openNewQueja">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                        {% endif %}
                                    </div>

                                </div>

                            </div>

                        </nav>


                    </div>

                    <div style="margin-top: 15px;">
                        <table class="display table-striped table-hover" id="quejas-datatable" style="width: 100%; height: 100%">
                            <thead>
                            <tr>
                                <th data-priority="0"></th>
                                <th data-priority="1"></th>
                                <th>Vigente</th>
                                <th>Clave</th>
                                <th data-priority="2">Título</th>
                                <th >Cliente</th>
                                <th>Etapa</th>
                                <th>Final</th>
                                <th data-priority="3">...</th>
                                <th>Fecha Reporte</th>
                            </tr>
                            </thead>
                        </table>

                    </div>

                </div>
            </div>
        </div>

    </div>


    <div class="row" style="margin-top: 10px;" id="queja-container"></div>

</div>


<!-- Modal -->
<div class="modal fade" id="queja-info-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title modal-titleAux" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">

                    <form id="frmQueja">
                        <div class="row">
                            <div class="col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label for="clave">Clave</label>
                                    <input type="text" class="form-control" id="clave" name="clave" placeholder="Clave" disabled>
                                </div>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="activo">Activo</label>
                                    <div class="checkbox" >
                                        <label>
                                            <input type="checkbox" id="activo" name="activo" disabled>
                                        </label>
                                    </div>
                                </div>


                            </div>
                        </div>
						<div class="item-divider">
						</div>
                        <br>
						<div class="row">
                            <div class="col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label for="claveCliente">* Clave Cliente</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="claveCliente" name="claveCliente" placeholder="Clave"   readonly required>
                                        <span class="input-group-btn">
                                            <button id="buscaCliente" class="btn btn-default" type="button" disabled>
                                                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-9 col-md-9">
                                <div class="form-group">
                                    <label for="cliente">Cliente</label>
                                    <input type="text" class="form-control" id="cliente" placeholder="" name="cliente" disabled>
                                </div>
                            </div>
                        </div>
						<div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="telefono">Telefono</label>
                                    <input type="text" class="form-control" id="telefono" placeholder="Teléfono" name="telefono" disabled>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="form-group">
                                    <label for="correo">Correo Electr&oacute;nico</label>
                                    <input type="text" class="form-control" id="correo" placeholder="Correo Electr&oacute;nico" name="correo" disabled> 
                                </div>
                            </div>
                        </div>
						<div class="item-divider">
						</div>
						<br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="titulo">* Título</label>
                                    <input type="text" class="form-control" id="titulo" placeholder="Título" name="titulo" required title="Inserte un Título">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="descripcion">* Descripción</label>
                                    <textarea  id="descripcion" name="descripcion" class="form-control" rows="3" required></textarea>

                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="nomReporta">* Nombre Reporta</label>
                                    <input type="text" class="form-control" id="nomReporta" placeholder="Nombre(s)" name="nomReporta" required>
                                  </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="aPaternoReporta">* Ape Paterno</label>
                                    <input type="text" class="form-control" id="aPaternoReporta" placeholder="Apellido Paterno" name="aPaternoReporta" required>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="aMaternoReporta">* Ape Materno</label>
                                    <input type="text" class="form-control" id="aMaternoReporta" placeholder="Apellido Materno" name="aMaternoReporta" required>
                                </div>
                            </div>
                        </div>
						<div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="telefono_q">Telefono</label>
                                    <input type="text" class="form-control" id="telefono_q" placeholder="Telefono" name="telefono">
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="form-group">
                                    <label for="correo_q">Correo Electr&oacute;nico</label>
                                    <input type="text" class="form-control" id="correo_q" placeholder="Correo Electr&oacute;nico" name="correo_q"> 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="origen">* Orígen</label>
                                    <select class="form-control" id="origen" name="origen" required>
                                        <option value="">Sel. Orígen</option>
                                        <option value="AYUNTAMIENTO">AYUNTAMIENTO</option>
                                        <option value="DIRECTA">DIRECTA</option>
                                        <option value="PAGINA">REPORTE EN LINEA - WEB</option>
                                        <option value="APP">REPORTE EN LINEA - APP</option>
                                    </select>

                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="folio">Folio</label>
                                    <input type="text" class="form-control" id="folio" placeholder="Folio" name="folio">

                                </div>
                            </div>
                        </div>
                        <div id="ayuntamiento" class="row oculto">
                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="nomAyunta">Nombre Ayuntamiento</label>
                                    <input type="text" class="form-control" id="nomAyunta" placeholder="Nombre(s)" name="nomAyunta">
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="aPaternoAyunta">Ape Paterno</label>
                                    <input type="text" class="form-control" id="aPaternoAyunta" placeholder="Apellido Paterno" name="aPaternoAyunta">
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="aMaternoAyunta">Ape Materno</label>
                                    <input type="text" class="form-control" id="aMaternoAyunta" placeholder="Apellido Materno" name="aMaternoAyunta">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="origen">Fecha Reporte</label>
                                    <input type="text" class="form-control" id="fReporte" name="fReporte" placeholder="dd/MM/yyyy">

                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="folio">Fecha Atención</label>
                                    <input type="text" class="form-control" id="fAtencion" name="fAtencion" placeholder="dd/MM/yyyy">

                                </div>
                            </div>
                        </div>
                            <button id="btnGuardaForm" type="submit" class="btn btn-default fade" style="display: none">Submit</button>
                    </form>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="btnGuardar" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="cliente-selec-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Búsqueda de Cliente</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-2">
                            <div class="form-group">
                                <label>Buscar por</label>
                                <select class="form-control" id="clientes-filter-select">
                                    <option value="nom">Nombre</option>
                                    <option value="dir" selected="selected">Dirección</option>
                                    <option value="">Todos</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-9 col-md-9" style="display: none" id="clientes-filtro-nombre">
                            <div class="form-group">
                                <label class="hidden-xs">&nbsp;</label>
                                <input type="text" class="form-control cliente-search-val" placeholder="Escriba el nombre del propietario" id="clientes-nombre-val">
                            </div>
                        </div>
                        <!-- direccion -->
                        <div class="col-xs-12 col-sm-9 col-md-9" id="clientes-filtro-direccion">
                            <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <div class="form-group">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <input id="clientes-calle-val" name="calle" type="text" class="form-control cliente-search-val" placeholder="Calle">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="form-group">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <input id="clientes-numero-val" name="numero" type="text" class="form-control cliente-search-val" placeholder="Número">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 form-group">
                                    <label class="hidden-xs">&nbsp;</label>
                                    <select class="form-control" id="clientes-colonia-val" style="width: 100%;">
                                        <option value="">Sel. Colonia</option>
                                        {% for colonia in colonias %}
                                        <option value="{{ colonia.id }}">{{ colonia.nombre }}</option>
                                        {% endfor %}
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-1">
                            <label>&nbsp;</label>
                            <button class="btn btn-primary btn-block clientes-search-do" style="margin-bottom: 10px;" type="button" title="Buscar">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>

                    <table class="display table-striped table-hover" id="clientes-datatable" style="width: 100%;">
                        <thead>
                        <tr>
                            <th>Clave</th>
                            <th>Persona</th>
                        </tr>
                        </thead>
                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="btnSeleccionar" type="button" class="btn btn-primary">Seleccionar</button>
            </div>
        </div>
    </div>
</div>


<button id="btnMenu" type="button" class="btn btn-primary fade">Seleccionar</button>

<script type="application/javascript" src="../js/queja-events.js"></script>
<script type="application/javascript" src="../plugins/contextMenu/jquery.contextMenu.min.js"></script>
<script type="application/javascript" src="../plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="application/javascript" src="../plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>


<script>
    {% if
        acl.isAllowedUser('queja', 'edit')
        or  acl.isAllowedUser('queja', 'info')
        or acl.isAllowedUser('queja', 'delete')
    %}
        {% if acl.isAllowedUser('queja', 'info') %}
        buttons += '<button class="btn btn-primary btn-sm queja-info" type="button" title="Consultar">' +
            '<i class="fa fa-info"></i>' +
            '</button> ';
        {% endif %}

        {% if acl.isAllowedUser('queja', 'edit') %}
        buttons += '<button class="btn btn-primary btn-sm queja-edit" type="button" title="Modificar">' +
            '<i class="fa fa-pencil"></i>' +
            '</button> ';
        {% endif %}

        {% if acl.isAllowedUser('queja', 'delete') %}
        buttons += '<button class="btn btn-danger btn-sm queja-delete" type="button" title="¿Desea eliminar?">' +
            '<i class="fa fa-remove"></i>' +
            '</button> ';
        {% endif %}
    {% endif %}
    $(document).ready(function() {
        quejasDT = $('#quejas-datatable').DataTable({
            "scrollX": true,
            "searching": false,
            "select": true,
            "dom": '<"clear"><rt><"bottom"lip><"clear">',
            "autoWidth": true,
            "paging": true,
            "info": true,
            "processing": true,
            "scrollCollapse": false,
            "pagingType": "full",
            "language": {
                "paginate": {
                    "next": ">",
                    "first": "<<",
                    "last": ">>",
                    "previous": "<"
                },
                "search": "",
                "searchPlaceholder": "Buscar en los resultados encontrados",
                "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
                "infoEmpty": "",
                "infoFiltered": " - filtrado de _MAX_",
                "emptyTable": "Sin resultados",
                "sZeroRecords": "Sin resultados",
                processing: "Procesando ...",
                "lengthMenu": "Mostrar _MENU_ registros"
            },
            responsive: {
                details: {
                    type: 'column',
                    target: 1
                }
            },
            "columnDefs": [
                {
                    targets: [0],
                    orderable: false,
                    searchable: false,
                    defaultContent: buttons,
                    className: 'dt-center no-wrap',
                    {% if
                        not acl.isAllowedUser('queja', 'edit')
                        and  not acl.isAllowedUser('queja', 'info')
                        and not acl.isAllowedUser('queja', 'delete')
                    %}
                        visible: false
                    {% endif %}
                },
                { targets: [1], orderable: false, className: 'control', searchable: false},
                { targets: [2], searchable: true, className: 'dt-center'},
                { targets: [3], className: 'dt-center'},

                {
                    "targets": [ 7 ],
                    "visible": false
                },
                { targets: '_all', visible: true }
            ],
            "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
            "pageLength": 10,
            "order": [[ 3, 'desc' ]],
            "processing": true,
            "deferLoading": 0,
            "createdRow" : function( row, data, index ) {

                // Add identity if it specified
                if(!data.hasOwnProperty("id") ) {
                    row.id = "queja-" + data[3]            }
            },
            "fnDrawCallback": function( settings ) {
                if(quejasDT)
                    quejasDT.columns.adjust();
            }


        });

        $(window).on("resize", function(){
            quejasDT.columns.adjust();
        });



        {% if acl.isAllowedUser('queja', 'info') %}
        $("#quejas-datatable").off( 'click', '.queja-info');
        $("#quejas-datatable").on( 'click', '.queja-info', function () {
            var $tr = $(this).closest("tr");
            var idAttr = $tr.attr("id").split("-");
            var id = idAttr[1];
            if ( $tr.hasClass('selected') ) {
                //$tr.removeClass('selected');
            }
            else {
                quejasDT.$('tr.selected').removeClass('selected');
                $tr.addClass('selected');
            }

            $("#clave").val(id);
            loadQuejaInfo(id, "info");
            /* poner aqui la seccion para la consulta */
        });
        {% endif %}

        {% if acl.isAllowedUser('queja', 'edit') %}
        $("#quejas-datatable").off( 'click', '.queja-edit');
        $("#quejas-datatable").on( 'click', '.queja-edit', function () {
            var $tr = $(this).closest("tr");
            var idAttr = $tr.attr("id").split("-");
            var id = idAttr[1];
            if ( $tr.hasClass('selected') ) {
                //$tr.removeClass('selected');
            }
            else {
                quejasDT.$('tr.selected').removeClass('selected');
                $tr.addClass('selected');
            }


            /* poner aqui la seccion para la ediction */
            loadQuejaInfo(id, "edit");
        });
        {% endif %}

        {% if acl.isAllowedUser('queja', 'delete') %}
        $("#quejas-datatable").off( 'click', '.queja-delete');
        $("#quejas-datatable").on( 'click', '.queja-delete', function () {
            $(".popover.confirmation").confirmation("destroy");
            var $tr = $(this).closest("tr");
            var idAttr = $tr.attr("id").split("-");
            var id = idAttr[1];
            $(this).confirmation({
                rootSelector: "body",
                container: "body",
                singleton: true,
                popout: true,
                btnOkLabel: "Si",
                onConfirm: function() {

                    if ( $tr.hasClass('selected') ) {
                        //$tr.removeClass('selected');
                    }
                    else {
                        quejasDT.$('tr.selected').removeClass('selected');
                        $tr.addClass('selected');
                    }
                    eliminarQueja($tr, id);
                },
                onCancel: function() {
                    $(this).confirmation('destroy');
                },
            });

            $(this).confirmation("show");
        });
        {% endif %}


        $('#quejas-datatable tbody').on( 'click', 'tr', function () {

            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');

            }
            else {
                quejasDT.$('tr.selected').removeClass('selected');
                $(this).toggleClass('selected');

            }
        });
    });
</script>