{{ content() }}
{{ stylesheet_link('plugins/leaflet/leaflet.css') }}
{{ stylesheet_link('plugins/leaflet/leaflet-sidebar/css/leaflet-sidebar.css') }}

<style>
    #map { width: 100%; height: 100%; border: 1px solid #009C4B;}
    body {
        padding: 0;
        margin: 0;
    }

    #sidebar {
        height: 550px;
        opacity: 0.8;
    }

    html, body {
        height: 100%;
        font: 10pt "Helvetica Neue", Arial, Helvetica, sans-serif;
    }

    /*.sidebar-pane {*/
        /*padding: 0px;*/
    /*}*/
    /*.sidebar-content {*/
        /*overflow-y:hidden;*/
    /*}*/
    .sidebar-header-bg, .sidebar-tabs > li.active, .sidebar-tabs > ul > li.active{
        background-color: rgba(233, 106, 8, 0.6) !important;
    }

    /*#nav-filtros{*/
        /*height: 100%;*/
        /*overflow-y: hidden;*/
    /*}*/

    .filtro-clave-clear-do,
    .filtro-dispositivo-clear-do {
        position: absolute;
        right: 15px;
        z-index: 10;
        top: 0px;
        height: 33px;
        width: 30px;
        text-align: center;
        cursor: pointer;
    }


    .sidebar-pane.active{
        height: 100%;
    }
    .sidebar-pane.active .panel-body{
        height: calc(100% - 40px);
        overflow-y: auto;
    }

</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>SEGUIMIENTO COBRATARIOS</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div style="height: inherit !important; position:relative;">

    <!-- Sidebar Filtros -->
    <div id="sidebar" class="sidebar collapsed">
        <!-- Nav tabs -->
        <div class="sidebar-tabs">
            <ul role="tablist">
                <li><a href="#nav-filtros" role="tab"><i class="fa fa-search"></i></a></li>
                <li><a href="#Historial" role="tab"><i class="fa fa-road"></i></a></li>
            </ul>
        </div>

        <!-- Tab panes -->
        <div class="sidebar-content m-0">
            <!-- Start Sidebar Filtros Ubicación actual-->
            <div class="sidebar-pane" id="nav-filtros">
                <h1 class="sidebar-header sidebar-header-bg" >
                    Dispositivos
                    <span class="sidebar-close"><i class="fa fa-caret-left"></i></span>
                </h1>

                <div style="height: calc(100% - 40px);">
                    <div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Buscar por</label>
                                    <select class="form-control" id="ftipobusqueda">
                                        <option value="dispositivo" selected>Dispositivo</option>
                                        <option value="cobratario">Cobratario</option>
                                        {#<option value="clave">ID</option>#}
                                        {#<option value="">Todos</option>#}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- Dispositivo -->
                            <div class="col-xs-10" id="filtro-dispositivo">
                                <div class="form-group">
                                    {#<label>&nbsp;</label>#}
                                    <div class="filtro-dispositivo-clear-do" title="Limpiar">
                                        <i class="fa fa-close" style="margin-top: 10px;"></i>
                                    </div>
                                    <input type="text" class="form-control" id="fdispositivo" placeholder="Escriba el dispositivo">
                                </div>
                            </div>

                            <!-- usuario -->
                            <div class="col-xs-10" style="display: none" id="filtro-cobratario">
                                {#<label for="fcobratario">Cobratario</label>#}
                                <select class="form-control" id="fcobratario" style="width: 100%;">
                                    <option value="">Sel. cobratario</option>
                                    {% for cobratario in cobratarios %}
                                        <option value="{{ cobratario.id }}">{{ cobratario.usuario }}</option>
                                    {% endfor %}
                                </select>
                            </div>

                            <div class="col-xs-2">
                                <button class="btn btn-sm btn-primary" id="btnFiltrarCobs" type="button" title="Aplicar filtros"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    </br>
                    <div class="accordion" id="menu-rutas" style="height: calc(100% - 119px); overflow-y: auto;"></div>
                </div>
            </div>
            <!-- End Sidebar Filtros -->

            <!-- Start Sidebar Historial Ruta Buss -->
            <div class="sidebar-pane" id="Historial">
                <h1 class="sidebar-header sidebar-header-bg">Recorrido de dispositivos<span class="sidebar-close"><i class="fa fa-caret-left"></i></span></h1>
                <div style="height: calc(100% - 40px);">
                    <div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="fcobratarioseg">Dispositivo *</label>
                                    <select class="form-control" id="fcobratarioseg" style="width: 100%;">
                                        <option value="">Sel. cobratario</option>
                                        {% for disp in dispositivos %}
                                            <option value="{{ disp.dispositivo }}">{{ disp.dispositivo }} ({{ disp.usuario ? disp.usuario : "SIN ASIGNACION" }})</option>
                                        {% endfor %}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="form-group">
                                    <label for="ffecini">Fecha de Ini.</label>
                                    <input id="ffecini" type="text" class="form-control" placeholder="Fecha Inicial">
                                </div>
                            </div>
                            <div class="col-xs-5">
                                <div class="form-group">
                                    <label for="ffecfin">Fecha de Fin.</label>
                                    <input id="ffecfin" type="text" class="form-control" placeholder="Fecha Final">
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <strong>&nbsp;</strong>
                                <button id="btn-Search-Seguimiento" class="btn btn-sm btn-primary" type="button" title="Buscar"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="cargando-ruras"></div>
                        <div id="play-content" class="col-sm-12" style="display: none; height: calc(100% - 146px); overflow-y: auto;">
                            <div class="row">
                                <div class="col-xs-10">
                                    <strong>Velocidad</strong>
                                    <input type="range" min="1" max="10" step="0.5" value="1" class="slider" id="speedMarkerSeg">
                                </div>

                                <div class="col-xs-2 align-self-center">
                                    <button id="btn-Play-Seguimiento" class="btn btn-xs btn-primary" title="Comenzar">
                                        <i class="fa fa-play"></i>
                                    </button>
                                    <button id="btn-Pause-Seguimiento" class="btn btn-xs btn-primary" title="Detener" style="display: none;">
                                        <i class="fa fa-pause"></i>
                                    </button>
                                    {#<button id="btn-Delete-Seguimiento" class="btn btn-xs btn-primary" title="Eliminar">#}
                                        {#<i class="fa fa-remove"></i>#}
                                    {#</button>#}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 align-self-center">
                                    <b>Cobros realizados: </b><span id="total-cobros"></span>

                                    <table class="display table-striped table-hover" id="dt-cobros" style="width: 100%;">
                                        <!-- [Clave|Nombre,Razon Social|calle|Numero|Ultimo año de pago|ultimo mes de pago|Colonia|Adeudo] -->
                                        <thead>
                                        <tr>
                                            <th data-priority="0"></th>
                                            <th data-priority="1">Calle</th>
                                            <th data-priority="2">Numero</th>
                                            <th>Colonia</th>
                                            <th>Fecha</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End Sidebar Historial Ruta Buss -->

        </div>
    </div>

    <div style="height: 600px; width: 100%;" class="mx-auto p-1">
        <!--
        <div>
            <span class="badge badge-info">&nbsp;&nbsp;</span> <strong>Ruta programada</strong>
            <span class="badge badge-danger">&nbsp;&nbsp;</span> <strong>Ruta real</strong>
        </div>
        <br>
        -->
        <div class="custom-popup" id="map"></div>
    </div>

    <!-- Modal HTML embedded directly into document -->
    <div id="myModal" class="modal">
        <div id="TituloModal"></div>
        <!--<a href="#" rel="modal:close">Close</a>-->
        </br>
        <h6 id="BodyModal"></h6>
    </div>
</div>
</div>
{{ javascript_include("plugins/datetime-picker/es.js") }}
{{ javascript_include("plugins/datetime-picker/bootstrap-datetimepicker.min.js") }}
{{ javascript_include("plugins/leaflet/leaflet.js") }}
{{ javascript_include("plugins/leaflet/leaflet-sidebar/js/leaflet-sidebar.js") }}
{{ javascript_include("plugins/leaflet/Leaflet-MovingMarker/js/MovingMarker.js") }}
{{ javascript_include("plugins/leaflet/leaflet-easyprint/dist/bundle.js") }}
{{ javascript_include("js/trackcob/index-events.js") }}