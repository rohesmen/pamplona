{{ content() }}
<div id="page-title">
    <h1 class="fontRW-EB text-center f-blanco">SISTEMA ADMINISTRATIVO PAMPLONA {{ config.application.sede }}</h1>
</div>
<div class="row border-bottom dashboard-header">
    <div class="col-md-7">
        <img src="/img/pamplona_index_2.jpg" class="img-responsive" alt="Responsive image">
    </div>
    <div class="col-md-5">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color: #94c01e; color: white;">
                        <h5>Misión</h5>
                    </div>
                    <div class="ibox-content">
                        Preservar y mantener limpia la ciudad, prestando servicios de recolección de basura
                        con calidad y eficiencia, al mismo tiempo que se preserva la salud y se satisface al ciudadano.
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color: #94c01e; color: white;">
                        <h5>Visión</h5>
                    </div>
                    <div class="ibox-content">
                        Convertirnos en la primera Empresa Recolectora Yucateca con Equipos y Tecnología de
                        Vanguardia, mantener siempre y en todo momento satisfecho al Ciudadano prestándole
                        Servicio de Calidad con los equipos necesarios y acordes a sus necesidades, para crecer.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="plugins/flexslider/flexslider.css" type="text/css">
<style>
    .flexslider {
        margin-bottom: 0px;
    }

    .flex-control-nav {
        position: relative;
        bottom: auto;
    }

    .custom-navigation {
        background-color: white;
        display: table;
        width: 100%;
        table-layout: fixed;
        padding: 5px 10px;
    }

    .custom-navigation > * {
        display: table-cell;
    }

    .custom-navigation > a {
        width: 27px;
        overflow: hidden;
        font-size: 40px;
    }

    .custom-navigation .flex-next {
        text-align: right;

    }

    .custom-navigation a:before {
        font-family: "flexslider-icon";
        content: '\f001';
    }

    .custom-navigation .flex-next:before {
        text-align: right;
        content: '\f002';
    }
</style>
<script src="plugins/flexslider/jquery.flexslider.js"></script>
<script type="text/javascript" charset="utf-8">
    //    $(window).load(function() {
    //        $('.flexslider').flexslider({
    //            animation: "slide",
    //            controlsContainer: $(".custom-controls-container"),
    //            customDirectionNav: $(".custom-navigation a")
    //        });
    //    });
</script>