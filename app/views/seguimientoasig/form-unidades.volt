<div class="modal inmodal fade" id="unidadseguimiento-info-modal-UnidadMC" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header unidadseguimiento-modal-header-UnidadMC">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Consultar Unidad</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="idUnidadMC" disabled value="{{ unidad ? unidad.placa : '' }}">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label>Nombre *</label>
                            <input id="txtNombreUnidadMC" type="text" placeholder="Nombre *" class="form-control" disabled value="{{ unidad ? unidad.nombre : '' }}">
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Placa *</label>
                            <input id="txtPlacaUnidadMC" type="text" placeholder="Placa *" class="form-control" disabled value="{{ unidad ? unidad.placa : '' }}">
                        </div>
                        <div class="col-md-4 form-group">
                            <label>No. Economico</label>
                            <input id="txtNoEconomicoUnidadMC" type="text" placeholder="No. Economico" class="form-control" disabled value="{{ unidad ? unidad.numero_economico : '' }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Descripcion</label>
                            <textarea id="txtDescripcionUnidadMC" name="txtDescripcionUnidadMC" rows="8" placeholder="Descripcion" class="form-control" style="resize: none;" disabled>{{ unidad ? unidad.descripcion : '' }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>