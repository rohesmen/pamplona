{{ stylesheet_link('plugins/jQueryUI/jquery-ui.css') }}
<style>
    #calendar-asignacion td{
        border: 1px solid #CDCDCD;
        padding: 5px;
    }
    .unidades .panel-heading,
    .unidades .panel-body {
        padding: 3px 7px;
    }

    .unidades .panel {
        margin-bottom: 5px;
    }

    .unidades .caret {
        border-top: none;
        border-bottom: 4px dashed;
    }
    .unidades .collapsed .caret {
        border-top: 4px dashed;
        border-bottom: none;
    }
    #calendar-asignacion h5 {
        margin-bottom: 5px;
    }
    .cuadrilla,
    .turno {
        font-size: 10px;
    }
    .programado {
        background-color: #b7ffbc;
        text-align: center;
    }
    .programado.ui-droppable-hover {
        background-color: #CDCDCD;
    }
    .unidades .list-group-item {
        padding: 5px;
    }
    .unidades .list-group {
        margin-bottom: 5px;
    }

    .btnConsultarUnidad {
        background-color: transparent;
    }

    .item-divider{
        height: 1px;
        border: 1px solid #BDBDBD !important;
        margin-bottom: 10px !important;
    }
    .onoffswitch {
        width: 90px;
    }
    .onoffswitch-inner:before {
        content: "Chofer" !important;
    }
    .onoffswitch-inner:after {
        content: "Recolector" !important;
    }
    .onoffswitch-switch {
        right: 72px;
    }
    #modal-asignacion .popover {
        z-index: 10000;
    }

</style>
<!-- View Clientes -->
{{ content() }}

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-md-10">
        <h2>Asignación de recolección</h2>
    </div>
    <div class="col-md-2"></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        {% if acl.isAllowedUser('seguimientoasig', 'create') %}
        <div class="col-md-3 unidades">
            {% if unidades|length > 0 %}
            {% for data in unidades %}
                {% set u = data['unidad'] %}
                <div class="panel panel-default UnidadDraggable" data-placa={{ u.placa }}>
                    <div class="panel-heading">
                        <a  role="button" data-toggle="collapse"  href="#{{ 'uni'~u.placa }}"
                            aria-expanded="{{ loop.first ? 'true' : 'false' }}"
                            class="{{ loop.first ? '' : 'collapsed' }}"
                            aria-controls="{{ 'uni'~u.placa }}">
                            {{ u.numero_economico }} <span class="caret"></span>
                        </a>
                        <button class="btn btn-sm btnConsultarUnidad" title="Consultar Unidad" data-placa="{{ u.placa }}">
                            <i class="fa fa-info"></i>
                        </button>
                    </div>
                    <div class="panel-body" >
                        <div class="collapse {{ loop.first ? 'in' : '' }}" id="{{ 'uni'~u.placa }}">
                            <ul class="list-group">
                                {% for c in data["cuadrilla"] %}
                                {% set cobj = c['data'] %}
                                {% set recs = c['recolectores'] %}
                                    <li class="list-group-item">
                                        <span class="badge numerocuadrilla-{{ cobj.id }}" data-idcuadrilla="{{ cobj.id }}" data-nombrecuadrilla="{{ cobj.nombre }}">{{ recs|length }}</span>
                                        <span class="nombrecuadrilla-{{ cobj.id }}">{{ cobj.nombre }}</span>
                                    </li>
                                {% endfor %}
                            </ul>
                        </div>
                    </div>
                </div>
            {% endfor %}
            {% else %}
            <p>Sin unidades con asignación</p>
            {% endif %}
        </div>
        {% endif %}
        <div class="{{ acl.isAllowedUser('seguimientoasig', 'create') ? 'col-md-9' : 'col-md-12' }}">
            <table style="width: 100%" id="calendar-asignacion">
                <thead>
                    <tr>
                        <th>Rutas</th>
                        {% for d in dias %}
                            <th style="text-align: center;">{{ d.dia }}</th>
                        {% endfor %}
                    </tr>
                </thead>
                <tbody>
                {% for r in rutas %}
                <tr>
                    <td style="text-align: center; word-break: break-word; width: 30%;">{{ r.nombre }}</td>
                    {% for d in dias %}
                        <td class="droppableDias {{ recoleccion[d.sigla][r.id] is defined ? 'programado' : '' }}" id="{{ 't'~turnos[0].id~'d'~d.sigla~'r'~r.id }}"
                            data-id="{{ recoleccion[d.sigla][r.id] is defined ? recoleccion[d.sigla][r.id].id : '' }}"
                            data-idturno="{{ recoleccion[d.sigla][r.id] is defined ? recoleccion[d.sigla][r.id].idturno : '' }}"
                            data-placa="{{ recoleccion[d.sigla][r.id] is defined ? recoleccion[d.sigla][r.id].placa : '' }}"
                            data-ruta="{{ r.nombre }}" data-cuadrilla="{{ recoleccion[d.sigla][r.id] is defined ? recoleccion[d.sigla][r.id].idcuadrilla : '' }}"
                            data-sigladia="{{ d.sigla }}" data-dia="{{ d.dia }}" data-idruta="{{ r.id }}">
                            {% if recoleccion[d.sigla][r.id] is defined %}
                                {% set datarec = recoleccion[d.sigla][r.id] %}
                            <div>
                                <h5>{{ datarec.placa }}</h5>
                                <div class="cuadrilla nombrecuadrilla-{{ datarec.idcuadrilla }}">{{ datarec.cuadrilla  }}</div>
                                <div class="turno">{{ datarec.turno_nombre  }}</div>
                                <div class="acciones-unidad">
                                    {% if acl.isAllowedUser('seguimientoasig', 'deactivate') %}
                                    <button class="btn btn-primary btn-sm delete-asignacion-unidad" data-id="{{ datarec.id }}">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                    {% endif %}
                                </div>
                            </div>
                            {% endif %}
                        </td>
                    {% endfor %}
                </tr>
                {% endfor %}
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal agregar asignacion -->
<div class="modal inmodal fade" id="modal-asignacion" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header cliente-modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="title-modal-asignacion">Crear asignación</h4>
            </div>

            <div class="modal-body">
                <input type="hidden" id="addID">
                <input type="hidden" id="addIDTD">
                <input type="hidden" id="addDia">
                <input type="hidden" id="addIdRuta">
                <input type="hidden" id="addIdCuadrilla">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Ruta</label>
                            <input type="text" class="form-control input-sm" id="addTextRuta" disabled>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Dia</label>
                            <input type="text" class="form-control input-sm" id="addTextDia" disabled>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Placa</label>
                            <input type="text" class="form-control input-sm" id="addPlaca" disabled>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Turno *</label>
                            <select class="form-control" id="addCmbTurno">
                                <option value="">Seleccionar</option>
                                {% for data in turnos %}
                                    <option value="{{ data.id }}">{{ data.nombre }}</option>
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                </div>

                <div class="item-divider"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Cuadrilla *</label>
                            <select class="form-control" id="addCmbCuadrilla">
                                <option value="">Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class="control-label">Recolector *</label>
                            <select class="form-control" id="addCmbRecolector">
                                <option value="">Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        ¿Chofer? *
                        <div class="onoffswitch">
                            <input type="checkbox" class="onoffswitch-checkbox" id="addchkIsChofer">
                            <label class="onoffswitch-label" for="addchkIsChofer">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <button class="btn btn-block btn-info" id="addcreate_asignacion_recoleccion" title="Agregar">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row">
                        <table class="display table-striped table-hover" id="addcuadrilla_recoleccion-datatable" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th data-priority="1"></th>
                                    <th data-priority="2"></th>
                                    <th data-priority="3">Recolector</th>
                                    <th data-priority="4">Tipo</th>
                                    <th>idrecolector</th>
                                    <th>ischofer</th>
                                    <th>activo</th>
                                    <th>id</th>
                                    <th>edicion</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button id="btnGuardar" type="button" class="btn btn-primary" title="Guardar">Guardar</button>
            </div>
        </div>
    </div>
</div>

<div id="content-modal-unidades-consulta"></div>

<!-- /.modal agregar asignacion -->
<script>
    unidades = {{ unidades|json_encode }};
</script>
{{ javascript_include("plugins/bootbox/bootbox.min.js") }}
{{ javascript_include("plugins/jQueryUI/jquery-ui.js") }}
{{ javascript_include("js/seguimientoasig/index.js") }}
<!-- modulo de cuadrillas -->
{{ partial("cuadrillaseguimiento/form", ["mode": "create"]) }}