<table style="width: 100%" border="0" cellpadding="0" cellspacing="0" align="center" >
    <tbody>
    <tr>
        <td align="center" colspan="2"><b>{{ config.application.nom_com }}</b></td>
    </tr>
    <tr>
        <td align="center" colspan="2">{{ config.application.dir_pam }}</td>
    </tr>
    <tr>
        <td align="center" colspan="2">M&eacute;rida, Yucat&aacute;n, M&eacute;xico. R.F.C.: {{ config.application.rfcpam }}</td>
    </tr>
    <tr>
        <td align="right">Tel. {{ config.application.tel_pam }}</td>
        <td align="center">No.:  {{ folio }}</td>
    </tr>
    <tr>
        <td align="left" colspan="2">Nombre: {{ nombre }}</td>
    </tr>

    <tr>
        <td align="left" colspan="2">Direcci&oacute;n: {{ direccion }}</td>
    </tr>

    <tr>
        <td align="left" colspan="2">Colonia: {{ colonia }}</td>
    </tr>
    <tr>
        <td align="left" colspan="2">Municipio: {{ municipio }}</td>
    </tr>
    <tr>
        <td align="left" colspan="2">No. Cliente: {{ idcliente }}</td>
    </tr>
    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>

    <tr>
        <td colspan="2" align="left">RECIBO DE RECOLECCION EN EL DIA:  <label>{{ fecha_pago }}</label></td>
    </tr>
    <tr>
        <td colspan="2" align="left"> PAGO CORRESPONDIENTE AL MES (ES) DE:{{ meses }}</td>
    </tr>
    <tr>
        <td align="left" colspan="2" width="100%">Cantidad en letras</td>
    </tr>
    <tr>
        <td align="left" colspan="2"  width="100%">Son: {{ cantidad_letras }}</td>
    </tr>
    {% if ispagoanual %}
        <tr id="row-total-promo2">
            <td align="right" width="40%">Descuento Anual:  </td>
            <td align="right" width="60%"><label id="recibo-total-promo">{{ monto_anual}}</label></td>
        </tr>
        <tr id="row-total-promo2" >
            <td align="right" width="40%" colspan="2">Usted participa en la promoción anual derivado a su pago anual. Favor de comprobar este comprobante como boleto participante. Consulte las bases en {{ config.application.publicWebUrl }}</td>
        </tr>
    {% endif %}
    {% if comision %}
    <tr>
        <td align="right" width="40%">Subtotal: </td>
        <td align="right" width="60%">{% if comision %}{{ subtotal }}{% endif %}
        </td>
    </tr>
    <tr>
        <td align="right" width="40%">Comisión por pago en línea: </td>
        <td align="right" width="60%">{% if comision %}{{ comision }}{% endif %}
        </td>
    </tr>
    {% endif %}
        <tr>
            <td align="right" width="40%">Total pagado: </td>
            <td align="right" width="60%"><label>{{ cantidad }}</label></td>
        </tr>

    </tbody>
</table>