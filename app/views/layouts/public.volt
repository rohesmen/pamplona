<div id="wrapper">
    <?php $this->partial("layouts/side-bar"); ?>
    <div id="page-wrapper" class="orange-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    {#<form role="search" class="navbar-form-custom" action="http://webapplayers.com/inspinia_admin-v2.3/search_results.html">#}
                        {#<div class="form-group">#}
                            {#<input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">#}
                        {#</div>#}
                    {#</form>#}
                </div>
                <ul class="nav navbar-top-links navbar-right" id="menubar-height">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">Bienvenido {{ identity['userName'] }}</span>
                    </li>
                    <li class="dropdown" id="menu-usuario-perfil">
                        <a class="dropdown-toggle" title="Menú de usuario" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" data-toggle="modal" data-target="#user-info-modal"><i class="fa fa-user fa-fw"></i> Perfil</a>
                            </li>
                            {% if acl.isAllowedUser('users', 'changePassword') == true %}
                                <li><a href="#" id="btn-user-password-change-modal"><i class="fa fa-gear fa-fw"></i> Cambiar contraseña</a></li>
                            {% endif %}
                            <li class="divider"></li>
                            <li><a href="/session/logout"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                </ul>
            </nav>
        </div>
        {{ content() }}
    </div>
    <!-- /#page-wrapper -->

</div><!-- /#wrapper -->
<!-- Modal -->
<div class="modal fade" id="user-info-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Información</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label" for="user-info-usuario">Usuario:</label>
          <label class="militant-info-label" id="user-info-usuario">{{ identity['userName'] }}</label>
        </div>
        <div class="form-group">
          <label class="control-label" for="user-info-nombre">Nombre:</label>
          <label class="militant-info-label" id="user-info-nombre">{{ identity['fullName'] }}</label>
        </div>
        {%- if identity['assignament'] != '' %}
        <div class="form-group">
          <label class="control-label" for="user-info-asignacion">Tipo de consulta:</label>
          <label class="militant-info-label" id="user-info-asignacion">{{ identity['assignament'] }}</label>
        </div>
        <div class="form-group">
          <label class="control-label" for="user-info-asignacion">Militantes:</label>
          <label class="militant-info-label" id="user-info-asignacion">{{ identity['militants'] }}</label>
        </div>
        {% endif %}
        {%- if identity['general'] == true %}
          <div class="form-group">
            <label class="control-label" for="militant-info-general">Tipo de consulta:</label>
            <label class="militant-info-label" id="militant-info-general">General</label>
          </div>
        {% endif %}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Modal -->
{% if acl.isAllowedUser('users', 'changePassword') == true %}
<div class="modal fade" id="user-password-change-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Cambio de contraseña</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label" for="user-info-usuario">Usuario</label>
            <label class="col-sm-8 control-label militant-info-label" id="user-info-usuario" style="text-align: left;">{{ identity['userName'] }}</label>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label" for="user-password-before">Contraseña actual</label>
            <div class="col-sm-8">
              <input class="form-control" type="password" id="user-password-before" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label" for="user-password-new">Contraseña nueva</label>
            <div class="col-sm-8">
              <input class="form-control" type="password" id="user-password-new" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label" for="user-password-confirm">Confirmar contraseña</label>
            <div class="col-sm-8">
              <input class="form-control" type="password" id="user-password-confirm" value="">
            </div>
          </div>
        </div>
        <div class="row" style="margin-bottom: 10px;">
          <div class="col-lg-12" id="user-password-change-error"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="btn-user-password-change" class="btn btn-primary">Aceptar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- /.modal -->
{% endif %}

<div class="modal fade" id="general-modal" tabindex="-1" role="dialog" aria-labelledby="general-modal-Label" aria-hidden="true"></div>
<div class="modal fade" id="main-modal" tabindex="-1" role="dialog" aria-labelledby="main-modal-Label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>

<div class="modal fade" id="sesion-modal" tabindex="-1" role="dialog" aria-labelledby="main-modal-Label" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Inicio de sesión</h4>
            </div>
            <div class="modal-body">
                Su sesión ah expirado, se redigira al inicio se sesion en <br>
                <div id="countDownSession" style="text-align: center; font-size: 22px; font-weight: bold;">10</div>
            </div>
            <div class="modal-footer">
                <a href="/session/logout" class="btn btn-default">Aceptar</a>
            </div>
        </div>
    </div>
</div>