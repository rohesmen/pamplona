<nav class="navbar-default navbar-static-side" role="navigation" style="height: 100%">
    <div class="sidebar-collapse" style="height: 100%">
        <ul class="nav metismenu" id="side-menu" style="height: 100%">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <img src="../img/logo_pamplona.png" class="img-responsive">
                        </span>
                    </a>
                </div>
                <div class="logo-element">P</div>
            </li>
            <li>
                <a href="/"><i class="fa fa-home fa-fw"></i> <span class="nav-label">Inicio</span></a>
            </li>
            {% for menu in identity['menu'] %}
                {% if menu.modulos|length > 1 %}
                    <li>
                        <a href="{{ menu.ruta }}1">
                            <i class="fa {{ menu.icono }}"></i>
                            <span class="nav-label">{{ menu.nombre }}</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            {% for modulo in menu.modulos %}
                                <li>
                                    <a href="{{ modulo.ruta }}">{{ modulo.nombre }}</a>
                                </li>
                            {% endfor %}
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                {% else %}
                    <li>
                        <a href="{{ menu.modulos[0].ruta }}">
                            <i class="fa {{ menu.icono }}"></i>
                            <span class="nav-label">{{ menu.modulos[0].nombre }}</span>
                        </a>
                        <!-- /.nav-second-level -->
                    </li>
                {% endif %}

            {% endfor %}
        </ul>
    </div>
</nav>