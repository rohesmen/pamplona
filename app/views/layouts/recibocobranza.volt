<html style="width: 100%">
<body style="width: 100%">
<table style="width: 100%;font-family:helvetica,arial,sans-serif;font-size:14px;color:#666666" border="0" cellpadding="0" cellspacing="0" align="center" >
    <tbody>
    <tr>
        <td width="100%" colspan="2" valign="top" style="text-align:center" align="center">
            <img src="{{ config.application.publicUrl }}/img/logo_pamplona.png" style="max-width:700px;margin:20px 0px">
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2"><b>{{ config.application.nom_com }}</b></td>
    </tr>
    <tr>
        <td align="center" colspan="2">{{ config.application.dir_pam }}</td>
    </tr>
    <tr>
        <td align="center" colspan="2">M&eacute;rida, Yucat&aacute;n, M&eacute;xico. R.F.C.: {{ config.application.rfcpam }}</td>
    </tr>
    <tr>
        <td align="right">Tel. {{ config.application.tel_pam }}</td>
        <td align="center">No.:  {{ folio }}</td>
    </tr>
    <tr>
        <td align="left" colspan="2">Nombre: {{ nombre }}</td>
    </tr>

    <tr>
        <td align="left" colspan="2">Direcci&oacute;n: {{ direccion }}</td>
    </tr>
    <tr>
        <td align="left" colspan="2">Municipio: {{ municipio }}</td>
    </tr>
    <tr>
        <td align="left" colspan="2">No. Cliente: {{ idcliente }}</td>
    </tr>
    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>

    <tr>
        <td colspan="2" align="left">RECIBO DE RECOLECCION EN EL DIA:  <label>{{ fecha_pago }}</label></td>
    </tr>
    <tr>
        <td colspan="2" align="left"> PAGO CORRESPONDIENTE AL MES (ES) DE:</td>
    </tr>
    <tr>
        <td colspan="2" align="left"> {{ meses }}</td>
    </tr>
    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
    {% if isnegociacion %}
        <tr id="row-total-promo2">
            <td align="right" width="40%">Total: </td>
            <td align="right" width="60%"><label id="recibo-total-promo">{{ total_pago}}</label></td>
        </tr>
        <tr id="row-total-promo2">
            <td align="left" width="60%">Descuento: </td>
            <td align="right" width="40%"><label id="recibo-total-promo">{{ descuento_negociacion}}</label></td>
        </tr>
    {% elseif ispagoanual %}
        <tr id="row-total-promo2">
            <td align="left" width="60%">Descuento Anual:  </td>
            <td align="right" width="40%"><label id="recibo-total-promo">{{ monto_anual}}</label></td>
        </tr>

    {% elseif descuento > 0 %}
        <tr id="row-total-promo2">
            <td align="right" width="40%">Total: </td>
            <td align="right" width="60%"><label id="recibo-total-promo">{{ total_pago}}</label></td>
        </tr>
        <tr id="row-total-promo2">
            <td align="left" width="60%">Descuento:  </td>
            <td align="right" width="40%"><label id="recibo-total-promo">{{ format_descuento}}</label></td>
        </tr>

    {% endif %}
    <tr>
        <td align="left" width="60%">Cantidad en letras</td>
        <td align="right" width="40%"></td>
    </tr>
    <tr>
        <td align="left" width="60%">Son: {{ cantidad_letras }}</td>
        <td align="right" width="40%">Total pagado: <label>{{ cantidad }}</label></td>
    </tr>
    <tr>
        <td align="left" colspan="2" width="100%">
            AGENTE DE COBRANZA: <label id="usuario-cobranza">{{ identity['userName'] }}</label>
        </td>
    </tr>
    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
    {% if ispagoanual %}
        <tr id="row-total-promo2" >
            <td align="left" width="100%" colspan="2">Usted participa en la promoción anual derivado a su pago anual. Favor de comprobar este comprobante como boleto participante. Consulte las bases en {{ config.application.publicWebUrl }}</td>
        </tr>
    {% endif %}
    </tbody>
</table>
</body>
</html>