<html>
<head></head>
<body style="background-color: #E4E4E4;padding: 20px; margin: 0; min-width: 640px;">
	<table style="width: 100%;font-family:helvetica,arial,sans-serif;font-size:14px;border:1px solid;color:#666666" border="0" cellpadding="0" cellspacing="0" align="center" >
		<thead>
			<tr>
				<td width="100%" valign="top" style="text-align:center" align="center">
					<img src="{{ config.application.publicUrl }}/img/logo_pamplona.png" style="max-width:700px;margin:20px 0px">
				</td>
			</tr>
			<tr>
				<td align="center"><b>{{ config.application.nom_com }}</b></td>
			</tr>
			<tr>
				<td align="center">{{ config.application.dir_pam }}</td>
			</tr>
			<tr>
				<td align="center">M&eacute;rida, Yucat&aacute;n, M&eacute;xico.</td>
			</tr>
			<tr>
				<td align="center">R.F.C.: {{ config.application.rfcpam }}</td>
			</tr>
			<tr>
				<td align="center">Tel. {{ config.application.tel_pam }}</td>
			</tr>
		</thead>
		{{ content() }}
	</table>

	</body>
</html>