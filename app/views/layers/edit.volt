<style type="text/css">
.btn-primary:hover, .btn-primary:active, .btn-primary.active, .open>.dropdown-toggle.btn-primary {
    background-color: #004e8d;
}
.select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #e9e9e9;
    border-radius: 0px;
}

/*div.panel.panel-danger table tr td{
	color: red !important;
}*/
tr.fila-inactiva > td{
	color: red !important;
}
</style>

{{ content() }}
<?php $this->flashSession->output() ?>

<!--Switchery [ OPTIONAL ]-->
<link href="/plugins/switchery/switchery.min.css" rel="stylesheet">
<!--Switchery [ OPTIONAL ]-->
<script src="/plugins/switchery/switchery.min.js"></script>

{# <!--Bootstrap Table [ OPTIONAL ]-->
<link href="/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
<!--Bootstrap Table [ OPTIONAL ]-->
<script src="/plugins/bootstrap-table/bootstrap-table.min.js"></script> #}

<div id="page-title">
	<div id="titulo-modulo"><h1 class="page-header text-overflow">{{ moduleTitle }}</h1></div>
	<div id="titulo-acciones">
		{% if acl.isAllowedUser('layers', 'create') %}
			<button class="btn btn-primary btn-sm btnEditLayer">
				<i class="fa fa-save"></i>
				<span class="hidden-xs hidden-sm">Guardar</span>
			</button>
		{% endif %}

		<a href="/layers" class="btn btn-warning btn-sm titulo-acciones-pull-right hidden-md hidden-lg usuario-cancelar">
			<i class="fa fa-remove"></i>
			<span class="hidden-xs hidden-sm">Cancelar</span>
		</a>
		<a href="/layers" class="btn btn-warning btn-sm titulo-acciones-pull-right hidden-xs hidden-sm usuario-cancelar" style="margin-right: 6px;">
			<i class="fa fa-remove"></i>
			<span class="hidden-xs hidden-sm">Cancelar</span>
		</a>
	</div>
</div>

<div id="page-content">
	<div class="tab-base">
		<!--Nav Tabs-->
		<ul class="nav nav-tabs" role="tablist">
			<li class="active" role="presentation">
				<a data-toggle="tab" href="#tab-nuevo" aria-controls="tab-nuevo" role="tab">Nuevo</a>
			</li>
		</ul>

		<!--Tabs Content-->
		<div class="tab-content">
			<div role="tabpanel" id="tab-nuevo" class="tab-pane fade active in">
				<form class="form-horizontal">
					<div class="container">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtClave">Clave</label>
							<div class="col-sm-3">
								<input type="text" id="txtClave" name="txtClave" class="form-control" disabled="disabled" value="{{ infolayer.id }}">
							</div>

							<label class="col-sm-2 control-label" for="chkActivo">Activo</label>
							<div class="col-sm-1">
								<input type="checkbox" id="chkActivo" {% if infolayer.activo %} checked="checked" {% endif %}>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtNombre">Nombre *</label>
							<div class="col-sm-3">
								<input type="text" id="txtNombre" name="txtNombre" class="form-control save-required" placeholder="Nombre" value="{{ infolayer.nombre }}">
							</div>
							
							<label class="col-sm-2 control-label" for="txtCapa">Capa</label>
							<div class="col-sm-3">
								<input type="text" id="txtCapa" name="txtCapa" class="form-control" placeholder="Capa" value="{{ infolayer.capa }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtDescripcion">Descripcion</label>
							<div class="col-sm-3">
								<input type="text" id="txtDescripcion" name="txtDescripcion" class="form-control" placeholder="Descripcion" value="{{ infolayer.descripcion }}">
							</div>
							
							<label class="col-sm-2 control-label" for="chkConEtiqueta">Con Etiqueta</label>
							<div class="col-sm-1">
								<input type="checkbox" id="chkConEtiqueta" {% if infolayer.conetiqueta %} checked="checked" {% endif %}>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtEstilo">Estilo</label>
							<div class="col-sm-3">
								<input type="text" id="txtEstilo" name="txtEstilo" class="form-control" placeholder="Estilo" value="{{ infolayer.estilo }}">
							</div>
							<label class="col-sm-2 control-label" for="txtEstiloEtiqueta">Estilo Etiqueta</label>
							<div class="col-sm-3">
								<input type="text" id="txtEstiloEtiqueta" name="txtEstiloEtiqueta" class="form-control" placeholder="Estilo Etiqueta" value="{{ infolayer.estilo_etiqueta }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtEsquema">Esquema</label>
							<div class="col-sm-3">
								<input type="text" id="txtEsquema" name="txtEsquema" class="form-control" placeholder="Esquema" value="{{ infolayer.esquema }}">
							</div>
							<label class="col-sm-2 control-label" for="txtTabla">Tabla</label>
							<div class="col-sm-3">
								<input type="text" id="txtTabla" name="txtTabla" class="form-control" placeholder="Tabla" value="{{ infolayer.tabla }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtOrden">Orden *</label>
							<div class="col-sm-3">
								<input type="text" id="txtOrden" name="txtOrden" class="form-control save-required" placeholder="Orden" value="{{ infolayer.orden }}">
							</div>
							<label class="col-sm-2 control-label" for="slcGrupos">Grupo *</label>
							<div class="col-sm-3">
								 {{ form.render("slcGrupos") }}
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>

{#<script type="application/javascript" src="/plugins/dropzone/dropzone.min.js"></script>#}
<script type="application/javascript" src="/js/layers/edit-events.js"></script>
<script type="application/javascript" src="/js/layers/edit-functions.js"></script>
<link rel="stylesheet" href="/plugins/jstree-bootstrap/themes/proton/style.min.css" />
<script src="/plugins/jstree-bootstrap/jstree.min.js"></script>