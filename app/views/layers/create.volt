{{ content() }}
<style type="text/css">
.btn-primary:hover, .btn-primary:active, .btn-primary.active, .open>.dropdown-toggle.btn-primary {
    background-color: #004e8d;
}
.select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #e9e9e9;
    border-radius: 0px;
}

/*div.panel.panel-danger table tr td{
	color: red !important;
}*/
tr.fila-inactiva > td{
	color: red !important;	
}
</style>

<!--Switchery [ OPTIONAL ]-->
{{ stylesheet_link('plugins/switchery/switchery.min.css') }}
<!--Switchery [ OPTIONAL ]-->
{{ javascript_include("plugins/switchery/switchery.min.js") }}


<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-md-6">
		<h2>Creación de capas</h2>
	</div>
	<div class="col-md-6 text-right">
		{% if acl.isAllowedUser('layers', 'create') %}
			<button class="btn btn-primary btn-sm btnSaveLayer">
				<i class="fa fa-save"></i>
				<span class="hidden-xs hidden-sm">Guardar</span>
			</button>
		{% endif %}

		<a href="/layers" class="btn btn-warning btn-sm titulo-acciones-pull-right hidden-md hidden-lg usuario-cancelar">
			<i class="fa fa-remove"></i>
			<span class="hidden-xs hidden-sm">Cancelar</span>
		</a>
		<a href="/layers" class="btn btn-warning btn-sm titulo-acciones-pull-right hidden-xs hidden-sm usuario-cancelar" style="margin-right: 6px;">
			<i class="fa fa-remove"></i>
			<span class="hidden-xs hidden-sm"> Cancelar</span>
		</a>

	</div>
</div>

<div id="page-content">
	<div class="wrapper wrapper-content animated fadeInRight bg-white">
		<div class="tab-base">
		<!--Nav Tabs-->
		<ul class="nav nav-tabs" role="tablist">
			<li class="active" role="presentation">
				<a data-toggle="tab" href="#tab-nuevo" aria-controls="tab-nuevo" role="tab">Nuevo</a>
			</li>
		</ul>

		<!--Tabs Content-->
		<div class="tab-content" style="padding-top: 15px;">
			<div role="tabpanel" id="tab-nuevo" class="tab-pane fade active in">
				<form class="form-horizontal">
					<div class="container">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtClave">Clave</label>
							<div class="col-sm-3">
								{{ form.render("txtClave") }}
							</div>

							<label class="col-sm-2 control-label" for="chkActivo">Activo</label>
							<div class="col-sm-1">
								<input type="checkbox" id="chkActivo">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtNombre">Nombre *</label>
							<div class="col-sm-3">
								{{ form.render("txtNombre") }}
							</div>
							
							<label class="col-sm-2 control-label" for="txtCapa">Capa</label>
							<div class="col-sm-3">
								{{ form.render("txtCapa") }}
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtDescripcion">Descripcion</label>
							<div class="col-sm-3">
								{{ form.render("txtDescripcion") }}
							</div>
							
							<label class="col-sm-2 control-label" for="chkConEtiqueta">Con Etiqueta</label>
							<div class="col-sm-1">
								<input type="checkbox" id="chkConEtiqueta">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtEstilo">Estilo</label>
							<div class="col-sm-3">
								{{ form.render("txtEstilo") }}
							</div>
							
							<label class="col-sm-2 control-label" for="txtEstiloEtiqueta">Estilo Etiqueta</label>
							<div class="col-sm-3">
								{{ form.render("txtEstiloEtiqueta") }}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtEstiloEtiqueta">Esquema</label>
							<div class="col-sm-3">
								{{ form.render("txtEsquema") }}
							</div>
							
							<label class="col-sm-2 control-label" for="chkCql">Tabla</label>
							<div class="col-sm-3">
								{{ form.render("txtTabla") }}
							</div>
						</div>
												
						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtOrden">Orden *</label>
							<div class="col-sm-3">
								{{ form.render("txtOrden") }}
							</div>
							<label class="col-sm-2 control-label" for="slcGrupos">Grupo *</label>
							<div class="col-sm-3">
								 {{ form.render("slcGrupos") }}
							</div>
						</div>
						<div class="form-group">
							
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	</div>
</div>

{{ javascript_include("js/layers/create-events.js") }}
{{ javascript_include("js/layers/create-functions.js") }}
{{ stylesheet_link('plugins/jstree-bootstrap/themes/proton/style.min.css') }}
{{ javascript_include("plugins/jstree-bootstrap/jstree.min.js") }}