<!-- View Bitacora -->
{{ content() }}

<link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
<link href="../plugins/datepicker/bootstrap-datepicker.css" rel="stylesheet">
<link href="../plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="../plugins/jsgrid-1.5.3/jsgrid.css" rel="stylesheet">
<link href="../plugins/jsgrid-1.5.3/jsgrid-theme.css" rel="stylesheet">
<link href="css/excel2007.css" rel="stylesheet">




<div id="page-title">
	<div id="titulo-modulo"><h1 class="page-header text-overflow">Bit&aacute;cora registro</h1></div>
	<div id="titulo-acciones">
		
	</div>
</div>

<ol class="breadcrumb">
	<li><a href="/">Inicio</a></li>
	<li><a href="/bitacora">Bit&aacute;cora Registro</a></li>
</ol>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    {% include "bitacora/filter.volt" %}
                    <div style="margin-top: 15px;">
						<div class="row" style="margin-top: 10px;" id="bitacora-datatable-container"></div>
						<div id="bitacora-datatable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- view content -->


{{ javascript_include("plugins/validate/jquery.validate.min.js") }}
{{ javascript_include("js/bitacora-events.js") }}
{{ javascript_include("plugins/datepicker/bootstrap-datepicker.js") }}
{{ javascript_include("plugins/bootstrap-timepicker/js/bootstrap-timepicker.js") }}
{{ javascript_include("plugins/datepicker/locales/bootstrap-datepicker.es.js") }}
{{ javascript_include("plugins/jsgrid-1.5.3/jsgrid.js") }}

<script type="application/javascript">
    function initTable(data, choferes, recolectores, turnos, supervisores, coordinadores){
		var TimeField = function(config) {
			jsGrid.Field.call(this, config);
		};
		TimeField.prototype = new jsGrid.Field({
			itemTemplate: function(value) {
				if(value){
					var vReturn = value.split(":");
					return vReturn[0]+":"+ vReturn[1];
				}else{
					return "";
				}
			},
	 
			insertTemplate: function(value) {
				return this._insertPicker = $('<input>').timepicker('showWidget');
			},
	 
			editTemplate: function(value) {
				return this._editPicker = $("<input>").timepicker('setTime', value);
			},
	 
			insertValue: function() {
				return this._insertPicker.val();
			},
	 
			editValue: function() {
				return this._editPicker.val();
			}
		});
	 
		jsGrid.fields.myTimeField = TimeField;
	
		$("#bitacora-datatable").jsGrid({
			width: "100%",
			height: "auto",
			editing: true,
			sorting: true,
			paging: true,
			data: data,
			pageIndex: 1,
			pageSize: 20,
			pageButtonCount: 10,
			pagerFormat: "Pag.: {first} {prev} {pages} {next} {last}    {pageIndex} de {pageCount}",
			pagePrevText: "<",
			pageNextText: ">",
			pageFirstText: "<<",
			pageLastText: ">>",
			pageNavigatorNextText: "...",
			pageNavigatorPrevText: "...",
			fields: [
				{ name: "id", type: "number", visible:false },
				{ name: "dia", title:"Dia", type: "text", width: 90, editing:false },
				{ name: "fecha", title:"Fecha", type: "text", width: 90, editing:false },
				{ name: "unidad", title:"Unidad",type: "text", width: 90, editing:false },
				{ name: "idunidad", visible:false,type: "number"},
				{ name: "idturno", title:"Turno",type: "select", items: turnos, valueField: "id", textField: "nombre",
					validate: { message: "Selecciona un turno", validator: function(value) { return value > 0; } }
				},
				{ name: "horasalida", title:"Hora Salida",type: "myTimeField", width: 100 },
				{ name: "horaregreso", title:"Hora Regreso", type: "myTimeField", width: 100 },
				{ name: "totalhoras", title:"Total Horas", type: "myTimeField", width: 100 },
				{ name: "kilometraje", title:"Kilometraje",type: "text", width: 90 ,validate: { message: "Kilometraje Obligatorio", validator: function(value) { return value != ""; } }},
				{ name: "vueltasrelleno", title:"Vueltas Relleno",type: "text", width: 110 },
				{ name: "tiempomuerto", title:"Tiempo Muerto",type: "number", width: 110 , validate: {
					message: "El valor no puede ser negativo", validator: function(value) {
						if(value == undefined){
							return true;
						}else{
							return value >= 0; 	
						}
					} 
				}},
				{ name: "idsupervisor", title:"Supervisor",type: "select", items: supervisores, valueField: "id", textField: "nombre", validate: { message: "Seleccione un Supervisor", validator: function(value) { return value > 0; } }},
				{ name: "idcoordinador", title:"Coordinador",type: "select", items: coordinadores, valueField: "id", textField: "nombre" , validate: { message: "Seleccione un Coordinador", validator: function(value) { return value > 0; } }},
				{ name: "idchofer", title:"Chofer",type: "select", items: choferes, valueField: "id", textField: "nombre" , validate: { message: "Seleccione un Chofer", validator: function(value) { return value > 0; } }},
				{ name: "idrecolector1", title:"Recolector",type: "select", items: recolectores, valueField: "id", textField: "nombre" },
				{ name: "idrecolector2", title:"Recolector",type: "select", items: recolectores, valueField: "id", textField: "nombre" },
				{ name: "idrecolector3", title:"Recolector",type: "select", items: recolectores, valueField: "id", textField: "nombre" },
				{ name: "observaciones", title:"Observaciones", type: "text", width: 120 },
				{ name: "idusuario", visible:false,type: "number"},
				{ name: "usuario", title:"Usuario", type: "text", editing:false},
				{ type: "control" , deleteButton: false}
			],
			invalidNotify:function(args) {
				var messages = $.map(args.errors, function(error) {
					return "- "+error.message+"<br>";
				});
				showGeneralMessage(messages.join("\n"), "error", true);
			},
			onItemUpdated: function(args) {
				var data = {
						fecha: args.item.fecha,
						dia: args.item.dia,
						horaregreso: args.item.horaregreso,
						horasalida: args.item.horasalida,
						id: args.item.id,
						idchofer: args.item.idchofer,
						idsupervisor: args.item.idsupervisor,
						idcoordinador: args.item.idcoordinador,
						idturno: args.item.idturno,
						idunidad: args.item.idunidad,
						idrecolector1: args.item.idrecolector1,
						idrecolector2: args.item.idrecolector2,
						idrecolector3: args.item.idrecolector3,
						kilometraje: args.item.kilometraje,
						observaciones: args.item.observaciones,
						tiempomuerto: args.item.tiempomuerto,
						totalhoras: args.item.totalhoras,
						vueltasrelleno: args.item.vueltasrelleno
					}
					$.ajax({
						url: "/bitacora/save",
						data: JSON.stringify(data),
						type: "POST",
						beforeSend: function(){
							$('#bitacora-datatable-container').html('<div style="text-align: center;">' +
								'<i class="fa fa-spinner fa-pulse fa-2x"></i> Cargando...' +
								'</div>');
						},
						success: function (resp) {
							var resp2 = JSON.parse(resp);
							if(resp2.ok){
								args.item.id = resp2.id;
								$("#bitacora-datatable").jsGrid("setRowData", args.item);
								showGeneralMessage("Guardado exitoso", "success", true);
							}else{
								showGeneralMessage("Ocurrió un error al guardar el registro", "error", true);
							}
						},
						/*error: function(){
							showGeneralMessage("Ocurrió un error al guardar el registro", "error", true);
						},*/
						complete: function () {
							$('#bitacora-datatable-container').empty();
						}
					});
			},
			onDataLoaded: function(grid, data){
				grid.jsGrid("sort", { field: "idunidad", order: "asc" });
			}
		});	
	}
</script>