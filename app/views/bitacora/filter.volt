<div style="border: 1px solid #ddd;">
    <nav class="navbar navbar-default" style="border-color: #000000; margin-bottom: 0px;">
        <div>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header" style="float: none; display: block;">
                <button type="button" id="colapse-busqueda" class="navbar-toggle" data-toggle="collapse"
                        data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse in clientes-only" id="contenedor-filtros" aria-expanded="true">
                <div class="nav-bar">
                    <!-- filtros -->
					<div class="col-xs-12 col-sm-4 col-md-6 col-lg-4" id="clientes-filtro-direccion">
                         <div class="row">
							<div class="col-md-12 col-sm-12 form-group">
									<label for="fInicio">Fecha</label>
									<div class="input-group date">
                                        <input id="fInicio" name="fInicio" type="text" class="form-control" required>
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>
							</div>
						</div>
                    </div>
                    <!-- boton nuevo -->
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-2" style="text-align: right;">
                        <label>&nbsp;</label>
						<button class="btn btn-primary btn-block" type="button" title="Generar" style="display: inline-block; margin-bottom: 10px;"
								id="generarBitacora">
							Generar
						</button>
                    </div>
                </div>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>