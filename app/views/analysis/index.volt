{% if DISABLE_NAVBAR %}
    {% set HIDE_POSITION = "-300px" %}
{% else %}
    {% set HIDE_POSITION = "-250px" %}
{% endif %}
{% set SHOW_POSITION = "0px" %}
{{ content() }}
{{ stylesheet_link('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}
{{ stylesheet_link('plugins/datatables_new/Select-1.2.0/css/select.dataTables.min.css') }}
{{ javascript_include("plugins/datatables_new/Select-1.2.0/js/dataTables.select.min.js") }}
<style>

    .btn-group-sm>.btn, .btn-xs {
        padding: 4px 6px;
    }
    #ine-datatable_wrapper {
        padding-bottom: 0px;
    }
    .panel-heading {
         padding: 0px;
    }
    .panel-control {
        height: 100%;
        position: relative;
        float: right;
        padding: 15px 15px;
    }

    .panel-title {
        font-weight: 600;
        padding: 0 20px 0 20px;
        font-size: 1.2em;
        line-height: 50px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .select2-container .select2-selection {
        height: 34px;
        overflow-y: auto;
    }

    ::-ms-clear {
        display: none;
    }

    .form-control-clear {
        z-index: 10;
        pointer-events: auto;
        cursor: pointer;
        padding: 10px;
    }

    .mainnav-lg #resultados {
        left: -80px;
        transition: 0.35s;
    }

    .mainnav-lg #resultados.open {
        left: 220px;
        transition: 0.35s;
    }

    .mainnav-sm #resultados {
        left: -250px;
        transition: 0.35s;
    }

    .mainnav-sm #resultados.open {
        left: 50px;
        transition: 0.35s;
    }

    #resultados.open{
        left: 0px;
    }

    #resultados{
        transition: 0.35s;
        height: 450px;
        width: 300px;
        background: white;
        position: absolute;
        border-right: 1px solid #d1d9de;
        z-index: 3;
        left: -300px;
        padding: 10px;
    }

    @media (max-width: 767px) {
        .summary-geoadmin.shown .sum-label label {
            width: auto;
        }
    }
    @media (max-width: 767px) {
        .mainnav-sm #resultados {
            left: -300px;
        }

        .mainnav-sm #resultados.open {
            left: 0px;
        }

        .sum-table i {
            display: none;
        }

        #tabla-datos > div {
            display: none;
        }

        .summary-geoadmin.shown {
            width: calc(100% - 15px);
            z-index: 3;
            max-height: 100%;
            -webkit-animation: slide-up-fade-in .3s ease-out;
            -moz-animation: slide-up-fade-in .3s ease-out;
        }
        .summary-geoadmin > div{
            top: 0px !important;
            left: 0px !important;
        }
        .getlegend-geoadmin.shown {
            width: calc(100% - 15px);
            z-index: 3;
            max-height: 100%;
            -webkit-animation: slide-up-fade-in .3s ease-out;
            -moz-animation: slide-up-fade-in .3s ease-out;
        }
        .getlegend-geoadmin > div {
            top: 0px !important;
            left: 0px !important;
        }

        #predio-info {
            width: calc(100% - 30px) !important;
            bottom: 0px !important;
            left: 15px !important;
            top: inherit !important;
            max-height: 100%;
            -webkit-animation: slide-up-fade-in .3s ease-out;
            -moz-animation: slide-up-fade-in .3s ease-out;
            z-index: 2;
        }

        #predio-info .tab-content-outer {
            width: 100% !important;
        }

        #detail-feature-info {
            width: calc(100% - 60px) !important;
            bottom: 0px !important;
            left: 30px !important;
            top: inherit !important;
            max-height: 100%;
            -webkit-animation: slide-up-fade-in .3s ease-out;
            -moz-animation: slide-up-fade-in .3s ease-out;
        }

        #map-streetview-container {
            bottom: 0px !important;
            left: 15px !important;
            top: inherit !important;
            max-height: 100%;
            -webkit-animation: slide-up-fade-in .3s ease-out;
            -moz-animation: slide-up-fade-in .3s ease-out;
        }

        #map-streetview-container.show{
            width: calc(100% - 30px);
        }

        #map-streetview-container .panel-body {
            width: 100% !important;
        }

        .summary-geoadmin ul li table{
            width: 100%;
        }
        .sum-legend{
            width: 25px;
        }
        .sum-switch{
            width: 25px !important;
        }

        span.select2.select2-container.select2-container--bootstrap.select2-container--below {
            margin-bottom: 15px;
        }

        .layer-switcher.shown {
            top: 0px !important;
            width: calc(100% - 15px);
            -webkit-animation: slide-down .3s ease-out;
            -moz-animation: slide-down .3s ease-out;
        }
    }

    @media (min-width: 768px) and (max-width: 1024px) {
        .getlegend-geoadmin.shown {
            width: 450px;
            z-index: 1;
            max-height: 100%;
            -webkit-animation: slide-up-fade-in .3s ease-out;
            -moz-animation: slide-up-fade-in .3s ease-out;
        }

        .getlegend-geoadmin > div {
            top: 0px !important;
            left: -50% !important;
        }

        .summary-geoadmin.shown {
            width: 450px;
            z-index: 3;
            max-height: 100%;
            -webkit-animation: slide-up-fade-in .3s ease-out;
            -moz-animation: slide-up-fade-in .3s ease-out;
        }

        .summary-geoadmin > div {
            top: 0px !important;
            left: 50% !important;
        }
        .layer-switcher.shown {
            top: 0px !important;
        }
    }

    @-webkit-keyframes slide-down {
        0% { opacity: 0; -webkit-transform: translateY(-100%); }
        100% { opacity: 1; -webkit-transform: translateY(0); }
    }
    @-moz-keyframes slide-down {
        0% { opacity: 0; -moz-transform: translateY(-100%); }
        100% { opacity: 1; -moz-transform: translateY(0); }
    }

    @keyframes slide-up-fade-in{
        0% {
            opacity:0;
            transform:  translate(0px,40px)  ;
        }
        100% {
            opacity:1;
            transform:  translate(0px,0px)  ;
        }
    }

    @-moz-keyframes slide-up-fade-in{
        0% {
            opacity:0;
            -moz-transform:  translate(0px,40px)  ;
        }
        100% {
            opacity:1;
            -moz-transform:  translate(0px,0px)  ;
        }
    }

    @-webkit-keyframes slide-up-fade-in {
        0% {
            opacity:0;
            -webkit-transform:  translate(0px,40px)  ;
        }
        100% {
            opacity:1;
            -webkit-transform:  translate(0px,0px)  ;
        }
    }

    @-o-keyframes slide-up-fade-in {
        0% {
            opacity:0;
            -o-transform:  translate(0px,40px)  ;
        }
        100% {
            opacity:1;
            -o-transform:  translate(0px,0px)  ;
        }
    }

    @-ms-keyframes slide-up-fade-in {
        0% {
            opacity:0;
            -ms-transform:  translate(0px,40px)  ;
        }
        100% {
            opacity:1;
            -ms-transform:  translate(0px,0px)  ;
        }
    }


    #resultados-container {
        overflow-x: hidden;
    }

    .btn-toggle-resultados,
    .btn-toggle-resultados:hover,
    .btn-toggle-resultados:focus{
        position: absolute;
        border-top: 1px solid #d1d9de;
        border-right: 1px solid #d1d9de;
        border-bottom: 1px solid #d1d9de;
        right: -34px;
        padding: 8px 10px;
        background: white;
        height: 34px;
        top: 15px;
    }

    .input-group-btn .btn,
    #container .form-control,
    .bs-form-controls {
        height: 34px;
    }

    .dataTables_wrapper .dataTables_filter label {
        width: 100% !important;
        margin-bottom: 0px !important;
    }

    .dataTables_wrapper .dataTables_filter input {
        margin-left: 0px !important;
        width: 100% !important;
        border: none;
        background-color: #f9f9f9;
    }
    table .fa-map-marker{
        color: dodgerblue;
    }

    .select2-container--bootstrap .select2-results>.select2-results__options {
        max-height: 220px;
    }

    .filter-located{
        /*width: 37px;*/
        float: right;
    }

    .filter-located .btn-group ul a{
        /*padding-left: 25px;*/
    }

    .filter-located .btn-group ul a i{
        width: 13px;
        padding-right: 20px;
    }


    .filter-located .btn {
        height: 34px;
        color: dodgerblue;
    }
    .filter-located .open  .btn:hover,
    .filter-located .open .btn:focus{
        color: dodgerblue;
    }
    .filter-located .dropdown-menu{
        left: inherit;
        right: 0;
    }
    #resultados-container  .dataTables_filter{
        width: 242px;
    }

    #resultados-container .dataTables_wrapper .dataTables_filter label input{
        border: 1px solid #d1d9de;
        /*border-right: none;*/
    }
    .zoom-enable {
        cursor: zoom-in;
    }
    .zoom-enable.selected {
        cursor: zoom-out;
    }
    .layer-info-buffer{
        display: none;
    }
    .sum-table{
        cursor: pointer;
    }
    .noUi-connect {
        background-color: #42a5f5 !important;
    }
    .noUi-horizontal {
        height: 6px !important;
    }
    .noUi-horizontal .noUi-handle {
        height: 20px !important;
        top: -9px !important;
    }
    .noUi-handle:after,
    .noUi-handle:before {
        height: 8px !important;
        top: 5px !important;
    }
	
	.select2-container--bootstrap .select2-selection{
		font-size: 10px !important;
	}
	
	.select2-container .select2-selection{
		height: 50px;
	}
	
	table.dataTable.dtr-inline.collapsed > tbody > tr > td:first-child::before, table.dataTable.dtr-inline.collapsed > tbody > tr > th:first-child::before{
		content: 'i' !important;
		background-color: #2ECCFA !important;
	}
	
	div.dtr-modal div.dtr-modal-close {
		border: 1px solid #454545;
		font-size: 15px !important;
		font-weight: bold;
	}
	
	.close{
		border: 1px solid #454545;
		color: #454545;
		font-size: 15px !important;
		font-weight: bold;
		margin-top: -5px;
		background-color: #f9f9f9;
		text-align: center;
		border-radius: 3px;
		cursor: pointer;
		width: 22px;
		height: 22px;
		padding-top: 3px;
	}
</style>

<div id="resultados" class="colExpToggle">
    <span id="toggleVisibility" class="btn btn-link btn-toggle-resultados"><i class="fa fa-users"></i></span>
    <div id="resultados-container">

    </div>
</div>

<div class="row" style="padding: 15px 15px 0 15px;">
    <div class="col-xs-11 col-xs-offset-1 col-sm-4 col-sm-offset-1 col-md-2">
        <select id="search-category"  class="form-control">
            {% for layer in search_layers %}
                {% if layer.clave != 'POINT' %}
                    <option value="{{ layer.clave }}" data-layer="{{ layer.capa }}">{{ layer.nombre }}</option>
                {% endif %}
            {% endfor %}
        </select>
		<br>
    </div>
	<div class="col-xs-12 col-sm-7 col-md-4">
		<select id="area-select" class="form-control" multiple="multiple" style="display: none;"></select>
		<div id="persona-input" class="form-group has-feedback has-clear" style="display: none;">
			<input type="text" class="form-control bs-form-controls" placeholder="Criterio de búsqueda" onkeypress="handleKeyPress(event)">
			<span class="form-control-clear fa fa-remove form-control-feedback hidden"></span>
		</div>
		<div id="direccion-input" class="form-group has-feedback has-clear">
			<div class="row">			
				<div class="col-md-6">
					<input id="direccion-calle" type="text" class="form-control bs-form-controls" placeholder="Calle" onkeypress="handleKeyPress(event)">
				</div>
				<div class="col-md-6">
					<input id="direccion-numero" type="text" class="form-control bs-form-controls" placeholder="Numero" onkeypress="handleKeyPress(event)">
				</div>
			</div>
			<div class="row">			
				<div class="col-md-12">
					<select id="direccion-colonia" class="form-control"></select>
				</div>
			</div>
		</div>
        <div class="clientes-por-pagar" style="display: none;">
			<div class="row">			
				<div class="col-md-6">
					<input id ="txtDCustomerB" type="text" class="form-control" placeholder="De">	
				</div>
				<div class="col-md-6">
					<input id ="txtDCustomerE" type="text" class="form-control" placeholder="Hasta">	
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<select id="colonias-select" class="form-control" multiple="multiple"></select>
				</div>
			</div>
		</div>
    </div>
    <div class="col-xs-9 col-sm-9 col-md-2 col-lg-1 text-xs-center">
        <button id="btn-buscar" class="btn btn-default btn-block" type="button">
            <i class="fa fa-search "></i>
            <span class="hidden-xs hidden-sm">Buscar</span>
        </button>
    </div>
    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2 text-xs-center">
        <button class="btn btn-default btn-block" onclick="clearMapResults()">
            <i class="fa fa-trash"></i>
            <span class="hidden-xs hidden-sm">Limpiar resultados</span>
        </button>
    </div>
</div>

<div id="page-content">
    <div class="panel">
        <div class="panel-body" style="padding :0; position: relative;">
            <div id="analysis-map" style="height: 620px; background-color: white; width: 100%;"></div>
			<div id="popup" class="ol-popup">
            <div id="ine-map-loading" style="position: absolute; top: 50%; left: 50%; display:none; margin-top: -60px;">
                {{ image("img/spin.svg") }}
            </div>
            
            <div id="tabla-datos"></div>
        </div>
		<div id="map-streetview-container" style="position: absolute; bottom: 0px; left: 10px; z-index: 1001;"></div>
    </div>
</div>

<div id="modal-table-data" title="Datos de la capa">
	<div id="container-table-data" style="background-color: white; width: auto; height: 100%;"></div>
</div>


{{ stylesheet_link('plugins/jQueryUI/jquery-ui.css') }}
{{ stylesheet_link('plugins/noUiSlider/nouislider.min.css') }}
{{ stylesheet_link('plugins/openlayers/css/ol.css') }}
{{ stylesheet_link('plugins/openlayers/css/ol3-zoomextent.css') }}
{{ stylesheet_link('plugins/openlayers/css/ol3-layerswitcher.css') }}
{{ stylesheet_link('plugins/openlayers/css/ol3-infopadron.css') }}
{{ stylesheet_link('plugins/openlayers/css/ol3-infopredio.css') }}
{{ stylesheet_link('plugins/openlayers/css/ol3-export.css') }}
{{ stylesheet_link('plugins/openlayers/css/ol3-infobuffer.css') }}
{{ stylesheet_link('plugins/openlayers/css/ol3-infopadrones.css') }}
{{ stylesheet_link('plugins/openlayers/css/ol3-baselayerswitcher.css') }}
{{ stylesheet_link('plugins/openlayers/css/ol3-getlegend.css') }}
{{ stylesheet_link('plugins/openlayers/css/ol3-summary.css') }}
{{ stylesheet_link('plugins/openlayers/css/streetviewcontrol-ol.css') }}
    {{ stylesheet_link('plugins/openlayers/css/ol3-drawinginfo.css') }}
    {{ stylesheet_link('plugins/highcharts/css/highcharts.css') }}
{{ stylesheet_link('plugins/openlayers/css/info-control-ol.css') }}

    {{ stylesheet_link('plugins/select2/select2.min.css') }}
    {{ stylesheet_link('plugins/select2/select2-bootstrap.min.css') }}
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key={{ config.application.googleapi }}"></script>
<script type="application/javascript">
    var zonas_marginadas = {{ zonas_marginadas|json_encode }};
    var HIDE_POSITION = '{{ HIDE_POSITION }}';
    var SHOW_POSITION = '{{ SHOW_POSITION }}';
    var LEFT_HIDE = false;

    
    urlGeoserver = '{{ config.application.geoserverUrl }}';
    urlInfoGeoserver = '/analysis/geoserver';
    grupos = {{ grupos }};
	var COLONIAS = {{ colonias }};
	var ZONAS = {{ zonas }};
    var urlBase = '';

    function initDT() {
        var container = $( "#resultados-container" );
        container.empty();
        $('<table class="table display table-bordered" id="ine-datatable" style="width: 100%;">'+
            '<thead>'+
            '<tr>'+
				'<th>&nbsp;</th>'+
				'<th>&nbsp;</th>'+
				'<th>No. Cliente</th>'+
				'<th>Cliente</th>'+
				'<th>Dirección</th>'+
				'<th>ubicado</th>'+
				'<th>Zona</th>'+
				'<th>no_cliente</th>'+
				'<th>Colonia</th>'+
				'<th>Último Pago</th>'+
				'<th>Latitud</th>'+
				'<th>Longitud</th>'+
				'<th>Fecha &Uacute;ltimo Pago</th>'+
				'<th>Origen</th>'+
				'<th>Calle</th>'+
				'<th>Numero</th>'+
				'<th>Importe</th>'+
				'<th>Saldo</th>'+
            '</tr>'+
            '</thead>'+
            '<tbody></tbody>'+
            '</table>').appendTo(container);

        ineDT = $("#ine-datatable").DataTable({
            "dom": "<'row'<'col-sm-3'B><'col-sm-9'f>>" +
				   "<'row'<'col-sm-12'tr>>" +
				   "<'row'<'col-sm-4'i><'col-sm-8'p>>",
            "autoWidth": false,
            "paging": false,
            //"ordering": false,
            "info": true,
            "scrollY": "250px",
            "scrollCollapse": false,
            "paging": true,
			"pagingType": "full",
            "language": {
                "paginate": {
                    "next": ">",
                    "first": "<<",
                    "last": ">>",
                    "previous": "<"
                },
                select: {
                    rows: {
                        _: "",
                        0: "",
                        1: ""
                    }
                },
                "search": "",
                "searchPlaceholder": "Buscar en los resultados",
                "info": "Total:  _TOTAL_ <br> Pag.: _PAGE_ / _PAGES_",
                "infoEmpty": "",
                "infoFiltered": "",
                "emptyTable": "Sin resultados",
                "sZeroRecords": "Sin resultados",
                processing: "Procesando ..."
            },
			"lengthMenu": [[30, 60, 90, 120, -1], [30, 60, 90, 120]],
            "pageLength": 30,
            "order": [[3, 'asc']],
            select: {
                style: 'single'
            },
            responsive: {
                details: {
					display: $.fn.dataTable.Responsive.display.modal( {
						header: function ( row ) {
							var data = row.data();
							return "No. Cliente: " + data[2] + " - Origen: " + data[13];
						}
					} ),
					
					renderer: function ( api, rowIdx, columns ) {
						var data = $.map( columns, function ( col, i ) {
							console.log(col);
							if(col.columnIndex == 3
							|| col.columnIndex == 4 || col.columnIndex == 6
							|| col.columnIndex == 8 || col.columnIndex == 9
							|| col.columnIndex == 12 || col.columnIndex == 16
							|| col.columnIndex == 17){
								return '<tr data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
											'<td style="font-weight:bold;">'+col.title+':'+'</td> '+
											'<td>'+col.data+'</td>'+
										'</tr>';
							}else{
								return "";
							}
							
						} ).join('');
						var element = document.getElementById('popup');
						$(element).popover('destroy');
						return data ? $('<table class="ui table">').append( data ).append( '<table/>' ) : false;
					}
					
					/*renderer: $.fn.dataTable.Responsive.renderer.tableAll({tableClass: 'table'})*/
				}
            },
            "processing": true,
            "serverSide": true,
            "deferLoading": 0,
            ajax: {
                url: "/analysis/getPersons",
                type: 'POST',
                "data": function (d) {
                    d.query = Array.isArray(valueGlobalSearch) ? valueGlobalSearch : JSON.stringify(valueGlobalSearch);
                    d.type = typeGlobalSearch;
                },
                error: function (xhr, status, error) {
                    var textError = "";
                    var tipoError = "";
                    if (xhr.status == 401) {
                        window.location = "/";
                    }
                    else {
                        showGeneralMessage("Ocurrió un error, contacte al administrador", "warning", true);
                    }
                },
				complete: function(a){
					if(a.responseJSON){
						setMarkers(a.responseJSON.data);
						//initMarkerList(a.responseJSON.latslons);
						$('#resultados').addClass("open");
						LEFT_HIDE = true;
						onCompleteAjax();
					}					
				}
            },
            "columnDefs": [
                {
                    targets: [0],
                    orderable: false,
                    className: 'control',
                    responsivePriority: 0,
                    searchable: false,
                    width: "25px"
                },
				{targets: [1, 16, 17], visible: false, searchable: false},
				{targets: [2, 3, 4], visible: true, searchable: true},
				{targets: [-1, 5, 6, 7, 8, 14, 15], visible: false, orderable: false, searchable: true},
				{targets: [9, 12, 13], visible: true, searchable: false},
				{targets: [10,11], visible: false, searchable: false},
                {targets: '_all', type: "numeric", visible: true}
            ],
            "lengthChange": false,
            "fnDrawCallback": function( oSettings ) {
                if(ineDT){
                    ineDT.columns.adjust();
                }
            },
			buttons: [
				{
					text: 'Exportar',
					exportOptions: {
						columns: ':visible'
					},
					action: function ( e, dt, node, config ) {
						var columns = generateQuery();
						window.open("/analysis/exportCsv"+columns);
						
					}
				}
			]
        });

        ineDT
            .on( 'user-select', function ( e, dt, type, cell, originalEvent ) {
                if ( $(originalEvent.target).index() === 0 ) {
                    e.preventDefault();
                }
            } );

        ineDT
            .on('select', function (e, dt, type, indexes) {
                var rowData = ineDT.rows(indexes).data().toArray();
                var ubicado = rowData[0][5];

				var element = document.getElementById('popup');
				$(element).popover('destroy');
				
                if (ubicado == true) {
					var data = [parseFloat(rowData[0][11]), parseFloat(rowData[0][10])];
					data = ol.proj.transform(data, 'EPSG:4326', 'EPSG:3857');
					map.getView().setZoom(21);
					map.getView().setCenter(data);
                }
                else {
                    dt.deselect();
                }
            })
            .on('deselect', function (e, dt, type, indexes) {
                clearMarkerPerson();
            });

        //$(".filter-located").append('<div class="btn-group"> <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-map-marker"></span></button> <ul class="dropdown-menu"> <li><a href="#"><i class="fa" data-value="true"></i><span>Ubicados</span></a></li> <li><a href="#"><i class="fa" data-value="false"></i><span>No ubicados</span></a></li> <li><a href="#"><i class="fa fa-check" data-value=""></i><span>Todos</span></a></li></ul></div>');
        var label = $(".dataTables_filter label")
        label.addClass("has-feedback has-clear");

        $(".dataTables_filter label input").on('input propertychange', function() {
            var $this = $(this);
            var visible = Boolean($this.val());
            $this.siblings('.form-control-clear').toggleClass('hidden', !visible);
        }).trigger('propertychange');

        var span = document.createElement('span');
        span.className = "form-control-clear fa fa-remove form-control-feedback hidden";
        span.style="line-height: inherit";
        span.onclick = function(){
            $(this).siblings('input[type="search"]').val('')
                .trigger('propertychange').trigger('keyup').focus();
        };

        label.append(span);

        $(window).resize();

        $('.filter-located ul a').click(function(){
            $('.filter-located ul a').each(function(){
                $(this).find('i').removeClass('fa-check');
            });
            $(this).find('i').addClass('fa-check');
            ineDT.column(5).search(getLocationFilterValue());
            ineDT.draw();
        });

        return ineDT;
    }

    var ineDT = initDT();

    valueGlobalSearch = "";
    ineDT.column(5).search("ALGOMUYDIFICILDEENCONTRAR")

    $(".mainnav-toggle").click(function(){ setTimeout(function(){map.updateSize();},400); });
</script>
{{ javascript_include("plugins/jQueryUI/jquery-ui.js") }}
{{ javascript_include("plugins/jQueryUI/jquery.ui.touch-punch.min.js") }}
{{ javascript_include("plugins/noUiSlider/nouislider.min.js") }}
{{ javascript_include("plugins/noUiSlider/wNumb.js") }}
{{ javascript_include("plugins/proj4/proj4.js") }}
{{ javascript_include("plugins/openlayers/js/ol.js") }}
{{ javascript_include("plugins/openlayers/js/ol3gm.js") }}
{{ javascript_include("plugins/openlayers/js/ol3-layerswitcher.js") }}
{% if acl.isAllowedUser("users", "change_password") %}
	
{% endif %}
{% if acl.isAllowedUser("analysis", "layer_data_table") %}
	{{ javascript_include("plugins/openlayers/js/ol3-layerswitcher-geo-admin.js") }}
{% else %}
	{{ javascript_include("plugins/openlayers/js/ol3-layerswitcher-geo.js") }}
{% endif %}

    {{ javascript_include("plugins/select2/select2.min.js") }}
    {{ javascript_include("plugins/select2/i18n/es.js") }}

{{ javascript_include("plugins/openlayers/js/ol3-baselayerswitcher.js") }}
{{ javascript_include("plugins/openlayers/js/info-control-ol.js") }}
{{ javascript_include("plugins/openlayers/js/ol3-infopadron.js") }}
{{ javascript_include("plugins/openlayers/js/ol3-infopredio.js") }}
{{ javascript_include("plugins/openlayers/js/ol3-export.js") }}
{{ javascript_include("plugins/openlayers/js/ol3-infobuffer.js") }}
{{ javascript_include("plugins/openlayers/js/ol3-infopadrones.js") }}
{{ javascript_include("plugins/openlayers/js/ol3-getlegend.js") }}
{{ javascript_include("plugins/openlayers/js/ol3-summary.js") }}
{{ javascript_include("plugins/openlayers/js/streetviewcontrol-ol.js") }}
    {{ javascript_include("plugins/openlayers/js/ol3-drawinginfo.js") }}
{{ javascript_include("plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js") }}
{{ javascript_include("plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js") }}
    {{ javascript_include("plugins/highcharts/js/highcharts.js") }}
{{ javascript_include("js/analysis/analysis-functions.js") }}
{{ javascript_include("js/analysis/analysis-events.js") }}