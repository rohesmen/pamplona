<div id="dt-data-panel{{ id }}" class="panel panel-default" style="width: 500px; height: 380px; position: absolute; top: 80px;">

	<div class="panel-heading nemesis-panel">
		<div class="panel-control">
			<a href="#" class="close icon-lg">
				<span class="fa fa-times"></span>
			</a>
		</div>
		<h3 class="panel-title">{{ title }}</h3>
	</div>

	<div class="panel-body" style="padding:5px; overflow: auto; width: 100%; height: calc(100% - 38px);">
		{#<div class="table-responsive">#}
			<table class="table display table-bordered table-condensed" id="dt-data{{ id }}">
				<thead>
					<tr>
						{% for name in headers %}
							<th>{{ name.nombre }}</th>
						{% endfor %}
					</tr>
				</thead>
				<tbody>
				{#{% for row in data %}#}
					{#<tr>#}
						{#{% for name, value in row %}#}
							{#{% if name != 'the_geom' and name != 'nombrebusqueda' and name != 'urlcontenido'  and name != 'contenido' %}#}
								{#<td>{{ value }}</td>#}
							{#{% endif %}#}
						{#{% endfor %}#}
					{#</tr>#}
				{#{% endfor %}#}
				</tbody>
			</table>
		{#</div>#}

	</div>
</div>

<script class="geotable-script">
    if(typeof dtInstceSummary  === "undefined"){
        dtInstceSummary = [];
	}
	var columnsDef = {{ colsdef }};
	var pk = "{{ pk }}";
	$(document).ready(function() {

        $("#dt-data-panel{{ id }} .close").off("click");
        $("#dt-data-panel{{ id }} .close").on("click", function () {
            $("#dt-data-panel{{ id }}").hide();
        });
        $dt{{ auxid }} = $("#dt-data{{ id }}").DataTable({
            "dom": '<"clear"><"top container-float"f><rt><"bottom"lip><"clear">',
            "lengthChange": false,
            "autoWidth": false,
            "paging": true,
            //"ordering": false,
            "info": true,
            "processing": true,
            "scrollY": "170px",
            "scrollCollapse": false,
            "pagingType": "full",
            "language": {
                "paginate": {
                    "next": ">",
                    "first": "<<",
                    "last": ">>",
                    "previous": "<"
                },
                select: {
                    rows: {
                        _: "",
                        0: "",
                        1: ""
                    }
                },
                "search": "",
                "searchPlaceholder": "Filtra los elementos",
                "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
                "infoEmpty": "",
                "infoFiltered": " - filtrado de _MAX_",
                "emptyTable": "Sin elementos",
                "sZeroRecords": "Sin resultados",
                "lengthMenu": "Mostrar _MENU_ registros",
                processing: "Procesando ..."
            },
            "lengthMenu": [[15, 30, 40, -1], [15, 30, 40, "Todos"]],
            "pageLength": 15,
            "order": [[2, 'asc']],
            select: {
                style: 'single',
            },
            responsive: {
                details: {
                    type: 'column',
                    target: 0
                }
            },
            "processing": true,
            "serverSide": true,
            ajax: {
                url: "/analysis/searchdatatable",
                type: 'POST',
                "data": function (d) {
                    d.idcat = {{ id }};
                    d.query = Array.isArray({{ querySearch }}) ? {{ querySearch }} : JSON.stringify({{ querySearch }});
                    d.type = '{{ typeSearch }}';
                },
                error: function (xhr, status, error) {
                    var textError = "";
                    var tipoError = "";
                    if (xhr.status == 401) {
                        window.location = "/";
                    }
                    else {
                        showGeneralMessage("Ocurrió un error, contacte al administrador", "error", true);
                    }
                }
            },
            "columnDefs": columnsDef
        });
        dtInstceSummary[$dt{{ auxid }}];
        $dt{{ auxid }}
            .on( 'user-select', function ( e, dt, type, cell, originalEvent ) {
                if ( $(originalEvent.target).index() === 0 ) {
                    e.preventDefault();
                }
            } );
        $dt{{ auxid }}
            .on('select', function (e, dt, type, indexes) {
                var rowData = $dt{{ auxid }}.rows(indexes).data().toArray();
                var id = rowData[0][1];
                var ubicado = rowData[0][3];
                clearMarkerPerson();
                if (ubicado == true) {
                    $.ajax({
                        url: "/analysis/pointperson/" + id,
                        success: function (resp) {
                            var json = JSON.parse(resp);
                            infoPredio.show(json, true);
//                            initMarkerPerson(json, true);
                        }
                    });
                }
                else {
                    dt.deselect();
                }
            })
            .on('deselect', function (e, dt, type, indexes) {
                clearMarkerPerson();
            });

        $("#dt-data-panel{{ id }}").draggable({
            containment: "#analysis-map",
            handle: ".panel-title"
        });

        $("#dt-data-panel{{ id }}").resizable({
            containment: "#analysis-map",
            minHeight: 380,
            minWidth: 500
		});

        $("#dt-data-panel{{ id }}" ).on( "resize", function( event, ui ) {
            $('#dt-data{{ id }}_wrapper .dataTables_scrollBody').height($("#dt-data-panel{{ id }}").height() - 50 - 34 - 106 - 15);
            $dt{{ auxid }}.columns.adjust();
		});
	});
</script>