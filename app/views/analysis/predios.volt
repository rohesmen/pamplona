<div class="tab-base">
    <!--Nav Tabs-->
    <ul class="nav nav-tabs" role="tablist">
        <li class="active" role="presentation">
            <a data-toggle="tab" href="#tab-datos" aria-controls="tab-datos" role="tab">Datos</a>
        </li>
        <li role="presentation">
            <a data-toggle="tab" href="#tab-grafica" aria-controls="tab-grafica" role="tab">Gráfica</a>
        </li>
    </ul>
    <!--Tabs Content-->
    <div class="tab-content">
        <div role="tabpanel" id="tab-datos" class="tab-pane fade active in">
            <button class="btn btn-primary" id="btn-descargar-sana">Descargar</button>
            <table class="table display table-bordered table-condensed" id="dt-sana">
                <thead>
                <tr>
                    <td></td>
                    <td>Folio</td>
                    <td># catastral</td>
                    <td>Calle</td>
                    <td>Número</td>
                    <td>Colonia</td>
{#                    <td>Uni. Hab.</td>#}
{#                    <td>Poblacion.</td>#}
{#                    <td>Superfice</td>#}
{#                    <td>Latitud</td>#}
{#                    <td>Longitud</td>#}
                </tr>
                </thead>
                <tbody>
                {#{% for d in datos %}#}
                {#<tr>#}
                {#<td></td>#}
                {#<td>{{ d.no_cliente }}</td>#}
                {#<td>{{ d.nombre }}</td>#}
                {#<td>{{ d.folio_catastral }}</td>#}
                {#<td>{{ d.calle }}</td>#}
                {#<td>{{ d.numero }}</td>#}
                {#<td>{{ d.cruzamiento1 }}</td>#}
                {#<td>{{ d.cruzamiento2 }}</td>#}
                {#<td>{{ d.colonia }}</td>#}
                {#<td>{{ d.zona }}</td>#}
                {#<td>{{ d.meses_a_pagar }}</td>#}
                {#<td>{{ d.ultimo_pago }}</td>#}
                {#<td>{{ d.fecha_ultimo_pago }}</td>#}
                {#<td>{{ d.latitud }}</td>#}
                {#<td>{{ d.longitud }}</td>#}
                {#</tr>#}
                {#{% endfor %}#}
                {#</tbody>#}
            </table>
        </div>

        <div role="tabpanel" id="tab-grafica" class="tab-pane">
            <div class="row">
                <div class="col-md-12">
                    <div id="grafica-sana" style="height: 250px"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        infoDrawingCOntrol.updatewmsLayer("Cllientes", "sana:{{ tipo }}", "sana:{{ tipo }}");
        dtsana= $("#dt-sana").DataTable({
            "dom": '<"clear"><"top container-float"f><rt><"bottom"lip><"clear">',
            "lengthChange": false,
            "autoWidth": false,
            "paging": true,
            //"ordering": false,
            "info": true,
            "processing": true,
            "scrollY": "100px",
            "scrollCollapse": false,
            "pagingType": "full",
            "language": {
                "paginate": {
                    "next": ">",
                    "first": "<<",
                    "last": ">>",
                    "previous": "<"
                },
                select: {
                    rows: {
                        _: "",
                        0: "",
                        1: ""
                    }
                },
                "search": "",
                "searchPlaceholder": "Filtra los elementos",
                "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
                "infoEmpty": "",
                "infoFiltered": " - filtrado de _MAX_",
                "emptyTable": "Sin elementos",
                "sZeroRecords": "Sin resultados",
                "lengthMenu": "Mostrar _MENU_ registros",
                processing: "Procesando ..."
            },
            "lengthMenu": [[15, 30, 40, -1], [15, 30, 40, "Todos"]],
            "pageLength": 5,
            "order": [[1, 'asc']],
            select: {
                style: 'single',
            },
            responsive: {
                details: {
                    type: 'column',
                    target: 0
                }
            },
            "processing": true,
            "serverSide": true,
            ajax: {
                url: "/analysis/searchdatadash",
                type: 'POST',
                "data": function (d) {
                    d.geometry = JSON.stringify({{ geometry }});
                    d.type = '{{ tipo }}';
                    d.zona = {{ zona == 'si' ? 'true' : 'false' }}
                },
                error: function (xhr, status, error) {
                    var textError = "";
                    var tipoError = "";
                    if (xhr.status == 401) {
                        window.location = "/";
                    }
                    else {
                        showGeneralMessage("Ocurrió un error, contacte al administrador", "error", true);
                    }
                }
            },
            "columnDefs": [{
                targets: [0],
                orderable: false,
                searchable: false,
                responsivePriority: 0,
                className: "control",
                width: "25px"
            },
                // { targets: [2], visible: false, orderable: false, searchabel: false},
                { targets: '_all', visible: true }
            ]
        });



        $("#map-dash-clientes" ).on( "resize", function( event, ui ) {
            $('#dt-sana_wrapper .dataTables_scrollBody').height($("#map-dash-clientes").height() - 50 - 50 - 106 - 15 - 100);
            dtsana.columns.adjust();
        });


        dtsana
            .on( 'user-select', function ( e, dt, type, cell, originalEvent ) {
                if ( $(originalEvent.target).index() === 0 ) {
                    e.preventDefault();
                }
            } );
        dtsana
            .on('select', function (e, dt, type, indexes) {
                // dt.deselect();
                var rowData = dtsana.rows(indexes).data().toArray();
                var id = rowData[0][1];
                var latitud = rowData[0][9];
                var longitud = rowData[0][10];
                if (latitud) {
                    infoDrawingCOntrol.setMarker(latitud, longitud);
                }
            })
            .on('deselect', function (e, dt, type, indexes) {
                infoDrawingCOntrol.clearMarker();
            });

        /*grafica = Highcharts.chart('grafica-sana', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name} ({point.y}): <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name} ({point.y})</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Predios',
                colorByPoint: true,
                data: [{
                    name: 'Predios',
                    y: {{ datos }},
                    sliced: true,
                    selected: true
                }, {
                    name: 'Clientes',
                    y: {{ clientes }}
                }, {
                    name: 'Comercios',
                    y: {{ comercios }}
                }]
            }]
        });*/

        grafica = Highcharts.chart('grafica-sana', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                title: {
                    text: 'Cantidad'
                }

            },
            legend: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        format: '{point.y}',
                        // color: '#FFFFFF',
                    }
                }
            },
            series: [
                {
                    "name": "Cantidad",
                    "colorByPoint": true,
                    "data": [
                        ['Predios', {{ datos }}],
                        ['Clientes', {{ clientes }}],
                        ['Comercios', {{ comercios }}],
                    ],
                    // dataLabels: {
                    //     enabled: true,
                    //     rotation: -90,
                    //     color: '#FFFFFF',
                    //     align: 'right',
                    //     format: '{point.y:.1f}', // one decimal
                    //     y: 10, // 10 pixels down from the top
                    //     style: {
                    //         fontSize: '13px',
                    //         fontFamily: 'Verdana, sans-serif'
                    //     }
                    // }
                }
            ],
        });

        $("#btn-descargar-sana").on("click", function () {
            var data = {
                geometry: JSON.stringify({{ geometry }}),
                type: '{{ tipo }}'
            }
            $.ajax({
                url: "/analysis/gendesdash",
                type: 'POST',
                "data": $.param(data),
                success: function(resp){
                    window.location = "/analysis/desdash/" + resp;
                },
                error: function (xhr, status, error) {
                    var textError = "";
                    var tipoError = "";
                    if (xhr.status == 401) {
                        window.location = "/";
                    }
                    else {
                        showGeneralMessage("Ocurrió un error, contacte al administrador", "error", true);
                    }
                }
            });
        });

        $('a[href="#tab-grafica"]').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab

            dtsana.columns.adjust();
            dtsana.columns.adjust();
            grafica.reflow();

            $("#map-dash-clientes" ).resize();
            $("#map-dash-clientes" ).resize();
        });

        $('a[href="#tab-datos"]').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab

            dtsana.columns.adjust();
            dtsana.columns.adjust();
        });
    });
</script>