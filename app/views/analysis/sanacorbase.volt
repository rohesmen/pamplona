<style>

    .pad-all {
        padding: 10px;
    }

    .text-2x {
        font-size: 2em;
    }

    .mar-no {
        margin: 0 !important;
    }

    .panel-info .panel-heading, .panel-info .panel-footer, .panel-info.panel-colorful {
        background-color: #03a9f4;
        border-color: #03a9f4;
        color: #fff;
    }

    .panel-warning .panel-heading, .panel-warning .panel-footer, .panel-warning.panel-colorful {
        background-color: #ffb300;
        border-color: #ffb300;
        color: #fff;
    }

    .media-body, .media-left, .media-right {
        display: table-cell;
        vertical-align: top;
    }

    .middle .media-left, .middle .media-right, .middle .media-body {
        vertical-align: middle;
    }

    .media-body, .media-left, .media-right {
        display: table-cell;
        vertical-align: top;
    }

    .middle .media-left, .middle .media-right, .middle .media-body {
        vertical-align: middle;
    }
</style>
<div class="tab-base">
    <!--Nav Tabs-->
    <ul class="nav nav-tabs" role="tablist">
        <li class="active" role="presentation">
            <a data-toggle="tab" href="#tab-datos" aria-controls="tab-datos" role="tab">Datos</a>
        </li>
        <li role="presentation">
            <a data-toggle="tab" href="#tab-grafica" aria-controls="tab-grafica" role="tab">Gráfica</a>
        </li>
    </ul>
    <!--Tabs Content-->
    <div class="tab-content">
        <div role="tabpanel" id="tab-datos" class="tab-pane fade active in">
            <button class="btn btn-primary" id="btn-descargar-sana">Descargar</button>
            <table class="table display table-bordered table-condensed" id="dt-sana">
                <thead>
                <tr>
                    <td></td>
                    <td>Cliente</td>
                    <td>Nombre</td>
                    <td>Folio catastral</td>
                    <td>Calle</td>
                    <td>Número</td>
                    <td>Cruz1.</td>
                    <td>Cruz2.</td>
                    <td>Colonia</td>
                    <td>Zona</td>
                    <td>Meses a pagar</td>
                    <td>Saldo</td>
                    <td>Ult. pago</td>
                    <td>Fec. ult. pago</td>
                    <td>Latitud</td>
                    <td>Longitud</td>
                </tr>
                </thead>
                <tbody>
                {#{% for d in datos %}#}
                    {#<tr>#}
                        {#<td></td>#}
                        {#<td>{{ d.no_cliente }}</td>#}
                        {#<td>{{ d.nombre }}</td>#}
                        {#<td>{{ d.folio_catastral }}</td>#}
                        {#<td>{{ d.calle }}</td>#}
                        {#<td>{{ d.numero }}</td>#}
                        {#<td>{{ d.cruzamiento1 }}</td>#}
                        {#<td>{{ d.cruzamiento2 }}</td>#}
                        {#<td>{{ d.colonia }}</td>#}
                        {#<td>{{ d.zona }}</td>#}
                        {#<td>{{ d.meses_a_pagar }}</td>#}
                        {#<td>{{ d.ultimo_pago }}</td>#}
                        {#<td>{{ d.fecha_ultimo_pago }}</td>#}
                        {#<td>{{ d.latitud }}</td>#}
                        {#<td>{{ d.longitud }}</td>#}
                    {#</tr>#}
                {#{% endfor %}#}
                {#</tbody>#}
            </table>
        </div>

        <div role="tabpanel" id="tab-grafica" class="tab-pane">
            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-warning panel-colorful media middle pad-all" style="margin-top: 35px;">
                        <div class="media-left">
                            <div class="pad-hor">
                                <i class="fa fa-user fa-2x"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <p class="text-2x mar-no text-semibold">{{ datos }}</p>
                            <p class="mar-no">Clientes</p>
                        </div>
                    </div>

                    <div class="panel panel-warning panel-colorful media middle pad-all">
                        <div class="media-left">
                            <div class="pad-hor">
                                <i class="fa fa-money fa-2x"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <p class="text-2x mar-no text-semibold">{{ importe }}</p>
                            <p class="mar-no">Por recuperar</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div id="grafica-sana" style="height: 250px"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
         infoDrawingCOntrol.updatewmsLayer("Cllientes", "evander:{{ tipo }}_view", "clientes_puntos");
        dtsana= $("#dt-sana").DataTable({
            "dom": '<"clear"><"top container-float"f><rt><"bottom"lip><"clear">',
            "lengthChange": false,
            "autoWidth": false,
            "paging": true,
            //"ordering": false,
            "info": true,
            "processing": true,
            "scrollY": "100px",
            "scrollCollapse": false,
            "pagingType": "full",
            "language": {
                "paginate": {
                    "next": ">",
                    "first": "<<",
                    "last": ">>",
                    "previous": "<"
                },
                select: {
                    rows: {
                        _: "",
                        0: "",
                        1: ""
                    }
                },
                "search": "",
                "searchPlaceholder": "Filtra los elementos",
                "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
                "infoEmpty": "",
                "infoFiltered": " - filtrado de _MAX_",
                "emptyTable": "Sin elementos",
                "sZeroRecords": "Sin resultados",
                "lengthMenu": "Mostrar _MENU_ registros",
                processing: "Procesando ..."
            },
            "lengthMenu": [[15, 30, 40, -1], [15, 30, 40, "Todos"]],
            "pageLength": 5,
            "order": [[1, 'asc']],
            select: {
                style: 'single',
            },
            responsive: {
                details: {
                    type: 'column',
                    target: 0
                }
            },
            "processing": true,
            "serverSide": true,
            ajax: {
                url: "/analysis/searchdatadash",
                type: 'POST',
                "data": function (d) {
                    d.geometry = JSON.stringify({{ geometry }});
                    d.type = '{{ tipo }}';
                    d.zona = {{ zona == 'si' ? 'true' : 'false' }};
                    d.idzona = {{ idzona }};
                },
                error: function (xhr, status, error) {
                    var textError = "";
                    var tipoError = "";
                    if (xhr.status == 401) {
                        window.location = "/";
                    }
                    else {
                        showGeneralMessage("Ocurrió un error, contacte al administrador", "error", true);
                    }
                }
            },
            "columnDefs": [{
                targets: [0],
                orderable: false,
                searchable: false,
                responsivePriority: 0,
                className: "control",
                width: "25px"
            },
                { targets: [-1, 2, -2], visible: false, orderable: false, searchabel: false},
                { targets: '_all', visible: true }
            ]
        });



        $("#map-dash-clientes" ).on( "resize", function( event, ui ) {
            $('#dt-sana_wrapper .dataTables_scrollBody').height($("#map-dash-clientes").height() - 50 - 50 - 106 - 15 - 100);
            dtsana.columns.adjust();
            grafica.reflow()
        });


        grafica = Highcharts.chart('grafica-sana', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name} ({point.y}): <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name} ({point.y})</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Clientes',
                colorByPoint: true,
                data: [{
                    name: 'Anualizados',
                    y: {{ anualizado }},
                    sliced: true,
                    selected: true
                }, {
                    name: 'Al dia',
                    y: {{ aldia }}
                }, {
                    name: 'Adelantados',
                    y: {{ adelantados }}
                }, {
                    name: 'Atrasados',
                    y: {{ atrasados }}
                }]
            }]
        });

        $('a[href="#tab-grafica"]').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab

            dtsana.columns.adjust();
            dtsana.columns.adjust();
            grafica.reflow();

            $("#map-dash-clientes" ).resize();
            $("#map-dash-clientes" ).resize();
        });

        $('a[href="#tab-datos"]').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab

            dtsana.columns.adjust();
            dtsana.columns.adjust();
        });

        setTimeout(function () {
            dtsana.columns.adjust();
            dtsana.columns.adjust();
            grafica.reflow();

            $("#map-dash-clientes" ).resize();
            $("#map-dash-clientes" ).resize();
        },1000);

        dtsana
            .on( 'user-select', function ( e, dt, type, cell, originalEvent ) {
                if ( $(originalEvent.target).index() === 0 ) {
                    e.preventDefault();
                }
            } );
        dtsana
            .on('select', function (e, dt, type, indexes) {
                // dt.deselect();
                var rowData = dtsana.rows(indexes).data().toArray();
                var id = rowData[0][1];
                var latitud = rowData[0][13];
                var longitud = rowData[0][14];
                if (latitud) {
                    infoDrawingCOntrol.setMarker(latitud, longitud);
                }
            })
            .on('deselect', function (e, dt, type, indexes) {
                infoDrawingCOntrol.clearMarker();
            });

        $("#btn-descargar-sana").on("click", function () {
            var data = {
                geometry: JSON.stringify({{ geometry }}),
                type: '{{ tipo }}'
            }
            $.ajax({
                url: "/analysis/gendesdash",
                type: 'POST',
                "data": $.param(data),
                success: function(resp){
                    window.location = "/analysis/desdash/" + resp;
                },
                error: function (xhr, status, error) {
                    var textError = "";
                    var tipoError = "";
                    if (xhr.status == 401) {
                        window.location = "/";
                    }
                    else {
                        showGeneralMessage("Ocurrió un error, contacte al administrador", "error", true);
                    }
                }
            });
        })
    });
</script>