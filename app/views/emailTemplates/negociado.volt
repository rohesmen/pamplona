

<tbody>
	<tr>
		<td style="padding:40px 0  0 0;">
			<table>
				<tr>
					<td><b>Clave cliente:</b></td>
					<td>{{ idcliente }}</td>
				</tr>
				<tr>
					<td><b>Fecha:</b></td>
					<td>{{ fecha }}</td>
				</tr>
				<tr>
					<td><b>Cobratario:</b></td>
					<td>{{ cobratario }}</td>
				</tr>
				<tr>
					<td><b>Total:</b></td>
					<td>{{ total }}</td>
				</tr>
				<tr>
					<td><b>Descuento:</b></td>
					<td>{{ descuento }}</td>
				</tr>
				<tr>
					<td><b>Total pagado:</b></td>
					<td>{{ totalpagado }}</td>
				</tr>
			</table>
		</td>
	</tr>
</tbody>

