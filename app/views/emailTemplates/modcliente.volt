

<tbody>
<tr>
	<td style="padding:40px 0  0 0;">
		<table>
			<tr>
				<td><b>Clave cliente:</b></td>
				<td>{{ idcliente }}</td>
			</tr>
			<tr>
				<td><b>Fecha:</b></td>
				<td>{{ fecha }}</td>
			</tr>
			<tr>
				<td><b>Cobratario:</b></td>
				<td>{{ cobratario }}</td>
			</tr>
			<tr>
				<td><b>Es comercio:</b></td>
				<td>{{ iscomercio ? 'SI' : 'NO' }}</td>
			</tr>
			{% if iscomercio %}
				<tr>
					<td><b>Nombre comercio:</b></td>
					<td>{{ nombre_comercio }}</td>
				</tr>
			{% endif %}
			{% if motivo %}
				<tr>
					<td><b>Motivo del cambio:</b></td>
					<td>{{ motivo }}</td>
				</tr>
			{% endif %}
			<tr>
				<td><b>Dirección:</b></td>
				<td>{{ direccion }}</td>
			</tr>
			{% if mensualidad %}
			<tr>
				<td><b>Mensualidad:</b></td>
				<td>{{ mensualidad }}</td>
			</tr>
			{% endif %}
		</table>
	</td>
</tr>
</tbody>

