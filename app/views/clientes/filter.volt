<div>
    <nav class="navbar navbar-default" >
        <div>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header" style="float: none; display: none;">
                <button type="button" id="colapse-busqueda" class="navbar-toggle" data-toggle="collapse"
                        data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse in clientes-only" id="contenedor-filtros" aria-expanded="true" style="border-color: #FFFFFF;">
                {#<form id="frmPersonas" name="frmPersonas" class="navbar-form navbar-right" role="search" style="border: none;">#}
                <div class="nav-bar">
                    {#<div style="font-weight: bold; ;padding: 15px 5px 5px 10px;">Selecciona tipo de búsqueda por:</div>#}
                    <!-- filtros -->
                    <div class="col-xs-6 col-sm-3 col-md-2">
                        <div class="form-group">
                            <label>Buscar por</label>
                            <select class="form-control" id="clientes-filter-select">
                                <option value="nom">Nombre</option>
                                <option value="dir" selected="selected">Dirección</option>
                                <option value="cla">Clave</option>
                                <option value="ruta">Ruta</option>
                                {#<option value="">Todos</option>#}
                            </select>
                        </div>
                    </div>
                    <!-- vigente -->
                    <div class="col-xs-6 col-sm-2 col-md-2">
                        <div class="form-group">
                            <label>Vigente</label>
                            <select class="form-control" id="clientes-vigente-val" style="width: 100%;">
                                <option value="">Todos</option>
                                <option value="true">Si</option>
                                <option value="false">No</option>
                            </select>
                        </div>
                    </div>
                    <!--Todos -->
                    <div class="col-xs-12 col-sm-3" style="display: none" id="clientes-filtro-todos"></div>
                    <!-- Nombre -->
                    <div class="col-xs-12 col-sm-7 col-md-8" style="display: none" id="clientes-filtro-nombre">
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <div class="clientes-filter-clear-do" title="Limpiar">
                                <i class="fa fa-close" style="margin-top: 10px;"></i>
                            </div>
                            <input type="text" class="form-control" placeholder="Escriba el nombre" id="clientes-nombre-val">
                        </div>
                    </div>
                    <!-- direccion -->
                    <div class="col-xs-12 col-sm-6" id="clientes-filtro-direccion">
                        <div class="row">
                            <div class="col-xs-6 col-md-3">
                                <div class="form-group">
                                    <label class="hidden-xs">&nbsp;</label>
                                    <input id="clientes-calle-val" name="calle" type="text" class="form-control" placeholder="Calle">
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <div class="form-group">
                                    <label class="hidden-xs">&nbsp;</label>
                                    <input id="clientes-numero-val" name="numero" type="text" class="form-control" placeholder="Número">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 form-group">
                                <label class="hidden-xs">&nbsp;</label>
                                <select class="form-control" id="clientes-colonia-val" style="width: 100%;">
                                    <option value="">Sel. Colonia</option>
                                    {% for colonia in colonias %}
                                        <option value="{{ colonia.id }}">{{ colonia.nombre }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- clave -->
                    <div class="col-xs-12 col-sm-6" style="display: none" id="clientes-filtro-clave">
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <div class="clientes-filter-clear-do" title="Limpiar">
                                <i class="fa fa-close" style="margin-top: 10px;"></i>
                            </div>
                            <input type="text" class="form-control" placeholder="Números" id="clientes-clave-val" data-inputmask="'mask': '9', 'repeat': 10, 'greedy' : false">
                        </div>
                    </div>
                    <!-- rutas -->
                    <div class="col-xs-12 col-sm-6" style="display: none" id="clientes-filtro-rutavalidada">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Ruta</label>
                                    <select class="form-control" id="clientes-ruta-filter-val" style="width: 100%;">
                                        <option value="">Todos</option>
                                        <option value="-1">Sin Asignación</option>
                                        {% for data in rutas %}
                                            <option value="{{ data.id }}">{{ data.nombre }}</option>
                                        {% endfor %}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Validado</label>
                                    <select class="form-control" id="clientes-rutavalidada-filter-val" style="width: 100%;">
                                        <option value="">Todos</option>
                                        <option value="true">Si</option>
                                        <option value="false">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- boton search -->
                    <div class="col-xs-6 col-sm-2 col-md-1">
                        <label>&nbsp;</label>
                        <button class="btn btn-primary btn-block clientes-search-do" style="margin-bottom: 10px;" type="button" title="Buscar">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                    <!-- boton nuevo -->
                    <div class="col-xs-6 col-sm-2 col-md-1" style="text-align: right;">
                        <label>&nbsp;</label>
                        {% if acl.isAllowedUser('clientes', 'add') %}
                            <button class="btn btn-primary btn-block" type="button" title="Nuevo cliente" style="display: inline-block; margin-bottom: 10px;"
                                    id="openNewCliente">
                                <i class="fa fa-plus"></i>
                            </button>
                        {% endif %}
                    </div>
                </div>
                {#</form>#}

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->

    </nav>
</div>