<form id="wizard-form" class="wizard-big">
    <input type="hidden" id="idcliente" />

    <!-- dirección -->
    <h1>Dir. serv.</h1>
    <div class="step-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 form-group {{ acl.isAllowedUser('clientes', 'abandonado') ? '' : '' }}">
                    <label>Abandonado <input type="checkbox" id="chkabandonado"></label>
                </div>
                <div class="col-md-3 form-group {{ acl.isAllowedUser('clientes', 'comercio') ? '' : '' }}" style="display: none;">
                    <label>Comercio <input type="checkbox" id="chkcomercio"></label>
                    
                </div>
                <div class="col-md-3 form-group {{ acl.isAllowedUser('clientes', 'comercio') ? '' : '' }}">
                    <label>Estatus</label>
                    <select class="form-control" id="idestatus_cliente" name="idestatus_cliente" required>
                        {% for estatus in estatus_cliente %}
                            <option data="{{ estatus.iscomercio ? 'true' : 'false' }}" value="{{ estatus.id }}">{{ estatus.nombre }}</option>
                        {% endfor %}
                    </select>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Nombre comercio</label>
                            <input id="nombre_comercio" type="text" class="form-control" disabled>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 form-group {{ acl.isAllowedUser('clientes', 'marginado') ? '' : '' }}">
                    <label>Marginado <input type="checkbox" id="chkmarginado" disabled></label>
                </div>
                <div class="col-md-3 form-group {{ acl.isAllowedUser('clientes', 'privada') ? '' : '' }}">
                    <label>Privada/cerrada <input type="checkbox" id="chkprivada"></label>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Nombre privada</label>
                            <input id="nombre_privada" type="text" class="form-control" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 form-group hidden">
                    <label>Tipo descuento</label>
                    <select class="form-control m-b" id="idtipo_descuento" name="idtipo_descuento">
                        <option value="">Sel. tipo descuento</option>
                        {% for descuento in tipo_descuento %}
                            <option value="{{ descuento.id }}">{{ descuento.nombre }}</option>
                        {% endfor %}
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label>$ Año inicial *</label>
                    <input id="ultimo_anio_pago" name="ultimo_anio_pago" placeholder="YYYY" type="text" class="form-control" value="2016" required>
                </div>
                <div class="col-md-3 form-group">
                    <label>$ Mes inicial *</label>
                    <select id="ultimo_mes_pago" name="ultimo_mes_pago" class="form-control" required>
                        <option value="">Seleccionar</option>
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                    </select>
                    {#<input id="ultimo_mes_pago" name="ultimo_mes_pago" type="text" placeholder="Último mes de pago" class="form-control" required>#}
                </div>
                <div class="col-sm-3 form-group">
                    <label>Colonia *</label>
                    <select class="form-control" id="idcolonia" name="idcolonia" required>
                        <option value="">Sel. Colonia</option>
                        {% for colonia in colonias %}
                            <option data-data='{ "ismarginado" : {{ colonia.ismarginado ? "true" : "false" }} }' value="{{ colonia.id }}">{{ colonia.nombre }}</option>
                        {% endfor %}
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label>Mensualidad</label>
                    <input id="mensualidad" name="mensualidad" min="1" placeholder="00.00" type="text" class="form-control do-not-disabled" disabled>
                    <label class=" {{ acl.isAllowedUser('clientes', 'cobro_manual') ? '' : 'hidden' }}"> Cobro manual <input type="checkbox" class="form-check-input" name="chMontoManual" id="chMontoManual"></label>
                </div>
            </div>
        </div>
        <div class="col-sm-6 border-right">
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label>Calle *</label>
                    <input id="calle" name="calle" type="text" placeholder="Calle" class="form-control" required>
                </div>
                <div class="col-sm-4 form-group">
                    <label>Letra</label>
                    <input id="calle_letra" name="calle_letra" type="text" placeholder="Calle letra" class="form-control">
                </div>
                <div class="col-sm-4 form-group">
                    <label>Tipo</label>
                    <input id="tipo_calle" name="tipo_calle" type="text" placeholder="Tipo calle" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label>Número *</label>
                    <input id="numero" name="numero" type="text" placeholder="Número" class="form-control" required>
                </div>
                <div class="col-sm-4 form-group">
                    <label>Letra</label>
                    <input id="numero_letra" name="numero_letra" type="text" placeholder="Número letra" class="form-control">
                </div>
                <div class="col-sm-4 form-group">
                    <label>Tipo</label>
                    <input id="tipo_numero" name="tipo_numero" type="text" placeholder="Tipo número" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label>Cruza1</label>
                    <input id="cruza1" name="cruza1" type="text" placeholder="Cruzamiento 1" class="form-control">
                </div>
                <div class="col-sm-4 form-group">
                    <label>Letra</label>
                    <input id="letra_cruza1" name="letra_cruza1" type="text" placeholder="Letra cruzamiento 1" class="form-control">
                </div>
                <div class="col-sm-4 form-group">
                    <label>Tipo</label>
                    <input id="tipo_cruza1" name="tipo_cruza1" type="text" placeholder="Tipo cruzamiento 1" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label>Cruza2</label>
                    <input id="cruza2" name="cruza2" type="text" placeholder="Cruzamiento 2" class="form-control">
                </div>
                <div class="col-sm-4 form-group">
                    <label>Letra</label>
                    <input id="letra_cruza2" name="letra_cruza2" type="text" placeholder="Letra cruzamiento 2" class="form-control">
                </div>
                <div class="col-sm-4 form-group">
                    <label>Tipo</label>
                    <input id="tipo_cruza2" name="tipo_cruza2" type="text" placeholder="Tipo cruzamiento 2" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12 form-group">
                    <label>Ruta *</label>
                    <select class="form-control m-b" id="idruta" name="idruta" required>
                        <option value="">Seleccionar</option>
                        {% for r in rutas %}
                            <option value="{{ r.id }}">{{ r.nombre }}</option>
                        {% endfor %}
                    </select>
                </div>
                <div class="col-sm-12 form-group">
                    <label>Referencia ubicación</label>
                    <input id="referencia_ubicacion" name="referencia_ubicacion" type="text" placeholder="Referencia ubicación" class="form-control">
                </div>
                <div class="col-sm-6 form-group hidden">
                    <label>Localidad</label>
                    <input id="localidad" name="localidad" type="text" placeholder="Localidad" class="form-control">
                </div>
                <div class="col-sm-6 form-group">
                    <label>Folio catastral</label>
                    <input id="folio_catastral" name="folio_catastral" type="text" placeholder="Folio catastral" class="form-control">
                </div>
                <div class="col-sm-6 form-group">
                    <label>Otro</label>
                    <input id="direccion_otro" name="direccion_otro" type="text" placeholder="Otro" class="form-control">
                </div>
            </div>
            <div class="row">

            </div>
        </div>
    </div>
    <!-- ./dirección -->

    <!-- Ubicacion -->
    <h1>Ubicación</h1>
    <div class="step-content">
        <input type="hidden" id="latitud" value="0">
        <input type="hidden" id="longitud" value="0">
        <div class="row">
            <div class="col-md-12">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Buscar..." id="cliente-input-gelocation">
                    <span class="input-group-btn">
                        {#<button class="btn btn-primary" type="button" id="cliente-location"><i class="fa fa-location-arrow"></i></button>#}
                        <button class="btn btn-primary" type="button" id="cliente-search-location">Buscar</button>
                    </span>
                </div>
            </div>
        </div>
        <div id="map" class="map"></div>
    </div>
    <!-- ./Ubicacion -->

    <!-- Requisitos -->
    <h1>Requisitos</h1>
    <div class="step-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label>1. Comprobante Domiciliario * </label>
                </div>
                <div class="col-sm-3 form-group">
                    <select id="idcomprobante" name="idcomprobante" class="form-control obligatorio" required>
                        <option value="">Seleccionar</option>
                        {% for comprobante in tipocomprobante %}
                        <option value="{{ comprobante.id }}">{{ comprobante.tipo_documento }}</option>
                        {% endfor %}
                    </select>
                </div>
                <!--<div class="col-sm-5 form-group infocomp" style="display: none;">
                    <label id="imgcomprobanteinfodiv" style="margin-top: 5px;"></label>
                </div>-->

                    <div class="col-sm-4 form-group editcomp" style="padding-right: 3px; width: 235px;">
                        <input type="text" id="imgcomprobante" name="imgcomprobante" class="form-control obligatorio" readonly required>
                        <!--<input  type="file" id="file-comprobante" class="form-control">-->
                    </div>
                    <div class="col-sm-1 form-group infocomp" style="display: none; padding-right: 0px; width: 55px;">
                        <div id="imgcomprobanteinfodiv"></div>
                    </div>
                    <div class="col-sm-1 form-group editcomp" id="container-loadimgPag0" style="width: 30px; padding-right: 0px; padding-left: 0px;">
                        <div id="rutaUpload0" class="dropzone dropzone-requisitos" style="">
                            <div class="dz-default dz-message">
                                <div class="dz-icon">
                                    <i class="demo-pli-upload-to-cloud icon-5x"></i>
                                </div>
                                <div>
                                    <p class="text-sm text-muted">
                                        <button class="btn btn-sm btn-primary" id="btnimgComp" >
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </p>
                                </div>
                            </div>
                            <div class="fallback">
                                <input type="file" id="form-descripcion">
                            </div>
                        </div>
                    </div>

            </div>

            <div class="row">
                <div class="col-sm-4 form-group">
                    <label>2. Identificaci&oacute;n * </label>
                    <!--<input  type="file" id="file-comprobante" class="form-control">-->
                </div>
                <div class="col-sm-3 form-group">
                    <select class="form-control obligatorio" id="ididentificacion" name="ididentificacion" required>
                        <option value="">Seleccionar</option>
                        {% for identificacion in tipoidentificacion %}
                        <option value="{{ identificacion.id }}">{{ identificacion.tipo_documento }}</option>
                        {% endfor %}
                    </select>
                </div>
                <!--<div class="col-sm-5 form-group infoiden"  style="display: none;">
                    <label id="imgidentificacioninfodiv" style="margin-top: 5px;"></label>
                </div>-->
                <div class="col-sm-3 form-group editiden" style="padding-right: 3px; width: 235px;">
                    <input type="text" id="imgIden" name="imgIden" class="form-control obligatorio" readonly required>
                    <!--<input  type="file" id="file-comprobante" class="form-control">-->
                </div>
                <div class="col-sm-1 form-group infoiden" style="display: none; padding-right: 0px; width: 55px;">
                    <div id="imgidentificacioninfodiv"></div>
                </div>
                <div class="col-sm-1 form-group editiden" id="container-loadimgPag" style="width: 30px; padding-right: 0px; padding-left: 0px;">
                    <!--<div class="">-->
                        <!--<div>
                            <input type="text" id="imgComp" name="imgComp" placeholder="subir archivo" class="form-control" required>
                        </div>-->
                        <div id="rutaUpload" class="dropzone dropzone-requisitos" style="">
                            <div class="dz-default dz-message">
                                <div class="dz-icon">
                                    <i class="demo-pli-upload-to-cloud icon-5x"></i>
                                </div>
                                <div>
                                   <!-- <span class="dz-text">Arrastrar archivo</span>
                                    <p class="text-sm text-muted">o seleccionar manualmente</p>-->
                                    <p class="text-sm text-muted">
                                        <button class="btn btn-sm btn-primary" id="btnimgIden" >
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </p>
                                </div>
                            </div>
                            <!--<div>
                                <button class="btn btn-sm btn-primary" id="btnimg22" style="margin-top: 27px;">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>-->

                            <div class="fallback">
                                <input type="file" id="form-descripcion">
                            </div>
                        </div>
                    <!--</div>-->
                    <!--<input type="text" id="imgidentificacion" name="imgidentificacion" placeholder="subir archivo" class="form-control" required>-->

                </div>
            </div>

            <div class="row">
                <div class="col-sm-4 form-group">
                    <label>3. Fotograf&iacute;a del predio * </label>
                </div>
                <!--<div class="col-sm-5 form-group infofoto" style="display: none;">
                    <label id="imgfotopredioinfodiv" style="margin-top: 5px;"></label>
                </div>-->
                <div class="col-sm-4 form-group editfoto" style="padding-right: 3px; width: 235px;">
                    <input type="text" id="imgfotopredio" name="imgfotopredio" class="form-control obligatorio" readonly required>

                </div>
                <div class="col-sm-1 form-group infofoto" style="display: none; padding-right: 0px; width: 55px;">
                    <div id="imgfotopredioinfodiv"></div>
                </div>
                <!--<div class="col-sm-3 form-group">
                    <input type="text" id="imgfotografia" name="imgfotografia" placeholder="subir archivo" class="form-control">
                </div>-->
                <div class="col-sm-1 form-group editfoto" id="container-loadimgPag2" style="width: 30px; padding-right: 0px; padding-left: 0px;">
                    <!--<div class="">-->
                    <!--<div>
                        <input type="text" id="imgComp" name="imgComp" placeholder="subir archivo" class="form-control" required>
                    </div>-->
                    <div id="rutaUpload2" class="dropzone dropzone-requisitos" style="" >
                        <div class="dz-default dz-message">
                            <div class="dz-icon">
                                <i class="demo-pli-upload-to-cloud icon-5x"></i>
                            </div>
                            <div>
                                <!-- <span class="dz-text">Arrastrar archivo</span>
                                 <p class="text-sm text-muted">o seleccionar manualmente</p>-->
                                <p class="text-sm text-muted">
                                    <button class="btn btn-sm btn-primary" id="btnimgfotopredio" >
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </p>
                            </div>

                        </div>
                        <!--<div>
                            <button class="btn btn-sm btn-primary" id="btnimg22" style="margin-top: 27px;">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>-->

                        <div class="fallback">
                            <input type="file" id="form-descripcion2">
                        </div>
                    </div>

                    <!--</div>-->
                    <!--<input type="text" id="imgidentificacion" name="imgidentificacion" placeholder="subir archivo" class="form-control" required>-->

                </div>
            </div>

            <div class="row">
                <div class="col-sm-4 form-group">
                    <label>4. Tel&eacute;fono de contacto * </label>
                </div>
                <div class="col-sm-3 form-group">
                    <input id="tel_contacto" name="tel_contacto" name="tel_contacto" type="text" placeholder="Teléfono" class="form-control obligatorio" required>
                </div>
            </div>
        </div>
    </div>
    <!-- ./Requisitos -->

    <!-- Persona -->
    <h1>Persona</h1>
    <div class="step-content container-step-persona">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <input type="radio" name="tipo_cliente" id="tipo_cliente1" class="radio_tipo_cliente" value="fisica" checked/>
                    <label for="tipo_cliente1" class="label_tipo_cliente">Fisica</label>
                </div>
                <div class="col-sm-6 col-md-3">
                    <input type="radio" name="tipo_cliente" id="tipo_cliente2" class="radio_tipo_cliente" value="moral"/>
                    <label for="tipo_cliente2" class="label_tipo_cliente">Moral</label>
                </div>
                {#<div class="col-sm-12 form-group">#}
                    {#<label> <input type="checkbox" id="is-fisica" name="is-fisica" class="i-checks" checked> Persona física/moral </label>#}
                {#</div>#}
            </div>
            <div class="row">
                <div class="col-md-3 form-group ocultar-fisica">
                    <label>Ape paterno</label>
                    <input id="apepat" name="apepat" type="text" placeholder="Apellido paterno" class="form-control">
                </div>
                <div class="col-md-3 form-group ocultar-fisica">
                    <label>Ape materno</label>
                    <input id="apemat" name="apemat" type="text" placeholder="Apellido materno" class="form-control">
                </div>
                <div class="col-md-6 form-group ocultar-fisica">
                    <label>Nombre</label>
                    <input id="nombres" name="nombres" type="text" placeholder="Nombre" class="form-control">
                </div>
                <div class="col-md-12 form-group ocultar-moral">
                    <label>Nombre sucursal</label>
                    <input id="nombre_sucursal" name="nombre_sucursal" type="text" placeholder="Sucursal" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-9 col-sm-10 col-md-11 form-group">
                    <label for="cliente_rs">Razón social</label>
                    <select id="cliente_rs"  class="form-control" placeholder="Razón social">
                        <option value="">Seleccionar</option>
                        {% for rs in rss %}
                            {% if !rs.fisica %}
                                {% continue %}
                            {% endif %}
                            <option value="{{ rs.id }}">{{ rs.razon_social }}</option>
                        {% endfor %}
                    </select>
                </div>
                <div class="col-xs-3 col-sm-2 col-md-1 form-group">
                    <button class="btn btn-sm btn-primary" id="rs_add_modal" style="margin-top: 27px;"><i class="fa fa-plus"></i></button>
                </div>
            </div>
        </div>
    </div>
    <!-- ./Persona -->

    <!-- Facturacion -->
    <h1>Facturación</h1>
    <div class="step-content ocultar-moral-for-validate">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 form-group">
                    <label>Contacto del servicio *</label>
                    <input id="contacto_servicio" name="contacto_servicio" type="text" placeholder="Nombre" class="form-control" required>
                </div>
                <div class="col-md-3 form-group">
                    <label>Tel. *</label>
                    <input id="contacto_servicio_tel" name="contacto_servicio_tel" type="text" placeholder="Teléfono" class="form-control" required>
                </div>
                <div class="col-md-3 form-group">
                    <label>Ext.</label>
                    <input id="contacto_servicio_ext" name="contacto_servicio_ext" type="text" placeholder="Extensión" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <label>Contacto de pago 1 *</label>
                    <input id="contacto_pago1" name="contacto_pago1" type="text" placeholder="Nombre" class="form-control" required>
                </div>
                <div class="col-md-3 form-group">
                    <label>Tel. *</label>
                    <input id="contacto_pago1_tel" name="contacto_pago1_tel" type="text" placeholder="Teléfono" class="form-control" required>
                </div>
                <div class="col-md-3 form-group">
                    <label>Ext.</label>
                    <input id="contacto_pago1_ext" name="contacto_pago1_ext" type="text" placeholder="Extensión" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <label>Contacto de pago 2</label>
                    <input id="contacto_pago2" name="contacto_pago2" type="text" placeholder="Nombre" class="form-control">
                </div>
                <div class="col-md-3 form-group">
                    <label>Tel.</label>
                    <input id="contacto_pago2_tel" name="contacto_pago2_tel" type="text" placeholder="Teléfono" class="form-control">
                </div>
                <div class="col-md-3 form-group">
                    <label>Ext.</label>
                    <input id="contacto_pago2_ext" name="contacto_pago2_ext" type="text" placeholder="Extensión" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 form-group">
                    <label>Método pago *</label>
                    <select class="form-control" id="metodo_pago" name="metodo_pago" required>
                        <option value="">Seleccionar</option>
                        {% for m in metodos_pago %}
                            <option value="{{ m.id }}">{{ m.nombre }}</option>
                        {% endfor %}
                    </select>
                </div>
                <div class="col-md-4 form-group">
                    <label>Contrato</label>
                    <input id="numero_contrato" name="numero_contrato" type="text" placeholder="Contrato" class="form-control">
                </div>
                <div class="col-md-4 form-group">
                    <label>Forma facturación *</label>
                    <select class="form-control" id="periodicidad_facturacion" name="periodicidad_facturacion" required>
                        <option value="">Seleccionar</option>
                        <option value="Mensual">Mensual</option>
                        <option value="Bimestral">Bimestral</option>
                        <option value="Trimestral">Trimestral</option>
                        <option value="Semestral">Semestral</option>
                        <option value="Anual">Anual</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 form-group">
                    <label>Tipo fact. *</label>
                    <select class="form-control" id="tipo_facturacion" name="tipo_facturacion" required>
                        <option value="">Seleccionar</option>
                        <option value="Fija">Fija</option>
                        <option value="Tamboreo">Tamboreo</option>
                    </select>
                </div>
                <div class="col-md-2 form-group ocultar-fija">
                    <label>Monto *</label>
                    <input id="monto_tasa_fija" name="monto_tasa_fija" type="text" placeholder="Monto" class="form-control">
                </div>
                <div class="col-md-2 form-group ocultar-tamboreo">
                    <label>Calcular por *</label>
                    <select class="form-control" id="facturacion_tamboreo" name="facturacion_tamboreo" required>
                        <option value="">Seleccionar</option>
                        {% for m in facturacion_tamboreo %}
                            <option value="{{ m.id }}" data-iva="{{ m.monto_iva }}" data-siniva="{{ m.monto_sin_iva }}">{{ m.nombre }}</option>
                        {% endfor %}
                    </select>
                </div>
                <div class="col-md-2 form-group ocultar-tamboreo">
                    <label>Cantidad *</label>
                    <input id="cantidad_tamboreo" name="cantidad_tamboreo" type="text" placeholder="Cantidad" class="form-control">
                </div>
                <div class="col-md-2 form-group ocultar-tamboreo">
                    <label>Total iva</label>
                    <input type="text" id="tamboreo_iva" class="form-control do-not-disabled" disabled>
                </div>
                <div class="col-md-2 form-group ocultar-tamboreo">
                    <label>Total sin iva</label>
                    <input type="text" id="tamboreo_sin_iva" class="form-control do-not-disabled" disabled>
                </div>
            </div>
        </div>
    </div>
    <!-- ./Facturacion -->

    <!-- Propietario -->
    <h1>Propietario</h1>
    <div class="step-content">
        <div class="row">
            <div class="col-sm-3 form-group">
                <label>Ape paterno</label>
                <input id="apepat_propietario" name="apepat_propietario" type="text" placeholder="Apellido paterno propietario" class="form-control">
            </div>
            <div class="col-sm-3 form-group">
                <label>Ape materno</label>
                <input id="apemat_propietario" name="apemat_propietario" type="text" placeholder="Apellido materno propietario" class="form-control">
            </div>
            <div class="col-sm-6 form-group">
                <label>Nombre</label>
                <input id="nombres_propietario" name="nombres_propietario" type="text" placeholder="Nombre propietario" class="form-control">
            </div>
        </div>
        <div class="col-sm-6 border-right hidden">
            <div class="row">
                <div class="col-sm-8 form-group">
                    <label>Estatus cliente</label>
                    <select class="form-control m-b" id="idestatuscliente" name="idestatuscliente">
                        <option value="">Sel. estatus cliente</option>
                        {#{% for estatus in estatus_cliente %}#}
                            {#<option value="{{ estatus.id }}">{{ estatus.nombre }}</option>#}
                        {#{% endfor %}#}
                    </select>
                </div>
                {#<div class="col-sm-4 form-group">#}
                    {#<label> <input type="checkbox" id="is-ubicado" name="is-ubicado" class="i-checks"> Ubicado </label>#}
                {#</div>#}
            </div>
        </div>
        <div class="col-sm-6 hidden">
            <div class="row">
                <div class="col-sm-6 form-group">
                    <label>Tarifa colonia</label>
                    <select class="form-control m-b" id="idtarifa_colonia" name="idtarifa_colonia">
                        <option value="">Sel. tarifa colonia</option>
                        {#{% for tarifa in tarifa_colonia %}#}
                            {#<option value="{{ tarifa.id }}">{{ tarifa.tarifa }}</option>#}
                        {#{% endfor %}#}
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <label>Tipo contrato</label>
                    <select class="form-control m-b" id="idtipo_contrato" name="idtipo_contrato">
                        <option value="">Sel. tipo contrato</option>
                        {#{% for contratos in tipo_contratos %}#}
                            {#<option value="{{ contratos.id }}">{{ contratos.nombre }}</option>#}
                        {#{% endfor %}#}
                    </select>
                </div>
                {#<div class="col-sm-6 form-group">#}
                {#<label>Referencia ubicación</label>#}
                {#<input id="referencia_ubicacion" name="referencia_ubicacion" type="text" placeholder="Referencia ubicación" class="form-control">#}
                {#</div>#}
            </div>
        </div>
    </div>
    <!-- ./Propietario -->

    <!-- otros datos -->
    <h1>Otros</h1>
    <div class="step-content">
        <div class="col-sm-6">
            <div class="form-group">
                <label>Cobratario</label>
                <input type="text" disabled class="form-control do-not-disabled" id="cobratario">
            </div>
{#            <div class="form-group">#}
{#                <label>Teléfono</label>#}
{#                <input id="telefono" name="telefono" type="text" placeholder="Teléfono" class="form-control">#}
{#            </div>#}
            <div class="form-group">
                <label>Correo</label>
                <input id="correo" name="correo" type="text"  placeholder="Correo" class="form-control">
            </div>
        </div>
        <div class="col-sm-6">
            <!-- observación -->
            <div class="form-group">
                <label>Observación</label>
                <textarea id="observacion" name="observacion" rows="10" class="form-control" style="resize: none;"></textarea>
            </div>
            <!-- ./observación -->
        </div>
    </div>
    <!-- ./otros datos -->
</form>