<!-- View Clientes -->
{{ content() }}

<link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
<link href="../plugins/datepicker/bootstrap-datepicker.css" rel="stylesheet">
<link href="css/pamplona/clientes.css" rel="stylesheet">
<link href="plugins/dropzone/dropzone.min.css" rel="stylesheet">

{{ stylesheet_link('plugins/select2/select2.min.css') }}
    {{ stylesheet_link('plugins/select2/select2-bootstrap.min.css') }}

<style>
    .dropzone-requisitos {
        min-height: initial;
        border: none;
        background: transparent;
        padding: 0px;
    }
    .dropzone-requisitos .dz-message{
        margin: 0px;
    }
    .dropzone-requisitos.dz-started .dz-message {
        display: block;
    }
    .dropzone-requisitos .dz-preview {
        display: none;
    }
</style>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2>Clientes</h2>
    </div>
    <div class="col-lg-6" style="text-align: right;">
    {% if acl.isAllowedUser('clientes', 'adddescuento') or
        acl.isAllowedUser('clientes', 'segdescpen') %}

        <div class="btn-group">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Pensionados y jubilados <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                {% if acl.isAllowedUser('clientes', 'adddescuento') %}
                <li id="addDescuento"><a href="#">Aplicar descuento</a></li>
                {% endif %}
                {% if acl.isAllowedUser('clientes', 'adddescuento') and acl.isAllowedUser('clientes', 'segdescpen') %}
                    <li role="separator" class="divider"></li>
                {% endif %}
                {% if acl.isAllowedUser('clientes', 'segdescpen') %}
                <li id="segDescPen"><a href="#">Seguimiento</a></li>
                {% endif %}
            </ul>
        </div>
    {% endif %}

        {% if acl.isAllowedUser('clientes', 'bitmens') or
            acl.isAllowedUser('clientes', 'segdescpen') or
            acl.isAllowedUser('clientes', 'chgmencs') or
            acl.isAllowedUser('clientes', 'chgmencf') or
            acl.isAllowedUser('clientes', 'chgmenci') or
            acl.isAllowedUser('clientes', 'chgmench') %}

            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Cobro manual <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" style="right: 0px; left: auto;">
                    {% if acl.isAllowedUser('clientes', 'chgmencs') or
                        acl.isAllowedUser('clientes', 'chgmencf') or
                        acl.isAllowedUser('clientes', 'chgmenci') or
                        acl.isAllowedUser('clientes', 'chgmench') %}
                        <li id="clientes-chgmens" data-idcliente=""><a href="#">Cambiar mensualidad</a></li>

                    {% endif %}
                    {% if (acl.isAllowedUser('clientes', 'chgmencs') or
                        acl.isAllowedUser('clientes', 'chgmencf') or
                        acl.isAllowedUser('clientes', 'chgmenci') or
                        acl.isAllowedUser('clientes', 'chgmench')
                        ) and acl.isAllowedUser('clientes', 'bitmens') %}
                    <li role="separator" class="divider"></li>
                    {% endif %}
                    {% if acl.isAllowedUser('clientes', 'bitmens') %}
                        <li id="clientes-bitmens"><a href="#">Seguimiento</a></li>
                    {% endif %}
                </ul>
            </div>
        {% endif %}
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    {% include "clientes/filter.volt" %}
                    <div style="margin-top: 15px;">
                        <table class="display table-striped table-hover" id="clientes-datatable" style="width: 100%;">
                            <thead>
                            <tr>
                                <th data-priority="1"></th>
                                <th data-priority="2"></th>
                                <th>Vigente</th>
                                <th>Clave</th>
                                <th data-priority="3">Calle</th>
                                <th data-priority="4">Número</th>
                                <th class="no-wrap">Cruz. 1</th>
                                <th class="no-wrap">Cruz. 2</th>
                                <th data-priority="5">Colonia</th>
                                <th>Persona</th>
                                <th>Propietario</th>
                                <th>$ Mes inicial</th>
                                <th>$ año inicial</th>
                                <th>Comercio</th>
                                <th>Abandonado</th>
                                <th>Marginado</th>
                                <th>Privada</th>
                                <th>latitud</th>
                                <th>longitud</th>
                                <th>idcolonia</th>
                                <th>nombre_comercio</th>
                                <th>nombre_privada</th>
                                <th>pensionado</th>
                                <th data-priority="6">Estatus</th>
                                <th>idestatus_cliente</th>
                                <th>comercio bool</th>
                                <th>vigente bool</th>
                                <th>activo bool</th>
                                <th>estatus sigla</th>
                                <th>mensualidad</th>
                                <th>idruta</th>
                                <th data-priority="6">Ruta</th>
                                <th data-priority="7">Ruta validada</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- view content -->

<!-- Modal cliente -->
<div class="modal inmodal fade" id="cliente-info-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header cliente-modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Cliente nuevo</h4>
            </div>

            <div class="modal-body">
                <div id="mensaje-shortcut" class="visible-md visible-lg">Guardar: CTRL+SHIFT+A</div>
                {% include "clientes/wizard.volt" %}
            </div>

        </div>
    </div>
</div>
<!-- /.modal cliente -->

<div class="modal fade" id="modal-razonsocial" tabindex="-1" role="dialog" aria-labelledby="myModalrs">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalrs">Agregar razón social</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <input type="radio" name="rs_tipo_cliente" id="rs_tipo_cliente1" class="radio_tipo_cliente" value="fisica" checked/>
                            <label for="rs_tipo_cliente1" class="label_tipo_cliente">Fisica</label>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <input type="radio" name="rs_tipo_cliente" id="rs_tipo_cliente2" class="radio_tipo_cliente" value="moral"/>
                            <label for="rs_tipo_cliente2" class="label_tipo_cliente">Moral</label>
                        </div>
                    </div>
                    <!-- persona moral -->
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Razón social *</label>
                            <input id="rs_razon_social" type="text" placeholder="Razón soccial" class="form-control rs-required">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Nombre comercial</label>
                            <input id="rs_nombre_comercial" type="text" placeholder="Nombre comercial" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 form-group">
                            <label>RFC *</label>
                            <input id="rs_rfc" type="text" placeholder="RFC" class="form-control rs-required">
                        </div>
                        <div class="col-md-3 form-group">
                            <label>Teléfono *</label>
                            <input id="rs_telefono" type="text" placeholder="Teléfono" class="form-control rs-required">
                        </div>
                        <div class="col-md-3 form-group">
                            <label>Correo *</label>
                            <input id="rs_correo" type="text" placeholder="Correo" class="form-control rs-required">
                        </div>
                        <div class="col-md-3 form-group">
                            <label>Código Contpaq</label>
                            <input id="rs_codigo_contpaq" type="text" placeholder="Contpaq" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12"><label>Domicilio fiscal</label></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 form-group">
                            <label>Calle *</label>
                            <input id="rs_calle" type="text" placeholder="Calle" class="form-control rs-required">
                        </div>
                        <div class="col-md-2 form-group">
                            <label>Letra</label>
                            <input id="rs_calle_letra" type="text" placeholder="Calle letra" class="form-control">
                        </div>
                        <div class="col-md-2 form-group">
                            <label>Tipo</label>
                            <input id="rs_calle_tipo" type="text" placeholder="Tipo calle" class="form-control">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Colonia *</label>
                            <select id="rs_idcolonia" class="form-control rs-required">
                                <option value="">Sel. Colonia</option>
                                {% for colonia in colonias %}
                                    <option value="{{ colonia.id }}">{{ colonia.nombre }}</option>
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 form-group">
                            <label>Número *</label>
                            <input id="rs_numero" type="text" placeholder="Número" class="form-control rs-required">
                        </div>
                        <div class="col-sm-3 form-group">
                            <label>Letra</label>
                            <input id="rs_numero_letra" type="text" placeholder="Número letra" class="form-control">
                        </div>
                        <div class="col-sm-3 form-group">
                            <label>Tipo</label>
                            <input id="rs_numero_tipo" type="text" placeholder="Tipo número" class="form-control">
                        </div>
                        <div class="col-md-3 form-group">
                            <label>CP *</label>
                            <input id="rs_cp" type="text" placeholder="CP" class="form-control rs-required">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 form-group">
                            <label>Cruza1</label>
                            <input id="rs_cruza1" type="text" placeholder="Cruz.1" class="form-control">
                        </div>
                        <div class="col-md-2 form-group">
                            <label>Letra</label>
                            <input id="rs_cruza1_letra" type="text" placeholder="Letra cruz. 1" class="form-control">
                        </div>
                        <div class="col-md-2 form-group">
                            <label>Tipo</label>
                            <input id="rs_cruza1_tipo" type="text" placeholder="Tipo cruz. 1" class="form-control">
                        </div>
                        <div class="col-md-2 form-group">
                            <label>Cruza2</label>
                            <input id="rs_cruza2" type="text" placeholder="Cruz. 2" class="form-control">
                        </div>
                        <div class="col-md-2 form-group">
                            <label>Letra</label>
                            <input id="rs_cruza2_letra" type="text" placeholder="Letra cruz. 2" class="form-control">
                        </div>
                        <div class="col-md-2 form-group">
                            <label>Tipo</label>
                            <input id="rs_cruza2_tipo" type="text" placeholder="Tipo cruz. 2" class="form-control">
                        </div>
                    </div>
                    <!-- ./persona moral -->
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnGuardarRS" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="clientes-descuento-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Descuento pensionado</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">

                    <div id="frmClienteDescuento">
                        <div class="row">
                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="clave">Clave</label>
                                    <input type="text" class="form-control" id="clave" name="clave" placeholder="Clave" disabled>
                                </div>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="activo">Activo</label>
                                    <div class="checkbox" >
                                        <label>
                                            <input type="checkbox" id="activo" name="activo" disabled>
                                        </label>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="nombre">Oficio</label>
                                    <input type="text" class="form-control" id="cOficio" placeholder="Oficio" name="cOficio" required>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="fInicio">Fecha Inicio</label>
                                    <input type="text" class="form-control" id="fInicio" name="fInicio" placeholder="MM/yyyy" required disabled>

                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="fFin">Fecha Fin</label>
                                    <input type="text" class="form-control" id="fFin" name="fFin" placeholder="MM/yyyy" required>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3" id="conatainer-dropzone">
                                <div class="form-group">
                                    <div id="rutaUploadOficioPensionado" class="dropzone" style="text-align: center;">
                                        <div class="dz-default dz-message">
                                            <div class="dz-icon">
                                                <i class="fa fa-cloud-upload fa-5x"></i>
                                            </div>
                                            <div>
                                                <span class="dz-text">Arrastrar archivo</span>
                                                <p class="text-sm text-muted">o seleccionar manualmente</p>
                                                <p class="text-sm text-muted">Permitido: imagen, pdf</p>
                                            </div>
                                        </div>
                                        <div class="fallback">
                                            <input type="file" id="form-descripcion">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 " style="display: none;">
                                <div  class="text-center" id="containerIMG">
                                    <img id="containerOficioPensionado" src="img/no-image.png" class="img-responsive" style="display: inline-block;">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="btnGuardarModal" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal cliente -->

<!-- Modal -->
<div class="modal fade" id="valida-ruta-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Validación de ruta</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <input type="hidden" id="idClienteValidarRuta">
                    <h3 id="modalPorValidar">Se realizara la validación de la ruta</h3>
                    <div class="row">
                        <div class="col-sm-12 form-group" id="valida-ruta-modal-parent" style="position:relative;">
                            <label>Ruta *</label>
                            <select class="form-control m-b" id="idruta-validacion" name="idruta-validacion">
                                <option value="">Seleccionar</option>
                                {% for r in rutas %}
                                    <option value="{{ r.id }}">{{ r.nombre }}</option>
                                {% endfor %}
                            </select>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Observaciones</label>
                                <textarea id="observacionesValidaRuta" class="form-control" style="resize: none;" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="data-validacion">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Fecha validación</label>
                                <input id="dateFechaVaidacion" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Usuario validación</label>
                                <input id="fechaVaidacion" class="form-control" disabled />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="btnGuardarValidacion" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal cliente -->

<!-- Steps -->
{{ javascript_include("plugins/staps/jquery.steps.min.js") }}
<!-- Jquery Validate -->
{{ javascript_include("plugins/dropzone/dropzone.js") }}
{{ javascript_include("plugins/validate/jquery.validate.min.js") }}
{{ javascript_include("js/clientes-events.js?time="~time() ) }}
{{ javascript_include("js/imagenes.js") }}
{{ javascript_include("plugins/datepicker/bootstrap-datepicker.js") }}
{{ javascript_include("plugins/datepicker/locales/bootstrap-datepicker.es.js") }}


<script type="application/javascript">
    var isEditMesAnio = '{{ acl.isAllowedUser('clientes', 'edmesanio') }}';
    var permeditmens = [];
    {% if acl.isAllowedUser('clientes', 'chgmencs') %}
    permeditmens.push("CS");
    {% endif %}
    {% if acl.isAllowedUser('clientes', 'chgmencf') %}
    permeditmens.push("CF");
    {% endif %}
    {% if acl.isAllowedUser('clientes', 'chgmenci') %}
    permeditmens.push("CI");
    {% endif %}
    {% if acl.isAllowedUser('clientes', 'chgmench') %}
    permeditmens.push("CH");
    {% endif %}



    var buttons = "";
    {% if
    acl.isAllowedUser('clientes', 'edit')
    or  acl.isAllowedUser('clientes', 'info')
    or acl.isAllowedUser('clientes', 'delete')
    %}
    {% if acl.isAllowedUser('clientes', 'info') %}
    buttons += '<button class="btn btn-primary btn-sm clientes-info" type="button" title="Consultar">' +
        '<i class="fa fa-info"></i>' +
        '</button> ';
    {% endif %}

    {% if acl.isAllowedUser('clientes', 'edit') %}
    buttons += '<button class="btn btn-primary btn-sm clientes-edit" type="button" title="Modificar">' +
        '<i class="fa fa-pencil"></i>' +
        '</button> ';
    {% endif %}

    {% if acl.isAllowedUser('clientes', 'delete') %}
    buttons += '<button class="btn btn-danger btn-sm clientes-delete" type="button" title="¿Desea eliminar?">' +
        '<i class="fa fa-remove"></i>' +
        '</button> ';
    {% endif %}

    {% endif %}
    $(document).ready(function(){

        $("body").on("click", ".btnInfoRutaValidar", function (){
            let idcliente = $(this).data("idcliente");
            let $tr = $(this).closest("tr");
            let datatr = clientesDT.row($tr).data();
            $("#idruta-validacion")
        });

        rutaDropzonePensionado = new Dropzone("div#rutaUploadOficioPensionado",{
            url: "/clientes/guardardescuento",
            acceptedFiles: "image/*,application/pdf",
            maxFiles: 1,
            dictInvalidFileType: "Tipo de archivo no permitido",
            maxFilesize: "10",
            dictFileTooBig: "Peso máximo: 10M",
            autoProcessQueue: false
        });

        rutaDropzonePensionado.on('maxfilesreached', function() {
            rutaDropzonePensionado.removeEventListeners();
        });

        rutaDropzonePensionado.on('removedfile', function (file) {
            //rutaDropzonePensionado.setupEventListeners();
        });

        rutaDropzonePensionado.on('error', function (file, error, response) {
            //rutaDropzonePensionado.removeFile(file);
            if(typeof response !== "undefined") {
                showGeneralMessage(response.statusText, "error", false);
                $("#btnGuardarModal").prop("disabled", false);
                $("#btnGuardarModal").prev().prop("disabled", false);
            }
        });

        rutaDropzonePensionado.on('success', function (file, response) {
            var json = JSON.parse(response);
            rutaDropzonePensionado.removeAllFiles(true);
            if(json.lOk == false){
                showGeneralMessage(json.cMensaje, "error", false);
            }
            else{
                $("#btnGuardarModal").prop("disabled", false);
                $("#btnGuardarModal").prev().prop("disabled", false);
                $("#clientes-descuento-modal").modal("hide");
                showGeneralMessage("Descuento guardado satisfactoriamente.", "success", true);
                buscar();
                
            }
        });

        rutaDropzonePensionado.on('complete', function (file, response) {
            $("#btnGuardarModal").prop("disabled", false);
            $("#btnGuardarModal").prev().prop("disabled", false);
            rutaDropzonePensionado.enable();
        });

        rutaDropzonePensionado.on("sending", function(file, xhr, formData){
            formData.append("clave", $("#clave").val());
            formData.append("idCliente", $("#clave").val());
            formData.append("activo", $("#activo").is(':checked'));
            formData.append("oficio", $("#cOficio").val());
            formData.append("fInicio", $("#fInicio").val());
            formData.append("fFin", $("#fFin").val());

            $("#btnGuardarModal").prop("disabled", true);
            $("#btnGuardarModal").prev().prop("disabled", true);
        });


        razones_sociales = {{ rss_json }};
        clientes_rs = $('#cliente_rs').selectize();

        $("input[name=rs_tipo_cliente]").on("change", function () {
            $("#rs_rfc").inputmask('remove');

            var valor = this.value;
            if(valor == "fisica"){
                $("#rs_rfc").inputmask({ "mask": "aaaa999999***", clearIncomplete: true});
            }
            else{
                $("#rs_rfc").inputmask({ "mask": "aaa999999***", clearIncomplete: true});
            }
        });

        $("#rs_calle").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
        $("#rs_rfc").inputmask({ "mask": "aaaa999999***", clearIncomplete: true});
        $("#rs_numero").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
        $("#rs_cruza1").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
        $("#rs_cruza2").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
        $("#rs_telefono").inputmask({ "mask": "(9999) 99-99-99"});
        $("#rs_correo").inputmask({ alias: "email", clearIncomplete: true});
        $("#rs_cp").inputmask({ "mask": "99999", "clearIncomplete": true});

        rs_idcolonia = $("#rs_idcolonia").selectize();
        $("#modal-razonsocial").on("shown.bs.modal", function(){
            $("#modal-razonsocial :input:not(.radio_tipo_cliente)").val("");
            $("#rs_tipo_cliente1")[0].checked = true;
            rs_idcolonia[0].selectize.setValue("", false);
        });

        $("#modal-razonsocial").on("hidden.bs.modal", function(){
            $("body").addClass("modal-open");
        });

        $("#rs_add_modal").on("click", function(e){
            e.preventDefault();
            $("#modal-razonsocial").modal({
                show: true,
                backdrop: "static"
            });
        });

        $("#btnGuardarRS").on("click", function () {
            var errors = 0;
            $("#modal-razonsocial .modal-body .has-error").removeClass("has-error");
            $("#modal-razonsocial .modal-body :input:not(.radio_tipo_cliente)").each(function(){
                var $el = $(this);
                if($el.hasClass("rs-required") && $el.val() == ""){
                    $el.closest(".form-group").addClass("has-error");
                    errors++;
                }
            });

            if(errors > 0){
                showGeneralMessage("Los campos marcados con * son obligatorios", "warning", true);
                return;
            }
            var data = {
                id: null,
                rfc: getTextValue($("#rs_rfc").val()),
                telefono: getTextValue($("#rs_telefono").val()),
                correo: getTextValue($("#rs_correo").val()),
                razon_social: getTextValue($("#rs_razon_social").val()),
                codigo_contpaqi: getTextValue($("#rs_codigo_contpaq").val()),
                nombre_comercial: getTextValue($("#rs_nombre_comercial").val()),
                calle: parseInt($("#rs_calle").val()),
                calle_letra: getTextValue($("#rs_calle_letra").val()),
                calle_tipo: getTextValueUpper($("#rs_calle_tipo").val()),
                numero: parseInt($("#rs_numero").val()),
                numero_letra: getTextValue($("#rs_numero_letra").val()),
                numero_tipo: getTextValue($("#rs_numero_tipo").val()),
                cruza1: getTextValue($("#rs_cruza1").val()),
                cruza1_letra: getTextValue($("#rs_cruza1_letra").val()),
                cruza1_tipo: getTextValue($("#rs_cruza1_tipo").val()),
                cruza2: getTextValue($("#rs_cruza2").val()),
                cruza2_letra: getTextValue($("#rs_cruza2_letra").val()),
                cruza2_tipo: getTextValue($("#rs_cruza2_tipo").val()),
                idcolonia: parseInt($("#rs_idcolonia").val()),
                fisica: $("input[name=rs_tipo_cliente]:checked").val() == "fisica" ? true : false,
                cp: parseInt($("#rs_cp").val()),
            }
            $.ajax({
                url: "/clientes/savers",
                data: $.param(data),
                type: "POST",
                beforeSend: function(){
                    $("#btnGuardarRS").html('<i class="fa fa-refresh fa-spin" aria-hidden="true"></i> Guardando...');
                    $("#btnGuardarRS").attr("disabled", "disabled");
                },
                success: function (resp) {
                    showGeneralMessage("La información se guardo correctamente", "success", true);
                    $("#modal-razonsocial").modal("hide");
                    var json = JSON.parse(resp);
                    razones_sociales.push(json);
                    if($("input[name=tipo_cliente]:checked").val() == "fisica" && json.fisica == "true"){
                        clientes_rs[0].selectize.addOption(json);
                    }


                },
                error: function(){
                    showGeneralMessage("Ocurrió un error al guardar la información", "error", true);
                },
                complete: function () {
                    $("#btnGuardarRS").html('Guardar');
                    $("#btnGuardarRS").removeAttr("disabled");
                }
            });
        });








        $("#cliente-location").on("click", function () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                   marker = createMarker(map, marker, position.coords.latitude, position.coords.longitude,initZoom);
                }, function() {
                    showGeneralMessage("Geolocalizacion fallo","warning", true);
                });
            } else {
                showGeneralMessage("Geolocalizacion no soportado","warning", true);
            }
        });



        $("input[name=tipo_cliente]").on("change", function () {
            $(".container-step-persona :input:not(.radio_tipo_cliente)").val("");
            $(".validate-required").removeClass("error");
            var valor = this.value;
            $("#cliente_rs").prev().html("Razón social");
            $("#cliente_rs").removeAttr("required");
            clientes_rs[0].selectize.clearOptions();

            if(valor == "fisica"){
                $(".ocultar-moral").hide();
                $(".ocultar-fisica").show();
                /*$("#wizard-form-t-3").hide();
                $("#wizard-form-t-4 .number").html("4.");
                $("#wizard-form-t-5 .number").html("5.");*/
                $("#wizard-form-t-4").hide();
                $("#wizard-form-t-5 .number").html("5.");
                $("#wizard-form-t-6 .number").html("6.");
                $(".validate-required").removeAttr("required");
                $(".validate-required").each(function(){
                    var text = $(this).prev().text();
                    $(this).prev("label").html(text.replace(" *", ""));
                });
                for(var i in razones_sociales){
                    var rs = razones_sociales[i];
                    if(rs.fisica == "true"){
                        clientes_rs[0].selectize.addOption(rs);
                    }
                }
            }
            else{
                $("#cliente_rs").prev().html("Razón social *");
                $("#cliente_rs").attr("required", "required");
                $(".ocultar-fisica").hide();
                $(".ocultar-moral").show();
                /*$("#wizard-form-t-3").show();
                $("#wizard-form-t-3 .number").html("4.");
                $("#wizard-form-t-4 .number").html("5.");
                $("#wizard-form-t-5 .number").html("6.");*/
                $("#wizard-form-t-4").show();
                $("#wizard-form-t-4 .number").html("5.");
                $("#wizard-form-t-5 .number").html("6.");
                $("#wizard-form-t-6 .number").html("7.");
                $(".validate-required").attr("required", "required");
                $(".validate-required").each(function(){
                    var text = $(this).prev().text();
                    $(this).prev("label").html(text + " *");
                });
                for(var i in razones_sociales){
                    var rs = razones_sociales[i];
                    if(rs.fisica == "true")
                        continue;
                    clientes_rs[0].selectize.addOption(rs);
                }
            }
        });


        $("#tipo_facturacion").on("change", function(){
            var valor = this.value;
            $("#facturacion_tamboreo").val("");
            $("#tamboreo_iva").val("");
            $("#tamboreo_sin_iva").val("");
            $("#cantidad_tamboreo").val("");
            if(valor == "Fija"){
                $(".ocultar-tamboreo").hide();
                $(".ocultar-fija").show();
                $("#monto_tasa_fija").attr("required", "required");
                $("#facturacion_tamboreo").removeAttr("required");
                $("#cantidad_tamboreo").removeAttr("required");
            }
            else{
                $(".ocultar-tamboreo").show();
                $(".ocultar-fija").hide();
                $("#monto_tasa_fija").removeAttr("required");
                $("#facturacion_tamboreo").attr("required", "required");
                $("#cantidad_tamboreo").attr("required", "required");
            }
        });

        $("#monto_tasa_fija").on("change", function(){
            var valor = this.value;
            $("#mensualidad").val(valor);
        });
        $("#facturacion_tamboreo").on("change", function(){
            if (!$("#chMontoManual").is(':checked'))
            {
                calcularTamboreo();
            }
        });

        $("#cantidad_tamboreo").on("keyup", function(){
            if (!$("#chMontoManual").is(':checked'))
            {
                calcularTamboreo();
            }
        });

        shortcut.add("Ctrl+Shift+A", function() {
//            console.log("shortcut");
            if($("#cliente-info-modal").css("display") != "none"){
//                console.log("shortcut+guardar");
                $("#wizard-form .actions  a[href=#finish]").click();
            }
        });

        /*$("#idruta").on("change", function(){
            var valor = this.value;
            $("#cobratario").val("");
            if(valor != ""){
                $.ajax({
                    url: "/clientes/rutacobratario/" + valor,
                    success: function(resp){
                        var json = JSON.parse(resp);
                        $("#cobratario").val(json.nombre);
                    }
                });
            }
        });*/
        idcoloniaForm = $("#idcolonia").selectize({
            render:       {
                item: function(data) {
                    return "<div data-value='"+data.value+"' data-ismarginado='"+data.ismarginado+"' class='item'>"+data.text+" </div>";
                }
            }
        });
        idcoloniaForm[0].selectize.on("change", function(){
            var valor = idcoloniaForm[0].selectize.getValue();
            if(valor != ""){
                setColoniaRuta(valor, null);
                let ismarginado = $("#idcolonia").data().selectize.options[$("#idcolonia").val()].ismarginado;
                $("#chkmarginado").prop("disabled", false);
                $("#chkmarginado")[0].checked = ismarginado;
                $("#chkmarginado").prop("disabled", true);
            }
            else {
                $("#mensualidad").val("");
            }
        });

        idrutaSelectize = $("#idruta").selectize();
        //input mask
//        $("#clientes-nombre-val").inputmask({
//            mask: "a",
//            "repeat": 50,
//            "greedy": false
//        });
        $("#clientes-nombre-val").inputmask('Regex', {regex: "^[a-zA-Z ]{1,50}?$"});
        $("#ultimo_anio_pago").inputmask({ "mask": "9999", "clearIncomplete": true});
        $("#correo").inputmask({ alias: "email", clearIncomplete: true});
        $("#cp").inputmask({ "mask": "99999", "clearIncomplete": true});
        $("#mensualidad").inputmask('Regex', {regex: "^[0-9]{1,9}(\\.\\d{1,2})?$"});
//        $('#mensualidad').inputmask('decimal', {
//            rightAlign: true
//        });
        $("#calle").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
        $("#numero").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
        $("#cruza1").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
        $("#cruza2").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
        $("#folio_catastral").inputmask({ "mask": "9", "repeat": 15, "greedy": false});
        $("#contacto_servicio_tel").inputmask({ "mask": "(9999) 99-99-99"});
        $("#contacto_servicio_ext").inputmask({ "mask": "9", "repeat": 10, "greedy": false});
        $("#contacto_pago1_tel").inputmask({ "mask": "(9999) 99-99-99"});
        $("#contacto_pago1_ext").inputmask({ "mask": "9", "repeat": 10, "greedy": false});
        $("#contacto_pago2_tel").inputmask({ "mask": "(9999) 99-99-99"});
        $("#telefono").inputmask({ "mask": "(9999) 99-99-99", "clearIncomplete": true});
        $("#contacto_pago2_ext").inputmask({ "mask": "9", "repeat": 10, "greedy": false});
        $("#cantidad_tamboreo").inputmask({ "mask": "9", "repeat": 10, "greedy": false});
        $("#monto_tasa_fija").inputmask('Regex', {regex: "^[0-9]{1,9}(\\.\\d{1,2})?$"});
        $("#tel_contacto").inputmask({ "mask": "(9999) 99-99-99"});
//        $('#monto_tasa_fija').inputmask('decimal', {
//            rightAlign: true
//        });

        clientesDT = $("#clientes-datatable").DataTable({
            // "dom": "<'row'<'col-sm-6'><'col-sm-6'fB>>" +
            // "<'row'<'col-sm-12'tr>>" +
            // "<'row'<'col-sm-12'i>>" +
            // "<'row'<'col-sm-6'l><'col-sm-6'p>>",
            "dom": '<"clear"><"top container-float"<"filter-located">f><rt><"bottom"lip><"clear">',
            "autoWidth": false,
            "paging": true,
            "select": true,
            "info": true,
            // searching: true,
            "processing": true,
//            "scrollY":        "506px",
            "scrollCollapse": false,
            "pagingType": "full",
            "language": {
                "paginate": {
                    "next": ">",
                    "first": "<<",
                    "last": ">>",
                    "previous": "<"
                },
                "search": "",
                "searchPlaceholder": "Buscar en los resultados encontrados",
                "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
                "infoEmpty": "",
                "infoFiltered": " - filtrado de _MAX_",
                "emptyTable": "Sin resultados",
                "sZeroRecords": "Sin resultados",
                processing: "Procesando ...",
                "lengthMenu": "Mostrar _MENU_ registros"
            },
            "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
            "pageLength": 10,
            "order": [[ 4, 'desc' ], [ 5, 'desc' ], [ 8, 'desc' ]],
            responsive: {
                details: {
                    type: 'column',
                    target: 1
                }
            },
            "processing": true,
            "serverSide": true,
            "deferLoading": 0,
            ajax: {
                url: "/clientes/buscar",
                "data": function ( d ) {
                    d.tipo_busqueda = $("#clientes-filter-select").val();
                },
                error: function(xhr, status, error){
                    var textError = "";
                    var tipoError = "";
                    $(window).resize();
                    $(window).resize();
                    if(xhr.status == 401) {
                        window.location = "/";
                    }
                    else {
                        showGeneralMessage("Ocurrió un error, contacte al administrador", "warning", true);
                    }
                }
            },
            //responsive: true,
            "columnDefs": [
                {
                    targets: [0],
                    orderable: false,
                    searchable: false,
                    defaultContent: buttons,
                    className: 'dt-center no-wrap',
                    {% if
                    not acl.isAllowedUser('clientes', 'edit')
                    and  not acl.isAllowedUser('clientes', 'info')
                    and not acl.isAllowedUser('clientes', 'delete')
                    %}
                    visible: false
                    {% endif %}
                },
                { targets: [1], orderable: false, className: 'control', searchable: false},
                { targets: [2], searchable: true, className: 'dt-center'},
                { targets: [3, 4, 5], className: 'dt-center'},
                { targets: [17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 30], visible: false },
                { targets: [28, 29], visible: false, searchable: false },
                { targets: '_all', visible: true }
            ],
            "lengthChange": true
        });

        $(window).on("resize", function(){
            clientesDT.columns.adjust();
        });

        $("#clientes-datatable").on( 'draw.dt', function () {
            $(".popover.confirmation").confirmation("destroy");
            clientesDT.columns.adjust();
        } );

        {% if
        acl.isAllowedUser('clientes', 'edit')
        or  acl.isAllowedUser('clientes', 'info')
        or acl.isAllowedUser('clientes', 'delete')
        or acl.isAllowedUser('clientes', 'bitmens')
        %}
        {% if acl.isAllowedUser('clientes', 'info') %}
        $("#clientes-datatable").off( 'click', '.clientes-info');
        $("#clientes-datatable").on( 'click', '.clientes-info', function () {
            var $tr = $(this).closest("tr");
            var idAttr = $tr.attr("id").split("-");
            var id = idAttr[1];
            if ( $tr.hasClass('selected') ) {
                //$tr.removeClass('selected');
            }
            else {
                clientesDT.$('tr.selected').removeClass('selected');
                $tr.addClass('selected');
            }
            $("#cliente-info-modal").modal({
                show: true,
                backdrop: "static"
            });
            $("#idcliente").val(id);
            loadClienteInfo(id, "info");
            /* poner aqui la seccion para la consulta */
        });
        {% endif %}

        {% if acl.isAllowedUser('clientes', 'edit') %}
        $("#clientes-datatable").off( 'click', '.clientes-edit');
        $("#clientes-datatable").on( 'click', '.clientes-edit', function () {
            var $tr = $(this).closest("tr");
            var idAttr = $tr.attr("id").split("-");
            var id = idAttr[1];
            if ( $tr.hasClass('selected') ) {
                //$tr.removeClass('selected');
            }
            else {
                clientesDT.$('tr.selected').removeClass('selected');
                $tr.addClass('selected');
            }

            $("#idcliente").val(id);
            /* poner aqui la seccion para la ediction */
            loadClienteInfo(id, "edit");
        });
        {% endif %}

        {% if acl.isAllowedUser('clientes', 'delete') %}
        $("#clientes-datatable").off( 'click', '.clientes-delete');
        $("#clientes-datatable").on( 'click', '.clientes-delete', function () {
            $(".popover.confirmation").confirmation("destroy");
            var $tr = $(this).closest("tr");
            var idAttr = $tr.attr("id").split("-");
            var id = idAttr[1];
            $(this).confirmation({
                rootSelector: "body",
                container: "body",
                singleton: true,
                popout: true,
                btnOkLabel: "Si",
                onConfirm: function() {

                    if ( $tr.hasClass('selected') ) {
                        //$tr.removeClass('selected');
                    }
                    else {
                        clientesDT.$('tr.selected').removeClass('selected');
                        $tr.addClass('selected');
                    }
                    eliminarCLiente(id);
                },
                onCancel: function() {
                    $(this).confirmation('destroy');
                },
            });

            $(this).confirmation("show");
        });
        {% endif %}

        {% if acl.isAllowedUser('clientes', 'bitmens') %}
        $("#clientes-bitmens").on( 'click', function () {
            if(clientesDT.rows('.selected').data().length == 0){
                showGeneralMessage("Seleccione un registro", "warning", true);
                return ;
            }//fin:if(clientesDT.rows('.selected').data().length == 0)

            var rowData = clientesDT.row('tr.selected').data();
            var $tr = $(clientesDT.row('tr.selected').node());
            var idAttr = $tr.attr("id").split("-");
            var id = idAttr[1];
            if ( $tr.hasClass('selected') ) {
                //$tr.removeClass('selected');
            }
            else {
                clientesDT.$('tr.selected').removeClass('selected');
                $tr.addClass('selected');
            }
            var _this = this;
            var $ul = $(this).parents("ul");
            var $btn = $ul.prev();
            $.ajax({
                url: "/clientes/bitmens/" + id,
                beforeSend: function(){
                    $btn.prop("disabled", true);
                    $btn.html('<i class="fa fa-spin fa-spinner"></i> Cobro manual <span class="caret"></span>');
                },
                success: function (resp) {
                    $("body").append(resp);
                },
                error: function () {
                    showGeneralMessage("Ocurrió un error al obtener la informacion", "warning", true);
                },
                complete: function(){
                    $btn.html('Cobro manual <span class="caret"></span>');
                    $btn.prop("disabled", false);
                }
            });
        });
        {% endif %}
        
        {% endif %}


        {% if acl.isAllowedUser('clientes', 'adddescuento') %}
        $("#addDescuento").off( 'click');
        $("#addDescuento").on( 'click', function () {
            loadModalDescuento();
        });
        {% endif %}

        {% if acl.isAllowedUser('clientes', 'segdescpen') %}
        $("#segDescPen").off( 'click');
        $("#segDescPen").on( 'click', function () {
            if(clientesDT.rows('.selected').data().length == 0){
                showGeneralMessage("Seleccione un registro", "warning", true);
                return ;
            }

            var rowid = clientesDT.row("tr.selected").node().id;
            idClienteDescuento = rowid.split("-")[1]; 
            $.ajax({
                url: "/clientes/segdescpen/" + idClienteDescuento,
                success: function (resp) {
                    $("body").append(resp);
                },
                error: function () {
                    showGeneralMessage("Ocurrió un error al obtener la informacion", "warning", true);
                }
            });
        });
        {% endif %}

        {% if acl.isAllowedUser('clientes', 'infosegsta') %}
        $("#clientes-datatable").off( 'click', '.cliestat-change-bitacora');
        $("#clientes-datatable").on( 'click', '.cliestat-change-bitacora', function () {
            var $tr = $(this).closest("tr");
            var idAttr = $tr.attr("id").split("-");
            var id = idAttr[1];
            var idestatus = $(this).data("id");
            if ( $tr.hasClass('selected') ) {
                //$tr.removeClass('selected');
            }
            else {
                clientesDT.$('tr.selected').removeClass('selected');
                $tr.addClass('selected');
            }

            $.ajax({
                url: "/clientes/infosegsta/" + id,
                success: function (resp) {
                    $("body").append(resp);
                },
                error: function () {
                    showGeneralMessage("Ocurrió un error al obtener la informacion", "warning", true);
                },
            });
        });
        {% endif %}

        $('#clientes-datatable tbody').on( 'click', 'tr', function () {

            $("#clientes-chgmens").hide();
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');

            }
            else {
                clientesDT.$('tr.selected').removeClass('selected');
                $(this).toggleClass('selected');
                var rowdata = clientesDT.row(".selected").data();
                var $tr = clientesDT.row(".selected").node();
                var sigla = rowdata[28];
                var idAttr = $tr.id.split("-");
                var idcliente = idAttr[1];
                if(permeditmens.indexOf(sigla) > -1){
                    $("#clientes-chgmens").show();
                    $("#clientes-chgmens").data("idcliente", idcliente);
                    $("#chgmens-monto-antigu").val(rowdata[29]);
                }
            }
        });


    });

    $("#contenedor-filtros :input[type=text]").on('keypress', function(e){
        if(e.which == 13){
            buscar();
        }
    });

    /**
     * [loadClienteInfo realiza la consulta de cliente por clave]
     */
    var loadClienteInfo = function($indexCliente, mode) {
        if($indexCliente == null){
            return;
        }

        $.ajax({
            url: "/clientes/get/" + $indexCliente,
            type: 'GET',
            success: function(data){
                $data = jQuery.parseJSON(data);

                clearClienteInfo();
                setModeClienteInfo(mode);
                setClienteInfo($indexCliente, $data);

                $("#cliente-info-modal").modal({
                    show: true,
                    backdrop: "static"
                });
            }
        });
    };

    var clearClienteInfo = function() {
//        $('#wizard-form')[0].reset();
        $('#wizard-form input:not([name=tipo_cliente],#idcliente)').val("");
        // console.log("limpiar form...");

        $("#idcomprobante").val("");
        $("#ididentificacion").val("");
    }

    // Disable enabled button cancel
    // var button = $("#wizard-form").find('a[href="#cancel"]');
    //     button.attr("href", '#cancel-disabled');
    //     button.parent().addClass("disabled");

    /**
     * [togglePrevious Habilita o deshabilita los botones (previous, next, finish) para el modal de cliente ]
     * @param  {[boolean]} enable) boleano para habilitar o desabilitar
     */
    function togglePrevious(enable) { toggleButton("previous", enable); }
    function toggleNext    (enable) { toggleButton("next",     enable); }
    function toggleFinish  (enable) { toggleButton("finish",   enable); }
    function toggleButton(buttonId, enable)
    {
        if (enable)
        {
            // Enable disabled button
            var button = $("#wizard-form").find('a[href="#' + buttonId + '-disabled"]');
            button.attr("href", '#' + buttonId);
            button.parent().removeClass();
        }
        else
        {
            // Disable enabled button
            var button = $("#wizard-form").find('a[href="#' + buttonId + '"]');
            button.attr("href", '#' + buttonId + '-disabled');
            button.parent().addClass("disabled");
        }
    }

    var setModeClienteInfo = function(mode) {
        if(mode == "info"){
            validateForm = false;
           $('#cliente-info-modal .modal-title').html('Información');
            $("#wizard-form .content :input").attr("disabled", true);
            $("#wizard-form .actions").find("li:last").hide();
            $("#wizard-form .actions").find("li:last").prev().hide();

            $(".editcomp").css("display","none");
            $(".editiden").css("display","none");
            $(".editfoto").css("display","none");

            $(".infocomp").css("display","block");
            $(".infoiden").css("display","block");
            $(".infofoto").css("display","block");

            $(".obligatorio").attr("required",false);

            idcoloniaForm[0].selectize.disable();
            idrutaSelectize[0].selectize.disable();
           clientes_rs[0].selectize.disable();
            toggleFinish(false);
        }
        else if(mode == 'edit'){
            $('#cliente-info-modal .modal-title').html('Modificar');
           $("#wizard-form .content :input:not(.do-not-disabled)").removeAttr("disabled");
            $("#wizard-form .actions").find("li:last").show();
            $("#wizard-form .actions").find("li:last").prev().show();

            $(".editcomp").css("display","block");
            $(".editfoto").css("display","block");
            $(".editiden").css("display","block");

            $(".infocomp").css("display","block");
            $(".infoiden").css("display","block");
            $(".infofoto").css("display","block");

            $(".obligatorio").attr("required",false);

            idcoloniaForm[0].selectize.enable();
            idrutaSelectize[0].selectize.enable();
           clientes_rs[0].selectize.enable();
            validateForm = true;
            toggleFinish(true);
        }
        else if(mode == 'new'){
            $('#cliente-info-modal .modal-title').html('Cliente nuevo');
           $("#wizard-form .content :input:not(.do-not-disabled)").removeAttr("disabled");
            $("#wizard-form .actions").find("li:last").show();
            $("#wizard-form .actions").find("li:last").prev().show();

            $(".editcomp").css("display","block");
            $(".editiden").css("display","block");
            $(".editfoto").css("display","block");

            $(".infocomp").css("display","none");
            $(".infoiden").css("display","none");
            $(".infofoto").css("display","none");

            $(".obligatorio").attr("required",true);

            idcoloniaForm[0].selectize.enable();
            idrutaSelectize[0].selectize.enable();
           clientes_rs[0].selectize.enable();

            validateForm = true;
            toggleFinish(true);

        }
    };

    $('#cliente-info-modal').on('hidden.bs.modal', function () {
        clearClienteInfo();
        toggleFinish(true);
        setModeClienteInfo('new');
    });

    var setColoniaRuta = function(idcolonia, idruta){
        $.ajax({
            url: "/clientes/montocolonia/" + idcolonia,
            success: function(resp){
                var json = JSON.parse(resp);
                $("#mensualidad").val(json.monto);
                /*$("#idruta").html("<option value=''>Seleccionar</option>");
                for(var i in json.rutas){
                    $("#idruta").append("<option value='" + json.rutas[i].id + "'>" + json.rutas[i].nombre + "</option>");
                }
                if(idruta){
                    $("#idruta").val(idruta);
                }*/
            },
            error: function(){
                $("#mensualidad").val("");
            }
        });
    }



    function updateLatLng(lat, lng){
        $("#latitud").val(lat);
        $("#longitud").val(lng);
    }
    function setCenterMap(map, lat, lng, initZoom){
        map.setCenter({
            lat: lat, lng: lng
        });
//        map.setZoom(initZoom);
        google.maps.event.trigger(map, 'resize');
    }

    function clearMarker(marker){
        if(marker == null) return;
        google.maps.event.clearListeners(marker, 'dragend');
        marker.setMap(null);
        marker = null;
        return null;
    }

    function fitBoundMarker(map, marker){
        var bounds = new google.maps.LatLngBounds();
        bounds.extend(marker.getPosition());
        map.fitBounds(bounds);
    }
    function createMarker(map, marker, lat, lng, initZoom){
        lat = parseFloat(lat);
        lng = parseFloat(lng);
        clearMarker(marker);
        google.maps.event.trigger(map, 'resize');
        marker = new google.maps.Marker({
            position: {
                lat: parseFloat(lat),
                lng: parseFloat(lng)
            },
            map: map
        });

        google.maps.event.addListener(marker, 'dragend', function() {
            var position = marker.getPosition();
            var lat = position.lat();
            var lng = position.lng();
            updateLatLng(lat, lng);
//            setCenterMap(map, lat, lng, initZoom);
        });
//        setCenterMap(map, lat, lng, initZoom);
//        var bounds = new google.maps.LatLngBounds();
//        bounds.extend(marker.getPosition());
//        map.fitBounds(bounds);
        fitBoundMarker(map, marker);
        google.maps.event.trigger(map, 'resize');
        updateLatLng(lat, lng);
        return marker;
    }

    function formatValue(value) {
        var valueWithFormat = value;
        if(value == null || value == undefined) {
            valueWithFormat = "";
        } else {
            valueWithFormat = value.trim().toUpperCase()
        }
        return valueWithFormat;
    }
console.log("asda");
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZJImFXCY1MX5n3_OVhXg7Jgb9zBbLlLg" async defer></script>
{% if acl.isAllowedUser('clientes', 'editsta') %}
    {{ partial ("clientes/estatus/change") }}
{{ javascript_include("js/clientes-change-status.js") }}
{% endif %}

 {% if acl.isAllowedUser('clientes', 'chgmencs') or
     acl.isAllowedUser('clientes', 'chgmencf') or
     acl.isAllowedUser('clientes', 'chgmenci') or
     acl.isAllowedUser('clientes', 'chgmench') %}
     {{ partial ("clientes/mensualidad/change") }}
     {{ javascript_include("js/clientes-change-mensualidad.js") }}
 {% endif %}

{{ javascript_include("plugins/select2/select2.min.js") }}
    {{ javascript_include("plugins/select2/i18n/es.js") }}