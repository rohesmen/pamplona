<!-- Modal bitacora cambios mensualidad cliente -->
<div class="modal inmodal fade" id="cliente-bitestatus-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header cliente-modal-header">
                <button type="button" class="close" id="cliente-bitestatus-btnclose">
                    <span aria-hidden="true">
                        <i class="fa fa-remove"></i>
                </button>
                <h4 class="modal-title">Seguimiento de cambios de estatus</h4>
            </div>

            <div class="modal-body">
                <table class="display table-striped table-hover" id="cliente-bitestatus-table" style="width: 100%;">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Usuario</th>
                        <th>Estatus</th>
                        <th>Fecha</th>
                        <th>Origen</th>
                        <th>Motivo</th>
                    </tr>
                    </thead>
                    <tbody>
                    {% for b in bitacora %}
                        <tr>
                            <td></td>
                            <td>{{ b.usuario }}</td>
                            <td>{{ b.estatus }}</td>
                            <td>{{ b.fecha }}</td>
                            <td>{{ b.origen }}</td>
                            <td>{{ b.motivo }}</td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        historialDT = $('#cliente-bitestatus-table').DataTable({
            "dom": 'rt<"row"<"col-md-6 col-sm-6"i><"col-md-6 col-sm-6"p>>',
            responsive: {
                details: {
                    type: 'column',
                    target: 0
                }
            },
            "ordering": false,
            "columnDefs": [
                { targets: 0, className: 'control', orderable: false, searchable: false },
                { targets: '_all',  visible: true, orderable: false, searchable: false }
            ],
            "language": {
                "paginate": {
                    "next": ">",
                    "first": "<<",
                    "last": ">>",
                    "previous": "<"
                },
                "search": "",
                "searchPlaceholder": "Buscar en los resultados encontrados",
                "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
                "infoEmpty": "",
                "infoFiltered": " - filtrado de _MAX_",
                "emptyTable": "Sin resultados",
                "sZeroRecords": "Sin resultados",
                processing: "Procesando ...",
                "lengthMenu": "Mostrar _MENU_ registros"
            },
        });



        $("#cliente-bitestatus-modal").modal({
            show: true,
            backdrop: "static"
        });

        $("#cliente-bitestatus-btnclose").off("click");
        $("#cliente-bitestatus-btnclose").on("click", function () {
            $("#cliente-bitestatus-modal").modal("hide");
        });

        $("#cliente-bitestatus-modal").off("shown.bs.modal");
        $("#cliente-bitestatus-modal").on("shown.bs.modal", function(){
            historialDT.columns.adjust();
        });

        $("#cliente-bitestatus-modal").off("hidden.bs.modal");
        $("#cliente-bitestatus-modal").on("hidden.bs.modal", function(){
            $("#cliente-bitestatus-modal").remove();
        });
    });
</script>
<!-- /.Modal bitacora cambios mensualidad cliente -->
