<!-- Modal bitacora cambios  desceunto pensionado -->
<div class="modal inmodal fade" id="cliente-cliestat-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header cliente-modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <i class="fa fa-remove"></i>
                </button>
                <h4 class="modal-title">Cambiar estatus</h4>
            </div>

            <div class="modal-body">
                <input type="hidden" id="cliestat-idcliente">
                <input type="hidden" id="cliestat-sigla">
                <input type="hidden" id="cliestat-idestatus">
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <label for="cliestat-nomcom">Nombre comercio:</label>
                        <input id="cliestat-nomcom" type="text" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <label for="cliestat-motivo">Motivo</label>
                        <textarea id="cliestat-motivo" style="resize: none;" rows="5" type="text" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="cliestat-btnGuardar" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- /.Modal bitacora cambios desceunto pensionado -->
