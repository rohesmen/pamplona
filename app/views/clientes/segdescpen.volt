<!-- Modal bitacora cambios  desceunto pensionado -->
<div class="modal inmodal fade" id="cliente-bitdespen-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header cliente-modal-header">
                <button type="button" class="close" id="cliente-bitdespen-btnclose">
                    <span aria-hidden="true">
                        <i class="fa fa-remove"></i>
                        </button>
                <h4 class="modal-title">Seguimiento descuento pensionado</h4>
            </div>

            <div class="modal-body">
                <table class="display table-striped table-hover" id="cliente-bitdespen-table" style="width: 100%;">
                    <thead>
                    <tr>
                        <th></th>
                        <th>ID</th>
                        <th>Usuario aplicó</th>
                        <th>Activo</th>
                        <th>Meses</th>
                        <th>Documento</th>
                        <th>Fecha aplicación</th>
                        <th>Fecha cancelación</th>
                        <th>Usuario cancelación</th>
                        <th>Motivo cancelación</th>
                        <th>idpago</th>
                    </tr>
                    </thead>
                    <tbody>
                        {% for b in bitdescpen %}
                        <tr>
                            <th></th>
                            <th>{{ b.id }}</th>
                            <th>{{ b.usuario }}</th>
                            <th>{{ b.activo ? 'SI' : 'NO' }}</th>
                            <th>{{ b.meses }}</th>
                            <th>
                                <a href="/img/despen/{{ b.archivo }}" target="blank" class="btn btn-primary {{ b.archivo ? '' : 'disabled' }}">
                                    <i class="fa fa-paperclip"></i>
                                </a>
                            </th>
                            <th>{{ b.fecha }}</th>
                            <th>{{ b.fecha_cancelacion }}</th>
                            <th>{{ b.usuario_cancelacion }}</th>
                            <th>{{ b.motivo_cancelacion }}</th>
                            <th>{{ b.idpago }}</th>
                        </tr>
                        {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function () {
    bitdespenDT = $('#cliente-bitdespen-table').DataTable({
        "dom": 'rt<"row"<"col-md-6 col-sm-6"i><"col-md-6 col-sm-6"p>>',
        responsive: {
            details: {
                type: 'column',
                target: 0
            }
        },
        "ordering": false,
        "columnDefs": [
            { targets: 0, className: 'control', orderable: false, searchable: false },
            { targets: [1,-1], visible: false },
            { targets: '_all',  visible: true, orderable: false, searchable: false }
        ],
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
    });

    

    $("#cliente-bitdespen-modal").modal({
        show: true,
        backdrop: "static"
    });

    $("#cliente-bitdespen-btnclose").off("click");
    $("#cliente-bitdespen-btnclose").on("click", function () {
        $("#cliente-bitdespen-modal").modal("hide");
    });

    $("#cliente-bitdespen-modal").off("shown.bs.modal");
    $("#cliente-bitdespen-modal").on("shown.bs.modal", function(){
        bitdespenDT.columns.adjust();
    });

    $("#cliente-bitdespen-modal").off("hidden.bs.modal");
    $("#cliente-bitdespen-modal").on("hidden.bs.modal", function(){
        $("#cliente-bitdespen-modal").remove();
    });
});
</script>
<!-- /.Modal bitacora cambios desceunto pensionado -->
