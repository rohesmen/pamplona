<!-- Modal bitacora cambios  desceunto pensionado -->
<div class="modal inmodal fade" id="cliente-chgmens-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header cliente-modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <i class="fa fa-remove"></i>
                </button>
                <h4 class="modal-title">Cambiar monto de mensualidad</h4>
            </div>

            <div class="modal-body">
                <input type="hidden" id="chgmens-idcliente">
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <label for="chgmens-monto-antigu">Antigua mensualidad:</label>
                        <input id="chgmens-monto-antigu" type="text" class="form-control" disabled>
                    </div>
                    <div class="col-sm-6 form-group">
                        <label for="chgmens-monto">Nueva mensualidad:</label>
                        <input id="chgmens-monto" type="text" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <label for="chgmens-motivo">Motivo</label>
                        <textarea id="chgmens-motivo" style="resize: none;" rows="5" type="text" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="chgmens-btnGuardar" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- /.Modal bitacora cambios desceunto pensionado -->
