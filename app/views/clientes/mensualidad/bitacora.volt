<!-- Modal bitacora cambios mensualidad cliente -->
<div class="modal inmodal fade" id="cliente-bitmens-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header cliente-modal-header">
                <button type="button" class="close" id="cliente-bitmens-btnclose">
                    <span aria-hidden="true">
                        <i class="fa fa-remove"></i>
                        </button>
                <h4 class="modal-title">Bitácora de cambios de mensualidad</h4>
            </div>

            <div class="modal-body">
                <table class="display table-striped table-hover" id="cliente-bitmens-table" style="width: 100%;">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Usuario</th>
                        <th>Mensualidad</th>
                        <th>Fecha</th>
                        <th>Origen</th>
                        <th>Motivo</th>
                    </tr>
                    </thead>
                    <tbody>
                        {% for b in bitacora %}
                        <tr>
                            <td></td>
                            <td>{{ b.usuario }}</td>
                            <td>{{ b.mensualidad }}</td>
                            <td>{{ b.fecha }}</td>
                            <td>{{ b.origen }}</td>
                            <td>{{ b.motivo }}</td>
                        </tr>
                        {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function () {
    historialBitmensDT = $('#cliente-bitmens-table').DataTable({
        "dom": 'rt<"row"<"col-md-6 col-sm-6"i><"col-md-6 col-sm-6"p>>',
        responsive: {
            details: {
                type: 'column',
                target: 0
            }
        },
        "ordering": false,
        "columnDefs": [
            { targets: 0, className: 'control', orderable: false, searchable: false },
            { targets: '_all',  visible: true, orderable: false, searchable: false }
        ],
        "language": {
            "paginate": {
                "next": ">",
                "first": "<<",
                "last": ">>",
                "previous": "<"
            },
            "search": "",
            "searchPlaceholder": "Buscar en los resultados encontrados",
            "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
            "infoEmpty": "",
            "infoFiltered": " - filtrado de _MAX_",
            "emptyTable": "Sin resultados",
            "sZeroRecords": "Sin resultados",
            processing: "Procesando ...",
            "lengthMenu": "Mostrar _MENU_ registros"
        },
    });

    

    $("#cliente-bitmens-modal").modal({
        show: true,
        backdrop: "static"
    });

    $("#cliente-bitmens-btnclose").off("click");
    $("#cliente-bitmens-btnclose").on("click", function () {
        $("#cliente-bitmens-modal").modal("hide");
    });

    $("#cliente-bitmens-modal").off("shown.bs.modal");
    $("#cliente-bitmens-modal").on("shown.bs.modal", function(){
        historialBitmensDT.columns.adjust();
    });

    $("#cliente-bitmens-modal").off("hidden.bs.modal");
    $("#cliente-bitmens-modal").on("hidden.bs.modal", function(){
        $("#cliente-bitmens-modal").remove();
    });
});
</script>
<!-- /.Modal bitacora cambios mensualidad cliente -->
