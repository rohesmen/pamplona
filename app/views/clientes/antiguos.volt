<div class="container-fluid">
    <!-- add folio -->
    <div class="row">
        <div class="col-md-3 form-group">
            <label>Cliente</label>
            <input id="folio" type="text" class="form-control required">
        </div>
        <div class="col-md-1 form-group">
            <div>&nbsp;</div>
            <button id="btnSearch" type="button" class="btn btn-primary">
                <i class="fa fa-search"></i>
            </button>
        </div>
        <div class="col-md-3 form-group">
            <label>Mes</label>
            <input id="mes" disabled class="form-control required">
        </div>
        <div class="col-md-3 form-group">
            <label>Año</label>
            <input id="anio" disabled class="form-control required">
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 form-group">
            <label>Nuevo mes</label>
            <input id="newmes" class="form-control required">
        </div>
        <div class="col-md-3 form-group">
            <label>Nuevo año</label>
            <input id="newanio" class="form-control required">
        </div>
        <div class="col-md-1 form-group">
            <div>&nbsp;</div>
            <button id="bntSave" type="button" class="btn btn-primary">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <!-- ./add folio -->
</div>
<script>
    $("#btnSearch").on("click", function () {
        $.ajax({
            url: '/clientes/antiguos/' + $("#folio").val(),
            success: function (resp) {
                var json = JSON.parse(resp);
                $("#mes").val(json.mes);
                $("#anio").val(json.anio);
            }
        });
    });
    $("#bntSave").on("click", function () {
        $.ajax({
            url: '/clientes/antiguos/' + $("#folio").val(),
            type: 'POST',
            data: JSON.stringify({
                mes: $("#newmes").val(),
                anio: $("#newanio").val(),
            }),
            success: function (resp) {
                showGeneralMessage("guardado", "success", true);
                $("#btnSearch").click();
            }
        });
    });
</script>