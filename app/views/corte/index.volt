<!-- View Clientes -->
{{ content() }}

{#<link href="plugins/datetime-picker/bootstrap-datetimepicker.min.css" rel="stylesheet">#}
{#<link href="plugins/datatables_new/datatables.min.css" rel="stylesheet">#}
{#<link href="plugins/datatables_new/Buttons-1.2.2/css/buttons.bootstrap.min.css" rel="stylesheet">#}
{#<link href="plugins/select2/select2.min.css" rel="stylesheet">#}

{#<link href="plugins/selectize/selectize.css" rel="stylesheet">#}
{#<link href="css/pamplona/corte.css" rel="stylesheet">#}
<link href="../plugins/datepicker/bootstrap-datepicker.css" rel="stylesheet">

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Corte</h2>
    </div>
    <div class="col-lg-2">
        <h2>
            <button id="searchFolio" class="btn btn-primary" type="button" title="Búsqueda folio">
                Búsqueda folio
            </button>
        </h2>
    </div>
</div>
<div class="wrapper wrapper-content" id="corteSearch">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    {#<div class="container-fluid" style="border: 1px solid #ddd;">#}
                        <div style="border: 1px solid #ddd;">
                            <nav class="navbar navbar-default" style="border-color: #000000; margin-bottom: 0px;">
                                <div>
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header" style="float: none; display: block;">
                                        <button type="button" id="colapse-busqueda" class="navbar-toggle" data-toggle="collapse"
                                                data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
                                    </div>

                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="navbar-collapse collapse in clientes-only" id="contenedor-filtros" aria-expanded="true">
                                        {#<form id="frmPersonas" name="frmPersonas" class="navbar-form navbar-right" role="search" style="border: none;">#}
                                        <div class="nav-bar">
                                            {#<div class="col-sm-12 col-md-12 form-inline" id="clientes-filtro-direccion">#}
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                        <label>Filtros de búsqueda:</label>
                                                        <input type="hidden" id="idBusquedaPagos" value=""/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-9 col-sm-8">
                                                        <div class="row">
                                                            <div class="col-xs-6 col-md-3 col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="corte_busqueda_fIni">Fecha de Ini.</label>
                                                                    <input id="corte_busqueda_fIni" name="corte_busqueda_fIni"
                                                                           type="text" class="form-control" placeholder="Fecha Inicial"
                                                                           value="{{ date("d-m-Y") }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-md-3 col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="corte_busqueda_fFin">Fecha de Fin.</label>
                                                                    <input id="corte_busqueda_fFin" name="corte_busqueda_fFin" type="text" class="form-control"
                                                                           placeholder="Fecha Final" value="{{ date("d-m-Y") }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-6 form-group">
                                                                <label for="corte_busqueda_cCobratario">Cobratario</label>
                                                                <select class="form-control" id="corte_busqueda_cCobratario" style="width: 100%;">
                                                                    <option value=""> -- Todos -- </option>
                                                                    {% for cobratario in cobratarios %}
                                                                        <option value="{{ cobratario.id }}">{{ cobratario.nombre }} {{ cobratario.apellido_paterno }} {{ cobratario.apellido_materno }}</option>
                                                                    {% endfor %}
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-6 form-group">
                                                                <label for="corte_busqueda_cTipo">Tipo Cliente</label>
                                                                <select class="form-control" id="corte_busqueda_cTipo" style="width: 100%;">
                                                                    <option value="4"> -- Todos -- </option>
                                                                    <option value="1">Empresa</option>
                                                                    <option value="2">Persona</option>
                                                                    <option value="3">Pendiente</option>
                                                                </select>
                                                            </div>
{#                                                            <div class="col-xs-12 col-md-1 col-sm-6">#}
{#                                                                <button class="btn btn-primary corte-search-do hidden-xs hidden-sm pb" type="button" title="Buscar">#}
{#                                                                    <i class="fa fa-search"></i>#}
{#                                                                </button>#}
{#                                                                <button class="btn btn-primary btn-block corte-search-do hidden-md hidden-lg" style="margin-bottom: 10px;"#}
{#                                                                        type="button" title="Buscar">#}
{#                                                                    <i class="fa fa-search"></i>#}
{#                                                                </button>#}
{#                                                            </div>#}
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12 col-md-3 col-sm-4" style="margin-top: 22px;">
                                                        <div class="row">

                                                            <div class="col-md-3">
                                                                <button class="btn btn-primary corte-search-do hidden-xs hidden-sm pb" type="button" title="Buscar">
                                                                    <i class="fa fa-search"></i>
                                                                </button>
                                                                <button class="btn btn-primary btn-block corte-search-do hidden-md hidden-lg" style="margin-bottom: 10px;"
                                                                        type="button" title="Buscar">
                                                                    <i class="fa fa-search"></i>
                                                                </button>
                                                            </div>

                                                            {% if acl.isAllowedUser('corte', 'add') %}
                                                                <div class="col-md-3">
                                                                    <button class="btn btn-primary corte-add-do hidden-xs hidden-sm pb" type="button" title="Nuevo corte">
                                                                        <i class="fa fa-plus"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary btn-block corte-add-do hidden-md hidden-lg" type="button" title="Nuevo corte">
                                                                        <i class="fa fa-plus"></i>
                                                                    </button>
                                                                </div>
                                                            {% endif %}

                                                            {#% if acl.isAllowedUser('corte', 'export') %#}

                                                                <div class="col-md-3">
                                                                    <button class="btn btn-primary hidden-xs hidden-sm pb" id="btnExportCorte" type="button" title="Exportar corte excel">
                                                                        <i class="fa fa-file-excel-o"></i>
                                                                    </button>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <button class="btn btn-primary hidden-xs hidden-sm pb" id="btnExportCortePDF" type="button" title="Exportar corte pdf">
                                                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                                    </button>
                                                                </div>

                                                            {#% endif %#}

                                                        </div>
                                                    </div>
                                                </div>
                                            {#</div>#}
                                        </div>
                                        {#</form>#}

                                    </div><!-- /.navbar-collapse -->
                                </div><!-- /.container-fluid -->

                            </nav>
                        </div>


                        {#<div class="row">#}
                            {#<div class="col-sm-12 col-md-12 form-inline" id="clientes-filtro-direccion">#}
                                {#<div class="row">#}
                                    {#<div class="col-md-12 col-sm-12">#}
                                        {#<label>Filtros de búsqueda:</label>#}
                                        {#<input type="hidden" id="idBusquedaPagos" value=""/>#}
                                    {#</div>#}
                                {#</div>#}
                                {#<div class="row">#}
                                    {#<div class="col-md-11 col-sm-8">#}
                                        {#<div class="row">#}
                                            {#<div class="col-xs-6 col-md-3 col-sm-6">#}
                                                {#<div class="form-group">#}
                                                    {#<label for="corte_busqueda_fIni">Fecha de Ini.</label>#}
                                                    {#<input id="corte_busqueda_fIni" name="corte_busqueda_fIni" type="text" class="form-control" placeholder="Fecha Inicial">#}
                                                {#</div>#}
                                            {#</div>#}
                                            {#<div class="col-xs-6 col-md-3 col-sm-6">#}
                                                {#<div class="form-group">#}
                                                    {#<label for="corte_busqueda_fFin">Fecha de Fin.</label>#}
                                                    {#<input id="corte_busqueda_fFin" name="corte_busqueda_fFin" type="text" class="form-control" placeholder="Fecha Final">#}
                                                {#</div>#}
                                            {#</div>#}
                                            {#<div class="col-md-2 col-sm-6 form-group">#}
                                                {#<label for="corte_busqueda_cCobratario">Cobratario</label>#}
                                                {#<select class="form-control" id="corte_busqueda_cCobratario" style="width: 100%;">#}
                                                    {#<option value=""> -- Todos -- </option>#}
                                                    {#{% for cobratario in cobratarios %}#}
                                                        {#<option value="{{ cobratario.id }}">{{ cobratario.nombre }} {{ cobratario.apellido_paterno }} {{ cobratario.apellido_materno }}</option>#}
                                                    {#{% endfor %}#}
                                                {#</select>#}
                                            {#</div>#}
                                            {#<div class="col-md-3 col-sm-6 form-group">#}
                                                {#<label for="corte_busqueda_cTipo">Tipo Cliente</label>#}
                                                {#<select class="form-control" id="corte_busqueda_cTipo" style="width: 100%;">#}
                                                    {#<option value="4"> -- Todos -- </option>#}
                                                    {#<option value="1">Empresa</option>#}
                                                    {#<option value="2">Persona</option>#}
                                                    {#<option value="3">Pendiente</option>#}
                                                {#</select>#}


                                                {#<?PHP /*<label for="corte_busqueda_cTipo">Tipo</label><br>#}
                                                {#<div class="radio">#}
                                                  {#<label>#}
                                                    {#<input type="radio" name="corte_busqueda_cTipo" id="corte_busqueda_cTipo" value="1">#}
                                                    {#Empresa#}
                                                  {#</label>#}
                                                {#</div>#}
                                                {#<div class="radio">#}
                                                  {#<label>#}
                                                    {#<input type="radio" name="corte_busqueda_cTipo" id="corte_busqueda_cTipo" value="2">#}
                                                    {#Persona#}
                                                  {#</label>#}
                                                {#</div>#}
                                                {#<div class="radio">#}
                                                  {#<label>#}
                                                    {#<input type="radio" name="corte_busqueda_cTipo" id="corte_busqueda_cTipo" value="3" checked>#}
                                                    {#Ambos#}
                                                  {#</label>#}
                                                {#</div> */ ?>#}
                                            {#</div>#}
                                            {#<div class="col-md-1 col-sm-6">#}
                                                {#<button class="btn btn-primary corte-search-do hidden-xs hidden-sm pb" type="button" title="Buscar">#}
                                                    {#<i class="fa fa-search"></i>#}
                                                {#</button>#}
                                                {#<button class="btn btn-primary btn-block corte-search-do hidden-md hidden-lg" style="margin-bottom: 10px;"#}
                                                        {#type="button" title="Buscar">#}
                                                    {#<i class="fa fa-search"></i>#}
                                                {#</button>#}
                                            {#</div>#}
                                        {#</div>#}
                                    {#</div>#}
                                    {#<div class="col-md-1 col-sm-4">#}
                                                {#{% if acl.isAllowedUser('corte', 'add') %}#}
                                                    {#<button class="btn btn-primary corte-add-do hidden-xs hidden-sm pb" type="button" title="Nuevo corte">#}
                                                        {#<i class="fa fa-plus"></i>#}
                                                    {#</button>#}
                                                    {#<button class="btn btn-primary btn-block corte-add-do hidden-md hidden-lg" type="button" title="Nuevo corte">#}
                                                        {#<i class="fa fa-plus"></i>#}
                                                    {#</button>#}
                                                {#{% endif %}#}
                                    {#</div>#}
                                {#</div>#}
                            {#</div>#}
                        {#</div>#}
                    {#</div>#}
                    <div style="margin-top: 15px;">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="container-montos">
                                    <div class="montos-numero" id="monto-total-label">$ 0</div>
                                    <div class="montos-descripcion">Monto calculado</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="container-montos">
                                    <div class="montos-numero" id="monto-entregado-label">$ 0</div>
                                    <div class="montos-descripcion">Monto entregado</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="container-montos">
                                    <div class="montos-numero" id="monto-diferencia-label">$ 0</div>
                                    <div class="montos-descripcion">Diferencia</div>
                                </div>
                            </div>
                        </div>
                        <table id="tblCorte"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                          <thead>
                            <tr>
                              <th></th>
                              <th>activo</th>
                              <th class="never">id</th>
                              <th>Cobratario</th>
                              <th class="never">id_cobratario</th>
                              <th>Monto Calculado</th>
                              <th>Monto Entregado</th>
                              <th>Fecha Inicial</th>
                              <th>Fecha Final</th>
                              <th>Usuario Creación</th>
                              <th>Fecha Creación</th>
                              <th >Diferencia</th>
                              <th class="never">Persona</th>
                              <th class="never">Activo</th>
                              <th class="never">Pendiente</th>
                              <th class="never">Salario</th>
                              <th class="never">Pasaje</th>
                            </tr>   
                          </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- view content -->


<div class="modal fade" role="dialog" id ="corteInfo">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal_corte_nombre">Corte</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="ibox">
            <div class="ibox-content">
                <div class="container-fluid" style="border: 1px solid #ddd;">
                    <div class="row">
                        <div class="col-sm-12 col-md-12" id="">
                            <br>
                                <div class="container-fluid">
                                <form id="formCorte" class="">
                                    <input type="hidden" id="idcorte" />
                                    <input type="hidden" id="lP" />
                                    <input type="hidden" id="corte-cE" />
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 text-center">
                                        <h3 class="modal-title" id="corte-titulo-modal">Nuevo Corte</h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="corte_add_cCobratario">Cobratario *</label>
                                                <select class="form-control required" name="corte_add_cCobratario" id="corte_add_cCobratario" style="width: 100%;" >
                                                    <option value="" selected> -- Seleccione -- </option>
                                                    {% for cobratario in cobratarios %}
                                                        <option value="{{ cobratario.id }}">{{ cobratario.nombre }} {{ cobratario.apellido_paterno }} {{ cobratario.apellido_materno }}</option>
                                                    {% endfor %}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6" style="display: none;">
                                            <?PHP /*<label for="corte_add_cTipo">Tipo</label><br>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="corte_add_cTipo" id="corte_add_cTipo" value="1" class="required">
                                                    Empresa
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="corte_add_cTipo" id="corte_add_cTipo" value="2" checked>
                                                    Persona
                                                </label>
                                            </div> */ ?>
                                            <div class="form-group">
                                                <label for="corte_add_cTipo">Tipo Cliente *</label>
                                                <select class="form-control required" name="corte_add_cTipo" id="corte_add_cTipo" style="width: 100%;" >
                                                    <option value="1"> Empresa </option>
                                                    <option value="2" selected> Persona </option>
                                                    <option value="3"> Pendiente </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 form-group">
                                                <label for="corte_add_fIni">Fecha inicial *</label>
                                                <input id="corte_add_fIni" name="corte_add_fIni" type="text" class="form-control" placeholder="Fecha Inicial" required disabled>
                                        </div>
                                        <div class="col-md-6 col-sm-6 form-group">
                                                <label for="corte_add_fFin">Fecha final *</label>
                                                <input id="corte_add_fFin" name="corte_add_fFin" type="text" class="form-control" placeholder="Fecha Final" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="corte_add_montoE">Monto Entregado *</label>
                                                <input id="corte_add_montoE" name="corte_add_montoE" class="form-control" placeholder="00.00" value="00.00" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <label for="corte_add_montoC">Monto Calculado</label>
                                            <input id="corte_add_montoC" name="corte_add_montoC" type="text" class="al-right form-control" placeholder="00.00" value="00.00" readonly>
                                            <input id="corte_add_montoC2" type="text" class="al-right form-control" placeholder="00.00" value="00.00" readonly style="display: none;">
                                            <input id="corte_add_montoC_h" name="corte_add_montoC_h" type="hidden" value="0">
                                            <span class="glyphicon glyphicon-repeat gly-spin icon_calculando" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-md-offset-6 col-sm-offset-6">
                                            <div class="form-group">
                                                <label for="corte_add_diff">Diferencia</label>
                                                <input id="corte_add_diff" name="corte_add_diff" type="text" class="al-right form-control" placeholder="00.00" value="00.00" readonly>
                                                <input id="corte_add_diff2" type="text" class="al-right form-control" placeholder="00.00" value="00.00" readonly style="display: none;">
                                                <span class="glyphicon glyphicon-repeat gly-spin icon_calculando" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="corte_add_montoS">Salario *</label>
                                                <input id="corte_add_montoS" name="corte_add_montoS" type="text" class="form-control" placeholder="00.00" value="00.00" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <label for="corte_add_montoP">Pasaje *</label>
                                            <input id="corte_add_montoP" name="corte_add_montoP" type="text" class="al-right form-control" placeholder="00.00" value="00.00" required>
                                        </div>
                                    </div>
                                    {% if acl.isAllowedUser('corte', 'add') %}
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-md-offset-6 col-sm-offset-6">
                                                <div class="form-group">
                                                    <button class="btn btn-primary corte-guarda-do hidden-xs hidden-sm pb" type="button" title="Generar corte">
                                                        <i class="fa fa-save"></i>
                                                    </button>
                                                    <button class="btn btn-primary btn-block corte-guarda-do hidden-md hidden-lg" type="button" title="Generar corte">
                                                        <i class="fa fa-save"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                            
                                    {% endif %}
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    
      </div>
    </div>
  </div>
</div>



<div class="modal fade" tabindex="-1" role="dialog" id ="corteElimina">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal_corte_nombre">Corte</h4>
      </div>
      <div class="modal-body">
            ¿Cancelar el corte?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnEliminar">Aceptar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id ="modalSearchFolio">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_corte_nombre">Búsqueda por folio</h4>
            </div>
            <div class="modal-body">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Buscar por folio" id="txtFolio">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button" id="btnSearchFolio">Buscar</button>
                    </span>
                </div><!-- /input-group -->
                <div class="row" style="margin-bottom: 10px; margin-top: 10px;">
                    <div class="col-md-12">
                        <b>Cliente: </b><span id="txtNombreFolio"></span>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-5">
                        <b>Calle: </b><span id="txtCalleFolio"></span>
                    </div>
                    <div class="col-md-2">
                        <b>Número: </b><span id="txtNumeroFolio"></span>
                    </div>
                    <div class="col-md-5">
                        <b>Colonia: </b><span id="txtColoniaFolio"></span>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <b>Cobratario: </b><span id="txtCobratarioFolio"></span>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-4">
                        <b>Fecha: </b><span id="txtFechaFolio"></span>
                    </div>
                    <div class="col-md-4">
                        <b>Monto: </b><span id="txtMontoFolio"></span>
                    </div>
                    <div class="col-md-4">
                        <b>En corte: </b><span id="txtCorteFolio"></span>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <b>Meses: </b><span id="txtMesesFolio"></span>
                    </div>
                </div>
            </div>
{#            <div class="modal-footer">#}
{#                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>#}
{#                <button type="button" class="btn btn-primary" id="btnEliminar">Aceptar</button>#}
{#            </div>#}
        </div>
    </div>
</div>

<form method="get" action="prueba.php" target="_blank" id="imprimirReporteCorte">
</form>

<div class="modal fade" tabindex="-1" role="dialog" id ="cortePagos">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" >Detalle de pagos</h4>
      </div>
      <div class="modal-body">
           <table id="tblPagos"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
              <thead>
                <tr>
                  {#<th class="none">Folio pago</th>#}
                    <th >Clave</th>
                    <th >Folio interno</th>
                    <th class="none">Mes pago</th>
                    {#<th class="none">Año pago</th>#}
                    <th class="none">Dirección</th>
                    <th class="none">Colonia</th>
                  <th>Fecha de Pago</th>
                  <th>Cantidad</th>
                  {#<th>Estatus</th>#}
                </tr>   
              </thead>
            </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

{#{{ javascript_include("plugins/datetime-picker/es.js") }}#}
{#{{ javascript_include("plugins/datetime-picker/bootstrap-datetimepicker.min.js") }}#}
<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>
{#{{ javascript_include("plugins/datatables_new/datatables.min.js") }}#}
{{ javascript_include("plugins/validate/jquery.validate.min.js") }}

{{ javascript_include("plugins/datatables_new/Buttons-1.2.2/js/dataTables.buttons.js") }}
{{ javascript_include("plugins/datatables_new/JSZip-2.5.0/jszip.js") }}
{{ javascript_include("plugins/datatables_new/pdfmake-0.1.18/build/pdfmake.js") }}
{{ javascript_include("plugins/datatables_new/pdfmake-0.1.18/build/vfs_fonts.js") }}
{{ javascript_include("plugins/datatables_new/Buttons-1.2.2/js/buttons.bootstrap.js") }}
{{ javascript_include("plugins/datatables_new/Buttons-1.2.2/js/buttons.html5.js") }}
{{ javascript_include("plugins/datatables_new/Buttons-1.2.2/js/buttons.colVis.js") }}
{{ javascript_include("plugins/datatables_new/sum.js") }}

{#{{ javascript_include("plugins/select2/select2.min.js") }}#}
{#{{ javascript_include("plugins/selectize/selectize.min.js") }}#}

{{ javascript_include("js/corte-events.js") }}

<script type="application/javascript">

    var dataTable = "";
    var calculando_monto = false;
    function formateaDate(date) {
        var d = new Date(date || Date.now()),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day, month, year].join('-');
    }

    function calculaDiferencia(){
        var mCal = $("#corte_add_montoC_h").val();
        var mCap = $("#corte_add_montoE").val();

        if(mCal == "") mCal = 0;
        if(mCap == "") mCap = 0;

        var dif = mCap.toString().replace(",", "") - mCal.toString().replace(",", "");
        $("#corte_add_diff").val('' + parseFloat(dif, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
    }

    function calculaMonto(fIni,fFin, cCobratario, cTipo = 0){
        $(".icon_calculando").show();
        calculando_monto = true;
        $("#corte_add_montoC").val("00.00");
        $("#corte_add_montoC_h").val(0);
        if(fIni != null && fIni != "" && fFin != null && fFin != "" && cCobratario != ""){
            $.ajax({
                url: "/corte/monto/",
                data: { fIni: fIni, fFin : fFin, cPer : cTipo, cCob: cCobratario } ,
                contentType: 'application/json; charset=utf-8',
                type: 'GET',
                dataType: "json",
                success: function(data){
                    if(data.lError==false){
                        $("#corte_add_montoC").val(data.cMensaje);
                        $("#corte_add_montoC_h").val(data.cC);
                        $("#lP").val(data.cP);
                        calculando_monto = false;
                        calculaDiferencia();
                    }else{
                        $("#corte_add_montoC").val("00.00");
                        $("#corte_add_montoC_h").val(0);
                        $("#lP").val("");
                        calculando_monto = false;
                        calculaDiferencia();
                    }
                    $(".icon_calculando").hide();
                },   
                error: function(){
                    $("#corte_add_montoC").val("00.00");
                    $("#corte_add_montoC_h").val(0);
                    $("#lP").val("");
                    $(".icon_calculando").hide();
                    calculando_monto = false;
                    calculaDiferencia();
                }
            });
        }else{
            $("#corte_add_montoC").val("00.00");
            $("#corte_add_montoC_h").val(0);
            $("#lP").val("");
            $(".icon_calculando").hide();
        }
    }

    
    $(document).ready(function(){
        //$('#corte_add_cCobratario').select2();
        //$('#corte_busqueda_cCobratario').select2();
        $('#corte_busqueda_cCobratario').selectize({
            create:false,
            sortField:'text'
        });

        $('#corte_add_cCobratario').selectize({
            create:false,
            sortField:'text'
        });
        
        $("#corteInfo").on("hidden.bs.modal", function () {
            $("#corte_add_diff2").hide();
            $("#corte_add_montoC2").hide();
            $("#corte_add_diff").show();
            $("#corte_add_montoC").show();
        });

        var validator = $("#formCorte").validate({
            ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
            messages: {
                corte_add_fIni: "Seleccione la fecha de inicio",
                corte_add_fFin: "Seleccione la fecha fin",
                corte_add_cCobratario: "Seleccione el cobratario",

                corte_add_montoE: {
                    required: "Capture el monto entregado",
                    
                },
                corte_add_montoS: {required: "Capture el monto del salario",},
                corte_add_montoP: {required: "Capture el monto del pasaje",}
            }
        });

        $( "#corte_add_montoE" ).keyup(function() {
          calculaDiferencia();
        });

        $(".icon_calculando").hide();
        shortcut.add("Ctrl+Shift+A", function() {
            if($("#cliente-info-modal").css("display") != "none"){
                $("#wizard-form .actions  a[href=#finish]").click();
            }
        });

        {% if acl.isAllowedUser('corte', 'add') %}
        $(".corte-add-do").on('click', function () {
            $('#corte_add_fFin').datetimepicker('update', new Date());
            
            $("#corte-titulo-modal").text("Nuevo Corte");
            habilitaForm();
            reiniciaForm();
            $('.corte-guarda-do').show();
            $('#corteInfo').modal('show');

        });

        $(".corte-guarda-do").attr("disabled",false);
        $(".corte-guarda-do").on("click",function(){
            var resp = $('#formCorte').valid();
            if(resp){
                $(this).attr("disabled",true);
                data = {
                    id: $("#idcorte").val() != "" ? $("#idcorte").val() : null,
                    fI: $("#corte_add_fIni").val(),
                    fF: $("#corte_add_fFin").val(),
                    cT: parseInt($("#corte_add_cTipo").val()),
                    cC: parseInt($("#corte_add_cCobratario").val()),
                    mE: ($("#corte_add_montoE").val()),
                    mC: ($("#corte_add_montoC").val()),
                    mD: ($("#corte_add_diff").val()),
                    mS: ($("#corte_add_montoS").val()),
                    mP: ($("#corte_add_montoP").val()),
                    lP: $("#lP").val()
                };
                var u = (data['id']!=null?'/corte/edit/':'/corte/save/');

                $.ajax({
                url: u,
                data: $.param(data),
                type: 'POST',
                beforeSend: function(){
                    $(".corte-guarda-do").html('<i class="fa fa-spin fa-spinner"></i>');
                },
                success: function(resp){
                    $(".corte-guarda-do").html('<i class="fa fa-save"></i>');
                    var json = JSON.parse(resp);
                    if(json.lError == false){
                        dataTable.ajax.reload();
                        toastr.success('','Corte guardado con éxito!', {timeOut: 5000});
                        $("#corteInfo").modal("hide");
                    }else{
                        toastr.error("No fue posible registrar el corte", json.cMensaje);
                    }
                    
                    $(".corte-guarda-do").attr("disabled",false);
                },
                error: function(err){
                    $(".corte-guarda-do").html('<i class="fa fa-save"></i>');
                    toastr.error('No fue posible generar el corte', err);
                }
            });
            }
        });
        {% endif %}

        $("#btnRegresar_corte").on('click', function () {
            $('#myModal').modal(options);
        });

        $("#corte_add_montoE").inputmask("decimal", { allowMinus: false, radixPoint: ".", autoGroup: true, groupSeparator: ",", groupSize: 3, digits: 2, "placeholder": "0.00" });
        $("#corte_add_montoS").inputmask("decimal", { allowMinus: false, radixPoint: ".", autoGroup: true, groupSeparator: ",", groupSize: 3, digits: 2, "placeholder": "0.00" });
        $("#corte_add_montoP").inputmask("decimal", { allowMinus: false, radixPoint: ".", autoGroup: true, groupSeparator: ",", groupSize: 3, digits: 2, "placeholder": "0.00" });

        {% if acl.isAllowedUser('corte', 'search') %}
            $("body").on('click','.corte-btnConsulta',function(e){
                $("#corte_add_diff").hide();
                $("#corte_add_montoC").hide();
                $("#corte_add_diff2").show();
                $("#corte_add_montoC2").show();
                $('#corteInfo').modal('show');
                var $tr = $(this).closest("tr");
                var datos = $('#tblCorte').DataTable().row( $tr ).data();
                $("#corte-titulo-modal").text("Consulta de Corte");
                $("#idcorte").val(datos[2]);
                $("#corte_add_fIni").val(datos[7]);
                $("#corte_add_fFin").val(datos[8]);
                $('#corte_add_cCobratario').val(datos[4]);
                var select = $("#corte_add_cCobratario").selectize();
                var selectize = select[0].selectize;
                selectize.setValue(datos[4], false);
                selectize.disable();
                if(datos[15] == true)
                    $('#corte_add_cTipo').val("3");
                else
                    $('#corte_add_cTipo').val(datos[12]);
                /*$('#corte_add_cTipo [value="'+(datos[12]==true?2:1)+'"]').prop('checked', true);*/
                
                $("#corte_add_montoE").val((parseFloat(datos[6])).toFixed(2));

                $("#corte_add_montoS").val((parseFloat(datos[16])).toFixed(2));
                $("#corte_add_montoP").val((parseFloat(datos[17])).toFixed(2));

                $("#corte_add_diff").val((parseFloat(datos[11])).toFixed(2));
                $("#corte_add_diff2").val((parseFloat(datos[11])).toFixed(2));
                $("#corte_add_montoC").val(datos[5]);
                $("#corte_add_montoC2").val(datos[5]);
                $("#lP").val(datos[13]);

                $('.corte-guarda-do').hide();

                deshabilitaForm();
                // $('#corteInfo').modal('show');
            });

            $("body").on('click','.corte-btnReporte',function(e){
                var $tr = $(this).closest("tr");
                var datos = $('#tblCorte').DataTable().row( $tr ).data();

                var req = new XMLHttpRequest();
                var params = "id="+datos[2];
                req.open("POST", "/corte/pdf", true);
                req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                req.responseType = "blob";
                var _this = this;
                $(this).html('<span class="fa fa-spin fa-spinner" aria-hidden="true"></span>');
                $(this).prop("disabled", true);
                req.onload = function (event) {
                    $(_this).html('<span class="fa fa-file-text-o" aria-hidden="true"></span>');
                    $(_this).prop("disabled", false);
                    var blob = req.response;
                    var link=document.createElement('a');
                    link.href=window.URL.createObjectURL(blob);
                    link.target = '_blank';
                    /*link.download="reporteCorte.pdf";*/
                    link.click();
                };
                req.send(params);
            });
            
        {% endif %}

        
        {% if acl.isAllowedUser('corte', 'edit') %}
            /*$("body").on('click','.corte-btnEditar',function(e){
                var $tr = $(this).closest("tr");
                var datos = $('#tblCorte').DataTable().row( $tr ).data();

                $("#corte-titulo-modal").text("Modificar Corte");
                $("#idcorte").val(datos[2]);
                $("#corte_add_fIni").val(datos[7]);
                $("#corte_add_fFin").val(datos[8]);
                $('#corte_add_cCobratario').val(datos[4]);
                
                if(datos[15] == true)
                    $('#corte_add_cTipo').val("3");
                else
                    $('#corte_add_cTipo').val(datos[12]);
                
                $("#corte_add_montoE").val((parseFloat(datos[5])).toFixed(2));
                $("#corte_add_diff").val((parseFloat(datos[11])).toFixed(2));
                $("#corte_add_montoC").val((parseFloat(datos[5])-parseFloat(datos[11])).toFixed(2));
                $("#corte_add_montoC_h").val((parseFloat(datos[5])-parseFloat(datos[11])).toFixed(2));
                

                $("#lP").val(datos[13]);

                $('.corte-guarda-do').show();

                habilitaForm();
                $('#corteInfo').modal('show');
            });*/

            $("body").on('click','.corte-btnPagos',function(e){
                var $tr = $(this).closest("tr");
                var datos = $('#tblCorte').DataTable().row( $tr ).data();

                $('#idBusquedaPagos').val(datos[2]);
                dataTablePagos.ajax.reload();
                setTimeout(function () {
                    dataTablePagos.columns.adjust();
                }, 500);

                $('#cortePagos').modal('show');
            });
        {% endif %}

        {% if acl.isAllowedUser('corte', 'cancelar') %}
            $("body").on('click','.corte-btnCancelar',function(e){
                var $tr = $(this).closest("tr");
                var datos = $('#tblCorte').DataTable().row( $tr ).data();
                $('#corte-cE').val(datos[2]);
                $('#corteElimina').modal('show');
            });

            $("#btnEliminar").on('click',function(e){
                $.ajax({
                    url: "/corte/delete/" + $('#corte-cE').val(),
                    type: 'POST',
                    success: function(data){
                        var json = JSON.parse(data);
                        if(json.lError == false){
                            dataTable.ajax.reload();
                            toastr.success('','Corte cancelado con éxito!', {timeOut: 5000});
                            $("#corteElimina").modal("hide");
                        }else{
                            toastr.error("No fue posible cancelar el corte", json.cMensaje);
                        }
                    },
                    error: function(err){
                        toastr.error("No fue posible cancelar el corte", err);
                        
                    }
                });
                $("#corteElimina").modal("hide");
                $('#corte-cE').val("");
            });
        {% endif %}
    });

    /**
     * [loadClienteInfo realiza la consulta de cliente por clave]
     */
    var loadClienteInfo = function($indexCliente, mode) {
        if($indexCliente == null){
            return;
        }

        $.ajax({
            url: "/clientes/get/" + $indexCliente,
            type: 'GET',
            success: function(data){
                $data = jQuery.parseJSON(data);

                setModeClienteInfo(mode);
                // clearUserInfo();
                setClienteInfo($indexCliente, $data);

            },
            error: function(){
            }
        });
    };

    $('#cliente-info-modal').on('hidden.bs.modal', function () {
        toggleFinish(true);
        setModeClienteInfo('new');
    });

    function formatValue(value) {
        var valueWithFormat = value;
        if(value == null || value == undefined) {
            valueWithFormat = "";
        } else {
            valueWithFormat = value.trim().toUpperCase()
        }
        return valueWithFormat;
    }
</script>

