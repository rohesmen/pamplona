<style>
    .clientes-filter-clear-do {
        position: absolute;
        right: 15px;
        z-index: 10;
        top: 25px;
        height: 33px;
        width: 30px;
        text-align: center;
        cursor: pointer;
    }
    #modal-addfolioinventario .popover,
    #modal-disponibles .popover {
        z-index: 10000;
    }
</style>
<!-- View Clientes -->
{{ content() }}

<link href="../plugins/datepicker/bootstrap-datepicker.css" rel="stylesheet">
<link href="css/pamplona/folios.css" rel="stylesheet">

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Folios</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    {% include "folios/filter.volt" %}
                    <div style="margin-top: 15px;">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="container-montos">
                                    <div class="montos-numero" id="monto-transito-label">0</div>
                                    <div class="montos-descripcion">Transito</div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="container-montos">
                                    <div class="montos-numero" id="monto-monto-porliquidar-label">$0.00</div>
                                    <div class="montos-descripcion">Monto por liquidar</div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="container-montos">
                                    <div class="montos-numero" id="monto-porliquidar-label">0</div>
                                    <div class="montos-descripcion">Folios por liquidar</div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="container-montos">
                                    <div class="montos-numero" id="monto-disponibles-label">0</div>
                                    <div class="montos-descripcion">Folios Disponibles</div>
                                </div>
                            </div>
                        </div>
                        <table class="display table-striped table-hover" id="folios-datatable" style="width: 100%;">
                            <thead>
                            <tr>
                                <th data-priority="1"></th>
                                <th data-priority="1"></th>
                                <th>Vigente</th>
                                <th>Clave</th>
                                <th data-priority="4">Num ini</th>
                                <th class="no-wrap">Num fin</th>
                                <th class="no-wrap">Cobratario</th>
                                <th data-priority="5">Por liquidar</th>
                                <th data-priority="6">$ Por liquidar</th>
                                <th>Disponible</th>
                                <th>Transito</th>
                                <th>Fec. asig</th>
                                <th>Asignó</th>
                                <th>idcobratario</th>
                                <th>activo</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- view content -->

<div class="modal fade" id="modal-addfolio" tabindex="-1" role="dialog" aria-labelledby="myModalrs">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="titleFolio">Asignar folio</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id">
                <input type="hidden" id="maxfoliodisp">
                <div class="container-fluid">
                    <!-- add folio -->
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label for="cobratario">Cobratario*</label>
                            <select class="form-control required" id="cobratario" name="cobratario" style="width: 100%;">
                                <option value="">Todos</option>
                                {% for cobratario in cobratarios %}
                                    <option value="{{ cobratario.id }}">{{ cobratario.usuario }}</option>
                                {% endfor %}
                            </select>
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Folio inicial</label>
                            <input id="iniFolioAsig" name="iniFolioAsig" disabled type="text" placeholder="Solo numeros" class="form-control">
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Num. folios*</label>
                            <input id="cantidadfolios" name="cantidadfolios" type="text" placeholder="Solo numeros" class="form-control required">
                        </div>
                    </div>
                    <!-- ./add folio -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button id="btnGuardarFolio" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-addfolioinventario" tabindex="-1" role="dialog" aria-labelledby="myModalrs">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="titleFolio">Inventario de folios</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <!-- add folio -->
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label>Num. inicial*</label>
                            <input id="num_ini_inventario" name="num_ini_inventario" value="1" type="text" placeholder="Solo numeros" disabled class="form-control required">
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Num. final *</label>
                            <input id="num_fin_inventario" name="num_fin_inventario" type="text" placeholder="Solo numeros" class="form-control required">
                        </div>
                        <div class="col-md-4 form-group">
                            <div>&nbsp;</div>
                            <button id="btnGuardarFolioInv" type="button" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Observaciones </label>
                            <textarea class="form-control" id="txtObservInven" style="resize: none" rows="3"></textarea>
                        </div>
                    </div>
                    <!-- ./add folio -->
                </div>

                <div class="container-fluid">
                    <div class="row">
                        <table class="display table-striped table-hover" id="foliosinv-datatable" style="width: 100%;">
                            <thead>
                            <tr>
                                <th data-priority="1"></th>
                                <th data-priority="1"></th>
                                <th>Vigente</th>
                                <th>Clave</th>
                                <th data-priority="4">Num ini</th>
                                <th class="no-wrap">Num fin</th>
                                <th class="no-wrap">Usados</th>
                                <th class="no-wrap">Disponibles</th>
                                <th class="no-wrap">Total</th>
                                <th class="no-wrap">Foliador</th>
                                <th class="no-wrap">Observaciones</th>
                                <th class="no-wrap">Fecha</th>
                                <th class="no-wrap">Usuario</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-addFoliosUsados" tabindex="-1" role="dialog" aria-labelledby="myModalrs">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="titleFolio">Folios usados</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <h3>Antes de guardar verificar el folio</h3>
                    <!-- add folio -->
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Folio *</label>
                            <input id="foliousado" name="foliousado" type="text" class="form-control">
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Observaciones *</label>
                            <textarea class="form-control" id="observaciones_foliousado" style="resize: none;" rows="4"></textarea>
                        </div>
                    </div>
                    <!-- ./add folio -->
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnSaveFoliosUsados">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnCancelFoliosUsados">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-xliquidar" tabindex="-1" role="dialog" aria-labelledby="myModalrs">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="titleFolio">Folios por liquidar</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <!-- add folio -->
                    <div class="row" id="container-folios-xliquidar">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-disponibles" tabindex="-1" role="dialog" aria-labelledby="myModalrs">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="titleFolio">Folios disponibles</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <button class="btn btn-danger btn-sm" title="¿Cancelar folios?" id="btnCancelFolios">
                            <i class="fa fa-remove"></i> Cancelar
                        </button>
                        <button class="btn btn-primary btn-sm" id="btnTransferir" title="¿Transferir folios?">
                            <i class="fa fa-exchange"></i> Transferir
                        </button>
                    </div>
                </div>

                <div class="row" id="container-folios-disponibles">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-transferira" tabindex="-1" role="dialog" aria-labelledby="myModalrs">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="titleFolio">Transferir a</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="tipotransferencia">Tipo</label>
                            <select class="form-control" id="tipotransferencia" style="width: 100%;">
                                <option value="TES">Tesoreria</option>
                                <option value="COB">Cobratario</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-8" style="display: none;" id="container-cob-trans">
                        <div class="form-group">
                            <label for="tcobratario">Cobratario</label>
                            <select class="form-control" id="tcobratario" style="width: 100%;">
                                <option value="">Todos</option>
                                {% for cobratario in cobratarios %}
                                    <option value="{{ cobratario.id }}">{{ cobratario.usuario }}</option>
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="btnGuardarTransferir" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-confirmation-cancel" tabindex="-1" role="dialog" aria-labelledby="myModalrs">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="titleFolio">¿Desea cancelar los folios?</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <!-- add folio -->
                    <div class="row">
                        <div class="col-md-12">
                            <label>Motivo</label>
                            <textarea class="form-control" id="observacionesCancel"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <button id="btnGuardarCancelacion" type="button" class="btn btn-primary">Si</button>
            </div>
        </div>
    </div>
</div>

<script>
    var isInfo = '{{ acl.isAllowedUser('folios', 'info') }}';
    var isEdit = '{{ acl.isAllowedUser('folios', 'edit') }}';
    var isDelete = '{{ acl.isAllowedUser('folios', 'delete') }}';

    var buttons = "";
    {% if
        acl.isAllowedUser('folios', 'edit')
        or  acl.isAllowedUser('folios', 'info')
        or acl.isAllowedUser('folios', 'delete')
    %}
    {% if acl.isAllowedUser('folios', 'info') %}
    buttons += '<button class="btn btn-primary btn-sm clientes-info" type="button" title="Consultar">' +
        '<i class="fa fa-info"></i>' +
        '</button> ';
    {% endif %}

    {% if acl.isAllowedUser('folios', 'edit') %}
    buttons += '<button class="btn btn-primary btn-sm clientes-edit" type="button" title="Modificar">' +
        '<i class="fa fa-pencil"></i>' +
        '</button> ';
    {% endif %}

    {% if acl.isAllowedUser('folios', 'delete') %}
    buttons += '<button class="btn btn-danger btn-sm clientes-delete" type="button" title="¿Desea eliminar?">' +
        '<i class="fa fa-remove"></i>' +
        '</button> ';
    {% endif %}
    {% endif %}

    buttons += '<button class="btn btn-primary btn-sm clientes-info" type="button" title="Consultar">' +
        '<i class="fa fa-info"></i>' +
        '</button> ';
    buttons += '<button class="btn btn-primary btn-sm clientes-edit" type="button" title="Modificar">' +
        '<i class="fa fa-pencil"></i>' +
        '</button> ';
    buttons += '<button class="btn btn-danger btn-sm clientes-delete" type="button" title="¿Desea eliminar?">' +
        '<i class="fa fa-remove"></i>' +
        '</button> ';
</script>
{{ javascript_include("js/folios-events.js") }}
{{ javascript_include("plugins/datepicker/bootstrap-datepicker.js") }}
{{ javascript_include("plugins/datepicker/locales/bootstrap-datepicker.es.js") }}
