<div>
    <nav class="navbar navbar-default" >
        <div>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header" style="float: none; display: none;">
                <button type="button" id="colapse-busqueda" class="navbar-toggle" data-toggle="collapse"
                        data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse in clientes-only" id="contenedor-filtros" aria-expanded="true" style="border-color: #FFFFFF;">
                {#<form id="frmPersonas" name="frmPersonas" class="navbar-form navbar-right" role="search" style="border: none;">#}
                <div class="nav-bar">
                    {#<div style="font-weight: bold; ;padding: 15px 5px 5px 10px;">Selecciona tipo de búsqueda por:</div>#}
                    <!-- filtros -->
                    <!-- vigente -->
                    <div class="col-xs-6 col-sm-2 col-md-2">
                        <div class="form-group">
                            <label>Vigente</label>
                            <select class="form-control" id="folios-vigente-val" style="width: 100%;">
                                <option value="">Todos</option>
                                <option value="true">Si</option>
                                <option value="false">No</option>
                            </select>
                        </div>
                    </div>
                    <!-- Nombre -->
                    <div class="col-xs-12 col-md-3">
                        <div class="form-group">
                            <label for="fcobratario">Cobratario</label>
                            <select class="form-control" id="fcobratario" style="width: 100%;">
                                <option value="">Todos</option>
                                {% for cobratario in cobratarios %}
                                    <option value="{{ cobratario.id }}">{{ cobratario.usuario }}</option>
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                    <!-- clave -->
                    <div class="col-xs-12 col-md-3" style="display: none;">
                        <div class="form-group">
                            <label>Serie</label>
                            <div class="clientes-filter-clear-do" title="Limpiar">
                                <i class="fa fa-close" style="margin-top: 10px;"></i>
                            </div>
                            <input type="text" class="form-control"  id="filtro-serie" style="text-transform: uppercase;">
                        </div>
                    </div>
                    <!-- boton search -->
                    <div class="col-xs-6 col-sm-2 col-md-1">
                        <label>&nbsp;</label>
                        <button class="btn btn-primary btn-block" id="clientes-search-do" style="margin-bottom: 10px;" type="button" title="Buscar">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                    <!-- boton nuevo -->
{#                    {% if acl.isAllowedUser('folios', 'add') %}#}
                    <div class="col-xs-6 col-sm-2 col-md-1" style="text-align: right;">
                        <label>&nbsp;</label>

                            <button class="btn btn-primary btn-block" type="button" title="Asignacion" style="display: inline-block; margin-bottom: 10px;"
                                    id="addFolio">
                                <i class="fa fa-plus"></i>
                            </button>
                    </div>
{#                    {% endif %}#}
{#                    {% if acl.isAllowedUser('folios', 'inventario') %}#}
                    <div class="col-xs-6 col-sm-2 col-md-2" style="text-align: right;">
                        <label>&nbsp;</label>

                        <button class="btn btn-primary btn-block" type="button" title="Inventario" style="display: inline-block; margin-bottom: 10px;"
                                id="addFolioInventario">
                            Inventario
                        </button>
                    </div>
                    <div class="col-xs-6 col-sm-2 col-md-2" style="text-align: right;">
                        <label>&nbsp;</label>
                        {% if acl.isAllowedUser('folios', 'used') %}
                        <button class="btn btn-primary btn-block" type="button" title="Folios usados" style="display: inline-block; margin-bottom: 10px;"
                                id="addFoliosUsados">
                            Folios Usados
                        </button>
                        {% endif %}
                    </div>
                    {#                        {% endif %}#}
                </div>
                {#</form>#}

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->

    </nav>
</div>