<style>
    .filtro-recolectoresseguimiento-clear-do {
        position: absolute;
        right: 15px;
        z-index: 10;
        top: 25px;
        height: 33px;
        width: 30px;
        text-align: center;
        cursor: pointer;
    }
    .recolectoresseguimiento-modal-header {
        padding: 15px !important;
    }
    #recolectoresseguimiento-info-modal .popover {
        z-index: 10000;
    }
    .disnone{
        display: none;
    }
    .divB {
        border-radius: 20px 20px 20px 20px;
        box-shadow: 0px 0px 6px 0px #777777;
        border: 1px  solid #fff;
        height: 270px;
        display: flex;
        text-align: center;
        vertical-align: middle;
        flex-flow: column;
        overflow: hidden;
    }
    .divBtn {
        padding: 2px;
        text-align: right;
        height: 25px;
    }
    .divBtn .btn {
        margin-left: 1px;
        margin-right: 1px;
    }
    .imgDiv {
        height: 225px;
        display: flex;
    }
    .imgDiv img {
        margin: auto;
        border-radius: 20px 20px 20px 20px;
        border: 1px  solid #fff;
        max-height: 95%;
        vertical-align: middle;
        transition: 1s;
    }
    .imgDiv:hover img {
        transform: scale(1.02);
    }
    #btneditimg {
        background-color: transparent;
    }

</style>
<!-- View Clientes -->
{{ content() }}

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>RECOLECTORES</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- filtros -->
                    <div>
                        <nav class="navbar navbar-default" >
                            <div>
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header" style="float: none; display: none;">
                                    <button type="button" id="colapse-busqueda" class="navbar-toggle" data-toggle="collapse"
                                            data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
                                </div>
                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="navbar-collapse collapse in clientes-only" id="contenedor-filtros" aria-expanded="true" style="border-color: #FFFFFF;">
                                    <div class="nav-bar">
                                        <!-- activo -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Activo</label>
                                                <select class="form-control" id="fvigente" style="width: 100%;">
                                                    <option value="">Todos</option>
                                                    <option value="true">Si</option>
                                                    <option value="false">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- nombre -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <div class="filtro-recolectoresseguimiento-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="text" class="form-control Filt" id="fnombre" placeholder="Nombre del recolector">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <div class="filtro-recolectoresseguimiento-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="text" class="form-control Filt" id="fapepat" placeholder="Ape. paterno del recolector">
                                            </div>
                                        </div>
                                        <div class="{{ acl.isAllowedUser('recolectoresseguimiento', 'create') ?  'col-md-2' : 'col-md-3' }}">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <div class="filtro-recolectoresseguimiento-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="text" class="form-control Filt" id="fapemat" placeholder="Ape. materno del recolector">
                                            </div>
                                        </div>
                                        <!-- boton search -->
                                        <div class="col-md-1">
                                            <label>&nbsp;</label>
                                            <button class="btn btn-primary btn-block" id="recolectoresseguimiento-search-do" style="margin-bottom: 10px;" type="button" title="Buscar">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                        {% if acl.isAllowedUser('recolectoresseguimiento', 'create') %}
                                            <!-- boton nuevo -->
                                            <div class="col-md-1">
                                                <div class="" style="text-align: right;">
                                                    <label>&nbsp;</label>
                                                    <button class="btn btn-primary btn-block" title="Crear" style="display: inline-block; margin-bottom: 10px;" id="addrecolectoresseguimientoDo">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        {% endif %}
                                    </div>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->
                        </nav>
                    </div>
                    <div style="margin-top: 15px;">
                        <table class="display table-striped table-hover" id="recolectoresseguimiento-datatable" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th data-priority="2"></th>
                                    <th data-priority="1"></th>
                                    <th data-priority="6">Activo</th>
                                    <th data-priority="7">ID</th>
                                    <th data-priority="3">Nombre(s)</th>
                                    <th data-priority="4">Ape. Paterno</th>
                                    <th data-priority="5">Ape. Materno</th>
                                    <th data-priority="8">Fecha Creación</th>
                                    <th data-priority="9">Fecha Modificacion</th>
                                    <th>activo</th>
                                    <th>foto</th>
                                    <th>ruta_foto</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal recolector seguimiento -->
<div class="modal inmodal fade" id="recolectoresseguimiento-info-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header recolectoresseguimiento-modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Nuevo Recolector</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="divB">
                                        <div class="divBtn">
                                            <button id="btneditimg" class="btn btn-sm" title="Editar Foto">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                        </div>
                                        <a id="imagenrecolectorhref" class="imgDiv" href="/img/usuario.png" target="_blank">
                                            <img id="imagenrecolector" class="img-responsive" src="/img/usuario.png">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="row disnone">
                                <div class="col-md-12">
                                    <div class="form-group input_container">
                                        <label for="fileUploadFoto">Foto</label>
                                        <input type="file" class="form-control" accept="image/*" id="fileUploadFoto">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8" style="margin-top: 10px;">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label>Nombre(s) *</label>
                                    <input id="txtNombre" type="text" placeholder="Nombre(s) *" class="form-control rs-required txtDis">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Ape. Paterno *</label>
                                    <input id="txtApepat" type="text" placeholder="Ape. Paterno *" class="form-control rs-required txtDis">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Ape. Materno</label>
                                    <input id="txtApemat" type="text" placeholder="Ape. Materno" class="form-control rs-required txtDis">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnGuardar" type="button" class="btn btn-primary" title="Guardar">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- view content -->
<script>
    coacciones = '{{ coacciones }}';
</script>
{{ javascript_include("js/recolectoresseguimiento/index-events.js") }}