{{ content() }}

<link href="css/pamplona/clientes.css" rel="stylesheet">
<link href="css/pamplona/tamboreo.css" rel="stylesheet">

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>TAMBOREO</h2>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="container-fluid" style="border: 1px solid #ddd;">
                        <nav class="navbar navbar-default" style="border-color: #000000; margin-bottom: 0px;">

                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header" style="float: none; display: block;">
                                <button type="button" class="navbar-toggle" data-toggle="collapse"
                                        data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
                            </div>
                            <div class="navbar-collapse collapse in" id="contenedor-filtros" aria-expanded="true">

                                <div class="row">
                                    <div style="font-weight: bold; ;padding: 15px 5px 5px 10px;">Selecciona tipo de búsqueda por:</div>
                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                        <div class="form-group" style="margin-bottom: 5px;">
                                            <select class="form-control" id="tamboreos-filter-select">
                                                <option value="">Todos</option>
                                                <option value="cla">Clave</option>
                                                <option value="nom">Nombre</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-9 "  id="tamboreos-filtro-todos">
                                        <div class="form-group">
                                            <div class="input-group custom-search-form" >
                                                <div class="input-group-btn">
                                                    <button class="btn btn-primary tamboreos-search-do hidden-xs " type="button" title="Buscar">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                    <button class="btn btn-primary  btn-block tamboreos-search-do hidden-sm hidden-md hidden-lg" type="button" title="Buscar">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div><!-- /input-group -->
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-9 oculto"  id="tamboreos-filtro-clave">
                                        <div class="form-group">
                                            <div class="input-group custom-search-form" >
                                                <div class="tamboreo-filter-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="number" min="1" class="form-control tamboreos-search-val" placeholder="Números" id="tamboreos-clave-val" data-inputmask="'mask': '9', 'repeat': 10, 'greedy' : false">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-primary tamboreos-search-do" type="button" title="Buscar">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div><!-- /input-group -->
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-9 oculto"  id="tamboreos-filtro-nombre">
                                        <div class="form-group">
                                            <div class="input-group custom-search-form" >
                                                <div class="tamboreo-filter-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="text" class="form-control tamboreos-search-val" placeholder="Escriba el valor a buscar" id="tamboreos-nombre-val">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-primary tamboreos-search-do" type="button" title="Buscar">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div><!-- /input-group -->
                                        </div>
                                    </div>




                                    <div class="col-xs-12 col-sm-2 col-md-1 text-right" >
                                        {% if acl.isAllowedUser('tamboreo', 'add') %}
                                        <button class="btn btn-primary btn-block" type="button" title="Nuevo Tamboreo" style="display: inline-block; margin-bottom: 10px;"
                                                id="openNewTamboreo">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                        {% endif %}
                                    </div>

                                </div>

                            </div>

                        </nav>


                    </div>

                    <div style="margin-top: 15px;">
                        <table class="display table-striped table-hover" id="tamboreos-datatable" style="width: 100%; height: 100%">
                            <thead>
                            <tr>
                                <th data-priority="0"></th>
                                <th data-priority="1"></th>
                                <th>Vigente</th>
                                <th>Clave</th>
                                <th data-priority="2">Nombre</th>
                                <th>Descripción</th>
                                <th>Fecha Creción</th>

                            </tr>
                            </thead>
                        </table>

                    </div>

                </div>
            </div>
        </div>

    </div>


    <div class="row" style="margin-top: 10px;" id="tamboreo-container"></div>

</div>


<!-- Modal -->
<div class="modal fade" id="tamboreo-info-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title modal-titleAux" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">

                    <form id="frmTamboreo">
                        <div class="row">
                            <div class="col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label for="clave">Clave</label>
                                    <input type="text" class="form-control" id="clave" name="clave" placeholder="Clave" disabled>
                                </div>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="activo">Activo</label>
                                    <div class="checkbox" >
                                        <label>
                                            <input type="checkbox" id="activo" name="activo" disabled>
                                        </label>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="titulo">* Nombre</label>
                                    <select class="form-control" id="titulo" name="titulo" style="width: 100%;" required>
                                        <option value="">Sel. Opción</option>
                                        <option value="Tambor">Tambor</option>
                                        <option value="Bolsa">Bolsa</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="titulo">* Monto</label>
                                    <input type="number" min="0" class="form-control" id="monto" name="monto" placeholder="">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="descripcion">* Descripción</label>
                                    <textarea  id="descripcion" name="descripcion" class="form-control" rows="3" required></textarea>

                                </div>
                            </div>

                        </div>

                            <button id="btnGuardaForm" type="submit" class="btn btn-default fade" style="display: none">Submit</button>
                    </form>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="btnGuardar" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript" src="../js/tamboreo-events.js"></script>

<script>
    {% if
        acl.isAllowedUser('tamboreo', 'info')
    %}
        {% if acl.isAllowedUser('tamboreo', 'info') %}
        buttons += '<button class="btn btn-primary btn-sm tamboreo-info" type="button" title="Consultar">' +
            '<i class="fa fa-info"></i>' +
            '</button> ';
        {% endif %}


    {% endif %}
    $(document).ready(function() {
        tamboreosDT = $('#tamboreos-datatable').DataTable({
            "scrollX": true,
            "searching": false,
            "select": true,
            "dom": '<"clear"><rt><"bottom"lip><"clear">',
            "autoWidth": true,
            "paging": true,
            "info": true,
            "processing": true,
            "scrollCollapse": false,
            "pagingType": "full",
            "language": {
                "paginate": {
                    "next": ">",
                    "first": "<<",
                    "last": ">>",
                    "previous": "<"
                },
                "search": "",
                "searchPlaceholder": "Buscar en los resultados encontrados",
                "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
                "infoEmpty": "",
                "infoFiltered": " - filtrado de _MAX_",
                "emptyTable": "Sin resultados",
                "sZeroRecords": "Sin resultados",
                processing: "Procesando ...",
                "lengthMenu": "Mostrar _MENU_ registros"
            },
            responsive: {
                details: {
                    type: 'column',
                    target: 1
                }
            },
            "columnDefs": [
                {
                    targets: [0],
                    orderable: false,
                    searchable: false,
                    defaultContent: buttons,
                    className: 'dt-center no-wrap',
                    {% if
                        not acl.isAllowedUser('tamboreo', 'info')
                    %}
                        visible: false
                    {% endif %}
                },
                { targets: [1], orderable: false, className: 'control', searchable: false},
                { targets: [2], searchable: true, className: 'dt-center'},
                { targets: [3], className: 'dt-center'},

                { targets: '_all', visible: true }
            ],
            "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
            "pageLength": 10,
            "order": [[ 3, 'asc' ]],
            "processing": true,
            "deferLoading": 0,
            "createdRow" : function( row, data, index ) {

                // Add identity if it specified
                if(!data.hasOwnProperty("id") ) {
                    row.id = "tamboreo-" + data[3]            }
            },
            "fnDrawCallback": function( settings ) {
                if(tamboreosDT)
                    tamboreosDT.columns.adjust();
            }


        });

        $(window).on("resize", function(){
            tamboreosDT.columns.adjust();
        });



        {% if acl.isAllowedUser('tamboreo', 'info') %}
        $("#tamboreos-datatable").off( 'click', '.tamboreo-info');
        $("#tamboreos-datatable").on( 'click', '.tamboreo-info', function () {
            var $tr = $(this).closest("tr");
            var idAttr = $tr.attr("id").split("-");
            var id = idAttr[1];
            if ( $tr.hasClass('selected') ) {
                //$tr.removeClass('selected');
            }
            else {
                tamboreosDT.$('tr.selected').removeClass('selected');
                $tr.addClass('selected');
            }

            $("#clave").val(id);
            loadTamboreoInfo(id, "info");
            $("#tamboreo-info-modal").modal({
                show: true,
                backdrop: "static"
            });
            /* poner aqui la seccion para la consulta */
        });
        {% endif %}

        {% if acl.isAllowedUser('tamboreo', 'edit') %}
        $("#tamboreos-datatable").off( 'click', '.tamboreo-edit');
        $("#tamboreos-datatable").on( 'click', '.tamboreo-edit', function () {
            var $tr = $(this).closest("tr");
            var idAttr = $tr.attr("id").split("-");
            var id = idAttr[1];
            if ( $tr.hasClass('selected') ) {
                //$tr.removeClass('selected');
            }
            else {
                tamboreosDT.$('tr.selected').removeClass('selected');
                $tr.addClass('selected');
            }


            /* poner aqui la seccion para la ediction */
            loadTamboreoInfo(id, "edit");
            $("#tamboreo-info-modal").modal({
                show: true,
                backdrop: "static"
            });
        });
        {% endif %}

        {% if acl.isAllowedUser('tamboreo', 'delete') %}
        $("#tamboreos-datatable").off( 'click', '.tamboreo-delete');
        $("#tamboreos-datatable").on( 'click', '.tamboreo-delete', function () {
            $(".popover.confirmation").confirmation("destroy");
            var $tr = $(this).closest("tr");
            var idAttr = $tr.attr("id").split("-");
            var id = idAttr[1];
            $(this).confirmation({
                rootSelector: "body",
                container: "body",
                singleton: true,
                popout: true,
                btnOkLabel: "Si",
                onConfirm: function() {

                    if ( $tr.hasClass('selected') ) {
                        //$tr.removeClass('selected');
                    }
                    else {
                        tamboreosDT.$('tr.selected').removeClass('selected');
                        $tr.addClass('selected');
                    }
                    eliminarTamboreo($tr, id);
                },
                onCancel: function() {
                    $(this).confirmation('destroy');
                },
            });

            $(this).confirmation("show");
        });
        {% endif %}


        $('#tamboreos-datatable tbody').on( 'click', 'tr', function () {

            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');

            }
            else {
                tamboreosDT.$('tr.selected').removeClass('selected');
                $(this).toggleClass('selected');

            }
        });
    });
</script>