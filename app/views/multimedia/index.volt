<!-- View Clientes -->
{{ content() }}

<link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
<link href="../plugins/datepicker/bootstrap-datepicker.css" rel="stylesheet">
<link href="css/pamplona/multimedia.css" rel="stylesheet">
<link href="plugins/dropzone/dropzone.min.css" rel="stylesheet">

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Multimedia</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    {% include "multimedia/filter.volt" %}
                    <div style="margin-top: 15px;">
                        <table class="display table-striped table-hover" id="multimedia-datatable" style="width: 100%;">
                            <thead>
                            <tr>
                                <th data-priority="1"></th>
                                <th data-priority="2">Acciones</th>
                                <th>Vigente</th>
                                <th>Clave</th>
                                <th>Tipo</th>
                                <th >Nombre</th>
                                <th >Descripción</th>
                                <th >Fecha</th>
                                <th >Fecha Creación</th>
                                <th >Activo</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- view content -->

<!-- Modal -->
<div class="modal fade" id="addMultimediaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Agregar multimedia</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="ruta_hide" name="ruta_hide">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-2 col-md-4">
                            <div class="form-group">
                                <label for="clave">Clave</label>
                                <input type="text" class="form-control" id="clave" name="clave" placeholder="Clave" disabled>
                            </div>
                        </div>
                        <div class="col-xs-10 col-md-8">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="tipo">Tipo</label>
                                <select class="form-control" id="tipo" name="tipo">
                                    <option value="noticia">Noticia</option>
                                    <option value="galeria">Galeria</option>
                                    <option value="video">Video</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="fecha">Fecha</label>
                                <div id="container-fecha">
                                    <div class="input-group date">
                                        <input type="text" class="form-control" id="fecha" name="fecha" placeholder="dd/mm/yyyy" required>
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="descripcion">Descripción</label>
                                <textarea id="descripcion" placeholder="descripcion" class="form-control" rows="5" style="resize: none;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3" id="conatainer-dropzone">
                            <div class="form-group">
                                <div id="rutaUpload" class="dropzone" style="text-align: center;">
                                    <div class="dz-default dz-message">
                                        <div class="dz-icon">
                                            <i class="fa fa-cloud-upload fa-5x"></i>
                                        </div>
                                        <div>
                                            <span class="dz-text">Arrastrar archivo</span>
                                            <p class="text-sm text-muted">o seleccionar manualmente</p>
                                            {#<p class="text-sm text-muted">Permitido: xls, xlsx, csv</p>#}
                                            <p class="text-sm text-muted">Permitido: imagen, video</p>
                                        </div>
                                    </div>
                                    <div class="fallback">
                                        <input type="file" id="form-descripcion">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 " style="display: none;">
                            <div  class="text-center" id="containerIMG">
                                <img id="containerIMG" src="img/no-image.png" class="img-responsive" style="display: inline-block;">
                            </div>
                        </div>
                    </div>
                    <button id="btnGuardaForm"  type="button" class="btn btn-default fade">Guardar</button>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnCancelarModal" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button id="btnGuardarModal" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

{{ javascript_include("js/multimedia-events.js") }}
{{ javascript_include("plugins/datepicker/bootstrap-datepicker.js") }}
{{ javascript_include("plugins/datepicker/locales/bootstrap-datepicker.es.js") }}
{{ javascript_include("plugins/dropzone/dropzone.min.js") }}

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZJImFXCY1MX5n3_OVhXg7Jgb9zBbLlLg" async defer></script>
