<div style="border: 1px solid #ddd;">
    <nav class="navbar navbar-default" style="border-color: #000000; margin-bottom: 0px;">
        <div>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header" style="float: none; display: block;">
                <button type="button" id="colapse-busqueda" class="navbar-toggle" data-toggle="collapse"
                        data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse in clientes-only" id="contenedor-filtros" aria-expanded="true">
                {#<form id="frmPersonas" name="frmPersonas" class="navbar-form navbar-right" role="search" style="border: none;">#}
                <div class="nav-bar">
                    {#<div style="font-weight: bold; ;padding: 15px 5px 5px 10px;">Selecciona tipo de búsqueda por:</div>#}
                    <!-- filtros -->
                    <!-- vigente -->
                    <div class="col-xs-2 col-sm-2 col-md-4">
                        <div class="form-group">
                            <label>Vigente</label>
                            <select class="form-control" id="filtro-vigente" style="width: 100%;">
                                <option value="">Todos</option>
                                <option value="true">Si</option>
                                <option value="false">No</option>
                            </select>
                        </div>
                    </div>
                    <!-- Nombre -->
                    <div class="col-xs-10 col-sm-10 col-md-6" >
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <div class="filter-clear-do" title="Limpiar">
                                <i class="fa fa-close" style="margin-top: 10px;"></i>
                            </div>
                            <input type="text" class="form-control" placeholder="Escriba el nombre" id="filtro-nombre">
                        </div>
                    </div>
                    <!-- boton search -->
                    <div class="col-xs-6 col-sm-6 col-md-1">
                        <label>&nbsp;</label>
                        <button class="btn btn-primary btn-block clientes-search-do" style="margin-bottom: 10px;" type="button" title="Buscar">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                    <!-- boton nuevo -->
                    <div class="col-xs-6 col-sm-6 col-md-1" style="text-align: right;">
                        <label>&nbsp;</label>
                        {% if acl.isAllowedUser('multimedia', 'add') %}
                            <button class="btn btn-primary btn-block" type="button" title="Agregar multimedia" style="display: inline-block; margin-bottom: 10px;"
                                    id="addMultimediaDo">
                                <i class="fa fa-plus"></i>
                            </button>
                        {% endif %}
                    </div>
                </div>
                {#</form>#}

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->

    </nav>
</div>