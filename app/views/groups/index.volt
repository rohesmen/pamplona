{{ content() }}
<?php $this->flashSession->output() ?>

{{ stylesheet_link('plugins/datatables_new/Select-1.2.0/css/select.dataTables.min.css') }}


<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-md-6">
		<h2>Grupo de capas</h2>
	</div>
	<div class="col-md-6 text-right">
		{% if acl.isAllowedUser('groups', 'create') %}
			<a href="/groups/create" class="btn btn-primary btn-sm">
				<i class="fa fa-plus"></i>
				<span class="hidden-xs hidden-sm"> Nuevo</span>
			</a>
		{% endif %}

		{% if acl.isAllowedUser('groups', 'deactivate') %}
			<button class="btn btn-danger btn-sm titulo-acciones-pull-right hidden-md hidden-lg disable-onselect-usuario grupo-desactivar" disabled>
				<i class="fa fa-link"></i>
				<span class="hidden-xs hidden-sm">Desactivar</span>
			</button>

			<button class="btn btn-danger btn-sm titulo-acciones-pull-right hidden-xs hidden-sm disable-onselect-usuario grupo-desactivar" disabled style="margin-right: 6px;">
				<i class="fa fa-remove"></i>
				<span class="hidden-xs hidden-sm">Desactivar</span>
			</button>
		{% endif %}

		{% if acl.isAllowedUser('groups', 'export') %}
			<div class="btn-group titulo-acciones-pull-right">
				<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-right: 6px;">
					<i class="fa fa-download"></i>
					<span class="hidden-xs hidden-sm"> Exportar </span>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="/groups/excel"><i class="fa fa-file-excel-o"></i> EXCEL</a></li>
					<li><a href="/groups/pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
				</ul>
			</div>
		{% endif %}

	</div>
</div>

<div id="page-content">
	<div class="row">
		<div class="col-lg-3" style="display: none;">
			<div class="panel filtros">

				<div class="panel-heading">
					<div class="panel-control panel-control-component">
						<button class="btn btn-primary">Aplicar <i class="fa fa-angle-double-right"></i></button>
					</div>
					<h3 class="panel-title">Filtros</h3>
				</div>

				<div class="panel-body">
					<div class="panel">
						<div class="panel-heading" data-target="#panel-filtro-estatus" data-toggle="collapse">
							<div class="panel-control">
								<button class="btn btn-default" data-target="#panel-filtro-estatus" data-toggle="collapse" aria-expanded="true"><i
											class="demo-pli-arrow-down"></i></button>
							</div>
							<h3 class="panel-title">Estatus</h3>
						</div>

						<!--Panel body-->
						<div id="panel-filtro-estatus" class="collapse">
							<div class="panel-body">
								<div id="treeEstatus">
									<ul>
										<li data-jstree='{"opened":true, "selected":true, "icon":"glyphicon glyphicon-leaf"}'>Todos
											<ul>
												<li id="chkFiltroActivos">Activos</li>
												<li id="chkFiltroInactivos">Inactivos</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="panel">
				<div class="panel-heading">
					<div class="panel-control_ panel-control-component_">
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-6 col-md-6 col-lg-3">
									<h3 class="panel-title">Buscar:</h3>
								</div>
								<div class="col-xs-6 col-md-2 col-lg-3">
									<select class="form-control" id="filter-campo" style="margin-top: 10px">
										<option value="clave">Clave</option>
										<option value="nombre">Nombre</option>
									</select>
								</div>
								<div class="col-xs-12 col-md-4 col-lg-6">
									<div class="input-group" style="margin-top: 10px">
										<input type="text" placeholder="Buscar ..." class="form-control" style="height: inherit;" id="txtBuscar">
										<span class="input-group-btn">
											<button class="btn btn-default btn-remove-nem" type="button" id="btnClear"><i class="fa fa-remove"></i></button>
											<button class="btn btn-primary" type="button" id="btnBuscar"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="visible-xs-block visible-sm-block" style="height: 25px;"></div>
				<div class="panel-body" style="min-height: 400px;">
					<div id="contenedor-tabla" style="display: none;">
						<table id="dt-grupos" class="table table-responsive display table-bordered" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Acción</th>
								<th data-priority="0"></th>
								<th class="hidden-xs">Activo</th>
								<th data-priority="1">Clave</th>
								<th data-priority="3">Nombre</th>
								<th class="hidden-xs">Creado</th>
								<th class="hidden-xs">Modificado</th>
								<th class="hidden-xs">id</th>
								<th class="hidden-xs">activo</th>
							</tr>
						</thead>
						<tbody>
						{% for grupo in grupos %}
							<tr style="cursor:pointer;">
								<td>
									{# {% if acl.isAllowedUser('groups', 'edit') %}
										{{ link_to("groups/edit/" ~ grupo.id, '<i class="fa fa-cog"></i>', "class": "btn btn-primary btn-sm") }}
									{% endif %} #}

										{{ link_to("groups/info/" ~ grupo.id, '<i class="fa fa-info"></i>', "class": "btn btn-primary btn-sm btnInfoModal") }}
									{% if acl.isAllowedUser('groups', 'edit') %}
										<a href="/groups/edit/{{ grupo.id }}" class="btn btn-primary btn-sm">
											<i class="fa fa-pencil"></i>
										</a>
									{% endif %}									
								</td>
								<td></td>
								<td class="hidden-xs">
									{% if grupo.activo %}
										<i class="fa fa-check-circle" style="color: green;"></i>
									{% else %}
										<i class="fa fa-times-circle" style="color: red;"></i>
									{% endif %}
								</td>
								<td>{{ grupo.id }}</td>
								<td>
									{{ grupo.nombre }}
								</td>
								<td class="hidden-xs">{{ date("d/m/Y", strtotime(grupo.fecha_creacion)) }}</td>
								<td class="hidden-xs">{{ date("d/m/Y", strtotime(grupo.fecha_modificacion)) }}</td>
								<td class="hidden-xs">{{ grupo.id }}</td>
								<td class="hidden-xs">{{ grupo.activo ? 'true' : 'false' }}</td>
							</tr>
						{% endfor %}
						</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="info-grupo-modal" role="dialog" tabindex="-1" aria-labelledby="info-grupo-modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<!--Modal header-->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
				<h4 class="modal-title">Información del grupo</h4>
			</div>

			<!--Modal body-->
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4">
						<dl>
							  <dt>Clave</dt>
							  <dd id="ddId"></dd>
							  
							  <dt>Nombre</dt>
							  <dd id="ddNombre"></dd>

							  <dt>Descripci&oacute;n</dt>
							  <dd id="ddDescripcion"></dd>
							  
							  <dt>Orden</dt>
							  <dd id="ddOrden"></dd>
							  
							  <dt>Activo</dt>
							  <dd id="ddActivo"></dd>
							  
							  <dt>Fecha creaci&oacute;n</dt>
							  <dd id="ddFCreacion"></dd>
						</dl>
					</div>								
				</div>
			</div>

			<!--Modal footer-->
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cerrar</button>
				<a href="#" class="btn btn-primary" id="btnEditModal"{% if not acl.isAllowedUser('groups', 'edit') %} style="display: none;"{% endif %}>Modificar</a>
			</div>
		</div>
	</div>
</div>

{{ javascript_include("plugins/datatables_new/Select-1.2.0/js/dataTables.select.min.js") }}
{{ stylesheet_link('plugins/jstree-bootstrap/themes/proton/style.min.css') }}
{{ javascript_include("plugins/jstree-bootstrap/jstree.min.js") }}
{{ javascript_include("js/groups/events.js") }}