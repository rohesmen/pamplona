{{ content() }}
<style type="text/css">
.btn-primary:hover, .btn-primary:active, .btn-primary.active, .open>.dropdown-toggle.btn-primary {
    background-color: #004e8d;
}
.select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #e9e9e9;
    border-radius: 0px;
}

/*div.panel.panel-danger table tr td{
	color: red !important;
}*/
tr.fila-inactiva > td{
	color: red !important;	
}
</style>

<!--Switchery [ OPTIONAL ]-->
{{ stylesheet_link('plugins/switchery/switchery.min.css') }}
<!--Switchery [ OPTIONAL ]-->
{{ javascript_include("plugins/switchery/switchery.min.js") }}

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-md-6">
		<h2>Creacion de grupo de capas</h2>
	</div>
	<div class="col-md-6 text-right">
		{% if acl.isAllowedUser('groups', 'create') %}
			<button class="btn btn-primary btn-sm btnSaveGroup">
				<i class="fa fa-save"></i>
				<span class="hidden-xs hidden-sm">Guardar</span>
			</button>
		{% endif %}

		<a href="/groups" class="btn btn-warning btn-sm titulo-acciones-pull-right hidden-md hidden-lg grupo-cancelar">
			<i class="fa fa-remove"></i>
			<span class="hidden-xs hidden-sm">Cancelar</span>
		</a>
		<a href="/groups" class="btn btn-warning btn-sm titulo-acciones-pull-right hidden-xs hidden-sm grupo-cancelar" style="margin-right: 6px;">
			<i class="fa fa-remove"></i>
			<span class="hidden-xs hidden-sm"> Cancelar</span>
		</a>

	</div>
</div>

<div id="page-content">
	<div class="wrapper wrapper-content animated fadeInRight bg-white">
		<div class="tab-base">
		<!--Nav Tabs-->
		<ul class="nav nav-tabs" role="tablist">
			<li class="active" role="presentation">
				<a data-toggle="tab" href="#tab-nuevo" aria-controls="tab-nuevo" role="tab">Nuevo</a>
			</li>
		</ul>

		<!--Tabs Content-->
		<div class="tab-content">
			<div role="tabpanel" id="tab-nuevo" class="tab-pane fade active in">
				<form class="form-horizontal">
					<div class="container">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="chkActivo">Activo</label>
							<div class="col-sm-1">
								<input type="checkbox" id="chkActivo">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtClave">Clave</label>
							<div class="col-sm-3">
								{{ form.render("txtClave") }}
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtNombre">Nombre *</label>
							<div class="col-sm-3">
								{{ form.render("txtNombre") }}
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtDescripcion">Descripcion</label>
							<div class="col-sm-3">
								{{ form.render("txtDescripcion") }}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtOrden">Orden *</label>
							<div class="col-sm-3">
								{{ form.render("txtOrden") }}
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	</div>
</div>

<script type="application/javascript" src="../js/groups/create-events.js"></script>
<script type="application/javascript" src="../js/groups/create-functions.js"></script>
<link rel="stylesheet" href="../plugins/jstree-bootstrap/themes/proton/style.min.css" />
<script src="../plugins/jstree-bootstrap/jstree.min.js"></script>