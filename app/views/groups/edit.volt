<style type="text/css">
.btn-primary:hover, .btn-primary:active, .btn-primary.active, .open>.dropdown-toggle.btn-primary {
    background-color: #004e8d;
}
.select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #e9e9e9;
    border-radius: 0px;
}

/*div.panel.panel-danger table tr td{
	color: red !important;
}*/
tr.fila-inactiva > td{
	color: red !important;
}
</style>

{{ content() }}
<?php $this->flashSession->output() ?>

<!--Switchery [ OPTIONAL ]-->
<link href="/plugins/switchery/switchery.min.css" rel="stylesheet">
<!--Switchery [ OPTIONAL ]-->
<script src="/plugins/switchery/switchery.min.js"></script>

{# <!--Bootstrap Table [ OPTIONAL ]-->
<link href="/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
<!--Bootstrap Table [ OPTIONAL ]-->
<script src="/plugins/bootstrap-table/bootstrap-table.min.js"></script> #}

<div id="page-title">
	<div id="titulo-modulo"><h1 class="page-header text-overflow">{{ moduleTitle }}</h1></div>
	<div id="titulo-acciones">
		{% if acl.isAllowedUser('groups', 'create') %}
			<button class="btn btn-primary btn-sm btnEditGroup">
				<i class="fa fa-save"></i>
				<span class="hidden-xs hidden-sm">Guardar</span>
			</button>
		{% endif %}

		<a href="/groups" class="btn btn-warning btn-sm titulo-acciones-pull-right hidden-md hidden-lg gruupo-cancelar">
			<i class="fa fa-remove"></i>
			<span class="hidden-xs hidden-sm">Cancelar</span>
		</a>
		<a href="/groups" class="btn btn-warning btn-sm titulo-acciones-pull-right hidden-xs hidden-sm gruupo-cancelar" style="margin-right: 6px;">
			<i class="fa fa-remove"></i>
			<span class="hidden-xs hidden-sm">Cancelar</span>
		</a>
	</div>
</div>

<div id="page-content">
	<div class="tab-base">
		<!--Nav Tabs-->
		<ul class="nav nav-tabs" role="tablist">
			<li class="active" role="presentation">
				<a data-toggle="tab" href="#tab-nuevo" aria-controls="tab-nuevo" role="tab">Nuevo</a>
			</li>
		</ul>

		<!--Tabs Content-->
		<div class="tab-content">
			<div role="tabpanel" id="tab-nuevo" class="tab-pane fade active in">
				<form class="form-horizontal">
					<div class="container">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="chkActivo">Activo</label>
							<div class="col-sm-1">
								<input type="checkbox" id="chkActivo" {% if infogroup.activo %} checked="checked" {% endif %}>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtClave">Clave</label>
							<div class="col-sm-3">
								<input type="text" id="txtClave" name="txtClave" class="form-control" disabled="disabled" value="{{ infogroup.id }}">
							</div>

						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtNombre">Nombre *</label>
							<div class="col-sm-3">
								<input type="text" id="txtNombre" name="txtNombre" class="form-control save-required" placeholder="Nombre" value="{{ infogroup.nombre }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtDescripcion">Descripcion</label>
							<div class="col-sm-3">
								<input type="text" id="txtDescripcion" name="txtDescripcion" class="form-control" placeholder="Descripcion" value="{{ infogroup.descripcion }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="txtOrden">Orden *</label>
							<div class="col-sm-3">
								<input type="text" id="txtOrden" name="txtOrden" class="form-control save-required" placeholder="Orden" value="{{ infogroup.orden }}">
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>

<script type="application/javascript" src="/js/groups/edit-events.js"></script>
<script type="application/javascript" src="/js/groups/edit-functions.js"></script>
<link rel="stylesheet" href="/plugins/jstree-bootstrap/themes/proton/style.min.css" />
<script src="/plugins/jstree-bootstrap/jstree.min.js"></script>