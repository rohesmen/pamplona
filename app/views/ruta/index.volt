{{ content() }}

<link href="css/pamplona/clientes.css" rel="stylesheet">
<link href="css/pamplona/ruta.css" rel="stylesheet">
<script type="application/javascript" src="../js/ruta-events.js"></script>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>RUTA</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="container-fluid" style="border: 1px solid #ddd;">
                        <nav class="navbar navbar-default" style="border-color: #000000; margin-bottom: 0px;">

                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header" style="float: none; display: block;">
                                <button type="button" class="navbar-toggle" data-toggle="collapse"
                                        data-target="#contenedor-filtros" aria-expanded="true" style="cursor: pointer;">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a id="divPanelDerecho" role="presentation" class="active hidden navbar-brand" href="#" data-toggle="modal" data-target="#modalRegParticip"></a>
                            </div>
                            <div class="navbar-collapse collapse in" id="contenedor-filtros" aria-expanded="true">

                                <div class="row">
                                    <div style="font-weight: bold; ;padding: 15px 5px 5px 10px;">Selecciona tipo de búsqueda por:</div>
                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                        <div class="form-group" style="margin-bottom: 5px;">
                                            <select class="form-control" id="rutas-filter-select">
                                                <option value="">Todos</option>
                                                <option value="cla">Clave</option>
                                                <option value="nom">Ruta</option>
                                                <option value="col">Colonia</option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-9 "  id="rutas-filtro-todos">
                                        <div class="form-group">
                                            <div class="input-group custom-search-form" >
                                                <div class="input-group-btn">
                                                    <button class="btn btn-primary clientes-search-do hidden-xs " type="button" title="Buscar">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                    <button class="btn btn-primary btn-block clientes-search-do hidden-sm hidden-md hidden-lg" style="margin-bottom: 10px;"
                                                            type="button" title="Buscar">
                                                        <i class="fa fa-search"></i>
                                                    </button>

                                                </div>
                                            </div><!-- /input-group -->
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-9 oculto"  id="rutas-filtro-clave">
                                        <div class="form-group">
                                            <div class="input-group custom-search-form" >
                                                <div class="ruta-filter-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="number" min="1" class="form-control cliente-search-val" placeholder="Números" id="rutas-clave-val" data-inputmask="'mask': '9', 'repeat': 10, 'greedy' : false">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-primary clientes-search-do" type="button" title="Buscar">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div><!-- /input-group -->
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-9 oculto"  id="rutas-filtro-nombre">
                                        <div class="form-group">
                                            <div class="input-group custom-search-form" >
                                                <div class="ruta-filter-clear-do" title="Limpiar">
                                                    <i class="fa fa-close" style="margin-top: 10px;"></i>
                                                </div>
                                                <input type="text" class="form-control cliente-search-val" placeholder="Escriba el valor a buscar" id="rutas-nombre-val">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-primary clientes-search-do" type="button" title="Buscar">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div><!-- /input-group -->
                                        </div>
                                    </div>
                                    <div id="rutas-filtro-colonia" class="oculto">
                                        <div class="col-xs-12 col-sm-4 col-md-8 form-group">
                                            <select class="form-control cliente-search-val" id="rutas-colonia-val" style="width: 100%;">
                                                <option value="">Sel. Colonia</option>
                                                {% for colonia in colonias %}
                                                <option value="{{ colonia.id }}">{{ colonia.nombre }}</option>
                                                {% endfor %}
                                            </select>


                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-1">

                                            <button class="btn btn-primary btn-block clientes-search-do hidden-xs hidden-sm" type="button" title="Buscar">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <button class="btn btn-primary btn-block clientes-search-do hidden-md hidden-lg" style="margin-bottom: 10px;"
                                                    type="button" title="Buscar">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>



                                    <div class="col-xs-12 col-sm-2 col-md-1 text-right" >
                                        {% if acl.isAllowedUser('ruta', 'add') %}
                                        <button class="btn btn-primary btn-block" type="button" title="Nueva Ruta" style="display: inline-block; margin-bottom: 10px;"
                                                id="openNewRuta">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                        {% endif %}
                                    </div>

                                </div>

                            </div>

                        </nav>


                    </div>

                    <div style="margin-top: 15px;">
                        <table class="display table-striped table-hover" id="rutas-datatable" style="width: 100%; height: 100%">
                            <thead>
                            <tr>
                                <th data-priority="1"></th>
                                <th data-priority="2"></th>
                                <th>Vigente</th>
                                <th>Clave</th>
                                <th data-priority="3">Ruta</th>
                                <th data-priority="4">Colonia</th>
                                <th>Usuario</th>

                            </tr>
                            </thead>
                        </table>

                    </div>

                </div>
            </div>
        </div>

    </div>


    <div class="row" style="margin-top: 10px;" id="ruta-container"></div>

</div>


<!-- Modal -->
<div class="modal fade" id="ruta-info-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">

                    <form id="frmRuta">
                        <div class="row">
                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="clave">Clave</label>
                                    <input type="text" class="form-control" id="clave" name="clave" placeholder="Clave" disabled>
                                </div>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="activo">Activo</label>
                                    <div class="checkbox" >
                                        <label>
                                            <input type="checkbox" id="activo" name="activo" disabled>
                                        </label>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="nombre">Nombre</label>
                                    <input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre" required>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="colonias">Colonia</label>
                                    <select class="form-control" id="colonias" name="colonias" required>
                                        <option value="">Sel. Colonia</option>
                                        {% for colonia in colonias %}
                                        <option value="{{ colonia.id }}">{{ colonia.nombre }}</option>
                                        {% endfor %}
                                    </select>

                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="colonias">Usuario</label>
                                    <select class="form-control" id="usuarios" name="usuarios" required>
                                        <option value="">Sel. Usuario</option>
                                        {% for usuario in usuarios %}



                                        {% set usuarioval = [usuario.nombre, usuario.apellido_paterno, usuario.apellido_materno]|join(" ") %}





                                        <option value="{{ usuario.id }}">{{ usuarioval }}</option>
                                        {% endfor %}
                                    </select>

                                </div>
                            </div>
                        </div>






                            <button id="btnGuardaForm" type="submit" class="btn btn-default fade">Submit</button>
                    </form>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="btnGuardar" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script>
    {% if
        acl.isAllowedUser('ruta', 'edit')
        or  acl.isAllowedUser('ruta', 'info')
        or acl.isAllowedUser('ruta', 'delete')
    %}
        {% if acl.isAllowedUser('ruta', 'info') %}
        buttons += '<button class="btn btn-primary btn-sm ruta-info" type="button" title="Consultar">' +
            '<i class="fa fa-info"></i>' +
            '</button> ';
        {% endif %}

        {% if acl.isAllowedUser('ruta', 'edit') %}
        buttons += '<button class="btn btn-primary btn-sm ruta-edit" type="button" title="Modificar">' +
            '<i class="fa fa-pencil"></i>' +
            '</button> ';
        {% endif %}

        {% if acl.isAllowedUser('ruta', 'delete') %}
        buttons += '<button class="btn btn-danger btn-sm ruta-delete" type="button" title="¿Desea eliminar?">' +
            '<i class="fa fa-remove"></i>' +
            '</button> ';
        {% endif %}
    {% endif %}
    $(document).ready(function() {
        rutasDT = $('#rutas-datatable').DataTable({
            "scrollX": true,
            "searching": false,
            "select": true,
            "dom": '<"clear"><rt><"bottom"lip><"clear">',
            "autoWidth": true,
            "paging": true,
            "info": true,
            "processing": true,
            "scrollCollapse": false,
            "pagingType": "full",
            "language": {
                "paginate": {
                    "next": ">",
                    "first": "<<",
                    "last": ">>",
                    "previous": "<"
                },
                "search": "",
                "searchPlaceholder": "Buscar en los resultados encontrados",
                "info": "Resultados:  _TOTAL_ - Pags.: _PAGE_ / _PAGES_",
                "infoEmpty": "",
                "infoFiltered": " - filtrado de _MAX_",
                "emptyTable": "Sin resultados",
                "sZeroRecords": "Sin resultados",
                processing: "Procesando ...",
                "lengthMenu": "Mostrar _MENU_ registros"
            },
            responsive: {
                details: {
                    type: 'column',
                    target: 1
                }
            },
            "columnDefs": [
                {
                    targets: [0],
                    orderable: false,
                    searchable: false,
                    defaultContent: buttons,
                    className: 'dt-center no-wrap',
                    {% if
                        not acl.isAllowedUser('ruta', 'edit')
                        and  not acl.isAllowedUser('ruta', 'info')
                        and not acl.isAllowedUser('ruta', 'delete')
                    %}
                        visible: false
                    {% endif %}
                },
                { targets: [1], orderable: false, className: 'control', searchable: false},
                { targets: [2], searchable: true, className: 'dt-center'},
                { targets: [3], className: 'dt-center'},
                { targets: '_all', visible: true }
            ],
            "lengthMenu": [[10, 20, 30, 40], [10, 20, 30, 40]],
            "pageLength": 10,
            "order": [[ 3, 'asc' ]],
            "processing": true,
            "deferLoading": 0

        });

        {% if acl.isAllowedUser('ruta', 'info') %}
        $("#rutas-datatable").off( 'click', '.ruta-info');
        $("#rutas-datatable").on( 'click', '.ruta-info', function () {
            var $tr = $(this).closest("tr");
            var idAttr = $tr.attr("id").split("-");
            var id = idAttr[1];
            if ( $tr.hasClass('selected') ) {
                //$tr.removeClass('selected');
            }
            else {
                rutasDT.$('tr.selected').removeClass('selected');
                $tr.addClass('selected');
            }

            $("#clave").val(id);
            loadRutaInfo(id, "info");
            $("#ruta-info-modal").modal({
                show: true,
                backdrop: "static"
            });
            /* poner aqui la seccion para la consulta */
        });
        {% endif %}

        {% if acl.isAllowedUser('ruta', 'edit') %}
        $("#rutas-datatable").off( 'click', '.ruta-edit');
        $("#rutas-datatable").on( 'click', '.ruta-edit', function () {
            var $tr = $(this).closest("tr");
            var idAttr = $tr.attr("id").split("-");
            var id = idAttr[1];
            if ( $tr.hasClass('selected') ) {
                //$tr.removeClass('selected');
            }
            else {
                rutasDT.$('tr.selected').removeClass('selected');
                $tr.addClass('selected');
            }


            /* poner aqui la seccion para la ediction */
            loadRutaInfo(id, "edit");
            $("#ruta-info-modal").modal({
                show: true,
                backdrop: "static"
            });
        });
        {% endif %}

        {% if acl.isAllowedUser('ruta', 'delete') %}
        $("#rutas-datatable").off( 'click', '.ruta-delete');
        $("#rutas-datatable").on( 'click', '.ruta-delete', function () {
            $(".popover.confirmation").confirmation("destroy");
            var $tr = $(this).closest("tr");
            var idAttr = $tr.attr("id").split("-");
            var id = idAttr[1];
            $(this).confirmation({
                rootSelector: "body",
                container: "body",
                singleton: true,
                popout: true,
                btnOkLabel: "Si",
                onConfirm: function() {

                    if ( $tr.hasClass('selected') ) {
                        //$tr.removeClass('selected');
                    }
                    else {
                        rutasDT.$('tr.selected').removeClass('selected');
                        $tr.addClass('selected');
                    }
                    eliminarRuta($tr, id);
                },
                onCancel: function() {
                    $(this).confirmation('destroy');
                },
            });

            $(this).confirmation("show");
        });
        {% endif %}


    });
</script>