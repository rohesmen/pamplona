{{ content() }}

<link href="css/pamplona/caja.css" rel="stylesheet">
<!-- ---------------------------------------------------------------------------- -->
<!-- Bootstrap Date-Picker Plugin -->
<link rel="stylesheet" href="plugins/datepicker/bootstrap-datepicker3.css" />
<link href="plugins/dropzone/dropzone.min.css" rel="stylesheet">
<script>
	var proman = {{ proman|json_encode }};
	var memsproman = proman ? proman.meses.split(",") : [];
	var auxpromanmeses = memsproman.map(function (x) {
		return parseInt(x, 10);
	});

	var isdescan = false;
	var tottdescan = 0.0;
</script>
<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>
<!-- ---------------------------------------------------------------------------- -->
<!-- Script para convertir numero a letras para la cantidades-->
<script type="text/javascript" src="./js/numero-letras.js"></script>
{{ javascript_include("plugins/dropzone/dropzone.js") }}
<script type="application/javascript" src="../js/caja-events.js?time={{ time() }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZJImFXCY1MX5n3_OVhXg7Jgb9zBbLlLg&callback=initMap"
		async defer></script>
		
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>COBRO</h2>
	</div>
		
   
    <div class="col-lg-4">
		<h2>
			 
		</h2>
    </div>    	  
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="ibox">
			<div class="ibox-content">
				<div class="container-fluid" style="border: 1px solid #ddd;">
					<div class="row">
						
						<div style="font-weight: bold; ;padding: 15px 5px 5px 10px;">Selecciona tipo de búsqueda por:</div>
						<div class="col-xs-4 col-sm-4 col-md-2">
							<div class="form-group" style="margin-bottom: 5px;">
								<select class="form-control" id="clientes-filter-select">
                                    <!-- [Todos, Empresa, Direccion, Clave cliente]-->
									{#<option value="todos">Todos</option>#}
									{#<option value="rsocial">Empresa</option>#}
									<option value="nom">Nombre</option>
									<option value="dir" selected="selected">Dirección</option>
									<option value="cla">Clave</option>
									<option value="folio">Folio catastral</option>
									<option value="privada">Privada/Cerrada</option>
								</select>
							</div>
						</div>
                        <!-- ------------------------------------------------------------------------------------- -->
                        <!-- Input para buscar todos los clientes-->
						<div class="col-xs-3 col-md-9" style="display: none" id="clientes-filtro-todos">
							<button class="btn btn-primary clientes-search-do hidden-xs hidden-sm" type="button" title="Buscar">
								<i class="fa fa-search"></i>
							</button>
							<button class="btn btn-primary btn-block clientes-search-do hidden-md hidden-lg" style="margin-bottom: 10px;"
									type="button" title="Buscar">
								<i class="fa fa-search"></i>
							</button>
						</div>
                        <!-- ------------------------------------------------------------------------------------- -->
                        <!-- Input para buscar clientes por Empresa(Razon Social)-->
                        <div class="col-xs-8 col-sm-8 col-md-10"  style="display: none;" id="clientes-filtro-empresa">
							<div class="form-group">
								<div class="input-group custom-search-form" >
									<div class="clientes-filter-clear-do" title="Limpiar">
										<i class="fa fa-close" style="margin-top: 10px;"></i>
									</div>
									<input type="text" class="form-control cliente-search-val"
                                           placeholder="Escriba el nombre de la Empresa" id="clientes-empresa-val">
									<div class="input-group-btn">
										<button class="btn btn-primary clientes-search-do" type="button" title="Buscar">
											<i class="fa fa-search"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
                        <!-- ------------------------------------------------------------------------------------- -->
                        <!-- -Busqueda por clave -->
						<div class="col-xs-8 col-sm-8 col-md-10"  style="display: none;" id="clientes-filtro-clave">
							<div class="form-group">
								<div class="input-group custom-search-form" >
									<div class="clientes-filter-clear-do" title="Limpiar">
										<i class="fa fa-close" style="margin-top: 10px;"></i>
									</div>
									<input type="text" class="form-control cliente-search-val" placeholder="Números" id="clientes-clave-val" data-inputmask="'mask': '9', 'repeat': 10, 'greedy' : false">
									<div class="input-group-btn">
										<button class="btn btn-primary clientes-search-do" type="button" title="Buscar">
											<i class="fa fa-search"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
                        <!-- ------------------------------------------------------------------------------------- -->
                        <!-- -Busqueda por Nombre -->
						<div class="col-xs-8 col-sm-8 col-md-10"  style="display: none;" id="clientes-filtro-nombre">
							<div class="form-group">
								<div class="input-group custom-search-form" >
									<div class="clientes-filter-clear-do" title="Limpiar">
										<i class="fa fa-close" style="margin-top: 10px;"></i>
									</div>
                                    <input type="text" class="form-control cliente-search-val" placeholder="Escriba el nombre" id="clientes-nombre-val">
									<div class="input-group-btn">
										<button class="btn btn-primary clientes-search-do" type="button" title="Buscar">
											<i class="fa fa-search"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
						<!-- ------------------------------------------------------------------------------------- -->
						<!-- -Busqueda por folio catastral -->
						<div class="col-xs-8 col-sm-8 col-md-10"  style="display: none;" id="clientes-filtro-folio">
							<div class="form-group">
								<div class="input-group custom-search-form" >
									<div class="clientes-filter-clear-do" title="Limpiar">
										<i class="fa fa-close" style="margin-top: 10px;"></i>
									</div>
									<input type="text" class="form-control cliente-search-val" placeholder="Escriba el folio" id="clientes-folio-val">
									<div class="input-group-btn">
										<button class="btn btn-primary clientes-search-do" type="button" title="Buscar">
											<i class="fa fa-search"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
                        <!-- ------------------------------------------------------------------------------------- -->
                        <!-- -Busqueda por direccion -->
						<div id="clientes-filtro-direccion">
							<div class="col-xs-4 col-sm-4 col-md-2">
								<div class="form-group">
									<input id="clientes-calle-val" name="calle" type="text" class="form-control cliente-search-val" placeholder="Calle">
								</div>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-3">
								<div class="form-group">
									<input id="clientes-numero-val" name="numero" type="text" class="form-control cliente-search-val" placeholder="Número">
								</div>
							</div>
                            <div class="col-xs-9 col-sm-10 col-md-4 form-group">
								<select class="form-control cliente-search-val" id="clientes-colonia-val" style="width: 100%;">
									<option value="">Sel. Colonia</option>
									{% for colonia in colonias %}
										<option value="{{ colonia.id }}">{{ colonia.nombre }}</option>
									{% endfor %}
								</select>
							</div>
                            <div class="col-xs-3 col-sm-2 col-md-1">
								<button class="btn btn-primary clientes-search-do hidden-xs hidden-sm" type="button" title="Buscar">
									<i class="fa fa-search"></i>
								</button>
								<button class="btn btn-primary btn-block clientes-search-do hidden-md hidden-lg" style="margin-bottom: 10px;"
										type="button" title="Buscar">
									<i class="fa fa-search"></i>
								</button>
							</div>
						</div>
						<!-- ------------------------------------------------------------------------------------- -->

						<!-- -Busqueda por privada/cerrada -->
						<div class="col-xs-8 col-sm-8 col-md-10"  style="display: none;" id="clientes-filtro-privada">
							<div class="form-group">
								<div class="input-group custom-search-form" >
									<div class="clientes-filter-clear-do" title="Limpiar">
										<i class="fa fa-close" style="margin-top: 10px;"></i>
									</div>
									<input type="text" class="form-control cliente-search-val" placeholder="Nombre de la privada/Cerrada" id="clientes-privada-val">
									<div class="input-group-btn">
										<button class="btn btn-primary clientes-search-do" type="button" title="Buscar">
											<i class="fa fa-search"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
						<!-- ------------------------------------------------------------------------------------- -->
					</div>
				</div>
                <!-- -------------------------------------------------------------------------------------------- -->
                <!-- Agregamos la tabla para desplegar resultados -->
                <div class="row" style="margin-top: 10px;" id="hist-table-container"></div>
                <div class="row" style="margin-top: 10px;" id="caja-container"></div>
				<div style="margin-top: 15px;">
					
					{% if acl.isAllowedUser('caja', 'cobmench') or
						acl.isAllowedUser('caja', 'cobmencs') or
						acl.isAllowedUser('caja', 'cobmenci') %}
                        <button type="button" id="btnCobrarCLiente" class="btn btn-primary btn-sm btnCobroCliente
						{{ acl.isAllowedUser('caja', 'cobmench') ? 'cobmench' : '' }}
						{{ acl.isAllowedUser('caja', 'cobmencs') ? 'cobmencs' : '' }}
						{{ acl.isAllowedUser('caja', 'cobmenci') ? 'cobmenci' : '' }}" aria-label="Left Align"
								disabled onclick="fnCobrarCliente();">
						<span class="glyphicon glyphicon-usd" aria-hidden="true"></span> Cliente
                        </button>
					{% endif %}
					{% if acl.isAllowedUser('caja', 'descuento_libre') %}
						<button type="button" id="btnCobrarDescuento" class="btn btn-primary btn-sm btnCobroCliente" aria-label="Left Align"
								disabled onclick="fnCobrarClienteDescuento()">
							<span class="glyphicon glyphicon-tags" aria-hidden="true"></span> Descuento
						</button>
					{% endif %}
					{% if acl.isAllowedUser('caja', 'cobmencf') %}
						<button type="button" id="btnCobComFact" class="btn btn-primary btn-sm btnCobroCliente {{ acl.isAllowedUser('caja', 'cobmencf') ? 'cobmencf' : '' }}" aria-label="Left Align"
								disabled onclick="fnCobrarClienteFactura();">
							<span class="glyphicon glyphicon-usd" aria-hidden="true"></span> Facturado
						</button>
					{% endif %}
					{% if acl.isAllowedUser('caja', 'cobrar_servicio') %}
                        <!--<button type="button" class="btn btn-primary btn-sm" aria-label="Left Align" onclick="fnModalCobroServicio();">
						<span class="glyphicon glyphicon-usd" aria-hidden="true"></span> Servicio
                        </button>-->
					{% endif %}
					{% if acl.isAllowedUser('caja', 'ver_ubicacion') %}
					<button type="button" id="btn-ubicacion" class="btn btn-primary btn-sm" 
							aria-label="Left Align" onclick="fnConsultarUbicacion();">
						<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Ubicación
                        </button>
					{% endif %}
					{% if acl.isAllowedUser('caja', 'ver_historial') %}
                        <button type="button" class="btn btn-primary btn-sm" aria-label="Left Align" onclick="fnConsultarHistorial();">
						<span class="glyphicon glyphicon-book" aria-hidden="true"></span> Historial
                        </button>
					{% endif %}
					{% if acl.isAllowedUser('caja', 'reimprimir_recibo') %}
					<button type="button" class="btn btn-primary btn-sm" aria-label="Left Align" onclick="fnModalReimprimir();">						
						<i class="fa fa-print" aria-hidden="true"></i> Reimprimir
                        </button>
					{% endif %}
					{% if acl.isAllowedUser('caja', 'cobrar_rsocial') %}
					<button id="btn-cobro-rsocial" class="btn btn-primary btn-sm" type="button" 
						title="Cobro Masivo" onclick="fnModalCobroMasivo();">
						<i class="fa fa-usd" aria-hidden="true"></i> Raz&oacute;n Social
					</button>
					{% endif %}	
					{% if acl.isAllowedUser('caja', 'cancelar_rsocial') %}
					<button type="button" class="btn btn-primary btn-sm" aria-label="Left Align" onclick="fnModalPagosRazonSocial();">						
						<i class="fa fa-file-text" aria-hidden="true"></i> Pagos Razon Social
					</button>
					 {% endif %}
					 {% if acl.isAllowedUser('clientes', 'bitmens') %}
					<button class="btn btn-primary btn-sm clientes-bitmens" type="button" title="Consultar bitácora de modificaicon de mensualidades">
					<i class="fa fa-list"></i> Bit. mens.
					</button>
					{% endif %}
                    <table class="display table-striped table-hover" id="clientes-datatable" style="width: 100%;">
                        <!-- [Clave|Nombre,Razon Social|calle|Numero|Ultimo año de pago|ultimo mes de pago|Colonia|Adeudo] -->
                        <thead>
						<tr>
							<th data-priority="0">Clave</th>
							<th>Nombre</th>
							<th data-priority="1">Calle</th>
							<th data-priority="2">Numero</th>
							<th>Ultimo Año de Pago</th>
							<th>Ultimo Mes de Pago</th>
							<th>Colonia</th>
							<th>Adeudo</th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th>Comercio</th>
							<th>idestatuscliente</th>
							<th>Estatus cliente</th>
							<th>sigla_estatus_cliente</th>
							<th>isfactura</th>
							<th>permcobro</th>
							<th>activo</th>
                        </tr>
                        </thead>
                    </table>
				</div>
				<!-- -------------------------------------------------------------------------------------------- -->
			</div>
		</div>
	</div><!--[div class="row"]-->
	<!-- -------------------------------------------------------------------------------------------- -->
</div>
<div class="row" style="margin-top: 10px;" id="load-container"></div>
<!-- ------------------------------------------------------------------------------- -->
<!-- Modal para Ubicacion(maps) -->
<div id="modalUbicacion" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Ubicacion</h4>
			</div>
			
			<div class="modal-body">		
				<div class="row" style="margin-top: 10px;" id="loading-map"></div>
				<div class="row">
					<div class="col-md-12">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Buscar..." id="cliente-input-gelocation">
							<span class="input-group-btn">								
								<button class="btn btn-primary" type="button" id="cliente-search-location">Buscar</button>
							</span>
						</div>
					</div>
				</div>
				<div id="map" class="map"></div>						
				<!-- div class="row">
					<div class="wrapper wrapper-content animated fadeInRight">
                        <div id="map" class="map"></div>
                    </div>
				</div -->
				<input type="hidden" id="hdd-map-idcliente" name="hdd-map-idcliente" value="" />	
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-sm" id="btn-ubicacion-save"
						aria-label="Left Align" onclick="fnUpdateUbicacion();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
				</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
		</div>
	</div>
</div><!-- /.modal -->
<!-- ------------------------------------------------------------------------------- -->
<!-- Modal para Cobro de Servicio -->
<div id="modalCobrarServicio" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Cobro de servicio</h4>
			</div>
			<div class="modal-body">
                <div class="wrapper wrapper-content animated fadeInRight">
                    <!-- ------------------------------------------------------------------ -->
                    <div class="row" style="margin-top: 10px;" id="load-guardar"></div>
					<div class="row">

                        <div class="col-xs-6 form-group">
                            <label>Tipo de servicio:</label>
							<select class="form-control" id="select-servicio-tipo">
								{% for mp in tiposervicio %}
									<option value="{{ mp.id }}">{{ mp.nombre }}</option>
								{% endfor %}
							</select>
                        </div>
                        <div class="col-xs-6 form-group">
                            <label>Concepto:</label>
                            <input type="text" class="form-control" id="edt-servicio-concepto" placeholder="" />
                        </div>
                        <div class="col-xs-6 form-group">
                            <label>Tipo de pago:</label>
							<select class="form-control" id="select-servicio-fpago">
								{% for mp in metodopago %}
									{% set valorrefrencia = mp.referencia ? "si" : "no" %}
									<option data-referencia="{{ valorrefrencia }}" value="{{ mp.id }}">{{ mp.nombre }}</option>
								{% endfor %}
							</select>
                        </div>
                        <div class="col-xs-6 form-group">
                            <label>Referencia:</label>
                            <input type="text" class="form-control" id="edt-servicio-ref" placeholder="" disabled />
						</div>
                        <div class="col-xs-6 form-group">
                            <label>Monto:</label>
							<input type="text" class="form-control" id="edt-servicio-monto" placeholder="Números" />
						</div>
                        <div class="col-xs-6 form-group">
                            <!-- Date input -->
							<label class="control-label" for="edt-servicio-fecha">Fecha</label>
                            <input class="form-control" id="edt-servicio-fecha" name="edt-servicio-fecha" placeholder="YYYY-MM-DD" type="text"/>
						</div>
						<div class="col-xs-6 form-group">
                            <div class="checkbox">
								<label><input id="chkFactServicio" type="checkbox">Requiere Factura</label>
							</div>							
						</div>
						<div class="col-xs-6 form-group">
							<button type="button" id="btn-servicio-nota" class="btn btn-primary btn-sm" onclick="fnprintNotaServicio();">
								<i class="fa fa-print" aria-hidden="true"></i> Nota de Cobro
							</button>
						</div>																																	
						<input type="hidden" id="hdd-servicio-idcte"   name="hdd-servicio-idcte" value="" />						
						<input type="hidden" id="hdd-servicio-nombre"  name="hdd-servicio-nombre" value="" />
						<input type="hidden" id="hdd-servicio-ruta"    name="hdd-servicio-ruta" value="" />
						<input type="hidden" id="hdd-servicio-direc"   name="hdd-servicio-direc" value="" />
						<input type="hidden" id="hdd-servicio-colonia" name="hdd-servicio-colonia" value="" />
						<input type="hidden" id="hdd-servicio-idpago"  name="hdd-servicio-idpago" value="" />						
                    </div><!-- fin:[div class="row"]-->
				</div>
				<!-- ------------------------------------------------------------------ -->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-sm" id="btn-servicio-cobro"
						aria-label="Left Align" onclick="fnCobrarServicio();">
                    <span class="glyphicon glyphicon-usd" aria-hidden="true"></span>Cobrar
                </button>
                <button type="button" class="btn btn-sm" data-dismiss="modal">Cerrar</button>
            </div>
		</div>
	</div>
</div><!-- /.modal -->
<!-- ------------------------------------------------------------------------------- -->
<!-- Modal para el Historial -->
<div id="modalHistorial" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Historial de Pagos</h4>
			</div>
			<div class="modal-body">				
				<div class="row" style="margin-top: 0px;" id="wait-ajax"></div>
				<div class="row" style="margin-top: 0px;" id="loading-cancelar"></div>
				<div class="row">										
					{% if acl.isAllowedUser('caja', 'cancelar') %}
					<button id="btn-hist-cancelar" class="btn btn-danger btn-sm clientes-delete" 
							type="button" title="¿Desea eliminar?" onclick="fnCancelarHistPago();">
						<i class="fa fa-remove"></i> Cancelar
					</button>
					{% endif %}
					<button id="btn-hist-detalles" class="btn btn-primary btn-sm clientes-delete"
							type="button" title="¿Desea eliminar?" onclick="fnVerDetalles();">
						<i class="fa fa-list"></i> Ver detalles
					</button>
					<table class="display table-striped table-hover" id="historial-table" style="width: 100%;">
						<thead>
						<tr>
							<th></th>
							<th>Fecha pago</th>
							<th>Cliente</th>
							<th>Folio</th>
							<th>Usuario</th>
							<th>Activo</th>
                            <th>Subtotal</th>
							<th>Comisión</th>
							<th>Total</th>
							<th>PAYPAL</th>
							<th>En corte</th>
							<th></th> <!-- 11: idpago -->
							<th>Des. Pensionado</th>
							<th></th> <!-- 13: iddescunto_pensionado -->
							<th>Impresiones</th> <!-- 13: iddescunto_pensionado -->
							<th>Negociación</th> <!-- 13: iddescunto_pensionado -->
						</tr>
						</thead>
					</table>
                </div>				
            </div>
            <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
        </div>
	</div>
</div>
<!-- ------------------------------------------------------------------------------- -->
<!-- Modal para Cobro de Servicio de Recolecta -->
<div class="modal fade" id="modal-cobro-cliente" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Servicio de Recolección de Basura</h4>
			</div>
			<div class="modal-body">
				<div class="row" style="margin-top: 0px;" id="aviso-adeudo"></div>
                
                <div class="row">
                	<div class="col-xs-12 col-sm-12 col-lg-12">
                		<button class="btn btn-primary btn-sm clientes-edit" style="float: right;" onclick="fnEditarCliente();"><i class="fa fa-pencil"></i></button>
                	</div>
                	
                </div>

				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Nombre:</label>
						<input type="text" class="form-control" id="edt-cobro-Nombre" placeholder="" disabled/>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Direccion:</label>
						<input type="text" class="form-control" id="edt-cobro-direccion" placeholder="" disabled/>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-6 form-group">
						<label>último año:</label>
						<input type="text" class="form-control" id="edt-cobro-ultanio" placeholder="" disabled/>
					</div>
					<div class="col-xs-6 form-group">
						<label>Ultimo mes:</label>
						<input type="text" class="form-control" id="edt-cobro-ultmes" placeholder="" disabled/>
					</div>
					<!--div class="col-xs-2 form-group" style="margin-top: 20px;">
						<div class="checkbox">
							<label><input id="chkDescuento" type="checkbox">Descuento</label>
						</div>
					</div -->
				</div><!-- [div class="row"] -->		
				

				<div class="row" id="container_folio_factura">
					<!--<div class="col-xs-6 form-group" style="margin-top: 20px;">
						<div class="checkbox">
							<label><input id="chkFactCobro" type="checkbox">Factura</label>
						</div>
					</div>-->
					<div class="col-xs-6 form-group" >
						<label>Folio Factura:</label>
						<input type="text" class="form-control" id="folio_factura"/>
					</div>
					<div class="col-xs-6 form-group" >
						<label>Factura:</label>
						<div id="rutaFolioFactura" class="dropzone" style="text-align: center;">
							<div class="dz-default dz-message">
								<div class="dz-icon">
									<i class="fa fa-cloud-upload fa-5x"></i>
								</div>
								<div>
									<span class="dz-text">Arrastrar archivo</span>
									<p class="text-sm text-muted">o seleccionar manualmente</p>
									<p class="text-sm text-muted">Permitido:  pdf</p>
								</div>
							</div>
							<div class="fallback">
								<input type="file" id="form-descripcion">
							</div>
						</div>
					</div>
				</div>

				<div class="row" id="container-datos-pago">
					<div class="col-xs-6 col-md-4 form-group">
						<label>Tipo de pago:</label>
						<select class="form-control" id="select-basura-fpago">
							{% for mp in metodopago %}
								{% set valorrefrencia = mp.referencia ? "si" : "no" %}
								<option data-referencia="{{ valorrefrencia }}" value="{{ mp.id }}">{{ mp.nombre }}</option>
							{% endfor %}
						</select>
					</div>
					<div class="col-xs-6 col-md-3 form-group" id="container_referencia_bancaria">
						<label>referencia:</label>
						<input type="text" class="form-control" id="referencia_bancaria"/>
					</div>
                    <div class="col-xs-6 col-md-5 form-group" id="container_folio_interno" style="display: {{ acl.isAllowedUser('caja', 'folio_interno') ?  'block' : 'none' }}">
						<label>Folio interno asignado:</label>
						<div class="input-group">
							<input type="text" class="form-control" disabled id="folio_interno" style="text-transform: uppercase"/>
							<span class="input-group-addon">
								<input type="checkbox" aria-label="..." id="folio_interno_sistema"
										{{ acl.isAllowedUser('caja', 'folio_interno') ?  '' : 'disabled' }}>
							</span>
						</div>
                    </div>
				</div>
				
				<div class="row">	

					<div class="col-xs-5 form-group">
						<label>Año:</label>
						<select class="form-control" id="select-cobro-anio"></select>
					</div>
					<!--<div class="col-xs-5 form-group">-->
						<!--<label>Mes:</label>-->
						<!--<select class="form-control" id="select-cobro-mes">							-->
							<!--<option value="1">Enero</option>-->
							<!--<option value="2">Febrero</option>-->
							<!--<option value="3">Marzo</option>-->
							<!--<option value="4">Abril</option>-->
							<!--<option value="5">Mayo</option>-->
							<!--<option value="6">Junio</option>-->
							<!--<option value="7">Julio</option>-->
							<!--<option value="8">Agosto</option>-->
							<!--<option value="9">Septiembre</option>-->
							<!--<option value="10">Octubre</option>-->
							<!--<option value="11">Noviembre</option>-->
							<!--<option value="12">Diciembre</option>-->
						<!--</select>-->
					<!--</div>-->
					<!---->
					<div class="col-xs-2 form-group" style="margin-top: 22px;">
						<button id="btn-cobro-add" class="btn btn-primary btn-block" type="button" title="Agregar" onclick="fnAddMensualidadYear();">
							<i class="fa fa-plus"></i>
						</button>
					</div>

					<!--div class="col-xs-3 form-group" style="margin-top: 20px;">
						<button type="button" id="btn-recibo-cliente" class="btn btn-primary btn-block" onclick="fnPrintReciboMes();">
							<i class="fa fa-print" aria-hidden="true"></i>  Recibo
						</button>
					</div -->					
					<input type="hidden" id="hdd-recolecta-idcte" name="hdd-recolecta-idcte" value="" />
					<input type="hidden" id="hdd-mensualidad" name="hdd-mensualidad" value="" />
					<input type="hidden" id="hdd-ultpago-mes" name="hdd-ultpago-mes" value="" />
					<input type="hidden" id="hdd-recolecta-ruta" name="hdd-recolecta-ruta" value="" />
					<input type="hidden" id="hdd-recolecta-direc" name="hdd-recolecta-direc" value="" />
					<input type="hidden" id="hdd-recolecta-colonia" name="hdd-recolecta-colonia" value="" />
					<input type="hidden" id="hdd-cobro-idpago"  name="hdd-cobro-idpago" value="" />	
					<input type="hidden" id="hdd-cobro-idpagop"  name="hdd-cobro-idpagop" value="" />
					<input type="hidden" id="hdd-cobro_manual"  name="hdd-cobro_manual" value="" />
					<input type="hidden" id="hdd-monto_cobro"  name="hdd-monto_cobro" value="" />	
				</div><!-- fin:[div class="row"]--> 								
				<div class="row" id="container-descuento">
					<div class="col-xs-6 form-group" style="margin-top: 20px;" id="group_cobro_manual">
						<label>¿Cobro manual? <span id="label_cobro_manual"></span></label>
						<input type="text" class="form-control disabled" disabled="disabled" id="monto_cobro"/>
					</div>
                    {% if acl.isAllowedUser('caja', 'descuento_libre') %}
					<div class="col-xs-6 form-group" style="margin-top:20px;">
						<div class="checkbox">
							<label><input id="chkDescuento" type="checkbox">Aplicar descuento a</label>
						</div>
					</div>
                    {% endif %}
				</div>
				
				<div class="row" style="margin-top: 10px;" id="load-save-recolecta"></div>
				<!-- -------------------------------------------------------------------- -->
				<div class="row" style="margin-top: 0px; margin-left:5px">
					<table class="table-condensed table-hover" id="mensualidad-table" style="width: 100%;">
						<thead>
						<tr>
							<th></th>
							<th>Mes</th>
							<th>Mensualidad</th>
							<th>Descuento</th>
							<th>Total</th>
							<th></th>
							<th></th>
						</tr>
						</thead>
					</table>
				</div>
				<!-- -------------------------------------------------------------------- -->
				<div class="row">

					<div id="divTotalesFactura">
	
						<div class="col-xs-3 col-xs-offset-3">
							<label>Subtotal:</label>
							<input type="text" class="form-control disabled" disabled="disabled" id="subtotalPagar"/>
						</div>

						<div class="col-xs-3">
							<label>IVA:</label>
							<input type="text" class="form-control disabled" disabled="disabled" id="ivaPagar"/>
						</div>
	
					</div>

					<div id="divTotalPagar" class="col-xs-3">
						<label>Total:</label>
						<input type="text" class="form-control disabled" disabled="disabled" id="totalPagar"/>
					</div>

				</div>
			</div>

			<div class="modal-footer">
				<div class="row" id="container-btns-facturado">
					<div class="col-md-4" style="margin-bottom: 10px;"></div>
					<div class="col-md-4" style="margin-bottom: 10px;">
						<button type="button" class="btn btn-primary btn-block" id="btn-cobro-mes-facturado" onclick="fnCobrarFacturado();">
							<span class="glyphicon glyphicon-usd" aria-hidden="true"></span>Cobrar
						</button>
					</div>
					<div class="col-md-4" style="margin-bottom: 10px;">
						<button type="button" class="btn btn-default btn-block btnCloseCobrar" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
				<div class="row" id="container-btns-desceunto">
					<div class="col-md-4" style="margin-bottom: 10px;"></div>
					<div class="col-md-4" style="margin-bottom: 10px;">
						<button type="button" class="btn btn-primary btn-block" id="btn-cobro-mes-descuento" onclick="fnCobrarDescuento();">
							<span class="glyphicon glyphicon-usd" aria-hidden="true"></span>Cobrar
						</button>
					</div>
					<div class="col-md-4" style="margin-bottom: 10px;">
						<button type="button" class="btn btn-default btn-block btnCloseCobrar" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
				<div class="row" id="container-btns-cobrocliente">
					<div class="col-md-4" style="margin-bottom: 10px;">
						<button type="button" id="btn-recibo-cliente" class="btn btn-primary btn-block" onclick="fnPrintReciboMes();">
							<i class="fa fa-print" aria-hidden="true"></i>  Recibo
						</button>
					</div>
					<div class="col-md-4" style="margin-bottom: 10px;">
						<button type="button" class="btn btn-primary btn-block" id="btn-cobro-mes" onclick="fnCobrarRecolecta();">
							<span class="glyphicon glyphicon-usd" aria-hidden="true"></span>Cobrar
						</button>
					</div>
					<div class="col-md-4" style="margin-bottom: 10px;">
						<button type="button" class="btn btn-default btn-block btnCloseCobrar" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!--[Modal]-->
<!-- ------------------------------------------------------------------------------- -->
<!-- Modal para el cobro masivo por Razon social -->
<div class="modal fade" id="modalCobroMasivo" role="dialog">
	<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Cobro Por Razon Social</h4>
            </div>
            <div class="modal-body">
				<div class="row" style="margin-top: 0px;" id="aviso-adeudo-masivo"></div>		
				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Seleccione Razon Social:</label>
						<select class="form-control" id="select-masivo-rsocial">							
							{% for rs in razonsocial %}													
								<option data-data='{"fisica": {{rs.fisica}}  }' value="{{ rs.id }}">{{ rs.razon_social }}</option>
							{% endfor %}>
						</select>
					</div>		
				</div>														
				<div class="row">
					<div class="col-xs-6 form-group" style="margin-top: 20px;">
						<div class="checkbox">
							<label><input id="chkmasFactura" type="checkbox">Factura</label>
						</div>
					</div>
                    <div class="col-xs-6 form-group">
						<label>Folio Factura:</label>
						<input type="text" class="form-control" id="edt-masivo-folfac"/>
					</div>
				</div>
				<div class="row">
                    <div class="col-xs-6 form-group">
						<label>Tipo de pago:</label>
						<select class="form-control" id="select-masivo-fpago">
							{% for mp in metodopago %}
								{% set valorrefrencia = mp.referencia ? "si" : "no" %}
								<option data-referencia="{{ valorrefrencia }}" value="{{ mp.id }}">{{ mp.nombre }}</option>
							{% endfor %}
						</select>
					</div>
                    <div class="col-xs-6 form-group">
						<label>referencia:</label>
						<input type="text" class="form-control" id="edt-masivo-referencia"/>
					</div>
				</div>
				
				<div class="row">
                    <div class="col-xs-6 form-group">
						<div class="checkbox">
							<label><input id="chkDesctoMasivo" type="checkbox">Descuento</label>
						</div>
					</div>							
					<div class="col-xs-6 form-group" style="margin-top:0px;width:40%">
						<button type="button" id="btn-rsocial-recibo" class="btn btn-primary btn-block" 
							onclick="fngenerarReciboMasivo();" disabled>
							<i class="fa fa-print" aria-hidden="true"></i>  Recibo
						</button>
					</div>														
				</div>
							
                <div class="row">
					<div class="col-xs-5 form-group">
                        <label>Año:</label>
						<select class="form-control" id="select-masivo-anio"></select>
                    </div>
					<div class="col-xs-5 form-group">
                        <label>Mes:</label>
						<select class="form-control" id="select-masivo-mes">							
                            <option value="1">Enero</option>
                            <option value="2">Febrero</option>
                            <option value="3">Marzo</option>
                            <option value="4">Abril</option>
                            <option value="5">Mayo</option>
                            <option value="6">Junio</option>
                            <option value="7">Julio</option>
                            <option value="8">Agosto</option>
                            <option value="9">Septiembre</option>
                            <option value="10">Octubre</option>
                            <option value="11">Noviembre</option>
							<option value="12">Diciembre</option>
						</select>
					</div>					
					<div class="col-xs-2 form-group" style="margin-top: 22px;">
						<button id="btn-masivo-add" class="btn btn-primary btn-block" type="button" title="Agregar" onclick="fnaddMesMasivo();">
                            <i class="fa fa-plus"></i>
                        </button>
					</div>											
				</div><!-- fin:[div class="row"]--> 												
				<!-- -------------------------------------------------------------------- -->				
				<div class="row" style="margin-top: 0px;" id="loading-rsocial"></div>
				<div class="row" style="margin-top: 0px; margin-left:0px">
					<table class="table-condensed table-hover" id="rsocial-table" style="width: 100%;">
						<thead>
						<tr>
							<th>Mes</th>		<!-- [0:anio_mes] -->
							<th>Cliente</th>	<!-- [1:cliente] -->
							<th>Mensualidad</th><!-- [2:mensualidad] -->
							<th>Descuento</th>	<!-- [3:descuento] -->
							<th>Total</th>		<!-- [4:total] -->
							<th></th>			<!-- [5:annio] -->
							<th></th>			<!-- [6:imes] -->
							<th></th>			<!-- [7:idcliente] -->							
							<th></th>			<!-- [8:monto_mes] -->							
							<th></th>			<!-- [9:cliente] -->
							<th></th>			<!-- [10:cliente] -->												
							<th></th>			<!-- [11:idrazonsocial] -->
						</tr>
						</thead>						
					</table>
				</div>					
				<input type="hidden" id="hdd-idpagomasivo" name="hdd-idpagomasivo" value="" />
				<input type="hidden" id="hdd-idrazonsocial" name="hdd-idrazonsocial" value="" />
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-sm" id="btn-masivo-cobrar" onclick="fnCobrarPagoMasivo();">
					<span class="glyphicon glyphicon-usd" aria-hidden="true"></span>Cobrar
				</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
		</div>      
	</div>
</div><!--[Modal] -->  	
<!-- ------------------------------------------------------------------------------- -->
<div class="modal fade" id="modalPagosMasivos" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Pagos por Razon Social</h4>
			</div>
			<div class="modal-body">				
				<div class="row">
					<div class="col-xs-8 col-lg-4 form-group" style="">						
						<label class="control-label" for="edt-rsocial-fechaini">Fecha Inicial:</label>
						<input class="form-control" id="edt-rsocial-fechaini" name="edt-rsocial-fechaini" placeholder="dd/mm/yyyy" type="text"/>
					</div>					
					<div class="col-xs-8 col-lg-4 form-group" style="">	
						<label class="control-label" for="edt-rsocial-fechafin">Fecha Final:</label>
						<input class="form-control" id="edt-rsocial-fechafin" name="edt-rsocial-fechafin" placeholder="dd/mm/yyyy" type="text"/>
					</div>
					<div class="col-xs-2 col-lg-2 form-group">
						<button type="button" title="Buscar" class="btn btn-primary" 
							id="btn-rsocial-search" style="margin-top: 20px" onclick="fnbuscarPagosRazonSocial();">
							<i class="fa fa-search" aria-hidden="true"></i> 
						</button>
					</div>
				</div><!-- [class="row"] -->				
				<div class="row">										
					{% if acl.isAllowedUser('caja', 'cancelar') %}
					<button id="btn-pagorsocial-cancelar" class="btn btn-danger btn-sm clientes-delete" 
							type="button" title="¿Desea eliminar?" onclick="fncancelarPagoMasivo();">
						<i class="fa fa-remove"></i> Cancelar
					</button>
					{% endif %}		
					
					<button id="btn-pagorsocial-print" class="btn btn-primary btn-sm" 
							type="button" title="Imprimir Recibo" onclick="fnreimprimirPagoMasivo();">
						<i class="fa fa-print" aria-hidden="true"></i> Imprimir
					</button>										
				</div>					
				<div class="row" style="margin-top: 0px;" id="loading-rsocial-pagos"></div>
				<div class="row">
					<table class="display table-striped table-hover" id="pagosmasivo-table" style="width: 100%;">									
						<thead>
						<tr>
							<th>Folio</th>			<!-- 0:idpagomasivo -->													
							<th>Razon Social</th>	<!-- 1:razonsocial -->
							<th>Subtotal</th>		<!-- 2:subtotal -->
							<th>Descuento</th>		<!-- 3:descuento -->
							<th>Total</th>			<!-- 4:total -->
							<th>Activo</th>			<!-- 5:activo-->
							<th>Fec. Pago</th>		<!-- 6:fechapago -->                            
							<th>Mét. pago</th>		<!-- 7:metodopago -->
							<th>Referencia</th>		<!-- 8:referencia -->
							<th>Factura</th>		<!-- 9:factura -->
							<th></th>				<!-- 10:idrazonsocial -->
						</tr>
						</thead>
					</table>
                </div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<!-- ------------------------------------------------------------------------------- -->
<div class="modal fade" id="modalPerDescto" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Aplicar Descuento</h5>
			</div>
			<div class="modal-body">
				<div class="row" style="margin-top: 0px;" id="wait-permiso"></div>
				<div class="row" style="margin-top: 0px;" id="descto-message"></div>
				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Usuario:</label>
						<input type="text" class="form-control" id="edtPerUser" placeholder="Usuario" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Password:</label>
						<input type="password" class="form-control" id="edtPerPass" placeholder="Contraseña" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Motivo:</label>
						<input type="text" class="form-control" id="edtMotivo" />
					</div>
				</div>
				<!--<div class="row">
					<div class="col-xs-12 form-group">
						<label>Monto Descuento:</label>
						<input type="text" class="form-control" id="edtPerMonto" placeholder="Números" />
					</div>
				</div>-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-sm" id="btn-perdescto" onclick="fnAplicarDescto();">Aplicar</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div><!--[Modal]-->
<!-- -------------------------------------------------------- -->
<div class="modal fade" id="modalPerDesctoMasivo" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Aplicar Descuento</h5>
			</div>
			<div class="modal-body">
				<div class="row" style="margin-top: 0px;" id="wait-masivo-permiso"></div>
				<div class="row" style="margin-top: 0px;" id="aviso-masivo-permiso"></div>
				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Usuario:</label>
						<input type="text" class="form-control" id="edtMasivoUserPer" placeholder="Usuario" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Password:</label>
						<input type="password" class="form-control" id="edtMasivoPassPer" placeholder="Contraseña" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Monto Descuento:</label>
						<input type="text" class="form-control" id="edtMasivoMontoPer" placeholder="Números" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-sm" id="btn-masivo-aplicar" onclick="fnvalidarDesctoMasivo();">
					<i class="fa fa-check" aria-hidden="true"></i> Aplicar
				</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div><!--[Modal]-->
<!-- ------------------------------------------------------- -->
<div class="modal fade" id="modalReimpresion" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Historial de Recibos</h4>
			</div>
			<div class="modal-body">				
				<!-- div class="row" style="margin-top: 0px;" id="wait-search-recibos"></div-->
				<div class="row">
					<div class="col-xs-8 col-lg-4 form-group" style="">						
						<label class="control-label" for="edt-rprint-fechaini">Fecha Inicial:</label>
						<input class="form-control" id="edt-rprint-fechaini" name="edt-rprint-fechaini" placeholder="dd/mm/yyyy" type="text"/>
					</div>					
					<div class="col-xs-8 col-lg-4 form-group" style="">	
						<label class="control-label" for="edt-rprint-fechafin">Fecha Final:</label>
						<input class="form-control" id="edt-rprint-fechafin" name="edt-rprint-fechafin" placeholder="dd/mm/yyyy" type="text"/>
					</div>
					<div class="col-xs-2 form-group">
						<button type="button" title="Buscar" class="btn btn-primary" 
							id="btn-search" style="margin-top: 20px" onclick="fnbuscarRecibos();">
							<i class="fa fa-search" aria-hidden="true"></i> 
						</button>
					</div>
					{#<div class="col-xs-2 form-group">#}
						{#<button type="button" title="Buscar" class="btn btn-primary" #}
							{#id="btn-search" style="margin-top: 20px" onclick="fnbuscarRecibos();">#}
							{#<i class="fa fa-search" aria-hidden="true"></i> #}
						{#</button>#}
					{#</div>#}
				</div><!-- [class="row"] -->
				<div class="row" style="margin-top: 0px;" id="wait-search-recibos"></div>
				<!-- datable para los Recibos -->
				<!-- style="margin-top: 0px; margin-left:5px"-->
				<div class="row"> 																				
					<button id="btn-rprint-recibo" class="btn btn-primary btn-sm" 
							type="button" title="Imprimir Recibo" onclick="fnReprintRecibo();">
						<i class="fa fa-print" aria-hidden="true"></i> Imprimir
					</button>	
					<table class="table-condensed table-hover" id="table-recibos" style="width: 100%;">
						<thead>
						<tr>
							<th>Folio</th>
							<th>Fec. Pago</th>
							<th>Fec. Creacion</th>
							<th>Subtotal</th>
							<th>Descuento</th>
							<th>Total</th>
							<th>Activo</th>																
							<th>idtipocobro</th>
							<th>idtipocobro</th>
							<th>usuario</th>
						</tr>
						</thead>
					</table>
					<input type="hidden" id="hdd-reprint-idcliente" name="hdd-reprint-idcliente" value="" />			
					<input type="hidden" id="hdd-reprint-nombre"  	name="hdd-reprint-nombre" value="" />	
					<input type="hidden" id="hdd-reprint-direccion" name="hdd-reprint-direccion" value="" />	
					<input type="hidden" id="hdd-reprint-colonia"  	name="hdd-reprint-colonia" value="" />	
					<input type="hidden" id="hdd-reprint-ruta"  	name="hdd-reprint-ruta" value="" />	
					<!-- input type="hidden" id="hdd-reprint-email"  	name="hdd-reprint-email" value="" / -->	
					<input type="hidden" id="hdd-reprint-idcliente" name="hdd-reprint-idcliente" value="" />	
				</div><!-- fin:[class="row"] -->						
			</div>
			<div class="modal-footer">				
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<!-- ------------------------------------------------------- -->	
<div class="modal fade" id="modalRecibo" role="dialog">
	<div class="modal-dialog">    		
		<div class="modal-content">	
			<div class="modal-body">										
				<input type="hidden" id="hdd-recibo-email" name="hdd-recibo-email" value="" />
				<div class="row" id="recibo-print-body">																			
					<table id="tblinfocliente" class="table no-border table-condensed" border="0" cellpadding="0" cellspacing="0" align="center">
						<tbody>									
							<tr>
							  <td class="" colspan="2"  align="center">
								<b>{{ config.application.nom_com }}</b></td>
							  <td class=""></td>
							</tr>
							<tr>
								<td colspan="2" align="center">{{ config.application.dir_pam }}</td>
								<td class=""></td>
							</tr>
							<tr>
								<td colspan="2" align="center">M&eacute;rida, Yucat&aacute;n, M&eacute;xico. R.F.C.: {{ config.application.rfcpam }}</td>
								<td></td>
							</tr>
							<tr>
								<td align="right">Tel. {{ config.application.tel_pam }}</td>
								<td align="center">No.:  <label id="recibo-folio"></label></td>
							</tr>													
							<tr>
								<td align="left" colspan="2">Nombre:  <label id="recibo-nombre"></label></td>								
							</tr>
							
							<tr>
								<td align="left" colspan="2">Direcci&oacute;n:  <label id="recibo-direccion"></label></td>								
							</tr>
							
							<tr>
								<td align="left" colspan="2">Colonia:  <label id="recibo-colonia"></label></td>
							</tr>
							<tr>
								<td align="left" colspan="2">No. Cliente:  <label id="recibo-idcliente"></label></td>
							</tr>
							<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
							
							<tr>							
								<td colspan="2" align="left"><label id="recibo-concepto"></label></td>							
							</tr>	
							<tr>														
								<td colspan="2" align="left"><label id="recibo-meses"></label></td>									
							</tr>	
														
						</tbody>
					</table>																					
					<!-- Agregamos un borde a la tabla-->
					<!-- table id="tblconceptos" class="table no-border table-condensed" 
						border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #ddd;width:100%;" align="center">
						<tr>							
							<td colspan="2" align="left"><label id="recibo-concepto"></label></td>							
						</tr>	
						<tr>														
							<td colspan="2" align="left"><label id="recibo-meses"></label></td>									
						</tr>	
					</table -->
					<br>
					<table class="table no-border table-condensed" id="tblReciboTotales" border="0" cellpadding="0" cellspacing="0" align="center">
						<tbody>																
							<tr id="row-total1">
								<td align="left" width="60%">Cantidad en letras</td>
								<td align="right" width="40%"></td>
							</tr>																				

							<tr>
								<td align="left" colspan="2">
									AGENTE DE COBRANZA: <label id="usuario-cobranza">{{ identity['userName'] }}</label>
								</td>
							</tr>																																			
						</tbody>
					</table>
					<table class="table no-border table-condensed" id="txtMensajePromAn" border="0" cellpadding="0" cellspacing="0" style="display: none;">
						<tbody>
						<tr >
							<td colspan="2">Usted participa en la promoción anual derivado a su pago anual. Favor de comprobar este comprobante como boleto participante. Consulte las bases en {{ config.application.publicWebUrl }}</td>
						</tr>
						</tbody>
					</table>
				</div> <!-- [class="row"] -->				
            </div>
			<div class="modal-footer">				
				<button type="button" class="btn btn-primary btn-sm" id="btn-recibo-print" onclick="fnprintRecibo();">
					<i class="fa fa-print" aria-hidden="true"></i> Imprimir
				</button>
				<button type="button" class="btn btn-primary btn-sm" id="btn-recibo-email" onclick="fnshowModalCorreo();">
					<i class="fa fa-envelope" aria-hidden="true"></i> Correo
				</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
            </div>
		</div>
    </div>
</div><!--[Modal]-->
<!-- ------------------------------------------------------- -->	
<div class="modal fade" id="modalReciboMasivo" role="dialog">
	<div class="modal-dialog">    		
		<div class="modal-content">	
			<div class="modal-body">										
				<input type="hidden" id="hdd-rsocial-email" name="hdd-rsocial-email" value="" />
				<input type="hidden" id="hdd-recibo-idpagomasivo" name="hdd-recibo-idpagomasivo" value="" />
				<div class="row" id="idrecibo-rsocial-body">																			
					
					<table id="tblReciboinforsocial" class="table no-border table-condensed" border="0" 
						cellpadding="0" cellspacing="0" width = "500px" align="center">
						<tbody>																
							<tr>
							  <td class="" colspan="2"  align="center">
								<b>{{ config.application.nom_com }}</b></td>
							  <td class=""></td>
							</tr>
							<tr>
								<td colspan="2" align="center">{{ config.application.dir_pam }}</td>
								<td class=""></td>
							</tr>
							<tr>
								<td colspan="2" align="center">M&eacute;rida, Yucat&aacute;n, M&eacute;xico. R.F.C.: {{ config.application.rfcpam }}</td>
								<td></td>
							</tr>
							<tr>
								<td align="center">Tel. 9844799</td>
								<td align="left">Folio:  <label id="recibo-rsocial-folio"></label></td>
							</tr>					
							
							<tr>
								<td align="left" colspan="2">Razon Social:  <label id="recibo-rsocial-nombre"></label></td>													
							</tr>
							
							<tr>
								<td align="left" colspan="2">Direcci&oacute;n:  <label id="recibo-rsocial-dir"></label></td>								
							</tr>
							
							<tr>
								<td align="left" colspan="2">Colonia:  <label id="recibo-rsocial-colonia"></label></td>
							</tr>												
							<tr>
								<td align="left">Fecha:  <label id="recibo-rsocial-fecha"></label></td>
								<td align="left">Clave:  <label id="recibo-rsocial-clave"></label></td>
							</tr>							
						</tbody>
					</table> <!-- [fin:tblinforsocial] -->	
					<!-- ******************************************************************* -->	
					<!-- tabla para listar las empresas y los meses cubiertos-->
					<!--  table table-bordered, table no-border table-condensed, table table-bordered table-condensed -->					
					<table class="table table-cborder" id="recibo-rsocial-empresas" cellpadding="0" cellspacing="0"
						align="center" width = "500px">
						<thead>
							  <tr><th width="30%">Empresa</th><th width="45%">Meses</th><th width="25%">Total</th></tr>
						</thead>
						<tbody>							
						</tbody>
					</table>							
					<!-- ******************************************************************* -->					
					<br/>
					<table class="table no-border table-condensed" id="tbl-rsocial-totales" border="0" 
						   cellpadding="0" cellspacing="0" align="center" width = "500px">
						<tbody>																
						<tr id="">
							<td align="left" width="60%">Cantidad en letras</td>
							<td align="right" width="40%">Subsidio: $3.00</td>
						</tr>																				
						<tr id="">
							<td align="left"  width="60%">Son:  <label id="recibo-rsocial-letratotal"></label></td>
							<td align="right" width="40%">Total pagado:  <label id="recibo-rsocial-total"></label></td>
						</tr>																																			
						<tr>
							<td align="left" width="60%">
								Todos los Residuos sólidos (Basura)<br/> 
								serán depositados en el Relleno Sanitario<br/>
								Subsidio del 10% sobre Tarifa anterior aplicada.						
							</td>
							<td align="left" width="40%">
								Nota: Para cualquier aclaraci&oacute;n,<br/> 
								favor de conservar su último recibo.
							</td>
						</tr>
						</tbody>
					</table>				
				</div> <!-- -->				
			</div>
			<div class="modal-footer">				
				<button type="button" class="btn btn-primary btn-sm" id="btn-recibo-rsocial-print" onclick="fnprintReciboMasivo();">
					<i class="fa fa-print" aria-hidden="true"></i> Imprimir
				</button>
				<button type="button" class="btn btn-primary btn-sm" id="btn-recibo-rsocial-email" onclick="fnshowModalCorreoMasivo();">
					<i class="fa fa-envelope" aria-hidden="true"></i> Correo
				</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div><!--[Modal:modalReciboMasivo]-->
<!-- ------------------------------------------------------- -->
<!-- Modal para el envio del recibo por correo -->
<div class="modal fade" id="modalEmailRecibo" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Direccion de Correo</h4>
			</div>
			<div class="modal-body">
				<div class="row" style="margin-top: 0px;" id="loading-email"></div>
				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Correo:</label>
						<input type="text" class="form-control" id="edtEmailTo" placeholder="Correo Electronico" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-sm" id="btn-recibo-email" onclick="fnsendEmailRecibo();">
					<i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar
				</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
		</div>      
	</div>
</div><!--[Modal] -->  	
<!-- ------------------------------------------------------- -->
<!-- Modal para el envio del recibo por correo -->
<div class="modal fade" id="modalEmailPagoMasivo" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Direccion de Correo</h4>
			</div>
			<div class="modal-body">
				<div class="row" style="margin-top: 0px;" id="loading-rsocial-email"></div>
				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Correo:</label>
						<input type="text" class="form-control" id="edtEmailTo-rsocial" placeholder="Correo Electronico" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-sm" id="btn-rsocial-email" onclick="fnsendEmailReciboMasivo();">
					<i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar
				</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
		</div>      
	</div>
</div><!--[Modal] -->  	
<!-- ------------------------------------------------------- -->


<!-- Modal para el envio del recibo por correo -->
<div class="modal fade" id="modalDatosCliente" role="dialog">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Datos cliente</h4>
			</div>
			<div class="modal-body">

<form>

  <input type="hidden" id="idcliente" />

  <div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#persona" aria-controls="persona" role="tab" data-toggle="tab">persona</a></li>
    <li role="presentation"><a href="#direccion" aria-controls="direccion" role="tab" data-toggle="tab">direccion</a></li>
    <li role="presentation"><a href="#ubicacion" aria-controls="ubicacion" role="tab" data-toggle="tab">ubicacion</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="persona">
	    <!-- Persona -->
	    <br> 
	    <div class="step-content container-step-persona">
	        <div class="container-fluid">
	           <!-- <div class="row">
	                <div class="col-sm-6 col-md-3">
	                    <input type="radio" name="tipo_cliente" id="tipo_cliente1" class="radio_tipo_cliente" value="fisica" checked/>
	                    <label for="tipo_cliente1" class="label_tipo_cliente">Fisica</label>
	                </div>
	                <div class="col-sm-6 col-md-3">
	                    <input type="radio" name="tipo_cliente" id="tipo_cliente2" class="radio_tipo_cliente" value="moral"/>
	                    <label for="tipo_cliente2" class="label_tipo_cliente">Moral</label>
	                </div>
	                {#<div class="col-sm-12 form-group">#}
	                    {#<label> <input type="checkbox" id="is-fisica" name="is-fisica" class="i-checks" checked> Persona física/moral </label>#}
	                {#</div>#}
	            </div> -->
	            <div class="row">
	                <div class="col-md-3 form-group ocultar-fisica">
	                    <label>Ape paterno</label>
	                    <input id="apepat" name="apepat" type="text" placeholder="Apellido paterno" class="form-control">
	                </div>
	                <div class="col-md-3 form-group ocultar-fisica">
	                    <label>Ape materno</label>
	                    <input id="apemat" name="apemat" type="text" placeholder="Apellido materno" class="form-control">
	                </div>
	                <div class="col-md-6 form-group ocultar-fisica">
	                    <label>Nombre</label>
	                    <input id="nombres" name="nombres" type="text" placeholder="Nombre" class="form-control">
	                </div>
	             <!--   <div class="col-md-12 form-group ocultar-moral">
	                    <label>Nombre sucursal</label>
	                    <input id="nombre_sucursal" name="nombre_sucursal" type="text" placeholder="Sucursal" class="form-control">
	                </div>-->
	            </div>
	           <!-- <div class="row">
	                <div class="col-xs-9 col-sm-10 col-md-11 form-group">
	                    <label for="cliente_rs">Razón social</label>
	                    <select id="cliente_rs"  class="form-control" placeholder="Razón social">
	                        <option value="">Seleccionar</option>
	                        {% for rs in rss %}
	                            {% if !rs.fisica %}
	                                {% continue %}
	                            {% endif %}
	                            <option value="{{ rs.id }}">{{ rs.razon_social }}</option>
	                        {% endfor %}
	                    </select>
	                </div>
	                <div class="col-xs-3 col-sm-2 col-md-1 form-group">
	                    <button class="btn btn-sm btn-primary" id="rs_add_modal" style="margin-top: 27px;"><i class="fa fa-plus"></i></button>
	                </div>
	            </div> -->
	        </div>
	    </div>
	    <!-- ./Persona -->
    </div>
    <div role="tabpanel" class="tab-pane fade" id="direccion">
	    <!-- dirección -->
	    <br>
		<div class="container-fluid">
			<!-- area de comercio, abandonado, marginado, privada-->
			<div class="row">
				<div class="col-md-4 form-group {{ acl.isAllowedUser('caja', 'abandonado') or  acl.isAllowedUser('caja', 'marginado') ? '' : '' }}">
					{#{% if  acl.isAllowedUser('caja', 'abandonado') %}#}
					<label>Abandonado <input type="checkbox" id="chkabandonado"></label>
					{#{% endif %}#}
                    {#{% if  acl.isAllowedUser('caja', 'marginado') %}#}
					<label>Marginado <input type="checkbox" id="chkmarginado"></label>
                    {#{% endif %}#}
				</div>
				<div class="col-md-4 form-group {{ acl.isAllowedUser('caja', 'comercio') ? '' : '' }}">
					<label>Comercio <input type="checkbox" id="chkcomercio"></label>
					<div class="row">
						<div class="col-md-12 form-group">
							<label>Nombre</label>
							<input id="nombre_comercio" type="text" class="form-control" disabled>
						</div>
					</div>
				</div>
				<div class="col-md-4 form-group {{ acl.isAllowedUser('caja', 'privada') ? '' : '' }}">
					<label>Privada/cerrada <input type="checkbox" id="chkprivada"></label>
					<div class="row">
						<div class="col-md-12 form-group">
							<label>Nombre</label>
							<input id="nombre_privada" type="text" class="form-control" disabled>
						</div>
					</div>
				</div>
			</div>
			<!-- fin area de comercio, abandonado, marginado, privada-->
		<div class="row">	    
	        <div class="col-sm-6 border-right">



	            <div class="row">
	                <div class="col-sm-4 form-group">
	                    <label>Calle *</label>
	                    <input id="calle" name="calle" type="text" placeholder="Calle" class="form-control" required>
	                </div>
	                <div class="col-sm-4 form-group">
	                    <label>Letra</label>
	                    <input id="calle_letra" name="calle_letra" type="text" placeholder="Calle letra" class="form-control">
	                </div>
	                <div class="col-sm-4 form-group">
	                    <label>Tipo</label>
	                    <input id="tipo_calle" name="tipo_calle" type="text" placeholder="Tipo calle" class="form-control">
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-sm-4 form-group">
	                    <label>Número *</label>
	                    <input id="numero" name="numero" type="text" placeholder="Número" class="form-control" required>
	                </div>
	                <div class="col-sm-4 form-group">
	                    <label>Letra</label>
	                    <input id="numero_letra" name="numero_letra" type="text" placeholder="Número letra" class="form-control">
	                </div>
	                <div class="col-sm-4 form-group">
	                    <label>Tipo</label>
	                    <input id="tipo_numero" name="tipo_numero" type="text" placeholder="Tipo número" class="form-control">
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-sm-4 form-group">
	                    <label>Cruza1</label>
	                    <input id="cruza1" name="cruza1" type="text" placeholder="Cruzamiento 1" class="form-control">
	                </div>
	                <div class="col-sm-4 form-group">
	                    <label>Letra</label>
	                    <input id="letra_cruza1" name="letra_cruza1" type="text" placeholder="Letra cruzamiento 1" class="form-control">
	                </div>
	                <div class="col-sm-4 form-group">
	                    <label>Tipo</label>
	                    <input id="tipo_cruza1" name="tipo_cruza1" type="text" placeholder="Tipo cruzamiento 1" class="form-control">
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-sm-4 form-group">
	                    <label>Cruza2</label>
	                    <input id="cruza2" name="cruza2" type="text" placeholder="Cruzamiento 2" class="form-control">
	                </div>
	                <div class="col-sm-4 form-group">
	                    <label>Letra</label>
	                    <input id="letra_cruza2" name="letra_cruza2" type="text" placeholder="Letra cruzamiento 2" class="form-control">
	                </div>
	                <div class="col-sm-4 form-group">
	                    <label>Tipo</label>
	                    <input id="tipo_cruza2" name="tipo_cruza2" type="text" placeholder="Tipo cruzamiento 2" class="form-control">
	                </div>
	            </div>
	        </div>
	        <div class="col-sm-6">
	            <div class="row">
	                <div class="col-sm-6 form-group">
	                    <label>Colonia *</label>
	                    <select class="form-control" id="idcolonia" name="idcolonia" required>
	                        <option value="">Sel. Colonia</option>
	                        {% for colonia in colonias %}
	                            <option value="{{ colonia.id }}">{{ colonia.nombre }}</option>
	                        {% endfor %}
	                    </select>
	                </div>
	                <div class="col-sm-6 form-group hidden">
	                    <label>Localidad</label>
	                    <input id="localidad" name="localidad" type="text" placeholder="Localidad" class="form-control">
	                </div>
	                <div class="col-sm-6 form-group">
	                    <label>Folio catastral</label>
	                    <input id="folio_catastral" name="folio_catastral" type="text" placeholder="Folio catastral" class="form-control">
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-sm-6 form-group">
	                    <label>Otro</label>
	                    <input id="direccion_otro" name="direccion_otro" type="text" placeholder="Otro" class="form-control">
	                </div>

	                <div class="col-sm-6 form-group">
	                    <label>Referencia ubicación</label>
	                    <input id="referencia_ubicacion" name="referencia_ubicacion" type="text" placeholder="Referencia ubicación" class="form-control">
	                </div>
	            </div>
	        </div>
		</div>
	    </div>
	    <!-- ./dirección -->
    </div>
    <div role="tabpanel" class="tab-pane fade" id="ubicacion">
	    <!-- Ubicacion -->
	    <div class="container-fluid">
	       <div class="row" style="margin-top: 10px;" id="loading-map2"></div>
				<div class="row">
					<div class="col-md-12">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Buscar..." id="cliente-input-gelocation2">
							<span class="input-group-btn">								
								<button class="btn btn-primary" type="button" id="cliente-search-location2">Buscar</button>
							</span>
						</div>
					</div>
				</div>
				<div id="map2" class="map"></div>						
				<!-- div class="row">
					<div class="wrapper wrapper-content animated fadeInRight">
                        <div id="map" class="map"></div>
                    </div>
				</div -->
	    </div>
	    <!-- ./Ubicacion -->  
    </div>
  </div>

 </div>  
 
</form>
			
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-sm" id="btn-rsocial-email" onclick="fnActualizarDatosCliente();">
					<i class="fa fa-save" aria-hidden="true"></i> Guardar
				</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
		</div>      
	</div>
</div><!--[Modal] -->  	
<!-- ------------------------------------------------------- -->




<div id="modalDatosPago" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Datos del pago</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="display table-striped table-hover" id="datos-historial-table" style="width: 100%;">
						<thead>
						<tr>
							<th>Año</th>
							<th>Mes</th>
							<th>Descuento</th>
							<th>Total</th>
						</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<div id="modalMotivoCancelacion" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
{#				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>#}
				<h4 class="modal-title">Motivo de cancelación</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<textarea class="form-control" id="txtMotivoCancelacion"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="btnDoCancelCancelMotivo">Cerrar</button>
				<button type="button" class="btn btn-primary" id="btnDoCancelMotivo" onclick="fnCancelarConMotivo()">Aceptar</button>
			</div>
		</div>
	</div>
</div>

