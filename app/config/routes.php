<?php
/*
 * Define custom routes. File gets included in the router service definition.
 */
$router = new Phalcon\Mvc\Router();

$router->add('/confirm/{code}/{email}', array(
    'controller' => 'user_control',
    'action' => 'confirmEmail'
));

$router->add('/reset-password/{code}/{email}', array(
    'controller' => 'user_control',
    'action' => 'resetPassword'
));

$router->add('/api/clientes/{id}/adeudo', array(
    'controller' => 'api',
    'action' => 'clientesAdeudo'
));

$router->add('/api/clientes/{id}/historial', array(
    'controller' => 'api',
    'action' => 'clientehistorial'
));

$router->add('/api/clientes/{id}/historialv2', array(
    'controller' => 'api',
    'action' => 'clientehistorialv2'
));

$router->add('/api/clientes/{id}/reportar', array(
    'controller' => 'api',
    'action' => 'clientereportar'
));

$router->add('/api/clientes/{id}/recibo', array(
    'controller' => 'api',
    'action' => 'clientesdescargarrecibo'
));

$router->add('/api/clientes/{id}/recibov2', array(
    'controller' => 'api',
    'action' => 'clientesdescargarrecibov2'
));

$router->add('/api/clientes/{id}/agendar', array(
    'controller' => 'api',
    'action' => 'clientesagendarcita'
));

$router->add('/api/clientes/{id}/misservicios', array(
    'controller' => 'api',
    'action' => 'misservicios'
));

$router->add('/api/clientes/{id}/agendarv2', array(
    'controller' => 'api',
    'action' => 'clientesagendarcitas'
));

$router->add('/api/clientes/{id}/misserviciosv2', array(
    'controller' => 'api',
    'action' => 'misservicioscondetalle'
));

/*
* Rutas clientes
 */
/*$router->add('/cliente', array(
	'controller' => 'cliente',
	'action' => 'index',
));*/

return $router;
