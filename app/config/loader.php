<?php
$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerNamespaces(array(
    'Vokuro\Models' => $config->application->modelsDir,
    'Vokuro\Controllers' => $config->application->controllersDir,
    'Vokuro\Forms' => $config->application->formsDir,
    'Vokuro' => $config->application->libraryDir
));

$loader->registerClasses(
	array(
		"PHPExcel"         => "../app/library/PHPExcel/Classes/PHPExcel.php",
		"PHPExcel_Style_Fill"         => "../app/library/PHPExcel/Classes/Style/Fill.php",
		"PHPExcel_Writer_Excel2007"         => "../app/library/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php",
		"PHPExcel_Settings"         => "../app/library/PHPExcel/Classes/PHPExcel/Settings.php"
	)
);

$loader->register();

// Use composer autoloader to load vendor classes
require_once __DIR__ . '/../../vendor/autoload.php';
