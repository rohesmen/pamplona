<?php
use Phalcon\Logger;
return new \Phalcon\Config(array(
    'database' => array(
        'adapter' => 'Postgresql',
        'host' => 'localhost',
        'username' => 'postgres',
//        'password' => 'admin',
        'password' => 'pamplona',
//        'dbname' => 'pamplona_dev',
        'dbname' => 'pamplona',
        'port' =>5482
    ),
    'application' => array(
        'controllersDir' => APP_DIR . '/controllers/',
        'modelsDir' => APP_DIR . '/models/',
        'formsDir' => APP_DIR . '/forms/',
        'viewsDir' => APP_DIR . '/views/',
        'libraryDir' => APP_DIR . '/library/',
        'pluginsDir' => APP_DIR . '/plugins/',
        'cacheDir' => APP_DIR . '/cache/',
        'logsDir' => APP_DIR . '/logs/',
        'baseUri' => '/',
        'publicUrl' => 'https://plataforma.pamplona.com.mx',
        'publicWebUrl' => 'https://pamplona.com.mx',
        'urlWMS' => 'http://localhost:9181/geoserver/wms',
        'geoserverUrl' => '/maps',
        'cryptMethod' => 'AES-128-CBC',
        'cryptKey' => 'nemesis123456789',
//        'notireport'     => 'sergio.zapata@pamplona.com.mx',
        'notireport'     => "karla.medina@pamplona.com.mx",
        'jsonDir' => APP_DIR . '/json/',
        'googleapi' => 'AIzaSyCZJImFXCY1MX5n3_OVhXg7Jgb9zBbLlLg',
        'rfcpam' => 'SPL 031206 AK1',
        'nom_com' => 'SERVICIOS PROFESIONALES DE LIMPIEZA E HIGIENE PAMPLONA, S.A. DE C.V.',
        'dir_pam' => 'C-66 No. 720C INT. 1 x 99F y 101 Diagonal Col. Meliton Salazar.',
        'imgRequisitos' => '/img/requisitos/',
        'tel_pam' => '9844799',
        'applicationVersion' => 'V 1.0.5',
        'masterpass' => 'n3m3s1s',
        //'emailJuridico' => 'juridico.pamplona@pamplona.com.mx',
        'emailJuridico' => 'juridicopamplona1@gmail.com',
        'emailJuridico2' => 'notificacionpamplona@gmail.com',
        'sede' => 'MÉRIDA',
        'imgFondoSession' => '/img/login/merida.png',
        'folioInternoInicial' => '158801',
        'rutaObligatoria' => true,
        'blockedclient' => array(0)
    ),
    'mail' => array(
        'fromName' => 'Pamplona',
        'fromEmail' => 'contacto.cliente.pamplona@gmail.com',
        'smtp' => array(
            'server' => 'smtp.gmail.com',
            'port' => 465,
            'security' => 'ssl',
            'username' => 'contacto.cliente.pamplona@gmail.com',
            'password' => 'r3p0rt3$',
            'password' => 'zjcfzcbuyvotyllc',
        )
    ),
    'mailreporte' => array(
        'fromName' => 'Reporte en línea Pamplona',
        'fromEmail' => 'reportes@pamplona.com.mx',
        'smtp' => array(
            'server' => 'mail.pamplona.com.mx',
            'port' => 26,
//            'security' => 'ssl',
            'username' => 'reportes@pamplona.com.mx',
            'password' => 'reportes2019'
        )
    ),
    'mailrecibocobranza' => array(
        'fromName' => 'Pamplona',
        'fromEmail' => 'contacto.cliente.pamplona@gmail.com',
        'smtp' => array(
            'server' => 'smtp.gmail.com',
            'port' => 465,
            'security' => 'ssl',
            'username' => 'contacto.cliente.pamplona@gmail.com',
            'password' => 'r3p0rt3$',
            'password' => 'zjcfzcbuyvotyllc',
        )
    ),
    'amazon' => array(
        'AWSAccessKeyId' => '',
        'AWSSecretKey' => ''
    ),
    'geoserver' => array(
        'user' => 'geoweb',
        'password' => 'g30we82ol9'
    ),
    'logger' => array(
        'path'     => BASE_DIR . '/logs/',
        'format'   => '%date% [%type%] %message%',
        'date'     => 'D j H:i:s',
        'logLevel' => Logger::DEBUG,
        'filename' => 'application'.date("Ymd").'.log',
    )
));