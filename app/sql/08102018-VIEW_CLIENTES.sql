﻿DROP VIEW cliente.view_clientes;

CREATE OR REPLACE VIEW cliente.view_clientes AS
select c.*, COALESCE(TRIM(c.calle), '') || ' ' || COALESCE(TRIM(c.calle_letra), '') calle_completa, COALESCE(TRIM(c.numero), '') || ' ' || COALESCE(TRIM(c.numero_letra), '') numero_completa,
COALESCE(TRIM(c.cruza1), '') || ' ' || COALESCE(TRIM(c.letra_cruza1), '') cruza1_completa, COALESCE(TRIM(c.cruza2), '') || ' ' || COALESCE(TRIM(c.letra_cruza2), '') cruza2_completa,
COALESCE(c.apepat, '')||' '||COALESCE(c.apemat, '')||' '||COALESCE(c.nombres, '')  persona,
COALESCE(c.apepat_propietario, '')||' '||COALESCE(c.apemat_propietario, '')||' '||COALESCE(c.nombres_propietario, '')  propietario,
COALESCE(col.nombre, '')||', '||COALESCE(col.localidad, '') colonia 
from cliente.cliente c
left join cliente.colonia as col on col.id = c.idcolonia;


