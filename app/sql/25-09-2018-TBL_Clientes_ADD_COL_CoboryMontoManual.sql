﻿/***************************************************************
* SISTEMA: Pamplona      			               *
* Modulo: Clientes                                             *
* Objetivo: Agregar columnas cobo_manual y monto_Cobro         *
* Versión    Fecha        Usuario         Descripcion          *
* ------     ---------    ---------       ---------------------*
* 1.0        25/09/2018   Jacobo Romero   Agergar las columnas *
*                                          -cobo_manual        *
*                                          -monto_Cobro        */


/*Se agegan las columnas cobro_manual*/
ALTER TABLE cliente.cliente ADD COLUMN cobro_manual boolean not null DEFAULT false;

/*Se agegan las columnas monto_cobro*/
ALTER TABLE cliente.cliente ADD COLUMN monto_cobro numeric not null DEFAULT 0;
